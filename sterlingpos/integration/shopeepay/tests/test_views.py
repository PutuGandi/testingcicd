import json
import datetime
import hashlib

from test_plus.test import TestCase

from model_mommy import mommy
from django.urls import reverse
from django.conf import settings
from django.test import override_settings, RequestFactory
from django.test.client import Client
from django.utils.translation import ugettext_lazy as _, ugettext

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant

from shopeepay.session import ShopeePaySession


class TestShopeePayNotificationView(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account-1')
        self.user = mommy.make(
            'users.User', email='test@email.com', account=self.account)
        self.outlet = mommy.make('outlet.Outlet', branch_id='branch-1', account=self.account)

        self.shoopepay_setting = mommy.make(
            'shopeepay.ShopeePaySettings',
            merchant_ext_id="9-240",
            account=self.account)

        mommy.make(
            'shopeepay.ShopeePayOutletStore',
            shoopepay_setting=self.shoopepay_setting,
            outlet=self.outlet,
            store_ext_id="9-240-footista.boga",
            account=self.account)

        self.shopeepay_session = ShopeePaySession(
            settings.PAYMENT["SHOPEEPAY"]["KEY"],
            settings.PAYMENT["SHOPEEPAY"]["SECRET"],
        )
        self.data = {
            "amount": 2500000,
            "merchant_ext_id": "9-240",
            "payment_status": 1,
            "reference_id": "simple02",
            "store_ext_id": "9-240-footista.boga",
            "terminal_id": "T01",
            "transaction_sn": "000000351706002216",
            "user_id_hash": "f355f673cc7e2d07b610b2a3646ca085b28eff7cf29ccda0ca8ce9875d64a002"
        }
    def tearDown(self):
        set_current_tenant(None)

    def test_view(self):

        headers = {
            "HTTP_X_AIRPAY_CLIENTID": "KAWN",
            "HTTP_X_AIRPAY_REQ_H": self.shopeepay_session._generate_signature(self.data),
        }       
        response = self.client.post(path='/callback/shopeepay/',
                                    data=self.data,
                                    content_type='application/json',
                                    **headers,
                                )
        self.assertTrue(response.status_code, 200)

    def test_view_bad_param(self):
        data = {}
        headers = {
            "HTTP_X_AIRPAY_CLIENTID": "KAWN",
            "HTTP_X_AIRPAY_REQ_H": self.shopeepay_session._generate_signature(
                data),
        }       
        response = self.client.post(path='/callback/shopeepay/',
                                    data=data,
                                    content_type="multipart/byteranges",
                                    **headers)
        self.assertTrue(response.status_code, 200)

    def test_view_bad_header(self):
        headers = {
            "HTTP_X_AIRPAY_CLIENTID": "KAWN",
            "HTTP_X_AIRPAY_REQ_H": "BAD-TOKEN",
        }        
        response = self.client.post(path='/callback/shopeepay/',
                                    data=self.data,
                                    content_type="multipart/byteranges",
                                    **headers)
        self.assertTrue(response.status_code, 105)
