from django.conf import settings

from django.db import models, DataError
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _, ugettext

from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.outlet.models import Outlet
from sterlingpos.core.models import (
    SterlingTenantModel,
    SterlingTenantForeignKey,
)


def _generate_merchant_ext_id(account):
    return f"1-{account.pk}-{account.name.replace(' ', '')}"


def _generate_store_ext_id(outlet):
    return f"{_generate_merchant_ext_id(outlet.account)}.{slugify(outlet.name)}.{outlet.pk}"


class ShopeePaySettings(SterlingTenantModel, TimeStampedModel):

    merchant_ext_id = models.CharField(max_length=255)

    class Meta:
        verbose_name = "ShopeePaySettings"
        verbose_name_plural = "ShopeePaySettings"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "ShopeePay {}".format(self.merchant_ext_id)

    @classmethod
    def generate_settings(cls, account):
        try:
            obj = cls.objects.get(account=account)
            return obj
        except cls.DoesNotExist:
            merchant_ext_id = _generate_merchant_ext_id(account)
            obj, _ = cls.objects.get_or_create(merchant_ext_id=merchant_ext_id)
            return obj   

    @property
    def store_ext_ids(self):
        return [
            {
                "name": instance.outlet.name,
                "id": instance.outlet.name,
                "branch_id": instance.outlet.branch_id,
                "store_ext_id": instance.store_ext_id,
            }
            for instance in self.outlet_stores.filter(is_active=True)
        ]


class ShopeePayOutletStore(SterlingTenantModel, TimeStampedModel):
    shoopepay_setting = SterlingTenantForeignKey(
        ShopeePaySettings, related_name="outlet_stores", on_delete=models.CASCADE)
    outlet = SterlingTenantForeignKey(Outlet, on_delete=models.CASCADE)
    store_ext_id = models.CharField(max_length=255)

    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "ShopeePayOutletStore"
        verbose_name_plural = "ShopeePayOutletStores"
        unique_together = (("account", "id"),)

    @classmethod
    def generate_settings(cls, shoopepay_setting, outlet):
        try:
            obj = cls.objects.get(
                shoopepay_setting=shoopepay_setting,
                outlet=outlet
            )
            return obj
        except cls.DoesNotExist:
            store_ext_id = _generate_store_ext_id(outlet)
            obj, _ = cls.objects.get_or_create(
                shoopepay_setting=shoopepay_setting,
                outlet=outlet,
                store_ext_id=store_ext_id)
            return obj


class ShopeePayHandler:

    def generate_settings(self, account):
        return ShopeePaySettings.generate_settings(account)

    def generate_outlet_setting(self, shopeepay_setting, outlet):
        return ShopeePayOutletStore.generate_settings(
            shopeepay_setting, outlet)

shopeepay_handler = ShopeePayHandler()
