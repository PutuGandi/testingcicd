import json
import logging
import hmac
import hashlib
import base64

from django.conf import settings
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

logger = logging.getLogger(__name__)


from shopeepay.session import ShopeePaySession


from sterlingpos.integration.shopeepay.models import ShopeePaySettings
from sterlingpos.push_notification.tasks import delayed_build_push_notif


def _generate_signature(secret, payload):
    request_json = json.dumps(payload).replace(" ", "").encode("utf-8")
    digest = hmac.new(secret, request_json, hashlib.sha256).digest()
    signature = base64.b64encode(digest).decode()

    return signature


class ShopeepayNotificationHandlingView(View):

    def get_shopeepay_session(self):
        shopeepay_session = ShopeePaySession(
            settings.PAYMENT["SHOPEEPAY"]["KEY"],
            settings.PAYMENT["SHOPEEPAY"]["SECRET"],
        )
        return shopeepay_session

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def valid_header(self, request, response_json):
        x_airpay_req_h = request.META.get("HTTP_X_AIRPAY_REQ_H", None)
        shopeepay_session = self.get_shopeepay_session()
        signature = _generate_signature(
            shopeepay_session.secret,
            response_json
        )
        logger.info(f"Shopeepay: Notification Validation {signature} - {x_airpay_req_h}")
        return x_airpay_req_h == signature

    def decode_response_body(self, response_body):
        try:
            response_json = json.loads(
                response_body[0].decode().replace("'", '"'))

            log_data = "Shopeepay: Notification data={} ".format(
                json.dumps(response_json)
            )
            logger.info(log_data)
        except Exception as e:
            logger.warning(f"Shopeepay: Unable to process Notification data: {response_body} due to {e}")
            response_json = {}
        
        return response_json

    def post(self, request, *args, **kwargs):
        response_body = request.readlines()

        response_json = self.decode_response_body(response_body)

        if not self.valid_header(request, response_json):
            response = HttpResponse("Not Authorized")
            response.status_code = 105
            response_json.update({"err": "Bad Signature"})
        else:
            response = HttpResponse("OK")
            response_json.update({"err": None})

        try:
            merchant = ShopeePaySettings.objects.get(
                merchant_ext_id=response_json.get("merchant_ext_id"))
            outlet_store = merchant.outlet_stores.get(
                store_ext_id=response_json.get("store_ext_id"))
            fcm_devices = merchant.account.registeredfcmdevice_set.all()

            registration_ids = fcm_devices.values_list("registered_id", flat=True)
            data_message = {
                "mode": "UPDATE_SHOPEEPAY_PAYMENT_CHECK_NOTIFICATION",
                "title": "Update Shopeepay Payment Check for payment {}".format(
                    response_json.get("reference_id")),
                "message": "Update Shopeepay Payment Check for payment {}".format(
                    response_json.get("reference_id")),
                "outlet_id": outlet_store.outlet.pk,
                "reference_id": response_json.get("reference_id"),
                "raw_data": response_json
            }
            delayed_build_push_notif.delay(list(registration_ids), data_message)
        except ShopeePaySettings.DoesNotExist as e:
            logger.warning(f"Shopeepay: Unable to process Notification data: {response_body} due to {e}")

        return response
