from django.apps import AppConfig


class ShopeePayConfig(AppConfig):
    name = "sterlingpos.integration.shopeepay"
    verbose_name = "ShopeePay"

    def ready(self):
        pass
