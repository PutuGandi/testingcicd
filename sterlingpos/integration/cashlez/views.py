import json
import logging

from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

logger = logging.getLogger(__name__)

from sterlingpos.integration.shopeepay.models import ShopeePaySettings
from sterlingpos.push_notification.tasks import delayed_build_push_notif


class ShopeepayNotificationHandlingView(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        response_body = request.readlines()
        response_json = json.loads(
            response_body[0].decode().replace("'", '"'))

        log_data = "Shopeepay: Notification data={} ".format(
            json.dumps(response_json)
        )
        logger.info(log_data)
        
        try:
            merchant = ShopeePaySettings.objects.get(
                merchant_ext_id=response_json.get("merchant_ext_id"))
            outlet_store = merchant.outlet_stores.get(
                store_ext_id=response_json.get("store_ext_id"))
            fcm_devices = merchant.account.registeredfcmdevice_set.all()

            registration_ids = fcm_devices.values_list("registered_id", flat=True)
            data_message = {
                "mode": "UPDATE_SHOPEEPAY_PAYMENT_CHECK_NOTIFICATION",
                "title": "Update Shopeepay Payment Check for payment {}".format(
                    response_json.get("reference_id")),
                "message": "Update Shopeepay Payment Check for payment {}".format(
                    response_json.get("reference_id")),
                "outlet_id": outlet_store.outlet.pk,
                "reference_id": response_json.get("reference_id"),
            }
            delayed_build_push_notif.delay(list(registration_ids), data_message)
        except ShopeePaySettings.DoesNotExist:
            pass

        return HttpResponse("OK")
