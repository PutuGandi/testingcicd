from django.apps import AppConfig


class CashlezConfig(AppConfig):
    name = "sterlingpos.integration.cashlez"
    verbose_name = "Cashlez"

    def ready(self):
        pass
