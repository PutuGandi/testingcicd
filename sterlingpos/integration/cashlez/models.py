from django.conf import settings

from django.db import models, DataError
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _, ugettext

from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.outlet.models import Outlet
from sterlingpos.core.models import (
    SterlingTenantModel,
    SterlingTenantForeignKey,
)


def _generate_merchant_ext_id(account):
    return f"1-{account.pk}-{account.name.replace(' ', '')}"


def _generate_store_ext_id(outlet):
    return f"{_generate_merchant_ext_id(outlet.account)}.{slugify(outlet.name)}.{outlet.pk}"


class CashlezSettings(SterlingTenantModel, TimeStampedModel):

    merchant_ext_id = models.CharField(max_length=255)

    class Meta:
        verbose_name = "CashlezSettings"
        verbose_name_plural = "CashlezSettings"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Cashlez {}".format(self.merchant_ext_id)

    @classmethod
    def generate_settings(cls, account):
        merchant_ext_id=_generate_merchant_ext_id(account)
        obj, _ = cls.objects.get_or_create(merchant_ext_id=merchant_ext_id)
        return obj   

    @property
    def store_ext_ids(self):
        return [
            {
                "name": instance.outlet.name,
                "id": instance.outlet.name,
                "branch_id": instance.outlet.branch_id,
                "store_ext_id": instance.store_ext_id,
            }
            for instance in self.outlet_stores.filter(is_active=True)
        ]


class CashlezOutletStore(SterlingTenantModel, TimeStampedModel):
    cashlez_setting = SterlingTenantForeignKey(
        CashlezSettings, related_name="outlet_stores", on_delete=models.CASCADE)
    outlet = SterlingTenantForeignKey(Outlet, on_delete=models.CASCADE)
    store_ext_id = models.CharField(max_length=255)

    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "CashlezOutletStore"
        verbose_name_plural = "CashlezOutletStores"
        unique_together = (("account", "id"),)

    @classmethod
    def generate_settings(cls, cashlez_setting, outlet):
        store_ext_id=_generate_store_ext_id(outlet)
        obj, _ = cls.objects.get_or_create(
            cashlez_setting=cashlez_setting,
            outlet=outlet,
            store_ext_id=store_ext_id)
        return obj
