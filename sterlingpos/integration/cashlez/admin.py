from django.contrib import admin

from sterlingpos.integration.cashlez.models import CashlezSettings, CashlezOutletStore


class CashlezSettingsAdmin(admin.ModelAdmin):

    list_display = ("account", "merchant_ext_id", )
    search_fields = ("account__name", "merchant_ext_id", )
    raw_id_fields = ("account",)


class CashlezOutletStoreAdmin(admin.ModelAdmin):

    list_display = ("outlet", "account", "store_ext_id", "is_active", )
    list_filter = ("is_active", )
    search_fields = ("outlet__name", "account__name", "store_ext_id", )
    raw_id_fields = ("outlet", "account", "cashlez_setting")


admin.site.register(CashlezSettings, CashlezSettingsAdmin)
admin.site.register(CashlezOutletStore, CashlezOutletStoreAdmin)
