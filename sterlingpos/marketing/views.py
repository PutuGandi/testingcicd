from django.urls import reverse_lazy
from django.shortcuts import render_to_response
from django.http import HttpResponsePermanentRedirect, HttpResponseRedirect, HttpResponseGone
from django.views.generic import TemplateView, RedirectView
from django.views.generic.edit import FormMixin

from sterlingpos.users.forms import UserSignupForm

class HomeView(TemplateView):
    template_name = "marketing/home.html"


class AboutView(TemplateView):
    template_name = "marketing/about.html"


class FeaturesView(TemplateView):
    template_name = "marketing/features.html"


class PricingView(TemplateView):
    template_name = "marketing/pricing.html"

class SignUpRedirectView(RedirectView):
    url = reverse_lazy('account_signup')

