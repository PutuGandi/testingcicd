from django.db.models import Count

from sterlingpos.outlet.models import Outlet
from sterlingpos.catalogue.models import Product
from sterlingpos.stocks.models import Stock, StockTransaction, Batch, BatchItem
from sterlingpos.notification.models import NotificationSetting
from sterlingpos.notification.tasks import notify_stocks_email
from sterlingpos.stocks.tasks import (
    celery_update_stock_tail_sum_for,
    locking_stock_transaction_aggregate,
)
from sterlingpos.productions.tasks import (
    product_order_proses_production,
    product_composite_proses_production,
    product_order_addon_process_production,
)


def create_missing_batch_sales(instance, quantity, outlet, sales_order):
    missing_name = "Missing Batch"
    missing_batch, _ = Batch.objects.get_or_create(
        product=instance.product, is_missing_batch=True, batch_id=missing_name
    )
    batch_outlet, _ = missing_batch.batch_items.get_or_create(outlet=outlet)
    batch_outlet.substract_batch_balance(
        quantity,
        unit=instance.product.uom,
        transaction_type=StockTransaction.TRANSACTION_TYPE.transaction,
        source_object=sales_order,
    )


def update_batch_stock_empty(outlet, batches):
    if batches:
        for batch in batches:
            batch_outlet = batch.batch_items.get(outlet=outlet)
            batch_outlet.delete()
            if not batch.batch_items.all():
                batch.delete()


def update_batch_item(instance, quantity, outlet, sales_order, batches):
    if batches:
        for batch in batches:
            try:
                batch_outlet = batch.batch_items.get(outlet=outlet)
                if quantity == 0:
                    break
                if batch.is_missing_batch:
                    batch_outlet.substract_batch_balance(
                        quantity,
                        unit=instance.product.uom,
                        transaction_type=StockTransaction.TRANSACTION_TYPE.transaction,
                        source_object=sales_order,
                    )
                    quantity = 0
                else:
                    quantity_temp = batch_outlet.balance - quantity
                    qty = batch_outlet.balance - quantity_temp
                    if quantity_temp < 0 and batch_outlet.balance > 0:
                        qty = quantity - abs(quantity_temp)
                    quantity -= qty
                    batch_outlet.substract_batch_balance(
                        qty,
                        unit=instance.product.uom,
                        transaction_type=StockTransaction.TRANSACTION_TYPE.transaction,
                        source_object=sales_order,
                    )
                    if not batch_outlet.balance:
                        batch_outlet.delete()
                    if not batch.balance:
                        batch.delete()

            except BatchItem.DoesNotExist:
                if batch.is_missing_batch:
                    batch_outlet, _ = batch.batch_items.get_or_create(outlet=outlet)
                    batch_outlet.substract_batch_balance(
                        quantity,
                        unit=instance.product.uom,
                        transaction_type=StockTransaction.TRANSACTION_TYPE.transaction,
                        source_object=sales_order,
                    )
                else:
                    if quantity != 0:
                        create_missing_batch_sales(
                            instance, quantity, outlet, sales_order
                        )
                quantity = 0
    else:
        create_missing_batch_sales(instance, quantity, outlet, sales_order)


def update_product_stock(sender, instance, *args, **kwargs):
    created = kwargs.get("created")
    if created:
        if instance.product.is_manage_stock and instance.sales_order.is_settled:
            product_stock = instance.product.stocks.get(
                outlet=instance.sales_order.outlet
            )
            quantity_diff = product_stock.balance - instance.quantity
            notify_to_email = True
            auto_production = False
            if (
                instance.product.classification
                == Product.PRODUCT_CLASSIFICATION.manufactured
                and instance.product.bom.count()
                and quantity_diff < 0
            ):
                product_order_proses_production.apply_async(
                    countdown=5,
                    kwargs={
                        "product_order_pk": instance.pk,
                        "outlet_pk": instance.sales_order.outlet.pk,
                        "quantity": abs(quantity_diff),
                        "account_pk": instance.account.pk,
                        "sales_order_code": instance.sales_order.code,
                    },
                )
                notify_to_email = False
                auto_production = True

            product_stock.subtract_balance(
                quantity=instance.quantity,
                unit=instance.product.uom,
                transaction_type=StockTransaction.TRANSACTION_TYPE.transaction,
                source_object=instance.sales_order,
            )
            enable_notify_stocks = (
                instance.account.notificationsetting_set.filter(
                    notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification
                )
                .first()
                .is_enabled
            )

            if (
                product_stock.balance <= instance.product.min_stock_alert
                and enable_notify_stocks
            ):
                recipients = NotificationSetting.get_email_recipients_for_type(
                    NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
                    instance.account,
                )

                if recipients and notify_to_email:
                    notify_stocks_email.delay(
                        instance.product.__str__(),
                        product_stock.balance,
                        "{}, {}, {}".format(
                            product_stock.outlet.name,
                            product_stock.outlet.city,
                            product_stock.outlet.province,
                        ),
                        recipients,
                    )

            if instance.product.is_batch_tracked and not auto_production:
                batches = Batch.objects.filter(
                    product=instance.product, deleted__isnull=True
                )
                batches = batches.annotate(num_outlets=Count("batch_items")).order_by(
                    "expiration_date", "is_missing_batch", "num_outlets"
                )
                outlet = instance.sales_order.outlet
                if quantity_diff:
                    quantity = instance.quantity
                    sales_order = instance.sales_order
                    update_batch_item(instance, quantity, outlet, sales_order, batches)
                else:
                    update_batch_stock_empty(outlet, batches)


def update_composite_detail_stock(sender, instance, *args, **kwargs):
    created = kwargs.get("created")
    if created:
        if (
            instance.product_order.product.classification
            in [
                Product.PRODUCT_CLASSIFICATION.composite,
                Product.PRODUCT_CLASSIFICATION.dynamic_composite,
            ]
            and instance.product.is_manage_stock
            and instance.product_order.sales_order.is_settled
        ):
            product_stock = instance.product.stocks.get(
                outlet=instance.product_order.sales_order.outlet
            )
            quantity_diff = product_stock.balance - (
                instance.product_order.quantity * instance.product_quantity
            )
            notify_to_email = True
            auto_production = False
            if (
                instance.product.classification
                == Product.PRODUCT_CLASSIFICATION.manufactured
                and instance.product.bom.count()
                and quantity_diff < 0
            ):
                product_composite_proses_production.apply_async(
                    countdown=5,
                    kwargs={
                        "product_order_composite_pk": instance.pk,
                        "outlet_pk": instance.product_order.sales_order.outlet.pk,
                        "quantity": abs(quantity_diff),
                        "account_pk": instance.account.pk,
                        "sales_order_code": instance.product_order.sales_order.code,
                    },
                )
                notify_to_email = False
                auto_production = True

            product_stock.subtract_balance(
                quantity=(instance.product_order.quantity * instance.product_quantity),
                unit=instance.product.uom,
                transaction_type=StockTransaction.TRANSACTION_TYPE.transaction,
                source_object=instance.product_order.sales_order,
            )
            enable_notify_stocks = (
                instance.account.notificationsetting_set.filter(
                    notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification
                )
                .first()
                .is_enabled
            )

            if (
                product_stock.balance <= instance.product.min_stock_alert
                and enable_notify_stocks
            ):
                recipients = NotificationSetting.get_email_recipients_for_type(
                    NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
                    instance.account,
                )

                if recipients and notify_to_email:
                    notify_stocks_email.delay(
                        instance.product.__str__(),
                        product_stock.balance,
                        "{}, {}, {}".format(
                            product_stock.outlet.name,
                            product_stock.outlet.city,
                            product_stock.outlet.province,
                        ),
                        recipients,
                    )

            if instance.product.is_batch_tracked and not auto_production:
                batches = Batch.objects.filter(
                    product=instance.product, deleted__isnull=True
                )
                batches = batches.annotate(num_outlets=Count("batch_items")).order_by(
                    "expiration_date", "is_missing_batch", "num_outlets"
                )
                outlet = instance.product_order.sales_order.outlet
                if quantity_diff:
                    quantity = instance.product_order.quantity * instance.product_quantity
                    sales_order = instance.product_order.sales_order
                    update_batch_item(instance, quantity, outlet, sales_order, batches)
                else:
                    update_batch_stock_empty(outlet, batches)


def update_addon_detail_stock(sender, instance, *args, **kwargs):
    created = kwargs.get("created")
    if created:
        if (
            instance.product.classification
            == Product.PRODUCT_CLASSIFICATION.complementary
            and instance.product.is_manage_stock
            and instance.product_order.sales_order.is_settled
        ):
            product_stock = instance.product.stocks.get(
                outlet=instance.product_order.sales_order.outlet
            )
            quantity_diff = product_stock.balance - instance.quantity
            notify_to_email = True
            auto_production = False

            if instance.product.bom.exists() and quantity_diff < 0:
                product_order_addon_process_production.apply_async(
                    countdown=5,
                    kwargs={
                        "product_addon_order_pk": instance.pk,
                        "outlet_pk": instance.product_order.sales_order.outlet.pk,
                        "quantity": abs(quantity_diff),
                        "account_pk": instance.account.pk,
                    },
                )
                notify_to_email = False
                auto_production = True

            product_stock.subtract_balance(
                quantity=(instance.quantity),
                unit=instance.product.uom,
                transaction_type=StockTransaction.TRANSACTION_TYPE.transaction,
                source_object=instance.product_order.sales_order,
            )

            enable_notify_stocks = (
                instance.account.notificationsetting_set.filter(
                    notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification
                )
                .first()
                .is_enabled
            )

            if (
                product_stock.balance <= instance.product.min_stock_alert
                and enable_notify_stocks
            ):
                recipients = NotificationSetting.get_email_recipients_for_type(
                    NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
                    instance.account,
                )

                if recipients and notify_to_email:
                    notify_stocks_email.delay(
                        instance.product.__str__(),
                        product_stock.balance,
                        "{}, {}, {}".format(
                            product_stock.outlet.name,
                            product_stock.outlet.city,
                            product_stock.outlet.province,
                        ),
                        recipients,
                    )

            if instance.product.is_batch_tracked and not auto_production:
                batches = Batch.objects.filter(
                    product=instance.product, deleted__isnull=True
                )
                batches = batches.annotate(num_outlets=Count("batch_items")).order_by(
                    "expiration_date", "is_missing_batch", "num_outlets"
                )
                outlet = instance.product_order.sales_order.outlet
                if product_stock.balance:
                    quantity = instance.quantity
                    sales_order = instance.product_order.sales_order
                    update_batch_item(instance, quantity, outlet, sales_order, batches)
                else:
                    update_batch_stock_empty(outlet, batches)


def create_stock_for_product(sender, instance, *args, **kwargs):
    if instance.is_manage_stock:
        outlet_list = Outlet.objects.filter(
            account=instance.account, stocks__product__pk__in=[instance.pk]
        )
        for outlet in Outlet.objects.filter(account=instance.account).exclude(
            pk__in=outlet_list
        ):
            Stock.objects.create_for_product(product=instance, outlet=outlet)


def create_stock_for_outlet(sender, instance, *args, **kwargs):
    created = kwargs.get("created")
    if created:
        for product in Product.objects.filter(
            account=instance.account, is_manage_stock=True
        ):
            Stock.objects.create_for_product(product=product, outlet=instance)


def update_stock_transaction_tail_sum(sender, instance, *args, **kwargs):
    created = kwargs.get("created")
    if created:

        prev_stock_transaction = (
            instance.stock.transactions.filter(
                created__lt=instance.created,
                tail_sum__isnull=False,
            )
            .order_by("-pk")
            .first()
        )

        if prev_stock_transaction:
            instance.tail_sum = prev_stock_transaction.tail_sum + instance.quantity
            instance.save()
        else:
            instance.tail_sum = instance.quantity
            instance.save()


def update_hourly_stock_transaction_aggregate(
        sender, instance, *args, **kwargs):
    created = kwargs.get("created")
    if created:
        locking_stock_transaction_aggregate.apply_async(
            countdown=10,
            kwargs={
                "account_pk": instance.account.pk,
                "start_date": instance.created.date(),
                "end_date": instance.created.replace(
                    hour=23, minute=59, second=59, microsecond=59),
                "stock_pk": instance.stock.pk,
            },
        )
