import types
from collections import namedtuple

from django.apps import apps
from django.db import connection
from django.db.models import (
    FilteredRelation,
    Subquery,
    Prefetch,
    Sum,
    Count,
    Q,
    F,
    IntegerField,
    DecimalField,
    Case,
    Value,
    When,
    Avg,
    Func,
    CharField,
    BooleanField,
    TimeField,
)
from django.db.models.functions import (
    Coalesce,
    TruncMonth,
    TruncDate,
    TruncDay,
    TruncHour,
    TruncMinute,
    ExtractWeekDay,
)
from django.db.models.expressions import RawSQL

from datetime import timedelta
from sterlingpos.core.models import (
    get_current_tenant,
    SterlingTenantQuerySet,
    SterlingTenantManager,
    SterlingSafeDeleteAllManager,
)
from sterlingpos.stocks.raw_query import (
    stock_card_query,
    stock_card_grouper,
    stock_card_outlet_filter,
)

from sterlingpos.stocks.query import (
    names_to_path,
    add_filtered_relation, 
    join_to,
)


def _namedtuplefetchall(cursor, fieldnames=None):
    "Return all rows from a cursor as a namedtuple"
    if not fieldnames:
        fieldnames = [col[0] for col in desc]
    desc = cursor.description
    nt_result = namedtuple('Result', fieldnames)
    return [nt_result(*row) for row in cursor.fetchall()]


class StockQueryset(SterlingTenantQuerySet):

    def stock_card(
            self, 
            start_date=None, 
            end_date=None,
            outlet=None,
            **kwargs):

        from sterlingpos.stocks.models import HourlyStockTransaction

        class NoGroupingRawSQL(RawSQL):
            def get_group_by_cols(self):
                return []

        outlet_qs = kwargs.get("outlet_qs", None)
        if outlet_qs:
            self = self.filter(outlet_id__in=outlet_qs)

        account = get_current_tenant()
        if account:
            self = self.filter(account_id=account.pk)

        if outlet:
            self = self.filter(outlet_id=outlet.pk)

        base_qs = HourlyStockTransaction.objects.stock_card(
            start_date, end_date, outlet, **kwargs
        )

        self = join_to(
            self.model,
            base_qs, 'id', 'stock_id',
            self, 'stock_card'
        )
        
        self = self.values('product')
        self.query.group_by = ('product',)
        self = self.annotate(

            counting=Count("product"),

            start_stock=NoGroupingRawSQL(
                'COALESCE(SUM("stock_card"."start_stock"), 0)',
                params=[]
            ),
            last_stock=NoGroupingRawSQL(
                'COALESCE(SUM("stock_card"."last_stock"), 0)',
                params=[]
            ),

            stock_in=NoGroupingRawSQL(
                'COALESCE(SUM("stock_card"."stock_in"), 0)',
                params=[]
            ),
            stock_out=NoGroupingRawSQL(
                'COALESCE(SUM("stock_card"."stock_out"), 0)',
                params=[]
            ),
           
            stock_productions=NoGroupingRawSQL(
                'COALESCE(SUM("stock_card"."stock_productions"), 0)',
                params=[]
            ),
            stock_sales=NoGroupingRawSQL(
                'COALESCE(SUM("stock_card"."stock_sales"), 0)',
                params=[]
            ),
        )

        self = self.values(
            "product",
            "product__sku",
            "product__catalogue__name",
            "product__name",
            "product__category__name",
            "product__uom__unit",
            "start_stock",
            "stock_in",
            "stock_out",
            "stock_productions",
            "stock_sales",
            "last_stock",
        )
        return self

    def stock_card_subquery(
            self,
            account,
            start_date=None, 
            end_date=None,
            outlet=None,
            **kwargs):

        fieldnames = kwargs.get("fieldnames", None)

        params={
            "account_id": account.pk,
            "start_date": start_date,
            "end_date": end_date,
            "outlet": outlet.pk if outlet else None,
        }

        with connection.cursor() as cursor:
            cursor.execute(stock_card_query, params)
            row = _namedtuplefetchall(cursor, fieldnames)
            return row


class StockManager(
    StockQueryset.as_manager().__class__, SterlingSafeDeleteAllManager
):

    def create_for_product(self, product, *args, **kwargs):
        stock = self.create(product=product, account=product.account, *args, **kwargs)
        return stock


class StockCardQueryset(SterlingTenantQuerySet):

    def stock_card(self, start_date=None, end_date=None, outlet=None, **kwargs):

        basic_filter = Q(pk__isnull=False)
        basic_filter &= Q(is_manage_stock=True)

        initial_stock_filter = basic_filter
        last_stock_filter = basic_filter
        outlet_qs = kwargs.get("outlet_qs", None)

        if outlet_qs:
            basic_filter &= Q(stocks__outlet__in=outlet_qs)
            initial_stock_filter &= Q(stocks__outlet__in=outlet_qs)
            last_stock_filter &= Q(stocks__outlet__in=outlet_qs)

        if start_date:
            initial_stock_filter &= Q(filtered_transaction__created__lt=start_date)

        if end_date:
            last_stock_filter &= Q(filtered_transaction__created__lte=end_date)

        filtered_transaction_filter = last_stock_filter
        account = get_current_tenant()
        if account:
            filtered_transaction_filter &= Q(
                filtered_transaction__account=account)

        if start_date and end_date:
            inbetween_filter = Q(
                filtered_transaction__created__range=[start_date, end_date]
            )

        if outlet:
            basic_filter &= Q(stocks__outlet=outlet)
            initial_stock_filter &= Q(stocks__outlet=outlet)
            last_stock_filter &= Q(stocks__outlet=outlet)
            filtered_transaction_filter &= Q(stocks__outlet=outlet)

        filtered_relation = FilteredRelation(
            "stocks__transactions",
            condition=filtered_transaction_filter,
        )

        self.query.names_to_path = types.MethodType(names_to_path, self.query)
        self.query._filtered_relations["filtered_transaction"] = filtered_relation

        self = self.annotate(
            start_stock=Coalesce(
                Sum(
                    "filtered_transaction__quantity",
                    filter=(initial_stock_filter)
                    & Q(filtered_transaction__deleted__isnull=True),
                ),
                Value(0),
                output_field=DecimalField(),
            ),
            last_stock=Coalesce(
                Sum(
                    "filtered_transaction__quantity",
                    filter=(last_stock_filter)
                    & Q(filtered_transaction__deleted__isnull=True),
                ),
                Value(0),
                output_field=DecimalField(),
            ),

            stock_in=Coalesce(
                Sum(
                    "filtered_transaction__quantity",
                    filter=(
                        inbetween_filter
                        & Q(filtered_transaction__quantity__gte=0)
                        & Q(filtered_transaction__deleted__isnull=True)
                    ),
                ),
                Value(0),
            ),
            stock_out=Coalesce(
                Sum(
                    "filtered_transaction__quantity",
                    filter=(
                        inbetween_filter
                        & Q(filtered_transaction__quantity__lt=0)
                        & ~Q(
                            filtered_transaction__transaction_type__in=[
                                "transaction",
                                "productions",
                            ]
                        )
                        & Q(filtered_transaction__deleted__isnull=True)
                    ),
                ),
                Value(0),
            ),
           
            stock_productions=Coalesce(
                Sum(
                    "filtered_transaction__quantity",
                    filter=(
                        inbetween_filter
                        & Q(filtered_transaction__quantity__lt=0)
                        & Q(filtered_transaction__transaction_type="productions")
                        & Q(filtered_transaction__deleted__isnull=True)
                        & Q(
                            filtered_transaction__reference__0__fields__automatic_production=False
                        )
                    ),
                ),
                Value(0),
            ),
            stock_sales=Coalesce(
                Sum(
                    "filtered_transaction__quantity",
                    filter=(
                        inbetween_filter
                        & Q(filtered_transaction__quantity__lt=0)
                        & Q(filtered_transaction__deleted__isnull=True)
                        & (
                            (Q(filtered_transaction__transaction_type="transaction"))
                            | (
                                Q(filtered_transaction__transaction_type="productions")
                                & Q(
                                    filtered_transaction__reference__0__fields__automatic_production=True
                                )
                            )
                        )
                    ),
                ),
                Value(0),
            ),
        )

        return self

    def stock_card_v2(
            self, start_date=None, end_date=None, outlet=None, **kwargs):

        basic_filter = Q(pk__isnull=False)
        basic_filter &= Q(is_manage_stock=True)

        initial_stock_filter = basic_filter
        last_stock_filter = basic_filter
        last_stock_filter_2 = Q(pk__isnull=False)

        outlet_qs = kwargs.get("outlet_qs", None)
        if outlet_qs:
            basic_filter &= Q(stocks__outlet__in=outlet_qs)
            initial_stock_filter &= Q(stocks__outlet__in=outlet_qs)
            last_stock_filter &= Q(stocks__outlet__in=outlet_qs)
            last_stock_filter_2 &= Q(stocks__outlet__in=outlet_qs)

        if start_date:
            initial_stock_filter &= Q(filtered_transaction_2__aggregated_time__lt=start_date)

        if end_date:
            last_stock_filter &= Q(filtered_transaction__created__lte=end_date)
            last_stock_filter_2 &= Q(filtered_transaction_2__aggregated_time__lte=end_date)

        filtered_transaction_filter = last_stock_filter
        filtered_transaction_filter_2 = last_stock_filter_2

        account = get_current_tenant()
        if account:
            filtered_transaction_filter &= Q(
                filtered_transaction__account=account)
            filtered_transaction_filter_2 &= Q(
                filtered_transaction_2__account=account)

        if start_date and end_date:
            inbetween_filter = Q(
                filtered_transaction__created__gte=start_date,
                filtered_transaction__created__lte=end_date
            )
            inbetween_filter_2 = Q(
                filtered_transaction_2__aggregated_time__gte=start_date,
                filtered_transaction_2__aggregated_time__lte=end_date
            )

        if outlet:
            basic_filter &= Q(stocks__outlet=outlet)
            initial_stock_filter &= Q(stocks__outlet=outlet)
            last_stock_filter &= Q(stocks__outlet=outlet)
            filtered_transaction_filter &= Q(stocks__outlet=outlet)
            filtered_transaction_filter_2 &= Q(stocks__outlet=outlet)

        filtered_relation = FilteredRelation(
            "stocks__transactions",
            condition=filtered_transaction_filter,
        )
        filtered_relation_2 = FilteredRelation(
            "stocks__hourly_transactions",
            condition=filtered_transaction_filter_2,
        )

        self.query.names_to_path = types.MethodType(names_to_path, self.query)
        self.query._filtered_relations["filtered_transaction"] = filtered_relation
        self.query._filtered_relations["filtered_transaction_2"] = filtered_relation_2
        self.query.table_map.update({
            "stocks_hourlystocktransaction": [
                "stocks_hourlystocktransaction"],
            "stocks_stocktransaction": [
                "stocks_stocktransaction"],
        })
        self = self.annotate(
            start_stock=Coalesce(
                Sum(
                    "filtered_transaction_2__quantity",
                    filter=(initial_stock_filter)
                    & Q(filtered_transaction_2__deleted__isnull=True),
                ),
                Value(0),
                output_field=DecimalField(),
            ),
            last_stock=Coalesce(
                Sum(
                    "filtered_transaction_2__quantity",
                    filter=(last_stock_filter_2)
                    & Q(filtered_transaction_2__deleted__isnull=True),
                ),
                Value(0),
                output_field=DecimalField(),
            ),

            stock_in=Coalesce(
                Sum(
                    "filtered_transaction_2__quantity",
                    filter=(
                        inbetween_filter_2
                        & Q(filtered_transaction_2__quantity__gte=0)
                        & Q(filtered_transaction_2__deleted__isnull=True)
                    ),
                ),
                Value(0),
            ),
            stock_out=Coalesce(
                Sum(
                    "filtered_transaction_2__quantity",
                    filter=(
                        inbetween_filter_2
                        & Q(filtered_transaction_2__quantity__lt=0)
                        & ~Q(
                            filtered_transaction_2__transaction_type__in=[
                                "transaction",
                                "productions",
                            ]
                        )
                        & Q(filtered_transaction_2__deleted__isnull=True)
                    ),
                ),
                Value(0),
            ),
           
            stock_productions=Coalesce(
                Sum(
                    "filtered_transaction_2__quantity",
                    filter=(
                        inbetween_filter_2
                        & Q(filtered_transaction_2__quantity__lt=0)
                        & Q(filtered_transaction_2__transaction_type="productions")
                        & Q(filtered_transaction_2__deleted__isnull=True)
                        & Q(
                            filtered_transaction_2__auto_production=False
                        )
                    ),
                ),
                Value(0),
                Value(0),
                output_field=DecimalField(),
            ),
            stock_sales=Coalesce(
                Sum(
                    "filtered_transaction_2__quantity",
                    filter=(
                        inbetween_filter_2
                        & Q(filtered_transaction_2__quantity__lt=0)
                        & Q(filtered_transaction_2__deleted__isnull=True)
                        & (
                            (Q(filtered_transaction_2__transaction_type="transaction"))
                            | (
                                Q(filtered_transaction_2__transaction_type="productions")
                                & Q(
                                    filtered_transaction_2__auto_production=True
                                )
                            )
                        )
                    ),
                ),
                # Value(0),
                Value(0),
                output_field=DecimalField(),
            ),
        )

        return self


class StockCardTenantManager(
    StockCardQueryset.as_manager().__class__, SterlingSafeDeleteAllManager
):

    def stock_card_subquery(
            self,
            account,
            start_date=None, 
            end_date=None,
            outlet=None,
            **kwargs):

        outlet_ids = kwargs.get("outlet_ids", None)

        stock_card_final_query = [
            stock_card_query,
            stock_card_grouper,
        ]

        params={
            "account_id": account.pk,
            "start_date": start_date,
            "end_date": end_date,
        }

        if outlet:
            outlet_ids = [outlet.pk]

        if outlet_ids:
            params.update({
                "outlet_ids": tuple(set(outlet_ids))
            })
            stock_card_final_query = [
                stock_card_query,
                stock_card_outlet_filter,
                stock_card_grouper,
            ]

        qs = self.raw(
            "\n".join(stock_card_final_query), params)

        return qs


class StockTransactionManager(SterlingTenantManager):
    
    # TODO: Remove this once we update django-multitenancy
    def get_queryset(self):
        queryset = super().get_queryset()
        current_tenant = get_current_tenant()
        if current_tenant:
            return queryset.filter(account=current_tenant)
        return queryset

    def get_summary_qs(
            self, start_date, end_date, outlets=None):
        qs = self.get_queryset()
        qs = qs.filter(
            created__gte=start_date,
            created__lte=end_date,
        )

        if outlets:
            qs = qs.filter(outlet__in=outlets)

        return qs

    def get_transaction_summary_grouped_by_time(
            self, start_date, end_date, trunc_by, outlets=None):
        qs = self.get_summary_qs(
            start_date, end_date, outlets
        ).annotate(
            auto_production=Case(
                When(
                    reference__0__fields__automatic_production=True,
                    then=True,
                ),
                default=False,
                output_field=BooleanField()
            ),
            positive_quantity=Case(
                When(
                    quantity__gte=0,
                    then=True,
                ),
                default=False,
                output_field=BooleanField()
            )
        )

        time_field = "created"
        if trunc_by == "month":
            qs = qs.annotate(time_unit=TruncMonth(time_field))
        elif trunc_by == "date":
            qs = qs.annotate(time_unit=TruncDate(time_field))
        elif trunc_by == "hour":
            qs = qs.annotate(time_unit=TruncHour(time_field))
        elif trunc_by == "minute":
            qs = qs.annotate(time_unit=TruncMinute(time_field))
        else:
            raise ValueError("Unsupported trunc segment")

        qs = (
            qs.values(
                "stock",
                "outlet",
                "unit",
                "transaction_type",
                "auto_production",
                "positive_quantity",
                "time_unit",
            ).annotate(
                count=Coalesce(Count("created"), 0),
                quantity_sum=Coalesce(
                    Sum("quantity", output_field=DecimalField()), 0),
            ).order_by("time_unit")
        )

        return qs


class HourlyStockTransactionManager(SterlingTenantManager):

    # TODO: Remove this once we update django-multitenancy
    def get_queryset(self):
        queryset = super().get_queryset()
        current_tenant = get_current_tenant()
        if current_tenant:
            return queryset.filter(account=current_tenant)
        return queryset

    def stock_card(
            self,
            start_date=None,
            end_date=None,
            outlet=None,
            **kwargs):

        basic_filter = Q(pk__isnull=False)
        initial_stock_filter = basic_filter
        last_stock_filter = basic_filter

        outlet_qs = kwargs.get("outlet_qs", None)
        if outlet_qs:
            self = self.filter(outlet_id__in=outlet_qs)

        account = get_current_tenant()
        if account:
            self = self.filter(account_id=account.pk)

        if outlet:
            self = self.filter(outlet_id=outlet.pk)

        if start_date:
            initial_stock_filter &= Q(
                aggregated_time__lt=start_date)

        if end_date:
            last_stock_filter &= Q(
                aggregated_time__lte=end_date)

            self = self.filter(
                aggregated_time__lte=end_date)


        if start_date and end_date:
            inbetween_filter = Q(
                aggregated_time__gte=start_date,
                aggregated_time__lte=end_date
            )
        
        self = self.values('stock').annotate(
            start_stock=Coalesce(
                Sum(
                    "quantity",
                    filter=(initial_stock_filter)
                    & Q(deleted__isnull=True),
                ),
                Value(0),
                output_field=DecimalField(),
            ),
            last_stock=Coalesce(
                Sum(
                    "quantity",
                    filter=(last_stock_filter)
                    & Q(deleted__isnull=True),
                ),
                Value(0),
                output_field=DecimalField(),
            ),

            stock_in=Coalesce(
                Sum(
                    "quantity",
                    filter=(
                        inbetween_filter
                        & Q(quantity__gte=0)
                        & Q(deleted__isnull=True)
                    ),
                ),
                Value(0),
            ),
            stock_out=Coalesce(
                Sum(
                    "quantity",
                    filter=(
                        inbetween_filter
                        & Q(quantity__lt=0)
                        & ~Q(
                            transaction_type__in=[
                                "transaction",
                                "productions",
                            ]
                        )
                        & Q(deleted__isnull=True)
                    ),
                ),
                Value(0),
            ),

            stock_productions=Coalesce(
                Sum(
                    "quantity",
                    filter=(
                        inbetween_filter
                        & Q(quantity__lt=0)
                        & Q(transaction_type="productions")
                        & Q(deleted__isnull=True)
                        & Q(auto_production=False)
                    ),
                ),
                Value(0),
                output_field=DecimalField(),
            ),
            stock_sales=Coalesce(
                Sum(
                    "quantity",
                    filter=(
                        inbetween_filter
                        & Q(quantity__lt=0)
                        & Q(deleted__isnull=True)
                        & (
                            (Q(transaction_type="transaction"))
                            | (
                                Q(transaction_type="productions")
                                & Q(auto_production=True)
                            )
                        )
                    ),
                ),
                Value(0),
                output_field=DecimalField(),
            ),
        )

        return self
