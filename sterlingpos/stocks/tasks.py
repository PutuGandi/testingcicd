import logging
import types
import gc
import datetime
import time
import hmac
import hashlib

from contextlib import contextmanager

from django.template.loader import render_to_string
from django.utils.dateparse import parse_datetime
from django.conf import settings
from django.core.cache import cache
from django.apps import apps
from django.db import connection
from django.db.models import (
    Q,
    Sum,
    F,
    Sum,
    FilteredRelation,
)
from weasyprint import HTML, CSS
from decimal import Decimal

from celery import shared_task
from sterlingpos.taskapp.celery import app
from sterlingpos.accounts.models import Account
from sterlingpos.catalogue.utils import uom_conversion
from sterlingpos.stocks.query import names_to_path
from sterlingpos.catalogue.utils import uom_conversion, uom_cost_conversion

from sterlingpos.core.models import (
    get_current_tenant,
    set_current_tenant,
)

logger = logging.getLogger(__name__)

LOCK_EXPIRE = 60 * 3


def __convert_datetime(date_time):
    
    if not isinstance(date_time, datetime.datetime):
        try:
            date_time = parse_datetime(date_time)
        except:
            date_time = datetime.datetime.now().date()
    return date_time


@contextmanager
def cache_lock(lock_id, oid):
    timeout_at = time.monotonic() + LOCK_EXPIRE - 3
    # cache.add fails if the key already exists
    status = cache.add(
        lock_id, oid, LOCK_EXPIRE)
    try:
        yield status
    finally:
        if time.monotonic() < timeout_at and status:
            cache.delete(lock_id)


def create_missing_batch(product):
    missing_name = "Missing Batch"
    missing_batch, _ = product.batches.get_or_create(
        is_missing_batch=True, batch_id=missing_name
    )
    return missing_batch


def stock_count_update_missing_batch(batch_outlet, balance, unit):
    batch_outlet.set_batch_balance(
        balance,
        unit=unit,
        source_object=None,
    )


@shared_task(bind=True, max_retries=5)
def set_counting_to_stocks(self, pk, account_pk, user_pk=None):
    try:
        account = Account.objects.get(pk=account_pk)
        set_current_tenant(account)
        stock_count = account.stockcount_set.get(pk=pk)
        for counted in stock_count.counted_stock.all():
            converted_counted = uom_conversion(
                counted.unit, counted.stock.product.uom, counted.counted
            )
            if (
                counted.stock_count not in ["canceled", "completed"]
                and counted.quantity != counted.counted
            ):
                if user_pk:
                    user = account.user_set.get(pk=user_pk)
                    counted.stock.set_balance(
                        converted_counted,
                        unit=counted.stock.product.uom,
                        source_object=stock_count,
                        transaction_type="stock_count",
                        user=user,
                    )
                else:
                    counted.stock.set_balance(
                        converted_counted,
                        unit=counted.stock.product.uom,
                        source_object=stock_count,
                        transaction_type="stock_count",
                    )

                deleted_batch = account.batch_set.filter(
                    product=counted.stock.product,
                    batch_items__outlet=stock_count.outlet,
                )
                for deleted in deleted_batch:
                    try:
                        deleted.batch_items.get(outlet=stock_count.outlet).delete()
                    except Exception:
                        pass
                    if deleted.balance == 0:
                        deleted.delete()

                missing_batch = None
                if converted_counted > 0:
                    missing_batch = (
                        missing_batch
                        if missing_batch
                        else create_missing_batch(counted.stock.product)
                    )
                    batch_outlet, _ = missing_batch.batch_items.get_or_create(
                        outlet=stock_count.outlet
                    )
                    stock_count_update_missing_batch(
                        batch_outlet, converted_counted, counted.stock.product.uom
                    )
        set_current_tenant(None)
    except Exception as e:
        print(e)
        logged_data = "Unable to process StockCount {} : {}".format(
            get_current_tenant(), e
        )
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


def update_stock_tail_sum_for(account, stock):
    logger.info(
        f"Populating tail sum StockTransaction for Account {account} stock {stock}"
    )
    StockTransaction = apps.get_model("stocks.StockTransaction")

    qs = StockTransaction.objects.select_related("stock__product__uom").filter(
        Q(stock=stock) & Q(tail_sum__isnull=True)
    )

    filtered_relation = FilteredRelation(
        "stock__transactions",
        condition=Q(stock=stock, stock__transactions__created__lt=F("created")),
    )
    qs.query.names_to_path = types.MethodType(names_to_path, qs.query)
    qs.query._filtered_relations["post_transaction"] = filtered_relation

    qs = qs.annotate(quantity_acc=Sum("post_transaction__quantity"))

    for instance in qs:
        StockTransaction.objects.filter(pk=instance.pk).update(
            tail_sum=instance.quantity_acc
        )


@app.task(bind=True)
def celery_update_stock_tail_sum_for(self, account_pk, stock_pk):
    account = Account.objects.get(pk=account_pk)
    set_current_tenant(account)
    stock = account.stock_set.get(pk=stock_pk)
    update_stock_tail_sum_for(account, stock)
    set_current_tenant(None)


@app.task(bind=True)
def update_stock_transaction_tail_sum(self):
    for account in Account.objects.all():
        logger.info(f"Populating tail sum StockTransaction for Account {account}")
        set_current_tenant(account)

        for stock in account.stock_set.all():
            celery_update_stock_tail_sum_for.delay(account.pk, stock.pk)

        set_current_tenant(None)
        logger.info(
            f"Finished populating tail sum StockTransaction for Account {account}"
        )


@app.task(bind=True)
def populate_stock_transaction_outlet_field(self):
    Stock = apps.get_model("stocks", "Stock")

    qs = Stock.objects.filter(transactions__outlet__isnull=True).distinct()
    count = qs.count()
    split_by = count // 4
    ranges = [
        (split_by * i, split_by * (i + 1)) if i < 4 else (i, count) for i in range(0, 4)
    ]
    for limit, offset in ranges:
        populate_stock_transaction_outlet_field_by_stock_pk_range.delay(limit, offset)
    logger.info(f"Populating outlet for StockTransaction Completed")


@app.task(bind=True)
def populate_stock_transaction_outlet_field_by_stock_pk_range(self, limit, offset):
    Stock = apps.get_model("stocks", "Stock")

    qs = Stock.objects.filter(transactions__outlet__isnull=True).distinct()[
        limit:offset
    ]

    for i, obj in enumerate(qs):
        set_current_tenant(obj.account)
        logger.info(f"Populating outlet for StockTransaction for {obj} {i+1}/{offset}")
        obj.transactions.update(outlet=obj.outlet)
        set_current_tenant(None)

    logger.info(
        f"Populating outlet for StockTransaction by ranges {limit} to {offset} Completed"
    )


@shared_task(bind=True, max_retries=5)
def set_status_received_in_received_order(self):
    try:
        for account in Account.objects.all():
            set_current_tenant(account)
            account.purchaseorderreceive_set.filter(
                purchase_order__status__in=["partial", "received", "closed"]
            ).update(received=True)
            set_current_tenant(None)

        logger.info(f"Set received status in received order Completed")
    except Exception as e:
        logged_data = "Unable to set received status {} : {}".format(
            get_current_tenant(), e
        )
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


def populate_stock_transaction_aggregated_for_account(
        account, start_date, end_date, stock_pk=None):

    Stock = apps.get_model("stocks", "Stock")
    set_current_tenant(account)

    start_date = __convert_datetime(start_date)
    end_date = __convert_datetime(end_date)

    logger.info(
        f"Starting populating HourlyStockTransaction for Account {account}.{account.pk} {start_date}|{end_date}"
    )

    try:
        stock = Stock.objects.get(pk=stock_pk)
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT public.do_hourly_stock_transaction_aggregation_for_date_range_and_stock_id(
                    %s, %s, %s
                )""",
                [
                    stock.pk,
                    start_date,
                    end_date,
                ]
            )
    except:
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT public.do_hourly_stock_transaction_aggregation_for_date_range_and_account_id(
                    %s, %s, %s
                )""",
                [
                    account.pk,
                    start_date,
                    end_date,
                ]
            )

    logger.info(
        f"Finished populating HourlyStockTransaction for Account {account}.{account.pk} {start_date}|{end_date}"
    )
    set_current_tenant(None)


@app.task(bind=True, rate_limit="100/m", max_retries=5)
def populate_stock_transaction_aggregated_for_account_task(
        self, account_pk, start_date, end_date, stock_pk=None):
    account = Account.objects.get(pk=account_pk)
    populate_stock_transaction_aggregated_for_account(
        account, start_date, end_date, stock_pk
    )


@app.task(bind=True)
def populate_stock_transaction_aggregated(self, start_date, end_date):

    start_date = __convert_datetime(start_date)
    end_date = __convert_datetime(end_date)

    with connection.cursor() as cursor:
        cursor.execute("""
            SELECT public.do_hourly_stock_transaction_aggregation_for_date_range(
                %s, %s
            )""",
            [
                start_date,
                end_date,
            ]
        )
    logger.info(
        f"Finished populating HourlyStockTransaction for all Accounts {start_date}|{end_date}"
    )


@shared_task(bind=True, max_retries=5)
def daily_stock_transaction_aggregation(self):
    try:
        start_date = datetime.date.today() - datetime.timedelta(days=1)
        end_date = datetime.date.today()

        populate_stock_transaction_aggregated.delay(start_date, end_date)
    except Exception as e:
        logged_data = f"Unable to start task status {e}"
        logger.warning(logged_data)
        self.retry(countdown=5 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def hourly_stock_transaction_aggregation(self):
    try:
        now = datetime.datetime.today().replace(
            minute=0, second=0, microsecond=0)
        start_date = now - datetime.timedelta(hours=2)
        end_date = now
        if now.day == start_date.day:
            populate_stock_transaction_aggregated.delay(
                start_date, end_date
            )
        else:
            logger.info("Passing hourly stock aggregation.")
    except Exception as e:
        logged_data = f"Unable to start task status {e}"
        logger.warning(logged_data)
        self.retry(countdown=5 ** self.request.retries)


def calculate_avarage_cost(self, product_id, quantity, unit_id, cost, account_id):
    try:
        account = Account.objects.get(pk=account_id)
        set_current_tenant(account)
        product = account.product_set.get(pk=product_id)
        unit = account.uom_set.get(pk=unit_id)
        current_balance = 0
        for stock in product.stocks.all():
            if stock.get_last_tail_sum:
                current_balance += stock.get_last_tail_sum

        cost_by_uom = uom_cost_conversion(
            unit,
            product.uom,
            Decimal(quantity),
            Decimal(cost),
        )
        converted_qty = uom_conversion(unit, product.uom, Decimal(quantity))
        total_balance = current_balance + converted_qty
        if total_balance > 0:
            avg_cost = (
                (current_balance * product.cost.amount) + (converted_qty * cost_by_uom)
            ) / (total_balance)
            product.moving_average_cost.amount = avg_cost
            product.save()
        else:
            product.moving_average_cost.amount = cost_by_uom
            product.save()
        set_current_tenant(None)
    except Exception as e:
        logged_data = f"Unable to calculate avg cost {e}"
        logger.warning(logged_data)
        self.retry(countdown=5 ** self.request.retries)


@app.task(bind=True, rate_limit="100/m")
def locking_stock_transaction_aggregate(
        self, account_pk, start_date, end_date, stock_pk):
    # The cache key consists of the task name and the MD5 digest
    # of the feed URL.
    feed_hexdigest = hmac.new(
        b'stock-transaction',
        f'{account_pk}|{stock_pk}'.encode('utf-8'),
        hashlib.sha256
    ).hexdigest()
    lock_id = '{0}-lock-{1}'.format(
        self.name, feed_hexdigest)

    logger.info(
        f"Aggregating hourly stock transaction: {stock_pk}")

    with cache_lock(lock_id, self.app.oid) as acquired:
        if acquired:
            account = Account.objects.get(
                pk=account_pk)
            return populate_stock_transaction_aggregated_for_account(
                account,
                start_date,
                end_date,
                stock_pk,
            )
    logger.info(
        f"Stock aggregation for {account_pk}|{stock_pk} is already being processed by another worker")


@shared_task(bind=True, rate_limit="50/m")
def refresh_daily_stock_balance_materialized_view_task(self):
    logger.info(
        f"Refreshing daily stock balance materialized view.")
    with connection.cursor() as cursor:
        cursor.execute("""
            REFRESH MATERIALIZED VIEW CONCURRENTLY stocks_dailystockbalancemv WITH DATA;
        """
        )
    logger.info(
        f"Finished refreshing daily stock balance materialized view.")
