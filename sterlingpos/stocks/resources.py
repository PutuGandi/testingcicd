import datetime

from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import MultipleObjectsReturned

from import_export import resources
from import_export.fields import Field
from decimal import Decimal, InvalidOperation

from sterlingpos.core.import_export import (
    RequiredCharWidget,
    CustomDecimalWidget,
)
from sterlingpos.core.models import get_current_tenant
from sterlingpos.stocks.models import (
    Supplier,
    SupplierProductDetail,
    StockTransfer,
    StockTransferDetail,
    StockCount,
    StockCountDetail,
)
from sterlingpos.catalogue.models import Product, UOM
from sterlingpos.outlet.models import Outlet


class SupplierResource(resources.ModelResource):
    code = Field(column_name="code", attribute="code", widget=RequiredCharWidget())
    name = Field(column_name="name", attribute="name", widget=RequiredCharWidget())
    sku = Field(column_name="sku", widget=RequiredCharWidget())
    product = Field(column_name="product", widget=RequiredCharWidget())
    quantity = Field(
        column_name="quantity",
        default=0,
        widget=CustomDecimalWidget(),
    )
    unit = Field(column_name="unit", widget=RequiredCharWidget())
    price = Field(
        column_name="price",
        default=0,
        widget=CustomDecimalWidget(),
    )

    class Meta:
        model = Supplier
        import_id_fields = ("code",)
        fields = ("code", "name")
        exclude = ("account",)
        clean_model_instance = True

    def init_instance(self, row=None):
        instance = super().init_instance(row)
        instance.account = get_current_tenant()
        instance.name = row["name"]
        instance.master_product_data = True
        instance.save()

        data_product = {
            "sku": row["sku"],
        }
        try:
            product = Product.objects.get(**data_product)
        except Product.DoesNotExist:
            raise ValueError(_(f"Produk dengan SKU: {row['sku']} tidak ada."))

        if not product.is_manage_stock:
            raise ValueError(
                _(f"Produk dengan SKU:{row['sku']} tidak menggunakan manajemen stok")
            )

        try:
            unit = UOM.objects.get(unit=row["unit"])
        except UOM.DoesNotExist:
            raise ValueError("UOM: {} does not exist".format(row["unit"]))

        if product.uom.category != unit.category:
            raise ValueError(
                _(
                    "Satuan: {} ({}) dan satuan produk: {} ({}) berbeda kategori".format(
                        unit.unit,
                        unit.category,
                        product.uom.unit,
                        product.uom.category,
                    )
                )
            )

        data_master_list = {
            "product": product,
            "quantity": row["quantity"],
            "unit": unit,
            "cost": row["price"],
            "supplier": instance,
        }
        SupplierProductDetail.objects.get_or_create(**data_master_list)
        return instance

    def get_instance(self, instance_loader, row):
        try:
            instance = super().get_instance(instance_loader, row)
            if instance:
                instance.name = row["name"]
                instance.master_product_data = True
                instance.save()

                data_product = {
                    "sku": row["sku"],
                }
                try:
                    product = Product.objects.get(**data_product)
                except Product.DoesNotExist:
                    raise ValueError(_(f"Produk dengan SKU: {row['sku']} tidak ada."))

                if not product.is_manage_stock:
                    raise ValueError(
                        _(
                            f"Produk dengan SKU:{row['sku']} tidak menggunakan manajemen stok"
                        )
                    )

                try:
                    unit = UOM.objects.get(unit=row["unit"])
                except UOM.DoesNotExist:
                    raise ValueError("UOM: {} does not exist".format(row["unit"]))

                if product.uom.category != unit.category:
                    raise ValueError(
                        _(
                            "Satuan: {} ({}) dan satuan produk: {} ({}) berbeda kategori".format(
                                unit.unit,
                                unit.category,
                                product.uom.unit,
                                product.uom.category,
                            )
                        )
                    )
                data_master_list = {
                    "product": product,
                    "quantity": row["quantity"],
                    "unit": unit,
                    "cost": row["price"],
                }
                try:
                    detail = instance.supplier_detail.get(product=product)
                    detail.quantity = row["quantity"]
                    detail.unit = unit
                    detail.cost = row["price"]
                    detail.save()
                except SupplierProductDetail.DoesNotExist:
                    instance.supplier_detail.get_or_create(**data_master_list)
                return instance
        except MultipleObjectsReturned:
            raise ValueError(
                _(
                    "There are more than 2 existing Supplier Product List under this SKU."
                )
            )


class StockTransferResource(resources.ModelResource):
    date_of_transfer = Field(
        column_name="date_of_transfer",
        widget=RequiredCharWidget(),
    )
    source_outlet_code = Field(
        column_name="source_outlet_code",
        attribute="source__outlet_code",
        widget=RequiredCharWidget(),
    )
    destination_outlet_code = Field(
        column_name="destination_outlet_code",
        attribute="destination__outlet_code",
        widget=RequiredCharWidget(),
    )
    sku = Field(column_name="sku", widget=RequiredCharWidget())
    product = Field(column_name="product", widget=RequiredCharWidget())
    quantity = Field(
        column_name="quantity",
        default=0,
        widget=CustomDecimalWidget(),
    )
    unit = Field(column_name="unit", widget=RequiredCharWidget())

    class Meta:
        model = StockTransfer
        exclude = ("account",)

    def __init__(self, *args, **kwargs):
        super(StockTransferResource, self).__init__(*args, **kwargs)
        self.stock_transfer_instance = []

    def init_instance(self, row=None):
        instance = super().init_instance(row)
        instance.account = get_current_tenant()

        try:
            date_of_transfer = datetime.datetime.strptime(row["date_of_transfer"], "%d/%m/%Y %H:%M")
            instance.date_of_transfer = date_of_transfer
        except ValueError:
            raise ValueError(_("Format tanggal tidak sesuai: DD/MM/YYYY HH:mm"))

        if row["source_outlet_code"] == row["destination_outlet_code"]:
            raise ValueError(_("Outlet Asal dan Outlet Tujuan tidak bisa sama"))

        try:
            instance.source = Outlet.objects.get(outlet_code=row["source_outlet_code"])
        except Outlet.DoesNotExist:
            raise ValueError(
                _(
                    "Outlet Asal dengan kode outlet {} tidak ada".format(
                        row["source_outlet_code"]
                    )
                )
            )
        try:
            instance.destination = Outlet.objects.get(
                outlet_code=row["destination_outlet_code"]
            )
        except Outlet.DoesNotExist:
            raise ValueError(
                _(
                    "Outlet Tujuan dengan kode outlet {} tidak ada".format(
                        row["destination_outlet_code"]
                    )
                )
            )
        instance.save()

        try:
            product = Product.objects.get(sku=row["sku"])
        except Product.DoesNotExist:
            raise ValueError(_("Produk {} tidak ada".format(row["sku"])))

        if not product.is_manage_stock:
            raise ValueError(
                _("Produk: {} tidak menggunakan manajemen stok".format(row["sku"]))
            )

        if not product.is_manage_stock:
            raise ValueError(
                _("Produk: {} tidak menggunakan manajemen stok".format(row["sku"]))
            )

        if not product.uom:
            raise ValueError(_("Produk: {} tidak memiliki satuan".format(row["sku"])))

        try:
            unit = UOM.objects.get(unit=row["unit"])
        except UOM.DoesNotExist:
            raise ValueError("UOM: {} does not exist".format(row["unit_to_consumed"]))

        if product.uom.category != unit.category:
            raise ValueError(
                _(
                    "Satuan: {} ({}) dan satuan produk: {} ({}) berbeda kategori".format(
                        unit.unit,
                        unit.category,
                        product.uom.unit,
                        product.uom.category,
                    )
                )
            )

        try:
            quantity = Decimal(row["quantity"])
        except InvalidOperation:
            raise ValueError(_("Jumlah: {} format salah".format(row["quantity"])))

        if quantity < 0:
            raise ValueError(_("Jumlah: {} tidak bisa negatif".format(row["quantity"])))

        stock_transfer_detail = {
            "stock_source": product.stocks.get(outlet=instance.source),
            "stock_destination": product.stocks.get(outlet=instance.destination),
            "unit": unit,
            "quantity": quantity,
            "stock_transfer": instance,
        }
        if instance.transfered_stock.filter(
            stock_source=stock_transfer_detail["stock_source"]
        ).exists():
            raise ValueError(
                _("Produk {} sudah ada didetail mutasi stok.".format(product))
            )
        instance.transfered_stock.create(**stock_transfer_detail)
        self.stock_transfer_instance.append(instance)
        print("instance", instance)
        return instance

    def get_instance(self, instance_loader, row):
        instance = None
        print(self.stock_transfer_instance)
        for stock_transfer in self.stock_transfer_instance:
            if (
                stock_transfer.source.outlet_code == row["source_outlet_code"]
                and stock_transfer.destination.outlet_code
                == row["destination_outlet_code"]
            ):
                instance = stock_transfer
                try:
                    date_of_transfer = datetime.datetime.strptime(row["date_of_transfer"], "%d/%m/%Y %H:%M")
                    instance.date_of_transfer = date_of_transfer
                    instance.save()
                except ValueError:
                    raise ValueError(_("Format tanggal tidak sesuai: DD/MM/YYYY HH:mm"))

                try:
                    product = Product.objects.get(sku=row["sku"])
                except Product.DoesNotExist:
                    raise ValueError(_("Produk {} tidak ada".format(row["sku"])))

                if not product.is_manage_stock:
                    raise ValueError(
                        _(
                            "Produk: {} tidak menggunakan manajemen stok".format(
                                row["sku"]
                            )
                        )
                    )

                if not product.is_manage_stock:
                    raise ValueError(
                        _(
                            "Produk: {} tidak menggunakan manajemen stok".format(
                                row["sku"]
                            )
                        )
                    )

                if not product.uom:
                    raise ValueError(
                        _("Produk: {} tidak memiliki satuan".format(row["sku"]))
                    )

                try:
                    unit = UOM.objects.get(unit=row["unit"])
                except UOM.DoesNotExist:
                    raise ValueError(
                        "UOM: {} does not exist".format(row["unit_to_consumed"])
                    )

                if product.uom.category != unit.category:
                    raise ValueError(
                        _(
                            "Satuan: {} ({}) dan satuan produk: {} ({}) berbeda kategori".format(
                                unit.unit,
                                unit.category,
                                product.uom.unit,
                                product.uom.category,
                            )
                        )
                    )

                try:
                    quantity = Decimal(row["quantity"])
                except InvalidOperation:
                    raise ValueError(
                        _("Jumlah: {} format salah".format(row["quantity"]))
                    )

                if quantity < 0:
                    raise ValueError(
                        _("Jumlah: {} tidak bisa negatif".format(row["quantity"]))
                    )

                stock_transfer_detail = {
                    "stock_source": product.stocks.get(outlet=stock_transfer.source),
                    "stock_destination": product.stocks.get(
                        outlet=stock_transfer.destination
                    ),
                    "unit": unit,
                    "quantity": quantity,
                }

                try:
                    detail = instance.transfered_stock.get(
                        stock_source=stock_transfer_detail["stock_source"],
                        stock_destination=stock_transfer_detail["stock_destination"],
                    )
                    detail.quantity = stock_transfer_detail["quantity"]
                    detail.unit = stock_transfer_detail["unit"]
                    detail.save()
                except StockTransferDetail.DoesNotExist:
                    instance.transfered_stock.create(**stock_transfer_detail)

                break
        return instance


class StockCountResource(resources.ModelResource):
    outlet_code = Field(
        column_name="outlet_code",
        attribute="outlet__outlet_code",
        widget=RequiredCharWidget(),
    )
    sku = Field(column_name="sku", widget=RequiredCharWidget())
    product = Field(column_name="product", widget=RequiredCharWidget())
    counted = Field(
        column_name="counted",
        default=0,
        widget=CustomDecimalWidget(),
    )
    unit = Field(column_name="unit", widget=RequiredCharWidget())

    class Meta:
        model = StockCount
        exclude = ("account",)

    def __init__(self, *args, **kwargs):
        super(StockCountResource, self).__init__(*args, **kwargs)
        self.stock_count_instance = []

    def init_instance(self, row=None):
        instance = super().init_instance(row)
        instance.account = get_current_tenant()
        try:
            instance.outlet = Outlet.objects.get(outlet_code=row["outlet_code"])
        except Outlet.DoesNotExist:
            raise ValueError(
                _("Outlet dengan kode outlet {} tidak ada.".format(row["outlet_code"]))
            )

        instance.save()

        try:
            product = Product.objects.get(sku=row["sku"])
        except Product.DoesNotExist:
            raise ValueError(_("Produk {} tidak ada".format(row["sku"])))

        if not product.is_manage_stock:
            raise ValueError(
                _("Produk: {} tidak menggunakan manajemen stok".format(row["sku"]))
            )

        if not product.is_manage_stock:
            raise ValueError(
                _("Produk: {} tidak menggunakan manajemen stok".format(row["sku"]))
            )

        if not product.uom:
            raise ValueError(_("Produk: {} tidak memiliki satuan".format(row["sku"])))

        try:
            unit = UOM.objects.get(unit=row["unit"])
        except UOM.DoesNotExist:
            raise ValueError("UOM: {} does not exist".format(row["unit_to_consumed"]))

        if product.uom.category != unit.category:
            raise ValueError(
                _(
                    "Satuan: {} ({}) dan satuan produk: {} ({}) berbeda kategori".format(
                        unit.unit,
                        unit.category,
                        product.uom.unit,
                        product.uom.category,
                    )
                )
            )

        try:
            counted = Decimal(row["counted"])
        except InvalidOperation:
            raise ValueError(_("Jumlah: {} format salah".format(row["counted"])))

        stock = product.stocks.get(outlet=instance.outlet)
        stock_count_detail = {
            "stock": stock,
            "unit": unit,
            "quantity": stock.get_last_tail_sum
            if stock.get_last_tail_sum
            else stock.balance,
            "counted": counted,
            "stock_count": instance,
        }

        if instance.counted_stock.filter(stock=stock).exists():
            raise ValueError(_("Produk {} sudah ada di stok opname.".format(product)))

        instance.counted_stock.create(**stock_count_detail)
        self.stock_count_instance.append(instance)
        print(instance)
        print(self.stock_count_instance)
        return instance

    def get_instance(self, instance_loader, row):
        instance = None
        print(self.stock_count_instance)
        for stock_count in self.stock_count_instance:
            print("ASD", stock_count.outlet.outlet_code)
            if stock_count.outlet.outlet_code == row["outlet_code"]:
                instance = stock_count
                try:
                    product = Product.objects.get(sku=row["sku"])
                except Product.DoesNotExist:
                    raise ValueError(_("Produk {} tidak ada".format(row["sku"])))

                if not product.is_manage_stock:
                    raise ValueError(
                        _(
                            "Produk: {} tidak menggunakan manajemen stok".format(
                                row["sku"]
                            )
                        )
                    )

                if not product.is_manage_stock:
                    raise ValueError(
                        _(
                            "Produk: {} tidak menggunakan manajemen stok".format(
                                row["sku"]
                            )
                        )
                    )

                if not product.uom:
                    raise ValueError(
                        _("Produk: {} tidak memiliki satuan".format(row["sku"]))
                    )

                try:
                    unit = UOM.objects.get(unit=row["unit"])
                except UOM.DoesNotExist:
                    raise ValueError(
                        "UOM: {} does not exist".format(row["unit_to_consumed"])
                    )

                if product.uom.category != unit.category:
                    raise ValueError(
                        _(
                            "Satuan: {} ({}) dan satuan produk: {} ({}) berbeda kategori".format(
                                unit.unit,
                                unit.category,
                                product.uom.unit,
                                product.uom.category,
                            )
                        )
                    )

                try:
                    counted = Decimal(row["counted"])
                except InvalidOperation:
                    raise ValueError(
                        _("Jumlah: {} format salah".format(row["counted"]))
                    )

                stock = product.stocks.get(outlet=instance.outlet)
                stock_count_detail = {
                    "stock": stock,
                    "unit": unit,
                    "quantity": stock.get_last_tail_sum
                    if stock.get_last_tail_sum
                    else stock.balance,
                    "counted": counted,
                    "stock_count": instance,
                }

                try:
                    detail = instance.counted_stock.get(stock=stock)
                    detail.quantity = stock_count_detail["quantity"]
                    detail.unit = stock_count_detail["unit"]
                    detail.counted = stock_count_detail["counted"]
                    detail.save()
                except StockCountDetail.DoesNotExist:
                    instance.counted_stock.create(**stock_count_detail)

                break
        return instance
