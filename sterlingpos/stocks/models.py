import json
from datetime import datetime

from django.conf import settings
from django.db import models
from django.db.models import Sum, Value, Q
from django.db.models.constraints import UniqueConstraint
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from safedelete.models import HARD_DELETE

from django.contrib.postgres.fields import JSONField
from django.core import serializers

from djmoney.models.fields import MoneyField

from model_utils import Choices
from model_utils.models import TimeStampedModel
from decimal import Decimal

from sterlingpos.stocks.managers import (
    StockManager,
    StockCardTenantManager,
    StockTransactionManager,
    HourlyStockTransactionManager,
)
from sterlingpos.accounts.models import Account
from sterlingpos.catalogue.models import Product, UOM
from sterlingpos.catalogue.utils import uom_conversion, uom_cost_conversion
from sterlingpos.stocks.utils import acronym
from sterlingpos.core.models import (
    SterlingTenantModel,
    SterlingTenantForeignKey,
    SterlingTenantOneToOneField,
    set_current_tenant,
    get_current_tenant,
)
from sterlingpos.stocks.tasks import set_counting_to_stocks
from sterlingpos.catalogue.tasks import calculate_avarage_cost


TRANSACTION_TYPE = Choices(
    ("manual_substract", "manual_substract", _("Manually Substracted")),
    ("manual_add", "manual_add", _("Manually Added")),
    ("manual_set", "manual_set", _("Manually Set")),
    ("transfer", "transfer", _("Transfer Procedure")),
    ("transaction", "transaction", _("Sales Transaction")),
    ("purchase_order", "purchase_order", _("Purchase Order")),
    ("stock_adjustment", "stock_adjustment", _("Stock Adjustment")),
    ("stock_count", "stock_count", _("Stock Count")),
    ("productions", "productions", _("Productions")),
)


class MaterializedTableMixins:
    
    def delete(self, **kwargs):
        """
        Materialized tables will be updated through a query,
        activated via celery task 
        """
        pass


class Supplier(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_("Name"), max_length=120)
    contact = models.CharField(_("Contact"), blank=True, max_length=255)
    email = models.CharField(_("Email"), blank=True, max_length=255)
    phone = models.CharField(_("Phone"), blank=True, max_length=255)
    address = models.CharField(_("Address"), blank=True, max_length=255)
    code = models.CharField(_("Supplier Code"), blank=True, max_length=50)
    master_product_data = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Supplier"
        verbose_name_plural = "Suppliers"
        unique_together = (("account", "id"),)

    def __str__(self):
        return self.name

    def create_supplier_detail(self, product, quantity, unit, cost):
        return self.supplier_detail.create(
            product=product,
            quantity=quantity,
            unit=unit,
            cost=cost,
            account=self.account,
        )

    def generate_code(self):
        return "SUPPLIER-{}".format(
            self.__class__.objects.filter(deleted__isnull=True).count() + 1
        )

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = self.generate_code()

        return super().save()


class SupplierProductDetail(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    supplier = SterlingTenantForeignKey(
        "stocks.Supplier", related_name="supplier_detail", on_delete=models.CASCADE
    )
    product = SterlingTenantForeignKey(
        "catalogue.Product", related_name="supplier_detail", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="supplier_detail",
        on_delete=models.CASCADE,
    )
    cost = MoneyField(
        _("Cost"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    class Meta:
        verbose_name = "SupplierProductDetail"
        verbose_name_plural = "SupplierProductDetails"
        unique_together = (("account", "id"),)


class Stock(SterlingTenantModel, TimeStampedModel):
    product = SterlingTenantForeignKey(
        "catalogue.Product", related_name="stocks", on_delete=models.CASCADE
    )
    outlet = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="stocks", on_delete=models.CASCADE
    )
    objects = StockManager()

    class Meta:
        verbose_name = "stock"
        verbose_name_plural = "stocks"
        unique_together = (("account", "id"), ("product", "outlet"))

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "{} - {} [{}]".format(
            self.product.sku, self.product.name, self.outlet.name
        )

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)
        return string

    @property
    def balance(self):
        info = self.transactions.aggregate(
            total=Coalesce(Sum("quantity"), Value(0)))
        return info["total"]

    @property
    def get_last_tail_sum(self):
        if self.transactions.exists():
            transaction_tail_sum = self.transactions.order_by(
                '-created'
            ).values_list(
                'tail_sum', flat=True
            ).first()
            return transaction_tail_sum
        return 0

    def _update_balance(
        self, quantity, unit, source_object, transaction_type, **kwargs
    ):
        if source_object:
            reference = serializers.serialize("json", [source_object])
        else:
            reference = "{}"
        reference = json.loads(reference)

        transaction = StockTransaction.objects.create(
            stock=self,
            outlet=self.outlet,
            quantity=quantity,
            unit=unit,
            transaction_type=transaction_type,
            reference=reference,
            account=self.account,
            **kwargs,
        )

        return transaction.quantity

    def add_balance(
        self,
        quantity,
        unit,
        source_object=None,
        transaction_type=TRANSACTION_TYPE.manual_add,
        **kwargs,
    ):
        return self._update_balance(
            abs(quantity), unit, source_object, transaction_type, **kwargs
        )

    def subtract_balance(
        self,
        quantity,
        unit,
        source_object=None,
        transaction_type=TRANSACTION_TYPE.manual_substract,
        **kwargs,
    ):
        quantity = -1 * abs(quantity)
        return abs(
            self._update_balance(
                quantity, unit, source_object, transaction_type, **kwargs
            )
        )

    def set_balance(
        self,
        quantity,
        unit,
        source_object=None,
        transaction_type=TRANSACTION_TYPE.manual_set,
        **kwargs,
    ):
        quantity = quantity - self.balance
        return abs(
            self._update_balance(
                quantity, unit, source_object, transaction_type, **kwargs
            )
        )


class StockTransaction(SterlingTenantModel, TimeStampedModel):
    TRANSACTION_TYPE = TRANSACTION_TYPE

    stock = SterlingTenantForeignKey(
        Stock, related_name="transactions", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    tail_sum = models.DecimalField(
        _("Tail Sum"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
        blank=True,
        null=True,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="transaction",
        on_delete=models.CASCADE,
    )
    outlet = SterlingTenantForeignKey(
        "outlet.Outlet",
        related_name="stock_transaction",
        null=True,
        on_delete=models.CASCADE,
    )

    transaction_type = models.CharField(
        _("Transaction Type"), max_length=50, choices=TRANSACTION_TYPE, db_index=True
    )

    user = SterlingTenantForeignKey(
        "users.User",
        related_name="stock_transactions",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    reference = JSONField()

    objects = StockTransactionManager()

    class Meta:
        verbose_name = "StockTransaction"
        verbose_name_plural = "StockTransactions"
        unique_together = (("account", "id"),)
        indexes = [
            models.Index(fields=["account", "created"], name="acc_created_idx"),
            models.Index(
                fields=["account", "stock", "created"], name="acc_stock_created_idx"
            ),
            models.Index(
                fields=["account", "outlet", "created"],
                name="st_acc_outlet_created_idx",
            ),
        ]

    def __str__(self):
        return "{} - {}".format(self.stock, self.quantity)

    def save(self, *args, **kwargs):
        if not self.outlet:
            self.outlet = self.stock.outlet
        super().save(*args, **kwargs)


class HourlyStockTransaction(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    TRANSACTION_TYPE = TRANSACTION_TYPE

    stock = SterlingTenantForeignKey(
        Stock, related_name="hourly_transactions", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    tail_sum = models.DecimalField(
        _("Tail Sum"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
        blank=True,
        null=True,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="hourly_transactions",
        on_delete=models.CASCADE,
    )
    outlet = SterlingTenantForeignKey(
        "outlet.Outlet",
        related_name="hourly_stock_transaction",
        null=True,
        on_delete=models.CASCADE,
    )

    transaction_type = models.CharField(
        _("Transaction Type"), max_length=50, choices=TRANSACTION_TYPE, db_index=True
    )
    aggregated_time = models.DateTimeField(null=True)

    auto_production = models.BooleanField(default=False)

    objects = HourlyStockTransactionManager()

    class Meta:
        verbose_name = "HourlyStockTransaction"
        verbose_name_plural = "HourlyStockTransaction"
        unique_together = (
            ("account", "id"),
        )
        indexes = [
            models.Index(
                fields=["account", "aggregated_time"], name="h_acc_created_idx"
            ),
            models.Index(
                fields=["account", "stock", "aggregated_time"],
                name="h_acc_stock_created_idx",
            ),
            models.Index(
                fields=["account", "outlet", "aggregated_time"],
                name="h_st_acc_outlet_created_idx",
            ),
        ]
        constraints  = [
            UniqueConstraint(
                fields=(
                    "account", 
                    "transaction_type", "auto_production",
                    "aggregated_time",            
                    "stock", "outlet", "unit",
                ),
                condition=Q(deleted__isnull=True),
                name='unique_hstock_trans_cons'),
        ]

    def __str__(self):
        return "{} - {}".format(self.stock, self.quantity)

    def save(self, *args, **kwargs):
        if not self.outlet:
            self.outlet = self.stock.outlet
        super().save(*args, **kwargs)


class StockTransfer(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ("canceled", "canceled", _("ditolak")),
        ("active", "active", _("aktif")),
        ("received", "received", _("diterima")),
    )

    status = models.CharField(
        _("Transfer Status"),
        max_length=15,
        choices=STATUS,
        default=STATUS.active,
        db_index=True,
    )

    code = models.CharField(_("Transfer Code"), max_length=120, db_index=True)

    source = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="stock_transfer", on_delete=models.CASCADE
    )
    destination = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="stock_receive", on_delete=models.CASCADE
    )

    date_of_transfer = models.DateTimeField(db_index=True, default=timezone.now)
    date_of_arrival = models.DateTimeField(null=True, db_index=True)

    note = models.TextField(_("Notes"), blank=True)

    class Meta:
        verbose_name = "StockTransfer"
        verbose_name_plural = "StockTransfers"
        unique_together = (("account", "id"),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "{} {} - {} [{}]".format(
            self.code, self.source.name, self.destination.name, self.status
        )

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)
        return string

    def received(self, user=None):
        if self.status == self.STATUS.active:
            for transfered_stock in self.transfered_stock.all():
                transfered_stock.process_transfer(user)

            self.status = self.STATUS.received
            self.save()
            return self

    def transfer_product_stock(self, product, unit, quantity):
        stock_source = product.stocks.get(outlet=self.source)
        stock_destination = product.stocks.get(outlet=self.destination)
        return self.transfered_stock.create(
            stock_source=stock_source,
            stock_destination=stock_destination,
            unit=unit,
            quantity=quantity,
            account=self.account,
        )

    def generate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            today = datetime.today()
            count = self.__class__.objects.filter(
                created__year=today.year, created__month=today.month
            ).count()
            count = "{0:0=4d}".format(count + 1)
            account = Account.objects.get(pk=get_current_tenant().pk)
            business_code = account.business_code
            if not business_code:
                business_code = acronym(account.name)
            source_outlet_code = self.source.outlet_code
            destination_outlet_code = self.destination.outlet_code
            if not source_outlet_code:
                source_outlet_code = acronym(self.source.name)
            if not destination_outlet_code:
                destination_outlet_code = acronym(self.destination.name)
            return "{}/{}-{}/ST/{}/{}/{}".format(
                business_code,
                source_outlet_code,
                destination_outlet_code,
                str(today.year)[-2:],
                today.month,
                count,
            )
        return "ST-{}".format(self.__class__.all_objects.count() + 1)

    def regenerate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            code = self.code.split("/")
            source_outlet_code = self.source.outlet_code
            destination_outlet_code = self.destination.outlet_code
            if not source_outlet_code:
                source_outlet_code = acronym(self.source.name)
            if not destination_outlet_code:
                destination_outlet_code = acronym(self.destination.name)
            new_code = self.code.replace(
                code[1], "{}-{}".format(source_outlet_code, destination_outlet_code)
            )
            return new_code
        return self.code

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = self.generate_code()
        else:
            self.code = self.regenerate_code()

        return super().save()


class StockTransferDetail(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    stock_transfer = SterlingTenantForeignKey(
        "stocks.StockTransfer",
        related_name="transfered_stock",
        on_delete=models.CASCADE,
    )

    stock_source = SterlingTenantForeignKey(
        "stocks.Stock", related_name="transfer", on_delete=models.CASCADE
    )
    stock_destination = SterlingTenantForeignKey(
        "stocks.Stock", related_name="receive", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="transfered_stock",
        on_delete=models.CASCADE,
    )

    @property
    def is_stock_available(self):

        converted_qty = uom_conversion(
            self.unit, self.stock_source.product.uom, self.quantity
        )
        balance = self.stock_source.balance
        if converted_qty > balance:
            return False
        return True

    class Meta:
        verbose_name = "StockTransferDetail"
        verbose_name_plural = "StockTransferDetails"
        unique_together = (("account", "id"),)

    def process_transfer(self, user=None):
        converted_qty = uom_conversion(
            self.unit, self.stock_source.product.uom, self.quantity
        )
        if (
            self.stock_transfer.status == StockTransfer.STATUS.active
            and converted_qty > 0
        ):
            self.stock_source.subtract_balance(
                converted_qty,
                unit=self.stock_source.product.uom,
                source_object=self.stock_transfer,
                transaction_type=TRANSACTION_TYPE.transfer,
                user=user,
            )
            self.stock_destination.add_balance(
                converted_qty,
                unit=self.stock_destination.product.uom,
                source_object=self.stock_transfer,
                transaction_type=TRANSACTION_TYPE.transfer,
                user=user,
            )


class PurchaseOrder(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ("closed", "closed", _("closed")),
        ("active", "active", _("aktif")),
        ("partial", "partial", _("partially received")),
        ("received", "received", _("diterima")),
    )
    DISCOUNT_TYPE = Choices(
        ("fixed", _("Fixed Amount")), ("percentage", _("Percentage"))
    )

    status = models.CharField(
        _("Order Status"),
        max_length=15,
        choices=STATUS,
        default=STATUS.active,
        db_index=True,
    )

    code = models.CharField(_("Purchase Code"), max_length=120, db_index=True)

    supplier = SterlingTenantForeignKey(
        "stocks.Supplier", related_name="purchase_order", on_delete=models.CASCADE
    )

    outlet = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="purchase_order", on_delete=models.CASCADE
    )
    tax = models.DecimalField(_("Tax"), max_digits=4, decimal_places=2, default=0.0)
    discount_type = models.CharField(
        _("Discount Type"),
        max_length=20,
        choices=DISCOUNT_TYPE,
        default=DISCOUNT_TYPE.fixed,
        db_index=True,
    )
    discount = models.DecimalField(
        _("Discount"), max_digits=10, decimal_places=2, default=0.0
    )

    date_of_purchase = models.DateTimeField(db_index=True)
    shipping_date = models.DateTimeField(null=True, db_index=True)
    date_of_arrival = models.DateTimeField(null=True, db_index=True)

    note = models.TextField(_("Notes"), blank=True)

    class Meta:
        verbose_name = "PurchaseOrder"
        verbose_name_plural = "PurchaseOrders"
        unique_together = (("account", "id"),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "{} {} [{}]".format(
            self.code, self.outlet.name, self.get_status_display()
        )

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)
        return string

    def received(self):
        received_all = []
        for purchased_stock in self.purchased_stock.all().select_related(
            "stock", "unit"
        ):
            total_received = 0
            if self.purchase_received.all():
                for received in self.purchase_received.filter(received=True):
                    try:
                        detail = received.received_detail.get(
                            stock=purchased_stock.stock
                        )
                        converted_qty = uom_conversion(
                            detail.unit,
                            purchased_stock.unit,
                            detail.quantity,
                        )
                        total_received += converted_qty
                    except PurchaseOrderReceiveDetail.DoesNotExist:
                        total_received += 0
                if total_received < purchased_stock.quantity:
                    received_all.append(False)
                else:
                    received_all.append(True)

        if self.purchase_received.filter(received=True):
            if False in received_all:
                self.status = PurchaseOrder.STATUS.partial
            else:
                self.status = PurchaseOrder.STATUS.received
        else:
            self.status = PurchaseOrder.STATUS.active
        self.save()
        return self

    def purchase_product_stock(
        self, product, quantity, unit, purchase_cost, discount_type, discount
    ):
        stock = product.stocks.get(outlet=self.outlet)

        return self.purchased_stock.create(
            stock=stock,
            quantity=quantity,
            unit=unit,
            purchase_cost=purchase_cost,
            account=self.account,
            discount_type=discount_type,
            discount=discount,
        )

    @property
    def total_discount(self):
        if self.discount_type == "percentage":
            return self.total_cost * (self.discount / Decimal(100))
        return self.discount

    @property
    def total_taxes(self):
        return Decimal(self.total_cost - self.total_discount) * (
            self.tax / Decimal(100)
        )

    @property
    def total_cost(self):
        total = 0
        if not self.status == "closed":
            for purchased_stock in self.purchased_stock.all():
                total += purchased_stock.total_cost
        else:
            for purchased_stock in self.purchased_stock.filter(is_received=True):
                total += purchased_stock.total_cost
        return total

    @property
    def grand_total(self):
        return self.total_cost - self.total_discount + self.total_taxes

    @property
    def total_cost_received(self):
        total_cost_received = 0
        for received in self.purchase_received.filter(received=True):
            total_cost_received += received.total_cost
        return total_cost_received

    def get_product_quantity(self, product):
        total = 0
        try:
            unit_po = self.supplier.supplier_detail.get(product=product).unit
        except UOM.DoesNotExist:
            pass
        for purchased_stock in self.purchased_stock.all():
            if product == purchased_stock.stock.product:
                if not unit_po:
                    unit_po = UOM.objects.get(
                        category=purchased_stock.stock.product.uom, uom_type="base"
                    )
                converted_qty = uom_conversion(
                    purchased_stock.unit, unit_po, purchased_stock.quantity
                )
                total += converted_qty
        return total

    @property
    def products(self):
        products = []
        for purchased_stock in self.purchased_stock.all():
            if purchased_stock.stock.product not in products:
                products.append(purchased_stock.stock.product)
        return products

    @property
    def get_received(self):
        total = 0
        for purchased_stock in self.purchased_stock.all():
            if purchased_stock.is_received:
                total += purchased_stock.quantity
        return total

    def generate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            today = datetime.today()
            count = self.__class__.objects.filter(
                created__year=today.year, created__month=today.month
            ).count()
            count = "{0:0=4d}".format(count + 1)
            account = Account.objects.get(pk=get_current_tenant().pk)
            business_code = account.business_code
            if not business_code:
                business_code = acronym(account.name)
            outlet_code = self.outlet.outlet_code
            if not outlet_code:
                outlet_code = acronym(self.outlet.name)
            month = "{0:0=2d}".format(today.month)
            return "{}/{}/PO/{}/{}/{}".format(
                business_code, outlet_code, str(today.year)[-2:], month, count
            )
        return "SPO-{}".format(self.__class__.all_objects.count() + 1)

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = self.generate_code()

        return super().save()


class PurchaseOrderDetail(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    DISCOUNT_TYPE = Choices(("fixed", _("IDR")), ("percentage", _("%")))

    is_received = models.BooleanField(default=False)
    received_date = models.DateTimeField(blank=True, null=True)

    purchase_order = SterlingTenantForeignKey(
        "stocks.PurchaseOrder", related_name="purchased_stock", on_delete=models.CASCADE
    )

    stock = SterlingTenantForeignKey(
        "stocks.Stock", related_name="purchased", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="purchased_stock",
        on_delete=models.CASCADE,
    )

    purchase_cost = MoneyField(
        _("Purchase Cost"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    discount_type = models.CharField(
        _("Discount Type"),
        max_length=20,
        choices=DISCOUNT_TYPE,
        default=DISCOUNT_TYPE.fixed,
        db_index=True,
    )
    discount = models.DecimalField(
        _("Discount"), max_digits=10, decimal_places=2, default=0.0
    )

    class Meta:
        verbose_name = "PurchaseOrderDetail"
        verbose_name_plural = "PurchaseOrderDetails"
        unique_together = (("account", "id"),)

    @property
    def total_cost(self):
        if self.discount_type == "percentage":
            discount = self.quantity * (
                self.purchase_cost.amount * (self.discount / 100)
            )
        else:
            discount = self.quantity * self.discount
        result = self.quantity * self.purchase_cost.amount
        return result - discount

    @property
    def get_cancelled_item(self):
        total_received = 0
        for received in self.purchase_order.purchase_received.all():
            try:
                detail = received.received_detail.get(stock=self.stock)
                converted_qty = uom_conversion(
                    detail.unit,
                    self.unit,
                    detail.quantity,
                )
                total_received += converted_qty
            except PurchaseOrderReceiveDetail.DoesNotExist:
                total_received += 0

        total_cancelled = self.quantity - total_received
        return total_cancelled

    def process_purchase(self, quantity, unit, discount_type, discount, user):
        if quantity > 0 and not self.is_received:
            if (
                self.purchase_order.status
                not in [PurchaseOrder.STATUS.received, PurchaseOrder.STATUS.closed]
                and not self.is_received
            ):
                converted_qty = uom_conversion(unit, self.unit, quantity)
                unreceived = self.quantity - converted_qty
                if self.quantity > converted_qty:
                    self.purchase_order.purchase_product_stock(
                        product=self.stock.product,
                        quantity=unreceived,
                        unit=self.unit,
                        purchase_cost=self.purchase_cost,
                        discount_type=discount_type,
                        discount=discount,
                    )

                converted_qty = uom_conversion(unit, self.stock.product.uom, quantity)
                converted_cost = uom_cost_conversion(
                    self.unit, unit, quantity, self.purchase_cost.amount
                )
                self.stock.add_balance(
                    converted_qty,
                    unit=self.stock.product.uom,
                    source_object=self.purchase_order,
                    transaction_type=TRANSACTION_TYPE.purchase_order,
                    user=user,
                )

                self.is_received = True
                self.quantity = quantity
                self.unit = unit
                self.purchase_cost = converted_cost
                self.save()
            return self


class PurchaseOrderReceive(SterlingTenantModel, TimeStampedModel):
    purchase_order = SterlingTenantForeignKey(
        "stocks.PurchaseOrder",
        related_name="purchase_received",
        on_delete=models.CASCADE,
    )
    received_date = models.DateTimeField(db_index=True, default=timezone.now)
    code = models.CharField(_("Received Code"), max_length=120, db_index=True)
    received = models.BooleanField(_("Received"), default=False)

    receipt_number = models.CharField(
        _("Receipt Number"), null=True, max_length=120, db_index=True
    )

    class Meta:
        verbose_name = "PurchaseOrderReceive"
        verbose_name_plural = "PurchaseOrderReceives"
        unique_together = (("account", "id"),)

    def __str__(self):
        return self.code

    @property
    def total_cost(self):
        total = 0
        for received_detail in self.received_detail.all():
            total += received_detail.total_cost
        return total

    @property
    def total_discount(self):
        if self.purchase_order.discount_type == "percentage":
            return self.total_cost * (self.purchase_order.discount / Decimal(100))
        else:
            total_discount = 0
            for received_detail in self.received_detail.all():
                total_cost = (
                    received_detail.quantity * received_detail.purchase_cost.amount
                )
                total_cost_po = (
                    self.purchase_order.total_cost
                    if self.purchase_order.status in ["active", "partial"]
                    else self.purchase_order.total_cost_received
                )
                if total_cost_po > 0:
                    discount = self.purchase_order.discount * (
                        total_cost / total_cost_po
                    )
                else:
                    discount = 0
                total_discount += discount
            return total_discount

    @property
    def total_taxes(self):
        return (self.total_cost - self.total_discount) * (self.purchase_order.tax / 100)

    @property
    def grand_total(self):
        return self.total_cost - self.total_discount + self.total_taxes

    @property
    def total_quantity(self):
        total = 0
        for received in self.received_detail.all():
            converted_qty = uom_conversion(
                received.unit,
                received.stock.product.uom,
                received.quantity,
            )
            total += converted_qty
        return total

    def total_product_received(self, product):
        try:
            stock = product.stocks.get(outlet=self.purchase_order.outlet)
            purchased_stock = self.purchase_order.purchased_stock.get(stock=stock)
            total = 0
            for received in self.received_detail.filter(
                stock__product=product
            ).distinct():
                converted_qty = uom_conversion(
                    received.unit,
                    purchased_stock.unit,
                    purchased_stock.quantity,
                )
                total += converted_qty
            return total
        except Exception:
            return None

    def receive_purchase(self, stock, quantity, unit):
        try:
            purchased_stock = self.purchase_order.purchased_stock.get(stock=stock)
            if purchased_stock.unit != unit:
                cost = uom_cost_conversion(
                    purchased_stock.unit,
                    unit,
                    quantity,
                    purchased_stock.purchase_cost.amount,
                )
            else:
                cost = purchased_stock.purchase_cost.amount
            return self.received_detail.create(
                stock=stock,
                quantity=quantity,
                unit=unit,
                purchase_cost=cost,
                account=self.account,
                discount_type=purchased_stock.discount_type,
                discount=purchased_stock.discount,
            )
        except Exception:
            return None

    def update_received(self):
        for received_detail in self.received_detail.all():
            try:
                purchased_stock = self.purchase_order.purchased_stock.get(
                    stock=received_detail.stock
                )
                if purchased_stock.unit != received_detail.unit:
                    cost = uom_cost_conversion(
                        purchased_stock.unit,
                        received_detail.unit,
                        received_detail.quantity,
                        purchased_stock.purchase_cost.amount,
                    )
                else:
                    cost = purchased_stock.purchase_cost.amount
                received_detail.purchase_cost = cost
                received_detail.save()
            except Exception:
                pass

    def generate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            po_code = self.purchase_order.code.split("/")
            try:
                return "PO{}-{}-{}".format(po_code[5], po_code[1], self.receipt_number)
            except IndexError:
                return "RO-{}-{}".format(self.purchase_order.code, self.receipt_number)
        else:
            return "RO-{}-{}".format(self.purchase_order.code, self.receipt_number)

    def regenerate_code(self):
        code = self.code
        string_split = code.split("-")
        receipt_num = string_split[-1]
        code = code.replace(receipt_num, self.receipt_number)
        return code

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = self.generate_code()
        else:
            self.code = self.regenerate_code()
        return super().save()


class PurchaseOrderReceiveDetail(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    DISCOUNT_TYPE = Choices(("fixed", _("IDR")), ("percentage", _("%")))
    received = SterlingTenantForeignKey(
        "stocks.PurchaseOrderReceive",
        related_name="received_detail",
        on_delete=models.CASCADE,
    )

    stock = SterlingTenantForeignKey(
        "stocks.Stock", related_name="received", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="received_detail",
        on_delete=models.CASCADE,
    )

    purchase_cost = MoneyField(
        _("Purchase Cost"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    discount_type = models.CharField(
        _("Discount Type"),
        max_length=20,
        choices=DISCOUNT_TYPE,
        default=DISCOUNT_TYPE.fixed,
        db_index=True,
    )
    discount = models.DecimalField(
        _("Discount"), max_digits=10, decimal_places=2, default=0.0
    )

    class Meta:
        verbose_name = "PurchaseOrderReceiveDetail"
        verbose_name_plural = "PurchaseOrderReceiveDetails"
        unique_together = (("account", "id"),)

    @property
    def total_cost(self):
        if self.discount_type == "percentage":
            discount = self.quantity * (
                self.purchase_cost.amount * (self.discount / 100)
            )
        else:
            discount = self.quantity * self.discount
        result = self.quantity * self.purchase_cost.amount
        return result - discount

    def calculate_avg_cost(self):
        calculate_avarage_cost.apply_async(
            count_down=5,
            kwargs={
                "product_id": self.stock.product.pk,
                "quantity": self.quantity,
                "unit_id": self.unit.pk,
                "cost": self.purchase_cost.amount,
                "account_id": self.account.pk,
            },
        )

    def received_product(self, user):
        if self.quantity > 0:
            converted_qty = uom_conversion(
                self.unit, self.stock.product.uom, self.quantity
            )
            self.stock.add_balance(
                converted_qty,
                unit=self.stock.product.uom,
                source_object=self.received.purchase_order,
                transaction_type=TRANSACTION_TYPE.purchase_order,
                user=user,
            )
            try:
                purchase_order_detail = (
                    self.received.purchase_order.purchased_stock.get(stock=self.stock)
                )
                purchase_order_detail.is_received = True
                purchase_order_detail.save()
                self.calculate_avg_cost()
            except PurchaseOrderDetail.DoesNotExist:
                self.delete()
        return self


class StockAdjustment(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ("item_receive", "item_receive", _("Received Item")),
        ("stock_count", "stock_count", _("Stock Count")),
        ("item_loss", "item_loss", _("Item Loss")),
        ("item_damaged", "item_damaged", _("Damaged Item")),
        ("waste_product", "waste_product", _("Waste Product")),
    )

    status = models.CharField(
        _("Order Status"),
        max_length=15,
        choices=STATUS,
        default=STATUS.item_receive,
        db_index=True,
    )

    code = models.CharField(_("Adjustment Code"), max_length=120, db_index=True)

    outlet = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="stock_adjustment", on_delete=models.CASCADE
    )
    date_of_adjustment = models.DateTimeField(db_index=True, default=timezone.now)

    note = models.TextField(_("Notes"), blank=True)
    is_processed = models.BooleanField(default=False)

    class Meta:
        verbose_name = "StockAdjustment"
        verbose_name_plural = "StockAdjustments"
        unique_together = (("account", "id"),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "{} {} [{}] {}".format(
            self.code, self.outlet.name, self.status, self.note
        )

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)
        return string

    def process_adjustment(self, user=None):
        if not self.is_processed:
            for adjusted_stock in self.adjusted_stock.all().select_related(
                "stock", "unit", "account", "stock__product__uom", "stock__outlet"
            ):
                adjusted_stock.process_adjustment(user)

            self.is_processed = True
            self.save()
            return self

    def adjust_product_stock(self, stock, unit, quantity, cost):
        # stock = product.stocks.get(outlet=self.outlet)
        return self.adjusted_stock.create(
            stock=stock, unit=unit, quantity=quantity, cost=cost, account=self.account
        )

    def generate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            today = datetime.today()
            count = self.__class__.objects.filter(
                created__year=today.year, created__month=today.month
            ).count()
            count = "{0:0=4d}".format(count + 1)
            account = Account.objects.get(pk=get_current_tenant().pk)
            business_code = account.business_code
            if not business_code:
                business_code = acronym(account.name)
            outlet_code = self.outlet.outlet_code
            if not outlet_code:
                outlet_code = acronym(self.outlet.name)
            return "{}/{}/SA/{}/{}/{}".format(
                business_code, outlet_code, str(today.year)[-2:], today.month, count
            )
        return "SA-{}".format(self.__class__.all_objects.count() + 1)

    @property
    def total_cost(self):
        total = 0
        if self.status == self.STATUS.item_receive:
            for adjusted_stock in self.adjusted_stock.all():
                total += adjusted_stock.total_cost
        return total

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = self.generate_code()

        return super().save()


class StockAdjustmentDetail(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    stock_adjustment = SterlingTenantForeignKey(
        "stocks.StockAdjustment",
        related_name="adjusted_stock",
        on_delete=models.CASCADE,
    )

    stock = SterlingTenantForeignKey(
        "stocks.Stock", related_name="adjusted_stock", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="adjusted_stock",
        on_delete=models.CASCADE,
    )
    cost = MoneyField(
        _("Cost"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
        default=0.0,
    )

    class Meta:
        verbose_name = "StockAdjustmentDetail"
        verbose_name_plural = "StockAdjustmentDetails"
        unique_together = (("account", "id"),)

    def process_adjustment(self, user=None):
        converted_qty = uom_conversion(self.unit, self.stock.product.uom, self.quantity)
        if not self.stock_adjustment.is_processed:
            if self.stock_adjustment.status == StockAdjustment.STATUS.item_receive:
                self.stock.add_balance(
                    converted_qty,
                    unit=self.stock.product.uom,
                    source_object=self.stock_adjustment,
                    transaction_type=TRANSACTION_TYPE.stock_adjustment,
                    user=user,
                )
            if self.stock_adjustment.status == StockAdjustment.STATUS.stock_count:
                self.stock.set_balance(
                    converted_qty,
                    unit=self.stock.product.uom,
                    source_object=self.stock_adjustment,
                    transaction_type=TRANSACTION_TYPE.stock_adjustment,
                    user=user,
                )
            if self.stock_adjustment.status in [
                StockAdjustment.STATUS.item_loss,
                StockAdjustment.STATUS.item_damaged,
                StockAdjustment.STATUS.waste_product,
            ]:
                self.stock.subtract_balance(
                    converted_qty,
                    unit=self.stock.product.uom,
                    source_object=self.stock_adjustment,
                    transaction_type=TRANSACTION_TYPE.stock_adjustment,
                    user=user,
                )
            return self

    def calculate_avg_cost(self):
        calculate_avarage_cost.apply_async(
            count_down=5,
            kwargs={
                "product_id": self.stock.product.pk,
                "quantity": self.quantity,
                "unit_id": self.unit.pk,
                "cost": self.cost.amount,
                "account_id": self.account.pk,
            },
        )

    @property
    def total_cost(self):
        total = 0
        if self.stock_adjustment.status == StockAdjustment.STATUS.item_receive:
            total = self.quantity * self.cost.amount
        return total

    @property
    def get_last_stock_before_adjustment(self):
        last_stock = 0
        transaction = StockTransaction.objects.filter(stock=self.stock).last()
        last_stock = transaction.tail_sum - self.quantity
        return last_stock


class StockCount(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ("canceled", "canceled", _("Canceled")),
        ("pending", "pending", _("Pending")),
        ("in_progress", "in_progress", _("In Progress")),
        ("completed", "completed", _("Completed")),
    )

    STOCK_COUNT_TYPE = Choices(
        ("partial", _("Partial Stock Opname")),
        ("full", _("Full Stock Opname")),
    )

    status = models.CharField(
        _("Stock Count Status"),
        max_length=15,
        choices=STATUS,
        default=STATUS.pending,
        db_index=True,
    )

    code = models.CharField(_("Count Code"), max_length=120, db_index=True)

    outlet = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="stock_count", on_delete=models.CASCADE
    )
    date_of_stock_count = models.DateTimeField(db_index=True, default=timezone.now)
    date_of_completion = models.DateTimeField(null=True, db_index=True)
    stock_count_type = models.CharField(
        _("Stock Count Type"),
        max_length=15,
        choices=STOCK_COUNT_TYPE,
        default=STOCK_COUNT_TYPE.partial,
    )
    include_sellable = models.BooleanField(_("Include Sellable Product"), default=True)
    include_manufactured = models.BooleanField(
        _("Include Manufactured Product"), default=True
    )

    note = models.TextField(_("Notes"), blank=True)

    class Meta:
        verbose_name = "StockCount"
        verbose_name_plural = "StockCounts"
        unique_together = (("account", "id"),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "{} {} [{}] {}".format(
            self.code, self.outlet.name, self.status, self.note
        )

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)
        return string

    def process_adjustment(self, user=None):
        if self.status not in [self.STATUS.canceled, self.STATUS.completed]:
            for counted_stock in self.counted_stock.all():
                counted_stock.process_adjustment(user)

            self.status = StockCount.STATUS.completed
            self.save()
            return self

    def count_product_stock(self, product):
        stock = product.stocks.get(outlet=self.outlet)

        return self.counted_stock.create(
            stock=stock, quantity=stock.balance, account=self.account, unit=product.uom
        )

    def full_count_product_stock(self):
        products = Product.objects.filter(is_manage_stock=True, uom__isnull=False)
        if not self.include_sellable:
            products = products.exclude(is_sellable=True)
        if not self.include_manufactured:
            products = products.exclude(
                classification__in=[Product.PRODUCT_CLASSIFICATION.manufactured]
            )

        for product in products:
            stock = product.stocks.get(outlet=self.outlet)
            self.counted_stock.create(
                stock=stock,
                quantity=stock.balance,
                account=self.account,
                unit=product.uom,
            )

    def full_count_product_stock_update(self):
        for counted in self.counted_stock.all():
            product = counted.stock.product
            stock = product.stocks.get(outlet=self.outlet)
            counted.stock = stock
            counted.quantity = stock.balance
            counted.unit = product.uom
            counted.save()

    def complete_full_counting(self, user=None):
        if self.status not in [self.STATUS.canceled, self.STATUS.completed]:
            self.status = StockCount.STATUS.completed
            self.save()
            if user:
                user = user.pk
            set_counting_to_stocks.delay(self.pk, self.account.pk, user)
        return self

    def generate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            today = datetime.today()
            count = self.__class__.objects.filter(
                created__year=today.year, created__month=today.month
            ).count()
            count = "{0:0=4d}".format(count + 1)
            account = Account.objects.get(pk=get_current_tenant().pk)
            business_code = account.business_code
            if not business_code:
                business_code = acronym(account.name)
            outlet_code = self.outlet.outlet_code
            if not outlet_code:
                outlet_code = acronym(self.outlet.name)
            return "{}/{}/SC/{}/{}/{}".format(
                business_code, outlet_code, str(today.year)[-2:], today.month, count
            )
        return "SC-{}".format(self.__class__.all_objects.count() + 1)

    def regenerate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            code = self.code.split("/")
            outlet_code = self.outlet.outlet_code
            if not outlet_code:
                outlet_code = acronym(self.outlet.name)
            new_code = self.code.replace(code[1], outlet_code)
            return new_code
        return self.code

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = self.generate_code()
        else:
            self.code = self.regenerate_code()
        return super().save()


class StockCountDetail(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    stock_count = SterlingTenantForeignKey(
        "stocks.StockCount", related_name="counted_stock", on_delete=models.CASCADE
    )

    stock = SterlingTenantForeignKey(
        "stocks.Stock", related_name="counted_stock", on_delete=models.CASCADE
    )

    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="counted_stock",
        on_delete=models.CASCADE,
    )

    counted = models.DecimalField(
        _("Counted"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
        default=0.000,
    )

    class Meta:
        verbose_name = "StockCountDetail"
        verbose_name_plural = "StockCountDetails"
        unique_together = (("account", "id"),)

    def process_adjustment(self, user=None):
        converted_counted = uom_conversion(
            self.unit, self.stock.product.uom, self.counted
        )
        if (
            self.stock_count.status
            not in [StockCount.STATUS.canceled, StockCount.STATUS.completed]
            and self.quantity != self.counted
        ):
            self.stock.set_balance(
                converted_counted,
                unit=self.stock.product.uom,
                source_object=self.stock_count,
                transaction_type=TRANSACTION_TYPE.stock_count,
                user=user,
            )
            return self

    @property
    def get_last_stock_before_count(self):
        return self.stock.balance

    @property
    def get_stocks_difference(self):
        counted = uom_conversion(self.unit, self.stock.product.uom, self.counted)
        return counted - self.quantity


class Batch(SterlingTenantModel, TimeStampedModel):
    product = SterlingTenantForeignKey(
        "catalogue.Product", related_name="batches", on_delete=models.CASCADE
    )

    expiration_date = models.DateField(_("Expiration Date"), null=True)
    is_missing_batch = models.BooleanField(default=False)
    batch_id = models.CharField(_("Batch ID"), max_length=120, null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Batch"
        verbose_name_plural = "Batches"
        unique_together = (("account", "id"),)

    def __str__(self):
        return self.batch_id

    @property
    def balance(self):
        total = 0
        for batch_item in self.batch_items.all():
            total += batch_item.balance
        return total


class BatchItem(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    batch = SterlingTenantForeignKey(
        "stocks.Batch", related_name="batch_items", on_delete=models.CASCADE
    )
    outlet = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="batch_items", on_delete=models.CASCADE
    )

    @property
    def balance(self):
        info = self.histories.aggregate(total=Coalesce(Sum("quantity"), Value(0)))
        return info["total"]

    def _update_batch_item_balance(
        self, quantity, unit, source_object, transaction_type, **kwargs
    ):
        if source_object:
            reference = serializers.serialize("json", [source_object])
        else:
            reference = "{}"

        history = BatchHistory.objects.create(
            batch_item=self,
            quantity=quantity,
            unit=unit,
            transaction_type=transaction_type,
            reference=reference,
            account=self.account,
            **kwargs,
        )

        return history.quantity

    def add_batch_balance(
        self,
        quantity,
        unit,
        source_object=None,
        transaction_type=TRANSACTION_TYPE.manual_add,
        **kwargs,
    ):
        return self._update_batch_item_balance(
            abs(quantity), unit, source_object, transaction_type, **kwargs
        )

    def substract_batch_balance(
        self,
        quantity,
        unit,
        source_object=None,
        transaction_type=TRANSACTION_TYPE.manual_substract,
        **kwargs,
    ):
        quantity = -1 * abs(quantity)
        return abs(
            self._update_batch_item_balance(
                quantity, unit, source_object, transaction_type, **kwargs
            )
        )

    def set_batch_balance(
        self,
        quantity,
        unit,
        source_object=None,
        transaction_type=TRANSACTION_TYPE.manual_set,
        **kwargs,
    ):
        quantity = quantity - self.balance
        return abs(
            self._update_batch_item_balance(
                quantity, unit, source_object, transaction_type, **kwargs
            )
        )

    class Meta:
        verbose_name = "BatchItem"
        verbose_name_plural = "BatchItems"
        unique_together = (("account", "id"),)


class BatchHistory(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    batch_item = SterlingTenantForeignKey(
        "stocks.BatchItem", related_name="histories", on_delete=models.CASCADE
    )
    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="batch_histories",
        on_delete=models.CASCADE,
    )

    transaction_type = models.CharField(
        _("Transaction Type"), max_length=50, choices=TRANSACTION_TYPE, db_index=True
    )

    reference = JSONField()

    class Meta:
        verbose_name = "BatchHistory"
        verbose_name_plural = "BatchHistories"
        unique_together = (("account", "id"),)


class StockCardProxy(Product):
    objects = StockCardTenantManager()

    class Meta:
        proxy = True


class InventorySetting(SterlingTenantModel, TimeStampedModel):
    CODE_FORMAT = Choices(
        ("simple", "simple", _("Simple")),
        ("comprehensive", "comprehensive", _("Comprehensive")),
    )

    inventory_code_format = models.CharField(
        _("Inventory Code Format"),
        max_length=50,
        choices=CODE_FORMAT,
        default=CODE_FORMAT.simple,
    )
    vendor_bill_accurate_excel = models.BooleanField(
        _("Vendor Bill Accurate Excel"), default=False
    )

    class Meta:
        verbose_name = "InventorySetting"
        verbose_name_plural = "InventorySettings"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Inventory Settings {}".format(self.account)


class VendorBill(SterlingTenantModel, TimeStampedModel):
    bill_date = models.DateField(db_index=True)
    due_date = models.DateField(db_index=True)
    received_order = SterlingTenantForeignKey(
        "stocks.PurchaseOrderReceive",
        related_name="vendor_bill",
        on_delete=models.CASCADE,
    )
    code = models.CharField(_("Vendor Bill Code"), max_length=120, db_index=True)

    class Meta:
        verbose_name = "VendorBill"
        verbose_name_plural = "VendorBills"
        unique_together = (("account", "id"),)

    def __str__(self):
        return self.code

    def create_vendor_bill_detail(self, product, purchase_cost):
        try:
            received_detail = self.received_order.received_detail.get(
                stock__product=product
            )
            data = {
                "product": received_detail.stock.product,
                "quantity": received_detail.quantity,
                "unit": received_detail.unit,
                "purchase_cost": purchase_cost,
                "discount_type": received_detail.discount_type,
                "discount": received_detail.discount,
            }
            return self.vendor_bill_detail.create(**data)
        except Exception:
            return None

    def generate_code(self):
        if (
            InventorySetting.objects.first().inventory_code_format
            == InventorySetting.CODE_FORMAT.comprehensive
        ):
            count = self.__class__.objects.filter(
                bill_date__year=self.bill_date.year,
                bill_date__month=self.bill_date.month,
            ).count()
            count = "{0:0=4d}".format(count + 1)
            outlet_code = self.received_order.purchase_order.outlet.outlet_code
            if not outlet_code:
                outlet_code = acronym(self.received_order.purchase_order.outlet.name)
            month = "{0:0=2d}".format(self.bill_date.month)
            return "VB/{}/{}/{}/{}".format(
                outlet_code, str(self.bill_date.year)[-2:], month, count
            )
        else:
            count = self.__class__.objects.all().count()
            count = "{0:0=4d}".format(count + 1)
            return "VB-{}".format(count)

    @property
    def total_cost(self):
        total = 0
        for vendor_bill_detail in self.vendor_bill_detail.all():
            total += vendor_bill_detail.total_cost
        return total

    @property
    def grand_total(self):
        return self.total_cost

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = self.generate_code()

        return super().save()


class VendorBillDetail(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    DISCOUNT_TYPE = Choices(("fixed", _("IDR")), ("percentage", _("%")))

    vendor_bill = SterlingTenantForeignKey(
        "stocks.VendorBill", related_name="vendor_bill_detail", on_delete=models.CASCADE
    )
    received_date = models.DateTimeField(blank=True, null=True)
    product = SterlingTenantForeignKey(
        "catalogue.Product",
        related_name="vendor_bill_detail",
        on_delete=models.CASCADE,
    )
    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
    )
    unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="vendor_bill_detail",
        on_delete=models.CASCADE,
    )
    purchase_cost = MoneyField(
        _("Purchase Cost"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    discount_type = models.CharField(
        _("Discount Type"),
        max_length=20,
        choices=DISCOUNT_TYPE,
        default=DISCOUNT_TYPE.fixed,
        db_index=True,
    )
    discount = models.DecimalField(
        _("Discount"), max_digits=10, decimal_places=2, default=0.0
    )

    class Meta:
        verbose_name = "VendorBillDetail"
        verbose_name_plural = "VendorBillDetails"
        unique_together = (("account", "id"),)

    @property
    def total_cost(self):
        if self.discount_type == "percentage":
            discount = self.quantity * (
                self.purchase_cost.amount * (self.discount / 100)
            )
        else:
            discount = self.quantity * self.discount
        result = self.quantity * self.purchase_cost.amount
        return result - discount
