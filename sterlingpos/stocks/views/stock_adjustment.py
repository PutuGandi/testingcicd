import pytz
import rules

from django.utils import timezone
from django.views.generic import (
    TemplateView,
    DetailView,
)

from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.db import transaction
from django.core.exceptions import SuspiciousOperation

from datetime import datetime
from decimal import Decimal

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import MessageMixin
from formtools.wizard.views import SessionWizardView
from formtools.wizard.forms import ManagementForm

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.catalogue.models import Product
from sterlingpos.catalogue.utils import (
    uom_conversion,
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    StockAdjustment,
    Batch,
    TRANSACTION_TYPE,
    BatchItem,
)
from sterlingpos.stocks.forms import (
    StockAdjustmentForm,
    StockAdjustmentDetailFormset,
    StockAdjustmentDetailFormsetHelper,
    BatchFormset,
)
from sterlingpos.catalogue.utils import get_normalized_decimal

from .mixin import PDFExportMixin
from .util import has_batch_tracked


STEP_STOCK_ADJUSTMENT_FORM = "stock_adjustment_form"
STEP_BATCH_TRACKED_FORM = "batch_tracked_form"


class StockAdjustmentListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ["code", "date_of_adjustment", "outlet", "status", "pk"]
    order_columns = ["code", "date_of_adjustment", "outlet__pk", "status", "pk"]
    datetime_col = ["date_of_adjustment"]
    model = StockAdjustment
    permission_name = "stock_adjustment"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.prefetch_related("adjusted_stock")
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(outlet__in=outlet_qs)
        return qs

    def render_column(self, row, column):
        if column == "code":
            action_data = {
                "name": row.code,
                "url": reverse_lazy(
                    "stocks:stocks_adjustment_detail", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "date_of_adjustment":
            fmt = "%d/%m/%Y %H:%M"
            date = row.date_of_adjustment.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        elif column == "outlet":
            return row.outlet.name
        else:
            return super().render_column(row, column)

    def filter_status(self, search_value):
        if search_value == "show":
            return Q()
        return Q(status=search_value)

    def filter_outlet__pk(self, value):
        return Q(outlet__pk=value)


class StockAdjustmentListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_adjustment_list.html"
    permission_name = "stock_adjustment"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        return context


class StockAdjustmentDetailView(SterlingRoleMixin, PDFExportMixin, DetailView):
    template_name = "dashboard/stocks/stock_adjustment.html"
    pdf_template = "dashboard/stocks/pdf/stock_adjustment.html"
    model = StockAdjustment
    permission_name = "stock_adjustment"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["adjusted_stock"] = self.object.adjusted_stock.all().select_related(
            "unit",
            "stock__outlet",
            "stock__product",
            "stock__product__uom",
            "stock__product__catalogue",
        )
        return context

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_pdf_data(self):
        outlet_permission = self.get_outlet_permission()
        data = {
            "object": self.get_object(),
            "adjusted_stock": self.get_object()
            .adjusted_stock.all()
            .select_related(
                "stock", "stock__product", "stock__product__catalogue", "unit"
            ),
            "base_url": self.request.build_absolute_uri("/"),
            "logo": self.request.user.account.brand_logo,
            "account": self.request.user.account,
            "outlet_permission": outlet_permission,
        }
        return data


class StockAdjustmentWizardView(SterlingRoleMixin, MessageMixin, SessionWizardView):
    template_name = {
        STEP_STOCK_ADJUSTMENT_FORM: "dashboard/stocks/stock_adjustment_form.html",
        STEP_BATCH_TRACKED_FORM: "dashboard/stocks/batch_tracked_form.html",
    }
    form_list = [
        (STEP_STOCK_ADJUSTMENT_FORM, StockAdjustmentForm),
        (STEP_BATCH_TRACKED_FORM, BatchFormset),
    ]
    condition_dict = {STEP_BATCH_TRACKED_FORM: has_batch_tracked}
    permission_name = "stock_adjustment"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy("stocks:stocks_adjustment")

    def __init__(self, *args, **kwargs):
        super(StockAdjustmentWizardView, self).__init__(*args, **kwargs)
        self.batched_products = []

    def get_template_names(self):
        return [self.template_name[self.steps.current]]

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def render(self, form=None, **kwargs):
        form = form or self.get_form()
        context = self.get_context_data(form=form, **kwargs)
        if self.steps.current == STEP_STOCK_ADJUSTMENT_FORM:
            outlet_permission = self.get_outlet_permission()
            context["formset"] = StockAdjustmentDetailFormset(
                self.storage.get_step_data(self.steps.current),
                instance=form.instance,
                form_kwargs={"outlet_permission": outlet_permission},
            )
        return self.render_to_response(context)

    def get_context_data(self, form, **kwargs):
        context = super(StockAdjustmentWizardView, self).get_context_data(
            form=form, **kwargs
        )
        if self.steps.current == STEP_STOCK_ADJUSTMENT_FORM:
            outlet_permission = self.get_outlet_permission()
            if "formset" not in context:
                context["formset"] = StockAdjustmentDetailFormset(
                    form_kwargs={"outlet_permission": outlet_permission}
                )
            context["formset_helper"] = StockAdjustmentDetailFormsetHelper

        if self.steps.current == STEP_BATCH_TRACKED_FORM:
            context["inventory_type"] = TRANSACTION_TYPE.stock_adjustment
            data = self.storage.get_step_data(STEP_STOCK_ADJUSTMENT_FORM)
            context["outlet_pk"] = data.get("stock_adjustment_form-outlet")
            context["outlet"] = Outlet.objects.get(pk=context["outlet_pk"])
            context["status"] = data.get("stock_adjustment_form-status")
            context["status_display"] = StockAdjustment.STATUS[
                data.get("stock_adjustment_form-status")
            ]

        return context

    def post(self, *args, **kwargs):
        wizard_goto_step = self.request.POST.get("wizard_goto_step", None)
        if wizard_goto_step and wizard_goto_step in self.get_form_list():
            return self.render_goto_step(wizard_goto_step)

        management_form = ManagementForm(self.request.POST, prefix=self.prefix)
        if not management_form.is_valid():
            raise SuspiciousOperation(
                _("ManagementForm data is missing or has been tampered.")
            )

        form_current_step = management_form.cleaned_data["current_step"]
        if (
            form_current_step != self.steps.current
            and self.storage.current_step is not None
        ):
            self.storage.current_step = form_current_step

        form = self.get_form(data=self.request.POST, files=self.request.FILES)
        if form.is_valid():
            self.storage.set_step_data(self.steps.current, self.process_step(form))
            self.storage.set_step_files(
                self.steps.current, self.process_step_files(form)
            )
            if self.steps.current == self.steps.last:
                if self.steps.current == STEP_STOCK_ADJUSTMENT_FORM:
                    outlet_permission = self.get_outlet_permission()
                    formset = StockAdjustmentDetailFormset(
                        self.request.POST,
                        instance=form.instance,
                        form_kwargs={"outlet_permission": outlet_permission},
                    )
                    if formset.is_valid():
                        return self.render_done(form)
                    else:
                        error_messages = _(
                            """There was a problem creating this Stock Adjustment.
                            Please review the fields marked red and resubmit."""
                        )

                        self.messages.error(error_messages)
                        return self.render_to_response(
                            self.get_context_data(form=form, formset=formset)
                        )
                else:
                    return self.render_done(form, **kwargs)
            else:
                if self.steps.current == STEP_STOCK_ADJUSTMENT_FORM:
                    outlet_permission = self.get_outlet_permission()
                    formset = StockAdjustmentDetailFormset(
                        self.request.POST,
                        instance=form.instance,
                        form_kwargs={"outlet_permission": outlet_permission},
                    )
                    if formset.is_valid():
                        return self.render_next_step(form)
                    else:
                        error_messages = _(
                            """There was a problem creating this Stock Adjustment.
                            Please review the fields marked red and resubmit."""
                        )

                        self.messages.error(error_messages)
                        return self.render_to_response(
                            self.get_context_data(form=form, formset=formset)
                        )
                else:
                    return self.render_done(form, **kwargs)
        if self.steps.current not in self.steps.all:
            return self.render_done(form, **kwargs)
        return self.render(form)

    def get_next_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return self.steps.first
        key = keys.index(step) + 1
        if len(keys) > key:
            return keys[key]
        return None

    def get_prev_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return None
        key = keys.index(step) - 1
        if key >= 0:
            return keys[key]
        return None

    def get_form_initial(self, step):
        if step == STEP_BATCH_TRACKED_FORM:
            initial_data = []
            data = self.storage.get_step_data(STEP_STOCK_ADJUSTMENT_FORM)
            total_products = data.get("adjusted_stock-TOTAL_FORMS")
            for i in range(int(total_products)):
                product_id = data.get("adjusted_stock-" + str(i) + "-product")
                quantity = data.get("adjusted_stock-" + str(i) + "-quantity")
                unit = data.get("adjusted_stock-" + str(i) + "-unit")
                deleted = data.get("adjusted_stock-" + str(i) + "-DELETE")
                if not deleted and product_id and quantity:
                    try:
                        product = Product.objects.get(pk=product_id)
                        if (
                            product.is_batch_tracked
                            and Decimal(quantity) > 0
                            and product not in self.batched_products
                        ):
                            self.batched_products.append(product)
                            initial_data.append(
                                {
                                    "product": product,
                                    "product_name": product.name,
                                    "quantity_to_assign": "{} {}".format(
                                        get_normalized_decimal(Decimal(quantity)), unit
                                    ),
                                    "total_quantity": quantity,
                                    "unit": unit,
                                }
                            )
                    except Product.DoesNotExist:
                        pass
            return self.instance_dict.get(step, initial_data)
        return super().get_form_initial(step)

    def get_form(self, step=None, data=None, files=None):
        form = super(StockAdjustmentWizardView, self).get_form(step, data, files)
        if step == STEP_BATCH_TRACKED_FORM:
            form.extra = len(self.batched_products)
        return form

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step)
        if step == STEP_BATCH_TRACKED_FORM:
            kwargs["status"] = self.get_cleaned_data_for_step(
                STEP_STOCK_ADJUSTMENT_FORM
            )["status"]
            kwargs["outlet"] = self.get_cleaned_data_for_step(
                STEP_STOCK_ADJUSTMENT_FORM
            )["outlet"]
        return kwargs

    def done(self, form_list, **kwargs):
        adjustment_form = list(form_list)[0]
        data = self.storage.get_step_data(STEP_STOCK_ADJUSTMENT_FORM)
        outlet_permission = self.get_outlet_permission()
        adjustment_formset = StockAdjustmentDetailFormset(
            data,
            instance=adjustment_form.instance,
            form_kwargs={"outlet_permission": outlet_permission},
        )

        if len(form_list) > 1:
            batch_formset = list(form_list)[1]

        with transaction.atomic():
            adjustment_form.instance.save()
            status = adjustment_form.instance.status
            outlet = adjustment_form.instance.outlet

            product_avg_costs = []

            for adjustment_product in adjustment_formset:
                if adjustment_product.is_valid() and adjustment_product.has_changed():
                    deleted = adjustment_product.cleaned_data.get("DELETE")
                    if not deleted:
                        adjusted_product = adjustment_product.save(commit=True)
                        if adjustment_form.instance.status in [
                            StockAdjustment.STATUS.item_receive
                        ]:
                            product_avg_costs.append(adjusted_product)

                        if adjusted_product.stock.product.is_batch_tracked:
                            if status == StockAdjustment.STATUS.stock_count:
                                batches = adjusted_product.stock.product.batches.all().prefetch_related(
                                    "batch_items"
                                )
                                for batch in batches:
                                    batch_outlet = batch.batch_items.filter(
                                        outlet=outlet
                                    )
                                    if batch_outlet:
                                        batch_item = batch_outlet.first()
                                        if adjusted_product.quantity == 0:
                                            batch_item.delete()
                                    if batch.balance == 0:
                                        batch.delete()
            adjustment_form.instance.process_adjustment(self.request.user)

            if len(form_list) > 1:
                for batch_form in batch_formset:
                    count_batches = []
                    if batch_form.is_valid():
                        for item in batch_form.nested:
                            if item.is_valid():
                                batch = None
                                deleted = item.cleaned_data.get("DELETE")
                                batch_id = item.cleaned_data.get("batch_id")
                                quantity = item.cleaned_data.get("quantity")
                                unit = item.cleaned_data.get("unit")
                                converted_qty = uom_conversion(
                                    unit, batch_form.instance.product.uom, quantity
                                )
                                if not deleted:
                                    if status in [
                                        StockAdjustment.STATUS.item_receive,
                                        StockAdjustment.STATUS.stock_count,
                                    ]:
                                        batch, created = Batch.objects.get_or_create(
                                            batch_id=batch_id,
                                            product=batch_form.instance.product,
                                        )

                                        if created:
                                            expiration_date = datetime.strptime(
                                                item.cleaned_data.get(
                                                    "expiration_date"
                                                ),
                                                "%d/%m/%Y",
                                            )
                                            batch.expiration_date = expiration_date
                                            batch.save()

                                            batch_item = item.save(commit=False)
                                            batch_item.batch = batch
                                            batch_item.outlet = outlet
                                            batch_item.save()
                                            if (
                                                status
                                                == StockAdjustment.STATUS.item_receive
                                            ):
                                                batch_item.add_batch_balance(
                                                    converted_qty,
                                                    batch_form.instance.product.uom,
                                                    adjustment_form.instance,
                                                    TRANSACTION_TYPE.stock_adjustment,
                                                )
                                            else:
                                                batch_item.set_batch_balance(
                                                    converted_qty,
                                                    batch_form.instance.product.uom,
                                                    adjustment_form.instance,
                                                    TRANSACTION_TYPE.stock_adjustment,
                                                )
                                                count_batches.append(batch.pk)
                                        else:
                                            (
                                                batch_item,
                                                created,
                                            ) = batch.batch_items.get_or_create(
                                                outlet=outlet
                                            )
                                            if (
                                                status
                                                == StockAdjustment.STATUS.stock_count
                                            ):
                                                batch_item.set_batch_balance(
                                                    converted_qty,
                                                    batch_form.instance.product.uom,
                                                    adjustment_form.instance,
                                                    TRANSACTION_TYPE.stock_adjustment,
                                                )
                                                count_batches.append(batch.pk)
                                            else:
                                                batch_item.add_batch_balance(
                                                    converted_qty,
                                                    batch_form.instance.product.uom,
                                                    adjustment_form.instance,
                                                    TRANSACTION_TYPE.stock_adjustment,
                                                )
                                    else:
                                        batch = Batch.objects.get(
                                            batch_id=batch_id,
                                            product=batch_form.instance.product,
                                        )

                                        batch_item = batch.batch_items.all().get(
                                            outlet=outlet
                                        )
                                        batch_item.substract_batch_balance(
                                            converted_qty,
                                            batch_form.instance.product.uom,
                                            adjustment_form.instance,
                                            TRANSACTION_TYPE.stock_adjustment,
                                        )
                                        if batch_item.balance == 0:
                                            batch_item.delete()
                            if batch:
                                if batch.balance == 0:
                                    batch.delete()
                if status == StockAdjustment.STATUS.stock_count:
                    deleted_batch = Batch.objects.filter(
                        product=batch_form.instance.product,
                        batch_items__outlet=outlet,
                    ).exclude(pk__in=count_batches)
                    for deleted in deleted_batch:
                        try:
                            deleted.batch_items.get(outlet=outlet).delete()
                        except BatchItem.DoesNotExist:
                            pass
                        if deleted.balance == 0:
                            deleted.delete()

            for product_avg_cost in product_avg_costs:
                product_avg_cost.calculate_avg_cost()
        self.messages.success(
            _(
                "Stock Adjustment {} was successfully created.".format(
                    adjustment_form.instance
                )
            )
        )
        return HttpResponseRedirect(self.get_success_url())
