import pytz
import rules

from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.generic import (
    TemplateView,
    View,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
    FormView,
)

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.db import transaction

from datetime import datetime
from decimal import Decimal

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    MessageMixin,
)
from formtools.wizard.views import SessionWizardView

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.core.import_export import SterlingImportMixin
from sterlingpos.catalogue.models import Category
from sterlingpos.catalogue.utils import uom_conversion
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    StockTransfer,
    StockCount,
    StockCountDetail,
    Batch,
    TRANSACTION_TYPE,
    BatchItem,
)
from sterlingpos.stocks.forms import (
    StockCountForm,
    StockCountDetailFormset,
    StockCountDetailFormsetHelper,
    StockCountCountingFormset,
    StockCountCountingFormsetHelper,
    BatchFormset,
    StockCountCompleteForm,
    StockCountProductForm,
)
from sterlingpos.catalogue.utils import get_normalized_decimal
from sterlingpos.stocks.resources import StockCountResource

from .mixin import PDFExportMixin
from .util import stock_count_has_batch_trakced


STEP_BATCH_TRACKED_FORM = "batch_tracked_form"
STEP_STOCK_COUNT_COMPLETE = "stock_count_complete_form"


class StockCountListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "code",
        "date_of_stock_count",
        "date_of_completion",
        "outlet",
        "status",
        "action",
        "pk",
    ]
    order_columns = [
        "code",
        "date_of_stock_count",
        "date_of_completion",
        "outlet__pk",
        "status",
        "action",
        "pk",
    ]
    datetime_col = ["date_of_stock_count", "date_of_completion"]
    model = StockCount
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.prefetch_related("counted_stock")
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(outlet__in=outlet_qs)
        return qs

    def render_column(self, row, column):
        if column == "code":
            action_data = {
                "name": row.code,
                "url": reverse_lazy(
                    "stocks:stocks_count_detail", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        if column == "action":
            action_data = {
                "id": row.id,
                "detail": reverse_lazy(
                    "stocks:stocks_count_detail", kwargs={"pk": row.pk}
                ),
                "edit": reverse_lazy(
                    "stocks:stocks_count_update", kwargs={"pk": row.pk}
                ),
                "delete": reverse_lazy(
                    "stocks:stocks_count_delete", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "date_of_stock_count":
            fmt = "%d/%m/%Y %H:%M"
            date = row.date_of_stock_count.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        elif column == "date_of_completion":
            fmt = "%d/%m/%Y %H:%M"
            if row.date_of_completion:
                date = row.date_of_completion.replace(tzinfo=pytz.UTC)
                localtz = date.astimezone(timezone.get_current_timezone())
                return localtz.strftime(fmt)
            return "-"
        elif column == "outlet":
            return row.outlet.name
        else:
            return super().render_column(row, column)

    def filter_status(self, search_value):
        if search_value == "show":
            return Q()
        return Q(status=search_value)

    def filter_outlet__pk(self, value):
        return Q(outlet__pk=value)


class StockCountListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_count_list.html"
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        return context


class StockCountDetailView(SterlingRoleMixin, PDFExportMixin, DetailView):
    template_name = "dashboard/stocks/stock_count.html"
    pdf_template = "dashboard/stocks/pdf/stock_count.html"
    model = StockCount
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super(StockCountDetailView, self).get_context_data(**kwargs)
        context["outlet_permission"] = self.get_outlet_permission()
        context["categories"] = Category.objects.all()
        return context

    def get_pdf_data(self):
        outlet_permission = self.get_outlet_permission()
        data = {
            "object": self.get_object(),
            "counted_stock": self.get_object()
            .counted_stock.all()
            .select_related(
                "stock", "stock__product", "stock__product__catalogue", "unit"
            ),
            "base_url": self.request.build_absolute_uri("/"),
            "logo": self.request.user.account.brand_logo,
            "account": self.request.user.account,
            "outlet_permission": outlet_permission,
        }
        return data


class StockCountDetailListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "stock.product.sku",
        "stock.product.catalogue.name",
        "stock.product.name",
        "stock.product.category.name",
        "quantity",
        "counted",
        "get_stocks_difference",
        "action",
    ]
    order_columns = [
        "stock__product__sku",
        "stock__product__catalogue__name",
        "stock__product__name",
        "stock__product__category",
        "quantity",
        "counted",
        "get_stocks_difference",
        "action",
    ]
    model = StockCountDetail
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.select_related(
            "stock_count",
            "stock",
            "stock__product",
            "stock__product__uom",
            "stock__product__catalogue",
            "stock__product__catalogue__uom",
            "stock__product__category",
            "unit",
        )
        qs = qs.filter(stock_count=self.kwargs["pk"])
        return qs

    def render_column(self, row, column):
        if column == "counted":
            return "{} {}".format(get_normalized_decimal(row.counted), row.unit.unit)
        elif column == "quantity":
            return "{} {}".format(
                get_normalized_decimal(row.quantity), row.stock.product.uom
            )
        elif column == "get_stocks_difference":
            return "{} {}".format(
                get_normalized_decimal(row.get_stocks_difference), row.stock.product.uom
            )
        else:
            return super().render_column(row, column)

    def render_action(self, obj):
        html = ""
        if obj.stock_count.status in ["in_progress", "pending"]:
            html = """
                <button class="btn btn-link stock-count-update" data-toggle="modal" 
                    data-target="#stock-count-modal" data-id="{}" data-stock-count="{}" data-url="{}">
                    <i class="fa fa-edit" aria-hidden="true"></i>
                </button>
            """.format(
                obj.id,
                obj.stock_count.id,
                reverse_lazy(
                    "stocks:stock_count_detail_counting",
                    kwargs={"stock_count_pk": obj.stock_count.id, "pk": obj.id},
                ),
            )
        return html

    def filter_stock__product__catalogue__name(self, value):
        return Q(stock__product__catalogue__name__icontains=value)

    def filter_stock__product__name(self, value):
        return Q(stock__product__name__icontains=value)

    def filter_stock__product__sku(self, value):
        return Q(stock__product__sku__icontains=value)

    def filter_stock__product__category(self, value):
        return Q(stock__product__category=value)


class StockCountDetailListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_count_detail_list.html"
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stock_count"] = StockCount.objects.get(pk=kwargs["pk"])
        context["categories"] = Category.objects.all()
        context["outlet_permission"] = self.get_outlet_permission()
        return context


class StockCountingUpdateView(
    SterlingRoleMixin, MessageMixin, JSONResponseMixin, UpdateView
):
    model = StockCountDetail
    form_class = StockCountProductForm
    template_name = "dashboard/stocks/stock_count_detail_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "pk": self.kwargs["pk"],
                "stock_count_pk": self.kwargs["stock_count_pk"],
                "table_id": "#tbl_stock_count",
                "product": self.object.stock.product,
            }
        )
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return self.render_json_response(self.get_error_result(form))
        return super().form_invalid(form)

    def get_error_result(self, form):
        return {"status": "error", "errors": form.errors}

    def get_form_valid_message(self):
        return _("<strong>%s</strong> was successfully updated!") % (
            self.object.stock.product.name,
        )

    def form_valid(self, form):
        form.instance.save()
        if self.request.is_ajax():
            return self.render_json_response(
                {
                    "status": "success",
                    "message": self.get_form_valid_message(),
                    "object_pk": self.object.pk,
                }
            )
        return super().form_valid(form)


class StockCountCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = "dashboard/stocks/stock_count_form.html"
    model = StockCount
    form_class = StockCountForm
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy("stocks:stocks_count_detail", kwargs={"pk": self.object.pk})

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super(StockCountCreateView, self).get_context_data(**kwargs)
        outlet_permission = self.get_outlet_permission()
        if "formset" not in context:
            context["formset"] = StockCountDetailFormset(
                form_kwargs={
                    "outlet_permission": outlet_permission,
                    "include_manufactured": True,
                    "include_sellable": True,
                }
            )
        context["formset_helper"] = StockCountDetailFormsetHelper
        return context

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        self.object = None
        outlet_permission = self.get_outlet_permission()
        form = self.get_form()
        formset = StockCountDetailFormset(
            self.request.POST,
            form_kwargs={
                "outlet_permission": outlet_permission,
                "include_manufactured": True
                if "include_manufactured" in self.request.POST
                else False,
                "include_sellable": True
                if "include_sellable" in self.request.POST
                else False,
            },
        )
        if form.is_valid():
            if request.POST.get("stock_count_type") == "full":
                return self.form_valid(form)
            else:
                if formset.is_valid():
                    return self.form_valid(form)
                else:
                    return self.form_invalid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form):
        form_params = self.request.POST or None
        outlet_permission = self.get_outlet_permission()
        formset = StockCountDetailFormset(
            form_params,
            instance=form.instance,
            form_kwargs={
                "outlet_permission": outlet_permission,
                "include_manufactured": True
                if "include_manufactured" in form_params
                else False,
                "include_sellable": True
                if "include_sellable" in form_params
                else False,
            },
        )
        with transaction.atomic():
            stock_count = form.save()
            if form_params.get("stock_count_type") == "full":
                stock_count.full_count_product_stock()
            else:
                if formset.is_valid():
                    for item in formset:
                        if item.has_changed():
                            deleted = item.cleaned_data.get("DELETE")
                            if not deleted:
                                item.save(commit=True)
        self.messages.success(
            _("Stock Count {} was successfully created.".format(form.instance))
        )
        return super(StockCountCreateView, self).form_valid(form)


class StockCountUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/stocks/stock_count_form.html"
    model = StockCount
    form_class = StockCountForm
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_queryset(self):
        qs = super().get_queryset().exclude(status__in=["canceled", "completed"])
        return qs

    def get_success_url(self):
        return reverse_lazy("stocks:stocks_count_detail", kwargs={"pk": self.object.pk})

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super(StockCountUpdateView, self).get_context_data(**kwargs)
        outlet_permission = self.get_outlet_permission()
        if self.object.stock_count_type == "partial":
            if "formset" not in context:
                context["formset"] = StockCountDetailFormset(
                    instance=self.object,
                    queryset=self.object.counted_stock.filter(
                        stock__product__archived=False
                    ),
                    form_kwargs={
                        "outlet_permission": outlet_permission,
                        "include_manufactured": self.object.include_manufactured,
                        "include_sellable": self.object.include_sellable,
                    },
                )
            context["formset_helper"] = StockCountDetailFormsetHelper
        return context

    def form_invalid(self, form, formset=None):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        outlet_permission = self.get_outlet_permission()
        form = self.get_form()
        if self.object.stock_count_type == "partial":
            formset = StockCountDetailFormset(
                self.request.POST,
                instance=self.object,
                form_kwargs={
                    "outlet_permission": outlet_permission,
                    "include_manufactured": self.object.include_manufactured,
                    "include_sellable": self.object.include_sellable,
                },
            )
        if form.is_valid():
            if self.object.stock_count_type == "partial":
                if formset.is_valid():
                    return self.form_valid(form)
                else:
                    return self.form_invalid(form, formset)
            else:
                return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form_params = self.request.POST or None
        outlet_permission = self.get_outlet_permission()
        if self.object.stock_count_type == "partial":
            formset = StockCountDetailFormset(
                form_params,
                instance=form.instance,
                form_kwargs={
                    "outlet_permission": outlet_permission,
                    "include_manufactured": self.object.include_manufactured,
                    "include_sellable": self.object.include_sellable,
                },
            )
        with transaction.atomic():
            form.instance.save()
            if self.object.stock_count_type == "partial":
                if formset.is_valid():
                    for delete_form in formset.deleted_forms:
                        if delete_form.instance and delete_form.instance.pk:
                            delete_form.instance.delete()

                    for item in formset:
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted:
                            item.save(commit=True)
            else:
                self.object.full_count_product_stock_update()

        self.messages.success(
            _("Stock Count {} was successfully updated.".format(form.instance))
        )
        return super(StockCountUpdateView, self).form_valid(form)


class StockCountImportTemplateView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_count_import.html"


class StockCountImportView(SterlingRoleMixin, SterlingImportMixin):
    template_name = "dashboard/stocks/stock_count_import_form.html"
    resource_class = StockCountResource
    file_headers = [
        "outlet",
        "sku",
        "product",
        "counted",
        "unit",
    ]
    success_url = reverse_lazy("stocks:stocks_count")


class StockCountCompleteCountingView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/stocks/stock_count_complete_counting.html"
    model = StockCount
    form_class = StockCountCompleteForm
    permission_name = "stock_count"
    success_url = reverse_lazy("stocks:stocks_count")

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["outlet_permission"] = self.get_outlet_permission()
        return context

    def get_queryset(self):
        qs = super().get_queryset().exclude(status__in=["canceled", "completed"])
        return qs

    def form_valid(self, form):
        stock_count = form.save()
        stock_count.complete_full_counting(self.request.user)
        self.messages.success(
            _("Stock Count {} was successfully completed.".format(stock_count))
        )
        return super(StockCountCompleteCountingView, self).form_valid(form)


class StockCountCompletedView(SterlingRoleMixin, MessageMixin, SessionWizardView):
    template_name = {
        STEP_STOCK_COUNT_COMPLETE: "dashboard/stocks/stock_count_complete.html",
        STEP_BATCH_TRACKED_FORM: "dashboard/stocks/batch_tracked_form.html",
    }
    form_list = [
        (STEP_STOCK_COUNT_COMPLETE, StockCountCompleteForm),
        (STEP_BATCH_TRACKED_FORM, BatchFormset),
    ]
    condition_dict = {STEP_BATCH_TRACKED_FORM: stock_count_has_batch_trakced}
    permission_name = "stock_count"

    def __init__(self, *args, **kwargs):
        super(StockCountCompletedView, self).__init__(*args, **kwargs)
        self.batched_products = []

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_template_names(self):
        return [self.template_name[self.steps.current]]

    def get_context_data(self, form, **kwargs):
        context = super(StockCountCompletedView, self).get_context_data(
            form=form, **kwargs
        )
        if self.steps.current == STEP_STOCK_COUNT_COMPLETE:
            context["object"] = self.get_form_instance(STEP_STOCK_COUNT_COMPLETE)

        if self.steps.current == STEP_BATCH_TRACKED_FORM:
            context["inventory_type"] = TRANSACTION_TYPE.stock_count
            instance = self.get_form_instance(STEP_STOCK_COUNT_COMPLETE)
            context["outlet_pk"] = instance.outlet.pk
            context["outlet"] = instance.outlet
            context["status"] = TRANSACTION_TYPE.stock_count
        context["status_display"] = TRANSACTION_TYPE["stock_count"]
        return context

    def get_form_instance(self, step):
        if step == STEP_STOCK_COUNT_COMPLETE:
            if "pk" in self.kwargs:
                instance = StockCount.objects.get(pk=self.kwargs["pk"])
                return instance
            else:
                return super().get_form_instance(step)

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_count_detail", kwargs={"pk": self.kwargs["pk"]}
        )

    def get_form_initial(self, step):
        if step == STEP_BATCH_TRACKED_FORM:
            initial_data = []
            instance = self.get_form_instance(STEP_STOCK_COUNT_COMPLETE)
            for counted in instance.counted_stock.all():
                if (
                    counted.stock.product.is_batch_tracked
                    and counted.stock.product not in self.batched_products
                ):
                    self.batched_products.append(counted.stock.product)
                    initial_data.append(
                        {
                            "product": counted.stock.product,
                            "product_name": counted.stock.product.name,
                            "quantity_to_assign": "{} {}".format(
                                get_normalized_decimal(Decimal(counted.counted)),
                                counted.unit,
                            ),
                            "total_quantity": counted.counted,
                            "unit": counted.unit.pk,
                        }
                    )
            return self.instance_dict.get(step, initial_data)
        return super().get_form_initial(step)

    def get_form(self, step=None, data=None, files=None):
        form = super(StockCountCompletedView, self).get_form(step, data, files)
        if step == STEP_BATCH_TRACKED_FORM:
            form.extra = len(self.batched_products)
        return form

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step)
        if step == STEP_BATCH_TRACKED_FORM:
            instance = self.get_form_instance(STEP_STOCK_COUNT_COMPLETE)
            kwargs["status"] = TRANSACTION_TYPE.stock_count
            kwargs["outlet"] = instance.outlet
        return kwargs

    def get_next_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return self.steps.first
        key = keys.index(step) + 1
        if len(keys) > key:
            return keys[key]
        return None

    def get_prev_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return None
        key = keys.index(step) - 1
        if key >= 0:
            return keys[key]
        return None

    def post(self, *args, **kwargs):
        if self.steps.current not in self.steps.all:
            form = self.get_form(data=self.request.POST, files=self.request.FILES)
            return self.render_done(form, **kwargs)
        return super().post(*args, **kwargs)

    def done(self, form_list, **kwargs):
        with transaction.atomic():
            stock_count_complete = list(form_list)[0]
            stock_count_complete.save()
            stock_count_complete.instance.process_adjustment(self.request.user)
            outlet = stock_count_complete.instance.outlet

            for counted in (
                stock_count_complete.instance.counted_stock.filter(
                    stock__product__is_batch_tracked=True
                )
                .select_related("stock", "stock__product")
                .distinct()
            ):
                batches = counted.stock.product.batches.all().prefetch_related(
                    "batch_items"
                )
                for batch in batches:
                    batch_outlet = batch.batch_items.filter(outlet=outlet)
                    if batch_outlet:
                        batch_outlet = batch_outlet.first()
                        if counted.counted == 0:
                            batch_outlet.delete()
                    if batch.balance == 0:
                        batch.delete()

            if len(form_list) > 1:
                batch_formset = list(form_list)[1]
                for batch_form in batch_formset:
                    batches = []
                    for item in batch_form.nested:
                        if item.is_valid():
                            batch = None
                            deleted = item.cleaned_data.get("DELETE")
                            batch_id = item.cleaned_data.get("batch_id")
                            quantity = item.cleaned_data.get("quantity")
                            unit = item.cleaned_data.get("unit")
                            converted_qty = uom_conversion(
                                unit, batch_form.instance.product.uom, quantity
                            )

                            if not deleted:
                                batch, created = Batch.objects.get_or_create(
                                    batch_id=batch_id,
                                    product=batch_form.instance.product,
                                )

                                if created:
                                    expiration_date = datetime.strptime(
                                        item.cleaned_data.get("expiration_date"),
                                        "%d/%m/%Y",
                                    )
                                    batch.expiration_date = expiration_date
                                    batch.save()

                                    batch_item = item.save(commit=False)
                                    batch_item.batch = batch
                                    batch_item.outlet = outlet
                                    batch_item.save()
                                    batch_item.set_batch_balance(
                                        converted_qty,
                                        batch_form.instance.product.uom,
                                        stock_count_complete.instance,
                                        TRANSACTION_TYPE.stock_count,
                                    )
                                else:
                                    (
                                        batch_item,
                                        created,
                                    ) = batch.batch_items.get_or_create(outlet=outlet)
                                    batch_item.set_batch_balance(
                                        converted_qty,
                                        batch_form.instance.product.uom,
                                        stock_count_complete.instance,
                                        TRANSACTION_TYPE.stock_count,
                                    )
                            batches.append(batch.pk)

                    deleted_batch = Batch.objects.filter(
                        product=batch_form.instance.product,
                        batch_items__outlet=outlet,
                    ).exclude(pk__in=batches)
                    for deleted in deleted_batch:
                        try:
                            deleted.batch_items.get(outlet=outlet).delete()
                        except BatchItem.DoesNotExist:
                            pass
                        if deleted.balance == 0:
                            deleted.delete()
        self.messages.success(
            _(
                "Stock Count {} was successfully completed.".format(
                    stock_count_complete.instance
                )
            )
        )
        return HttpResponseRedirect(self.get_success_url())


class StockCountBatchTrackedView(SterlingRoleMixin, MessageMixin, FormView):
    form_class = BatchFormset
    success_url = reverse_lazy("stocks:stocks_count")
    template_name = "dashboard/stocks/stock_batch_tracked_form.html"
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def __init__(self, **kwargs):
        super(StockCountBatchTrackedView, self).__init__(**kwargs)
        self.object = None
        self.oultet = None

    def dispatch(self, request, *args, **kwargs):
        obj_pk = self.request.resolver_match.kwargs["pk"]
        self.object = StockCount.objects.get(pk=obj_pk)
        self.outlet = self.object.outlet
        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.extra = len(
            self.object.counted_stock.filter(stock__product__is_batch_tracked=True)
        )
        return form

    def get_context_data(self, **kwargs):
        context = super(StockCountBatchTrackedView, self).get_context_data(**kwargs)
        context["inventory_type"] = "stock_count"
        context["object"] = self.object
        context["outlet_pk"] = self.outlet.pk
        context["outlet"] = self.outlet
        context["status"] = TRANSACTION_TYPE.stock_count
        context["status_display"] = TRANSACTION_TYPE["stock_count"]
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["status"] = TRANSACTION_TYPE.stock_count
        kwargs["outlet"] = self.outlet
        return kwargs

    def get_initial(self):
        initial = []
        for counted_stock in self.object.counted_stock.all():
            if counted_stock.stock.product.is_batch_tracked:
                initial.append(
                    {
                        "product": counted_stock.stock.product,
                        "product_name": counted_stock.stock.product.name,
                        "quantity_to_assign": "{} {}".format(
                            get_normalized_decimal(counted_stock.counted),
                            counted_stock.unit,
                        ),
                        "total_quantity": counted_stock.counted,
                        "unit": counted_stock.unit.pk,
                    }
                )
        return initial

    def form_valid(self, form):
        self.object.process_adjustment(self.request.user)
        outlet = self.object.outlet
        if (
            len(self.object.counted_stock.filter(stock__product__is_batch_tracked=True))
            > 0
        ):
            for batch_form in form:
                batches = []
                for item in batch_form.nested:
                    if not item.is_valid():
                        continue

                    batch = None
                    deleted = item.cleaned_data.get("DELETE")
                    batch_id = item.cleaned_data.get("batch_id")
                    quantity = item.cleaned_data.get("quantity")
                    unit = item.cleaned_data.get("unit")
                    converted_qty = uom_conversion(
                        unit, batch_form.instance.product.uom, quantity
                    )
                    if not deleted:
                        batch, created = Batch.objects.get_or_create(
                            batch_id=batch_id, product=batch_form.instance.product
                        )

                        if created:
                            expiration_date = datetime.strptime(
                                item.cleaned_data.get("expiration_date"), "%d/%m/%Y"
                            )
                            batch.expiration_date = expiration_date
                            batch.save()

                            batch_item = item.save(commit=False)
                            batch_item.batch = batch
                            batch_item.outlet = outlet
                            batch_item.save()
                            batch_item.set_batch_balance(
                                converted_qty,
                                batch_form.instance.product.uom,
                                self.object,
                                TRANSACTION_TYPE.stock_count,
                            )
                        else:
                            batch_item, created = batch.batch_items.get_or_create(
                                outlet=outlet
                            )
                            batch_item.set_batch_balance(
                                converted_qty,
                                batch_form.instance.product.uom,
                                self.object,
                                TRANSACTION_TYPE.stock_count,
                            )

                    batches.append(batch.pk)

            deleted_batch = Batch.objects.filter(
                product=batch_form.instance.product,
                batch_items__outlet=outlet,
            ).exclude(pk__in=batches)
            for deleted in deleted_batch:
                try:
                    deleted.batch_items.get(outlet=outlet).delete()
                except BatchItem.DoesNotExist:
                    pass
                if deleted.balance == 0:
                    deleted.delete()

        self.messages.success(
            _("Stock Count {} was successfully completed.".format(self.object))
        )
        return super().form_valid(form)


class StockCountCompleteView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, MessageMixin, View
):
    def get_object(self):
        obj = get_object_or_404(
            StockCount, pk=self.kwargs.get("pk"), status__in=["pending", "in_progress"]
        )
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}

        instance = self.get_object()

        batches_count = 0
        for counted_stock in instance.counted_stock.all():
            if counted_stock.stock.product.is_batch_tracked:
                batches_count += counted_stock.counted

        response["status"] = "success"
        if batches_count > 0:
            response["data"]["has_batch_tracked"] = True
        else:
            instance.process_adjustment(self.request.user)
            for counted_stock in instance.counted_stock.all():
                if counted_stock.stock.product.is_batch_tracked:
                    batches = counted_stock.stock.product.batches.all()
                    for batch in batches:
                        try:
                            batch_item = batch.batch_items.get(outlet=instance.outlet)
                            if counted_stock.counted > 0:
                                batch_item.set_batch_balance(
                                    counted_stock.counted,
                                    counted_stock.stock.product.uom,
                                    instance,
                                    TRANSACTION_TYPE.stock_adjustment,
                                )
                            else:
                                batch_item.delete()
                        except BatchItem.DoesNotExist:
                            pass
                        if batch.balance == 0:
                            batch.delete()

            response["data"]["has_batch_tracked"] = False
            response["data"] = mark_safe(
                "Stock Count {} has been completed".format(instance.code)
            )
            self.messages.success(
                _("Stock Count {} was successfully completed.".format(instance))
            )

        return self.render_json_response(response, status=200)


class StockCountCountingView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/stocks/stock_count_counting.html"
    model = StockCount
    form_class = StockCountCountingFormset
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_queryset(self):
        qs = super().get_queryset().exclude(status__in=["canceled", "completed"])
        return qs

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_count_detail", kwargs={"pk": self.get_object().pk}
        )

    def get_context_data(self, **kwargs):
        context = super(StockCountCountingView, self).get_context_data(**kwargs)
        context["formset_helper"] = StockCountCountingFormsetHelper
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["queryset"] = self.object.counted_stock.all()
        return kwargs

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_form(self, form_class=None):
        form_class = super().get_form(form_class)
        form_class.form_kwargs.update(
            {"outlet_permission": self.get_outlet_permission()}
        )
        return form_class

    def form_valid(self, form):
        response = super().form_valid(form)
        obj = self.get_object()
        obj.status = StockCount.STATUS.in_progress
        obj.save()
        for counted in form:
            unit = counted.cleaned_data.get("unit")
            sc = counted.instance
            sc.unit = unit
            sc.save()
        self.messages.success(_("Stock Count {} was successfully counted.".format(obj)))
        return response


class StockCountDeleteView(SterlingRoleMixin, MessageMixin, DeleteView):
    model = StockCount
    success_url = reverse_lazy("stocks:stocks_count")
    permission_name = "stock_count"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_queryset(self):
        qs = super().get_queryset().exclude(status__in=["canceled", "received"])
        return qs

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.status = StockTransfer.STATUS.canceled
        self.object.save()
        self.messages.warning(
            _("Stock Count {} was successfully deleted.".format(self.object))
        )
        return HttpResponseRedirect(self.get_success_url())
