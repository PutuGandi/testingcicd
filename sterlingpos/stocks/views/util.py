
import rules

from django.views.generic import (
    View,
    UpdateView,
)

from django.urls import reverse_lazy
from django.db.models import Q
from django.contrib import messages
from django.utils.translation import ugettext as _

from dal import autocomplete
from decimal import Decimal

from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    MessageMixin,
)

from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.catalogue.models import Product, UOM
from sterlingpos.catalogue.utils import uom_conversion
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    Stock,
    Supplier,
    PurchaseOrder,
    InventorySetting,
    PurchaseOrderReceive,
)
from sterlingpos.stocks.forms import InventorySettingForm
from sterlingpos.catalogue.utils import get_normalized_decimal


STEP_STOCK_ADJUSTMENT_FORM = "stock_adjustment_form"
STEP_PURCHASE_ORDER_RECEIVE_FORM = "purchase_order_receive_form"
STEP_STOCK_COUNT_COMPLETE = "stock_count_complete_form"


def has_batch_tracked(wizard):
    batched_products = []
    data = wizard.storage.get_step_data(STEP_STOCK_ADJUSTMENT_FORM)
    if data:
        total_products = data.get("adjusted_stock-TOTAL_FORMS")
        for i in range(int(total_products)):
            product_id = data.get("adjusted_stock-" + str(i) + "-product")
            quantity = data.get("adjusted_stock-" + str(i) + "-quantity")
            deleted = data.get("adjusted_stock-" + str(i) + "-DELETE")
            if not deleted and product_id and quantity:
                try:
                    product = Product.objects.get(pk=product_id)
                    if (
                        product.is_batch_tracked
                        and Decimal(quantity) > 0
                        and product not in batched_products
                    ):
                        batched_products.append(product)
                except Product.DoesNotExist:
                    pass

    if len(batched_products) > 0:
        return True
    return False


def purchase_order_has_batch_tracked(wizard):
    batched_products = []
    data = wizard.storage.get_step_data(STEP_PURCHASE_ORDER_RECEIVE_FORM)
    if data:
        purchase_order = PurchaseOrder.objects.get(pk=wizard.kwargs.get("pk"))
        for i, stock in enumerate(
            purchase_order.purchased_stock.filter(is_received=False)
        ):
            product_id = data.get(
                "purchase_order_receive_form-" + str(i) + "-product_id"
            )
            quantity = data.get("purchase_order_receive_form-" + str(i) + "-quantity")
            try:
                product = Product.objects.get(pk=product_id)
                if (
                    product.is_batch_tracked
                    and product not in batched_products
                    and Decimal(quantity) > 0
                ):
                    batched_products.append(batched_products)
            except Product.DoesNotExist:
                pass

    if len(batched_products) > 0:
        return True
    return False


def received_order_has_batch_tracked(wizard):
    batched_products = []
    try:
        for received in wizard.object_detail:
            if (
                received.stock.product.is_batch_tracked
                and received.quantity > 0
                and received.stock.product not in batched_products
            ):
                batched_products.append(received.stock.product)
    except PurchaseOrderReceive.DoesNotExist:
        pass
    if len(batched_products) > 0:
        return True
    return False


def stock_count_has_batch_trakced(wizard):
    has_batch_tracked = False
    instance = wizard.get_form_instance(STEP_STOCK_COUNT_COMPLETE)
    for counted_stock in instance.counted_stock.all():
        if counted_stock.stock.product.is_batch_tracked and counted_stock.counted > 0:
            has_batch_tracked = True
    return has_batch_tracked


class ProductReceivedAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Product.objects.none()
        purchase_order = self.forwarded.get("purchase_order")
        if purchase_order:
            qs = Product.objects.filter(
                stocks__purchased__purchase_order=purchase_order,
                archived=False,
                is_manage_stock=True,
                uom__isnull=False,
            )

        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(catalogue__name__icontains=self.q)
                | Q(sku__icontains=self.q)
            )
        return qs

    def get_result_label(self, item):
        return "{} - {} - {}".format(item.catalogue.name, item.name, item.sku)


class UnitProductAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = UOM.objects.none()
        product = self.forwarded.get("product")
        try:
            product = Product.objects.get(pk=product)
        except Product.DoesNotExist:
            product = None
        if product:
            qs = UOM.objects.filter(category=product.uom.category)

        if self.q:
            qs = qs.filter(unit__icontains=self.q)
        return qs

    def get_result_label(self, item):
        return item.unit


class InventorySettingUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    model = InventorySetting
    form_class = InventorySettingForm
    template_name = "dashboard/stocks/settings.html"
    permission_name = "inventory_setting"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_object(self, queryset=None):
        obj = self.request.user.account.inventorysetting_set.first()
        return obj

    def get_success_url(self):
        return reverse_lazy("stocks:stocks_settings_outlet")

    def form_valid(self, form):
        response = super(InventorySettingUpdateView, self).form_valid(form)
        messages.success(
            self.request, "{}".format(_("Inventory settings has been updated."))
        )
        return response


class StockQuantityConversion(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            product_pk = request.GET.get("product", "")
            quantity = request.GET.get("quantity", "")
            unit = request.GET.get("unit", "")
            uom = UOM.objects.get(pk=unit)
            product = Product.objects.get(pk=product_pk)
            converted_qty = uom_conversion(uom, product.uom, Decimal(quantity))
            response["result"]["converted_qty"] = get_normalized_decimal(converted_qty)
            response["result"]["unit"] = product.uom.unit
            response["status"] = "success"
            status_code = 200
        except (UOM.DoesNotExist, Product.DoesNotExist, ValueError):
            pass

        return self.render_json_response(response, status=status_code)


class StockQuantityView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            product_pk = request.GET.get("product", "")
            outlet_pk = request.GET.get("outlet", "")
            product = Product.objects.get(pk=product_pk)
            if product.uom:
                stocks = product.stocks.all().get(outlet__pk=outlet_pk)
                response["result"]["balance"] = get_normalized_decimal(stocks.balance)
                response["result"]["unit"] = product.uom.unit
            else:
                response["result"]["balance"] = "-"
                response["result"]["unit"] = ""
            response["status"] = "success"
            status_code = 200
        except (Product.DoesNotExist, Stock.DoesNotExist, ValueError):
            pass

        return self.render_json_response(response, status=status_code)


class ProductSupplierAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Product.objects.none()
        supplier = self.forwarded.get("supplier")
        try:
            supplier = Supplier.objects.get(pk=supplier)
        except Supplier.DoesNotExist:
            pass

        if supplier:
            if supplier.master_product_data:
                qs = Product.objects.filter(
                    supplier_detail__supplier=supplier,
                    archived=False,
                    is_manage_stock=True,
                    uom__isnull=False,
                )
            else:
                qs = Product.objects.filter(
                    is_manage_stock=True, uom__isnull=False, archived=False
                )
                qs = qs.exclude(
                    classification__in=[
                        Product.PRODUCT_CLASSIFICATION.composite,
                        Product.PRODUCT_CLASSIFICATION.non_inventory,
                        Product.PRODUCT_CLASSIFICATION.manufactured,
                    ]
                )

        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(catalogue__name__icontains=self.q)
                | Q(sku__icontains=self.q)
            )
        return qs

    def get_result_label(self, item):
        return "{} - {} - {}".format(item.catalogue.name, item.name, item.sku)


class OutletInventoryAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Outlet.objects.all()
        if not self.request.user.is_owner:
            qs = get_outlet_of_user(self.request.user)
        qs = qs.filter(archieved=False)

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs

    def get_result_label(self, item):
        return item.name


class OutletDestinationInventoryAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Outlet.objects.all()
        qs = qs.filter(archieved=False)

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs

    def get_result_label(self, item):
        return item.name
