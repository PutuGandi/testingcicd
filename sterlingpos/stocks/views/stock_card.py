import json
import pdb
from django.utils.safestring import mark_safe
from django.views.generic import (
    TemplateView,
    View,
)
from django.contrib.humanize.templatetags.humanize import intcomma

from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.db.models import F, Q, Sum, Value, Count
from django.db.models.functions import Coalesce
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils.text import slugify
from django.utils.html import escape

from datetime import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
)

from sterlingpos.core.mixins import DatatableMixins, CSVExportMixin
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.catalogue.models import Product, Category
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    Stock,
    StockCardProxy as StockCard,
)
from sterlingpos.stocks.forms import (
    StockBalanceForm,
)
from sterlingpos.catalogue.utils import get_normalized_decimal


class StockCardListJsonV4(
    SterlingRoleMixin, CSVExportMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    model = StockCard
    columns = [
        "sku",
        "catalogue_name",
        "product_name",
        "category_name",
        "start_stock",
        "stock_in",
        "stock_out",
        "stock_productions",
        "stock_sales",
        "last_stock",
    ]
    csv_header = [
        "ID",
        "SKU",
        "Produk",
        "Varian",
        "Kategori",
        "UOM",
        "Stock Awal",
        "Stock Masuk",
        "Operasi",
        "Produksi",
        "Penjualan",
        "Stock Akhir",
    ]
    csv_columns = [
        "id",
        "sku",
        "catalogue_name",
        "product_name",
        "category_name",
        "unit",
        "start_stock",
        "stock_in",
        "stock_out",
        "stock_productions",
        "stock_sales",
        "last_stock",
    ]

    def get_context_data(self, *args, **kwargs):
        context = {} 
        context["data"] = [
            [
                self.render_column(obj, column_name)
                for column_name in self.columns
            ]
            for obj in self.get_initial_queryset()
        ]
        return context

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        export_data = self.render_result(qs)
        return export_data

    def get_csv_filename(self):
        csv_filename = "Stock_card"
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get("outlet_pk"):
            outlet = get_object_or_404(outlet_qs, pk=self.kwargs.get("outlet_pk"))
            csv_filename = "{}_{}".format(csv_filename, slugify(outlet.name))

        if self.request.GET.get("start_date"):
            start_date = datetime.strptime(
                self.request.GET.get("start_date"), "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, start_date)

        if self.request.GET.get("end_date"):
            end_date = datetime.strptime(
                self.request.GET.get("end_date"), "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, end_date)

        return csv_filename

    def render_csv_column(self, row, column):
        if column == "start_stock":
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.start_stock)),
                row.unit,
            )
        elif column == "stock_in":
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.stock_in)),
                row.unit,
            )
        elif column == "stock_out":
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.stock_out)),
                row.unit,
            )
        elif column == "stock_sales":
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.stock_sales)),
                row.unit,
            )
        elif column == "last_stock":
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.last_stock)),
                row.unit,
            )
        else:
            return getattr(row, column)

    def get_initial_queryset(self):
        outlet_pk = self.kwargs.get("outlet_pk")
        outlet_qs = get_outlet_of_user(self.request.user)
        if outlet_pk:
            outlet = get_object_or_404(
                outlet_qs, pk=outlet_pk)
        else:
            outlet = None

        today = datetime.today().replace(
            hour=0, minute=0, second=0, microsecond=0)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = today + relativedelta(day=1)

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = today + relativedelta(day=1, months=+1, seconds=-1)

        if self.request.user.is_owner:
            qs = self.model.objects.stock_card_subquery(
                self.request.user.account,
                start_date,
                end_date,
                outlet,
            )
        else:
            qs = self.model.objects.stock_card_subquery(
                self.request.user.account,
                start_date,
                end_date,
                outlet,
                outlet_ids=outlet_qs.values_list('id', flat=True),
            )
        return qs

    def render_column(self, row, column):
        outlet_pk = self.kwargs.get("outlet_pk")
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = today + relativedelta(day=1)

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = today + relativedelta(day=1, months=+1, seconds=-1)

        query_string = ""
        if start_date and end_date:
            query_string = "?start_date={}&end_date={}".format(start_date, end_date)

        if column == "last_stock":
            if Decimal(row.last_stock) < 0:
                return "<span class='text-stock-out'>{} {}</span>".format(
                    get_normalized_decimal(row.last_stock),
                    row.unit,
                )
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.last_stock)),
                row.unit,
            )
        elif column == "start_stock":
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.start_stock)),
                row.unit,
            )
        elif column == "stock_sales":
            if Decimal(row.stock_sales) < 0:
                if outlet_pk:
                    return '<a href="{}{}" class="history-detail text-stock-out"><u>{} {}</u></a>'.format(
                        reverse_lazy(
                            "stocks:stock_sales_outlet",
                            kwargs={
                                "product_pk": row.id,
                                "outlet_pk": outlet_pk,
                            },
                        ),
                        query_string,
                        intcomma(get_normalized_decimal(row.stock_sales)),
                        row.unit,
                    )
                return '<a href="{}{}" class="history-detail text-stock-out"><u>{} {}</u></a>'.format(
                    reverse_lazy(
                        "stocks:stock_sales", kwargs={"product_pk": row.id}
                    ),
                    query_string,
                    intcomma(get_normalized_decimal(row.stock_sales)),
                    row.unit,
                )
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.stock_sales)),
                row.unit,
            )
        elif column == "stock_out":
            if Decimal(row.stock_out) < 0:
                if outlet_pk:
                    return '<a href="{}{}" class="history-detail text-stock-out"><u>{} {}</u></a>'.format(
                        reverse_lazy(
                            "stocks:stock_out_outlet",
                            kwargs={
                                "product_pk": row.id,
                                "outlet_pk": outlet_pk,
                            },
                        ),
                        query_string,
                        intcomma(get_normalized_decimal(row.stock_out)),
                        row.unit,
                    )
                return '<a href="{}{}" class="history-detail text-stock-out"><u>{} {}</u></a>'.format(
                    reverse_lazy(
                        "stocks:stock_out", kwargs={"product_pk": row.id}
                    ),
                    query_string,
                    intcomma(get_normalized_decimal(row.stock_out)),
                    row.unit,
                )
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.stock_out)),
                row.unit,
            )
        elif column == "stock_in":
            if Decimal(row.stock_in) > 0:
                if outlet_pk:
                    return '<a href="{}{}" class="history-detail text-stock-in"><u>{} {}</u></a>'.format(
                        reverse_lazy(
                            "stocks:stock_in_outlet",
                            kwargs={
                                "product_pk": row.id,
                                "outlet_pk": outlet_pk,
                            },
                        ),
                        query_string,
                        intcomma(get_normalized_decimal(row.stock_in)),
                        row.unit,
                    )
                return '<a href="{}{}" class="history-detail text-stock-in"><u>{} {}</u></a>'.format(
                    reverse_lazy(
                        "stocks:stock_in", kwargs={"product_pk": row.id}
                    ),
                    query_string,
                    intcomma(get_normalized_decimal(row.stock_in)),
                    row.unit,
                )
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.stock_in)),
                row.unit,
            )
        elif column == "stock_productions":
            if Decimal(row.stock_productions) < 0:
                if outlet_pk:
                    return '<a href="{}{}" class="history-detail text-stock-out"><u>{} {}</u></a>'.format(
                        reverse_lazy(
                            "stocks:stock_productions_outlet",
                            kwargs={
                                "product_pk": row.id,
                                "outlet_pk": outlet_pk,
                            },
                        ),
                        query_string,
                        intcomma(get_normalized_decimal(row.stock_productions)),
                        row.unit,
                    )
                return '<a href="{}{}" class="history-detail text-stock-out"><u>{} {}</u></a>'.format(
                    reverse_lazy(
                        "stocks:stock_productions",
                        kwargs={"product_pk": row.id},
                    ),
                    query_string,
                    intcomma(get_normalized_decimal(row.stock_productions)),
                    row.unit,
                )
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.stock_productions)),
                row.unit,
            )
        else:
            return getattr(row, column)

    def get_ajax(self, *args, **kwargs):
        context = self.get_context_data(*args, **kwargs)
        return self.render_json_response(context)


class StockCardListV4(
    SterlingRoleMixin, TemplateView
):
    template_name = "dashboard/stocks/stock_card_subquery.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        context["start_date"] = today + relativedelta(day=1)
        context["end_date"] = today + relativedelta(day=1, months=+1, days=-1)
        context["categories"] = Category.objects.all().exclude(
            classification__in=["Composite"]
        )
        return context


class ProductStockDetailJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ["outlet.name", "product.sku", "product_price", "quantity", "action"]
    order_columns = [
        "outlet__name",
        "product__sku",
        "product__price",
        "quantity",
        "action",
    ]
    model = Stock

    def get_initial_queryset(self):
        product_pk = self.kwargs.get("product_pk")
        qs = super(ProductStockDetailJson, self).get_initial_queryset()
        qs = (
            qs.filter(product__pk=product_pk)
            .select_related("product", "outlet")
            .prefetch_related("transactions")
            .annotate(
                product_price=F("product__price"),
                quantity=Coalesce(Sum("transactions__quantity"), Value(0)),
            )
        )
        return qs

    def render_column(self, row, column):
        if column == "outlet.name":
            action_data = {
                "name": escape(row.outlet.name),
                "url": reverse_lazy(
                    "stocks:stocks_transaction",
                    kwargs={"product_pk": row.product.pk, "outlet_pk": row.outlet.pk},
                ),
            }
            return action_data
        if column == "action":
            action_data = {
                "id": row.pk,
                "url": reverse_lazy(
                    "stocks:stocks_update", kwargs={"stock_pk": row.pk}
                ),
            }
            return action_data
        else:
            return super(ProductStockDetailJson, self).render_column(row, column)


class ProductStockDetailView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_list.html"

    def get_context_data(self, **kwargs):
        context = super(ProductStockDetailView, self).get_context_data(**kwargs)
        context["product"] = get_object_or_404(
            Product, pk=self.kwargs.get("product_pk")
        )
        return context


class ProductStockUpdateView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_object(self):
        obj = get_object_or_404(Stock, pk=self.kwargs.get("stock_pk"))
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}

        form = StockBalanceForm(
            data=request.POST, instance=self.get_object(), user=request.user
        )

        if form.data.get("action") == "add_balance":
            stock_action = _("Added")
        else:
            stock_action = _("Set")

        if form.is_valid():
            stock = form.save()
            status_code = 200
            response["status"] = "success"
            response["data"] = mark_safe(
                "{} {} {} {} {} {} {}".format(
                    _("Successfully"),
                    stock_action,
                    _("stock balance"),
                    stock.product.sku,
                    stock.outlet.name,
                    _("by"),
                    form.data.get("quantity"),
                )
            )
        else:
            response["data"] = form.errors
            status_code = 400
            messages.error(
                self.request,
                "{} {} {} {} {} {} {}".format(
                    _("Cannot"),
                    stock_action,
                    _("stock balance"),
                    self.get_object().product.sku,
                    self.get_object().outlet.name,
                    _("by"),
                    form.data.get("quantity"),
                ),
            )
        return self.render_json_response(response, status=status_code)


class StockSalesTransactionListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/transaction_stock_sales_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["product_pk"] = self.kwargs.get("product_pk")
        context["outlet_pk"] = self.kwargs.get("outlet_pk")
        if context["outlet_pk"]:
            context["outlet"] = Outlet.objects.get(pk=context["outlet_pk"])
        context["start_date"] = self.request.GET.get("start_date", None)
        context["end_date"] = self.request.GET.get("end_date", None)
        return context
