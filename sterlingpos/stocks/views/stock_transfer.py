import pytz
import rules

from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.generic import (
    TemplateView,
    View,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
    FormView,
)

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
from decimal import Decimal

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    MessageMixin,
)
from formtools.wizard.views import SessionWizardView

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.core.import_export import SterlingImportMixin

from sterlingpos.catalogue.utils import uom_conversion
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    StockTransfer,
    Batch,
    TRANSACTION_TYPE,
)
from sterlingpos.stocks.forms import (
    StockTransferForm,
    StockTransferDetailFormset,
    StockTransferDetailFormsetHelper,
    BatchFormset,
    StockTransferReceivedForm,
)
from sterlingpos.catalogue.utils import get_normalized_decimal
from sterlingpos.stocks.resources import StockTransferResource

from .mixin import PDFExportMixin


STEP_BATCH_TRACKED_FORM = "batch_tracked_form"
STEP_STOCK_TRANSFER_RECEIVED = "stock_transfer_received_form"


class StockTransferListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "code",
        "date_of_transfer",
        "date_of_arrival",
        "source",
        "destination",
        "status",
        "action",
        "pk",
    ]
    order_columns = [
        "code",
        "date_of_transfer",
        "date_of_arrival",
        "source__pk",
        "destination__pk",
        "status",
        "action",
        "pk",
    ]
    datetime_col = ["date_of_transfer", "date_of_arrival"]
    permission_name = "stock_transfer"
    model = StockTransfer

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.prefetch_related("transfered_stock")

        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(Q(source__in=outlet_qs) | Q(destination__in=outlet_qs))
        return qs

    def render_column(self, row, column):
        if column == "code":
            action_data = {
                "name": row.code,
                "url": reverse_lazy(
                    "stocks:stocks_transfer_detail", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "action":
            action_data = {
                "id": row.id,
                "detail": reverse_lazy(
                    "stocks:stocks_transfer_detail", kwargs={"pk": row.pk}
                ),
                "edit": reverse_lazy(
                    "stocks:stocks_transfer_update", kwargs={"pk": row.pk}
                ),
                "delete": reverse_lazy(
                    "stocks:stocks_transfer_delete", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "source":
            return row.source.name
        elif column == "destination":
            return row.destination.name
        elif column == "date_of_transfer":
            fmt = "%d/%m/%Y %H:%M"
            date = row.date_of_transfer.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        elif column == "date_of_arrival":
            fmt = "%d/%m/%Y %H:%M"
            if row.date_of_arrival:
                date = row.date_of_arrival.replace(tzinfo=pytz.UTC)
                localtz = date.astimezone(timezone.get_current_timezone())
                return localtz.strftime(fmt)
            return "-"
        else:
            return super().render_column(row, column)

    def filter_status(self, search_value):
        if search_value == "show":
            return Q()
        return Q(status=search_value)

    def filter_source__pk(self, value):
        return Q(source__pk=value)

    def filter_destination__pk(self, value):
        return Q(destination__pk=value)


class StockTransferListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_transfer_list.html"
    permission_name = "stock_transfer"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        return context


class StockTransferDetailView(SterlingRoleMixin, PDFExportMixin, DetailView):
    template_name = "dashboard/stocks/stock_transfer.html"
    pdf_template = "dashboard/stocks/pdf/stock_transfer.html"
    model = StockTransfer
    permission_name = "stock_transfer"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super(StockTransferDetailView, self).get_context_data(**kwargs)
        # can_received = True
        transfered_stock = self.object.transfered_stock.all().select_related(
            "stock_source",
            "stock_source__outlet",
            "stock_source__product",
            "stock_source__product__uom",
            "stock_source__product__catalogue",
            "unit",
            "stock_destination",
            "stock_destination__outlet",
            "stock_destination__product",
            "stock_destination__product__uom",
            "stock_destination__product__catalogue",
        )

        context["transfered_stock"] = transfered_stock
        context["outlet_qs"] = get_outlet_of_user(self.request.user)
        context["outlet_permission"] = self.get_outlet_permission()
        context["can_received_at"] = (
            True
            if self.object.date_of_transfer < datetime.now().replace(tzinfo=pytz.UTC)
            else False
        )
        return context

    def get_pdf_data(self):
        data = {
            "object": self.get_object(),
            "transfered_stock": self.get_object()
            .transfered_stock.all()
            .select_related(
                "stock_source",
                "stock_source__product",
                "stock_source__product__catalogue",
                "stock_destination",
                "unit",
            ),
            "base_url": self.request.build_absolute_uri("/"),
            "logo": self.request.user.account.brand_logo,
            "account": self.request.user.account,
            "outlet_qs": get_outlet_of_user(self.request.user),
        }
        return data


class StockTransferCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = "dashboard/stocks/stock_transfer_form.html"
    model = StockTransfer
    form_class = StockTransferForm
    permission_name = "stock_transfer"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_transfer_detail", kwargs={"pk": self.object.pk}
        )

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        outlet_permission = self.get_outlet_permission()
        if "formset" not in context:
            context["formset"] = StockTransferDetailFormset(
                form_kwargs={
                    "outlet_permission": outlet_permission,
                    "source_outlet": None,
                }
            )
        context["formset_helper"] = StockTransferDetailFormsetHelper
        return context

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        outlet_permission = self.get_outlet_permission()
        formset = StockTransferDetailFormset(
            self.request.POST,
            form_kwargs={
                "outlet_permission": outlet_permission,
                "source_outlet": request.POST.get("source"),
            },
        )
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form):
        form_params = self.request.POST or None
        outlet_permission = self.get_outlet_permission()
        formset = StockTransferDetailFormset(
            form_params,
            instance=form.instance,
            form_kwargs={
                "outlet_permission": outlet_permission,
                "source_outlet": self.request.POST.get("source"),
            },
        )
        with transaction.atomic():
            self.object = form.instance.save()
            for item in formset:
                if item.is_valid() and item.has_changed():
                    deleted = item.cleaned_data.get("DELETE")
                    if not deleted:
                        item.save(commit=True)

        self.messages.success(
            _("Stock Transfer {} was successfully created.".format(form.instance))
        )
        return super().form_valid(form)


class StockTransferUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/stocks/stock_transfer_form.html"
    model = StockTransfer
    form_class = StockTransferForm
    permission_name = "stock_transfer"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        can_update = self.get_object().source in get_outlet_of_user(user)
        return has_permission and can_update

    def get_queryset(self):
        qs = (
            super(StockTransferUpdateView, self)
            .get_queryset()
            .exclude(status__in=["canceled", "received"])
        )
        return qs

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_transfer_detail", kwargs={"pk": self.object.pk}
        )

    def get_outlet_permission(self):
        permission = []
        for outlet in Outlet.objects.exclude(
            subscription__subscription_plan__slug="mobile-lite"
        ):
            if outlet in get_outlet_of_user(self.request.user):
                permission.append(True)
            else:
                permission.append(False)
        if False in permission:
            return False
        return True

    def get_context_data(self, **kwargs):
        context = super(StockTransferUpdateView, self).get_context_data(**kwargs)
        outlet_permission = self.get_outlet_permission()
        if "formset" not in context:
            context["formset"] = StockTransferDetailFormset(
                instance=self.object,
                queryset=self.object.transfered_stock.filter(
                    stock_source__product__archived=False
                ),
                form_kwargs={
                    "outlet_permission": outlet_permission,
                    "source_outlet": self.object.source,
                },
            )
        context["formset_helper"] = StockTransferDetailFormsetHelper
        return context

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        outlet_permission = self.get_outlet_permission()
        form = self.get_form()
        formset = StockTransferDetailFormset(
            self.request.POST,
            instance=self.object,
            form_kwargs={
                "outlet_permission": outlet_permission,
                "source_outlet": self.object.source,
            },
        )
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form):
        form_params = self.request.POST or None
        outlet_permission = self.get_outlet_permission()
        formset = StockTransferDetailFormset(
            form_params,
            instance=form.instance,
            form_kwargs={
                "outlet_permission": outlet_permission,
                "source_outlet": self.object.source,
            },
        )

        with transaction.atomic():
            self.object = form.instance.save()

            if formset.is_valid():
                for delete_form in formset.deleted_forms:
                    if delete_form.instance and delete_form.instance.pk:
                        delete_form.instance.delete()

                for item in formset:
                    deleted = item.cleaned_data.get("DELETE")
                    if not deleted:
                        item.save(commit=True)

        self.messages.success(
            _("Stock Transfer {} was successfully updated.".format(form.instance))
        )
        return super(StockTransferUpdateView, self).form_valid(form)


class StockTransferDeleteView(SterlingRoleMixin, MessageMixin, DeleteView):
    success_url = reverse_lazy("stocks:stocks_transfer")
    model = StockTransfer
    permission_name = "stock_transfer"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_queryset(self):
        qs = super().get_queryset().exclude(status__in=["canceled", "received"])
        return qs

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.status = StockTransfer.STATUS.canceled
        self.object.save()
        self.messages.warning(
            _("Stock Trasnsfer {} was successfully deleted.".format(self.object))
        )
        return HttpResponseRedirect(self.get_success_url())


class StockTransferReceiveView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, MessageMixin, View
):
    permission_name = "stock_transfer"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_object(self):
        obj = get_object_or_404(
            StockTransfer, pk=self.kwargs.get("pk"), status="active"
        )
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}

        instance = self.get_object()
        has_batch_tracked = False
        for transfered_stock in instance.transfered_stock.all():
            if transfered_stock.stock_source.product.is_batch_tracked:
                has_batch_tracked = True

        response["status"] = "success"
        response["has_batch_tracked"] = has_batch_tracked
        if not has_batch_tracked:
            instance.received(self.request.user)
            response["has_batch_tracked"] = has_batch_tracked
            response["data"] = mark_safe(
                "Transfer Order {} has been received".format(instance.code)
            )
            self.messages.success(
                _("Stock Trasnsfer {} was successfully received.".format(instance))
            )
        else:
            response["data"]["has_batch_tracked"] = has_batch_tracked

        return self.render_json_response(response, status=200)


class StockTransferBatchTrackedView(SterlingRoleMixin, MessageMixin, FormView):
    form_class = BatchFormset
    success_url = reverse_lazy("stocks:stocks_transfer")
    template_name = "dashboard/stocks/stock_batch_tracked_form.html"
    permission_name = "stock_transfer"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def __init__(self, *args, **kwargs):
        super(StockTransferBatchTrackedView, self).__init__(*args, **kwargs)
        self.object = None
        self.oultet = None

    def dispatch(self, request, *args, **kwargs):
        obj_pk = self.request.resolver_match.kwargs["pk"]
        self.object = StockTransfer.objects.get(pk=obj_pk, status="active")
        self.outlet = self.object.source
        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.extra = len(
            self.object.transfered_stock.filter(
                stock_source__product__is_batch_tracked=True
            )
        )
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["inventory_type"] = "stock_transfer"
        context["object"] = self.object
        context["outlet_pk"] = self.outlet.pk
        context["outlet"] = self.outlet
        context["status"] = TRANSACTION_TYPE.transfer
        context["status_display"] = TRANSACTION_TYPE["transfer"]
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["status"] = TRANSACTION_TYPE.transfer
        kwargs["outlet"] = self.outlet
        return kwargs

    def get_initial(self):
        initial = []
        for transfered_stock in self.object.transfered_stock.all():
            if transfered_stock.stock_source.product.is_batch_tracked:
                initial.append(
                    {
                        "product": transfered_stock.stock_source.product,
                        "product_name": transfered_stock.stock_source.product.name,
                        "quantity_to_assign": "{} {}".format(
                            get_normalized_decimal(transfered_stock.quantity),
                            transfered_stock.unit,
                        ),
                        "total_quantity": transfered_stock.quantity,
                        "unit": transfered_stock.unit.pk,
                    }
                )
        return initial

    def form_valid(self, form):
        self.object.received(self.request.user)
        source_outlet = self.object.source
        destination_outlet = self.object.destination
        if (
            len(
                self.object.transfered_stock.filter(
                    stock_source__product__is_batch_tracked=True
                )
            )
            > 0
        ):
            for batch_form in form:
                for item in batch_form.nested:
                    if item.is_valid():
                        batch = None
                        deleted = item.cleaned_data.get("DELETE")
                        batch_id = item.cleaned_data.get("batch_id")
                        quantity = item.cleaned_data.get("quantity")
                        unit = item.cleaned_data.get("unit")
                        converted_qty = uom_conversion(
                            unit, batch_form.instance.product.uom, quantity
                        )
                        if not deleted:
                            batch = Batch.objects.filter(
                                batch_id=batch_id, product=batch_form.instance.product
                            )
                            batch = batch.first()
                            try:
                                batch_item_source = batch.batch_items.get(
                                    outlet=source_outlet
                                )
                                batch_item_source.save()
                                batch_item_source.substract_batch_balance(
                                    converted_qty,
                                    unit=batch_form.instance.product.uom,
                                    source_object=self.object,
                                    transaction_type=TRANSACTION_TYPE.transfer,
                                )
                            except ObjectDoesNotExist:
                                pass

                            try:
                                batch_item_dest = batch.batch_items.get(
                                    outlet=destination_outlet
                                )
                                batch_item_dest.save()
                                batch_item_dest.add_batch_balance(
                                    converted_qty,
                                    unit=batch_form.instance.product.uom,
                                    source_object=self.object,
                                    transaction_type=TRANSACTION_TYPE.transfer,
                                )
                            except ObjectDoesNotExist:
                                batch_item_dest = item.save(commit=False)
                                batch_item_dest.outlet = destination_outlet
                                batch_item_dest.batch = batch
                                batch_item_dest.save()
                                batch_item_dest.add_batch_balance(
                                    converted_qty,
                                    unit=batch_form.instance.product.uom,
                                    source_object=self.object,
                                    transaction_type=TRANSACTION_TYPE.transfer,
                                )

        self.messages.success(
            _("Stock Transfer {} was successfully completed.".format(self.object))
        )
        return super().form_valid(form)


def stock_transfer_has_batch_tracked(wizard):
    has_batch_tracked = False
    instance = wizard.get_form_instance(STEP_STOCK_TRANSFER_RECEIVED)
    for transfered_stock in instance.transfered_stock.all():
        if (
            transfered_stock.stock_source.product.is_batch_tracked
            and transfered_stock.quantity
        ):
            if transfered_stock.is_stock_available:
                has_batch_tracked = True
    return has_batch_tracked


class StockTransferReceivedView(SterlingRoleMixin, MessageMixin, SessionWizardView):
    template_name = {
        STEP_STOCK_TRANSFER_RECEIVED: "dashboard/stocks/stock_transfer_received.html",
        STEP_BATCH_TRACKED_FORM: "dashboard/stocks/batch_tracked_form.html",
    }
    form_list = [
        (STEP_STOCK_TRANSFER_RECEIVED, StockTransferReceivedForm),
        (STEP_BATCH_TRACKED_FORM, BatchFormset),
    ]
    condition_dict = {STEP_BATCH_TRACKED_FORM: stock_transfer_has_batch_tracked}
    permission_name = "stock_transfer"

    def __init__(self, *args, **kwargs):
        super(StockTransferReceivedView, self).__init__(*args, **kwargs)
        self.batched_products = []

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_template_names(self):
        return [self.template_name[self.steps.current]]

    def get_context_data(self, form, **kwargs):
        context = super(StockTransferReceivedView, self).get_context_data(
            form=form, **kwargs
        )
        if self.steps.current == STEP_STOCK_TRANSFER_RECEIVED:
            obj = self.get_form_instance(STEP_STOCK_TRANSFER_RECEIVED)
            context["object"] = obj
            context["transfered_stock"] = obj.transfered_stock.all().select_related(
                "stock_source",
                "stock_source__outlet",
                "stock_source__product",
                "stock_source__product__uom",
                "stock_source__product__catalogue",
                "unit",
                "stock_destination",
                "stock_destination__outlet",
                "stock_destination__product",
                "stock_destination__product__uom",
                "stock_destination__product__catalogue",
            )

        if self.steps.current == STEP_BATCH_TRACKED_FORM:
            context["inventory_type"] = TRANSACTION_TYPE.transfer
            instance = self.get_form_instance(STEP_STOCK_TRANSFER_RECEIVED)
            context["outlet_pk"] = instance.source.pk
            context["outlet"] = instance.source
            context["status"] = TRANSACTION_TYPE.transfer
            context["status_display"] = TRANSACTION_TYPE["transfer"]
        return context

    def get_form_instance(self, step):
        if step == STEP_STOCK_TRANSFER_RECEIVED:
            if "pk" in self.kwargs:
                instance = StockTransfer.objects.get(pk=self.kwargs["pk"])
                return instance
            else:
                return super().get_form_instance(step)

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_transfer_detail", kwargs={"pk": self.kwargs["pk"]}
        )

    def get_form_initial(self, step):
        if step == STEP_BATCH_TRACKED_FORM:
            initial_data = []
            instance = self.get_form_instance(STEP_STOCK_TRANSFER_RECEIVED)
            for transfered in instance.transfered_stock.all():
                if (
                    transfered.stock_source.product.is_batch_tracked
                    and transfered.stock_source.product not in self.batched_products
                    and transfered.quantity
                    and transfered.is_stock_available
                ):
                    self.batched_products.append(transfered.stock_source.product)
                    initial_data.append(
                        {
                            "product": transfered.stock_source.product,
                            "product_name": transfered.stock_source.product.name,
                            "quantity_to_assign": "{} {}".format(
                                get_normalized_decimal(Decimal(transfered.quantity)),
                                transfered.unit,
                            ),
                            "total_quantity": transfered.quantity,
                            "unit": transfered.unit.pk,
                        }
                    )
            return self.instance_dict.get(step, initial_data)
        return super().get_form_initial(step)

    def get_form(self, step=None, data=None, files=None):
        form = super(StockTransferReceivedView, self).get_form(step, data, files)
        if step == STEP_BATCH_TRACKED_FORM:
            form.extra = len(self.batched_products)
        return form

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step)
        if step == STEP_BATCH_TRACKED_FORM:
            instance = self.get_form_instance(STEP_STOCK_TRANSFER_RECEIVED)
            kwargs["status"] = TRANSACTION_TYPE.transfer
            kwargs["outlet"] = instance.source
        return kwargs

    def get_next_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return self.steps.first
        key = keys.index(step) + 1
        if len(keys) > key:
            return keys[key]
        return None

    def get_prev_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return None
        key = keys.index(step) - 1
        if key >= 0:
            return keys[key]
        return None

    def post(self, *args, **kwargs):
        if self.steps.current not in self.steps.all:
            form = self.get_form(data=self.request.POST, files=self.request.FILES)
            return self.render_done(form, **kwargs)
        return super().post(*args, **kwargs)

    def create_missing_batch(self, product):
        missing_name = "Missing Batch"
        missing_batch, _ = product.batches.get_or_create(
            is_missing_batch=True, batch_id=missing_name
        )
        return missing_batch

    def set_missing_batch(self, stock_transfer, source_outlet, destination_outlet):
        for transfered in stock_transfer.transfered_stock.all():
            if (
                transfered.stock_source.product.is_batch_tracked
                and transfered.stock_source.product not in self.batched_products
            ):
                missing_batch = self.create_missing_batch(
                    transfered.stock_source.product
                )
                source_batch_outlet, _ = missing_batch.batch_items.get_or_create(
                    outlet=source_outlet
                )
                destination_batch_outlet, _ = missing_batch.batch_items.get_or_create(
                    outlet=destination_outlet
                )
                converted_qty = uom_conversion(
                    transfered.unit,
                    transfered.stock_source.product.uom,
                    transfered.quantity,
                )
                source_batch_outlet.substract_batch_balance(
                    converted_qty,
                    unit=transfered.stock_source.product.uom,
                    source_object=stock_transfer,
                    transaction_type=TRANSACTION_TYPE.transfer,
                )
                destination_batch_outlet.add_batch_balance(
                    converted_qty,
                    unit=transfered.stock_source.product.uom,
                    source_object=stock_transfer,
                    transaction_type=TRANSACTION_TYPE.transfer,
                )

    def done(self, form_list, **kwargs):
        with transaction.atomic():
            stock_transfer_received = list(form_list)[0]
            stock_transfer = stock_transfer_received.save(commit=True)
            stock_transfer.received(self.request.user)
            source_outlet = stock_transfer.source
            destination_outlet = stock_transfer.destination

            if len(form_list) > 1:
                batch_formset = list(form_list)[1]
                for batch_form in batch_formset:
                    for item in batch_form.nested:
                        if item.is_valid():
                            batch = None
                            deleted = item.cleaned_data.get("DELETE")
                            batch_id = item.cleaned_data.get("batch_id")
                            quantity = item.cleaned_data.get("quantity")
                            unit = item.cleaned_data.get("unit")
                            converted_qty = uom_conversion(
                                unit, batch_form.instance.product.uom, quantity
                            )
                            if not deleted:
                                batch = Batch.objects.filter(
                                    batch_id=batch_id,
                                    product=batch_form.instance.product,
                                )
                                batch = batch.first()
                                try:
                                    batch_item_source = batch.batch_items.get(
                                        outlet=source_outlet
                                    )
                                    batch_item_source.save()
                                    batch_item_source.substract_batch_balance(
                                        converted_qty,
                                        unit=batch_form.instance.product.uom,
                                        source_object=stock_transfer,
                                        transaction_type=TRANSACTION_TYPE.transfer,
                                    )

                                    if batch_item_source.balance == 0:
                                        batch_item_source.delete()
                                except ObjectDoesNotExist:
                                    pass

                                try:
                                    batch_item_dest = batch.batch_items.get(
                                        outlet=destination_outlet
                                    )
                                    batch_item_dest.save()
                                    batch_item_dest.add_batch_balance(
                                        converted_qty,
                                        unit=batch_form.instance.product.uom,
                                        source_object=stock_transfer_received.instance,
                                        transaction_type=TRANSACTION_TYPE.transfer,
                                    )
                                except ObjectDoesNotExist:
                                    batch_item_dest = item.save(commit=False)
                                    batch_item_dest.outlet = destination_outlet
                                    batch_item_dest.batch = batch
                                    batch_item_dest.save()
                                    batch_item_dest.add_batch_balance(
                                        converted_qty,
                                        unit=batch_form.instance.product.uom,
                                        source_object=stock_transfer,
                                        transaction_type=TRANSACTION_TYPE.transfer,
                                    )
                        if batch:
                            if batch.balance == 0:
                                batch.delete()

        self.set_missing_batch(stock_transfer, source_outlet, destination_outlet)
        self.messages.success(
            _("Stock Transfer {} was successfully completed.".format(stock_transfer))
        )
        return HttpResponseRedirect(self.get_success_url())


class StockTransferImportTemplateView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_transfer_import.html"
    role_permission_name = "stock_transfer"


class StockTransferImportView(SterlingRoleMixin, SterlingImportMixin):
    template_name = "dashboard/stocks/stock_transfer_import_form.html"
    resource_class = StockTransferResource
    role_permission_name = "stock_transfer"
    file_headers = [
        "date_of_transfer",
        "source_outlet_code",
        "destination_outlet_code",
        "sku",
        "product",
        "quantity",
        "unit",
    ]
    success_url = reverse_lazy("stocks:stocks_transfer")
