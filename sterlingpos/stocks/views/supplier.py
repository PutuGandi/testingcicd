import rules
import csv

from django.views.generic import (
    TemplateView,
    View,
    CreateView,
    UpdateView,
    DeleteView,
)

from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.db.models import Q, Count
from django.utils.translation import ugettext as _
from django.db import transaction
from django.utils.html import escape
from django.http import HttpResponse

from dal import autocomplete

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    FormMessagesMixin,
    MessageMixin,
)

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.core.import_export import SterlingImportMixin

from sterlingpos.stocks.models import (
    Supplier,
    SupplierProductDetail,
    PurchaseOrder,
)
from sterlingpos.stocks.forms import (
    SupplierForm,
    SupplierProductDetailFormset,
    SupplierProductDetailFormsetHelper,
)
from sterlingpos.catalogue.utils import get_normalized_decimal
from sterlingpos.stocks.resources import SupplierResource


class SupplierListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ["name", "code", "contact", "email", "phone", "is_active"]
    order_columns = ["name", "code", "contact", "email", "phone", "is_active"]
    model = Supplier
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def render_column(self, row, column):
        if column == "name":
            action_data = {
                "name": escape(row.name),
                "url": reverse_lazy(
                    "stocks:stocks_supplier_update", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "is_active":
            if not row.is_active:
                return _("Inactive")
            return _("Active")
        else:
            return super().render_column(row, column)

    def filter_is_active(self, value):
        if value == "all":
            return Q()
        return Q(is_active=value)

    def get_supplier_master_data(self):
        master_data = []
        for supplier in self.get_initial_queryset().filter(master_product_data=True):
            for supplier_detail in supplier.supplier_detail.all():
                data = [
                    supplier_detail.supplier.code,
                    supplier_detail.supplier.name,
                    supplier_detail.product.sku,
                    supplier_detail.product.__str__(),
                    supplier_detail.quantity,
                    supplier_detail.unit.unit,
                    supplier_detail.cost.amount,
                ]
                master_data.append(data)
        return master_data

    def get(self, request, *args, **kwargs):
        if request.GET.get("export_file") == "csv":
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = 'attachment; filename="supplier.csv"'
            writer = csv.writer(response)
            writer.writerow(
                ["code", "name", "sku", "product", "quantity", "unit", "price"]
            )
            data = self.get_supplier_master_data()
            for item in data:
                writer.writerow(item)
            return response
        else:
            return super().get(request, *args, **kwargs)


class SupplierListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/supplier_list.html"
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission


class SupplierCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    model = Supplier
    form_class = SupplierForm
    template_name = "dashboard/stocks/supplier_form.html"
    success_url = reverse_lazy("stocks:stocks_supplier")
    message_info = _("created")
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["grid"] = "col-10"
        if "formset" not in context:
            context["formset"] = SupplierProductDetailFormset
        context["formset_helper"] = SupplierProductDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        formset = SupplierProductDetailFormset(request.POST)
        if form.is_valid():
            if "master_product_data" in request.POST:
                if formset.is_valid():
                    return self.form_valid(form)
                else:
                    return self.form_invalid(form, formset)
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        self.messages.error(
            _(
                """There was a problem creating this %s.
                Please review the fields marked red and resubmit."""
            )
            % self.model._meta.verbose_name
        )
        if "master_product_data" in self.request.POST:
            return self.render_to_response(
                self.get_context_data(form=form, formset=formset)
            )
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = SupplierProductDetailFormset(form_params, instance=form.instance)
        with transaction.atomic():
            supplier = form.save(commit=True)
            if formset.is_valid() and "master_product_data" in form_params:
                supplier.master_product_data = True
                for item in formset:
                    if item.has_changed():
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted:
                            item.save(commit=True)

        self.messages.success(
            "{} {} was was successfully {}!".format(
                self.model._meta.verbose_name.title(),
                supplier,
                self.message_info,
            )
        )
        return super().form_valid(form)


class SupplierUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    model = Supplier
    form_class = SupplierForm
    template_name = "dashboard/stocks/supplier_form.html"
    success_url = reverse_lazy("stocks:stocks_supplier")
    message_info = _("updated")
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["grid"] = "col-10"
        context["master_product_data"] = self.object.master_product_data
        if "formset" not in context:
            context["formset"] = SupplierProductDetailFormset(instance=self.object)
        context["formset_helper"] = SupplierProductDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        print(request.POST)
        form = self.get_form()
        formset = SupplierProductDetailFormset(request.POST, instance=self.object)
        if form.is_valid():
            if "master_product_data" in request.POST:
                if formset.is_valid():
                    return self.form_valid(form)
                else:
                    return self.form_invalid(form, formset)
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        self.messages.error(
            _(
                """There was a problem creating this %s.
                Please review the fields marked red and resubmit."""
            )
            % self.model._meta.verbose_name
        )
        if "master_product_data" in self.request.POST:
            return self.render_to_response(
                self.get_context_data(form=form, formset=formset)
            )
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = SupplierProductDetailFormset(form_params, instance=form.instance)

        with transaction.atomic():
            self.object = form.save()
            if formset.is_valid() and "master_product_data" in form_params:
                self.object.master_product_data = True
                self.object.save()

                for delete_form in formset.deleted_forms:
                    if delete_form.instance and delete_form.instance.pk:
                        delete_form.instance.delete()

                for item in formset:
                    if item.has_changed():
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted:
                            item.save(commit=True)

        if "master_product_data" not in form_params:
            self.object.master_product_data = False
            self.object.save()

        self.messages.success(
            "{} {} was was successfully {}!".format(
                self.model._meta.verbose_name.title(),
                self.object,
                self.message_info,
            )
        )
        return super().form_valid(form)


class SupplierDeleteView(SterlingRoleMixin, FormMessagesMixin, DeleteView):
    model = Supplier
    success_url = reverse_lazy("stocks:stocks_supplier")
    message_info = _("deleted")
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_form_invalid_message(self):
        return _(
            """There was a problem creating this {}.
                Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

    def get_form_valid_message(self):
        return _(
            "{} {} was successfully {}!".format(
                self.model._meta.verbose_name.title(),
                self.object,
                self.message_info,
            )
        )

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        active_po = self.object.purchase_order.exclude(
            status__in=[PurchaseOrder.STATUS.received, PurchaseOrder.STATUS.closed]
        )
        if not active_po:
            return super().delete(request, *args, **kwargs)
        else:
            self.messages.error(
                _(
                    "Can't delete supplier {}. Supplier still have active Purchase Order.".format(
                        self.object
                    )
                )
            )
            return HttpResponseRedirect(
                reverse_lazy(
                    "stocks:stocks_supplier_item_update", kwargs={"pk": self.object.pk}
                )
            )


class SupplierItemListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ["name", "code", "products", "contact", "email", "phone"]
    order_columns = ["name", "code", "products", "contact", "email", "phone"]
    model = Supplier
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def render_column(self, row, column):
        if column == "name":
            action_data = {
                "name": escape(row.name),
                "url": reverse_lazy(
                    "stocks:stocks_supplier_item_update", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "products":
            return _(
                "{} produk".format(
                    row.supplier_detail.filter(deleted__isnull=True).count()
                )
            )
        else:
            return super().render_column(row, column)

    def get_supplier_master_data(self):
        master_data = []
        for supplier in self.get_initial_queryset():
            for supplier_detail in supplier.supplier_detail.all():
                data = [
                    supplier_detail.supplier.code,
                    supplier_detail.supplier.name,
                    supplier_detail.product.sku,
                    supplier_detail.quantity,
                    supplier_detail.unit.unit,
                    supplier_detail.cost.amount,
                ]
                master_data.append(data)
        return master_data

    def get(self, request, *args, **kwargs):
        if request.GET.get("export_file") == "csv":
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = 'attachment; filename="supplier.csv"'
            writer = csv.writer(response)
            writer.writerow(["code", "name", "sku", "quantity", "unit", "price"])
            data = self.get_supplier_master_data()
            for item in data:
                writer.writerow(item)
            return response
        else:
            return super().get(request, *args, **kwargs)

    def get_filter_method(self):
        return "icontains"

    def filter_queryset(self, qs):
        columns = self.get_columns()
        if not self.pre_camel_case_notation:
            search = self._querydict.get("search[value]", None)
            columns_data = self.extract_datatables_column_data()
            q = Q()
            filter_method = self.get_filter_method()
            for col_no, col in enumerate(columns_data):
                data_field = col["data"]
                column_name = self.order_columns[col_no].replace(".", "__")
                try:
                    data_field = int(data_field)
                except ValueError:
                    pass
                if isinstance(data_field, int):
                    column = columns[data_field]
                else:
                    column = data_field
                column = column.replace(".", "__")

                if search and col["searchable"]:
                    q |= Q(**{"{0}__{1}".format(column, filter_method): search})

                if column_name in ["products"] and col["search.value"]:
                    qs = qs.annotate(
                        products=Count(
                            "supplier_detail",
                            filter=Q(supplier_detail__deleted__isnull=True),
                            distinct=True,
                        )
                    ).filter(products=col["search.value"])

                if col["search.value"]:
                    qs = qs.filter(
                        **{
                            "{0}__{1}".format(column, filter_method): col[
                                "search.value"
                            ]
                        }
                    )

            qs = qs.filter(q)
        return qs

    def ordering(self, qs):
        sorting_cols = 0
        column_sorted = None
        if self.pre_camel_case_notation:
            try:
                sorting_cols = int(self._querydict.get("iSortingCols", 0))
            except ValueError:
                sorting_cols = 0
        else:
            sort_key = "order[{0}][column]".format(sorting_cols)
            while sort_key in self._querydict:
                sorting_cols += 1
                sort_key = "order[{0}][column]".format(sorting_cols)

        order = []
        order_columns = self.get_order_columns()

        for i in range(sorting_cols):
            sort_dir = "asc"
            try:
                if self.pre_camel_case_notation:
                    sort_col = int(self._querydict.get("iSortCol_{0}".format(i)))
                    sort_dir = self._querydict.get("sSortDir_{0}".format(i))
                else:
                    sort_col = int(self._querydict.get("order[{0}][column]".format(i)))
                    sort_dir = self._querydict.get("order[{0}][dir]".format(i))
            except ValueError:
                sort_col = 0

            sdir = "-" if sort_dir == "desc" else ""
            sortcol = order_columns[sort_col]
            column_sorted = sort_col

            if isinstance(sortcol, list):
                for sc in sortcol:
                    if sortcol == "products":
                        qs = qs.annotate(
                            products=Count(
                                "supplier_detail",
                                filter=Q(supplier_detail__deleted__isnull=True),
                                distinct=True,
                            )
                        ).order_by("{0}products".format(sdir))
                    else:
                        order.append("{0}{1}".format(sdir, sc.replace(".", "__")))
            else:
                if sortcol == "products":
                    qs = qs.annotate(
                        products=Count(
                            "supplier_detail",
                            filter=Q(supplier_detail__deleted__isnull=True),
                            distinct=True,
                        )
                    ).order_by("{0}products".format(sdir))
                else:
                    order.append("{0}{1}".format(sdir, sortcol.replace(".", "__")))

        if order:
            return qs.order_by(*order)
        return qs


class SupplierItemListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/supplier_item_list.html"
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission


class SupplierItemCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    model = Supplier
    form_class = SupplierForm
    template_name = "dashboard/stocks/supplier_item_form.html"
    success_url = reverse_lazy("stocks:stocks_supplier_item")
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["grid"] = "col-10"
        if "formset" not in context:
            context["formset"] = SupplierProductDetailFormset
        context["formset_helper"] = SupplierProductDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        formset = SupplierProductDetailFormset(request.POST)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = SupplierProductDetailFormset(form_params, instance=form.instance)

        with transaction.atomic():
            self.object = form.instance.save()
            if formset.is_valid():
                for item in formset:
                    if item.has_changed():
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted:
                            item.save(commit=True)

        self.messages.success(
            _("Supplier Item {} was successfully created.".format(form.instance))
        )
        return super(SupplierItemCreateView, self).form_valid(form)


class SupplierItemUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    model = Supplier
    form_class = SupplierForm
    template_name = "dashboard/stocks/supplier_item_form.html"
    success_url = reverse_lazy("stocks:stocks_supplier_item")
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["grid"] = "col-10"
        if "formset" not in context:
            context["formset"] = SupplierProductDetailFormset(instance=self.object)
        context["formset_helper"] = SupplierProductDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset = SupplierProductDetailFormset(request.POST, instance=self.object)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = SupplierProductDetailFormset(form_params, instance=form.instance)

        with transaction.atomic():
            self.object = form.instance.save()
            if formset.is_valid():
                for delete_form in formset.deleted_forms:
                    if delete_form.instance and delete_form.instance.pk:
                        delete_form.instance.delete()

                for item in formset:
                    if item.has_changed():
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted:
                            item.save(commit=True)

        self.messages.success(
            _("Supplier Item {} was successfully updated.".format(form.instance))
        )
        return super(SupplierItemUpdateView, self).form_valid(form)


class SupplierItemDeleteView(SterlingRoleMixin, FormMessagesMixin, DeleteView):
    model = Supplier
    success_url = reverse_lazy("stocks:stocks_supplier_item")
    message_info = _("deleted")
    permission_name = "supplier"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_form_invalid_message(self):
        return _(
            """There was a problem creating this {}.
                Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

    def get_form_valid_message(self):
        return _(
            "{} {} was successfully {}!".format(
                self.model._meta.verbose_name.title(),
                self.object,
                self.message_info,
            )
        )

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        active_po = self.object.purchase_order.exclude(
            status__in=[PurchaseOrder.STATUS.received, PurchaseOrder.STATUS.closed]
        )
        if not active_po:
            return super().delete(request, *args, **kwargs)
        else:
            self.messages.error(
                _(
                    "Can't delete supplier {}. Supplier still have active Purchase Order.".format(
                        self.object
                    )
                )
            )
            return HttpResponseRedirect(
                reverse_lazy(
                    "stocks:stocks_supplier_item_update", kwargs={"pk": self.object.pk}
                )
            )


class SupplierAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Supplier.objects.filter(is_active=True)

        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs

    def get_result_label(self, item):
        return item.name

    def has_add_permission(self, request):
        if request.user.is_authenticated:
            return True
        return False


class SupplierCheckerView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            supplier_pk = request.GET.get("supplier", "")
            supplier = Supplier.objects.get(pk=supplier_pk)
            response["result"]["master_product_data"] = supplier.master_product_data
            response["status"] = "success"
            status_code = 200
        except Supplier.DoesNotExist:
            response["result"]["master_product_data"] = False
            response["status"] = "success"
            status_code = 200

        return self.render_json_response(response, status=status_code)


class SupplierMasterDataDetailView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            supplier_pk = request.GET.get("supplier", "")
            supplier = Supplier.objects.get(pk=supplier_pk)
            data = []
            for detail in supplier.supplier_detail.all():
                data.append(
                    {
                        "product": detail.product.__str__(),
                        "product_id": detail.product.pk,
                        "quantity": get_normalized_decimal(detail.quantity),
                        "unit": detail.unit.unit,
                        "unit_id": detail.unit.pk,
                        "cost": get_normalized_decimal(detail.cost.amount),
                        "total_cost": get_normalized_decimal(
                            detail.quantity * detail.cost.amount
                        ),
                    }
                )
            response["status"] = "success"
            response["result"]["data"] = data
            status_code = 200
        except (Supplier.DoesNotExist, ValueError):
            pass

        return self.render_json_response(response, status=status_code)


class SupplierProductDataView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            supplier_pk = request.GET.get("supplier", "")
            product_pk = request.GET.get("product", "")
            supplier = Supplier.objects.get(pk=supplier_pk)
            supplier_detail = supplier.supplier_detail.get(product=product_pk)
            response["status"] = "success"
            response["result"]["master_product_data"] = supplier.master_product_data
            response["result"]["unit"] = supplier_detail.unit.unit
            response["result"]["unit_id"] = supplier_detail.unit.pk
            response["result"]["quantity"] = get_normalized_decimal(
                supplier_detail.quantity
            )
            response["result"]["cost"] = get_normalized_decimal(
                supplier_detail.cost.amount
            )
            response["result"]["total_cost"] = get_normalized_decimal(
                supplier_detail.quantity * supplier_detail.cost.amount
            )
            status_code = 200
        except (SupplierProductDetail.DoesNotExist, ValueError):
            pass

        return self.render_json_response(response, status=status_code)


class SupplierImportTemplateView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/supplier_import.html"


class SupplierImportView(SterlingRoleMixin, SterlingImportMixin):
    template_name = "dashboard/stocks/supplier_import_form.html"
    resource_class = SupplierResource
    file_headers = ["code", "name", "sku", "product", "quantity", "unit", "cost"]
    success_url = reverse_lazy("stocks:stocks_supplier_import")
