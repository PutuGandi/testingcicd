from django.utils.translation import ugettext as _
from django.http import HttpResponse
from django.template.loader import get_template
from django.conf import settings

from weasyprint import HTML, CSS


class PDFExportMixin(object):
    def get(self, request, *args, **kwargs):
        if request.GET.get("export_file") == "pdf":
            template = get_template(self.pdf_template)
            context = self.get_pdf_data()

            html = template.render(context, request=request)
            html_pdf = HTML(string=html, base_url=request.build_absolute_uri())
            css = CSS(
                "{}{}{}".format(
                    settings.APPS_DIR, settings.STATIC_URL, "sass/project.css"
                )
            )
            pdf = html_pdf.write_pdf(stylesheets=[css])

            response = HttpResponse(pdf, content_type="application/pdf")
            response["Content-Disposition"] = 'inline; filename="%s.pdf"' % (
                context["object"].code
            )
            return response
        else:
            return super(PDFExportMixin, self).get(request, *args, **kwargs)
