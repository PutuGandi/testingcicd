import pytz
import rules

from django.utils import timezone
from django.views.generic import (
    TemplateView,
    CreateView,
    UpdateView,
    DetailView,
)
from django.contrib.humanize.templatetags.humanize import intcomma

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.db import transaction

from dal import autocomplete
from datetime import datetime
from decimal import Decimal

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    MessageMixin,
)
from formtools.wizard.views import SessionWizardView

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.private_apps.signals import feed_webhook_signal

from sterlingpos.catalogue.utils import (
    uom_conversion,
    uom_conversion_without_rounding,
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    Supplier,
    Batch,
    TRANSACTION_TYPE,
    PurchaseOrderReceive,
    PurchaseOrderReceiveDetail,
)
from sterlingpos.stocks.forms import (
    BatchFormset,
    PurchaseReceiveOrderForm,
    ReceivedOrderForm,
    ReceivedOrderDetailForm,
    ReceivedOrderCompleteForm,
)
from sterlingpos.catalogue.utils import get_normalized_decimal

from .mixin import PDFExportMixin
from .util import received_order_has_batch_tracked

STEP_BATCH_TRACKED_FORM = "batch_tracked_form"
STEP_STOCK_RECEIVED_ORDER_FORM = "received_order_form"


class ReceivedOrderListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "code",
        "received_date",
        "purchase_order.code",
        "purchase_order.outlet.name",
        "purchase_order.supplier.name",
    ]
    order_columns = [
        "code",
        "received_date",
        "purchase_order__code",
        "purchase_order__outlet__pk",
        "purchase_order__supplier__pk",
    ]
    datetime_col = ["received_date"]
    model = PurchaseOrderReceive
    permission_name = "received_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(purchase_order__outlet__in=outlet_qs)
        return qs

    def render_column(self, row, column):
        if column == "code":
            action_data = {
                "name": row.code,
                "url": reverse_lazy(
                    "stocks:stocks_received_order_detail", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "received_date":
            fmt = "%d-%m-%Y %H:%M"
            date = row.received_date.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        else:
            return super().render_column(row, column)

    def filter_purchase_order__outlet__pk(self, value):
        return Q(purchase_order__outlet__pk=value)


class ReceivedOrderListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/received_order_list.html"
    permission_name = "received_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        context["suppliers"] = Supplier.objects.all()
        return context


class ReceivedOrderCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    model = PurchaseOrderReceive
    form_class = ReceivedOrderForm
    template_name = "dashboard/stocks/received_order_form.html"
    permission_name = "received_order"
    # success_url = reverse_lazy("stocks:stocks_received_order")

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_received_order_detail", kwargs={"pk": self.object.pk}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def form_invalid(self, form):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=True)
            for purchased_stock in self.object.purchase_order.purchased_stock.all():
                quantity = purchased_stock.quantity
                total_received = 0
                for received in self.object.purchase_order.purchase_received.all():
                    try:
                        received_detail = received.received_detail.get(
                            stock=purchased_stock.stock
                        )
                        converted_qty = uom_conversion_without_rounding(
                            received_detail.unit,
                            purchased_stock.unit,
                            received_detail.quantity,
                        )
                        total_received += converted_qty
                    except PurchaseOrderReceiveDetail.DoesNotExist:
                        pass
                remain_qty = purchased_stock.quantity - total_received
                if total_received < quantity and get_normalized_decimal(remain_qty):
                    self.object.receive_purchase(
                        stock=purchased_stock.stock,
                        quantity=remain_qty,
                        unit=purchased_stock.unit,
                    )

        feed_webhook_signal.send(
            sender=self.__class__,
            instance=form.instance,
            created=True)

        self.messages.success(
            _("Received Order {} was successfully created.".format(form.instance))
        )
        return super(ReceivedOrderCreateView, self).form_valid(form)


class ReceivedOrderUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    model = PurchaseOrderReceive
    form_class = PurchaseReceiveOrderForm
    template_name = "dashboard/stocks/received_order_form.html"
    permission_name = "received_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_received_order_detail", kwargs={"pk": self.object.pk}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["purchase_order"] = self.object.purchase_order
        context["received_detail"] = self.object.received_detail.all()
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["purchase_order"] = self.object.purchase_order.pk
        return kwargs

    def form_invalid(self, form):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        self.object = form.save(commit=True)
        self.object.update_received()

        feed_webhook_signal.send(
            sender=self.__class__,
            instance=form.instance,
            created=False)

        self.messages.success(
            _("Received Order {} was successfully updated.".format(form.instance))
        )
        return super(ReceivedOrderUpdateView, self).form_valid(form)


class ReceivedOrderCompleteViewWizard(
    SterlingRoleMixin, MessageMixin, SessionWizardView
):
    template_name = {
        STEP_STOCK_RECEIVED_ORDER_FORM: "dashboard/stocks/received_order_complete_form.html",
        STEP_BATCH_TRACKED_FORM: "dashboard/stocks/batch_tracked_form.html",
    }
    form_list = [
        (STEP_STOCK_RECEIVED_ORDER_FORM, ReceivedOrderCompleteForm),
        (STEP_BATCH_TRACKED_FORM, BatchFormset),
    ]
    condition_dict = {STEP_BATCH_TRACKED_FORM: received_order_has_batch_tracked}
    permission_name = "received_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_template_names(self):
        return [self.template_name[self.steps.current]]

    def dispatch(self, request, *args, **kwargs):
        self.object = get_object_or_404(PurchaseOrderReceive, pk=self.kwargs["pk"])
        self.object_detail = self.object.received_detail.all().select_related(
            "stock", "stock__product"
        )
        return super().dispatch(request, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        super(ReceivedOrderCompleteViewWizard, self).__init__(*args, **kwargs)
        self.batched_products = []
        self.object = None
        self.object_detail = None

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_received_order_detail", kwargs={"pk": self.object.pk}
        )

    def get_context_data(self, form, **kwargs):
        context = super(ReceivedOrderCompleteViewWizard, self).get_context_data(
            form=form, **kwargs
        )
        if self.steps.current == STEP_STOCK_RECEIVED_ORDER_FORM:
            context["received_pk"] = self.kwargs["pk"]
            context["object"] = self.object
        if self.steps.current == STEP_BATCH_TRACKED_FORM:
            context["inventory_type"] = TRANSACTION_TYPE.purchase_order
            context["outlet"] = self.object.purchase_order.outlet
            context["outlet_pk"] = self.object.purchase_order.outlet.pk
            context["status"] = TRANSACTION_TYPE.purchase_order
            context["status_display"] = TRANSACTION_TYPE["purchase_order"]

        return context

    def get_form(self, step=None, data=None, files=None):
        form = super(ReceivedOrderCompleteViewWizard, self).get_form(step, data, files)
        if step == STEP_STOCK_RECEIVED_ORDER_FORM:
            form.instance = self.object
        if step == STEP_BATCH_TRACKED_FORM:
            form.extra = len(self.batched_products)
        return form

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step)
        if step == STEP_BATCH_TRACKED_FORM:
            kwargs["status"] = TRANSACTION_TYPE.purchase_order
            kwargs["outlet"] = self.object.purchase_order.outlet
        return kwargs

    def get_form_initial(self, step):
        if step == STEP_BATCH_TRACKED_FORM:
            initial_data = []
            try:
                received_order = self.object
                for received in received_order.received_detail.all().select_related(
                    "stock", "stock__product", "unit"
                ):
                    if (
                        received.stock.product.is_batch_tracked
                        and received.quantity > 0
                        and received.stock.product not in self.batched_products
                    ):
                        self.batched_products.append(received.stock.product)
                        initial_data.append(
                            {
                                "product": received.stock.product,
                                "product_name": received.stock.product.name,
                                "quantity_to_assign": "{} {}".format(
                                    get_normalized_decimal(Decimal(received.quantity)),
                                    received.unit,
                                ),
                                "total_quantity": received.quantity,
                                "unit": received.unit.pk,
                            }
                        )
            except PurchaseOrderReceive.DoesNotExist:
                pass
            return self.instance_dict.get(step, initial_data)
        return super().get_form_initial(step)

    def get_next_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return self.steps.first
        key = keys.index(step) + 1
        if len(keys) > key:
            return keys[key]
        return None

    def get_prev_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return None
        key = keys.index(step) - 1
        if key >= 0:
            return keys[key]
        return None

    def post(self, *args, **kwargs):
        if self.steps.current not in self.steps.all:
            form = self.get_form(data=self.request.POST, files=self.request.FILES)
            return self.render_done(form, **kwargs)
        return super().post(*args, **kwargs)

    def done(self, form_list, **kwargs):
        received_form = list(form_list)[0]
        if len(form_list) > 1:
            batch_formset = list(form_list)[1]

        with transaction.atomic():
            self.object = received_form.save(commit=True)
            for received in self.object.received_detail.filter(
                stock__product__is_manage_stock=True, stock__product__uom__isnull=False
            ).select_related(
                "stock",
                "stock__product",
                "stock__product__account",
                "stock__product__uom",
                "unit",
                "stock__outlet",
                "stock__account",
            ):
                received.received_product(
                    self.request.user,
                )
            purchase_order = self.object.purchase_order
            purchase_order.received()

            if len(form_list) > 1:
                for batch_form in batch_formset:
                    if batch_form.is_valid():
                        for item in batch_form.nested:
                            if item.is_valid():
                                batch = None
                                deleted = item.cleaned_data.get("DELETE")
                                batch_id = item.cleaned_data.get("batch_id")
                                quantity = item.cleaned_data.get("quantity")
                                unit = item.cleaned_data.get("unit")
                                converted_qty = uom_conversion(
                                    unit, batch_form.instance.product.uom, quantity
                                )
                                if not deleted:
                                    batch, created = Batch.objects.get_or_create(
                                        batch_id=batch_id,
                                        product=batch_form.instance.product,
                                    )

                                    if created:
                                        expiration_date = datetime.strptime(
                                            item.cleaned_data.get("expiration_date"),
                                            "%d/%m/%Y",
                                        )
                                        batch.expiration_date = expiration_date
                                        batch.save()

                                        batch_item = item.save(commit=False)
                                        batch_item.batch = batch
                                        batch_item.outlet = purchase_order.outlet
                                        batch_item.save()
                                        batch_item.add_batch_balance(
                                            converted_qty,
                                            batch_form.instance.product.uom,
                                            purchase_order,
                                            TRANSACTION_TYPE.purchase_order,
                                        )
                                    else:
                                        (
                                            batch_item,
                                            created,
                                        ) = batch.batch_items.get_or_create(
                                            outlet=purchase_order.outlet
                                        )
                                        batch_item.add_batch_balance(
                                            converted_qty,
                                            batch_form.instance.product.uom,
                                            purchase_order,
                                            TRANSACTION_TYPE.purchase_order,
                                        )

        return HttpResponseRedirect(self.get_success_url())


class RecevieDetailView(SterlingRoleMixin, PDFExportMixin, DetailView):
    template_name = "dashboard/stocks/purchase_order_received.html"
    model = PurchaseOrderReceive
    pdf_template = "dashboard/stocks/pdf/received.html"
    permission_name = "received_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["received_detail"] = self.object.received_detail.all()
        return context

    def get_pdf_data(self):
        data = {
            "object": self.get_object(),
            "received_stock": self.get_object()
            .received_detail.all()
            .select_related(
                "stock", "stock__product", "stock__product__catalogue", "unit"
            ),
            "base_url": self.request.build_absolute_uri("/"),
            "logo": self.request.user.account.brand_logo,
            "account": self.request.user.account,
        }
        return data


class ReceivedOrderDetailListJson(
    SterlingRoleMixin, DatatableMixins, BaseDatatableView
):
    columns = [
        "stock.product.sku",
        "stock.product.catalogue.name",
        "stock.product.name",
        "stock.product.category.name",
        "quantity",
        "action",
    ]
    order_columns = [
        "stock__product__sku",
        "stock__product__catalogue__name",
        "stock__product__name",
        "stock__product__category",
        "quantity",
        "action",
    ]
    model = PurchaseOrderReceiveDetail
    permission_name = "received_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.select_related(
            "received",
            "received__purchase_order",
            "stock",
            "stock__product",
            "stock__product__catalogue",
            "stock__product__category",
            "unit",
        )
        qs = qs.filter(received=self.kwargs["pk"])
        return qs

    def get_unreceived_stock(self, purchase_order, stock):
        total_received = 0
        purchase_order_detail = purchase_order.purchased_stock.get(stock=stock)
        received = purchase_order.purchase_received.filter(
            received=True
        ).prefetch_related("received_detail")
        for receive in received:
            try:
                receive = receive.received_detail.get(stock=stock)
                converted_qty = uom_conversion(
                    receive.unit, purchase_order_detail.unit, receive.quantity
                )
                total_received += converted_qty
            except PurchaseOrderReceiveDetail.DoesNotExist:
                total_received += 0

        if total_received < purchase_order_detail.quantity:
            return True
        return False

    def render_column(self, row, column):
        if column == "quantity":
            return "{} {}".format(
                intcomma(get_normalized_decimal(row.quantity)), row.unit
            )
        else:
            return super().render_column(row, column)

    def render_action(self, obj):
        html = ""
        if (
            obj.received.purchase_order.status in ["active", "partial"]
            and self.get_unreceived_stock(obj.received.purchase_order, obj.stock)
            and not obj.received.received
        ):
            html = """
                <button class="btn btn-link received-update" data-toggle="modal" 
                    data-target="#received-detail-modal" data-url="{}">
                    <i class="fa fa-edit" aria-hidden="true"></i>
                </button>
            """.format(
                reverse_lazy(
                    "stocks:stocks_received_order_detail_received",
                    kwargs={"received_order_pk": obj.received.id, "pk": obj.id},
                ),
            )
        return html

    def filter_stock__product__sku(self, value):
        return Q(stock__product__sku__icontains=value)

    def filter_stock__product__name(self, value):
        return Q(stock__product__name__icontains=value)

    def filter_stock__product__catalogue__name(self, value):
        return Q(stock__product__catalogue__name__icontains=value)

    def filter_stock__product__category(self, value):
        return Q(stock__product__category=value)


class ReceivedOrderDetailUpdateView(
    SterlingRoleMixin, MessageMixin, JSONResponseMixin, UpdateView
):
    model = PurchaseOrderReceiveDetail
    form_class = ReceivedOrderDetailForm
    template_name = "dashboard/stocks/received_order_detail_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "pk": self.kwargs["pk"],
                "received_order_pk": self.kwargs["received_order_pk"],
                "table_id": "#tbl_received_order",
                "product": self.object.stock.product,
            }
        )
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"purchase_order": self.object.received.purchase_order.pk})
        return kwargs

    def form_invalid(self, form):
        if self.request.is_ajax():
            return self.render_json_response(self.get_error_result(form))
        return super().form_invalid(form)

    def get_error_result(self, form):
        return {"status": "error", "errors": form.errors}

    def get_form_valid_message(self):
        return _("<strong>%s</strong> was successfully updated!") % (
            self.object.stock.product.name,
        )

    def form_valid(self, form):
        form.save(commit=True)
        if self.request.is_ajax():

            return self.render_json_response(
                {
                    "status": "success",
                    "message": self.get_form_valid_message(),
                    "object_pk": self.object.pk,
                }
            )

        return super().form_valid(form)


class ReceivedOrderAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = PurchaseOrderReceive.objects.filter(
            vendor_bill__isnull=True, received_detail__isnull=False
        )
        if self.q:
            qs = qs.filter(
                Q(code__icontains=self.q)
                | Q(purchase_order__supplier__name__icontains=self.q)
                | Q(purchase_order__supplier__code__icontains=self.q)
                | Q(purchase_order__outlet__name__icontains=self.q)
                | Q(purchase_order__code__icontains=self.q)
            )
        return qs

    def get_result_label(self, item):
        return f"{item.code} - {item.purchase_order.code}"
