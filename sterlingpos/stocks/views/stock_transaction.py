import json
import pytz

from django.utils import timezone
from django.views.generic import TemplateView
from django.contrib.humanize.templatetags.humanize import intcomma

from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.utils.text import slugify

from datetime import datetime
from dateutil.relativedelta import relativedelta
from django_datatables_view.base_datatable_view import BaseDatatableView

from sterlingpos.core.mixins import DatatableMixins, CSVExportMixin
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.catalogue.models import Product
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    Stock,
    StockTransaction,
)
from sterlingpos.catalogue.utils import get_normalized_decimal


class StockTransactionListJson(
    SterlingRoleMixin, CSVExportMixin, DatatableMixins, BaseDatatableView
):
    columns = [
        "stock.product.catalogue.name",
        "stock.product.name",
        "transaction_type",
        "created",
        "outlet",
        "quantity",
        "reference",
        "user.email",
    ]
    order_columns = [
        "stock__product__catalogue__name",
        "stock__product__name",
        "transaction_type",
        "created",
        "outlet__pk",
        "quantity",
        "reference",
        "user__email",
    ]
    csv_header = [
        "Produk",
        "Varian",
        "Tipe Transaksi",
        "Tanggal Transaksi",
        "Outlet",
        "Jumlah",
        "Referensi",
        "Pengguna",
    ]

    csv_columns = [
        "stock.product.catalogue.name",
        "stock.product.name",
        "transaction_type",
        "created",
        "outlet.name",
        "quantity",
        "reference",
        "user.email",
    ]
    datetime_col = ["created"]
    model = StockTransaction

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs)
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def get_csv_filename(self):
        csv_filename = "Stock_History"
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get("outlet_pk"):
            outlet = get_object_or_404(outlet_qs, pk=self.kwargs.get("outlet_pk"))
            csv_filename = "{}_{}".format(csv_filename, slugify(outlet.name))

        if self.request.GET.get("start_date"):
            start_date = datetime.strptime(
                self.request.GET.get("start_date"), "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, start_date)

        if self.request.GET.get("end_date"):
            end_date = datetime.strptime(
                self.request.GET.get("end_date"), "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, end_date)

        return csv_filename

    def render_csv_column(self, row, column):
        if column == "quantity":
            return "{} {}".format(get_normalized_decimal(row.quantity), row.unit)
        elif column == "reference":
            try:
                reference = json.loads(row.reference)
            except TypeError:
                reference = row.reference
            if reference:
                reference = reference[0]
                return reference["fields"].get("code")
            else:
                return None
        elif column == "created":
            fmt = "%d/%m/%Y %H:%M"
            date = row.created.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        else:
            return super().render_csv_column(row, column)

    def get_initial_queryset(self):
        qs = (
            super()
            .get_initial_queryset()
            .select_related(
                "stock",
                "stock__product",
                "stock__product__catalogue",
                "outlet",
                "unit",
            )
            .order_by("-created")
        )

        outlet_pk = self.kwargs.get("outlet_pk")
        outlet_qs = get_outlet_of_user(self.request.user)
        if outlet_pk:
            outlet = get_object_or_404(outlet_qs, pk=outlet_pk)
        else:
            outlet = None

        if self.request.user.is_owner:
            if outlet:
                qs = qs.filter(outlet=outlet)
        else:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(outlet__in=outlet_qs)
            if outlet:
                qs = qs.filter(outlet=outlet)

        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = today + relativedelta(day=1)

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = today + relativedelta(day=1, months=+1, seconds=-1)

        qs = qs.filter(created__gte=start_date, created__lte=end_date)
        return qs

    def filter_transaction_type(self, search_value):
        if search_value == "show":
            return Q()
        return Q(transaction_type=search_value)

    def render_column(self, row, column):
        if column == "reference":
            if row.reference:
                try:
                    reference = json.loads(row.reference)
                except TypeError:
                    reference = row.reference
                if reference:
                    reference = row.reference[0]
                    if reference.get("model") == "stocks.stockcount":
                        return "<a href='{}'>{}</a>".format(
                            reverse_lazy(
                                "stocks:stocks_count_detail",
                                kwargs={"pk": reference.get("pk")},
                            ),
                            reference["fields"].get("code"),
                        )
                    if reference.get("model") == "stocks.stocktransfer":
                        return "<a href='{}'>{}</a>".format(
                            reverse_lazy(
                                "stocks:stocks_transfer_detail",
                                kwargs={"pk": reference.get("pk")},
                            ),
                            reference["fields"].get("code"),
                        )
                    if reference.get("model") == "stocks.stockadjustment":
                        return "<a href='{}'>{}</a>".format(
                            reverse_lazy(
                                "stocks:stocks_adjustment_detail",
                                kwargs={"pk": reference.get("pk")},
                            ),
                            reference["fields"].get("code"),
                        )
                    if reference.get("model") == "stocks.purchaseorder":
                        return "<a href='{}'>{}</a>".format(
                            reverse_lazy(
                                "stocks:stocks_purchase_order_detail",
                                kwargs={"pk": reference.get("pk")},
                            ),
                            reference["fields"].get("code"),
                        )
                    if reference.get("model") == "sales.salesorder":
                        return "<a href='{}'>{}</a>".format(
                            reverse_lazy(
                                "sales:sales_order_detail",
                                kwargs={"pk": reference.get("pk")},
                            ),
                            reference["fields"].get("code")[-6:],
                        )
                    if reference.get("model") == "productions.productionsorder":
                        return "<a href='{}'>{}</a>".format(
                            reverse_lazy(
                                "productions:production_detail",
                                kwargs={"pk": reference.get("pk")},
                            ),
                            reference["fields"].get("code"),
                        )
                else:
                    return None
            else:
                return None
        elif column == "created":
            fmt = "%d-%m-%Y %H:%M"
            date = row.created.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        elif column == "quantity":
            if row.quantity < 0:
                return "<p class='text-color-red m-0'>{} {}</p>".format(
                    intcomma(get_normalized_decimal(row.quantity)),
                    row.unit,
                )
            elif row.quantity > 0:
                return "<p class='text-color-green m-0'>{} {}</p>".format(
                    intcomma(get_normalized_decimal(row.quantity)),
                    row.unit,
                )
            return "<p class='m-0'>{} {}</p>".format(
                intcomma(get_normalized_decimal(row.quantity)),
                row.unit,
            )
        elif column == "outlet":
            return row.outlet.name
        return super().render_column(row, column)

    def filter_stock__product__catalogue__name(self, value):
        return Q(stock__product__catalogue__name__icontains=value)

    def filter_stock__product__name(self, value):
        return Q(stock__product__name__icontains=value)


class StockTransactionListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/transaction_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        context["start_date"] = today + relativedelta(day=1)
        context["end_date"] = today + relativedelta(day=1, months=+1, days=-1)
        return context


class ProductStockTransactionListJson(
    SterlingRoleMixin, DatatableMixins, BaseDatatableView
):
    columns = ["created", "transaction_type", "quantity", "reference", "user.email"]
    order_columns = [
        "created",
        "transaction_type",
        "quantity",
        "reference",
        "user__email",
    ]
    model = StockTransaction

    def get_initial_queryset(self):
        stock_pk = self.kwargs.get("stock_pk")
        qs = super(ProductStockTransactionListJson, self).get_initial_queryset()
        qs = qs.filter(stock__pk=stock_pk)
        return qs

    def render_column(self, row, column):
        if column == "reference":
            try:
                reference = json.loads(row.reference)
            except TypeError:
                reference = row.reference
            if reference:
                reference = reference[0]
                if reference.get("model") == "stocks.stockcount":
                    return "Recounting <a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_count_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                if reference.get("model") == "stocks.stocktransfer":
                    return "Transfer <a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_transfer_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                if reference.get("model") == "stocks.stockadjustment":
                    return "Adjustment <a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_adjustment_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                if reference.get("model") == "stocks.purchaseorder":
                    return "Purchase Order <a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_purchase_order_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                if reference.get("model") == "sales.salesorder":
                    return "Sales Order <a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "sales:sales_order_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                if reference.get("model") == "productions.productionsorder":
                    return "<a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "productions:production_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
            else:
                return None
        return super().render_column(row, column)

    def filter_transaction_type(self, search_value):
        if search_value == "show":
            return Q()
        return Q(transaction_type=search_value)


class ProductStockTransactionListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/stock_transaction_list.html"

    def get_context_data(self, **kwargs):
        context = super(ProductStockTransactionListView, self).get_context_data(
            **kwargs
        )
        context["stock"] = get_object_or_404(
            Stock,
            product__pk=self.kwargs.get("product_pk"),
            outlet__pk=self.kwargs.get("outlet_pk"),
        )
        return context


class StockInTransactionListJson(StockTransactionListJson):
    def __init__(self, *args, **kwargs):
        super(StockInTransactionListJson, self).__init__(*args, **kwargs)
        self.start_date_name = ""
        self.end_date_name = ""

    def get_initial_queryset(self):
        product_pk = self.kwargs.get("product_pk")
        qs = super(StockInTransactionListJson, self).get_initial_queryset()
        qs = qs.filter(stock__product__pk=product_pk)
        qs = qs.filter(quantity__gte=0)

        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = today + relativedelta(day=1)
        self.start_date_name = start_date.strftime("%Y/%m/%d--%H.%M.%S")

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = today + relativedelta(day=1, months=+1, days=-1)
        self.end_date_name = end_date.strftime("%Y/%m/%d--%H.%M.%S")

        outlet_pk = self.kwargs.get("outlet_pk")

        qs = qs.filter(created__range=[start_date, end_date])
        if outlet_pk:
            qs = qs.filter(stock__outlet__pk=outlet_pk)
        return qs

    def get_csv_filename(self):
        product_pk = self.kwargs.get("product_pk")
        product = Product.objects.get(pk=product_pk)
        csv_filename = "{}_Stock_In".format(product.name)
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get("outlet_pk"):
            outlet = get_object_or_404(outlet_qs, pk=self.kwargs.get("outlet_pk"))
            csv_filename = "{}_{}".format(csv_filename, slugify(outlet.name))

        if self.start_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.start_date_name)
        if self.end_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.end_date_name)

        return csv_filename


class StockInTransactionListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/transaction_stock_in_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["product_pk"] = self.kwargs.get("product_pk")
        context["outlet_pk"] = self.kwargs.get("outlet_pk")
        if context["outlet_pk"]:
            context["outlet"] = Outlet.objects.get(pk=context["outlet_pk"])
        context["start_date"] = self.request.GET.get("start_date", None)
        context["end_date"] = self.request.GET.get("end_date", None)
        return context


class StockOutTransactionListJson(StockTransactionListJson):
    def __init__(self, *args, **kwargs):
        super(StockOutTransactionListJson, self).__init__(*args, **kwargs)
        self.start_date_name = ""
        self.end_date_name = ""

    def get_initial_queryset(self):
        product_pk = self.kwargs.get("product_pk")
        qs = super(StockOutTransactionListJson, self).get_initial_queryset()
        qs = qs.filter(stock__product__pk=product_pk)
        qs = qs.filter(quantity__lt=0)
        qs = qs.exclude(transaction_type__in=["transaction", "productions"])

        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = today + relativedelta(day=1)
        self.start_date_name = start_date.strftime("%Y/%m/%d--%H.%M.%S")

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = today + relativedelta(day=1, months=+1, days=-1)
        self.end_date_name = end_date.strftime("%Y/%m/%d--%H.%M.%S")

        outlet_pk = self.kwargs.get("outlet_pk")

        qs = qs.filter(created__range=[start_date, end_date])
        if outlet_pk:
            qs = qs.filter(stock__outlet__pk=outlet_pk)
        return qs

    def get_csv_filename(self):
        product_pk = self.kwargs.get("product_pk")
        product = Product.objects.get(pk=product_pk)
        csv_filename = "{}_Stock_Out".format(product.name)
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get("outlet_pk"):
            outlet = get_object_or_404(outlet_qs, pk=self.kwargs.get("outlet_pk"))
            csv_filename = "{}_{}".format(csv_filename, slugify(outlet.name))

        if self.start_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.start_date_name)
        if self.end_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.end_date_name)

        return csv_filename


class StockOutTransactionListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/transaction_stock_out_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["product_pk"] = self.kwargs.get("product_pk")
        context["outlet_pk"] = self.kwargs.get("outlet_pk")
        if context["outlet_pk"]:
            context["outlet"] = Outlet.objects.get(pk=context["outlet_pk"])
        context["start_date"] = self.request.GET.get("start_date", None)
        context["end_date"] = self.request.GET.get("end_date", None)
        return context


class StockSalesTransactionJsonList(StockTransactionListJson):
    def __init__(self, *args, **kwargs):
        super(StockSalesTransactionJsonList, self).__init__(*args, **kwargs)
        self.start_date_name = ""
        self.end_date_name = ""

    def get_initial_queryset(self):
        product_pk = self.kwargs.get("product_pk")
        qs = super(StockSalesTransactionJsonList, self).get_initial_queryset()
        qs = qs.filter(stock__product__pk=product_pk)
        qs = qs.filter(quantity__lt=0)
        qs = qs.filter(
            (Q(transaction_type="transaction"))
            | (
                Q(transaction_type="productions")
                & Q(reference__0__fields__automatic_production=True)
            )
        )

        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = today + relativedelta(day=1)
        self.start_date_name = start_date.strftime("%Y/%m/%d--%H.%M.%S")

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = today + relativedelta(day=1, months=+1, days=-1)
        self.end_date_name = end_date.strftime("%Y/%m/%d--%H.%M.%S")

        outlet_pk = self.kwargs.get("outlet_pk")

        qs = qs.filter(created__range=[start_date, end_date])
        if outlet_pk:
            qs = qs.filter(stock__outlet__pk=outlet_pk)
        return qs

    def get_csv_filename(self):
        product_pk = self.kwargs.get("product_pk")
        product = Product.objects.get(pk=product_pk)
        csv_filename = "{}_Stock_Sales".format(product.name)
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get("outlet_pk"):
            outlet = get_object_or_404(outlet_qs, pk=self.kwargs.get("outlet_pk"))
            csv_filename = "{}_{}".format(csv_filename, slugify(outlet.name))

        if self.start_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.start_date_name)
        if self.end_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.end_date_name)

        return csv_filename
