import json
import pytz

from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.generic import (
    TemplateView,
    View,
    DetailView,
    FormView,
)
from django.contrib.humanize.templatetags.humanize import intcomma

from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.utils.html import escape
from django.http import Http404

from dal_select2 import views as select2
from datetime import datetime
from decimal import Decimal, InvalidOperation

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    MessageMixin,
)

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.catalogue.models import UOM
from sterlingpos.catalogue.utils import (
    uom_conversion,
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.stocks.models import (
    Batch,
    TRANSACTION_TYPE,
    BatchItem,
)
from sterlingpos.stocks.forms import (
    BatchFormset,
)
from sterlingpos.catalogue.utils import get_normalized_decimal


class BatchAutocompleteView(select2.Select2ListView):
    def get_list(self):
        product_pk = self.request.resolver_match.kwargs["pk"]
        outlet_pk = self.request.resolver_match.kwargs["outlet_pk"]
        return Batch.objects.filter(
            product__pk=product_pk,
            is_missing_batch=False,
            batch_items__outlet__pk=outlet_pk,
        ).values_list("batch_id", flat=True)


class BatchCreateGetView(select2.Select2ListView):
    def get_list(self):
        product_pk = self.request.resolver_match.kwargs["pk"]
        return Batch.objects.filter(
            product__pk=product_pk, is_missing_batch=False
        ).values_list("batch_id", flat=True)

    def create(self, text):
        return text

    def post(self, request, pk):
        from django import http
        from django.core.exceptions import ImproperlyConfigured

        if not hasattr(self, "create"):
            raise ImproperlyConfigured('Missing "create()"')

        text = request.POST.get("text", None)

        if text is None:
            return http.HttpResponseBadRequest()

        text = self.create(text)

        if text is None:
            return http.HttpResponseBadRequest()

        return http.JsonResponse({"id": text, "text": text})


class BatchInformationView(
    SterlingRoleMixin, AjaxResponseMixin, JSONResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            batch_id = request.GET.get("batch_id", "")
            product_id = request.GET.get("product", "")
            outlet_pk = request.GET.get("outlet_pk", "")
            expiration_date = None
            batch = Batch.objects.get(batch_id=batch_id, product__pk=product_id)
            expiration_date = batch.expiration_date

            batch_item = batch.batch_items.get(outlet__pk=outlet_pk)
            response["status"] = "success"
            response["result"]["expiration_date"] = expiration_date.strftime("%d/%m/%Y")
            response["result"]["new_batch_id"] = False
            response["result"]["batch_item_balance"] = "{} {}".format(
                get_normalized_decimal(batch_item.balance),
                batch.product.uom,
            )

            status_code = 200
        except Batch.DoesNotExist:
            response["status"] = "success"
            response["result"]["new_batch_id"] = True
            response["result"]["batch_item_balance"] = 0
            status_code = 200
        except BatchItem.DoesNotExist:
            response["status"] = "success"
            response["result"]["expiration_date"] = expiration_date.strftime("%d/%m/%Y")
            response["result"]["new_batch_id"] = False
            response["result"]["batch_item_balance"] = 0
            status_code = 200

        return self.render_json_response(response, status=status_code)


class BatchTrackedListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ["batch_id", "product", "variant", "expiration_date", "created", "pk"]
    order_columns = [
        "batch_id",
        "product",
        "variant",
        "expiration_date",
        "created",
        "pk",
    ]
    datetime_col = ["expiration_date"]
    model = Batch

    def get_initial_queryset(self):
        qs = (
            super()
            .get_initial_queryset()
            .filter(
                product__is_manage_stock=True,
                product__uom__isnull=False,
                product__is_batch_tracked=True,
                product__archived=False,
            )
            .select_related(
                "product",
                "product__catalogue",
                "product__uom",
            )
        )
        return qs

    def render_column(self, row, column):
        if column == "batch_id":
            text_color = ""
            text_color = "text-danger" if row.is_missing_batch else text_color
            action_data = {
                "name": escape(row.batch_id),
                "url": reverse_lazy(
                    "stocks:stocks_batch_track_detail", kwargs={"pk": row.pk}
                ),
                "text_color": text_color,
            }
            return action_data
        elif column == "variant":
            if row.is_missing_batch:
                return mark_safe(
                    "<span class='text-danger'>{}</span>".format(row.product.name)
                )
            return row.product.name
        elif column == "product":
            if row.is_missing_batch:
                return mark_safe(
                    "<span class='text-danger'>{}</span>".format(
                        row.product.catalogue.name
                    )
                )
            return row.product.catalogue.name
        elif column == "expiration_date":
            if row.expiration_date:
                date = row.expiration_date.strftime("%d-%m-%Y")
                return date
            return "-"
        elif column == "created":
            fmt = "%d-%m-%Y %H:%M"
            date = row.created.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            if row.is_missing_batch:
                return mark_safe(
                    "<span class='text-danger'>{}</span>".format(localtz.strftime(fmt))
                )
            return localtz.strftime(fmt)
        else:
            return super().render_column(row, column)

    def filter_product(self, value):
        return Q(product__catalogue__name__icontains=value)

    def filter_variant(self, value):
        return Q(product__name__icontains=value) | Q(product__sku__icontains=value)


class BatchTrackedListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/all_batch_tracked_list.html"


class MissingBatchTrackedListJson(BatchTrackedListJson):
    def get_initial_queryset(self):
        qs = super(MissingBatchTrackedListJson, self).get_initial_queryset()
        qs = qs.filter(is_missing_batch=True)
        return qs


class MissingBatchTrackedListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/missing_batch_tracked_list.html"


class BatchTrackedDetailView(SterlingRoleMixin, DetailView):
    model = Batch
    template_name = "dashboard/stocks/batch_tracked_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        is_owner = self.request.user.is_owner
        stocks = self.object.product.stocks.all()
        batches = self.object.batch_items.all()

        if not is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            stocks = stocks.filter(outlet__in=outlet_qs)
            batches = batches.filter(outlet__in=outlet_qs)
        total_quantity = 0
        for stock in stocks:
            total_quantity += stock.balance
        context["product_quantity"] = get_normalized_decimal(total_quantity)
        context["is_owner"] = is_owner
        context["batches"] = batches
        return context


class BatchTrackedOutletDetailView(SterlingRoleMixin, DetailView):
    model = BatchItem
    template_name = "dashboard/stocks/batch_tracked_item_detail.html"

    def get_reference(self, reference):
        reference = json.loads(reference)
        reference_link = None
        if reference:
            reference = reference[0]
            if reference.get("model") == "stocks.stockcount":
                reference_link = mark_safe(
                    "<a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_count_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                )
            if reference.get("model") == "stocks.stocktransfer":
                reference_link = mark_safe(
                    "<a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_transfer_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                )
            if reference.get("model") == "stocks.stockadjustment":
                reference_link = mark_safe(
                    "<a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_adjustment_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                )
            if reference.get("model") == "stocks.purchaseorder":
                reference_link = mark_safe(
                    "<a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "stocks:stocks_purchase_order_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                )
            if reference.get("model") == "productions.productionsorder":
                reference_link = mark_safe(
                    "<a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "productions:production_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                )
            if reference.get("model") == "sales.salesorder":
                reference_link = mark_safe(
                    "<a href='{}'>{}</a>".format(
                        reverse_lazy(
                            "sales:sales_order_detail",
                            kwargs={"pk": reference.get("pk")},
                        ),
                        reference["fields"].get("code"),
                    )
                )
        return reference_link

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        histories = []
        for history in self.object.histories.all():
            reference = self.get_reference(history.reference)
            history_data = {
                "created": history.created,
                "transaction_type": TRANSACTION_TYPE[history.transaction_type],
                "quantity": history.quantity,
                "unit": history.unit,
                "reference": reference,
            }
            histories.append(history_data)
        context["histories"] = histories

        stocks = self.object.batch.product.stocks.all()
        total_quantity = 0
        for stock in stocks:
            total_quantity += stock.balance
        context["product_quantity"] = get_normalized_decimal(total_quantity)
        return context


class BatchesQuantityConversionJsonView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404

        try:
            unit_to = request.GET.get("unit_to", "")
            quantity = request.GET.get("quantity", "")
            quantity_to_assign = request.GET.get("quantity_to_assign", "")
            if not quantity:
                quantity = 0

            quantity = Decimal(quantity)
            quantity_to_assign = Decimal(quantity_to_assign)
            unit_to = UOM.objects.get(pk=unit_to)

            quantity_to_assign -= quantity
            response["status"] = "success"
            response["result"]["qty_calc"] = quantity_to_assign
            response["result"]["quantity_to_assign"] = "{} {}".format(
                get_normalized_decimal(quantity_to_assign),
                unit_to,
            )
            status_code = 200
        except (UOM.DoesNotExist, ValueError):
            pass

        return self.render_json_response(response, status=status_code)


class QuantityBatchConversionJsonView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404

        try:
            unit_from = request.GET.get("unit_from", "")
            unit_to = request.GET.get("unit_to", "")
            quantity = request.GET.get("quantity", "")
            if not quantity:
                quantity = 0

            quantity = Decimal(quantity)
            unit_from = UOM.objects.get(pk=unit_from)
            unit_to = UOM.objects.get(pk=unit_to)

            converted_qty = uom_conversion(unit_from, unit_to, quantity)
            response["status"] = "success"
            response["result"]["quantity"] = get_normalized_decimal(converted_qty)
            status_code = 200
        except (UOM.DoesNotExist, ValueError) as e:
            response["status"] = "success"
            status_code = 200
        except InvalidOperation:
            response["status"] = "success"
            response["result"]["quantity"] = 0
            status_code = 200

        return self.render_json_response(response, status=status_code)


class MissingBatchTrackedView(SterlingRoleMixin, MessageMixin, FormView):
    form_class = BatchFormset
    template_name = "dashboard/stocks/stock_batch_tracked_form.html"
    success_url = reverse_lazy("stocks:stocks_batch_track")

    def __init__(self, **kwargs):
        super(MissingBatchTrackedView, self).__init__(**kwargs)
        self.object = None
        self.oultet = None

    def dispatch(self, request, *args, **kwargs):
        obj_pk = self.request.resolver_match.kwargs["batch_pk"]
        outlet_pk = self.request.resolver_match.kwargs["outlet_pk"]
        self.object = None
        self.outlet = None
        try:
            self.object = Batch.objects.get(pk=obj_pk)
            self.outlet = Outlet.objects.get(pk=outlet_pk)
        except Batch.DoesNotExist:
            raise Http404
        except Outlet.DoesNotExist:
            raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["status"] = "missing_batch"
        kwargs["outlet"] = self.outlet
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(MissingBatchTrackedView, self).get_context_data(**kwargs)
        context["inventory_type"] = "missing_batch"
        context["object"] = self.object
        context["outlet_pk"] = self.outlet.pk
        context["outlet"] = self.outlet
        context["status"] = "missing_batch"
        context["status_display"] = _("Missing Batch")
        return context

    def get_initial(self):
        initial = []
        quantity = self.object.batch_items.get(outlet=self.outlet).balance
        initial.append(
            {
                "product": self.object.product,
                "product_name": self.object.product.name,
                "quantity_to_assign": "{} {}".format(
                    get_normalized_decimal(quantity),
                    self.object.product.uom.unit,
                ),
                "total_quantity": quantity,
                "unit": self.object.product.uom.pk,
            }
        )
        return initial

    def form_valid(self, form):
        for batch_form in form:
            deleted_batch = Batch.objects.filter(
                product=batch_form.instance.product,
                batch_items__outlet=self.outlet,
                is_missing_batch=True,
            ).first()
            deleted_items = deleted_batch.batch_items.get(outlet=self.outlet)
            for item in batch_form.nested:
                if not item.is_valid():
                    continue
                batch = None
                deleted = item.cleaned_data.get("DELETE")
                batch_id = item.cleaned_data.get("batch_id")
                quantity = item.cleaned_data.get("quantity")
                unit = item.cleaned_data.get("unit")
                converted_qty = uom_conversion(
                    unit, batch_form.instance.product.uom, quantity
                )
                if not deleted:
                    batch, created = Batch.objects.get_or_create(
                        batch_id=batch_id, product=batch_form.instance.product
                    )

                    if created:
                        expiration_date = datetime.strptime(
                            item.cleaned_data.get("expiration_date"), "%d/%m/%Y"
                        )
                        batch.expiration_date = expiration_date
                        batch.save()

                        batch_item = item.save(commit=False)
                        batch_item.batch = batch
                        batch_item.outlet = self.outlet
                        batch_item.save()
                        if converted_qty > 0:
                            batch_item.add_batch_balance(
                                quantity=converted_qty,
                                unit=batch_form.instance.product.uom,
                                source_object=None,
                                transaction_type=TRANSACTION_TYPE.manual_add,
                            )
                        else:
                            batch_item.substract_batch_balance(
                                quantity=converted_qty,
                                unit=batch_form.instance.product.uom,
                                source_object=None,
                                transaction_type=TRANSACTION_TYPE.manual_add,
                            )
                        deleted_items.set_batch_balance(
                            0, unit=batch_form.instance.product.uom, source_object=None
                        )
                    else:
                        batch_item, created = batch.batch_items.get_or_create(
                            outlet=self.outlet
                        )
                        if converted_qty > 0:
                            batch_item.add_batch_balance(
                                quantity=converted_qty,
                                unit=batch_form.instance.product.uom,
                                source_object=None,
                                transaction_type=TRANSACTION_TYPE.manual_add,
                            )
                        else:
                            batch_item.substract_batch_balance(
                                quantity=converted_qty,
                                unit=batch_form.instance.product.uom,
                                source_object=None,
                                transaction_type=TRANSACTION_TYPE.manual_add,
                            )
                        deleted_items.set_batch_balance(
                            0, unit=batch_form.instance.product.uom, source_object=None
                        )
            deleted_items.delete()

        if deleted_batch.balance == 0:
            deleted_batch.delete()

        self.messages.success(
            _("Batch {} was successfully updated.".format(self.object.product))
        )
        return super().form_valid(form)
