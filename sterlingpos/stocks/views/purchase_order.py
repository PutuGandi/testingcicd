import pytz
import rules
import dateutil.parser
import csv

from django.utils import timezone
from django.views.generic import (
    TemplateView,
    View,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.db import transaction
from django.core.exceptions import SuspiciousOperation
from django.http import HttpResponse

from dal import autocomplete
from datetime import datetime
from decimal import Decimal, InvalidOperation

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    MessageMixin,
)
from formtools.wizard.views import SessionWizardView
from formtools.wizard.forms import ManagementForm

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.private_apps.signals import feed_webhook_signal

from sterlingpos.catalogue.models import Product, UOM
from sterlingpos.catalogue.utils import (
    uom_cost_conversion,
    uom_conversion,
)
from sterlingpos.stocks.models import (
    Batch,
    PurchaseOrder,
    PurchaseOrderDetail,
    TRANSACTION_TYPE,
    PurchaseOrderReceive,
    PurchaseOrderReceiveDetail,
)
from sterlingpos.stocks.forms import (
    BatchFormset,
    PurchaseOrderForm,
    PurchaseOrderDetailFormset,
    PurchaseOrderDetailFormsetHelper,
    PurchaseOrderReceiveFormset,
    PurchaseOrderReceiveFormsetHelper,
    PurchaseReceiveOrderForm,
    PurchaseOrderReceiveDetailFormset,
    PurchaseOrderReceiveDetailFormsetHelper,
)
from sterlingpos.catalogue.utils import get_normalized_decimal

from .mixin import PDFExportMixin
from .util import purchase_order_has_batch_tracked

STEP_BATCH_TRACKED_FORM = "batch_tracked_form"
STEP_PURCHASE_ORDER_RECEIVE_FORM = "purchase_order_receive_form"
STEP_STOCK_COUNT_COMPLETE = "stock_count_complete_form"


class PurchaseOrderAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        exclude_po = PurchaseOrder.objects.filter(
            status__in=[PurchaseOrder.STATUS.active, PurchaseOrder.STATUS.partial],
            purchase_received__received=False,
        ).distinct()
        qs = PurchaseOrder.objects.filter(
            status__in=[PurchaseOrder.STATUS.active, PurchaseOrder.STATUS.partial]
        )
        qs = qs.exclude(pk__in=exclude_po)

        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(outlet__in=outlet_qs)

        if self.q:
            qs = qs.filter(
                Q(code__icontains=self.q)
                | Q(supplier__name__icontains=self.q)
                | Q(supplier__code__icontains=self.q)
                | Q(outlet__name__icontains=self.q)
            )
        return qs

    def get_result_value(self, result):
        return str(result.pk)

    def get_result_label(self, item):
        return item.__str__()


class PurchaseOrderProductDetailJsonView(
    SterlingRoleMixin, AjaxResponseMixin, JSONResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            purchase_order_pk = request.GET.get("purchase_order", "")
            purchase_order = PurchaseOrder.objects.get(pk=purchase_order_pk)
            purchase_orders = []
            if purchase_order.status not in [
                PurchaseOrder.STATUS.received,
                PurchaseOrder.STATUS.closed,
            ]:
                for detail in purchase_order.purchased_stock.all():
                    quantity = get_normalized_decimal(detail.quantity)
                    total_received = 0
                    received = purchase_order.purchase_received.all()
                    for receive in received:
                        try:
                            receive = receive.received_detail.get(stock=detail.stock)
                            converted_qty = uom_conversion(
                                receive.unit, detail.unit, receive.quantity
                            )
                            total_received += converted_qty
                        except PurchaseOrderReceiveDetail.DoesNotExist:
                            pass
                    remain_qty = detail.quantity - total_received
                    remain_qty = get_normalized_decimal(remain_qty)
                    units = UOM.objects.filter(category=detail.unit.category).values(
                        "pk", "unit"
                    )
                    if total_received < quantity:
                        data = {
                            "product": detail.stock.product.__str__(),
                            "total_qty": f"{quantity} {detail.unit}",
                            "total_remain_qty": f"{remain_qty} {detail.unit}",
                            "remain_qty": remain_qty,
                            "unit": detail.unit.pk,
                            "units": list(units),
                            "stock_id": detail.stock.pk,
                        }
                        purchase_orders.append(data)
            response["result"]["purchase_orders"] = purchase_orders
            response["status"] = "success"
            status_code = 200
        except (PurchaseOrder.DoesNotExist, ValueError):
            response["status"] = "success"
            status_code = 200
        return self.render_json_response(response, status=status_code)


class ProductInventoryAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Product.objects.all()
        qs = qs.filter(
            archived=False,
            is_manage_stock=True,
            uom__isnull=False,
            deleted__isnull=True,
        )
        qs = qs.exclude(
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.composite,
                Product.PRODUCT_CLASSIFICATION.non_inventory,
            ]
        )
        is_sellable = self.forwarded.get("is_sellable")
        is_include_manufactured = self.forwarded.get("is_include_manufactured")
        if not is_sellable and is_sellable is not None:
            qs = qs.exclude(is_sellable=True)

        if not is_include_manufactured and is_include_manufactured is not None:
            qs = qs.exclude(
                classification__in=[Product.PRODUCT_CLASSIFICATION.manufactured]
            )

        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(catalogue__name__icontains=self.q)
                | Q(sku__icontains=self.q)
            )
        return qs

    def get_result_label(self, item):
        return "{} - {} - {}".format(item.catalogue.name, item.name, item.sku)


class PurchaseOrderInventoryAutocomplete(ProductInventoryAutocompleteView):
    def get_queryset(self):
        qs = Product.objects.all()
        qs = qs.filter(
            archived=False,
            is_manage_stock=True,
            uom__isnull=False,
            deleted__isnull=True,
        )
        qs = qs.exclude(
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.composite,
                Product.PRODUCT_CLASSIFICATION.non_inventory,
                Product.PRODUCT_CLASSIFICATION.manufactured,
            ]
        )

        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(catalogue__name__icontains=self.q)
                | Q(sku__icontains=self.q)
            )
        return qs


class PurchaseOrderListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "code",
        "date_of_purchase",
        "supplier",
        "outlet",
        "status",
        "pk",
    ]
    order_columns = [
        "code",
        "date_of_purchase",
        "supplier__pk",
        "outlet__pk",
        "status",
        "pk",
    ]
    datetime_col = ["date_of_purchase"]
    model = PurchaseOrder
    permission_name = "purchase_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.prefetch_related("purchased_stock")
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(outlet__in=outlet_qs)
        return qs

    def render_column(self, row, column):
        if column == "code":
            action_data = {
                "name": row.code,
                "url": reverse_lazy(
                    "stocks:stocks_purchase_order_detail", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        elif column == "date_of_purchase":
            fmt = "%d/%m/%Y %H:%M"
            date = row.date_of_purchase.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        elif column == "outlet":
            return row.outlet.name
        elif column == "supplier":
            return row.supplier.name
        else:
            return super().render_column(row, column)

    def filter_status(self, search_value):
        if search_value == "show":
            return Q()
        return Q(status=search_value)

    def filter_outlet__pk(self, value):
        return Q(outlet__pk=value)

    def filter_supplier__pk(self, value):
        return Q(supplier__pk=value)


class PurchaseOrderListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/purchase_order_list.html"
    permission_name = "purchase_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        return context


class PurchaseOrderCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    model = PurchaseOrder
    form_class = PurchaseOrderForm
    template_name = "dashboard/stocks/purchase_order_form.html"
    permission_name = "purchase_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_purchase_order_detail", kwargs={"pk": self.object.pk}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = PurchaseOrderDetailFormset
        context["formset_helper"] = PurchaseOrderDetailFormsetHelper

        if "has_discount_tax" in self.request.POST:
            context["has_discount_tax"] = True
        return context

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        formset = PurchaseOrderDetailFormset(request.POST)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = PurchaseOrderDetailFormset(form_params, instance=form.instance)

        with transaction.atomic():
            self.object = form.instance.save()
            if formset.is_valid():
                for item in formset:
                    if item.has_changed():
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted and item.cleaned_data:
                            item.save(commit=True)

        feed_webhook_signal.send(
            sender=self.__class__,
            instance=form.instance,
            created=True)

        self.messages.success(
            _("Purchase Order {} was successfully created.".format(form.instance))
        )
        return super(PurchaseOrderCreateView, self).form_valid(form)


class PurchaseOrderDetailView(SterlingRoleMixin, PDFExportMixin, DetailView):
    template_name = "dashboard/stocks/purchase_order.html"
    model = PurchaseOrder
    pdf_template = "dashboard/stocks/pdf/purchase_order.html"
    permission_name = "purchase_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.select_related().prefetch_related(
            "purchased_stock",
            "purchased_stock__stock__product",
        )
        return qs

    def get_pdf_data(self):
        data = {
            "object": self.get_object(),
            "purchased_stock": self.get_object()
            .purchased_stock.all()
            .select_related(
                "stock", "stock__product", "stock__product__catalogue", "unit"
            )
            .order_by("-is_received", "modified"),
            "base_url": self.request.build_absolute_uri("/"),
            "logo": self.request.user.account.brand_logo,
            "account": self.request.user.account,
        }
        return data

    def get_context_data(self, **kwargs):
        context = super(PurchaseOrderDetailView, self).get_context_data(**kwargs)

        has_batch_tracked = any(
            self.object.purchased_stock.values_list(
                "stock__product__is_batch_tracked", flat=True
            )
        )
        context["has_batch_tracked"] = has_batch_tracked

        if self.object.status == PurchaseOrder.STATUS.received:
            context["received_object"] = self.object.purchased_stock.distinct("stock")
        context["purchased_stock"] = self.object.purchased_stock.all().order_by(
            "-is_received", "modified"
        )
        context["has_product"] = self.object.purchased_stock.filter(
            stock__product__archived=False
        )

        return context

    def get_purchased_stock(self):
        purchased_stocks = []
        for purchased_stock in self.object.purchased_stock.all():
            data = [
                purchased_stock.stock.product,
                "{}".format(
                    get_normalized_decimal(purchased_stock.quantity),
                ),
                purchased_stock.unit.unit,
                "Rp. {}".format(
                    get_normalized_decimal(purchased_stock.purchase_cost.amount)
                ),
                "Rp. {}".format(get_normalized_decimal(purchased_stock.discount)),
                "Rp. {}".format(get_normalized_decimal(purchased_stock.total_cost)),
            ]
            purchased_stocks.append(data)
        return purchased_stocks

    def get(self, request, *args, **kwargs):
        if request.GET.get("export_file") == "csv":
            self.object = self.get_object()
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = 'attachment; filename="{}.csv"'.format(
                self.object.code
            )

            writer = csv.writer(response)
            writer.writerow(
                [
                    "Shipping Address: ",
                    "{}, {}, {}, {} {}".format(
                        self.object.outlet.name,
                        self.object.outlet.address,
                        self.object.outlet.city,
                        self.object.outlet.province,
                        self.object.outlet.postal_code,
                    ),
                ]
            )
            writer.writerow(
                [
                    "Supplier: ",
                    "{}, {}".format(
                        self.object.supplier.name, self.object.supplier.address
                    ),
                ]
            )
            writer.writerow(["Referensi PO", self.object.code])

            fmt = "%d-%m-%Y %H:%M"
            date_of_purchase = self.object.date_of_purchase.replace(tzinfo=pytz.UTC)
            date_of_purchase = date_of_purchase.astimezone(
                timezone.get_current_timezone()
            )
            date_of_purchase = date_of_purchase.strftime(fmt)
            writer.writerow(["Tanggal Purchase Order", date_of_purchase])
            shipping_date = self.object.shipping_date.replace(tzinfo=pytz.UTC)
            shipping_date = shipping_date.astimezone(timezone.get_current_timezone())
            shipping_date = shipping_date.strftime(fmt)
            writer.writerow(["Ekspektasi Pengiriman", shipping_date])
            writer.writerow([""])
            writer.writerow(
                [
                    "Produk",
                    "Jumlah",
                    "Satuan",
                    "Harga per Unit",
                    "Diskon",
                    "Total Harga",
                ]
            )
            data = self.get_purchased_stock()
            for item in data:
                writer.writerow(item)

            writer.writerow(
                [
                    "",
                    "",
                    "",
                    "",
                    "Subtotal",
                    "Rp. {}".format(
                        get_normalized_decimal(Decimal(self.object.total_cost))
                    ),
                ]
            )
            writer.writerow(
                [
                    "",
                    "",
                    "",
                    "",
                    "Diskon",
                    "Rp. {}".format(
                        get_normalized_decimal(Decimal(self.object.total_discount))
                    ),
                ]
            )
            writer.writerow(
                [
                    "",
                    "",
                    "",
                    "",
                    "Pajak",
                    "Rp. {}".format(
                        get_normalized_decimal(Decimal(self.object.total_taxes))
                    ),
                ]
            )
            writer.writerow(
                [
                    "",
                    "",
                    "",
                    "",
                    "Total",
                    "Rp. {}".format(
                        get_normalized_decimal(Decimal(self.object.grand_total))
                    ),
                ]
            )
            return response
        else:
            return super().get(request, *args, **kwargs)


class PurchaseOrderUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/stocks/purchase_order_form.html"
    model = PurchaseOrder
    form_class = PurchaseOrderForm
    permission_name = "purchase_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_queryset(self):
        qs = (
            super(PurchaseOrderUpdateView, self)
            .get_queryset()
            .exclude(status__in=["closed", "received"])
        )
        return qs

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_purchase_order_detail", kwargs={"pk": self.object.pk}
        )

    def get_context_data(self, **kwargs):
        context = super(PurchaseOrderUpdateView, self).get_context_data(**kwargs)
        object_po = PurchaseOrder.objects.get(pk=self.kwargs["pk"])
        purchased_stock = PurchaseOrderDetail.objects.filter(
            purchase_order=self.object,
            stock__product__archived=False,
        )
        if "formset" not in context:
            context["formset"] = PurchaseOrderDetailFormset(
                instance=self.object, queryset=purchased_stock
            )
        context["formset_helper"] = PurchaseOrderDetailFormsetHelper
        context["object_pk"] = object_po.pk
        return context

    def form_invalid(self, form, formset, err_msg=None):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        if err_msg:
            self.messages.error(err_msg)
        else:
            self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset = PurchaseOrderDetailFormset(self.request.POST, instance=self.object)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = PurchaseOrderDetailFormset(form_params, instance=form.instance)

        with transaction.atomic():
            form.instance.save()

            if formset.is_valid():
                for delete_form in formset.deleted_forms:
                    if delete_form.instance and delete_form.instance.pk:
                        if delete_form.instance.is_received:
                            err_msg = _(
                                f"Tidak bisa menghapus produk {delete_form.instance.stock.product} karena sudah ada penerimaan"
                            )
                            return self.form_invalid(form, formset, err_msg)
                        else:
                            received_order = delete_form.instance.purchase_order.purchase_received.filter(
                                received=False
                            )
                            if received_order:
                                for received in received_order:
                                    if received:
                                        received.received_detail.filter(
                                            stock=delete_form.instance.stock
                                        ).delete()
                                        delete_form.instance.delete()
                                    else:
                                        err_msg = _(
                                            f"Tidak bisa menghapus produk {delete_form.instance.stock.product} karena sudah ada penerimaan"
                                        )
                                        return self.form_invalid(form, formset, err_msg)
                            else:
                                delete_form.instance.delete()

                for item in formset:
                    deleted = item.cleaned_data.get("DELETE")
                    if not deleted and item.cleaned_data:
                        if item.has_changed() and item.instance.pk:
                            stock = item.instance.stock
                            received_orders = (
                                item.instance.purchase_order.purchase_received.filter(
                                    purchase_order=item.instance.purchase_order,
                                    received=True,
                                )
                            )

                            try:
                                received = item.instance.purchase_order.purchase_received.filter(
                                    purchase_order=item.instance.purchase_order,
                                    received=False,
                                )
                                detail_received = (
                                    received.first().received_detail.filter(stock=stock)
                                )
                                purchased_detail = item.save(commit=True)
                                total_received = 0
                                for received_order in received_orders:
                                    try:
                                        received = received_order.received_detail.get(
                                            stock=item.instance.stock
                                        )
                                        converted_qty = uom_conversion(
                                            received.unit,
                                            purchased_detail.unit,
                                            received.quantity,
                                        )
                                        total_received += converted_qty
                                    except Exception:
                                        total_received += 0
                                data = {
                                    "stock": purchased_detail.stock,
                                    "quantity": purchased_detail.quantity
                                    - total_received,
                                    "unit": purchased_detail.unit,
                                    "purchase_cost": purchased_detail.purchase_cost,
                                    "discount_type": purchased_detail.discount_type,
                                    "discount": purchased_detail.discount,
                                }
                                detail_received.update(**data)
                            except Exception:
                                item.save(commit=True)

                        else:
                            item.save(commit=True)

            form.instance.received()

        feed_webhook_signal.send(
            sender=self.__class__,
            instance=form.instance,
            created=False)

        self.messages.success(
            _("Purhcase Order {} was successfully updated.".format(form.instance))
        )
        return super(PurchaseOrderUpdateView, self).form_valid(form)


class PurchaseOrderCancelRemainingItemsView(
    SterlingRoleMixin, MessageMixin, DeleteView
):
    model = PurchaseOrder
    success_url = reverse_lazy("stocks:stocks_purchase_order")
    permission_name = "purchase_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_queryset(self):
        qs = super().get_queryset().exclude(status__in=["canceled", "received"])
        return qs

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.purchase_received.filter(received=True):
            self.object.status = PurchaseOrder.STATUS.received
        else:
            self.object.status = PurchaseOrder.STATUS.closed
        self.object.save()

        for received in self.object.purchase_received.filter(received=False):
            received.delete()
        self.messages.warning(
            _("Purchase Order {} was successfully closed.".format(self.object))
        )
        return HttpResponseRedirect(self.get_success_url())


class PurchaseOrderReceiveWizardView(SterlingRoleMixin, SessionWizardView):
    template_name = {
        STEP_PURCHASE_ORDER_RECEIVE_FORM: "dashboard/stocks/purchase_order_receive.html",
        STEP_BATCH_TRACKED_FORM: "dashboard/stocks/batch_tracked_form.html",
    }
    form_list = [
        (STEP_PURCHASE_ORDER_RECEIVE_FORM, PurchaseOrderReceiveFormset),
        (STEP_BATCH_TRACKED_FORM, BatchFormset),
    ]
    condition_dict = {STEP_BATCH_TRACKED_FORM: purchase_order_has_batch_tracked}
    permission_name = "purchase_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_purchase_order_detail", kwargs={"pk": self.get_object().pk}
        )

    def __init__(self, *args, **kwargs):
        super(PurchaseOrderReceiveWizardView, self).__init__(*args, **kwargs)
        self.batched_products = []

    def get_template_names(self):
        return [self.template_name[self.steps.current]]

    def get_object(self):
        return PurchaseOrder.objects.get(pk=self.kwargs["pk"])

    def get_form_initial(self, step):
        initial_data = []
        if "pk" in self.kwargs and step == STEP_BATCH_TRACKED_FORM:
            data = self.storage.get_step_data(STEP_PURCHASE_ORDER_RECEIVE_FORM)
            total_products = data.get("purchase_order_receive_form-TOTAL_FORMS")
            for i in range(int(total_products)):
                product_id = data.get(
                    "purchase_order_receive_form-" + str(i) + "-product_id"
                )
                quantity = data.get(
                    "purchase_order_receive_form-" + str(i) + "-quantity"
                )
                unit = data.get("purchase_order_receive_form-" + str(i) + "-unit")
                product = Product.objects.get(pk=product_id)
                if (
                    product.is_batch_tracked
                    and product not in self.batched_products
                    and Decimal(quantity) > 0
                ):
                    self.batched_products.append(product)
                    initial_data.append(
                        {
                            "product": product,
                            "product_name": product.name,
                            "quantity_to_assign": "{} {}".format(
                                get_normalized_decimal(Decimal(quantity)), unit
                            ),
                            "total_quantity": quantity,
                            "unit": unit,
                        }
                    )
            return self.instance_dict.get(step, initial_data)
        return super().get_form_initial(step)

    def get_form_instance(self, step):
        instance = super().get_form_instance(step)
        if step == STEP_PURCHASE_ORDER_RECEIVE_FORM:
            instance = self.get_object()
        return instance

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form, **kwargs)
        if self.steps.current == STEP_PURCHASE_ORDER_RECEIVE_FORM:
            context["formset_helper"] = PurchaseOrderReceiveFormsetHelper
            context["object_pk"] = self.get_object().pk

        if self.steps.current == STEP_BATCH_TRACKED_FORM:
            context["inventory_type"] = TRANSACTION_TYPE.purchase_order
            obj = self.get_object()
            context["outlet_pk"] = obj.outlet.pk
            context["outlet"] = obj.outlet
            context["status"] = TRANSACTION_TYPE.purchase_order
            context["status_display"] = TRANSACTION_TYPE["purchase_order"]
        return context

    def get_form(self, step=None, data=None, files=None):
        form = super(PurchaseOrderReceiveWizardView, self).get_form(step, data, files)
        if step == STEP_BATCH_TRACKED_FORM:
            form.extra = len(self.batched_products)
        return form

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step)
        if step == STEP_PURCHASE_ORDER_RECEIVE_FORM:
            purchase_order = PurchaseOrder.objects.get(pk=self.kwargs["pk"])
            kwargs["queryset"] = purchase_order.purchased_stock.filter(
                is_received=False
            )
        elif step == STEP_BATCH_TRACKED_FORM:
            kwargs["status"] = TRANSACTION_TYPE.purchase_order
            kwargs["outlet"] = self.get_object().outlet
        return kwargs

    def post(self, *args, **kwargs):
        wizard_goto_step = self.request.POST.get("wizard_goto_step", None)
        if wizard_goto_step and wizard_goto_step in self.get_form_list():
            return self.render_goto_step(wizard_goto_step)

        management_form = ManagementForm(self.request.POST, prefix=self.prefix)
        if not management_form.is_valid():
            raise SuspiciousOperation(
                _("ManagementForm data is missing or has been tampered.")
            )
        form = self.get_form(data=self.request.POST, files=self.request.FILES)
        if form.is_valid():
            self.storage.set_step_data(self.steps.current, self.process_step(form))
            self.storage.set_step_files(
                self.steps.current, self.process_step_files(form)
            )
            if self.steps.current == self.steps.last:
                return self.render_done(form, **kwargs)
            else:
                if self.steps.current == STEP_PURCHASE_ORDER_RECEIVE_FORM:
                    return self.render_next_step(form)
                else:
                    return self.render_done(form, **kwargs)
        if self.steps.current not in self.steps.all:
            return self.render_done(form, **kwargs)
        return self.render(form)

    def get_next_step(self, step=None):
        if step is None:
            step = self.steps.current
        form_list = self.get_form_list()
        keys = list(form_list.keys())
        if step not in keys:
            return self.steps.first
        key = keys.index(step) + 1
        if len(keys) > key:
            return keys[key]
        return None

    def done(self, form_list, **kwargs):
        purchase_order = self.get_object()
        outlet = purchase_order.outlet
        purchase_order_receive_form = list(form_list)[0]
        if len(form_list) > 1:
            batch_formset = list(form_list)[1]

        with transaction.atomic():
            for receive in purchase_order_receive_form:
                if receive.is_valid():
                    purchased = receive.save(commit=False)
                    received_date = receive.cleaned_data.get("received_date")
                    purchased.received_date = dateutil.parser.parse(
                        received_date, dayfirst=True
                    )
                    purchased.save()

                    quantity = receive.cleaned_data.get("quantity")
                    unit = receive.cleaned_data.get("unit")
                    product = purchased.stock.product
                    purchase_order_detail = receive.cleaned_data.get("id")

                    current_balance = 0
                    for stock in product.stocks.all():
                        current_balance += stock.balance
                    cost_by_uom = uom_cost_conversion(
                        unit,
                        product.uom,
                        quantity,
                        purchased.purchase_cost.amount,
                    )
                    converted_qty = uom_conversion(
                        unit,
                        product.uom,
                        quantity,
                    )
                    total_balance = current_balance + converted_qty
                    if total_balance > 0:
                        avg_cost = (
                            (current_balance * product.moving_average_cost.amount)
                            + (converted_qty * cost_by_uom)
                        ) / (total_balance)
                        product.moving_average_cost.amount = avg_cost
                        product.save()
                    else:
                        product.moving_average_cost.amount = cost_by_uom
                        product.save()
                    receive.instance.process_purchase(
                        quantity=quantity,
                        unit=unit,
                        discount_type=purchase_order_detail.discount_type,
                        discount=purchase_order_detail.discount,
                        user=self.request.user,
                    )
            purchase_order_receive_form.instance.received()

            if len(form_list) > 1:
                for batch_form in batch_formset:
                    if batch_form.is_valid():
                        for item in batch_form.nested:
                            if item.is_valid():
                                batch = None
                                deleted = item.cleaned_data.get("DELETE")
                                batch_id = item.cleaned_data.get("batch_id")
                                quantity = item.cleaned_data.get("quantity")
                                unit = item.cleaned_data.get("unit")
                                converted_qty = uom_conversion(
                                    unit, batch_form.instance.product.uom, quantity
                                )
                                if not deleted:
                                    batch, created = Batch.objects.get_or_create(
                                        batch_id=batch_id,
                                        product=batch_form.instance.product,
                                    )

                                    if created:
                                        expiration_date = datetime.strptime(
                                            item.cleaned_data.get("expiration_date"),
                                            "%d/%m/%Y",
                                        )
                                        batch.expiration_date = expiration_date
                                        batch.save()

                                        batch_item = item.save(commit=False)
                                        batch_item.batch = batch
                                        batch_item.outlet = outlet
                                        batch_item.save()
                                        batch_item.add_batch_balance(
                                            converted_qty,
                                            batch_form.instance.product.uom,
                                            purchase_order_receive_form.instance,
                                            TRANSACTION_TYPE.purchase_order,
                                        )
                                    else:
                                        (
                                            batch_item,
                                            created,
                                        ) = batch.batch_items.get_or_create(
                                            outlet=outlet
                                        )
                                        batch_item.add_batch_balance(
                                            converted_qty,
                                            batch_form.instance.product.uom,
                                            purchase_order_receive_form.instance,
                                            TRANSACTION_TYPE.purchase_order,
                                        )

        feed_webhook_signal.send(
            sender=self.__class__,
            instance=purchase_order,
            created=True)

        return HttpResponseRedirect(self.get_success_url())


class PurchaseOrderDetailCost(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            purchase_order_pk = request.GET.get("purchase_order", "")
            purchase_order = PurchaseOrder.objects.get(pk=purchase_order_pk)

            product_pk = request.GET.get("product", "")
            product = Product.objects.get(pk=product_pk)
            outlet_pk = request.GET.get("outlet", "")
            stock = product.stocks.get(outlet__pk=outlet_pk)

            purchased = purchase_order.purchased_stock.get(
                stock=stock, is_received=False
            )
            response["status"] = "success"
            response["result"]["cost"] = int(purchased.purchase_cost.amount)
            status_code = 200
        except Product.DoesNotExist:
            pass

        return self.render_json_response(response, status=status_code)


class PurchaseOrderDiscountProductView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            discount = request.GET.get("discount", "")
            discount_type = request.GET.get("discount_type", "")
            product_cost = request.GET.get("cost", "")
            product_quantity = request.GET.get("quantity", "")
            total_cost = Decimal(product_quantity) * Decimal(product_cost)

            total_discount = 0
            if discount_type == "percentage":
                total_discount = Decimal(product_cost) * (Decimal(discount) / 100)
                total_discount = total_discount * Decimal(product_quantity)
            else:
                total_discount = Decimal(product_quantity) * Decimal(discount)
            response["status"] = "success"
            response["result"]["total_cost"] = get_normalized_decimal(
                total_cost - total_discount
            )
            status_code = 200
        except InvalidOperation:
            response["status"] = "success"
            status_code = 200
            response["result"]["total_discount"] = 0

        return self.render_json_response(response, status=status_code)


class PurchaseOrderReceivedListJson(
    SterlingRoleMixin, DatatableMixins, BaseDatatableView
):
    columns = [
        "code",
        "received_date",
        "purchase_order.code",
        "purchase_order.outlet.name",
    ]
    order_columns = [
        "code",
        "received_date",
        "purchase_order__code",
        "purchase_order__outlet__name",
    ]
    datetime_col = ["received_date"]
    model = PurchaseOrderReceive
    permission_name = "received_order"

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.filter(purchase_order__pk=self.kwargs["purchase_pk"])
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(purchase_order__outlet__in=outlet_qs)
        return qs

    def render_column(self, row, column):
        if column == "code":
            action_data = {
                "name": row.code,
                "url": reverse_lazy(
                    "stocks:stocks_received_detail",
                    kwargs={"purchase_pk": self.kwargs["purchase_pk"], "pk": row.pk},
                ),
            }
            return action_data
        elif column == "received_date":
            fmt = "%d-%m-%Y %H:%M"
            date = row.received_date.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            return localtz.strftime(fmt)
        else:
            return super().render_column(row, column)


class PurchaseOrderReceivedListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/purchase_order_received_list.html"
    permission_name = "received_order"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        purchase_order = get_object_or_404(
            PurchaseOrder, pk=self.kwargs.get("purchase_pk")
        )
        context["purchase_order"] = purchase_order
        context["can_receive"] = purchase_order.status in [
            PurchaseOrder.STATUS.active,
            PurchaseOrder.STATUS.partial,
        ]
        return context


class PurchaseOrderReceiveCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    model = PurchaseOrderReceive
    form_class = PurchaseReceiveOrderForm
    template_name = "dashboard/stocks/purchase_order_receive_form.html"
    permission_name = "purchase_order"

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_purchase_order_received",
            kwargs={"purchase_pk": self.object.purchase_order.pk},
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        purchase_order = get_object_or_404(
            PurchaseOrder, pk=self.kwargs.get("purchase_pk")
        )
        context["purchase_order"] = purchase_order
        if "formset" not in context:
            context["formset"] = PurchaseOrderReceiveDetailFormset(
                form_kwargs={"purchase_order": purchase_order.pk}
            )
        context["formset_helper"] = PurchaseOrderReceiveDetailFormsetHelper
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        purchase_order = get_object_or_404(
            PurchaseOrder, pk=self.kwargs.get("purchase_pk")
        )
        kwargs["purchase_order"] = purchase_order.pk
        return kwargs

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        purchase_order = get_object_or_404(
            PurchaseOrder, pk=self.kwargs.get("purchase_pk")
        )
        formset = PurchaseOrderReceiveDetailFormset(
            request.POST, form_kwargs={"purchase_order": purchase_order.pk}
        )
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        purchase_order = get_object_or_404(
            PurchaseOrder, pk=self.kwargs.get("purchase_pk")
        )
        formset = PurchaseOrderReceiveDetailFormset(
            form_params,
            instance=form.instance,
            form_kwargs={"purchase_order": purchase_order.pk},
        )

        with transaction.atomic():
            self.object = form.save(commit=True)
            if formset.is_valid():
                for item in formset:
                    if item.has_changed():
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted and item.cleaned_data:
                            received = item.save(commit=True)
                            received.received_product(self.request.user)
            self.object.purchase_order.received()

        self.messages.success(_("Purchase order receive at {}.".format(form.instance)))
        return super(PurchaseOrderReceiveCreateView, self).form_valid(form)


class PurchaseOrderProductDetailView(
    SterlingRoleMixin, AjaxResponseMixin, JSONResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            purchase_order_pk = request.GET.get("purchase_order", "")
            stock_id = request.GET.get("stock", "")
            purchase_order = PurchaseOrder.objects.get(pk=purchase_order_pk)
            purchase_order_detial = purchase_order.purchased_stock.get(stock=stock_id)
            quantity = get_normalized_decimal(purchase_order_detial.quantity)
            total_received = 0
            received = purchase_order.purchase_received.all()
            for receive in received:
                try:
                    receive = receive.received_detail.get(stock=stock_id)
                    converted_qty = uom_conversion(
                        receive.unit, purchase_order_detial.unit, receive.quantity
                    )
                    total_received += converted_qty
                except PurchaseOrderReceiveDetail.DoesNotExist:
                    pass
            response["result"]["total_qty"] = f"{quantity} {purchase_order_detial.unit}"
            remain_qty = purchase_order_detial.quantity - total_received
            remain_qty = get_normalized_decimal(remain_qty)
            units = UOM.objects.filter(
                category=purchase_order_detial.unit.category
            ).values("pk", "unit")
            response["result"]["product"] = (
                purchase_order_detial.stock.product.__str__(),
            )
            response["result"][
                "total_remain_qty"
            ] = f"{remain_qty} {purchase_order_detial.unit}"
            response["result"]["unit"] = purchase_order_detial.unit.pk
            response["result"]["units"] = list(units)
            response["result"]["remain_qty"] = remain_qty
            response["status"] = "success"
            status_code = 200
        except (
            PurchaseOrder.DoesNotExist,
            PurchaseOrderDetail.DoesNotExist,
            ValueError,
        ):
            response["status"] = "success"
            status_code = 200
        return self.render_json_response(response, status=status_code)


class PurchaseOrderReceiveUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    model = PurchaseOrderReceive
    form_class = PurchaseReceiveOrderForm
    template_name = "dashboard/stocks/purchase_order_receive_form.html"
    permission_name = "received_order"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_success_url(self):
        return reverse_lazy(
            "stocks:stocks_purchase_order_received",
            kwargs={"purchase_pk": self.object.purchase_order.pk},
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["purchase_order"] = self.object.purchase_order
        context["received_detail"] = self.object.received_detail.all()
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        purchase_order = get_object_or_404(
            PurchaseOrder, pk=self.kwargs.get("purchase_pk")
        )
        kwargs["purchase_order"] = purchase_order.pk
        return kwargs

    def form_invalid(self, form):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        self.object = form.save(commit=True)

        self.messages.success(
            _(
                "Purchase order receive {} was successfully updated.".format(
                    form.instance
                )
            )
        )
        return super(PurchaseOrderReceiveUpdateView, self).form_valid(form)
