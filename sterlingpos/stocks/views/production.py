from django.views.generic import (
    TemplateView,
)

from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _
from django.utils.text import slugify

from datetime import datetime
from dateutil.relativedelta import relativedelta

from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.catalogue.models import Product
from sterlingpos.outlet.models import Outlet
from .stock_transaction import StockTransactionListJson


class StockProductionTransactionJsonList(StockTransactionListJson):
    def __init__(self, *args, **kwargs):
        super(StockProductionTransactionJsonList, self).__init__(*args, **kwargs)
        self.start_date_name = ""
        self.end_date_name = ""

    def get_initial_queryset(self):
        product_pk = self.kwargs.get("product_pk")
        qs = super(StockProductionTransactionJsonList, self).get_initial_queryset()
        qs = qs.filter(stock__product__pk=product_pk)
        qs = qs.filter(
            quantity__lt=0,
            transaction_type="productions",
            reference__0__fields__automatic_production=False,
        )

        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = today + relativedelta(day=1)
        self.start_date_name = start_date.strftime("%Y/%m/%d--%H.%M.%S")

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = today + relativedelta(day=1, months=+1, days=-1)
        self.end_date_name = end_date.strftime("%Y/%m/%d--%H.%M.%S")

        outlet_pk = self.kwargs.get("outlet_pk")

        qs = qs.filter(created__range=[start_date, end_date])
        if outlet_pk:
            qs = qs.filter(stock__outlet__pk=outlet_pk)
        return qs

    def get_csv_filename(self):
        product_pk = self.kwargs.get("product_pk")
        product = Product.objects.get(pk=product_pk)
        csv_filename = "{}_Stock_production".format(product.name)
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get("outlet_pk"):
            outlet = get_object_or_404(outlet_qs, pk=self.kwargs.get("outlet_pk"))
            csv_filename = "{}_{}".format(csv_filename, slugify(outlet.name))

        if self.start_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.start_date_name)
        if self.end_date_name:
            csv_filename = "{}_{}".format(csv_filename, self.end_date_name)

        return csv_filename


class StockProductionTransactionListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/transaction_stock_production_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["product_pk"] = self.kwargs.get("product_pk")
        context["outlet_pk"] = self.kwargs.get("outlet_pk")
        if context["outlet_pk"]:
            context["outlet"] = Outlet.objects.get(pk=context["outlet_pk"])
        context["start_date"] = self.request.GET.get("start_date", None)
        context["end_date"] = self.request.GET.get("end_date", None)
        return context
