import rules
import xlwt

from django.views.generic import (
    TemplateView,
    View,
    CreateView,
    UpdateView,
    DetailView,
)

from django.urls import reverse_lazy
from django.utils.translation import ugettext as _
from django.db import transaction
from django.http import HttpResponse

from datetime import datetime

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    MessageMixin,
)

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.catalogue.models import Category
from sterlingpos.stocks.models import (
    Supplier,
    PurchaseOrderReceive,
    VendorBill,
)
from sterlingpos.stocks.forms import (
    VendorBillForm,
    VendorBillUpdateForm,
    VendorBillDetailFormset,
    VendorBillDetailFormsetHelper,
)
from sterlingpos.catalogue.utils import get_normalized_decimal

from .mixin import PDFExportMixin


class VendorBillListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "code",
        "bill_date",
        "due_date",
        "received_order.code",
        "received_order.purchase_order.supplier.name",
        "received_order.purchase_order.outlet.name",
    ]
    order_columns = [
        "code",
        "bill_date",
        "due_date",
        "received_order__code",
        "received_order__purchase_order__supplier__pk",
        "received_order__purchase_order__outlet__pk",
    ]
    datetime_col = ["bill_date", "due_date"]
    model = VendorBill
    permission_name = "vendor_bill"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(received_order__purchase_order__outlet__in=outlet_qs)

        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            start_date = None

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None), "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError):
            end_date = None

        if start_date:
            qs = qs.filter(bill_date__gte=start_date)
        if end_date:
            qs = qs.filter(bill_date__lte=end_date)
        return qs

    def get_data(self):
        data = []
        bills = self.get_initial_queryset()
        for bill in bills:
            ref = "PI - {} - {}".format(
                bill.received_order.purchase_order.supplier.name,
                bill.received_order.code,
            )
            categories = bill.vendor_bill_detail.values_list(
                "product__category", flat=True
            ).distinct()
            for i, category in enumerate(categories):
                details = bill.vendor_bill_detail.filter(product__category=category)
                category = Category.objects.get(pk=category)
                total = 0

                for detail in details:
                    total += detail.total_cost
                if i == 0:
                    data.append(
                        [
                            bill.code,
                            bill.bill_date,
                            ref,
                            "",
                            category.name,
                            total,
                            "",
                            ref,
                            "",
                            "",
                            "",
                            "",
                            "IDR",
                            "1",
                            "",
                            "",
                        ]
                    )
                else:
                    data.append(
                        [
                            "",
                            "",
                            "",
                            "",
                            category.name,
                            total,
                            "0",
                            ref,
                            "",
                            "",
                            "",
                            "",
                            "IDR",
                            "1",
                            "",
                            "",
                        ]
                    )
            data.append(
                [
                    "",
                    "",
                    "",
                    "",
                    bill.received_order.purchase_order.supplier.name,
                    "0",
                    bill.total_cost,
                    ref,
                    "",
                    "",
                    "",
                    bill.received_order.purchase_order.supplier.code,
                    "IDR",
                    "1",
                    "",
                    "",
                ]
            )
        return data

    def get(self, request, *args, **kwargs):
        if request.GET.get("export_file") == "excel":
            response = HttpResponse(content_type="application/ms-excel")
            response["Content-Disposition"] = 'attachment; filename="vendor_bill.xls"'

            wb = xlwt.Workbook(encoding="utf-8")
            ws = wb.add_sheet("Vendor Bill")
            row_num = 0
            font_style = xlwt.XFStyle()
            font_style.font.bold = True
            columns = [
                "JV No",
                "JV Date",
                "JV Description",
                "Account No",
                "Account Name",
                "Debit",
                "Credit",
                "Memo",
                "Department No",
                "Project No",
                "Customer No",
                "Vendor No",
                "Currency",
                "Rate",
                "Prime Debit",
                "Prime Credit",
            ]
            for col_num in range(len(columns)):
                ws.write(row_num, col_num, columns[col_num], font_style)

            font_style = xlwt.XFStyle()

            rows = self.get_data()
            for row in rows:
                row_num += 1
                for col_num in range(len(row)):
                    ws.write(row_num, col_num, row[col_num], font_style)

            wb.save(response)
            return response
        else:
            return super().get(request, *args, **kwargs)

    def render_column(self, row, column):
        if column == "code":
            action_data = {
                "name": row.code,
                "url": reverse_lazy(
                    "stocks:stocks_vendor_bill_detail", kwargs={"pk": row.pk}
                ),
            }
            return action_data
        else:
            return super().render_column(row, column)


class VendorBillListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/stocks/vendor_bill_list.html"
    permission_name = "vendor_bill"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        context["suppliers"] = Supplier.objects.all()
        return context


class VendorBillCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    model = VendorBill
    form_class = VendorBillForm
    template_name = "dashboard/stocks/vendor_bill_form.html"
    permission_name = "vendor_bill"
    success_url = reverse_lazy("stocks:stocks_vendor_bill")

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = VendorBillDetailFormset
        context["formset_helper"] = VendorBillDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        formset = VendorBillDetailFormset(request.POST)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = VendorBillDetailFormset(
            form_params,
            instance=form.instance,
        )

        with transaction.atomic():
            self.object = form.save(commit=True)
            if formset.is_valid():
                for item in formset:
                    if item.has_changed():
                        if item.cleaned_data:
                            item.save(commit=True)

        self.messages.success(
            _("Vendor Bill {} was successfully created.".format(form.instance))
        )
        return super(VendorBillCreateView, self).form_valid(form)


class VendorBillDetailViewJson(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    permission_name = "vendor_bill"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            received_order_pk = request.GET.get("received_order", "")
            received = PurchaseOrderReceive.objects.get(pk=received_order_pk)
            received_data = []
            for received_detail in received.received_detail.all():
                data = {
                    "product_name": received_detail.stock.product.__str__(),
                    "product": received_detail.stock.product.pk,
                    "quantity": get_normalized_decimal(received_detail.quantity),
                    "unit": received_detail.unit.unit,
                    "purchase_cost": get_normalized_decimal(
                        received_detail.purchase_cost.amount
                    ),
                }
                received_data.append(data)
            response["result"]["received_data"] = received_data
            response["status"] = "success"
            status_code = 200
        except PurchaseOrderReceive.DoesNotExist:
            response["status"] = "success"
            status_code = 200
            response["result"]["received_data"] = []

        return self.render_json_response(response, status=status_code)


class VendorBillDetailView(SterlingRoleMixin, PDFExportMixin, DetailView):
    template_name = "dashboard/stocks/vendor_bill.html"
    model = VendorBill
    pdf_template = "dashboard/stocks/pdf/vendor_bill.html"
    permission_name = "vendor_bill"

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["vendor_bill_detail"] = self.object.vendor_bill_detail.all()
        return context

    def get_pdf_data(self):
        data = {
            "object": self.get_object(),
            "vendor_bill_detail": self.get_object()
            .vendor_bill_detail.all()
            .select_related("product", "product__catalogue", "unit"),
            "base_url": self.request.build_absolute_uri("/"),
            "logo": self.request.user.account.brand_logo,
            "account": self.request.user.account,
        }
        return data


class VendorBillUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    model = VendorBill
    form_class = VendorBillUpdateForm
    template_name = "dashboard/stocks/vendor_bill_form.html"
    permission_name = "vendor_bill"
    success_url = reverse_lazy("stocks:stocks_vendor_bill")

    def test_func(self, user):
        permission_name = self.permission_name or self.get_permission_name()
        has_permission = rules.test_rule("can_access_view", user, permission_name)
        return has_permission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = VendorBillDetailFormset(instance=self.object)
        context["formset_helper"] = VendorBillDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset = VendorBillDetailFormset(request.POST, instance=self.object)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            """There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = VendorBillDetailFormset(form_params, instance=form.instance)

        with transaction.atomic():
            self.object = form.save(commit=True)
            if formset.is_valid():
                for item in formset:
                    if item.has_changed():
                        if item.cleaned_data:
                            item.save(commit=True)

        self.messages.success(
            _("Vendor Bill {} was successfully updated.".format(form.instance))
        )
        return super(VendorBillUpdateView, self).form_valid(form)
