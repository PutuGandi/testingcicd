import pytz
import csv

from django.utils import timezone
from django.views.generic import (
    FormView,
)

from django.utils.translation import ugettext as _
from django.http import StreamingHttpResponse
from django.apps import apps

from datetime import datetime
from dateutil.relativedelta import relativedelta

from sterlingpos.core.mixins import Echo
from sterlingpos.role.mixins import SterlingRoleMixin

from sterlingpos.catalogue.utils import uom_conversion
from sterlingpos.stocks.models import (
    StockTransfer,
    StockCount,
    StockAdjustment,
)
from sterlingpos.stocks.forms import InventoryFilterReportForm
from sterlingpos.catalogue.utils import get_normalized_decimal


class InventoryReportView(SterlingRoleMixin, FormView):
    template_name = "dashboard/stocks/inventory_report.html"
    form_class = InventoryFilterReportForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs[
            "dashboard_permission"
        ] = self.request.user.device_user.role.dashboard_permission
        kwargs["all_outlet_access"] = self.request.user.device_user.all_outlet_access
        return kwargs

    def set_append_detail_data(self, q, data, report_type):
        fmt = "%d/%m/%Y %H:%M"
        if "stocktransfer" in report_type:
            date = q.stock_transfer.date_of_transfer.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            arrival_date = q.stock_transfer.date_of_arrival
            arrival_date = q.stock_transfer.date_of_arrival
            str_local_arrival = None
            if arrival_date:
                arrival_date = arrival_date.replace(tzinfo=pytz.UTC)
                localtz_arrival = arrival_date.astimezone(
                    timezone.get_current_timezone())
                str_local_arrival = localtz_arrival.strftime(fmt)

            data_row = [
                q.stock_transfer.source.name,
                q.stock_transfer.destination.name,
                localtz.strftime(fmt),
                str_local_arrival,
                q.stock_transfer.code,
                get_normalized_decimal(q.quantity),
                q.unit.unit,
                StockTransfer.STATUS[q.stock_transfer.status],
            ]
            data.append(data_row)
        if "stockcount" in report_type:
            date = q.stock_count.date_of_stock_count.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            data_row = [
                q.stock_count.outlet.name,
                localtz.strftime(fmt),
                q.stock_count.code,
                q.get_last_stock_before_count,
                q.stock.product.uom.unit,
                get_normalized_decimal(q.counted),
                q.unit.unit,
                StockCount.STATUS[q.stock_count.status],
            ]
            data.append(data_row)
        if "stockadjustment" in report_type:
            date = q.stock_adjustment.date_of_adjustment.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            if q.stock_adjustment.status in [StockAdjustment.STATUS.item_receive]:
                total_cost = get_normalized_decimal(q.total_cost)
                quantity = get_normalized_decimal(q.quantity)
            else:
                if q.stock_adjustment.status in [StockAdjustment.STATUS.stock_count]:
                    quantity = "({})".format(get_normalized_decimal(q.quantity))
                else:
                    quantity = get_normalized_decimal(q.quantity * -1)
                total_cost = "-"

            data_row = [
                q.stock_adjustment.outlet.name,
                localtz.strftime(fmt),
                q.stock_adjustment.code,
                StockAdjustment.STATUS[q.stock_adjustment.status],
                quantity,
                q.unit.unit,
                total_cost,
            ]
            data.append(data_row)
        if "purchaseorderdetail" in report_type:
            date = q.purchase_order.date_of_purchase.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            data_row = [
                q.purchase_order.outlet.name,
                q.purchase_order.supplier.name,
                localtz.strftime(fmt),
                q.purchase_order.code,
                get_normalized_decimal(q.quantity),
                q.unit.unit,
                get_normalized_decimal(q.total_cost),
            ]
            data.append(data_row)
        if "purchaseorderreceivedetail" in report_type:
            date = q.received.received_date.replace(tzinfo=pytz.UTC)
            localtz = date.astimezone(timezone.get_current_timezone())
            data_row = [
                q.received.purchase_order.outlet.name,
                q.received.purchase_order.supplier.name,
                localtz.strftime(fmt),
                q.received.code,
                get_normalized_decimal(q.quantity),
                q.unit.unit,
                get_normalized_decimal(q.total_cost),
            ]
            data.append(data_row)
        if "vendorbill" in report_type:
            data_row = [
                q.vendor_bill.received_order.purchase_order.outlet.name,
                q.vendor_bill.received_order.purchase_order.supplier.name,
                q.vendor_bill.bill_date,
                q.vendor_bill.code,
                get_normalized_decimal(q.quantity),
                q.unit.unit,
                get_normalized_decimal(q.total_cost),
            ]
            data.append(data_row)

    def convert_query_to_dict(self, queryset, report_type):
        data = []
        product = None
        total_cost = 0
        total_qty = 0
        if "stocktransfer" in report_type:
            queryset = queryset.select_related(
                "stock_transfer",
                "stock_transfer__source",
                "stock_transfer__destination",
                "stock_source",
                "unit",
                "stock_source__product",
                "stock_source__product__catalogue",
                "stock_source__product__uom",
            )
            for q in queryset:
                if product == q.stock_source.product:
                    total_qty += uom_conversion(q.unit, product.uom, q.quantity)
                    self.set_append_detail_data(q, data, report_type)
                else:
                    if product:
                        data.append(
                            [
                                "",
                                "",
                                "",
                                "",
                                "",
                                get_normalized_decimal(total_qty),
                                product.uom.unit,
                                "",
                            ]
                        )
                    product = q.stock_source.product
                    total_qty = uom_conversion(q.unit, product.uom, q.quantity)
                    data.append([q.stock_source.product, "", "", "", "", "", ""])
                    self.set_append_detail_data(q, data, report_type)
            if product:
                data.append(
                    [
                        "",
                        "",
                        "",
                        "",
                        "",
                        get_normalized_decimal(total_qty),
                        product.uom.unit,
                        "",
                    ]
                )

        if "stockcount" in report_type:
            queryset = queryset.select_related(
                "stock_count",
                "unit",
                "stock",
                "stock__product",
                "stock__product__catalogue",
                "stock__product__catalogue__uom",
                "stock__product__uom",
            ).prefetch_related(
                "stock__transactions",
                "stock__transactions__unit",
            )
            for q in queryset:
                if product == q.stock.product:
                    self.set_append_detail_data(q, data, report_type)
                else:
                    product = q.stock.product
                    data.append([q.stock.product, "", "", "", "", "", "", ""])
                    self.set_append_detail_data(q, data, report_type)

        if "stockadjustment" in report_type:
            queryset = queryset.select_related(
                "stock",
                "unit",
                "stock_adjustment",
                "stock__product",
                "stock__product__uom",
                "stock__product__catalogue",
                "stock__product__catalogue__uom",
            )
            for q in queryset:
                if product == q.stock.product:
                    self.set_append_detail_data(q, data, report_type)
                else:
                    product = q.stock.product
                    data.append([q.stock.product, "", "", "", "", "", ""])
                    self.set_append_detail_data(q, data, report_type)

        if "purchaseorderdetail" in report_type:
            queryset = queryset.select_related(
                "stock",
                "unit",
                "purchase_order",
                "purchase_order__supplier",
                "purchase_order__outlet",
                "stock__product",
                "stock__product__uom",
                "stock__product__catalogue",
                "stock__product__catalogue__uom",
            )
            for q in queryset:
                if product == q.stock.product:
                    total_qty += uom_conversion(q.unit, product.uom, q.quantity)
                    total_cost += q.total_cost
                    self.set_append_detail_data(q, data, report_type)
                else:
                    if product:
                        data.append(
                            [
                                "",
                                "",
                                "",
                                "",
                                get_normalized_decimal(total_qty),
                                product.uom.unit,
                                get_normalized_decimal(total_cost),
                            ]
                        )
                    product = q.stock.product
                    total_qty = uom_conversion(q.unit, product.uom, q.quantity)
                    total_cost = q.total_cost
                    data.append([q.stock.product, "", "", "", "", "", ""])
                    self.set_append_detail_data(q, data, report_type)
            if product:
                data.append(
                    [
                        "",
                        "",
                        "",
                        "",
                        get_normalized_decimal(total_qty),
                        product.uom.unit,
                        get_normalized_decimal(total_cost),
                    ]
                )

        if "purchaseorderreceivedetail" in report_type:
            queryset = queryset.select_related(
                "received",
                "received__purchase_order__outlet",
                "received__purchase_order__supplier",
                "unit",
                "stock",
                "stock__product",
                "stock__product__catalogue",
                "stock__product__catalogue__uom",
                "stock__product__uom",
            )
            for q in queryset:
                if product == q.stock.product:
                    total_qty += uom_conversion(q.unit, product.uom, q.quantity)
                    total_cost += q.total_cost
                    self.set_append_detail_data(q, data, report_type)
                else:
                    if product:
                        data.append(
                            [
                                "",
                                "",
                                "",
                                "",
                                get_normalized_decimal(total_qty),
                                product.uom.unit,
                                get_normalized_decimal(total_cost),
                            ]
                        )
                    product = q.stock.product
                    total_qty = uom_conversion(q.unit, product.uom, q.quantity)
                    total_cost = q.total_cost
                    data.append([q.stock.product, "", "", "", "", "", ""])
                    self.set_append_detail_data(q, data, report_type)
            if product:
                data.append(
                    [
                        "",
                        "",
                        "",
                        "",
                        get_normalized_decimal(total_qty),
                        product.uom.unit,
                        get_normalized_decimal(total_cost),
                    ]
                )

        if "vendorbill" in report_type:
            queryset = queryset.select_related(
                "unit",
                "product",
                "product__uom",
                "product__catalogue",
                "product__catalogue__uom",
                "vendor_bill",
                "vendor_bill__received_order",
                "vendor_bill__received_order__purchase_order",
                "vendor_bill__received_order__purchase_order__supplier",
                "vendor_bill__received_order__purchase_order__outlet",
            )
            for q in queryset:
                if product == q.product:
                    total_qty += uom_conversion(q.unit, product.uom, q.quantity)
                    total_cost += q.total_cost
                    self.set_append_detail_data(q, data, report_type)
                else:
                    if product:
                        data.append(
                            [
                                "",
                                "",
                                "",
                                "",
                                get_normalized_decimal(total_qty),
                                product.uom.unit,
                                get_normalized_decimal(total_cost),
                            ]
                        )
                    product = q.product
                    total_qty = uom_conversion(q.unit, product.uom, q.quantity)
                    total_cost = q.total_cost
                    data.append([q.product, "", "", "", "", "", ""])
                    self.set_append_detail_data(q, data, report_type)
            if product:
                data.append(
                    [
                        "",
                        "",
                        "",
                        "",
                        get_normalized_decimal(total_qty),
                        product.uom.unit,
                        get_normalized_decimal(total_cost),
                    ]
                )
        return data

    def obj_filter_order_date(self, report_type, start_date, end_date):
        filter_date = {}
        ordered_date = ""
        if "stocktransfer" in report_type:
            end_date = end_date + relativedelta(hour=23, minute=59, second=59)
            filter_date = {
                "stock_transfer__date_of_arrival__range": [start_date, end_date],
                "stock_source__product__is_manage_stock": True,
                "stock_source__product__uom__isnull": False
            }
            ordered_date = "stock_transfer__date_of_arrival"
        if "stockcount" in report_type:
            end_date = end_date + relativedelta(hour=23, minute=59, second=59)
            filter_date = {
                "stock_count__date_of_stock_count__range": [start_date, end_date],
                "stock__product__is_manage_stock": True,
                "stock__product__uom__isnull": False
            }
            ordered_date = "stock_count__date_of_stock_count"
        if "stockadjustment" in report_type:
            end_date = end_date + relativedelta(hour=23, minute=59, second=59)
            filter_date = {
                "stock_adjustment__date_of_adjustment__range": [start_date, end_date],
                "stock__product__is_manage_stock": True,
                "stock__product__uom__isnull": False
            }
            ordered_date = "stock_adjustment__date_of_adjustment"
        if "purchaseorderdetail" in report_type:
            end_date = end_date + relativedelta(hour=23, minute=59, second=59)
            filter_date = {
                "purchase_order__date_of_purchase__range": [start_date, end_date],
                "stock__product__is_manage_stock": True,
                "stock__product__uom__isnull": False
            }
            ordered_date = "purchase_order__date_of_purchase"
        if "purchaseorderreceivedetail" in report_type:
            end_date = end_date + relativedelta(hour=23, minute=59, second=59)
            filter_date = {
                "received__received_date__range": [start_date, end_date],
                "stock__product__is_manage_stock": True,
                "stock__product__uom__isnull": False
            }
            ordered_date = "received__received_date"
        if "vendorbill" in report_type:
            filter_date = {
                "vendor_bill__bill_date__range": [start_date, end_date],
            }
            ordered_date = "vendor_bill__bill_date"
        return {
            'filter_date': filter_date,
            'ordered_date': ordered_date
        }

    def get_title_csv_header(self, report_type):
        report_title = ""
        csv_header = []
        if "stocktransfer" in report_type:
            report_title = "Mutasi Stok"
            csv_header = [
                "Outlet Asal",
                "Outlet Tujuan",
                "Tanggal Mutasi",
                "Tanggal Sampai",
                "Referensi",
                "Jumlah",
                "Unit",
                "Status",
            ]
        if "stockcount" in report_type:
            report_title = "Stok Opname"
            csv_header = [
                "Outlet",
                "Tanggal Stok Opname",
                "Referensi",
                "Stok Awal",
                "Unit",
                "Terhitung",
                "Unit",
                "Status",
            ]
        if "stockadjustment" in report_type:
            report_title = "Penyesuaian Stok"
            csv_header = [
                "Outlet",
                "Tanggal Penyesuaian",
                "Referensi",
                "Status Order",
                "Penyesuaian",
                "Unit",
                "Total",
            ]
        if "purchaseorderdetail" in report_type:
            report_title = "Purchase Order"
            csv_header = [
                "Outlet",
                "Pemasok",
                "Tanggal Purchase Order",
                "Referensi",
                "Jumlah",
                "Unit",
                "Total",
            ]
        if "purchaseorderreceivedetail" in report_type:
            report_title = "Received Order"
            csv_header = [
                "Outlet",
                "Pemasok",
                "Tanggal Penerimaan",
                "Referensi",
                "Jumlah",
                "Unit",
                "Total",
            ]
        if "vendorbill" in report_type:
            report_title = "Vendor Bill"
            csv_header = [
                "Outlet",
                "Pemasok",
                "Bill Date",
                "Referensi",
                "Jumlah",
                "Unit",
                "Total",
            ]
        return {
            "report_title": report_title,
            "csv_header": csv_header
        }

    def obj_filter_products(self, report_type, products):
        filter_product = {}
        if "stocktransfer" in report_type:
            filter_product = {"stock_source__product__in": products}
        elif "vendorbill" in report_type:
            filter_product = {"product__in": products}
        else:
            filter_product = {"stock__product__in": products}
        return filter_product

    def obj_filter_suppliers(self, report_type, suppliers):
        if "purchaseorderdetail" in report_type:
            filter_suppliers = {"purchase_order__supplier__in": suppliers}
        if "purchaseorderreceivedetail" in report_type:
            filter_suppliers = {
                "received__purchase_order__supplier__in": suppliers}
        if "vendorbill" in report_type:
            filter_suppliers = {
                "vendor_bill__received_order__purchase_order__supplier__in": 
                    suppliers
            }
        return filter_suppliers

    def obj_filter_outlets(self, report_type, outlets):
        filter_outlets = {}
        if "stocktransfer" in report_type:
            filter_outlets = {"stock_transfer__source__in": outlets}
        if "stockcount" in report_type:
            filter_outlets = {"stock_count__outlet__in": outlets}
        if "stockadjustment" in report_type:
            filter_outlets = {"stock_adjustment__outlet__in": outlets}
        if "purchaseorderdetail" in report_type:
            filter_outlets = {"purchase_order__outlet__in": outlets}
        if "purchaseorderreceivedetail" in report_type:
            filter_outlets = {"received__purchase_order__outlet__in": outlets}
        if "vendorbill" in report_type:
            filter_outlets = {
                "vendor_bill__received_order__purchase_order__outlet__in": outlets
            }
        return filter_outlets

    def form_valid(self, form):
        range_date = form.cleaned_data.get("range_date")
        range_date = range_date.split(" - ")
        start_date = datetime.strptime(range_date[0], "%d/%m/%Y")
        end_date = datetime.strptime(range_date[1], "%d/%m/%Y")
        products = form.cleaned_data.get("products")
        suppliers = form.cleaned_data.get("suppliers")
        outlets = form.cleaned_data.get("outlets")
        report_type = form.cleaned_data.get("report_type")

        queryset = apps.get_model(report_type).objects.all()

        obj_title_csv_header = self.get_title_csv_header(report_type)
        filtered_by = []

        obj_filter_date = self.obj_filter_order_date(
            report_type=report_type, start_date=start_date, end_date=end_date)
        queryset = queryset.filter(**obj_filter_date['filter_date'])
        filtered_by.append("Tanggal")

        if products:
            filter_product = self.obj_filter_products(report_type, products)
            queryset = queryset.filter(**filter_product)
            filtered_by.append("Produk")

        if suppliers:
            filter_suppliers = self.obj_filter_suppliers(report_type, suppliers)
            queryset = queryset.filter(**filter_suppliers)
            filtered_by.append("Supplier")

        if outlets:
            filter_outlets = self.obj_filter_outlets(report_type, outlets)
            queryset = queryset.filter(**filter_outlets)
            filtered_by.append("Outlet")

        if "stocktransfer" in report_type:
            queryset = queryset.order_by(
                "stock_source__product__name", obj_filter_date['ordered_date'])
        elif "vendorbill" in report_type:
            queryset = queryset.order_by(
                "product__name", obj_filter_date['ordered_date'])
        else:
            queryset = queryset.order_by(
                "stock__product__name", obj_filter_date['ordered_date'])

        file_name = "Inventory Report - {} - {}---{}.pdf".format(
            obj_title_csv_header['report_title'], 
            start_date.date(), end_date.date()
        )

        pseudo_buffer = Echo()
        writer = csv.writer(pseudo_buffer)

        csv_data = [obj_title_csv_header['csv_header']] + \
            self.convert_query_to_dict(queryset, report_type)
        response = StreamingHttpResponse(
            (writer.writerow(row) for row in csv_data), content_type="text/csv"
        )

        response["Content-Disposition"] = "attachment; filename={}.csv".format(
            file_name
        )

        return response
