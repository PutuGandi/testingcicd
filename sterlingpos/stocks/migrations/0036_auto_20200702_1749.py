# Generated by Django 2.2.12 on 2020-07-02 10:49

from django.db import migrations

def initial_supplier_code(apps, schema_editor):
    Account = apps.get_model("accounts", "Account")
    Supplier = apps.get_model("stocks", "Supplier")

    accounts = Account.objects.all()
    for account in accounts:
        for num, supplier in enumerate(Supplier.objects.filter(account=account, deleted__isnull=True).order_by("pk"), start=1):
            supplier.code = "SUPPLIER-{}".format(num)
            supplier.save()


class Migration(migrations.Migration):

    dependencies = [
        ('stocks', '0035_purchaseorderdetail_received_date'),
    ]

    operations = [
        migrations.RunPython(initial_supplier_code, migrations.RunPython.noop),
    ]
