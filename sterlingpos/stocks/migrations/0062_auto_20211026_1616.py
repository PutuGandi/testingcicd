# Generated by Django 2.2.16 on 2021-10-26 09:16
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0012_account_business_code'),
        ('stocks', '0061_auto_20210617_1419'),
    ]

    operations = [
        
        migrations.RunSQL(
            sql="DELETE FROM stocks_hourlystocktransaction WHERE deleted is not NULL",
            reverse_sql=migrations.RunSQL.noop,
        ),
        migrations.RunSQL(
            sql="""DELETE FROM stocks_hourlystocktransaction
                    WHERE id IN ( 
                        SELECT id FROM (
                            SELECT 
                                id,
                                account_id, outlet_id, stock_id, unit_id,
                                aggregated_time,
                                quantity,
                                transaction_type,
                                auto_production,
                                ROW_NUMBER() OVER(
                                    PARTITION BY 
                                        account_id, outlet_id, stock_id, unit_id,
                                        aggregated_time,
                                        transaction_type,
                                        auto_production ORDER BY stock_id)
                                AS row_num
                            FROM stocks_hourlystocktransaction
                            WHERE deleted is NULL) as temporal
                        WHERE temporal.row_num > 1);
            """,
            reverse_sql=migrations.RunSQL.noop,
        ),

        migrations.AlterUniqueTogether(
            name='hourlystocktransaction',
            unique_together={('account', 'id')},
        ),
        migrations.AddConstraint(
            model_name='hourlystocktransaction',
            constraint=models.UniqueConstraint(
                condition=models.Q(deleted__isnull=True),
                fields=('account', 'transaction_type', 'auto_production', 'aggregated_time', 'stock', 'outlet', 'unit'),
                name='unique_hstock_trans_cons'),
        ),

        migrations.RunSQL(
            sql="""CREATE OR REPLACE FUNCTION public.do_hourly_stock_transaction_aggregation_for_date_range(
                IN start_date timestamp with time zone,
                IN end_date timestamp with time zone)
                RETURNS void
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE PARALLEL UNSAFE
            AS $BODY$
            BEGIN
                INSERT INTO stocks_hourlystocktransaction (
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time, quantity,
                    transaction_type, auto_production,
                    created, modified)
                SELECT 
                        account_id, outlet_id, stock_id, unit_id,
                        date_trunc('hour', created) as aggregated_time,
                        sum(quantity) as quantity,
                        transaction_type,
                        CASE
                        WHEN (reference #>> '{0, fields, automatic_production}') = 'true' THEN TRUE
                        ELSE FALSE
                        END auto_production,
                        now() as created , now() as modified
                FROM stocks_stocktransaction
                WHERE created BETWEEN start_date AND end_date
                AND deleted is NULL
                GROUP BY 
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time,
                    transaction_type, auto_production
                ON CONFLICT (
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time,
                    transaction_type, auto_production
                ) WHERE deleted is NULL DO UPDATE
                SET modified = now(),
                    quantity = EXCLUDED.quantity;
            END;
            $BODY$;
            """,
            reverse_sql="""DROP FUNCTION public.do_hourly_stock_transaction_aggregation_for_date_range(
                timestamp with time zone, 
                timestamp with time zone
            );"""
        ),

        migrations.RunSQL(
            sql="""CREATE OR REPLACE FUNCTION public.do_hourly_stock_transaction_aggregation_for_date_range_and_stock_id(
                IN stock_pk bigint,
                IN start_date timestamp with time zone,
                IN end_date timestamp with time zone)
                RETURNS void
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE PARALLEL UNSAFE
            AS $BODY$
            BEGIN
                INSERT INTO stocks_hourlystocktransaction (
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time, quantity,
                    transaction_type, auto_production,
                    created, modified)
                SELECT 
                        account_id, outlet_id, stock_id, unit_id,
                        date_trunc('hour', created) as aggregated_time,
                        sum(quantity) as quantity,
                        transaction_type,
                        CASE
                        WHEN (reference #>> '{0, fields, automatic_production}') = 'true' THEN TRUE
                        ELSE FALSE
                        END auto_production,
                        now() as created , now() as modified
                FROM stocks_stocktransaction
                WHERE created BETWEEN start_date AND end_date
                AND stock_id = stock_pk
                AND deleted is NULL
                GROUP BY 
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time,
                    transaction_type, auto_production
                ON CONFLICT (
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time,
                    transaction_type, auto_production
                ) WHERE deleted is NULL DO UPDATE
                SET modified = now(),
                    quantity = EXCLUDED.quantity;
            END;
            $BODY$;
            """,
            reverse_sql="""DROP FUNCTION public.do_hourly_stock_transaction_aggregation_for_date_range_and_stock_id(
                bigint,
                timestamp with time zone, 
                timestamp with time zone
            );"""
        ),

        migrations.RunSQL(
            sql="""CREATE OR REPLACE FUNCTION public.do_hourly_stock_transaction_aggregation_for_date_range_and_account_id(
                IN account_pk bigint,
                IN start_date timestamp with time zone,
                IN end_date timestamp with time zone)
                RETURNS void
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE PARALLEL UNSAFE
            AS $BODY$
            BEGIN
                INSERT INTO stocks_hourlystocktransaction (
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time, quantity,
                    transaction_type, auto_production,
                    created, modified)
                SELECT 
                        account_id, outlet_id, stock_id, unit_id,
                        date_trunc('hour', created) as aggregated_time,
                        sum(quantity) as quantity,
                        transaction_type,
                        CASE
                        WHEN (reference #>> '{0, fields, automatic_production}') = 'true' THEN TRUE
                        ELSE FALSE
                        END auto_production,
                        now() as created , now() as modified
                FROM stocks_stocktransaction
                WHERE created BETWEEN start_date AND end_date
                AND account_id = account_pk
                AND deleted is NULL
                GROUP BY 
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time,
                    transaction_type, auto_production
                ON CONFLICT (
                    account_id, outlet_id, stock_id, unit_id,
                    aggregated_time,
                    transaction_type, auto_production
                ) WHERE deleted is NULL DO UPDATE
                SET modified = now(),
                    quantity = EXCLUDED.quantity;
            END;
            $BODY$;
            """,
            reverse_sql="""DROP FUNCTION public.do_hourly_stock_transaction_aggregation_for_date_range_and_account_id(
                bigint,
                timestamp with time zone, 
                timestamp with time zone
            );"""
        ),

        migrations.RunSQL(
            sql=f"""SELECT public.do_hourly_stock_transaction_aggregation_for_date_range(
                '2000-01-01 00:00:00', '{datetime.datetime.strftime(datetime.datetime.today(), "%Y-%m-%d %H:%M:%S")}'
            )""",
            reverse_sql=migrations.RunSQL.noop,
        ),
        migrations.RunSQL(
            sql="REFRESH MATERIALIZED VIEW stocks_dailystockbalancemv WITH DATA;",
            reverse_sql=migrations.RunSQL.noop,
        ),
    ]