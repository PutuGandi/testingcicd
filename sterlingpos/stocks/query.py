import copy
import difflib
import functools
import inspect
import sys
import warnings
from collections import Counter, namedtuple
from collections.abc import Iterator, Mapping
from itertools import chain, count, product
from string import ascii_uppercase

from django.core.exceptions import (
    EmptyResultSet, FieldDoesNotExist, FieldError,
)
from django.db import DEFAULT_DB_ALIAS, NotSupportedError, connections
from django.db.models.aggregates import Count
from django.db.models.constants import LOOKUP_SEP
from django.db.models.expressions import BaseExpression, Col, F, OuterRef, Ref
from django.db.models.fields import Field
from django.db.models.fields.related_lookups import MultiColSource
from django.db.models.lookups import Lookup
from django.db.models.query_utils import (
    Q, check_rel_lookup_compatibility, refs_expression,
)
from django.db.models.sql.constants import INNER, LOUTER, ORDER_DIR, SINGLE
from django.db.models.sql.datastructures import (
    BaseTable, Empty, Join, MultiJoin,
)
from django.db.models.sql.where import (
    AND, OR, ExtraWhere, NothingNode, WhereNode,
)


from django.db.models.fields.related import ForeignObject
from django.db.models.options import Options
from django.db.models.sql.where import ExtraWhere
from django.db.models.sql.datastructures import Join


__all__ = ['Query', 'RawQuery']


def get_field_names_from_opts(opts):
    return set(chain.from_iterable(
        (f.name, f.attname) if f.concrete else (f.name,)
        for f in opts.get_fields()
    ))

def names_to_path(self, names, opts, allow_many=True, fail_on_missing=False):
    """
    Walk the list of names and turns them into PathInfo tuples. A single
    name in 'names' can generate multiple PathInfos (m2m, for example).
    'names' is the path of names to travel, 'opts' is the model Options we
    start the name resolving from, 'allow_many' is as for setup_joins().
    If fail_on_missing is set to True, then a name that can't be resolved
    will generate a FieldError.
    Return a list of PathInfo tuples. In addition return the final field
    (the last used join field) and target (which is a field guaranteed to
    contain the same value as the final field). Finally, return those names
    that weren't found (which are likely transforms and the final lookup).
    """
    path, names_with_path = [], []
    for pos, name in enumerate(names):
        cur_names_with_path = (name, [])
        if name == 'pk':
            name = opts.pk.name

        field = None
        filtered_relation = None
        try:
            field = opts.get_field(name)
        except FieldDoesNotExist:
            if name in self.annotation_select:
                field = self.annotation_select[name].output_field
            elif name in self._filtered_relations and pos == 0:
                filtered_relation = self._filtered_relations[name]
                if LOOKUP_SEP in filtered_relation.relation_name:
                    parts = filtered_relation.relation_name.split(LOOKUP_SEP)
                    filtered_relation_path, field, _, _ = self.names_to_path(
                        parts, opts, allow_many, fail_on_missing,
                    )
                    path.extend(filtered_relation_path[:-1])
                else:
                    field = opts.get_field(filtered_relation.relation_name)
        if field is not None:
            # Fields that contain one-to-many relations with a generic
            # model (like a GenericForeignKey) cannot generate reverse
            # relations and therefore cannot be used for reverse querying.
            if field.is_relation and not field.related_model:
                raise FieldError(
                    "Field %r does not generate an automatic reverse "
                    "relation and therefore cannot be used for reverse "
                    "querying. If it is a GenericForeignKey, consider "
                    "adding a GenericRelation." % name
                )
            try:
                model = field.model._meta.concrete_model
            except AttributeError:
                # QuerySet.annotate() may introduce fields that aren't
                # attached to a model.
                model = None
        else:
            # We didn't find the current field, so move position back
            # one step.
            pos -= 1
            if pos == -1 or fail_on_missing:
                available = sorted([
                    *get_field_names_from_opts(opts),
                    *self.annotation_select,
                    *self._filtered_relations,
                ])
                raise FieldError("Cannot resolve keyword '%s' into field. "
                                    "Choices are: %s" % (name, ", ".join(available)))
            break
        # Check if we need any joins for concrete inheritance cases (the
        # field lives in parent, but we are currently in one of its
        # children)
        if model is not opts.model:
            path_to_parent = opts.get_path_to_parent(model)
            if path_to_parent:
                path.extend(path_to_parent)
                cur_names_with_path[1].extend(path_to_parent)
                opts = path_to_parent[-1].to_opts
        if hasattr(field, 'get_path_info'):
            pathinfos = field.get_path_info(filtered_relation)
            if not allow_many:
                for inner_pos, p in enumerate(pathinfos):
                    if p.m2m:
                        cur_names_with_path[1].extend(pathinfos[0:inner_pos + 1])
                        names_with_path.append(cur_names_with_path)
                        raise MultiJoin(pos + 1, names_with_path)
            last = pathinfos[-1]
            path.extend(pathinfos)
            final_field = last.join_field
            opts = last.to_opts
            targets = last.target_fields
            cur_names_with_path[1].extend(pathinfos)
            names_with_path.append(cur_names_with_path)
        else:
            # Local non-relational field.
            final_field = field
            targets = (field,)
            if fail_on_missing and pos + 1 != len(names):
                raise FieldError(
                    "Cannot resolve keyword %r into field. Join on '%s'"
                    " not permitted." % (names[pos + 1], name))
            break
    return path, final_field, targets, names[pos + 1:]

def add_filtered_relation(self, filtered_relation, alias):
    filtered_relation.alias = alias
    lookups = dict(get_children_from_q(filtered_relation.condition))
    relation_lookup_parts, relation_field_parts, _ = self.solve_lookup_type(filtered_relation.relation_name)
    if relation_lookup_parts:
        raise ValueError(
            "FilteredRelation's relation_name cannot contain lookups "
            "(got %r)." % filtered_relation.relation_name
        )
    for lookup in chain(lookups):
        lookup_parts, lookup_field_parts, _ = self.solve_lookup_type(lookup)
        shift = 2 if not lookup_parts else 1
        lookup_field_path = lookup_field_parts[:-shift]
        for idx, lookup_field_part in enumerate(lookup_field_path):
            if len(relation_field_parts) > idx:
                if relation_field_parts[idx] != lookup_field_part:
                    raise ValueError(
                        "FilteredRelation's condition doesn't support "
                        "relations outside the %r (got %r)."
                        % (filtered_relation.relation_name, lookup)
                    )
            else:
                raise ValueError(
                    "FilteredRelation's condition doesn't support nested "
                    "relations deeper than the relation_name (got %r for "
                    "%r)." % (lookup, filtered_relation.relation_name)
                )
    self._filtered_relations[filtered_relation.alias] = filtered_relation


class CustomJoin(Join):
    def __init__(self, subquery, subquery_params, parent_alias, table_alias, join_type, join_field, nullable):
        self.subquery_params = subquery_params
        super(CustomJoin, self).__init__(subquery, parent_alias, table_alias, join_type, join_field, nullable)

    def as_sql(self, compiler, connection):
        """
        Generates the full
        LEFT OUTER JOIN (somequery) alias ON alias.somecol = othertable.othercol, params
        clause for this join.
        """
        params = []
        sql = []
        alias_str = '' if self.table_alias == self.table_name else (' %s' % self.table_alias)
        params.extend(self.subquery_params)
        qn = compiler.quote_name_unless_alias
        qn2 = connection.ops.quote_name
        sql.append('%s (%s)%s ON (' % (self.join_type, self.table_name, alias_str))
        for index, (lhs_col, rhs_col) in enumerate(self.join_cols):
            if index != 0:
                sql.append(' AND ')
            sql.append('%s.%s = %s.%s' % (
                qn(self.parent_alias),
                qn2(lhs_col),
                qn(self.table_alias),
                qn2(rhs_col),
            ))
        extra_cond = self.join_field.get_extra_restriction(
            compiler.query.where_class, self.table_alias, self.parent_alias)
        if extra_cond:
            extra_sql, extra_params = compiler.compile(extra_cond)
            extra_sql = 'AND (%s)' % extra_sql
            params.extend(extra_params)
            sql.append('%s' % extra_sql)
        sql.append(')')
        return ' '.join(sql), params

def join_to(
        table, subquery, table_field, subquery_field, 
        queryset, alias):
    """
    Add a join on `subquery` to `queryset` (having table `table`).
    """
    # here you can set complex clause for join
    def extra_join_cond(where_class, alias, related_alias):
        if (alias, related_alias) == ('[sys].[columns]',
                                    '[sys].[database_permissions]'):
            where = '[sys].[columns].[column_id] = ' \
                    '[sys].[database_permissions].[minor_id]'
            children = [ExtraWhere([where], ())]
            return where_class(children)
        return None

    foreign_object = ForeignObject(
        to=subquery, from_fields=[None],
        to_fields=[None], rel=None,
        on_delete=None)

    foreign_object.opts = Options(table._meta)
    foreign_object.opts.model = table
    foreign_object.get_joining_columns = lambda: ((table_field, subquery_field),)
    foreign_object.get_extra_restriction = extra_join_cond
    subquery_sql, subquery_params = subquery.query.sql_with_params()
    join = CustomJoin(
        subquery_sql, subquery_params, table._meta.db_table,
        alias, "LEFT JOIN", foreign_object, True)

    queryset.query.join(join)

    # hook for set alias
    join.table_alias = alias
    queryset.query.external_aliases.add(alias)
    queryset.query.table_map[join.table_name].append(alias)

    join_obj = queryset.query.alias_map.pop(join.table_name)
    join_count = queryset.query.alias_refcount.pop(join.table_name)
    queryset.query.alias_map[alias] = join_obj
    queryset.query.alias_refcount[alias] = join_count
    return queryset
