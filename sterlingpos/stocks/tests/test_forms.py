from datetime import datetime

from test_plus.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.stocks.models import (
    Stock,
    StockTransfer,
    StockCount,
    StockAdjustment,
    PurchaseOrder,
)
from sterlingpos.stocks.forms import (
    StockBalanceForm,
    StockTransferForm,
    StockTransferDetailForm,
    StockCountForm,
    StockCountDetailForm,
    StockCountCountingForm,
    StockAdjustmentForm,
    StockAdjustmentDetailForm,
    PurchaseOrderForm,
    PurchaseOrderDetailForm,
    PurchaseOrderDetailSupplierForm,
    PurchaseReceiveOrderForm,
    PurchaseOrderReceiveDetailForm,
    ReceivedOrderForm,
    VendorBillForm,
    VendorBillDetailForm,
    StockTransferReceivedForm,
    StockCountCompleteForm,
)


class StockBalanceFormTest(TestCase):
    def setUp(self):
        self.form_class = StockBalanceForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.unit = self.account.uom_set.first()

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="outlet-1", account=self.account
        )

        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

    def test_add_product_stock(self):
        data = {
            "quantity": 50,
            "action": "add_balance",
            "unit": self.unit.pk,
        }
        form = self.form_class(data=data, instance=self.stock, user=self.user)
        self.assertTrue(form.is_valid(), form.errors)
        form.save()
        self.assertEqual(self.stock.balance, 50)

    def test_subtract_product_stock(self):
        data = {
            "quantity": 50,
            "action": "subtract_balance",
            "unit": self.unit.pk,
        }
        form = self.form_class(data=data, instance=self.stock, user=self.user)
        self.assertTrue(form.is_valid(), form.errors)
        form.save()
        self.assertEqual(self.stock.balance, -50)


class StockTransferFormTest(TestCase):
    def setUp(self):
        self.form_class = StockTransferForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )
        self.outlet_2 = mommy.make(
            "outlet.Outlet", name="outlet 2", branch_id="outlet-2", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_2, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_transfer(self):
        data = {
            "code": "TS001",
            "source": self.outlet_1.pk,
            "destination": self.outlet_2.pk,
            "date_of_transfer": datetime.now(),
            "account": self.account.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid())
        stock_transfer = form.save()
        self.assertEqual(stock_transfer.source, self.outlet_1)
        self.assertEqual(stock_transfer.destination, self.outlet_2)
        self.assertEqual(stock_transfer.date_of_transfer, datetime.now().date())


class StockTransferDetailFormTest(TestCase):
    def setUp(self):
        self.form_class = StockTransferDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )
        self.outlet_2 = mommy.make(
            "outlet.Outlet", name="outlet 2", branch_id="outlet-2", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_2, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)

        self.stock_transfer = StockTransfer.objects.create(
            code="TS001",
            source=self.outlet_1,
            destination=self.outlet_2,
            date_of_transfer=datetime.today(),
            account=self.account,
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_transfer_detail(self):
        data = {
            "product": self.product.pk,
            "unit": self.unit.pk,
            "quantity": 1,
            "source_stock": "{} {}".format(self.stock_1.balance, self.unit.unit),
            "stock_transfer": self.stock_transfer.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        stock_transfer_detail = form.save(commit=True)

        self.assertEqual(stock_transfer_detail.stock_transfer, self.stock_transfer)
        self.assertEqual(self.stock_transfer.transfered_stock.count(), 1)
        self.assertEqual(stock_transfer_detail.quantity, data["quantity"])
        self.assertEqual(stock_transfer_detail.stock_source, self.stock_1)
        self.assertEqual(stock_transfer_detail.stock_destination, self.stock_2)

    def test_create_out_of_stock_transfer_detail(self):
        self.stock_1.set_balance(0, unit=self.unit)

        data = {
            "product": self.product.pk,
            "unit": self.unit.pk,
            "quantity": 1,
            "source_stock": "{} {}".format(self.stock_1.balance, self.unit.unit),
            "stock_transfer": self.stock_transfer.pk,
        }
        form = self.form_class(data=data)
        self.assertFalse(form.is_valid())


class StockTransferReceivedFormTest(TestCase):
    def setUp(self):
        self.form_class = StockTransferReceivedForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )
        self.outlet_2 = mommy.make(
            "outlet.Outlet", name="outlet 2", branch_id="outlet-2", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_2, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)

        self.stock_transfer = StockTransfer.objects.create(
            code="TS001",
            source=self.outlet_1,
            destination=self.outlet_2,
            date_of_transfer=datetime.today().date(),
            account=self.account,
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_transfer_received(self):
        data = {
            "date_of_arrival": datetime.today().strftime("%d/%m/%Y"),
            "account": self.account.pk,
        }
        form = self.form_class(data=data, instance=self.stock_transfer)
        self.assertTrue(form.is_valid())
        stock_transfer = form.save()
        self.assertEqual(stock_transfer.date_of_arrival, datetime.today().date())


class StockCountFormTest(TestCase):
    def setUp(self):
        self.form_class = StockCountForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_count(self):
        data = {
            "code": "SC001",
            "outlet": self.outlet_1.pk,
            "account": self.account.pk,
            "date_of_stock_count": datetime.today().strftime("%d/%m/%Y"),
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid())
        stock_count = form.save()
        self.assertEqual(stock_count.outlet, self.outlet_1)
        self.assertEqual(stock_count.status, "pending")


class StockCountDetailFormTest(TestCase):
    def setUp(self):
        self.form_class = StockCountDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)

        self.stock_count = StockCount.objects.create(
            code="SC001", outlet=self.outlet_1, note="Notes", account=self.account,
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_count_detail(self):
        data = {
            "product": self.product.pk,
            "stock_count": self.stock_count.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        stock_count_detail = form.save()

        self.assertEqual(stock_count_detail.stock_count, self.stock_count)
        self.assertEqual(self.stock_count.counted_stock.count(), 1)
        self.assertEqual(stock_count_detail.quantity, 100)
        self.assertEqual(stock_count_detail.stock, self.stock_1)
        self.assertEqual(stock_count_detail.counted, 0)

    def test_update_product_of_stock_count_detail(self):
        product_2 = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        product_2_stock, _ = Stock.objects.get_or_create(
            product=product_2, outlet=self.outlet_1, account=self.account
        )
        product_2_stock.set_balance(50, unit=self.unit)

        stock_count_detail = self.stock_count.count_product_stock(self.product,)

        data = {
            "product": product_2.pk,
            "stock_count": self.stock_count.pk,
        }
        form = self.form_class(data=data, instance=stock_count_detail,)
        self.assertTrue(form.is_valid(), form.errors)
        stock_count_detail = form.save()

        self.assertEqual(stock_count_detail.stock_count, self.stock_count)
        self.assertEqual(self.stock_count.counted_stock.count(), 1)
        self.assertEqual(stock_count_detail.quantity, 50)
        self.assertEqual(stock_count_detail.stock, product_2_stock)
        self.assertEqual(stock_count_detail.counted, 0)


class StockCountCountingFormTest(TestCase):
    def setUp(self):
        self.form_class = StockCountCountingForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)

        self.stock_count = StockCount.objects.create(
            code="SC001", outlet=self.outlet_1, note="Notes", account=self.account,
        )

        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_counting_stock_count_detail(self):
        stock_count_detail = self.stock_count.count_product_stock(product=self.product,)
        data = {
            "counted": 50,
            "stock_count": self.stock_count.pk,
            "unit": self.unit.pk,
        }
        form = self.form_class(data=data, instance=stock_count_detail)
        self.assertTrue(form.is_valid(), form.errors)
        stock_count_detail = form.save()

        self.assertEqual(stock_count_detail.stock_count, self.stock_count)
        self.assertEqual(self.stock_count.counted_stock.count(), 1)
        self.assertEqual(stock_count_detail.quantity, 100)
        self.assertEqual(stock_count_detail.stock, self.stock_1)
        self.assertEqual(stock_count_detail.counted, 50)


class StockCountCompleteFormTest(TestCase):
    def setUp(self):
        self.form_class = StockCountCompleteForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)

        self.stock_count = StockCount.objects.create(
            code="SC001",
            outlet=self.outlet_1,
            note="Notes",
            account=self.account,
            date_of_stock_count=datetime.today().date(),
        )

        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_count_complete(self):
        data = {
            "date_of_completion": datetime.today().strftime("%d/%m/%Y"),
            "account": self.account.pk,
        }
        form = self.form_class(data=data, instance=self.stock_count)
        self.assertTrue(form.is_valid())
        stock_count = form.save()
        self.assertEqual(stock_count.date_of_completion, datetime.today().date())


class StockAdjustmentFormTest(TestCase):
    def setUp(self):
        self.form_class = StockAdjustmentForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_adjustment(self):
        data = {
            "code": "SA001",
            "outlet": self.outlet_1.pk,
            "status": StockAdjustment.STATUS.item_receive,
            "date_of_adjustment": datetime.today().strftime("%d/%m/%Y"),
            "account": self.account.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid())
        stock_adjustment = form.save()
        self.assertEqual(stock_adjustment.outlet, self.outlet_1)
        self.assertEqual(stock_adjustment.status, "item_receive")
        self.assertFalse(stock_adjustment.is_processed)


class StockAdjustmentDetailFormTest(TestCase):
    def setUp(self):
        self.form_class = StockAdjustmentDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)

        self.stock_adjustment = StockAdjustment.objects.create(
            code="SA001",
            outlet=self.outlet_1,
            note="Item Received",
            status=StockAdjustment.STATUS.item_receive,
            account=self.account,
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_stock_adjustment_detail(self):
        data = {
            "product": self.product.pk,
            "unit": self.unit.pk,
            "quantity": 10,
            "cost": 10000,
            "stock_adjustment": self.stock_adjustment.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        stock_adjustment_detail = form.save()

        self.assertEqual(
            stock_adjustment_detail.stock_adjustment, self.stock_adjustment
        )
        self.assertEqual(self.stock_adjustment.adjusted_stock.count(), 1)
        self.assertEqual(stock_adjustment_detail.quantity, 10)
        self.assertEqual(stock_adjustment_detail.stock, self.stock_1)

    def test_update_product_of_stock_adjustment_detail(self):
        product_2 = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            account=self.account,
            uom=self.unit,
        )
        product_2_stock, _ = Stock.objects.get_or_create(
            product=product_2, outlet=self.outlet_1, account=self.account
        )
        product_2_stock.set_balance(50, unit=self.unit)

        stock_adjustment_detail = self.stock_adjustment.adjust_product_stock(
            self.product, self.unit, 10, 10000
        )

        data = {
            "product": product_2.pk,
            "unit": self.unit.pk,
            "cost": 10000,
            "quantity": 20,
            "stock_adjustment": self.stock_adjustment.pk,
        }
        form = self.form_class(data=data, instance=stock_adjustment_detail,)
        self.assertTrue(form.is_valid(), form.errors)
        stock_adjustment_detail = form.save()

        self.assertEqual(
            stock_adjustment_detail.stock_adjustment, self.stock_adjustment
        )
        self.assertEqual(self.stock_adjustment.adjusted_stock.count(), 1)
        self.assertEqual(stock_adjustment_detail.quantity, 20)
        self.assertEqual(stock_adjustment_detail.stock, product_2_stock)


class PurchaseOrderFormTest(TestCase):
    def setUp(self):
        self.form_class = PurchaseOrderForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.stock_1.set_balance(100, unit=self.unit)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_purchase_order(self):
        data = {
            "code": "PO001",
            "date_of_purchase": datetime.now(),
            "supplier": self.supplier.pk,
            "outlet": self.outlet_1.pk,
            "discount_type": "fixed",
            "discount": 0,
            "tax": 0,
            "account": self.account.pk,
        }
        form = self.form_class(data=data)

        self.assertTrue(form.is_valid())
        purchase_order = form.save()
        self.assertEqual(purchase_order.outlet, self.outlet_1)
        self.assertEqual(purchase_order.status, "active")


class PurchaseOrderDetailFormTest(TestCase):
    def setUp(self):
        self.form_class = PurchaseOrderDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)
        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.purchase_order = PurchaseOrder.objects.create(
            code="PO001",
            outlet=self.outlet_1,
            supplier=self.supplier,
            date_of_purchase=datetime.today(),
            account=self.account,
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_purchase_order_detail(self):
        data = {
            "product": self.product.pk,
            "unit": self.unit.pk,
            "quantity": 1,
            "cost": 10000,
            "discount_type": "fixed",
            "discount": 0,
            "purchase_order": self.purchase_order.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        purchase_order_detail = form.save(commit=True)

        self.assertEqual(purchase_order_detail.purchase_order, self.purchase_order)
        self.assertEqual(self.purchase_order.purchased_stock.count(), 1)
        self.assertEqual(purchase_order_detail.quantity, data["quantity"])


class PurchaseOrderDetailSupplierFormTest(TestCase):
    def setUp(self):
        self.form_class = PurchaseOrderDetailSupplierForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.stock_1.set_balance(100, unit=self.unit)
        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.purchase_order = PurchaseOrder.objects.create(
            code="PO001",
            outlet=self.outlet_1,
            supplier=self.supplier,
            date_of_purchase=datetime.today(),
            account=self.account,
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_purchase_order_detail_supplier(self):
        data = {
            "product": self.product.pk,
            "unit_id": self.unit.pk,
            "quantity": 1,
            "cost": 10000,
            "discount_type": "fixed",
            "discount": 0,
            "purchase_order": self.purchase_order.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        purchase_order_detail = form.save(commit=True)

        self.assertEqual(purchase_order_detail.purchase_order, self.purchase_order)
        self.assertEqual(self.purchase_order.purchased_stock.count(), 1)
        self.assertEqual(purchase_order_detail.quantity, data["quantity"])


class PurchaseReceiveOrderFormTest(TestCase):
    def setUp(self):
        self.form_class = PurchaseReceiveOrderForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.stock_1.set_balance(100, unit=self.unit)
        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet_1,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_puchase_order_receive(self):
        data = {
            "receipt_number": "12345",
            "received_date": datetime.now(),
            "account": self.account.pk,
        }
        form = self.form_class(data=data, purchase_order=self.purchase_order.pk)

        self.assertTrue(form.is_valid())
        received = form.save()
        self.assertEqual(received.purchase_order, self.purchase_order)


class PurchaseOrderReceiveDetailFormTest(TestCase):
    def setUp(self):
        self.form_class = PurchaseOrderReceiveDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.stock_1.set_balance(100, unit=self.unit)
        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet_1,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_create_purchase_order_receive_detail(self):
        data = {
            "quantity": 10,
            "unit": self.unit.pk,
            "stock": self.stock_1.pk,
            "received": self.received.pk,
        }
        form = self.form_class(data=data, purchase_order=self.purchase_order.pk)
        self.assertTrue(form.is_valid(), form.errors)
        receive_detail = form.save(commit=True)

        self.assertEqual(receive_detail.received.purchase_order, self.purchase_order)
        self.assertEqual(self.received.received_detail.count(), 1)
        self.assertEqual(receive_detail.quantity, data["quantity"])


class ReceivedOrderFormTest(TestCase):
    def setUp(self):
        self.form_class = ReceivedOrderForm
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.stock_1.set_balance(100, unit=self.unit)
        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet_1,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

        self.purchase_order_2 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet_1,
            supplier=self.supplier,
            code="PO-001",
            status=PurchaseOrder.STATUS.closed,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_received_order_form(self):
        data = {
            "purchase_order": self.purchase_order.pk,
            "receipt_number": "12345",
            "received_date": datetime.now(),
            "account": self.account.pk,
        }
        form = self.form_class(data=data)

        self.assertTrue(form.is_valid())
        received = form.save()
        self.assertEqual(received.purchase_order, self.purchase_order)

    def test_received_order_form_failed(self):
        data = {
            "purchase_order": "",
            "receipt_number": "12345",
            "received_date": datetime.now(),
            "account": self.account.pk,
        }
        form = self.form_class(data=data)

        self.assertTrue(not form.is_valid())

    def test_received_order_form_failed_purchase_order_closed(self):
        data = {
            "purchase_order": self.purchase_order_2.pk,
            "receipt_number": "12345",
            "received_date": datetime.now(),
            "account": self.account.pk,
        }
        form = self.form_class(data=data)

        self.assertTrue(not form.is_valid())


class TestVendorBillForm(TestCase):
    def setUp(self):
        self.form_class = VendorBillForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.stock_1.set_balance(100, unit=self.unit)
        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet_1,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
        )
        self.received.receive_purchase(self.product, 10, self.unit)

    def tearDown(self):
        set_current_tenant(None)

    def test_vendor_bill_form(self):
        data = {
            "received_order": self.received.pk,
            "bill_date": datetime.now(),
            "due_date": datetime.now(),
            "account": self.account.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid())
        vendor_bill = form.save()
        self.assertEqual(vendor_bill.received_order, self.received)
        self.assertEqual(vendor_bill.received_order.purchase_order, self.purchase_order)

    def test_vendor_bill_form_failed(self):
        data = {
            "received_order": "",
            "bill_date": datetime.now(),
            "due_date": datetime.now(),
            "account": self.account.pk,
        }
        form = self.form_class(data=data)

        self.assertTrue(not form.is_valid())


class TestVendorBillDetailForm(TestCase):
    def setUp(self):
        self.form_class = VendorBillDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

        self.supplier = mommy.make("stocks.supplier", account=self.account)

        self.stock_1.set_balance(100, unit=self.unit)
        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet_1,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
        )
        self.received.receive_purchase(self.stock_1, 10, self.unit)
        self.vendor_bill = mommy.make(
            "stocks.VendorBill", account=self.account, received_order=self.received
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_vendor_bill_detail_form(self):
        data = {
            "vendor_bill": self.vendor_bill.pk,
            "purchase_cost": 1500,
            "product": self.product.pk,
            "account": self.account.pk,
        }
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid())
        vendor_bill_detail = form.save()
        self.assertEqual(vendor_bill_detail.vendor_bill, self.vendor_bill)
