import json
from datetime import datetime

from django.test import RequestFactory
from django.urls import reverse
from django.contrib.messages.storage.fallback import FallbackStorage
from django.utils.translation import ugettext as _

from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.catalogue.models import Catalogue
from sterlingpos.stocks.models import (
    Stock,
    Supplier,
    StockTransfer,
    StockAdjustment,
    StockCount,
    PurchaseOrder,
    PurchaseOrderReceive,
    VendorBill,
    Batch
)
from sterlingpos.stocks.views import (
    ProductStockListJson,
    ProductStockListView,
    ProductStockDetailJson,
    ProductStockDetailView,
    ProductStockUpdateView,
    StockTransactionListJson,
    SupplierListJson,
    SupplierCreateView,
    SupplierUpdateView,
    SupplierDeleteView,
    StockTransferListJson,
    StockTransferCreateView,
    StockTransferUpdateView,
    StockTransferReceiveView,
    StockCardListJson,
    ProductInventoryAutocompleteView,
    PurchaseOrderInventoryAutocomplete,
    OutletInventoryAutocompleteView,
    OutletDestinationInventoryAutocompleteView,
    StockTransferBatchTrackedView,
    StockTransferDeleteView,
    StockCountListJson,
    StockCountCreateView,
    StockCountUpdateView,
    StockCountCountingView,
    StockCountDeleteView,
    StockCountCompleteView,
    StockQuantityView,
    StockQuantityConversion,
    StockAdjustmentListJson,
    SupplierAutocompleteView,
    BatchAutocompleteView,
    BatchCreateGetView,
    BatchInformationView,
    StockAdjustmentWizardView,
    PurchaseOrderListJson,
    PurchaseOrderCreateView,
    PurchaseOrderDetailView,
    PurchaseOrderUpdateView,
    PurchaseOrderCancelRemainingItemsView,
    PurchaseOrderDetailCost,
    BatchTrackedListJson,
    BatchesQuantityConversionJsonView,
    QuantityBatchConversionJsonView,
    SupplierItemListJson,
    SupplierItemCreateView,
    SupplierItemUpdateView,
    SupplierItemDeleteView,
    PurchaseOrderReceivedListJson,
    PurchaseOrderReceiveCreateView,
    PurchaseOrderProductDetailView,
    PurchaseOrderReceiveUpdateView,
    ReceivedOrderListJson,
    PurchaseOrderAutocompleteView,
    ReceivedOrderCreateView,
    ReceivedOrderUpdateView,
    ReceivedOrderCreateViewWizard,
    VendorBillListJson,
    VendorBillCreateView,
    VendorBillDetailViewJson,
    ReceivedOrderAutocompleteView,
    VendorBillDetailView,
    VendorBillUpdateView,
    PurchaseOrderProductDetailJsonView,
    StockTransferReceivedView,
    StockCountCompletedView,
)

MAX_NUM_FORMS = 50


class TestSalesOrderListJson(TestCase):
    url_string = "stocks:stocks_update"

    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        outlet = mommy.make("outlet.Outlet", account=account, name="A", branch_id="A")
        self.unit = account.uom_set.first()

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.product = mommy.make(
            "catalogue.Product",
            price=10000,
            is_manage_stock=True,
            uom=self.unit,
            account=account,
        )

        self.stock = self.product.stocks.get(outlet=outlet)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def test_action_add_balance(self):
        data = {
            "action": "add_balance",
            "unit": self.unit.pk,
            "quantity": 10,
        }
        url = self.reverse(self.url_string, stock_pk=self.stock.pk)
        self.login(email=self.user.email, password="test")
        response = self.post(url, data=data, **self.request_header)

        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(self.stock.balance, 10)

    def test_action_subtract_balance(self):
        data = {
            "action": "subtract_balance",
            "unit": self.unit.pk,
            "quantity": 10,
        }
        url = self.reverse(self.url_string, stock_pk=self.stock.pk)
        self.login(email=self.user.email, password="test")
        response = self.post(url, data=data, **self.request_header)

        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(self.stock.balance, -10)

    def test_add_balance_with_bad_stock(self):
        data = {
            "action": "add_balance",
            "unit": self.unit.pk,
            "quantity": 10,
        }
        url = self.reverse(self.url_string, stock_pk=99)
        self.login(email=self.user.email, password="test")
        response = self.post(url, data=data, **self.request_header)

        self.assertEqual(response.status_code, 404, response.content)

    def test_get_object(self):
        request = self.factory.get("/")
        request.user = self.user

        view = ProductStockUpdateView()
        view.request = request
        view.kwargs = {"stock_pk": self.stock.pk}

        obj = view.get_object()
        self.assertEqual(obj, self.stock)


class TestProductStockListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        outlet = mommy.make("outlet.Outlet", account=account, name="A", branch_id="A")

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.product = mommy.make(
            "catalogue.Product",
            name="kwetiaw",
            price=10000,
            is_manage_stock=True,
            account=account,
        )

        self.product_archived = mommy.make(
            "catalogue.Product",
            name="lemongrass",
            price=5000,
            is_manage_stock=True,
            account=account,
            archived=True,
        )

        self.stock = self.product.stocks.get(outlet=outlet)
        set_current_tenant(account)
        self.url = reverse("stocks:product_stocks_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_product_stock_list_json(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content["data"][0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 5,
        }
        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_product_stock_list_json_filter(self):
        self.login(email=self.user.email, password="test")
        total_column = 5
        data = {"draw": 5}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[1][data]": 1,
                "columns[1][name]": "",
                "columns[1][searchable]": True,
                "columns[1][orderable]": False,
                "columns[1][search][value]": self.product.sku,
                "columns[1][search][regex]": False,
            }
        )

        response = self.get(self.url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
            "data_columns_name": {
                "name": self.product.name,
                "url": reverse("stocks:stocks", kwargs={"product_pk": self.product.pk}),
                "image": self.product.image_location,
            },
        }

        data = content["data"][0]
        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[0], expected_result_valid["data_columns_name"])


class TestProductStockListView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        outlet = mommy.make("outlet.Outlet", account=account, name="A", branch_id="A")

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.product = mommy.make(
            "catalogue.Product", price=10000, is_manage_stock=True, account=account
        )

        self.stock = self.product.stocks.get(outlet=outlet)
        set_current_tenant(account)

        self.url = self.reverse("stocks:product_stocks")

    def tearDown(self):
        set_current_tenant(None)

    def test_view_is_not_accessible_by_anon(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.reverse("account_login"), response.url)

    def test_good_view(self):
        self.login(email=self.user.email, password="test")
        response = self.assertGoodView(self.url)
        self.assertEqual(response.status_code, 200)

    def test_get_object(self):
        request = self.factory.get("/")
        request.user = self.user

        view = ProductStockUpdateView()
        view.request = request
        view.kwargs = {"stock_pk": self.stock.pk}

        obj = view.get_object()
        self.assertEqual(obj, self.stock)


class TestProductStockDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        outlet = mommy.make("outlet.Outlet", account=account, name="A", branch_id="A")

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.product = mommy.make(
            "catalogue.Product", price=10000, is_manage_stock=True, account=account
        )

        self.stock = self.product.stocks.get(outlet=outlet)
        self.url = reverse("stocks:stocks", kwargs={"product_pk": self.product.pk})
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test_stock_detail_view_success(self):
        request = self.factory.get(self.url)
        request.user = self.user

        response = ProductStockDetailView.as_view()(request, product_pk=self.product.pk)
        self.assertEqual(response.status_code, 200)


class TestProductStockDetailJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        self.account = mommy.make("accounts.Account")
        self.outlet = mommy.make("outlet.Outlet", account=self.account)

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )

        self.stock = self.product.stocks.get(outlet=self.outlet)
        self.url = reverse("stocks:stocks_data", kwargs={"product_pk": self.product.pk})
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_product_stock_detail_json(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content["data"][0]
        expected_result_valid = {
            "recordsTotal": 1,
            "recordsFiltered": 1,
            "data_columns_name": {
                "name": self.outlet.name,
                "url": reverse(
                    "stocks:stocks_transaction",
                    kwargs={"product_pk": self.product.pk, "outlet_pk": self.outlet.pk},
                ),
            },
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[0], expected_result_valid["data_columns_name"])


class TestProductStockUpdateView(TestCase):
    url_string = "stocks:stocks_update"

    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        outlet = mommy.make("outlet.Outlet", account=account, name="A", branch_id="A")
        self.unit = account.uom_set.first()

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.product = mommy.make(
            "catalogue.Product",
            price=10000,
            is_manage_stock=True,
            uom=self.unit,
            account=account,
        )

        self.stock = self.product.stocks.get(outlet=outlet)
        set_current_tenant(account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_add_update_stock_view(self):
        data = {
            "action": "add_balance",
            "unit": self.unit.pk,
            "quantity": 10,
        }
        url = self.reverse(self.url_string, stock_pk=self.stock.pk)
        self.login(email=self.user.email, password="test")
        response = self.post(url, data=data, **self.request_header)

        self.assertEqual(response.status_code, 200, response.content)
        response_data = json.loads(response.content)
        self.assertEqual(self.stock.balance, 10)
        self.assertEqual(response_data["status"], "success")

    def test_set_update_stock_view(self):
        data = {
            "action": "add_balance",
            "unit": self.unit.pk,
            "quantity": 100,
        }
        data2 = {
            "action": "set_balance",
            "unit": self.unit.pk,
            "quantity": 10,
        }

        url = self.reverse(self.url_string, stock_pk=self.stock.pk)
        self.login(email=self.user.email, password="test")
        response_before_set = self.post(url, data=data, **self.request_header)
        response_after_set = self.post(url, data=data2, **self.request_header)
        self.assertEqual(
            response_after_set.status_code, 200, response_after_set.content
        )
        response_data = json.loads(response_after_set.content)

        self.assertEqual(self.stock.balance, 10)
        self.assertEqual(response_data["status"], "success")


class TestProductStockTransactionListView(TestCase):
    url_update = "stocks:stocks_update"

    def setUp(self):
        self.factory = RequestFactory()

        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)

        self.product = mommy.make(
            "catalogue.Product", price=10000, is_manage_stock=True, account=self.account
        )
        self.outlet = mommy.make(
            "outlet.Outlet", account=self.account, name="A", branch_id="A"
        )

        self.stock = self.product.stocks.get(outlet=self.outlet)
        set_current_tenant(self.account)

        self.url_trans_list = reverse(
            "stocks:stocks_transaction",
            kwargs={"product_pk": self.product.pk, "outlet_pk": self.outlet.pk},
        )
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url_trans_list)
        self.assertEqual(response.status_code, 200)

    def test_stock_transaction_list_view_success(self):
        self.login(email=self.user.email, password="test")
        data = {
            "action": "add_balance",
            "quantity": 10,
        }
        url = self.reverse(self.url_update, stock_pk=self.stock.pk)
        self.login(email=self.user.email, password="test")
        self.post(url, data=data, **self.request_header)

        response = self.get(self.url_trans_list)
        self.assertEqual(response.status_code, 200)


class TestProductStockTransactionListJSON(TestCase):
    url_update = "stocks:stocks_update"

    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )
        self.unit = account.uom_set.first()

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.product = mommy.make(
            "catalogue.Product",
            price=10000,
            is_manage_stock=True,
            uom=self.unit,
            account=account,
        )

        self.stock = self.product.stocks.get(outlet=self.outlet)
        set_current_tenant(account)

        self.url = reverse(
            "stocks:stocks_transaction_data", kwargs={"stock_pk": self.stock.pk}
        )
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_product_stock_transaction_list_json(self):
        self.login(email=self.user.email, password="test")
        data = {
            "action": "add_balance",
            "unit": self.unit.pk,
            "quantity": 10,
        }
        url = self.reverse(self.url_update, stock_pk=self.stock.pk)
        update_response = self.post(url, data=data, **self.request_header)
        self.assertEqual(update_response.status_code, 200, update_response)

        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 1,
            "recordsFiltered": 1,
        }
        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_product_transaction_list_json_filter(self):
        self.login(email=self.user.email, password="test")
        data = {
            "action": "set_balance",
            "unit": self.unit.pk,
            "quantity": 10,
        }
        url = self.reverse(self.url_update, stock_pk=self.stock.pk)
        update_response = self.post(url, data=data, **self.request_header)
        self.assertEqual(update_response.status_code, 200, update_response.content)

        total_column = 4
        data = {"draw": 1}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[1][data]": 1,
                "columns[1][name]": "",
                "columns[1][searchable]": True,
                "columns[1][orderable]": False,
                "columns[1][search][value]": "manual_set",
                "columns[1][search][regex]": False,
            }
        )

        response = self.get(self.url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        data = content["data"][0]
        expected_result_valid = {
            "recordsTotal": 1,
            "recordsFiltered": 1,
            "data_quantity": "{}".format(self.stock.transactions.last().quantity),
            "data_user_email": self.stock.transactions.last().user.email,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[2], expected_result_valid["data_quantity"])
        self.assertEqual(data[4], expected_result_valid["data_user_email"])


class TestStockTransactionListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_transaction_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer.transfer_product_stock(self.product, self.unit, 10)
        self.stock_transfer.received(self.user)

        self.stock_adjustment = mommy.make(
            "stocks.StockAdjustment",
            account=self.account,
            outlet=self.source,
            status=StockAdjustment.STATUS.item_receive,
        )
        self.stock_adjustment.adjust_product_stock(self.product, self.unit, 10, 0)
        self.stock_adjustment.process_adjustment(self.user)

        self.stock_count = mommy.make(
            "stocks.StockCount", account=self.account, outlet=self.source
        )
        self.stock_count.count_product_stock(self.product)
        self.stock_count.process_adjustment(self.user)

        self.supplier = mommy.make("stocks.Supplier", account=self.account)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            supplier=self.supplier,
            outlet=self.source,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 100, self.unit, 0, "percentage", 5
        )
        for purchased in self.purchase_order.purchased_stock.all():
            purchased.process_purchase(100, self.unit, "percentage", 5, self.user)
        self.purchase_order.received()

        self.sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.source,
            account=self.account,
        )
        self.product_order = mommy.make(
            "sales.ProductOrder",
            sales_order=self.sales_order,
            product=self.product,
            quantity=2,
            account=self.account,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_transaction"))
        self.assertEqual(response.status_code, 200)

    def test_list_product_outlet(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse(
                "stocks:stocks_transaction",
                kwargs={"product_pk": self.product.pk, "outlet_pk": self.source.pk},
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_stock_transfer_list(self):
        expected_result_valid = {
            "recordsTotal": 8,
            "recordsFiltered": 8,
            "columnsTotal": 8,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockTransactionListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestProductStockTransactionListJson(TestStockTransactionListJson):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "stocks:stocks_transaction_data", kwargs={"stock_pk": self.source_stock.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestStockCardListJson(TestStockTransactionListJson):
    def setUp(self):
        super().setUp()
        self.url = reverse("stocks:stocks_card_data")

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stock_card"))
        self.assertEqual(response.status_code, 200)

    def test_stock_card_list(self):
        expected_result_valid = {
            "recordsTotal": 1,
            "recordsFiltered": 1,
            "columnsTotal": 8,
        }
        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockCardListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestStockInTransactionListJson(TestStockTransactionListJson):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "stocks:stock_in_data", kwargs={"product_pk": self.product.pk}
        )
        self.url_outlet = reverse(
            "stocks:stock_in_outlet_data",
            kwargs={"product_pk": self.product.pk, "outlet_pk": self.source.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_url_view_success_outlet(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url_outlet)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse("stocks:stock_in", kwargs={"product_pk": self.product.pk})
        )
        self.assertEqual(response.status_code, 200)

    def test_list_view_outlet(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse(
                "stocks:stock_in_outlet",
                kwargs={"product_pk": self.product.pk, "outlet_pk": self.source.pk},
            )
        )
        self.assertEqual(response.status_code, 200)


class TestStockOutTransactionListJson(TestStockTransactionListJson):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "stocks:stock_out_data", kwargs={"product_pk": self.product.pk}
        )
        self.url_outlet = reverse(
            "stocks:stock_out_outlet_data",
            kwargs={"product_pk": self.product.pk, "outlet_pk": self.source.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_url_view_success_outlet(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url_outlet)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse("stocks:stock_out", kwargs={"product_pk": self.product.pk})
        )
        self.assertEqual(response.status_code, 200)

    def test_list_view_outlet(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse(
                "stocks:stock_out_outlet",
                kwargs={"product_pk": self.product.pk, "outlet_pk": self.source.pk},
            )
        )
        self.assertEqual(response.status_code, 200)


class TestStockSalesTransactionJsonList(TestStockTransactionListJson):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "stocks:stock_sales_data", kwargs={"product_pk": self.product.pk}
        )
        self.url_outlet = reverse(
            "stocks:stock_sales_outlet_data",
            kwargs={"product_pk": self.product.pk, "outlet_pk": self.source.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_url_view_success_outlet(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url_outlet)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse("stocks:stock_sales", kwargs={"product_pk": self.product.pk})
        )
        self.assertEqual(response.status_code, 200)

    def test_list_view_outlet(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse(
                "stocks:stock_sales_outlet",
                kwargs={"product_pk": self.product.pk, "outlet_pk": self.source.pk},
            )
        )
        self.assertEqual(response.status_code, 200)


class TestSupplierJSONList(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)
        set_current_tenant(account)

        self.supplier_1 = mommy.make(Supplier, name="test-supplier-1", account=account)
        self.supplier_2 = mommy.make(Supplier, name="test-supplier-2", account=account)

        self.url = reverse("stocks:stocks_supplier_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_supplier_list_json(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 5,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = SupplierListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestSupplierCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)
        self.unit = account.uom_set.first()

        set_current_tenant(account)
        self.product = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )

        self.url = reverse("stocks:stocks_supplier_create")

    def tearDown(self):
        set_current_tenant(None)

    def test_get(self):
        request = self.factory.get(self.url)
        request.user = self.user
        response = SupplierCreateView.as_view()(request)

        assert response.status_code == 200

    def test_post(self):
        params = {
            "name": "Test Supplier",
            "code": "",
            "email": "",
            "contact": "",
            "phone": "",
            "address": "",
            "supplier_detail-TOTAL_FORMS": "1",
            "supplier_detail-INITIAL_FORMS": "0",
            "supplier_detail-MIN_NUM_FORMS": "1",
            "supplier_detail-MAX_NUM_FORMS": "100",
            "supplier_detail-0-product": "",
            "supplier_detail-0-quantity": "",
            "supplier_detail-0-unit": "",
            "supplier_detail-0-cost": "",
            "supplier_detail-0-supplier": "",
            "supplier_detail-0-id": "",
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = SupplierCreateView.as_view()(request)

        assert response.status_code == 302
        assert Supplier.objects.last().name == params["name"]
        assert Supplier.objects.last().code == "SUPPLIER-{}".format(
            Supplier.all_objects.count()
        )

    def test_post_with_master_product_data(self):
        params = {
            "name": "Test Supplier",
            "code": "",
            "email": "",
            "contact": "",
            "phone": "",
            "address": "",
            "master_product_data": "on",
            "supplier_detail-TOTAL_FORMS": "1",
            "supplier_detail-INITIAL_FORMS": "0",
            "supplier_detail-MIN_NUM_FORMS": "1",
            "supplier_detail-MAX_NUM_FORMS": "100",
            "supplier_detail-0-product": self.product.pk,
            "supplier_detail-0-quantity": 100,
            "supplier_detail-0-unit": self.unit.pk,
            "supplier_detail-0-cost": 10000,
            "supplier_detail-0-supplier": "",
            "supplier_detail-0-id": "",
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = SupplierCreateView.as_view()(request)

        assert response.status_code == 302
        assert Supplier.objects.last().name == params["name"]
        assert Supplier.objects.last().name == params["name"]
        assert Supplier.objects.last().code == "SUPPLIER-{}".format(
            Supplier.all_objects.count()
        )
        assert Supplier.objects.last().supplier_detail.count() == 1


class TestSupplierUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.supplier = mommy.make(Supplier, name="test", account=account)

        self.unit = account.uom_set.first()

        set_current_tenant(account)
        self.product = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )
        self.supplier_2 = mommy.make(
            Supplier, name="test", account=account, master_product_data=True
        )
        self.supplier_2.create_supplier_detail(self.product, 10, self.unit, 10000)
        self.product_2 = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )

        self.url = reverse(
            "stocks:stocks_supplier_update", kwargs={"pk": self.supplier.pk}
        )
        self.url_2 = reverse(
            "stocks:stocks_supplier_update", kwargs={"pk": self.supplier_2.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_get(self):
        request = self.factory.get(self.url)
        request.user = self.user
        response = SupplierUpdateView.as_view()(request, pk=self.supplier.pk)

        assert response.status_code == 200

    def test_post(self):
        params = {
            "name": "Test Supplier",
            "code": "Sup-123",
            "email": "",
            "contact": "",
            "phone": "",
            "address": "",
            "supplier_detail-TOTAL_FORMS": "1",
            "supplier_detail-INITIAL_FORMS": "0",
            "supplier_detail-MIN_NUM_FORMS": "1",
            "supplier_detail-MAX_NUM_FORMS": "100",
            "supplier_detail-0-product": "",
            "supplier_detail-0-quantity": "",
            "supplier_detail-0-unit": "",
            "supplier_detail-0-cost": "",
            "supplier_detail-0-supplier": "",
            "supplier_detail-0-id": "",
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = SupplierUpdateView.as_view()(request, pk=self.supplier.pk)

        assert response.status_code == 302
        assert Supplier.objects.get(pk=self.supplier.pk).name == params["name"]
        assert Supplier.objects.get(pk=self.supplier.pk).code == params["code"]

    def test_post_with_master_product_data(self):
        params = {
            "name": "Test Supplier",
            "code": "Sup-123",
            "email": "",
            "contact": "",
            "phone": "",
            "address": "",
            "master_product_data": "on",
            "supplier_detail-TOTAL_FORMS": "2",
            "supplier_detail-INITIAL_FORMS": "1",
            "supplier_detail-MIN_NUM_FORMS": "1",
            "supplier_detail-MAX_NUM_FORMS": "100",
            "supplier_detail-0-product": self.product.pk,
            "supplier_detail-0-quantity": 100,
            "supplier_detail-0-unit": self.unit.pk,
            "supplier_detail-0-cost": 10000,
            "supplier_detail-0-supplier": self.supplier_2.pk,
            "supplier_detail-0-id": self.supplier_2.supplier_detail.first().pk,
            "supplier_detail-1-product": self.product_2.pk,
            "supplier_detail-1-quantity": 1,
            "supplier_detail-1-unit": self.unit.pk,
            "supplier_detail-1-cost": 50000,
            "supplier_detail-1-supplier": "",
            "supplier_detail-1-id": "",
        }
        request = self.factory.post(self.url_2, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = SupplierUpdateView.as_view()(request, pk=self.supplier_2.pk)

        assert response.status_code == 302
        assert Supplier.objects.get(pk=self.supplier_2.pk).name == params["name"]
        assert Supplier.objects.get(pk=self.supplier_2.pk).code == params["code"]
        assert Supplier.objects.get(pk=self.supplier_2.pk).supplier_detail.count() == 2


class TestSupplierDeleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        self.supplier = mommy.make(Supplier, name="test", account=account)
        set_current_tenant(account)

        self.url = reverse(
            "stocks:stocks_supplier_delete", kwargs={"pk": self.supplier.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_delete(self):
        request = self.factory.post(self.url)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)
        response = SupplierDeleteView.as_view()(request, pk=self.supplier.pk)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            messages._queued_messages[0].message,
            "{} {} was successfully {}!".format(
                Supplier._meta.verbose_name.title(), self.supplier, _("deleted"),
            ),
        )


class TestStockTransferListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_transfer_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer_1 = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer_1.transfer_product_stock(self.product, self.unit, 10)

        self.stock_transfer_2 = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer_2.transfer_product_stock(self.product, self.unit, 5)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_transfer"))
        self.assertEqual(response.status_code, 200)

    def test_stock_transfer_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 8,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockTransferListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_filter_received(self):
        total_column = 8
        data = {"draw": 8}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[5][data]": 5,
                "columns[5][name]": "",
                "columns[5][searchable]": True,
                "columns[5][orderable]": True,
                "columns[5][search][value]": "received",
                "columns[5][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockTransferListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 0,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_filter_show_all(self):
        total_column = 8
        data = {"draw": 8}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[5][data]": 5,
                "columns[5][name]": "",
                "columns[5][searchable]": True,
                "columns[5][orderable]": True,
                "columns[5][search][value]": "show",
                "columns[5][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockTransferListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )


class TestStockTransferCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_transfer_data")
        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "date_of_transfer": datetime.today().strftime("%d/%m/%Y"),
            "source": self.source.pk,
            "destination": self.destination.pk,
            "transfered_stock-TOTAL_FORMS": 1,
            "transfered_stock-INITIAL_FORMS": 0,
            "transfered_stock-MIN_NUM_FORMS": 1,
            "transfered_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "transfered_stock-0-product": self.product.pk,
            "transfered_stock-0-quantity": 10,
            "transfered_stock-0-unit": self.unit.pk,
            "transfered_stock-0-source_stock": "{} {}".format(
                self.source_stock.balance, self.unit.unit
            ),
            "transfered_stock-0-stock_transfer": "",
            "transfered_stock-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferCreateView.as_view()(request)

        assert response.status_code == 302
        assert StockTransfer.objects.last().source == self.source
        assert StockTransfer.objects.last().destination == self.destination
        assert StockTransfer.objects.last().transfered_stock.all().count() == 1

    def test_form_invalid(self):
        params = {
            "date_of_transfer": datetime.today().strftime("%d/%m/%Y"),
            "source": self.source.pk,
            "destination": "",
            "transfered_stock-TOTAL_FORMS": 1,
            "transfered_stock-INITIAL_FORMS": 0,
            "transfered_stock-MIN_NUM_FORMS": 1,
            "transfered_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "transfered_stock-0-product": self.product.pk,
            "transfered_stock-0-quantity": 10,
            "transfered_stock-0-unit": self.unit.pk,
            "transfered_stock-0-source_stock": "{} {}".format(
                self.source_stock.balance, self.unit.unit
            ),
            "transfered_stock-0-stock_transfer": "",
            "transfered_stock-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferCreateView.as_view()(request)
        assert response.status_code == 200


class TestStockTransferUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer.transfer_product_stock(self.product, self.unit, 10)

        self.url = reverse(
            "stocks:stocks_transfer_update", kwargs={"pk": self.stock_transfer.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "date_of_transfer": self.stock_transfer.date_of_transfer,
            "source": self.stock_transfer.source.pk,
            "destination": self.stock_transfer.destination.pk,
            "transfered_stock-TOTAL_FORMS": 1,
            "transfered_stock-INITIAL_FORMS": 1,
            "transfered_stock-MIN_NUM_FORMS": 1,
            "transfered_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "transfered_stock-0-product": self.product.pk,
            "transfered_stock-0-quantity": 10,
            "transfered_stock-0-unit": self.unit.pk,
            "transfered_stock-0-source_stock": "{} {}".format(
                self.source_stock.balance, self.unit.unit
            ),
            "transfered_stock-0-stock_transfer": self.stock_transfer.pk,
            "transfered_stock-0-id": self.stock_transfer.transfered_stock.all()
            .first()
            .pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferUpdateView.as_view()(request, pk=self.stock_transfer.pk)

        assert response.status_code == 302
        assert StockTransfer.objects.last().source == self.source
        assert StockTransfer.objects.last().destination == self.destination
        assert StockTransfer.objects.last().transfered_stock.all().count() == 1

    def test_form_invalid(self):
        params = {
            "date_of_transfer": self.stock_transfer.date_of_transfer,
            "source": self.stock_transfer.source.pk,
            "destination": self.stock_transfer.source.pk,
            "transfered_stock-TOTAL_FORMS": 1,
            "transfered_stock-INITIAL_FORMS": 1,
            "transfered_stock-MIN_NUM_FORMS": 1,
            "transfered_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "transfered_stock-0-product": self.product.pk,
            "transfered_stock-0-quantity": 10,
            "transfered_stock-0-unit": self.unit.pk,
            "transfered_stock-0-source_stock": "{} {}".format(
                self.source_stock.balance, self.unit.unit
            ),
            "transfered_stock-0-stock_transfer": self.stock_transfer.pk,
            "transfered_stock-0-id": self.stock_transfer.transfered_stock.all()
            .first()
            .pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferUpdateView.as_view()(request, pk=self.stock_transfer.pk)
        assert response.status_code == 200

    def test_formset_invalid(self):
        params = {
            "date_of_transfer": self.stock_transfer.date_of_transfer,
            "source": self.stock_transfer.source.pk,
            "destination": self.stock_transfer.destination.pk,
            "transfered_stock-TOTAL_FORMS": 1,
            "transfered_stock-INITIAL_FORMS": 1,
            "transfered_stock-MIN_NUM_FORMS": 1,
            "transfered_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "transfered_stock-0-product": self.product.pk,
            "transfered_stock-0-quantity": 10,
            "transfered_stock-0-unit": self.unit.pk,
            "transfered_stock-0-source_stock": "{} {}".format(
                self.source_stock.balance, self.unit.unit
            ),
            "transfered_stock-0-stock_transfer": self.stock_transfer.pk,
            "transfered_stock-0-id": self.stock_transfer.transfered_stock.all()
            .first()
            .pk,
            "transfered_stock-0-DELETE": "[on]",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferUpdateView.as_view()(request, pk=self.stock_transfer.pk)
        assert response.status_code == 200

    def test_detele_stock_transfer_detail(self):
        product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        source_stock_2, _ = Stock.objects.get_or_create(
            product=product_2, outlet=self.source, account=self.account
        )
        source_stock_2.set_balance(100, unit=self.unit)
        destination_stock_2, _ = Stock.objects.get_or_create(
            product=product_2, outlet=self.destination, account=self.account
        )
        destination_stock_2.set_balance(20, unit=self.unit)
        self.stock_transfer.transfer_product_stock(product_2, self.unit, 10)

        params = {
            "date_of_transfer": self.stock_transfer.date_of_transfer,
            "source": self.stock_transfer.source.pk,
            "destination": self.stock_transfer.destination.pk,
            "transfered_stock-TOTAL_FORMS": 2,
            "transfered_stock-INITIAL_FORMS": 2,
            "transfered_stock-MIN_NUM_FORMS": 1,
            "transfered_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "transfered_stock-0-product": self.product.pk,
            "transfered_stock-0-quantity": 10,
            "transfered_stock-0-unit": self.unit.pk,
            "transfered_stock-0-source_stock": "{} {}".format(
                self.source_stock.balance, self.unit.unit
            ),
            "transfered_stock-0-stock_transfer": self.stock_transfer.pk,
            "transfered_stock-0-id": self.stock_transfer.transfered_stock.get(
                stock_source__product=self.product
            ).pk,
            "transfered_stock-0-DELETE": "[on]",
            "transfered_stock-1-product": product_2.pk,
            "transfered_stock-1-quantity": 10,
            "transfered_stock-1-unit": self.unit.pk,
            "transfered_stock-1-source_stock": "{} {}".format(
                source_stock_2.balance, self.unit.unit
            ),
            "transfered_stock-1-stock_transfer": self.stock_transfer.pk,
            "transfered_stock-1-id": self.stock_transfer.transfered_stock.get(
                stock_source__product=product_2
            ).pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferUpdateView.as_view()(request, pk=self.stock_transfer.pk)
        assert response.status_code == 302
        assert StockTransfer.objects.last().source == self.source
        assert StockTransfer.objects.last().destination == self.destination
        assert StockTransfer.objects.last().transfered_stock.all().count() == 1


class TestStockTransferDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer.transfer_product_stock(self.product, self.unit, 10)

        self.url = reverse(
            "stocks:stocks_transfer_detail", kwargs={"pk": self.stock_transfer.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestStockTrasnferReceivedView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer.transfer_product_stock(self.product, self.unit, 10)

        self.url = reverse(
            "stocks:stocks_transfer_receive", kwargs={"pk": self.stock_transfer.pk}
        )
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_post_ajax(self):
        request = self.factory.post(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferReceiveView.as_view()(
            request, pk=self.stock_transfer.pk
        )
        self.assertEqual(response.status_code, 200)

        self.source_stock.balance == 90
        self.destination_stock.balance == 30


class TestStockTransferBatchTrackedView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Product-X",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer.transfer_product_stock(self.product, self.unit, 10)

        self.batch = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product,
            batch_id="Batch-01",
        )
        self.soure_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.source,
        )
        self.destination_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.destination,
        )
        self.batch_other = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product,
            batch_id="Batch-02",
        )
        self.source_batch_other = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_other,
            outlet=self.source,
        )
        self.soure_batch.set_batch_balance(80, self.unit)
        self.source_batch_other.set_batch_balance(20, self.unit)
        self.destination_batch.set_batch_balance(20, self.unit)

        self.url = reverse(
            "stocks:stocks_transfers_batch_tracked",
            kwargs={"pk": self.stock_transfer.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "form-TOTAL_FORMS": 1,
            "form-INITIAL_FORMS": 0,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-batch_items-TOTAL_FORMS": 1,
            "form-0-batch_items-INITIAL_FORMS": 0,
            "form-0-batch_items-MIN_NUM_FORMS": 0,
            "form-0-batch_items-MAX_NUM_FORMS": 1000,
            "form-0-product": self.product.pk,
            "form-0-product_name": self.product.name,
            "form-0-quantity_to_assign": "{} {}".format(0, self.unit.unit),
            "form-0-unit": self.unit.pk,
            "form-0-total_quantity": 10,
            "form-0-id": [""],
            "form-0-batch_items-0-batch_id": self.batch.batch_id,
            "form-0-batch_items-0-batch_quantity": "{} {}".format(
                self.destination_batch.balance, self.unit.unit
            ),
            "form-0-batch_items-0-quantity": 10,
            "form-0-batch_items-0-unit": self.unit.pk,
            "form-0-batch_items-0-id": [""],
            "form-0-batch_items-0-batch": [""],
        }
        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.destination_batch.balance, 30)

    def test_batch_destination_doesnt_exist(self):
        params = {
            "form-TOTAL_FORMS": 1,
            "form-INITIAL_FORMS": 0,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-batch_items-TOTAL_FORMS": 1,
            "form-0-batch_items-INITIAL_FORMS": 0,
            "form-0-batch_items-MIN_NUM_FORMS": 0,
            "form-0-batch_items-MAX_NUM_FORMS": 1000,
            "form-0-product": self.product.pk,
            "form-0-product_name": self.product.name,
            "form-0-quantity_to_assign": "{} {}".format(0, self.unit.unit),
            "form-0-unit": self.unit.pk,
            "form-0-total_quantity": 5,
            "form-0-id": [""],
            "form-0-batch_items-0-batch_id": self.batch_other.batch_id,
            "form-0-batch_items-0-batch_quantity": "0",
            "form-0-batch_items-0-quantity": 5,
            "form-0-batch_items-0-unit": self.unit.pk,
            "form-0-batch_items-0-id": [""],
            "form-0-batch_items-0-batch": [""],
        }
        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.source_batch_other.balance, 15)


class TestStockTransferDeleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer.transfer_product_stock(self.product, self.unit, 10)

        self.url = reverse(
            "stocks:stocks_transfer_delete", kwargs={"pk": self.stock_transfer.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_delete(self):
        request = self.factory.post(self.url, data={})
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockTransferDeleteView.as_view()(request, pk=self.stock_transfer.pk)
        assert response.status_code == 302


class TestProductInventoryAutocompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.account")
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.product",
            name="Baso",
            is_manage_stock=True,
            account=self.account,
            uom=self.unit,
        )
        self.catalogue = Catalogue.prepare_catalogue(self.product)
        self.url = reverse("stocks:product_inventory_autocomplete")
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {
            "autocomplete": "{} - {} - {}".format(
                self.product.catalogue.name, self.product.name, self.product.sku
            )
        }
        params = {"q": "Bas"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = ProductInventoryAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        uoms = response_data["results"]
        self.assertEqual(uoms[0]["text"], expected_result["autocomplete"])


class TestPurchaseOrderInventoryAutocomplete(TestProductInventoryAutocompleteView):
    def setUp(self):
        super(TestPurchaseOrderInventoryAutocomplete, self).setUp()
        self.url = reverse("stocks:purchase_order_autocomplete")

    def test_autocomplete_success(self):
        expected_result = {
            "autocomplete": "{} - {} - {}".format(
                self.product.catalogue.name, self.product.name, self.product.sku
            )
        }
        params = {"q": "Bas"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = PurchaseOrderInventoryAutocomplete.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        uoms = response_data["results"]
        self.assertEqual(uoms[0]["text"], expected_result["autocomplete"])


class TestOutletInventoryAutocompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.account")
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.outlet = mommy.make("outlet.Outlet", name="Jakarta", account=self.account)
        self.url = reverse("stocks:outlet_inventory_autocomplete")
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": self.outlet.name}
        params = {"q": "Jak"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = OutletInventoryAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        data = response_data["results"]
        self.assertEqual(data[0]["text"], expected_result["autocomplete"])


class TestOutletDestinationInventoryAutocompleteView(
    TestOutletInventoryAutocompleteView
):
    def setUp(self):
        super(TestOutletDestinationInventoryAutocompleteView, self).setUp()
        self.url = reverse("stocks:outlet_dest_inventory_autocomplete")

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": self.outlet.name}
        params = {"q": "Jak"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = OutletDestinationInventoryAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)

        data = response_data["results"]
        self.assertEqual(data[0]["text"], expected_result["autocomplete"])


class TestStockCountListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_count_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )

        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(100, unit=self.unit)
        self.stock_2.set_balance(20, unit=self.unit)

        self.stock_count_1 = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count_1.count_product_stock(self.product_1)

        self.stock_count_2 = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count_2.count_product_stock(self.product_2)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_count"))
        self.assertEqual(response.status_code, 200)

    def test_stock_count_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 7,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockCountListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_filter_status_complete(self):
        total_column = 7
        data = {"draw": 7}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[4][data]": 4,
                "columns[4][name]": "",
                "columns[4][searchable]": True,
                "columns[4][orderable]": True,
                "columns[4][search][value]": "completed",
                "columns[4][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockCountListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 0,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_filter_status_show_all(self):
        total_column = 7
        data = {"draw": 7}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[4][data]": 4,
                "columns[4][name]": "",
                "columns[4][searchable]": True,
                "columns[4][orderable]": True,
                "columns[4][search][value]": "show",
                "columns[4][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockCountListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )


class TestStockCountDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.stock_count = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count.count_product_stock(self.product)
        self.url = reverse(
            "stocks:stocks_count_detail", kwargs={"pk": self.stock_count.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestStockCountCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_count_create")
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "outlet": self.outlet.pk,
            "date_of_stock_count": datetime.today().strftime("%d/%m/%Y"),
            "note": "",
            "counted_stock-TOTAL_FORMS": 1,
            "counted_stock-INITIAL_FORMS": 0,
            "counted_stock-MIN_NUM_FORMS": 1,
            "counted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "counted_stock-0-product": self.product.pk,
            "counted_stock-0-stock_count": "",
            "counted_stock-0-id": "",
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountCreateView.as_view()(request)

        assert response.status_code == 302
        assert StockCount.objects.last().outlet == self.outlet
        assert StockCount.objects.last().counted_stock.all().count() == 1

    def test_form_invalid(self):
        params = {
            "outlet": self.outlet.pk,
            "date_of_stock_count": datetime.today().strftime("%d/%m/%Y"),
            "note": "",
            "counted_stock-TOTAL_FORMS": 1,
            "counted_stock-INITIAL_FORMS": 0,
            "counted_stock-MIN_NUM_FORMS": 1,
            "counted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "counted_stock-0-product": "",
            "counted_stock-0-stock_count": "",
            "counted_stock-0-id": "",
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountCreateView.as_view()(request)
        assert response.status_code == 200


class TestStockCountUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)

        self.stock_count = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count.count_product_stock(self.product)

        self.url = reverse(
            "stocks:stocks_count_update", kwargs={"pk": self.stock_count.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "outlet": self.stock_count.outlet.pk,
            "date_of_stock_count": datetime.today().strftime("%d/%m/%Y"),
            "note": "",
            "counted_stock-TOTAL_FORMS": 1,
            "counted_stock-INITIAL_FORMS": 1,
            "counted_stock-MIN_NUM_FORMS": 1,
            "counted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "counted_stock-0-product": self.product.pk,
            "counted_stock-0-stock_count": self.stock_count.pk,
            "counted_stock-0-id": self.stock_count.counted_stock.all().first().pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountUpdateView.as_view()(request, pk=self.stock_count.pk)

        assert response.status_code == 302
        assert StockCount.objects.last().outlet == self.outlet
        assert StockCount.objects.last().counted_stock.all().count() == 1

    def test_form_invalid(self):
        params = {
            "outlet": self.stock_count.outlet.pk,
            "date_of_stock_count": datetime.today().strftime("%d/%m/%Y"),
            "note": "",
            "counted_stock-TOTAL_FORMS": 1,
            "counted_stock-INITIAL_FORMS": 1,
            "counted_stock-MIN_NUM_FORMS": 1,
            "counted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "counted_stock-0-product": self.product.pk,
            "counted_stock-0-stock_count": self.stock_count.pk,
            "counted_stock-0-id": self.stock_count.counted_stock.all().first().pk,
            "counted_stock-0-DELETE": "[on]",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountUpdateView.as_view()(request, pk=self.stock_count.pk)

        assert response.status_code == 200

    def test_delete_stock_count_detail(self):
        product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        stock_2, _ = Stock.objects.get_or_create(
            product=product_2, outlet=self.outlet, account=self.account
        )
        stock_2.set_balance(10, unit=self.unit)
        self.stock_count.count_product_stock(product_2)
        params = {
            "outlet": self.stock_count.outlet.pk,
            "date_of_stock_count": datetime.today().strftime("%d/%m/%Y"),
            "note": "",
            "counted_stock-TOTAL_FORMS": 2,
            "counted_stock-INITIAL_FORMS": 2,
            "counted_stock-MIN_NUM_FORMS": 1,
            "counted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "counted_stock-0-product": self.product.pk,
            "counted_stock-0-stock_count": self.stock_count.pk,
            "counted_stock-0-id": self.stock_count.counted_stock.get(
                stock__product=self.product
            ).pk,
            "counted_stock-0-DELETE": "[on]",
            "counted_stock-1-product": product_2.pk,
            "counted_stock-1-stock_count": self.stock_count.pk,
            "counted_stock-1-id": self.stock_count.counted_stock.get(
                stock__product=product_2
            ).pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountUpdateView.as_view()(request, pk=self.stock_count.pk)

        assert response.status_code == 302
        assert StockCount.objects.last().outlet == self.outlet
        assert StockCount.objects.last().counted_stock.all().count() == 1


class TestStockCountCountingView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="Product-x",
            sku="PRX-001",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)

        self.stock_count = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count.count_product_stock(self.product)

        self.url = reverse(
            "stocks:stocks_count_counting", kwargs={"pk": self.stock_count.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "counted_stock-TOTAL_FORMS": 1,
            "counted_stock-INITIAL_FORMS": 1,
            "counted_stock-MIN_NUM_FORMS": 0,
            "counted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "counted_stock-0-counted": 200,
            "counted_stock-0-unit": self.unit.pk,
            "counted_stock-0-id": self.stock_count.counted_stock.all().first().pk,
            "counted_stock-0-stock_count": self.stock_count.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountCountingView.as_view()(request, pk=self.stock_count.pk)

        assert response.status_code == 302


class TestStockCountDeleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="Product-x",
            sku="PRX-001",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)

        self.stock_count = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count.count_product_stock(self.product)

        self.url = reverse(
            "stocks:stocks_count_delete", kwargs={"pk": self.stock_count.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_delete(self):
        request = self.factory.post(self.url, data={})
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountDeleteView.as_view()(request, pk=self.stock_count.pk)
        assert response.status_code == 302


class TestStockCountBatchTrackedView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Product-X",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)

        self.stock_count = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count.count_product_stock(self.product)

        for counted in self.stock_count.counted_stock.all():
            counted.counted = 50
            counted.unit = self.unit
            counted.save()

        self.batch = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product,
            batch_id="Batch-01",
        )
        self.item_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.outlet,
        )

        self.item_batch.set_batch_balance(100, self.unit)

        self.url = reverse(
            "stocks:stocks_count_batch_tracked", kwargs={"pk": self.stock_count.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "form-TOTAL_FORMS": 1,
            "form-INITIAL_FORMS": 0,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-batch_items-TOTAL_FORMS": 1,
            "form-0-batch_items-INITIAL_FORMS": 0,
            "form-0-batch_items-MIN_NUM_FORMS": 0,
            "form-0-batch_items-MAX_NUM_FORMS": 1000,
            "form-0-product": self.product.pk,
            "form-0-product_name": self.product.name,
            "form-0-quantity_to_assign": "{} {}".format(0, self.unit.unit),
            "form-0-unit": self.unit.pk,
            "form-0-total_quantity": 50,
            "form-0-id": [""],
            "form-0-batch_items-0-batch_id": self.batch.batch_id,
            "form-0-batch_items-0-batch_quantity": "{} {}".format(
                self.item_batch.balance, self.unit.unit
            ),
            "form-0-batch_items-0-quantity": 50,
            "form-0-batch_items-0-unit": self.unit.pk,
            "form-0-batch_items-0-id": [""],
            "form-0-batch_items-0-batch": [""],
        }
        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.item_batch.balance, 50)

    def test_batch_doesnt_exist(self):
        params = {
            "form-TOTAL_FORMS": 1,
            "form-INITIAL_FORMS": 0,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-batch_items-TOTAL_FORMS": 1,
            "form-0-batch_items-INITIAL_FORMS": 0,
            "form-0-batch_items-MIN_NUM_FORMS": 0,
            "form-0-batch_items-MAX_NUM_FORMS": 1000,
            "form-0-product": self.product.pk,
            "form-0-product_name": self.product.name,
            "form-0-quantity_to_assign": "{} {}".format(0, self.unit.unit),
            "form-0-unit": self.unit.pk,
            "form-0-total_quantity": 50,
            "form-0-id": [""],
            "form-0-batch_items-0-batch_id": "NEW",
            "form-0-batch_items-0-batch_quantity": 0,
            "form-0-batch_items-0-quantity": 50,
            "form-0-batch_items-0-unit": self.unit.pk,
            "form-0-batch_items-0-expiration_date": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "form-0-batch_items-0-id": [""],
            "form-0-batch_items-0-batch": [""],
        }
        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 302)


class TestStockCountCompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.stock_count = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            status="in_progress",
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count.count_product_stock(self.product)
        self.url = reverse(
            "stocks:stocks_count_complete", kwargs={"pk": self.stock_count.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_post_ajax(self):
        request = self.factory.post(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockCountCompleteView.as_view()(request, pk=self.stock_count.pk)
        self.assertEqual(response.status_code, 200)

        self.stock.balance == 0


class TestStockQuantityView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)
        self.url = reverse("stocks:get_quantity")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {"product": self.product.pk, "outlet": self.outlet.pk}
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockQuantityView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_fail(self):
        params = {"product": "", "outlet": self.outlet.pk}
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockQuantityView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestStockQuantityConversion(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.other_unit = self.account.uom_set.filter(
            category=self.unit.category
        ).last()

        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)
        self.url = reverse("stocks:get_converted")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "product": self.product.pk,
            "quantity": 50,
            "unit": self.other_unit.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockQuantityConversion.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_fail(self):
        params = {"product": self.product.pk, "quantity": 50, "unit": ""}
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = StockQuantityConversion.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestStockAdjustmentListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_adjustment_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )

        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(10, unit=self.unit)
        self.stock_2.set_balance(20, unit=self.unit)

        self.stock_adjustment_1 = mommy.make(
            "stocks.StockAdjustment",
            account=self.account,
            outlet=self.outlet,
            date_of_adjustment=datetime.today(),
        )
        self.stock_adjustment_1.adjust_product_stock(
            self.product_1, self.unit, 20, 10000
        )

        self.stock_adjustment_2 = mommy.make(
            "stocks.StockAdjustment",
            account=self.account,
            outlet=self.outlet,
            date_of_adjustment=datetime.today(),
        )
        self.stock_adjustment_1.adjust_product_stock(self.product_2, self.unit, 5, 5000)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_adjustment"))
        self.assertEqual(response.status_code, 200)

    def test_stock_adjustment_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 5,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockAdjustmentListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_filter_status_item_receive(self):
        total_column = 5
        data = {"draw": 5}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[3][data]": 3,
                "columns[3][name]": "",
                "columns[3][searchable]": True,
                "columns[3][orderable]": True,
                "columns[3][search][value]": "item_receive",
                "columns[3][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockAdjustmentListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_filter_status_show_all(self):
        total_column = 5
        data = {"draw": 5}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[3][data]": 3,
                "columns[3][name]": "",
                "columns[3][searchable]": True,
                "columns[3][orderable]": True,
                "columns[3][search][value]": "show",
                "columns[3][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = StockAdjustmentListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )


class TestStockAdjustmentDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.stock_adjustment = mommy.make(
            "stocks.StockAdjustment",
            account=self.account,
            outlet=self.outlet,
            status="item_receive",
            date_of_adjustment=datetime.today(),
        )
        self.stock_adjustment.adjust_product_stock(self.product, self.unit, 20, 1000)
        self.stock_adjustment.process_adjustment(self.user)
        self.url = reverse(
            "stocks:stocks_adjustment_detail", kwargs={"pk": self.stock_adjustment.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestSupplierAutocompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.account")
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.supplier = mommy.make(
            "stocks.supplier", account=self.account, name="Supplier"
        )
        self.url = reverse("stocks:stocks_get_supplier")
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": "{}".format(self.supplier.name)}
        params = {"q": "Sup"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = SupplierAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        suppliers = response_data["results"]
        self.assertEqual(suppliers[0]["text"], expected_result["autocomplete"])


class TestBatchAutocompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.account")
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.batch_1 = mommy.make(
            "stocks.Batch",
            account=self.account,
            batch_id="test-batch-01",
            product=self.product,
        )
        self.item_batch_1 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_1,
            outlet=self.outlet,
        )
        self.batch_2 = mommy.make(
            "stocks.Batch",
            account=self.account,
            batch_id="batch-02",
            product=self.product,
        )
        self.item_batch_2 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_2,
            outlet=self.outlet,
        )

        self.url = reverse(
            "stocks:get_batch",
            kwargs={"pk": self.product.pk, "outlet_pk": self.outlet.pk},
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": "{}".format(self.batch_1.batch_id)}
        params = {"q": "tes"}

        self.login(email=self.user.email, password="1sampai6")
        response = self.get(self.url, data=params)

        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        batches = response_data["results"]
        self.assertEqual(batches[0]["text"], expected_result["autocomplete"])


class TestBatchCreateGetView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.account")
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.batch_1 = mommy.make(
            "stocks.Batch",
            account=self.account,
            batch_id="test-batch-01",
            product=self.product,
        )
        self.batch_2 = mommy.make(
            "stocks.Batch",
            account=self.account,
            batch_id="batch-02",
            product=self.product,
        )

        self.url = reverse("stocks:get_create_batch", kwargs={"pk": self.product.pk})
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": "003-batch-new"}
        params = {"text": "003-batch-new"}

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["text"], expected_result["autocomplete"])

    def test_autocomplete_fail(self):

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data={})

        self.assertEqual(response.status_code, 400)


class TestBatchInformationView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.account")
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.batch = mommy.make(
            "stocks.Batch",
            account=self.account,
            batch_id="test-batch-01",
            product=self.product,
            expiration_date=datetime.today(),
        )
        self.item_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.outlet,
        )

        self.item_batch.set_batch_balance(100, self.unit)

        self.batch_2 = mommy.make(
            "stocks.Batch",
            account=self.account,
            batch_id="test-batch-02",
            product=self.product,
            expiration_date=datetime.today(),
        )
        self.url = reverse("stocks:get_expiration_date")
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "batch_id": self.batch.batch_id,
            "product": self.product.pk,
            "outlet_pk": self.outlet.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BatchInformationView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_batch_doesnt_exist(self):
        params = {
            "batch_id": "New Batch",
            "product": self.product.pk,
            "outlet_pk": self.outlet.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BatchInformationView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_no_item_batch(self):
        params = {
            "batch_id": self.batch_2.batch_id,
            "product": self.product.pk,
            "outlet_pk": self.outlet.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BatchInformationView.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestStockAdjustmentWizardView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.account")
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            name="Product-x",
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(20, unit=self.unit)
        self.batch = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product,
            batch_id="Batch-01",
        )
        self.item_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.outlet,
        )
        self.item_batch.set_batch_balance(20, self.unit)

        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            name="Product-y",
            uom=self.unit,
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock_2.set_balance(20, unit=self.unit)

        self.url = reverse("stocks:stocks_adjustment_create")

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="1sampai6")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_no_batch_tracked_fail(self):
        params = {
            "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
            "stock_adjustment_form-status": "item_receive",
            "stock_adjustment_form-outlet": self.outlet.pk,
            "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "stock_adjustment_form-note": "",
            "adjusted_stock-TOTAL_FORMS": 1,
            "adjusted_stock-INITIAL_FORMS": 0,
            "adjusted_stock-MIN_NUM_FORMS": 1,
            "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "adjusted_stock-0-product": "",
            "adjusted_stock-0-quantity": "",
            "adjusted_stock-0-unit": self.unit.pk,
            "adjusted_stock-0-cost": 0,
            "adjusted_stock-0-stock_adjustment": "",
            "adjusted_stock-0-id": "",
        }

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 200)

    def test_post_batch_tracked_fail_quantity_less_than_assign_qty(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "item_receive",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": "",
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": 0,
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 0,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 200)

    def test_post_batch_tracked_fail_quantity_more_than_qty_assign(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "item_receive",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 1,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": 0,
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 20,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 200)

    def test_post_batch_tracked_fail_quantity_less_than_assign_qty_2(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "item_receive",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 1,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": 0,
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 0,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 200)

    def test_post_item_receive_no_batch_tracked(self):
        params = {
            "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
            "stock_adjustment_form-status": "item_receive",
            "stock_adjustment_form-outlet": self.outlet.pk,
            "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "stock_adjustment_form-note": "",
            "adjusted_stock-TOTAL_FORMS": 1,
            "adjusted_stock-INITIAL_FORMS": 0,
            "adjusted_stock-MIN_NUM_FORMS": 1,
            "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "adjusted_stock-0-product": self.product_2.pk,
            "adjusted_stock-0-quantity": 1,
            "adjusted_stock-0-unit": self.unit.pk,
            "adjusted_stock-0-cost": 0,
            "adjusted_stock-0-stock_adjustment": "",
            "adjusted_stock-0-id": "",
        }

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 302)

    def test_post_item_receive_batch_trakced(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "item_receive",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 1,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": 0,
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 1,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)

    def test_post_item_receive_new_batch_tracked(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "item_receive",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 1,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": 0,
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": "new-batch",
                "batch_tracked_form-0-batch_items-0-batch_quantity": "0",
                "batch_tracked_form-0-batch_items-0-quantity": 1,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-expiration_date": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)

    def test_post_stock_count_no_batch_tracked(self):
        params = {
            "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
            "stock_adjustment_form-status": "stock_count",
            "stock_adjustment_form-outlet": self.outlet.pk,
            "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "stock_adjustment_form-note": "",
            "adjusted_stock-TOTAL_FORMS": 1,
            "adjusted_stock-INITIAL_FORMS": 0,
            "adjusted_stock-MIN_NUM_FORMS": 1,
            "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "adjusted_stock-0-product": self.product_2.pk,
            "adjusted_stock-0-quantity": 25,
            "adjusted_stock-0-unit": self.unit.pk,
            "adjusted_stock-0-cost": "",
            "adjusted_stock-0-stock_adjustment": "",
            "adjusted_stock-0-id": "",
        }

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 302)

    def test_post_stock_count_batch_tracked(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "stock_count",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 25,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": "",
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 1,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)

    def test_post_stock_count_new_batch_traked(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "stock_count",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 25,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": "",
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": "new-batch",
                "batch_tracked_form-0-batch_items-0-batch_quantity": "0",
                "batch_tracked_form-0-batch_items-0-quantity": 1,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-expiration_date": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)

    def test_post_item_loss_no_batch_trakced(self):
        params = {
            "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
            "stock_adjustment_form-status": "item_loss",
            "stock_adjustment_form-outlet": self.outlet.pk,
            "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "stock_adjustment_form-note": "",
            "adjusted_stock-TOTAL_FORMS": 1,
            "adjusted_stock-INITIAL_FORMS": 0,
            "adjusted_stock-MIN_NUM_FORMS": 1,
            "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "adjusted_stock-0-product": self.product_2.pk,
            "adjusted_stock-0-quantity": 1,
            "adjusted_stock-0-unit": self.unit.pk,
            "adjusted_stock-0-cost": "",
            "adjusted_stock-0-stock_adjustment": "",
            "adjusted_stock-0-id": "",
        }

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 302)

    def test_post_item_loss_batch_tracked(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "item_loss",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 1,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": "",
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 1,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)

    def test_post_item_damaged_no_batch_trakced(self):
        params = {
            "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
            "stock_adjustment_form-status": "item_damaged",
            "stock_adjustment_form-outlet": self.outlet.pk,
            "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "stock_adjustment_form-note": "",
            "adjusted_stock-TOTAL_FORMS": 1,
            "adjusted_stock-INITIAL_FORMS": 0,
            "adjusted_stock-MIN_NUM_FORMS": 1,
            "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "adjusted_stock-0-product": self.product_2.pk,
            "adjusted_stock-0-quantity": 1,
            "adjusted_stock-0-unit": self.unit.pk,
            "adjusted_stock-0-cost": "",
            "adjusted_stock-0-stock_adjustment": "",
            "adjusted_stock-0-id": "",
        }

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 302)

    def test_post_item_damaged_batch_tracked(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "item_damaged",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 1,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": "",
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 1,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)

    def test_post_waste_product_no_batch_trakced(self):
        params = {
            "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
            "stock_adjustment_form-status": "waste_product",
            "stock_adjustment_form-outlet": self.outlet.pk,
            "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "stock_adjustment_form-note": "",
            "adjusted_stock-TOTAL_FORMS": 1,
            "adjusted_stock-INITIAL_FORMS": 0,
            "adjusted_stock-MIN_NUM_FORMS": 1,
            "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "adjusted_stock-0-product": self.product_2.pk,
            "adjusted_stock-0-quantity": 1,
            "adjusted_stock-0-unit": self.unit.pk,
            "adjusted_stock-0-cost": "",
            "adjusted_stock-0-stock_adjustment": "",
            "adjusted_stock-0-id": "",
        }

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 302)

    def test_post_waste_product_batch_tracked(self):
        params = (
            {
                "stock_adjustment_wizard_view-current_step": "stock_adjustment_form",
                "stock_adjustment_form-status": "waste_product",
                "stock_adjustment_form-outlet": self.outlet.pk,
                "stock_adjustment_form-date_of_adjustment": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "stock_adjustment_form-note": "",
                "adjusted_stock-TOTAL_FORMS": 1,
                "adjusted_stock-INITIAL_FORMS": 0,
                "adjusted_stock-MIN_NUM_FORMS": 1,
                "adjusted_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "adjusted_stock-0-product": self.product.pk,
                "adjusted_stock-0-quantity": 1,
                "adjusted_stock-0-unit": self.unit.pk,
                "adjusted_stock-0-cost": "",
                "adjusted_stock-0-stock_adjustment": "",
                "adjusted_stock-0-id": "",
            },
            {
                "stock_adjustment_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product.pk,
                "batch_tracked_form-0-product_name": self.product.name,
                "batch_tracked_form-0-quantity_to_assign": "0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 1,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.item_batch.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": 1,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)


class TestPurchaseOrderListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_purchase_order_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )

        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(10, unit=self.unit)
        self.stock_2.set_balance(20, unit=self.unit)

        self.purchase_order_1 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order_1.purchase_product_stock(
            self.product_1, 10, self.unit, 1000, "percentage", 5
        )

        self.purchase_order_2 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order_2.purchase_product_stock(
            self.product_2, 10, self.unit, 1000, "percentage", 5
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_purchase_order"))
        self.assertEqual(response.status_code, 200)

    def test_purchase_order_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 6,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = PurchaseOrderListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_filter_status_show_all(self):
        total_column = 6
        data = {"draw": 6}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[4][data]": 4,
                "columns[4][name]": "",
                "columns[4][searchable]": True,
                "columns[4][orderable]": True,
                "columns[4][search][value]": "show",
                "columns[4][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = PurchaseOrderListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_filter_status_active(self):
        total_column = 6
        data = {"draw": 6}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[4][data]": 4,
                "columns[4][name]": "",
                "columns[4][searchable]": True,
                "columns[4][orderable]": True,
                "columns[4][search][value]": "active",
                "columns[4][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = PurchaseOrderListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )


class TestPurchaseOrderCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.inventorysetting, _ = self.account.inventorysetting_set.get_or_create(
            account=self.account
        )

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_purchase_order_create")
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="test",
            sku="test-01",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "date_of_purchase": datetime.today().strftime("%d/%m/%Y"),
            "supplier": self.supplier.pk,
            "outlet": self.outlet.pk,
            "note": "",
            "discount_type": "fixed",
            "discount": "0",
            "tax": "0",
            "purchased_stock-TOTAL_FORMS": 1,
            "purchased_stock-INITIAL_FORMS": 0,
            "purchased_stock-MIN_NUM_FORMS": 1,
            "purchased_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "purchased_stock-0-product": self.product.pk,
            "purchased_stock-0-quantity": 1,
            "purchased_stock-0-unit": self.unit.pk,
            "purchased_stock-0-cost": 10000,
            "purchased_stock-0-discount_type": "percentage",
            "purchased_stock-0-discount": 5,
            "purchased_stock-0-purchase_order": "",
            "purchased_stock-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderCreateView.as_view()(request)
        assert response.status_code == 302
        assert PurchaseOrder.objects.last().outlet == self.outlet
        assert PurchaseOrder.objects.last().supplier == self.supplier
        assert PurchaseOrder.objects.last().status == "active"
        assert PurchaseOrder.objects.last().purchased_stock.all().count() == 1

    def test_form_invalid(self):
        params = {
            "date_of_purchase": datetime.today().strftime("%d/%m/%Y"),
            "supplier": self.supplier.pk,
            "outlet": self.outlet.pk,
            "note": "",
            "purchased_stock-TOTAL_FORMS": 1,
            "purchased_stock-INITIAL_FORMS": 0,
            "purchased_stock-MIN_NUM_FORMS": 1,
            "purchased_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "purchased_stock-0-product": "",
            "purchased_stock-0-quantity": 1,
            "purchased_stock-0-unit": self.unit.pk,
            "purchased_stock-0-cost": 10000,
            "purchased_stock-0-purchase_order": "",
            "purchased_stock-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderCreateView.as_view()(request)

        assert response.status_code == 200



class TestPurchaseOrderDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse(
            "stocks:stocks_purchase_order_detail", kwargs={"pk": self.purchase_order.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestPurchaseOrderUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.inventorysetting, _ = self.account.inventorysetting_set.get_or_create(
            account=self.account
        )

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="test",
            sku="test-1",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse(
            "stocks:stocks_purchase_order_update", kwargs={"pk": self.purchase_order.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "date_of_purchase": datetime.today().strftime("%d/%m/%Y"),
            "supplier": self.supplier.pk,
            "outlet": self.outlet.pk,
            "note": "",
            "discount_type": "fixed",
            "discount": "0",
            "tax": "0",
            "purchased_stock-TOTAL_FORMS": 1,
            "purchased_stock-INITIAL_FORMS": 1,
            "purchased_stock-MIN_NUM_FORMS": 1,
            "purchased_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "purchased_stock-0-product": self.product.pk,
            "purchased_stock-0-quantity": 10,
            "purchased_stock-0-unit": self.unit.pk,
            "purchased_stock-0-discount_type": "percentage",
            "purchased_stock-0-discount": 5,
            "purchased_stock-0-cost": 100000,
            "purchased_stock-0-purchase_order": self.purchase_order.pk,
            "purchased_stock-0-id": self.purchase_order.purchased_stock.all()
            .first()
            .pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderUpdateView.as_view()(request, pk=self.purchase_order.pk)
        assert response.status_code == 302
        assert PurchaseOrder.objects.last().outlet == self.outlet
        assert PurchaseOrder.objects.last().supplier == self.supplier
        assert PurchaseOrder.objects.last().purchased_stock.all().count() == 1
        assert PurchaseOrder.objects.last().purchased_stock.all().first().quantity == 10

    def test_form_invalid(self):
        params = {
            "date_of_purchase": datetime.today().strftime("%d/%m/%Y"),
            "supplier": self.supplier.pk,
            "outlet": self.outlet.pk,
            "note": "",
            "purchased_stock-TOTAL_FORMS": 1,
            "purchased_stock-INITIAL_FORMS": 1,
            "purchased_stock-MIN_NUM_FORMS": 1,
            "purchased_stock-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "purchased_stock-0-product": self.product.pk,
            "purchased_stock-0-quantity": 100000,
            "purchased_stock-0-unit": self.unit.pk,
            "purchased_stock-0-cost": 100000,
            "purchased_stock-0-total_cost": 100000,
            "purchased_stock-0-purchase_order": self.purchase_order.pk,
            "purchased_stock-0-id": self.purchase_order.purchased_stock.all()
            .first()
            .pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderUpdateView.as_view()(request, pk=self.purchase_order.pk)
        assert response.status_code == 200


class TestPurchaseOrderCancelRemainingItemsView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

        self.purchase_order_2 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order_2.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order_2,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.stock, 3, self.unit)
        self.purchase_order_2.received()

        self.url = reverse(
            "stocks:stocks_purchase_order_cancel", kwargs={"pk": self.purchase_order.pk}
        )
        self.url_2 = reverse(
            "stocks:stocks_purchase_order_cancel",
            kwargs={"pk": self.purchase_order_2.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_delete_no_received_item(self):
        request = self.factory.post(self.url, data={})
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderCancelRemainingItemsView.as_view()(
            request, pk=self.purchase_order.pk
        )
        assert response.status_code == 302
        assert (
            PurchaseOrder.objects.get(pk=self.purchase_order.pk).status
            == PurchaseOrder.STATUS.closed
        )

    def test_delete_with_received_item(self):
        request = self.factory.post(self.url_2, data={})
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderCancelRemainingItemsView.as_view()(
            request, pk=self.purchase_order_2.pk
        )
        assert response.status_code == 302
        assert (
            PurchaseOrder.objects.get(pk=self.purchase_order_2.pk).status
            == PurchaseOrder.STATUS.received
        )


class PurchaseOrderReceiveWizardView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="Product-Y",
            sku="PY-001",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            uom=self.unit,
            name="Product-X",
            sku="PX-001",
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock_2.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

        self.purchase_order_2 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order_2.purchase_product_stock(
            self.product_2, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse(
            "stocks:stocks_purchase_order_receive",
            kwargs={"pk": self.purchase_order.pk},
        )
        self.url_2 = reverse(
            "stocks:stocks_purchase_order_receive",
            kwargs={"pk": self.purchase_order_2.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="1sampai6")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "purchase_order_receive_wizard_view-current_step": "purchase_order_receive_form",
            "purchase_order_receive_form-TOTAL_FORMS": 1,
            "purchase_order_receive_form-INITIAL_FORMS": 1,
            "purchase_order_receive_form-MIN_NUM_FORMS": 0,
            "purchase_order_receive_form-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "purchase_order_receive_form-0-product": "{} {}".format(
                self.product.sku, self.product.name
            ),
            "purchase_order_receive_form-0-total_qty": "{} {}".format(
                self.purchase_order.purchased_stock.all().first().quantity,
                self.unit.unit,
            ),
            "purchase_order_receive_form-0-quantity": self.purchase_order.purchased_stock.all()
            .first()
            .quantity,
            "purchase_order_receive_form-0-unit": self.unit.pk,
            "purchase_order_receive_form-0-received_date": datetime.today().strftime(
                "%d/%m/%Y %H:%m"
            ),
            "purchase_order_receive_form-0-product_id": self.product.pk,
            "purchase_order_receive_form-0-id": self.purchase_order.purchased_stock.all()
            .first()
            .pk,
            "purchase_order_receive_form-0-purchase_order": self.purchase_order.pk,
        }

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 302)

    def test_post_batch_tracked(self):
        params = (
            {
                "purchase_order_receive_wizard_view-current_step": "purchase_order_receive_form",
                "purchase_order_receive_form-TOTAL_FORMS": 1,
                "purchase_order_receive_form-INITIAL_FORMS": 1,
                "purchase_order_receive_form-MIN_NUM_FORMS": 0,
                "purchase_order_receive_form-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "purchase_order_receive_form-0-product": "{} {}".format(
                    self.product_2.sku, self.product_2.name
                ),
                "purchase_order_receive_form-0-total_qty": "{} {}".format(
                    self.purchase_order_2.purchased_stock.all().first().quantity,
                    self.unit.unit,
                ),
                "purchase_order_receive_form-0-quantity": self.purchase_order_2.purchased_stock.all()
                .first()
                .quantity,
                "purchase_order_receive_form-0-unit": self.unit.pk,
                "purchase_order_receive_form-0-received_date": datetime.today().strftime(
                    "%d/%m/%Y %H:%m"
                ),
                "purchase_order_receive_form-0-product_id": self.product_2.pk,
                "purchase_order_receive_form-0-id": self.purchase_order_2.purchased_stock.all()
                .first()
                .pk,
                "purchase_order_receive_form-0-purchase_order": self.purchase_order_2.pk,
            },
            {
                "purchase_order_receive_wizard_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": 1,
                "batch_tracked_form-INITIAL_FORMS": 0,
                "batch_tracked_form-MIN_NUM_FORMS": 0,
                "batch_tracked_form-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": 1,
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": 0,
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": 1,
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": 1000,
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": self.product_2.name,
                "batch_tracked_form-0-quantity_to_assign": " 0 {}".format(
                    self.unit.unit
                ),
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": self.purchase_order_2.purchased_stock.all()
                .first()
                .quantity,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": "new",
                "batch_tracked_form-0-batch_items-0-batch_quantity": "{} {}".format(
                    self.stock_2.balance, self.unit.unit
                ),
                "batch_tracked_form-0-batch_items-0-quantity": self.purchase_order_2.purchased_stock.all()
                .first()
                .quantity,
                "batch_tracked_form-0-batch_items-0-expiration_date": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="1sampai6")
        response = self.post(self.url_2, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url_2, data=params[1])
        self.assertEqual(response.status_code, 302)


class TestPurchaseOrderDetailCost(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="Product-Y",
            sku="PY-001",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

        self.url = reverse("stocks:stocks_purchase_order_detail_cost",)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "product": self.product.pk,
            "outlet": self.outlet.pk,
            "purchase_order": self.purchase_order.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderDetailCost.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestBatchTrackedListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.url = reverse("stocks:stocks_batch_track_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.catalogue_1 = mommy.make(
            "catalogue.catalogue",
            account=self.account,
            is_manage_stock=True,
            name="Ikan",
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            catalogue=self.catalogue_1,
            uom=self.unit,
            name="Ikan ikan",
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(20, unit=self.unit)

        self.batch_1 = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_1,
            batch_id="Batch-01",
            expiration_date=datetime.today(),
        )
        self.item_batch_1 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_1,
            outlet=self.outlet,
        )
        self.item_batch_1.set_batch_balance(20, self.unit)

        self.catalogue_2 = mommy.make(
            "catalogue.catalogue",
            account=self.account,
            is_manage_stock=True,
            name="Baso",
        )

        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            catalogue=self.catalogue_2,
            uom=self.unit,
            name="Baso baso",
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )

        self.stock_2.set_balance(20, unit=self.unit)

        self.batch_2 = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_2,
            batch_id="Batch-02",
            expiration_date=datetime.today(),
        )
        self.item_batch_2 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_2,
            outlet=self.outlet,
        )
        self.item_batch_2.set_batch_balance(20, self.unit)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_batch_track"))
        self.assertEqual(response.status_code, 200)

    def test_batch_tracked_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 6,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = BatchTrackedListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_filter_variant(self):
        total_column = 6
        data = {"draw": 5}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[2][data]": 2,
                "columns[2][name]": "",
                "columns[2][searchable]": True,
                "columns[2][orderable]": True,
                "columns[2][search][value]": "B",
                "columns[2][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = BatchTrackedListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_filter_product(self):
        total_column = 6
        data = {"draw": 5}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[1][data]": 1,
                "columns[1][name]": "",
                "columns[1][searchable]": True,
                "columns[1][orderable]": True,
                "columns[1][search][value]": "B",
                "columns[1][search][regex]": False,
            }
        )

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = BatchTrackedListJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
        }

        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )


class TestBatchTrackedDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(20, unit=self.unit)

        self.batch_1 = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_1,
            batch_id="Batch-01",
            expiration_date=datetime.today(),
        )
        self.item_batch_1 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_1,
            outlet=self.outlet,
        )
        self.item_batch_1.set_batch_balance(20, self.unit)
        self.url = reverse(
            "stocks:stocks_batch_track_detail", kwargs={"pk": self.batch_1.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestBatchTrackedOutletDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(20, unit=self.unit)

        self.batch_1 = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_1,
            batch_id="Batch-01",
            expiration_date=datetime.today(),
        )
        self.item_batch_1 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_1,
            outlet=self.outlet,
        )
        self.item_batch_1.set_batch_balance(20, self.unit)
        self.url = reverse(
            "stocks:stocks_batch_track_outlet_detail",
            kwargs={"pk": self.item_batch_1.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestBatchesQuantityConversionJsonView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.get(category="weight", unit__iexact="kg")
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(20, unit=self.unit)

        self.batch_1 = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_1,
            batch_id="Batch-01",
            expiration_date=datetime.today(),
        )
        self.item_batch_1 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_1,
            outlet=self.outlet,
        )
        self.item_batch_1.set_batch_balance(20, self.unit)
        self.url = reverse("stocks:get_quantity_to_assign")

        self.stock_adjustment = mommy.make(
            "stocks.StockAdjustment",
            account=self.account,
            outlet=self.outlet,
            status=StockAdjustment.STATUS.item_receive,
        )
        self.stock_adjustment.adjust_product_stock(self.product_1, self.unit, 10, 10000)
        self.stock_adjustment.process_adjustment(self.user)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "quantity_to_assign": self.stock_adjustment.adjusted_stock.all()
            .first()
            .quantity,
            "quantity": 500,
            "unit_to": self.account.uom_set.get(category="weight", unit__iexact="g").pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BatchesQuantityConversionJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_qty_0(self):
        params = {
            "quantity_to_assign": self.stock_adjustment.adjusted_stock.all()
            .first()
            .quantity,
            "quantity": "",
            "unit_to": self.account.uom_set.get(category="weight", unit__iexact="g").pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BatchesQuantityConversionJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_fail(self):
        params = {
            "quantity_to_assign": self.stock_adjustment.adjusted_stock.all()
            .first()
            .quantity,
            "quantity": 500,
            "unit_to": "",
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BatchesQuantityConversionJsonView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestQuantityBatchConversionJsonView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.get(category="weight", unit__iexact="kg")
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product_1, outlet=self.outlet, account=self.account
        )
        self.stock_1.set_balance(20, unit=self.unit)

        self.batch_1 = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_1,
            batch_id="Batch-01",
            expiration_date=datetime.today(),
        )
        self.item_batch_1 = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_1,
            outlet=self.outlet,
        )
        self.item_batch_1.set_batch_balance(20, self.unit)
        self.url = reverse("stocks:get_quantity_batch")

        self.stock_adjustment = mommy.make(
            "stocks.StockAdjustment",
            account=self.account,
            outlet=self.outlet,
            status=StockAdjustment.STATUS.item_receive,
        )
        self.stock_adjustment.adjust_product_stock(self.product_1, self.unit, 10, 10000)
        self.stock_adjustment.process_adjustment(self.user)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "unit_from": self.stock_adjustment.adjusted_stock.all().first().unit.pk,
            "unit_to": self.account.uom_set.get(category="weight", unit__iexact="g").pk,
            "quantity": 500,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = QuantityBatchConversionJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_qty_0(self):
        params = {
            "unit_from": self.stock_adjustment.adjusted_stock.all().first().unit.pk,
            "unit_to": self.account.uom_set.get(category="weight", unit__iexact="g").pk,
            "quantity": "",
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = QuantityBatchConversionJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_fail(self):
        params = {
            "unit_from": self.stock_adjustment.adjusted_stock.all().first().unit.pk,
            "unit_to": "",
            "quantity": 500,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = QuantityBatchConversionJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestMissingBatchTrackedView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.catalogue = mommy.make(
            "catalogue.Catalogue",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            name="X",
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Product-X",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
            transaction_date=datetime.today(),
            code="kasfl3s",
        )
        mommy.make(
            "sales.ProductOrder",
            sales_order=sales_order,
            product=self.product,
            quantity=5,
            account=self.account,
        )

        self.missing_batch = self.product.batches.all().first()
        self.item_batch = self.missing_batch.batch_items.get(outlet=self.outlet)

        self.url = reverse(
            "stocks:stocks_missing_batch",
            kwargs={"batch_pk": self.missing_batch.pk, "outlet_pk": self.outlet.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "form-TOTAL_FORMS": 1,
            "form-INITIAL_FORMS": 0,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-batch_items-TOTAL_FORMS": 1,
            "form-0-batch_items-INITIAL_FORMS": 0,
            "form-0-batch_items-MIN_NUM_FORMS": 0,
            "form-0-batch_items-MAX_NUM_FORMS": 1000,
            "form-0-product": self.product.pk,
            "form-0-product_name": self.product.name,
            "form-0-quantity_to_assign": "{} {}".format(0, self.unit.unit),
            "form-0-unit": self.unit.pk,
            "form-0-total_quantity": -5,
            "form-0-id": [""],
            "form-0-batch_items-0-batch_id": "New Batch",
            "form-0-batch_items-0-batch_quantity": "{} {}".format(
                self.item_batch.balance, self.unit.unit
            ),
            "form-0-batch_items-0-quantity": -5,
            "form-0-batch_items-0-unit": self.unit.pk,
            "form-0-batch_items-0-expiration_date": datetime.today().strftime(
                "%d/%m/%Y"
            ),
            "form-0-batch_items-0-id": [""],
            "form-0-batch_items-0-batch": [""],
        }
        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 302)
        new_batch = self.product.batches.all().first()
        self.assertEqual(new_batch.batch_id, "New Batch")
        self.assertEqual(new_batch.batch_items.get(outlet=self.outlet).balance, -5)


class TestSupplierItemJSONList(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        account = mommy.make("accounts.Account", name="account-1")
        inventorysetting, _ = account.inventorysetting_set.get_or_create(
            account=account
        )
        inventorysetting.supplier_master_data = True
        inventorysetting.save()

        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)
        set_current_tenant(account)
        self.unit = account.uom_set.first()

        self.product_1 = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )
        self.product_2 = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )

        self.supplier_1 = mommy.make(Supplier, name="test-supplier-1", account=account)
        self.supplier_1.create_supplier_detail(self.product_1, 10, self.unit, 10000)

        self.supplier_2 = mommy.make(Supplier, name="test-supplier-2", account=account)
        self.supplier_2.create_supplier_detail(self.product_2, 10, self.unit, 10000)

        self.url = reverse("stocks:stocks_supplier_item_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_supplier_item_list_json(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 6,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = SupplierItemListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestSupplierItemCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        inventorysetting, _ = account.inventorysetting_set.get_or_create(
            account=account
        )
        inventorysetting.supplier_master_data = True
        inventorysetting.save()

        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)
        self.unit = account.uom_set.first()

        self.product = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )

        set_current_tenant(account)

        self.url = reverse("stocks:stocks_supplier_item_create")

    def tearDown(self):
        set_current_tenant(None)

    def test_get(self):
        request = self.factory.get(self.url)
        request.user = self.user
        response = SupplierItemCreateView.as_view()(request)

        assert response.status_code == 200

    def test_post(self):
        params = {
            "name": ["supplier-123"],
            "code": [""],
            "email": [""],
            "contact": [""],
            "phone": [""],
            "address": [""],
            "supplier_detail-TOTAL_FORMS": ["1"],
            "supplier_detail-INITIAL_FORMS": ["0"],
            "supplier_detail-MIN_NUM_FORMS": ["1"],
            "supplier_detail-MAX_NUM_FORMS": [MAX_NUM_FORMS],
            "supplier_detail-0-product": [self.product.pk],
            "supplier_detail-0-quantity": ["10"],
            "supplier_detail-0-unit": [self.product.uom.pk],
            "supplier_detail-0-cost": ["1500"],
            "supplier_detail-0-supplier": [""],
            "supplier_detail-0-id": [""],
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = SupplierItemCreateView.as_view()(request)

        assert response.status_code == 302
        assert Supplier.objects.last().supplier_detail.all().count() == 1
        assert Supplier.objects.last().code == "SUPPLIER-{}".format(
            Supplier.all_objects.count()
        )


class TestSupplierItemUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        inventorysetting, _ = account.inventorysetting_set.get_or_create(
            account=account
        )
        inventorysetting.supplier_master_data = True
        inventorysetting.save()
        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)
        set_current_tenant(account)

        self.unit = account.uom_set.first()
        self.product = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )

        self.product_add = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )

        self.supplier = mommy.make(Supplier, name="test", account=account)
        self.supplier.create_supplier_detail(self.product, 10, self.unit, 10000)
        self.url = reverse(
            "stocks:stocks_supplier_item_update", kwargs={"pk": self.supplier.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_get(self):
        request = self.factory.get(self.url)
        request.user = self.user
        response = SupplierItemUpdateView.as_view()(request, pk=self.supplier.pk)

        assert response.status_code == 200

    def test_post(self):
        params = {
            "name": [self.supplier.name],
            "code": ["SUP-123"],
            "email": [""],
            "contact": [""],
            "phone": [""],
            "address": [""],
            "supplier_detail-TOTAL_FORMS": ["2"],
            "supplier_detail-INITIAL_FORMS": ["1"],
            "supplier_detail-MIN_NUM_FORMS": ["1"],
            "supplier_detail-MAX_NUM_FORMS": [MAX_NUM_FORMS],
            "supplier_detail-0-product": [
                self.supplier.supplier_detail.first().product.pk
            ],
            "supplier_detail-0-quantity": [
                self.supplier.supplier_detail.first().quantity
            ],
            "supplier_detail-0-unit": [self.supplier.supplier_detail.first().unit.pk],
            "supplier_detail-0-cost": [
                self.supplier.supplier_detail.first().cost.amount
            ],
            "supplier_detail-0-supplier": [
                self.supplier.supplier_detail.first().supplier.pk
            ],
            "supplier_detail-0-id": [self.supplier.supplier_detail.first().pk],
            "supplier_detail-1-product": [self.product_add.pk],
            "supplier_detail-1-quantity": [10],
            "supplier_detail-1-unit": [self.product_add.uom.pk],
            "supplier_detail-1-cost": [1500],
            "supplier_detail-1-supplier": [""],
            "supplier_detail-1-id": [""],
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = SupplierItemUpdateView.as_view()(request, pk=self.supplier.pk)

        assert response.status_code == 302
        assert Supplier.objects.last().supplier_detail.all().count() == 2


class TestSupplierItemDeleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make("accounts.Account", name="account-1")
        inventorysetting, _ = account.inventorysetting_set.get_or_create(
            account=account
        )
        inventorysetting.supplier_master_data = True
        inventorysetting.save()
        self.outlet = mommy.make(
            "outlet.Outlet", account=account, name="A", branch_id="A"
        )

        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)
        self.unit = account.uom_set.first()
        self.product = mommy.make(
            "catalogue.product", account=account, is_manage_stock=True, uom=self.unit,
        )

        self.supplier = mommy.make(Supplier, name="test", account=account)
        self.supplier.create_supplier_detail(self.product, 10, self.unit, 10000)
        set_current_tenant(account)

        self.url = reverse(
            "stocks:stocks_supplier_item_delete", kwargs={"pk": self.supplier.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_delete(self):
        request = self.factory.post(self.url)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)
        response = SupplierItemDeleteView.as_view()(request, pk=self.supplier.pk)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            messages._queued_messages[0].message,
            "{} {} was successfully {}!".format(
                Supplier._meta.verbose_name.title(), self.supplier, _("deleted"),
            ),
        )


class TestPurchaseOrderReceiveListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received_1 = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received_1.receive_purchase(self.product, 7, self.unit)
        self.received_2 = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="98765",
        )
        self.received_2.receive_purchase(self.product, 3, self.unit)
        self.url = reverse(
            "stocks:stocks_purchase_order_received_data",
            kwargs={"purchase_pk": self.purchase_order.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(
            reverse(
                "stocks:stocks_purchase_order_received_data",
                kwargs={"purchase_pk": self.purchase_order.pk},
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_purchase_order_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 4,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = PurchaseOrderReceivedListJson.as_view()(
            request, purchase_pk=self.purchase_order.pk
        )

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestPurchaseOrderReceiveCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse(
            "stocks:stocks_purchase_order_received_create",
            kwargs={"purchase_pk": self.purchase_order.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_received_all(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "1",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 10,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveCreateView.as_view()(
            request, purchase_pk=self.purchase_order.pk
        )
        assert response.status_code == 302
        assert PurchaseOrderReceive.objects.last().purchase_order == self.purchase_order
        assert (
            PurchaseOrderReceive.objects.last().purchase_order.status
            == PurchaseOrder.STATUS.received
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 1

    def test_post_received_partial(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "1",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 4,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveCreateView.as_view()(
            request, purchase_pk=self.purchase_order.pk
        )
        assert response.status_code == 302
        assert PurchaseOrderReceive.objects.last().purchase_order == self.purchase_order
        assert (
            PurchaseOrderReceive.objects.last().purchase_order.status
            == PurchaseOrder.STATUS.partial
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 1

    def test_post_fail_form(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "receipt_number": "",
            "received_detail-TOTAL_FORMS": "1",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 4,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveCreateView.as_view()(
            request, purchase_pk=self.purchase_order.pk
        )
        assert response.status_code == 200

    def test_post_fail_formset(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "1",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 200,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveCreateView.as_view()(
            request, purchase_pk=self.purchase_order.pk
        )
        assert response.status_code == 200


class TestPurchaseOrderProductDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse("stocks:get_detail_product",)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "purchase_order": self.purchase_order.pk,
            "stock": self.stock.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = PurchaseOrderProductDetailView.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestPurchaseOrderProductDetailJsonView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

        self.purchase_order_2 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            status=PurchaseOrder.STATUS.closed,
        )
        self.purchase_order_2.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse("stocks:get_purchased_order")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "purchase_order": self.purchase_order.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = PurchaseOrderProductDetailJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response._container[0])
        assert data["status"] == "success"
        assert len(data["result"]["purchase_orders"]) == 1

    def test_get_ajax_purchased_order_closed(self):
        params = {
            "purchase_order": self.purchase_order_2.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = PurchaseOrderProductDetailJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response._container[0])
        assert data["status"] == "success"
        assert len(data["result"]["purchase_orders"]) == 0


class TestReceivedDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.product, 10, self.unit)
        self.url = reverse(
            "stocks:stocks_received_detail",
            kwargs={"purchase_pk": self.purchase_order.pk, "pk": self.received.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestPurchaseOrderReceiveUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.stock, 10, self.unit)
        self.url = reverse(
            "stocks:stocks_received_update",
            kwargs={"purchase_pk": self.purchase_order.pk, "pk": self.received.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_post(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "receipt_number": "98765",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveUpdateView.as_view()(
            request, purchase_pk=self.purchase_order.pk, pk=self.received.pk
        )
        assert response.status_code == 302
        self.assertEqual(
            self.received.code,
            "RO-{}-{}".format(self.purchase_order.code, self.received.receipt_number),
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 1

    def test_post_fail(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveUpdateView.as_view()(
            request, purchase_pk=self.purchase_order.pk, pk=self.received.pk
        )
        assert response.status_code == 200


class TestReceivedOrderListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received_1 = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received_1.receive_purchase(self.product, 7, self.unit)
        self.received_2 = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="98765",
        )
        self.received_2.receive_purchase(self.product, 3, self.unit)
        self.url = reverse("stocks:stocks_received_order_data",)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_received_order",))
        self.assertEqual(response.status_code, 200)

    def test_received_order_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 5,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = ReceivedOrderListJson.as_view()(request)

        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestPurchaseOrderAutocompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse("stocks:get_purchase_order")

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": "{}".format(self.purchase_order.__str__())}
        params = {"q": "PO-001"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = PurchaseOrderAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        uoms = response_data["results"]
        self.assertEqual(uoms[0]["text"], expected_result["autocomplete"])


class TestReceivedOrderCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.purchase_order.purchase_product_stock(
            self.product_2, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse("stocks:stocks_received_order_create",)

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_received_all(self):
        params = {
            "purchase_order": self.purchase_order.pk,
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "2",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 10,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
            "received_detail-1-quantity": 10,
            "received_detail-1-unit": self.unit.pk,
            "received_detail-1-stock": self.stock_2.pk,
            "received_detail-1-received": "",
            "received_detail-1-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ReceivedOrderCreateView.as_view()(request)
        assert response.status_code == 302
        assert PurchaseOrderReceive.objects.last().purchase_order == self.purchase_order
        assert (
            PurchaseOrderReceive.objects.last().purchase_order.status
            == PurchaseOrder.STATUS.received
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 2

    def test_post_received_partial(self):
        params = {
            "purchase_order": self.purchase_order.pk,
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "1",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 4,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ReceivedOrderCreateView.as_view()(request)
        assert response.status_code == 302
        assert PurchaseOrderReceive.objects.last().purchase_order == self.purchase_order
        assert (
            PurchaseOrderReceive.objects.last().purchase_order.status
            == PurchaseOrder.STATUS.partial
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 1

    def test_post_received_partial_case_2(self):
        params = {
            "purchase_order": self.purchase_order.pk,
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "2",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 10,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
            "received_detail-1-quantity": 5,
            "received_detail-1-unit": self.unit.pk,
            "received_detail-1-stock": self.stock_2.pk,
            "received_detail-1-received": "",
            "received_detail-1-id": "",
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ReceivedOrderCreateView.as_view()(request)
        assert response.status_code == 302
        assert PurchaseOrderReceive.objects.last().purchase_order == self.purchase_order
        assert (
            PurchaseOrderReceive.objects.last().purchase_order.status
            == PurchaseOrder.STATUS.partial
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 2

    def test_post_received_partial_case_3(self):
        params = {
            "purchase_order": self.purchase_order.pk,
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "2",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 5,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
            "received_detail-1-quantity": 5,
            "received_detail-1-unit": self.unit.pk,
            "received_detail-1-stock": self.stock_2.pk,
            "received_detail-1-received": "",
            "received_detail-1-id": "",
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ReceivedOrderCreateView.as_view()(request)
        assert response.status_code == 302
        assert PurchaseOrderReceive.objects.last().purchase_order == self.purchase_order
        assert (
            PurchaseOrderReceive.objects.last().purchase_order.status
            == PurchaseOrder.STATUS.partial
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 2

    def test_post_fail_form(self):
        params = {
            "purchase_order": "",
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "",
            "received_detail-TOTAL_FORMS": "1",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 4,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ReceivedOrderCreateView.as_view()(request)
        assert response.status_code == 200

    def test_post_fail_formset(self):
        params = {
            "purchase_order": self.purchase_order.pk,
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "12345",
            "received_detail-TOTAL_FORMS": "1",
            "received_detail-INITIAL_FORMS": "0",
            "received_detail-MIN_NUM_FORMS": "1",
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 200,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ReceivedOrderCreateView.as_view()(request)
        assert response.status_code == 200


class TestReceivedOrderUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.stock, 10, self.unit)
        self.url = reverse(
            "stocks:stocks_received_order_update", kwargs={"pk": self.received.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_post(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "receipt_number": "98765",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveUpdateView.as_view()(
            request, purchase_pk=self.purchase_order.pk, pk=self.received.pk
        )
        assert response.status_code == 302
        self.assertEqual(
            self.received.code,
            "RO-{}-{}".format(self.purchase_order.code, self.received.receipt_number),
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 1

    def test_post_fail(self):
        params = {
            "received_date": datetime.today().strftime("%d/%m/%Y"),
            "receipt_number": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = PurchaseOrderReceiveUpdateView.as_view()(
            request, purchase_pk=self.purchase_order.pk, pk=self.received.pk
        )
        assert response.status_code == 200


class TestReceivedOrderCreateViewWizard(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            is_batch_tracked=True,
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.purchase_order.purchase_product_stock(
            self.product_2, 10, self.unit, 1000, "percentage", 5
        )
        self.url = reverse("stocks:stocks_received_order_create_wizard")

    def tearDown(self):
        set_current_tenant(None)

    def test_post_no_batch_tracked(self):
        params = {
            "received_order_create_view_wizard-current_step": "received_order_form",
            "received_order_form-purchase_order": self.purchase_order.pk,
            "received_order_form-received_date": datetime.today().strftime(
                "%d/%m/%Y %H:%m"
            ),
            "received_order_form-receipt_number": "12345",
            "received_detail-TOTAL_FORMS": 1,
            "received_detail-INITIAL_FORMS": 0,
            "received_detail-MIN_NUM_FORMS": 1,
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 10,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 302)

        assert PurchaseOrderReceive.objects.last().purchase_order == self.purchase_order
        assert (
            PurchaseOrderReceive.objects.last().purchase_order.status
            == PurchaseOrder.STATUS.partial
        )
        assert PurchaseOrderReceive.objects.last().received_detail.all().count() == 1

    def test_post_form_fail_no_batch_tracked(self):
        params = {
            "received_order_create_view_wizard-current_step": "received_order_form",
            "received_order_form-purchase_order": self.purchase_order.pk,
            "received_order_form-received_date": datetime.today().strftime(
                "%d/%m/%Y %H:%m"
            ),
            "received_order_form-receipt_number": "",
            "received_detail-TOTAL_FORMS": 1,
            "received_detail-INITIAL_FORMS": 0,
            "received_detail-MIN_NUM_FORMS": 1,
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": 10,
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 200)

    def test_post_formset_fail_no_batch_tracked(self):
        params = {
            "received_order_create_view_wizard-current_step": "received_order_form",
            "received_order_form-purchase_order": self.purchase_order.pk,
            "received_order_form-received_date": datetime.today().strftime(
                "%d/%m/%Y %H:%m"
            ),
            "received_order_form-receipt_number": "12345",
            "received_detail-TOTAL_FORMS": 1,
            "received_detail-INITIAL_FORMS": 0,
            "received_detail-MIN_NUM_FORMS": 1,
            "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "received_detail-0-quantity": "",
            "received_detail-0-unit": self.unit.pk,
            "received_detail-0-stock": self.stock.pk,
            "received_detail-0-received": "",
            "received_detail-0-id": "",
        }

        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)
        self.assertEqual(response.status_code, 200)

    def test_post_has_batch_trakced(self):
        params = (
            {
                "received_order_create_view_wizard-current_step": "received_order_form",
                "received_order_form-purchase_order": self.purchase_order.pk,
                "received_order_form-received_date": datetime.today().strftime(
                    "%d/%m/%Y %H:%m"
                ),
                "received_order_form-receipt_number": "12412",
                "received_detail-TOTAL_FORMS": 1,
                "received_detail-INITIAL_FORMS": 0,
                "received_detail-MIN_NUM_FORMS": 1,
                "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "received_detail-0-quantity": 10,
                "received_detail-0-unit": self.unit.pk,
                "received_detail-0-stock": self.stock_2.pk,
                "received_detail-0-received": "",
                "received_detail-0-id": "",
            },
            {
                "received_order_create_view_wizard-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": ["1"],
                "batch_tracked_form-INITIAL_FORMS": ["0"],
                "batch_tracked_form-MIN_NUM_FORMS": ["0"],
                "batch_tracked_form-MAX_NUM_FORMS": ["1000"],
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": ["1"],
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": ["0"],
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": ["1"],
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": ["1000"],
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": self.product_2.name,
                "batch_tracked_form-0-quantity_to_assign": f"0 {self.unit.unit}",
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 10,
                "batch_tracked_form-0-id": [""],
                "batch_tracked_form-0-batch_items-0-batch_id": "BATCH",
                "batch_tracked_form-0-batch_items-0-batch_quantity": "0",
                "batch_tracked_form-0-batch_items-0-quantity": 10,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-expiration_date": ["08/08/2020"],
                "batch_tracked_form-0-batch_items-0-id": [""],
                "batch_tracked_form-0-batch_items-0-batch": [""],
            },
        )

        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 302)

    def test_post_batch_tracked_fail_quantity_less_than_assign_qty(self):
        params = (
            {
                "received_order_create_view_wizard-current_step": "received_order_form",
                "received_order_form-purchase_order": self.purchase_order.pk,
                "received_order_form-received_date": datetime.today().strftime(
                    "%d/%m/%Y %H:%m"
                ),
                "received_order_form-receipt_number": "12412",
                "received_detail-TOTAL_FORMS": 1,
                "received_detail-INITIAL_FORMS": 0,
                "received_detail-MIN_NUM_FORMS": 1,
                "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "received_detail-0-quantity": 10,
                "received_detail-0-unit": self.unit.pk,
                "received_detail-0-stock": self.stock_2.pk,
                "received_detail-0-received": "",
                "received_detail-0-id": "",
            },
            {
                "received_order_create_view_wizard-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": ["1"],
                "batch_tracked_form-INITIAL_FORMS": ["0"],
                "batch_tracked_form-MIN_NUM_FORMS": ["0"],
                "batch_tracked_form-MAX_NUM_FORMS": ["1000"],
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": ["1"],
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": ["0"],
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": ["1"],
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": ["1000"],
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": self.product_2.name,
                "batch_tracked_form-0-quantity_to_assign": f"0 {self.unit.unit}",
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 10,
                "batch_tracked_form-0-id": [""],
                "batch_tracked_form-0-batch_items-0-batch_id": "BATCH",
                "batch_tracked_form-0-batch_items-0-batch_quantity": "0",
                "batch_tracked_form-0-batch_items-0-quantity": 4,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-expiration_date": ["08/08/2020"],
                "batch_tracked_form-0-batch_items-0-id": [""],
                "batch_tracked_form-0-batch_items-0-batch": [""],
            },
        )

        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 200)

    def test_post_batch_tracked_fail_quantity_more_than_qty_assign(self):
        params = (
            {
                "received_order_create_view_wizard-current_step": "received_order_form",
                "received_order_form-purchase_order": self.purchase_order.pk,
                "received_order_form-received_date": datetime.today().strftime(
                    "%d/%m/%Y %H:%m"
                ),
                "received_order_form-receipt_number": "12412",
                "received_detail-TOTAL_FORMS": 1,
                "received_detail-INITIAL_FORMS": 0,
                "received_detail-MIN_NUM_FORMS": 1,
                "received_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
                "received_detail-0-quantity": 10,
                "received_detail-0-unit": self.unit.pk,
                "received_detail-0-stock": self.stock_2.pk,
                "received_detail-0-received": "",
                "received_detail-0-id": "",
            },
            {
                "received_order_create_view_wizard-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": ["1"],
                "batch_tracked_form-INITIAL_FORMS": ["0"],
                "batch_tracked_form-MIN_NUM_FORMS": ["0"],
                "batch_tracked_form-MAX_NUM_FORMS": ["1000"],
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": ["1"],
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": ["0"],
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": ["1"],
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": ["1000"],
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": self.product_2.name,
                "batch_tracked_form-0-quantity_to_assign": f"0 {self.unit.unit}",
                "batch_tracked_form-0-unit": self.unit.pk,
                "batch_tracked_form-0-total_quantity": 10,
                "batch_tracked_form-0-id": [""],
                "batch_tracked_form-0-batch_items-0-batch_id": "BATCH",
                "batch_tracked_form-0-batch_items-0-batch_quantity": "0",
                "batch_tracked_form-0-batch_items-0-quantity": 15,
                "batch_tracked_form-0-batch_items-0-unit": self.unit.pk,
                "batch_tracked_form-0-batch_items-0-expiration_date": ["08/08/2020"],
                "batch_tracked_form-0-batch_items-0-id": [""],
                "batch_tracked_form-0-batch_items-0-batch": [""],
            },
        )

        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url, data=params[1])
        self.assertEqual(response.status_code, 200)


class TestVendorBillListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received_1 = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received_1.receive_purchase(self.product, 3, self.unit)
        self.received_2 = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="234124",
        )
        self.received_2.receive_purchase(self.product, 7, self.unit)
        self.vendor_bill_1 = mommy.make(
            "stocks.VendorBill", account=self.account, received_order=self.received_1
        )
        self.vendor_bill_1.create_vendor_bill_detail(self.product, 1000)
        self.vendor_bill_2 = mommy.make(
            "stocks.VendorBill", account=self.account, received_order=self.received_2
        )
        self.vendor_bill_2.create_vendor_bill_detail(self.product, 1000)

        self.url = reverse("stocks:stocks_vendor_bill_data")

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_list_view(self):
        self.login(email=self.user.email, password="test")
        response = self.get(reverse("stocks:stocks_vendor_bill"))
        self.assertEqual(response.status_code, 200)

    def test_vendor_bill_list(self):
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 6,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = VendorBillListJson.as_view()(request)
        context = json.loads(response.content)
        self.assertEqual(response.status_code, 200)

        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestVendorBillCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.stock, 3, self.unit)
        self.url = reverse("stocks:stocks_vendor_bill_create")

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "received_order": self.received.pk,
            "bill_date": datetime.today().strftime("%d/%m/%Y"),
            "due_date": datetime.today().strftime("%d/%m/%Y"),
            "vendor_bill_detail-TOTAL_FORMS": 1,
            "vendor_bill_detail-INITIAL_FORMS": 0,
            "vendor_bill_detail-MIN_NUM_FORMS": 0,
            "vendor_bill_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "vendor_bill_detail-0-purchase_cost": 1500,
            "vendor_bill_detail-0-product": self.product.pk,
            "vendor_bill_detail-0-vendor_bill": "",
            "vendor_bill_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = VendorBillCreateView.as_view()(request)
        assert response.status_code == 302
        assert VendorBill.objects.last().vendor_bill_detail.count() == 1

    def test_post_form_fail(self):
        params = {
            "received_order": "",
            "bill_date": datetime.today().strftime("%d/%m/%Y"),
            "due_date": datetime.today().strftime("%d/%m/%Y"),
            "vendor_bill_detail-TOTAL_FORMS": 1,
            "vendor_bill_detail-INITIAL_FORMS": 0,
            "vendor_bill_detail-MIN_NUM_FORMS": 0,
            "vendor_bill_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "vendor_bill_detail-0-purchase_cost": 1500,
            "vendor_bill_detail-0-product": self.product.pk,
            "vendor_bill_detail-0-vendor_bill": "",
            "vendor_bill_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = VendorBillCreateView.as_view()(request)
        assert response.status_code == 200

    def test_post_formset_fail(self):
        params = {
            "received_order": "",
            "bill_date": datetime.today().strftime("%d/%m/%Y"),
            "due_date": datetime.today().strftime("%d/%m/%Y"),
            "vendor_bill_detail-TOTAL_FORMS": 1,
            "vendor_bill_detail-INITIAL_FORMS": 0,
            "vendor_bill_detail-MIN_NUM_FORMS": 0,
            "vendor_bill_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "vendor_bill_detail-0-purchase_cost": "",
            "vendor_bill_detail-0-product": self.product.pk,
            "vendor_bill_detail-0-vendor_bill": "",
            "vendor_bill_detail-0-id": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = VendorBillCreateView.as_view()(request)
        assert response.status_code == 200


class TestVendorBillDetailViewJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.product, 3, self.unit)
        self.url = reverse("stocks:stocks_vendor_bill_get_details")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "received_order": self.received.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        response = VendorBillDetailViewJson.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestReceivedOrderAutocompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.product, 3, self.unit)
        self.url = reverse("stocks:get_received_order")

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": "{}".format(self.received.code)}
        params = {"q": "12345"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = ReceivedOrderAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        uoms = response_data["results"]
        self.assertEqual(uoms[0]["text"], expected_result["autocomplete"])


class TestVendorBillDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.product, 10, self.unit)
        self.vendor_bill = mommy.make(
            "stocks.VendorBill", account=self.account, received_order=self.received
        )
        self.vendor_bill.create_vendor_bill_detail(self.product, 1000)

        self.url = reverse(
            "stocks:stocks_vendor_bill_detail", kwargs={"pk": self.vendor_bill.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestVendorBillUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        self.received = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received.receive_purchase(self.stock, 10, self.unit)
        self.vendor_bill = mommy.make(
            "stocks.VendorBill", account=self.account, received_order=self.received
        )
        self.vendor_bill.create_vendor_bill_detail(self.product, 1000)

        self.url = reverse(
            "stocks:stocks_vendor_bill_update", kwargs={"pk": self.vendor_bill.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_post(self):
        params = {
            "received_order": self.received.pk,
            "bill_date": datetime.today().strftime("%d/%m/%Y"),
            "due_date": datetime.today().strftime("%d/%m/%Y"),
            "vendor_bill_detail-TOTAL_FORMS": 1,
            "vendor_bill_detail-INITIAL_FORMS": 1,
            "vendor_bill_detail-MIN_NUM_FORMS": 0,
            "vendor_bill_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "vendor_bill_detail-0-purchase_cost": 1500,
            "vendor_bill_detail-0-product": self.product.pk,
            "vendor_bill_detail-0-vendor_bill": self.vendor_bill.pk,
            "vendor_bill_detail-0-id": self.vendor_bill.vendor_bill_detail.first().pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = VendorBillUpdateView.as_view()(request, pk=self.vendor_bill.pk)
        assert response.status_code == 302
        assert VendorBill.objects.last().vendor_bill_detail.count() == 1

    def test_post_form_fail(self):
        params = {
            "received_order": self.received.pk,
            "bill_date": datetime.today().strftime("%d/%m/%Y"),
            "due_date": "",
            "vendor_bill_detail-TOTAL_FORMS": 1,
            "vendor_bill_detail-INITIAL_FORMS": 1,
            "vendor_bill_detail-MIN_NUM_FORMS": 0,
            "vendor_bill_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "vendor_bill_detail-0-purchase_cost": 1500,
            "vendor_bill_detail-0-product": self.product.pk,
            "vendor_bill_detail-0-vendor_bill": self.vendor_bill.pk,
            "vendor_bill_detail-0-id": self.vendor_bill.vendor_bill_detail.first().pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = VendorBillUpdateView.as_view()(request, pk=self.vendor_bill.pk)
        assert response.status_code == 200

    def test_post_formset_fail(self):
        params = {
            "received_order": self.received.pk,
            "bill_date": datetime.today().strftime("%d/%m/%Y"),
            "due_date": "",
            "vendor_bill_detail-TOTAL_FORMS": 1,
            "vendor_bill_detail-INITIAL_FORMS": 1,
            "vendor_bill_detail-MIN_NUM_FORMS": 0,
            "vendor_bill_detail-MAX_NUM_FORMS": MAX_NUM_FORMS,
            "vendor_bill_detail-0-purchase_cost": "",
            "vendor_bill_detail-0-product": self.product.pk,
            "vendor_bill_detail-0-vendor_bill": self.vendor_bill.pk,
            "vendor_bill_detail-0-id": self.vendor_bill.vendor_bill_detail.first().pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = VendorBillUpdateView.as_view()(request, pk=self.vendor_bill.pk)
        assert response.status_code == 200


class TestStockTransferReceivedView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.source = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.destination = mommy.make(
            "outlet.Outlet", name="B", branch_id="B", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.source_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.source, account=self.account
        )
        self.source_stock.set_balance(100, unit=self.unit)
        self.destination_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.destination, account=self.account
        )
        self.destination_stock.set_balance(20, unit=self.unit)

        self.stock_transfer = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer.transfer_product_stock(self.product, self.unit, 10)

        self.url = reverse(
            "stocks:stocks_transfers_received", kwargs={"pk": self.stock_transfer.pk}
        )

        self.product_2 = mommy.make(
            "catalogue.product",
            name="Product-X",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            uom=self.unit,
        )
        self.source_stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.source, account=self.account
        )
        self.source_stock_2.set_balance(100, unit=self.unit)
        self.destination_stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.destination, account=self.account
        )
        self.destination_stock_2.set_balance(20, unit=self.unit)

        self.stock_transfer_2 = mommy.make(
            "stocks.StockTransfer",
            account=self.account,
            source=self.source,
            destination=self.destination,
        )
        self.stock_transfer_2.transfer_product_stock(self.product_2, self.unit, 10)

        self.batch = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_2,
            batch_id="Batch-01",
        )
        self.source_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.source,
        )
        self.destination_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.destination,
        )
        self.batch_other = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_2,
            batch_id="Batch-02",
        )
        self.source_batch_other = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch_other,
            outlet=self.source,
        )
        self.source_batch.set_batch_balance(80, self.unit)
        self.source_batch_other.set_batch_balance(20, self.unit)
        self.destination_batch.set_batch_balance(20, self.unit)

        self.url_2 = reverse(
            "stocks:stocks_transfers_received", kwargs={"pk": self.stock_transfer_2.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "stock_transfer_received_view-current_step": "stock_transfer_received_form",
            "stock_transfer_received_form-date_of_arrival": datetime.today().strftime(
                "%d/%m/%Y"
            ),
        }
        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 302)
        assert self.destination_stock.balance == 30

    def test_post_has_batch_tracked(self):
        params = (
            {
                "stock_transfer_received_view-current_step": "stock_transfer_received_form",
                "stock_transfer_received_form-date_of_arrival": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
            },
            {
                "stock_transfer_received_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": "1",
                "batch_tracked_form-INITIAL_FORMS": "0",
                "batch_tracked_form-MIN_NUM_FORMS": "0",
                "batch_tracked_form-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": "1",
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": "0",
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": "1",
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": f"{self.product_2.name}",
                "batch_tracked_form-0-quantity_to_assign": f"0 {self.product_2.uom.unit}",
                "batch_tracked_form-0-unit": self.product_2.uom.pk,
                "batch_tracked_form-0-total_quantity": "10",
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": f"{self.source_batch.balance} {self.product_2.uom.unit}",
                "batch_tracked_form-0-batch_items-0-quantity": "10",
                "batch_tracked_form-0-batch_items-0-unit": self.product_2.uom.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="test")
        response = self.post(self.url_2, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url_2, data=params[1])
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.destination_batch.balance, 30)

    def test_batch_destination_doesnt_exist(self):
        params = (
            {
                "stock_transfer_received_view-current_step": "stock_transfer_received_form",
                "stock_transfer_received_form-date_of_arrival": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
            },
            {
                "stock_transfer_received_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": "1",
                "batch_tracked_form-INITIAL_FORMS": "0",
                "batch_tracked_form-MIN_NUM_FORMS": "0",
                "batch_tracked_form-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": "1",
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": "0",
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": "1",
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": f"{self.product_2.name}",
                "batch_tracked_form-0-quantity_to_assign": f"0 {self.product_2.uom.unit}",
                "batch_tracked_form-0-unit": self.product_2.uom.pk,
                "batch_tracked_form-0-total_quantity": "10",
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch_other.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": "0",
                "batch_tracked_form-0-batch_items-0-quantity": "10",
                "batch_tracked_form-0-batch_items-0-unit": self.product_2.uom.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )
        self.login(email=self.user.email, password="test")
        response = self.post(self.url_2, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url_2, data=params[1])
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.source_batch_other.balance, 10)


class TestStockCountCompletedView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit)

        self.stock_count = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            status="in_progress",
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count.count_product_stock(self.product)
        for counted in self.stock_count.counted_stock.all():
            counted.counted = 50
            counted.unit = self.unit
            counted.save()

        self.url = reverse(
            "stocks:stocks_count_completed", kwargs={"pk": self.stock_count.pk}
        )

        self.product_2 = mommy.make(
            "catalogue.product",
            name="Product-X",
            account=self.account,
            is_manage_stock=True,
            is_batch_tracked=True,
            uom=self.unit,
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet, account=self.account
        )
        self.stock_2.set_balance(100, unit=self.unit)

        self.stock_count_2 = mommy.make(
            "stocks.StockCount",
            account=self.account,
            outlet=self.outlet,
            date_of_stock_count=datetime.today().strftime("%Y-%m-%d"),
        )
        self.stock_count_2.count_product_stock(self.product_2)

        for counted in self.stock_count_2.counted_stock.all():
            counted.counted = 50
            counted.unit = self.unit
            counted.save()

        self.batch = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product_2,
            batch_id="Batch-01",
        )
        self.item_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.outlet,
        )

        self.item_batch.set_batch_balance(100, self.unit)

        self.url_2 = reverse(
            "stocks:stocks_count_completed", kwargs={"pk": self.stock_count_2.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "stock_count_completed_view-current_step": "stock_count_complete_form",
            "stock_count_complete_form-date_of_completion": datetime.today().strftime(
                "%d/%m/%Y"
            ),
        }
        self.login(email=self.user.email, password="test")
        response = self.post(self.url, data=params)

        self.assertEqual(response.status_code, 302)
        assert self.stock.balance == 50

    def test_post_has_batch_tracked(self):
        params = (
            {
                "stock_count_completed_view-current_step": "stock_count_complete_form",
                "stock_count_complete_form-date_of_completion": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
            },
            {
                "stock_count_completed_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": "1",
                "batch_tracked_form-INITIAL_FORMS": "0",
                "batch_tracked_form-MIN_NUM_FORMS": "0",
                "batch_tracked_form-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": "1",
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": "0",
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": "1",
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": f"{self.product_2.name}",
                "batch_tracked_form-0-quantity_to_assign": f"0 {self.product_2.uom.unit}",
                "batch_tracked_form-0-unit": self.product_2.uom.pk,
                "batch_tracked_form-0-total_quantity": 50,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": self.batch.batch_id,
                "batch_tracked_form-0-batch_items-0-batch_quantity": f"{self.item_batch.balance} {self.product_2.uom.unit}",
                "batch_tracked_form-0-batch_items-0-quantity": 50,
                "batch_tracked_form-0-batch_items-0-unit": self.product_2.uom.pk,
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="test")
        response = self.post(self.url_2, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url_2, data=params[1])
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.item_batch.balance, 50)

    def test_post_new_batch_tracked(self):
        params = (
            {
                "stock_count_completed_view-current_step": "stock_count_complete_form",
                "stock_count_complete_form-date_of_completion": datetime.today().strftime(
                    "%d/%m/%Y"
                ),
            },
            {
                "stock_count_completed_view-current_step": "batch_tracked_form",
                "batch_tracked_form-TOTAL_FORMS": "1",
                "batch_tracked_form-INITIAL_FORMS": "0",
                "batch_tracked_form-MIN_NUM_FORMS": "0",
                "batch_tracked_form-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-batch_items-TOTAL_FORMS": "1",
                "batch_tracked_form-0-batch_items-INITIAL_FORMS": "0",
                "batch_tracked_form-0-batch_items-MIN_NUM_FORMS": "1",
                "batch_tracked_form-0-batch_items-MAX_NUM_FORMS": "1000",
                "batch_tracked_form-0-product": self.product_2.pk,
                "batch_tracked_form-0-product_name": f"{self.product_2.name}",
                "batch_tracked_form-0-quantity_to_assign": f"0 {self.product_2.uom.unit}",
                "batch_tracked_form-0-unit": self.product_2.uom.pk,
                "batch_tracked_form-0-total_quantity": 50,
                "batch_tracked_form-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch_id": "NEW BATCH",
                "batch_tracked_form-0-batch_items-0-batch_quantity": "0",
                "batch_tracked_form-0-batch_items-0-quantity": 50,
                "batch_tracked_form-0-batch_items-0-unit": self.product_2.uom.pk,
                'batch_tracked_form-0-batch_items-0-expiration_date': datetime.today().strftime(
                    "%d/%m/%Y"
                ),
                "batch_tracked_form-0-batch_items-0-id": "",
                "batch_tracked_form-0-batch_items-0-batch": "",
            },
        )

        self.login(email=self.user.email, password="test")
        response = self.post(self.url_2, data=params[0])
        self.assertEqual(response.status_code, 200)

        response = self.post(self.url_2, data=params[1])
        self.assertEqual(response.status_code, 302)
        assert len(Batch.objects.filter(product=self.product_2)) == 1
