from django.test import RequestFactory

from test_plus.test import TestCase
from model_mommy import mommy
from decimal import Decimal
from django.urls import reverse
from datetime import datetime, timedelta

from sterlingpos.catalogue.models import Product, UOM
from sterlingpos.stocks.models import TRANSACTION_TYPE
from sterlingpos.core.models import set_current_tenant
from sterlingpos.stocks.views import ProductStockUpdateView


class TestSubstructStockSalesOrder(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()

        self.accountowner = mommy.make(
            "accounts.AccountOwner", account=self.account, user=self.user
        )

        self.outlet = mommy.make(
            "outlet.Outlet", account=self.account, name="A", branch_id="A"
        )

        self.uom = UOM.objects.get(
            account=self.account, unit="pcs", category="units", uom_type="base"
        )

        self.product_1 = mommy.make(
            "catalogue.Product",
            price=10000,
            is_manage_stock=True,
            account=self.account,
            uom=self.uom,
        )
        self.stock_1 = self.product_1.stocks.get(outlet=self.outlet)

        self.product_2 = mommy.make(
            "catalogue.Product",
            price=5000,
            is_manage_stock=True,
            account=self.account,
            uom=self.uom,
        )
        self.stock_2 = self.product_2.stocks.get(outlet=self.outlet)

        self.product_composite = mommy.make(
            "catalogue.Product",
            account=self.account,
            parent=None,
            classification=Product.PRODUCT_CLASSIFICATION.composite,
        )
        self.composite_material_1 = mommy.make(
            "catalogue.CompositeProduct",
            account=self.account,
            product=self.product_1,
            product_quantity=1,
            composite_product=self.product_composite,
        )
        self.composite_material_2 = mommy.make(
            "catalogue.CompositeProduct",
            account=self.account,
            product=self.product_2,
            product_quantity=2,
            composite_product=self.product_composite,
        )

        self.add_on_1 = mommy.make(
            "catalogue.Product",
            classification=Product.PRODUCT_CLASSIFICATION.complementary,
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
            parent=None,
        )
        self.add_on_stock_1 = self.add_on_1.stocks.get(outlet=self.outlet)

        self.manufactured_product = mommy.make(
            "catalogue.Product",
            price=5000,
            is_manage_stock=True,
            account=self.account,
            uom=self.uom,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
        )
        self.manufactured_stock = self.manufactured_product.stocks.get(
            outlet=self.outlet
        )

        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(
            self.manufactured_product, through_defaults={"account": self.account}
        )
        self.bom_item_1 = mommy.make(
            "productions.BOMItemDetail",
            account=self.account,
            bom=self.bom,
            product=self.product_1,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )

        self.bom_item_2 = mommy.make(
            "productions.BOMItemDetail",
            account=self.account,
            bom=self.bom,
            product=self.product_2,
            quantity=2,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )

        self.manufactured_composite = mommy.make(
            "catalogue.Product",
            account=self.account,
            parent=None,
            classification=Product.PRODUCT_CLASSIFICATION.composite,
        )
        self.manufactured_composite_material_1 = mommy.make(
            "catalogue.CompositeProduct",
            account=self.account,
            product=self.manufactured_product,
            product_quantity=1,
            composite_product=self.manufactured_composite,
        )
        self.manufactured_composite_material_2 = mommy.make(
            "catalogue.CompositeProduct",
            account=self.account,
            product=self.product_1,
            product_quantity=2,
            composite_product=self.manufactured_composite,
        )

        mommy.make(
            "catalogue.ProductAddOn",
            product=self.add_on_1,
            product_usage=self.product_1,
            account=self.account,
        )

        mommy.make(
            "catalogue.ProductAddOn",
            product=self.add_on_1,
            product_usage=self.product_2,
            account=self.account,
        )
        self.factory = RequestFactory()
        self.stock_update = "stocks:stocks_update"
        self.request_header = {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}

    def tearDown(self):
        set_current_tenant(None)

    def test_update_stock_sales_order(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(10)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)
        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.product_1,
            quantity=2,
        )

        self.assertEqual(product_order.sales_order.is_settled, True)
        self.assertEqual(
            self.stock_1.balance, params["quantity"] - product_order.quantity
        )

    def test_update_stock_sales_order_composite_product(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(10)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_2.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_2.pk)
        self.assertEqual(response.status_code, 200)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.product_composite,
            quantity=1,
        )
        composite_detail_1 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            account=self.account,
            product_order=product_order,
            product_quantity=1,
            product=self.product_1,
        )
        composite_detail_2 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            account=self.account,
            product_order=product_order,
            product_quantity=2,
            product=self.product_2,
        )

        self.assertEqual(
            self.stock_1.balance,
            params["quantity"] - composite_detail_1.product_quantity,
        )
        self.assertEqual(
            self.stock_2.balance,
            params["quantity"] - composite_detail_2.product_quantity,
        )

    def test_update_stock_add_on_detail(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(10)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        url = reverse(
            "stocks:stocks_update", kwargs={"stock_pk": self.add_on_stock_1.pk}
        )
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(
            request, stock_pk=self.add_on_stock_1.pk
        )
        self.assertEqual(response.status_code, 200)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )

        product_order = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.product_1,
            quantity=1,
        )

        product_add_on = mommy.make(
            "sales.ProductOrderProductAddOnDetail",
            account=self.account,
            product_order=product_order,
            quantity=2,
            product=self.add_on_1,
        )

        self.assertEqual(
            self.add_on_stock_1.balance, params["quantity"] - product_add_on.quantity
        )

    def test_substraction_sales_order(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(10)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_2.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_2.pk)
        self.assertEqual(response.status_code, 200)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order_1 = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.product_composite,
            quantity=2,
        )
        composite_detail_1 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            account=self.account,
            product_order=product_order_1,
            product_quantity=1,
            product=self.product_1,
        )
        composite_detail_2 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            account=self.account,
            product_order=product_order_1,
            product_quantity=2,
            product=self.product_2,
        )

        product_order_2 = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.product_1,
            quantity=2,
        )

        qty_product_1 = (
            product_order_1.quantity * composite_detail_1.product_quantity
        ) + product_order_2.quantity
        qty_product_2 = product_order_1.quantity * composite_detail_2.product_quantity
        self.assertEqual(self.stock_1.balance, params["quantity"] - qty_product_1)
        self.assertEqual(self.stock_2.balance, params["quantity"] - qty_product_2)

    def test_update_stock_sales_order_manufactured_product(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(20)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_2.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_2.pk)
        self.assertEqual(response.status_code, 200)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.manufactured_product,
            quantity=2,
        )

        self.assertEqual(
            self.stock_1.balance,
            params["quantity"] - (product_order.quantity * self.bom_item_1.quantity),
        )
        self.assertEqual(
            self.stock_2.balance,
            params["quantity"] - (product_order.quantity * self.bom_item_2.quantity),
        )
        self.assertEqual(self.manufactured_stock.balance, 0)

    def test_update_stock_sales_order_composite_manufactured_product(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(20)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_2.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_2.pk)
        self.assertEqual(response.status_code, 200)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.manufactured_composite,
            quantity=1,
        )
        composite_detail_1 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            account=self.account,
            product_order=product_order,
            product_quantity=1,
            product=self.manufactured_product,
        )
        composite_detail_2 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            account=self.account,
            product_order=product_order,
            product_quantity=2,
            product=self.product_1,
        )

        self.assertEqual(
            self.stock_1.balance,
            params["quantity"]
            - (
                (composite_detail_1.product_quantity * self.bom_item_1.quantity)
                + composite_detail_2.product_quantity
            ),
        )
        self.assertEqual(
            self.stock_2.balance,
            params["quantity"]
            - (composite_detail_1.product_quantity * self.bom_item_2.quantity),
        )
        self.assertEqual(self.manufactured_stock.balance, 0)


class TestBatchTrackedUpdateSalesOrder(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        self.account = mommy.make("accounts.Account", name="account-1")
        self.outlet = mommy.make(
            "outlet.Outlet", account=self.account, name="A", branch_id="A"
        )

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()

        self.accountowner = mommy.make(
            "accounts.AccountOwner", account=self.account, user=self.user
        )

        self.stock_update = "stocks:stocks_update"
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

        self.uom = UOM.objects.get(
            account=self.account, unit="pcs", category="units", uom_type="base"
        )

        self.product_1 = mommy.make(
            "catalogue.Product",
            price=10000,
            is_manage_stock=True,
            account=self.account,
            is_batch_tracked=True,
            uom=self.uom,
        )
        self.stock_1 = self.product_1.stocks.get(outlet=self.outlet)

        self.product_2 = mommy.make(
            "catalogue.Product",
            price=5000,
            is_manage_stock=True,
            account=self.account,
            is_batch_tracked=True,
            uom=self.uom,
        )
        self.stock_2 = self.product_2.stocks.get(outlet=self.outlet)

        self.product_composite = mommy.make(
            "catalogue.Product",
            account=self.account,
            parent=None,
            classification=Product.PRODUCT_CLASSIFICATION.composite,
        )
        self.composite_material_1 = mommy.make(
            "catalogue.CompositeProduct",
            account=self.account,
            product=self.product_1,
            product_quantity=1,
            composite_product=self.product_composite,
        )
        self.composite_material_2 = mommy.make(
            "catalogue.CompositeProduct",
            account=self.account,
            product=self.product_2,
            product_quantity=2,
            composite_product=self.product_composite,
        )

        self.add_on_1 = mommy.make(
            "catalogue.Product",
            classification=Product.PRODUCT_CLASSIFICATION.complementary,
            is_manage_stock=True,
            is_batch_tracked=True,
            account=self.account,
            parent=None,
            uom=self.uom,
        )
        self.add_on_stock_1 = self.add_on_1.stocks.get(outlet=self.outlet)

        mommy.make(
            "catalogue.ProductAddOn",
            product=self.add_on_1,
            product_usage=self.product_1,
            account=self.account,
        )

        mommy.make(
            "catalogue.ProductAddOn",
            account=self.account,
            product=self.add_on_1,
            product_usage=self.product_2,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_update_stock_batch_tracked_sales_order(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(10)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        batch_1 = mommy.make(
            "stocks.Batch",
            batch_id="Batch01",
            expiration_date=datetime.today() + timedelta(days=2),
            product=self.product_1,
            account=self.account,
        )
        batch_1_item = mommy.make("stocks.BatchItem", batch=batch_1, outlet=self.outlet)
        batch_2 = mommy.make(
            "stocks.Batch",
            batch_id="Batch02",
            expiration_date=datetime.today() + timedelta(days=7),
            product=self.product_1,
            account=self.account,
        )
        batch_2_item = mommy.make(
            "stocks.BatchItem", batch=batch_2, outlet=self.outlet, account=self.account
        )

        batch_1.batch_items.get(outlet=self.outlet).add_batch_balance(
            7,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )
        batch_2.batch_items.get(outlet=self.outlet).add_batch_balance(
            3,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )

        self.assertEqual(batch_1_item.balance, 7)
        self.assertEqual(batch_2_item.balance, 3)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            sales_order=sales_order,
            product=self.product_1,
            quantity=5,
            account=self.account,
        )

        self.assertEqual(product_order.sales_order.is_settled, True)
        self.assertEqual(
            self.stock_1.balance, params["quantity"] - product_order.quantity
        )
        total_batch = batch_1.balance + batch_2.balance
        self.assertEqual(self.stock_1.balance, total_batch)

    def test_update_stock_batch_tracked_sales_order_composite(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(10)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        product_1_batch_1 = mommy.make(
            "stocks.Batch",
            batch_id="Batch01composite",
            expiration_date=datetime.today() + timedelta(days=2),
            product=self.product_1,
            account=self.account,
        )
        product_1_batch_1_item = mommy.make(
            "stocks.BatchItem",
            batch=product_1_batch_1,
            outlet=self.outlet,
            account=self.account,
        )
        product_1_batch_2 = mommy.make(
            "stocks.Batch",
            batch_id="Batch02composite",
            expiration_date=datetime.today() + timedelta(days=7),
            product=self.product_1,
            account=self.account,
        )
        product_1_batch_2_item = mommy.make(
            "stocks.BatchItem",
            batch=product_1_batch_2,
            outlet=self.outlet,
            account=self.account,
        )

        product_1_batch_1.batch_items.get(outlet=self.outlet).add_batch_balance(
            7,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )
        product_1_batch_2.batch_items.get(outlet=self.outlet).add_batch_balance(
            3,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )

        self.assertEqual(product_1_batch_1_item.balance, 7)
        self.assertEqual(product_1_batch_2_item.balance, 3)

        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_2.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_2.pk)
        self.assertEqual(response.status_code, 200)

        product_2_batch_1 = mommy.make(
            "stocks.Batch",
            batch_id="Batch021composite",
            expiration_date=datetime.today() + timedelta(days=2),
            product=self.product_2,
            account=self.account,
        )
        product_2_batch_1_item = mommy.make(
            "stocks.BatchItem",
            batch=product_2_batch_1,
            outlet=self.outlet,
            account=self.account,
        )
        product_2_batch_2 = mommy.make(
            "stocks.Batch",
            batch_id="Batch022composite",
            expiration_date=datetime.today() + timedelta(days=7),
            product=self.product_2,
            account=self.account,
        )
        product_2_batch_2_item = mommy.make(
            "stocks.BatchItem",
            batch=product_2_batch_2,
            outlet=self.outlet,
            account=self.account,
        )

        product_2_batch_1.batch_items.get(outlet=self.outlet).add_batch_balance(
            5,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )
        product_2_batch_2.batch_items.get(outlet=self.outlet).add_batch_balance(
            5,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )

        self.assertEqual(product_2_batch_1_item.balance, 5)
        self.assertEqual(product_2_batch_2_item.balance, 5)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order_1 = mommy.make(
            "sales.ProductOrder",
            sales_order=sales_order,
            product=self.product_composite,
            quantity=2,
            account=self.account,
        )
        composite_detail_1 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            product_order=product_order_1,
            product_quantity=1,
            product=self.product_1,
            account=self.account,
        )
        composite_detail_2 = mommy.make(
            "sales.ProductOrderCompositeDetail",
            product_order=product_order_1,
            product_quantity=2,
            product=self.product_2,
            account=self.account,
        )

        product_order_2 = mommy.make(
            "sales.ProductOrder",
            sales_order=sales_order,
            product=self.product_1,
            quantity=2,
            account=self.account,
        )

        qty_product_1 = (
            product_order_1.quantity * composite_detail_1.product_quantity
        ) + product_order_2.quantity
        qty_product_2 = product_order_1.quantity * composite_detail_2.product_quantity
        self.assertEqual(self.stock_1.balance, params["quantity"] - qty_product_1)
        self.assertEqual(self.stock_2.balance, params["quantity"] - qty_product_2)

        total_batch_product_1 = product_1_batch_1.balance + product_1_batch_2.balance
        self.assertEqual(self.stock_1.balance, total_batch_product_1)

        total_batch_product_2 = product_2_batch_1.balance + product_2_batch_2.balance
        self.assertEqual(self.stock_2.balance, total_batch_product_2)

    def test_update_stock_batch_tracked_sales_order_add_on(self):
        params = {"action": "add_balance", "unit": self.uom.pk, "quantity": Decimal(10)}
        url = reverse("stocks:stocks_update", kwargs={"stock_pk": self.stock_1.pk})
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(request, stock_pk=self.stock_1.pk)
        self.assertEqual(response.status_code, 200)

        url = reverse(
            "stocks:stocks_update", kwargs={"stock_pk": self.add_on_stock_1.pk}
        )
        request = self.factory.post(
            url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        response = ProductStockUpdateView.as_view()(
            request, stock_pk=self.add_on_stock_1.pk
        )
        self.assertEqual(response.status_code, 200)

        batch_1 = mommy.make(
            "stocks.Batch",
            batch_id="Batch01",
            expiration_date=datetime.today() + timedelta(days=2),
            product=self.product_1,
            account=self.account,
        )
        batch_1_item = mommy.make(
            "stocks.BatchItem", batch=batch_1, outlet=self.outlet, account=self.account
        )

        batch_2 = mommy.make(
            "stocks.Batch",
            batch_id="Batch02",
            expiration_date=datetime.today() + timedelta(days=7),
            product=self.product_1,
            account=self.account,
        )
        batch_2_item = mommy.make(
            "stocks.BatchItem", batch=batch_2, outlet=self.outlet, account=self.account
        )

        batch_1.batch_items.get(outlet=self.outlet).add_batch_balance(
            7,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )
        batch_2.batch_items.get(outlet=self.outlet).add_batch_balance(
            3,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )

        self.assertEqual(batch_1_item.balance, 7)
        self.assertEqual(batch_2_item.balance, 3)

        add_on_batch_1 = mommy.make(
            "stocks.Batch",
            batch_id="Batch01",
            expiration_date=datetime.today() + timedelta(days=2),
            product=self.add_on_1,
            account=self.account,
        )
        add_on_batch_1_item = mommy.make(
            "stocks.BatchItem",
            batch=add_on_batch_1,
            outlet=self.outlet,
            account=self.account,
        )

        add_on_batch_2 = mommy.make(
            "stocks.Batch",
            batch_id="Batch02",
            expiration_date=datetime.today() + timedelta(days=7),
            product=self.add_on_1,
            account=self.account,
        )
        add_on_batch_2_item = mommy.make(
            "stocks.BatchItem",
            batch=add_on_batch_2,
            outlet=self.outlet,
            account=self.account,
        )

        add_on_batch_1.batch_items.get(outlet=self.outlet).add_batch_balance(
            4,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )
        add_on_batch_2.batch_items.get(outlet=self.outlet).add_batch_balance(
            6,
            unit=self.uom,
            source_object=None,
            transaction_type=TRANSACTION_TYPE.manual_substract,
        )

        self.assertEqual(add_on_batch_1_item.balance, 4)
        self.assertEqual(add_on_batch_2_item.balance, 6)

        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            sales_order=sales_order,
            product=self.product_1,
            quantity=1,
            account=self.account,
        )

        product_add_on = mommy.make(
            "sales.ProductOrderProductAddOnDetail",
            product_order=product_order,
            quantity=2,
            product=self.add_on_1,
            account=self.account,
        )

        self.assertEqual(product_order.sales_order.is_settled, True)
        self.assertEqual(
            self.add_on_stock_1.balance, params["quantity"] - product_add_on.quantity
        )
        total_batch = batch_1.balance + batch_2.balance
        self.assertEqual(self.stock_1.balance, total_batch)
        total_add_on_batch = add_on_batch_1.balance + add_on_batch_2.balance
        self.assertEqual(self.add_on_stock_1.balance, total_add_on_batch)
