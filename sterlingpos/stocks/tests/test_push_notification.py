from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.push_notification.models import RegisteredFCMDevice

from sterlingpos.stocks.models import StockTransaction
from sterlingpos.stocks.push_notification import StockTransactionPushNotificationHandler


class TestStockTransactionPushNotificationHandler(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.uom = self.account.uom_set.get(unit="pcs")
        self.outlet = mommy.make(
            "outlet.Outlet",
            name="outlet 1",
            branch_id="outlet-1",
            account=self.account,
        )
        self.outlet_2 = mommy.make(
            "outlet.Outlet",
            name="outlet 2",
            branch_id="outlet-2",
            account=self.account,
        )
        self.product = mommy.make(
            "catalogue.Product",
            sku="PRODUCT-TEST",
            cost=500,
            price=1000,
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
        )
        self.registered_fcm = RegisteredFCMDevice.objects.create(
            name="test-device",
            registered_id="registered_id",
            account=self.account,
            outlet=self.outlet,
        )
        self.registered_fcm_2 = RegisteredFCMDevice.objects.create(
            name="test-device 2",
            registered_id="registered_id",
            account=self.account,
            outlet=self.outlet_2,
        )
        self.stock_transaction = StockTransaction.objects.create(
            stock=self.product.stocks.get(outlet=self.outlet),
            quantity=5,
            transaction_type="transfer",
            reference="{}",
            unit=self.uom,
            account=self.account,
        )

    def test_filtered_get_registration_ids(self):
        handler = StockTransactionPushNotificationHandler(
            instance=self.stock_transaction
        )
        registered_ids = handler.get_registration_ids()
        self.assertTrue(registered_ids.filter(pk=self.registered_fcm.pk).exists())
        self.assertFalse(registered_ids.filter(pk=self.registered_fcm_2.pk).exists())

    def test_get_data_message(self):
        handler = StockTransactionPushNotificationHandler(
            instance=self.stock_transaction
        )
        data_message = handler.get_data_message("UPDATE")

        self.assertEqual(data_message["mode"], "UPDATE_STOCK")
        self.assertEqual(data_message["title"], "STOCK UPDATE")
        self.assertEqual(data_message["message"], "Please sync your data.....")
        self.assertEqual(data_message["object_id"], self.product.pk)
        self.assertEqual(data_message["outlet_id"], self.outlet.pk)

    def test_get_mode_name(self):
        handler = StockTransactionPushNotificationHandler(
            instance=self.stock_transaction
        )

        mode_name = handler.get_mode_name("UPDATE")
        self.assertEqual(mode_name, "UPDATE_STOCK")

    def test_is_valid(self):
        handler = StockTransactionPushNotificationHandler(
            instance=self.stock_transaction
        )
        self.assertTrue(handler.is_valid)

    def test_is_not_valid_for_transaction(self):
        stock_transaction = StockTransaction.objects.create(
            stock=self.product.stocks.get(outlet=self.outlet),
            quantity=5,
            transaction_type="transaction",
            reference="{}",
            unit=self.uom,
            account=self.account,
        )
        handler = StockTransactionPushNotificationHandler(instance=stock_transaction)
        self.assertFalse(handler.is_valid)

    def test_is_not_valid_for_raw_product_or_not_for_sales(self):
        raw_product = mommy.make(
            "catalogue.Product",
            sku="PRODUCT-RAW",
            cost=500,
            price=1000,
            is_manage_stock=True,
            is_sellable=False,
            uom=self.uom,
            account=self.account,
        )
        stock_transaction = StockTransaction.objects.create(
            stock=raw_product.stocks.get(outlet=self.outlet),
            quantity=5,
            transaction_type="transfer",
            reference="{}",
            unit=self.uom,
            account=self.account,
        )
        handler = StockTransactionPushNotificationHandler(instance=stock_transaction)
        self.assertFalse(handler.is_valid)
