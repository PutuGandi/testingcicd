from datetime import datetime

from test_plus.test import TestCase
from model_mommy import mommy
from datetime import datetime

from sterlingpos.core.models import set_current_tenant
from sterlingpos.stocks.models import (
    Stock,
    StockTransaction,
    StockTransfer,
    PurchaseOrder,
    StockAdjustment,
    StockCount,
    Supplier,
    PurchaseOrderReceive,
    VendorBill,
)


class TestStock(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="outlet-1", account=self.account
        )

    def test__str__(self):
        stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.assertEqual(
            stock.__str__(),
            "{} - {} [{}]".format(
                self.product.sku, self.product.name, self.outlet.name
            ),
        )

    def test_balance(self):
        stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.assertEqual(stock.balance, 0)

    def test_add_balance(self):
        stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        stock.add_balance(100, unit=self.unit)
        self.assertEqual(stock.balance, 100)

    def test_subtract_balance(self):
        stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        stock.add_balance(100, unit=self.unit)
        stock.subtract_balance(50, unit=self.unit)

        self.assertEqual(stock.balance, 50)

    def test_add_balance_with_user(self):
        user = mommy.make("users.User", email="test@email.com", account=self.account)
        stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        stock.add_balance(100, unit=self.unit, user=user)
        self.assertEqual(stock.balance, 100)
        self.assertTrue(stock.transactions.last().user, user)

    def test_subtract_balance_via_sales_order(self):
        stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        stock.add_balance(100, unit=self.unit)
        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            account=self.account,
            outlet=self.outlet,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            quantity=50,
            product=self.product,
            sales_order=sales_order,
            account=self.account,
        )

        self.assertEqual(stock.balance, 50)

    def test_create_stock_for_new_outlet(self):
        outlet = mommy.make("outlet.Outlet", branch_id="outlet-2", account=self.account)
        self.assertTrue(
            Stock.objects.filter(outlet=outlet, product=self.product).exists()
        )


class TestStockTransfer(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")

        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )
        self.outlet_2 = mommy.make(
            "outlet.Outlet", name="outlet 2", branch_id="outlet-2", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_2, account=self.account
        )

    def test__str__(self):
        stock_transfer = StockTransfer.objects.create(
            code="TS001",
            source=self.outlet_1,
            destination=self.outlet_2,
            date_of_transfer=datetime.today(),
            account=self.account,
        )

        self.assertEqual(stock_transfer.__str__(), "TS001 outlet 1 - outlet 2 [active]")

    def test_transfer_product_stock(self):
        self.stock_1.set_balance(100, unit=self.unit)
        stock_transfer = StockTransfer.objects.create(
            code="TS001",
            source=self.outlet_1,
            destination=self.outlet_2,
            date_of_transfer=datetime.today(),
            account=self.account,
        )
        stock_transfer.transfer_product_stock(
            self.product, self.unit, 10,
        )
        self.assertEqual(stock_transfer.transfered_stock.count(), 1)

    def test_transfer_product_out_of_stock(self):
        stock_transfer = StockTransfer.objects.create(
            code="TS001",
            source=self.outlet_1,
            destination=self.outlet_2,
            date_of_transfer=datetime.today(),
            account=self.account,
        )
        stock_transfer.transfer_product_stock(
            self.product, self.unit, 10,
        )
        self.assertEqual(stock_transfer.transfered_stock.count(), 0)

    def test_received_stock_transfer(self):
        self.stock_1.set_balance(100, unit=self.unit)

        stock_transfer = StockTransfer.objects.create(
            code="TS001",
            source=self.outlet_1,
            destination=self.outlet_2,
            date_of_transfer=datetime.today(),
            account=self.account,
        )
        stock_transfer.transfer_product_stock(
            self.product, self.unit, 10,
        )
        stock_transfer.received()

        self.assertEqual(stock_transfer.status, "received")
        self.assertEqual(self.stock_2.balance, 10)
        self.assertEqual(self.stock_1.balance, 90)


class TestStockCount(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

    def test__str__(self):
        stock_count = StockCount.objects.create(
            code="SC001", outlet=self.outlet_1, note="Notes", account=self.account,
        )

        self.assertEqual(stock_count.__str__(), "SC001 outlet 1 [pending] Notes")

    def test_count_product_stock(self):
        self.stock_1.set_balance(100, unit=self.unit)
        stock_count = StockCount.objects.create(
            code="SC001",
            outlet=self.outlet_1,
            note="Notes",
            account=self.account,
            date_of_stock_count=datetime.today(),
        )
        stock_count.count_product_stock(self.product,)
        self.assertEqual(stock_count.counted_stock.count(), 1)

    def test_process_adjustment(self):
        self.stock_1.set_balance(100, unit=self.unit)

        stock_count = StockCount.objects.create(
            code="SC001",
            outlet=self.outlet_1,
            note="Notes",
            account=self.account,
            date_of_stock_count=datetime.today(),
        )
        counted_stock = stock_count.count_product_stock(self.product,)

        counted_stock.counted = 90
        counted_stock.save()

        stock_count.process_adjustment()

        self.assertEqual(stock_count.status, "completed")
        self.assertEqual(stock_count.date_of_completion, datetime.now().date())
        self.assertEqual(self.stock_1.balance, 90)


class TestStockAdjustment(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

        self.outlet_1 = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock_1, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet_1, account=self.account
        )

    def test__str__(self):
        stock_adjustment = StockAdjustment.objects.create(
            code="SA001",
            outlet=self.outlet_1,
            note="Notes",
            status=StockAdjustment.STATUS.item_receive,
            date_of_adjustment=datetime.today(),
            account=self.account,
        )

        self.assertEqual(
            stock_adjustment.__str__(), "SA001 outlet 1 [item_receive] Notes"
        )

    def test_count_product_stock(self):
        self.stock_1.set_balance(100, unit=self.unit)
        stock_adjustment = StockAdjustment.objects.create(
            code="SA001",
            outlet=self.outlet_1,
            note="Notes",
            status=StockAdjustment.STATUS.item_receive,
            date_of_adjustment=datetime.today(),
            account=self.account,
        )
        stock_adjustment.adjust_product_stock(self.product, self.unit, 10, 10000)
        self.assertEqual(stock_adjustment.adjusted_stock.count(), 1)

    def test_process_adjustment_item_receive(self):
        self.stock_1.set_balance(100, unit=self.unit)

        stock_adjustment = StockAdjustment.objects.create(
            code="SA001",
            outlet=self.outlet_1,
            note="Item Received",
            status=StockAdjustment.STATUS.item_receive,
            date_of_adjustment=datetime.today(),
            account=self.account,
        )
        adjusted_stock = stock_adjustment.adjust_product_stock(
            self.product, self.unit, 10, 10000
        )
        stock_adjustment.process_adjustment()

        self.assertTrue(stock_adjustment.is_processed)
        self.assertEqual(self.stock_1.balance, 110)

    def test_process_adjustment_stock_count(self):
        self.stock_1.set_balance(100, unit=self.unit)

        stock_adjustment = StockAdjustment.objects.create(
            code="SA001",
            outlet=self.outlet_1,
            note="Item Recount",
            status=StockAdjustment.STATUS.stock_count,
            date_of_adjustment=datetime.today(),
            account=self.account,
        )
        adjusted_stock = stock_adjustment.adjust_product_stock(
            self.product, self.unit, 10, 0
        )
        stock_adjustment.process_adjustment()

        self.assertTrue(stock_adjustment.is_processed)
        self.assertEqual(self.stock_1.balance, 10)

    def test_process_adjustment_item_loss(self):
        self.stock_1.set_balance(100, unit=self.unit)

        stock_adjustment = StockAdjustment.objects.create(
            code="SA001",
            outlet=self.outlet_1,
            note="Item Loss",
            status=StockAdjustment.STATUS.item_loss,
            date_of_adjustment=datetime.today(),
            account=self.account,
        )
        adjusted_stock = stock_adjustment.adjust_product_stock(
            self.product, self.unit, 10, 0
        )
        stock_adjustment.process_adjustment()

        self.assertTrue(stock_adjustment.is_processed)
        self.assertEqual(self.stock_1.balance, 90)

    def test_process_adjustment_item_damaged(self):
        self.stock_1.set_balance(100, unit=self.unit)

        stock_adjustment = StockAdjustment.objects.create(
            code="SA001",
            outlet=self.outlet_1,
            note="Item Damaged",
            status=StockAdjustment.STATUS.item_damaged,
            date_of_adjustment=datetime.today(),
            account=self.account,
        )
        adjusted_stock = stock_adjustment.adjust_product_stock(
            self.product, self.unit, 10, 0
        )
        stock_adjustment.process_adjustment()

        self.assertTrue(stock_adjustment.is_processed)
        self.assertEqual(self.stock_1.balance, 90)


class TestSupplier(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

    def test__str__(self):
        supplier, _ = Supplier.objects.get_or_create(
            name="Supplier-01", account=self.account
        )

        self.assertEqual(supplier.__str__(), "Supplier-01")
        self.assertEqual(
            supplier.code, "SUPPLIER-{}".format(Supplier.all_objects.count())
        )

    def test_supplier_detail(self):
        supplier = Supplier.objects.create(name="Supplier-Test", account=self.account)
        supplier.create_supplier_detail(self.product, 10, self.unit, 5000)
        self.assertEqual(supplier.supplier_detail.count(), 1)


class TestPurchaseOrder(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

        self.outlet = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", name="Supplier", account=self.account
        )

    def test__str__(self):
        purchase_order = PurchaseOrder.objects.create(
            supplier=self.supplier,
            outlet=self.outlet,
            account=self.account,
            date_of_purchase=datetime.today(),
            shipping_date=datetime.today(),
            tax=0,
            discount_type=PurchaseOrder.DISCOUNT_TYPE.fixed,
            discount=0,
        )
        purchase_order.purchased_stock.create(
            stock=self.stock,
            quantity=1,
            unit=self.unit,
            purchase_cost=1000,
            discount_type="fixed",
            discount=0,
            account=self.account,
        )

        self.assertEqual(purchase_order.purchased_stock.count(), 1)
        self.assertEqual(
            purchase_order.__str__(),
            f"{purchase_order.code} {purchase_order.outlet.name} [{purchase_order.get_status_display()}]",
        )


class TestPurchaseOrderReceive(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

        self.outlet = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.supplier = mommy.make(
            "stocks.Supplier", name="Supplier", account=self.account
        )

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            supplier=self.supplier,
            outlet=self.outlet,
            account=self.account,
            date_of_purchase=datetime.today(),
            shipping_date=datetime.today(),
            code="PO-001",
            tax=0,
            discount_type=PurchaseOrder.DISCOUNT_TYPE.fixed,
            discount=0,
        )

        self.purchase_order.purchased_stock.create(
            stock=self.stock,
            quantity=1,
            unit=self.unit,
            purchase_cost=1000,
            discount_type="fixed",
            discount=0,
            account=self.account,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        received = PurchaseOrderReceive.objects.create(
            purchase_order=self.purchase_order,
            received_date=datetime.today(),
            receipt_number="12345",
        )
        for purchased in self.purchase_order.purchased_stock.all():
            data = {
                "stock": purchased.stock,
                "quantity": purchased.quantity,
                "unit": purchased.unit,
                "purchase_cost": purchased.purchase_cost,
                "discount_type": purchased.discount_type,
                "discount": purchased.discount,
            }
            received.received_detail.create(**data)

        received.purchase_order.received()
        self.assertEqual(received.received_detail.count(), 1)
        self.assertEqual(received.__str__(), received.code)
        self.assertEqual(self.purchase_order.status, PurchaseOrder.STATUS.received)


class TestVendorBill(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

        self.outlet = mommy.make(
            "outlet.Outlet", name="outlet 1", branch_id="outlet-1", account=self.account
        )

        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.supplier = mommy.make(
            "stocks.Supplier", name="Supplier", account=self.account
        )

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            supplier=self.supplier,
            outlet=self.outlet,
            account=self.account,
            date_of_purchase=datetime.today(),
            shipping_date=datetime.today(),
            code="PO-001",
            tax=0,
            discount_type=PurchaseOrder.DISCOUNT_TYPE.fixed,
            discount=0,
        )

        self.purchase_order.purchased_stock.create(
            stock=self.stock,
            quantity=1,
            unit=self.unit,
            purchase_cost=1000,
            discount_type="fixed",
            discount=0,
            account=self.account,
        )

        self.received_order = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order,
            receipt_number="12345",
        )
        self.received_order.receive_purchase(self.stock, 10, self.unit)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        vendor_bill = VendorBill.objects.create(
            bill_date=datetime.today(),
            due_date=datetime.today(),
            received_order=self.received_order,
        )

        for received in self.received_order.received_detail.all():
            data = {
                "product": received.stock.product,
                "quantity": received.quantity,
                "unit": received.unit,
                "purchase_cost": received.purchase_cost,
                "discount_type": received.discount_type,
                "discount": received.discount,
            }
            vendor_bill.vendor_bill_detail.create(**data)

        self.assertEqual(vendor_bill.vendor_bill_detail.count(), 1)
        self.assertEqual(vendor_bill.__str__(), vendor_bill.code)

    def test_create_vendor_bill_detail(self):
        vendor_bill = VendorBill.objects.create(
            bill_date=datetime.today(),
            due_date=datetime.today(),
            received_order=self.received_order,
        )
        vendor_bill.create_vendor_bill_detail(self.product, 10000)
        self.assertEqual(vendor_bill.vendor_bill_detail.count(), 1)
        self.assertEqual(vendor_bill.vendor_bill_detail.first().product, self.product)
        self.assertEqual(
            vendor_bill.vendor_bill_detail.first().purchase_cost.amount, 10000
        )
