import csv
import os
from test_plus.test import TestCase

from model_mommy import mommy

from django.core.files.uploadedfile import SimpleUploadedFile

from sterlingpos.core.models import set_current_tenant
from sterlingpos.core.import_export import SterlingImportForm
from sterlingpos.stocks.resources import SupplierResource
from sterlingpos.stocks.models import Supplier


class SupplierResourceTest(TestCase):
    def setUp(self):
        self.form_class = SterlingImportForm
        self.resource = SupplierResource()
        self.headers = SupplierResource.Meta.fields

        self.account = mommy.make("accounts.Account", name="account")
        set_current_tenant(self.account)
        self.uom = self.account.uom_set.filter(
            unit="pcs", category="units", uom_type="base"
        ).first()
        self.product_1 = mommy.make(
            "catalogue.Product",
            sku="SKU-01",
            name="Product 1",
            uom=self.uom,
            account=self.account,
            is_manage_stock=True,
        )
        self.product_2 = mommy.make(
            "catalogue.Product",
            sku="SKU-02",
            name="Product 2",
            uom=self.uom,
            account=self.account,
            is_manage_stock=True,
        )
        self.product_3 = mommy.make(
            "catalogue.Product",
            sku="SKU-03",
            name="Product 3",
            uom=self.uom,
            account=self.account,
            is_manage_stock=True,
        )

        self.supplier = mommy.make(
            "stocks.Supplier", code="SUP-001", name="Supplier001", account=self.account
        )
        self.supplier.supplier_detail.create(
            product=self.product_1,
            quantity=10,
            unit=self.uom,
            cost=15000,
            account=self.account,
        )
        raw_items = [
            ["SUP-01", "Supplier 1", "SKU-01", "1", "pcs", "1000"],
            ["SUP-01", "Supplier 1", "SKU-02", "1", "pcs", "1000"],
            ["SUP-01", "Supplier 1", "SKU-03", "5", "pcs", "2000"],
        ]
        with open("supplier_import_test.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(["code", "name", "sku", "quantity", "unit", "price"])
            for item in raw_items:
                writer.writerow(item)

        raw_items_bad = [
            ["", "Supplier 1", "SKU-01", "1", "pcs", ""],
        ]

        with open("supplier_import_test_bad_file.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(["code", "name", "sku", "quantity", "unit", "price"])
            for item in raw_items_bad:
                writer.writerow(item)

        raw_update_items = [
            [self.supplier.code, self.supplier.name, "SKU-01", "15", "pcs", "12500"],
        ]
        with open("supplier_update_import_test.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(["code", "name", "sku", "quantity", "unit", "price"])
            for item in raw_update_items:
                writer.writerow(item)

    def tearDown(self):
        set_current_tenant(None)
        os.remove("supplier_import_test.csv")
        os.remove("supplier_import_test_bad_file.csv")
        os.remove("supplier_update_import_test.csv")

    def test_create_supplier(self):
        with open("supplier_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        form.save()
        self.assertEqual(Supplier.objects.count(), 2)
        self.assertEqual(Supplier.objects.last().supplier_detail.count(), 3)

    def test_create_supplier_with_bad_import_value(self):
        with open("supplier_import_test_bad_file.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))

    def test_update_supllier(self):
        with open("supplier_update_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        form.save()
        self.assertEqual(self.supplier.supplier_detail.first().product, self.product_1)
        self.assertEqual(self.supplier.supplier_detail.first().quantity, 15)
        self.assertEqual(self.supplier.supplier_detail.first().cost.amount, 12500)
