import dateutil.parser

from django.utils import timezone
from django.conf import settings
from django import forms
from django.forms import inlineformset_factory, modelformset_factory
from django.forms.models import BaseModelFormSet, BaseInlineFormSet
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.core.exceptions import ObjectDoesNotExist
from decimal import Decimal, InvalidOperation
from django.shortcuts import get_object_or_404

from sterlingpos.core.bootstrap import (
    KawnFieldSetWithHelpText,
    KawnField,
    KawnAppendedText,
)
from sterlingpos.catalogue.models import Product, UOM
from sterlingpos.stocks.models import (
    Stock,
    StockTransfer,
    StockTransferDetail,
    StockCount,
    StockCountDetail,
    StockAdjustment,
    StockAdjustmentDetail,
    Batch,
    BatchItem,
    Supplier,
    SupplierProductDetail,
    PurchaseOrder,
    PurchaseOrderDetail,
    TRANSACTION_TYPE,
    InventorySetting,
    PurchaseOrderReceive,
    PurchaseOrderReceiveDetail,
    VendorBill,
    VendorBillDetail,
)
from sterlingpos.catalogue.utils import (
    uom_conversion,
    get_normalized_decimal,
    uom_cost_conversion,
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.core.widgets import MoneyInputField

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, TEMPLATE_PACK, HTML
from dal import autocomplete, forward


MAX_DIGITS_INVENTORY = 5
MAX_NUM_FORMS = 100


class BaseFormsetLayout(object):
    template = "%s/formset.html" % TEMPLATE_PACK

    def __init__(self, formset, context, template=None):
        self.formset = formset
        self.context = context
        if template:
            self.template = template

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, **kwargs):
        return render_to_string(
            self.template,
            {"wrapper": self, "formset": self.formset, "context": self.context},
        )


class RequiredFormSet(BaseInlineFormSet):
    def is_valid(self):
        return super(RequiredFormSet, self).is_valid()

    def clean(self):
        count = 0
        for form in self.forms:
            try:
                if form.cleaned_data and not form.cleaned_data.get("DELETE", False):
                    count += 1
            except AttributeError:
                pass
        if count < 1:
            raise forms.ValidationError("Required at least one product.")


def unique_field_formset(field_name):
    class UniqueFieldFormSet(BaseInlineFormSet):
        def clean(self):
            if any(self.errors):
                return
            values = set()
            count = 0

            for form in self.forms:
                try:
                    if form.cleaned_data and not form.cleaned_data.get("DELETE", False):
                        count += 1
                except AttributeError:
                    pass
                if not form.cleaned_data["DELETE"]:
                    value = form.cleaned_data[field_name]
                    if value in values:
                        form.add_error(
                            field_name,
                            _("Duplicate {} values: '{}'.".format(field_name, value)),
                        )
                    values.add(value)
            if count < 1:
                raise forms.ValidationError("Required at least one product.")

    return UniqueFieldFormSet


class StockBalanceForm(forms.ModelForm):
    ACTION = [
        ("add_balance", _("Add")),
        ("subtract_balance", _("Subtract")),
        ("set_balance", _("Set")),
    ]

    quantity = forms.IntegerField(min_value=1)
    unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)
    action = forms.ChoiceField(choices=ACTION)

    class Meta:
        model = Stock
        exclude = ("account",)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super(StockBalanceForm, self).__init__(*args, **kwargs)
        self.fields["product"].disabled = True
        self.fields["outlet"].disabled = True
        self.fields["unit"].queryset = UOM.objects.filter(is_active=True)

    def save(self, *args, **kwargs):
        action_func = getattr(self.instance, self.cleaned_data.get("action"))
        action_func(
            self.cleaned_data.get("quantity"),
            unit=self.cleaned_data.get("unit"),
            user=self.user,
        )
        return self.instance


class StockTransferForm(forms.ModelForm):
    class Meta:
        model = StockTransfer
        fields = ("date_of_transfer", "source", "destination", "note")
        widgets = {
            "source": autocomplete.ModelSelect2(
                url="stocks:outlet_inventory_autocomplete",
                attrs={"data-minimum-input-length": 0},
            ),
            "destination": autocomplete.ModelSelect2(
                url="stocks:outlet_dest_inventory_autocomplete",
                attrs={"data-minimum-input-length": 0},
            ),
        }

    def __init__(self, *args, **kwargs):
        super(StockTransferForm, self).__init__(*args, **kwargs)
        self.fields["note"].widget.attrs["rows"] = 3
        self.fields["note"].widget.attrs["maxlength"] = 100
        self.fields["date_of_transfer"].label = _("Date of Transfer")
        self.fields["date_of_transfer"].widget.attrs["readonly"] = "True"
        self.fields["date_of_transfer"].widget.attrs["class"] = "bg-white"
        self.fields["date_of_transfer"].input_formats = ["%d/%m/%Y %H:%M"]
        self.fields["date_of_transfer"].widget.format = "%d/%m/%Y %H:%M"

        self.fields["source"].label = _("Source Stock")
        self.fields["destination"].label = _("Destination Stock")
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Stock Transfer"),
                KawnField("date_of_transfer"),
                Div(
                    Div(
                        Div(KawnField("source"), css_class="col-lg-6"),
                        Div(KawnField("destination"), css_class="col-lg-6"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                KawnField("note"),
            )
        )

    def clean_destination(self):
        source = self.cleaned_data.get("source")
        destination = self.cleaned_data["destination"]

        if source == destination:
            raise forms.ValidationError(_("Cannot transfer between same outlet."))
        return destination

    def clean_note(self):
        note = self.cleaned_data["note"]
        if len(note) > 100:
            raise forms.ValidationError(
                _("Catatan tidak bisa lebih dari 100 karakter.")
            )
        return note


class StockTransferDetailForm(forms.ModelForm):
    product = forms.ModelChoiceField(
        queryset=Product.objects.none(),
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy("stocks:product_inventory_autocomplete"),
            attrs={"data-placeholder": _("Type to search")},
        ),
    )
    source_stock = forms.CharField(
        label=_("Source Stock"),
        required=False,
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    destination_stock = forms.CharField(
        label=_("Destination Stock"),
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
    )
    unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)

    class Meta:
        model = StockTransferDetail
        fields = ("quantity", "stock_transfer")

    field_order = ["product", "quantity", "unit", "source_stock", "destination_stock"]

    def __init__(self, *args, **kwargs):
        self.outlet_permission = kwargs.pop("outlet_permission")
        self.source_outlet = kwargs.pop("source_outlet")

        super(StockTransferDetailForm, self).__init__(*args, **kwargs)
        self.empty_permitted = False
        self.fields["product"].widget.attrs["class"] = "products-dropdown"
        self.fields["product"].queryset = Product.objects.filter(is_manage_stock=True)
        self.fields["product"].label = _("Product")

        self.fields["quantity"].widget.attrs["class"] = "quantity-input"
        self.fields["quantity"].widget.attrs["pattern"] = "[0-9]+([\,|\.][0-9]+)?"
        self.fields["quantity"].widget.attrs["step"] = "0.001"

        self.fields["unit"].widget.attrs["class"] = "products-unit"
        self.fields["unit"].queryset = UOM.objects.filter(is_active=True)
        self.fields["unit"].empty_label = None

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["source_stock"].widget.attrs["style"] = styles
        self.fields["destination_stock"].widget.attrs["style"] = styles

        if not self.outlet_permission:
            del self.fields["source_stock"]
            del self.fields["destination_stock"]

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["unit"].initial = instance.unit
            self.fields["product"].initial = instance.stock_source.product

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        deleted = cleaned_data.get("DELETE")
        product = cleaned_data.get("product")
        unit = cleaned_data.get("unit")

        if quantity:
            quantity = Decimal(quantity)
            digits = len(quantity.as_tuple().digits) - abs(quantity.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                self._errors["quantity"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))]
                )

        if not deleted:
            if product:
                if not quantity:
                    self._errors["quantity"] = self.error_class(
                        [_("Nilai harus lebih besar dari 0")]
                    )
                if unit.category != product.uom.category:
                    self._errors["unit"] = self.error_class([_("Satuan tidak sesuai")])
            else:
                self._errors["product"] = self.error_class(
                    [_("This field is required.")]
                )

        return cleaned_data

    def save(self, *args, **kwargs):
        product = self.cleaned_data.pop("product")
        quantity = self.cleaned_data.pop("quantity")
        unit = self.cleaned_data.pop("unit")
        stock_transfer = self.cleaned_data.pop("stock_transfer")

        if self.instance and self.instance.pk:
            source = product.stocks.get(outlet=stock_transfer.source)
            destination = product.stocks.get(outlet=stock_transfer.destination)
            stock_transfer_detail = super().save(commit=False)
            stock_transfer_detail.stock_source = source
            stock_transfer_detail.stock_destination = destination
            stock_transfer_detail.unit = unit
            stock_transfer_detail.save()
        else:
            stock_transfer_detail = stock_transfer.transfer_product_stock(
                product=product, unit=unit, quantity=quantity
            )
        return stock_transfer_detail


class StockTransferDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(StockTransferDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "stock_transfer_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Stock Transfer Detail")
        self.add_content = _("Add Product")
        self.form_grid = "col-lg-12"


StockTransferDetailFormset = inlineformset_factory(
    StockTransfer,
    StockTransferDetail,
    form=StockTransferDetailForm,
    formset=unique_field_formset("product"),
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=True,
)


class StockTransferReceivedForm(forms.ModelForm):
    class Meta:
        model = StockTransfer
        fields = ("id",)

    def __init__(self, *args, **kwargs):
        super(StockTransferReceivedForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False

    def save(self, *args, **kwargs):
        transfer = super().save(commit=False)
        transfer.date_of_arrival = timezone.now()
        transfer.save()
        return transfer


class StockCountForm(forms.ModelForm):
    class Meta:
        model = StockCount
        fields = (
            "outlet",
            "stock_count_type",
            "note",
            "include_sellable",
            "include_manufactured",
        )
        widgets = {
            "outlet": autocomplete.ModelSelect2(
                url="stocks:outlet_inventory_autocomplete",
                attrs={"data-minimum-input-length": 0},
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["note"].widget.attrs["rows"] = 3
        self.fields["note"].widget.attrs["maxlength"] = 100
        self.fields["stock_count_type"].label = _("Tipe Stok Opname")
        self.fields["include_sellable"].label = _("Termasuk Produk Dapat Dijual")
        self.fields["include_manufactured"].label = _("Termasuk Produk Manufaktur")

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["stock_count_type"].required = False
            self.helper.layout = Layout(
                KawnFieldSetWithHelpText(
                    _("Stock Count"),
                    KawnField("outlet"),
                    KawnField("note"),
                )
            )
        else:
            self.helper.layout = Layout(
                KawnFieldSetWithHelpText(
                    _("Stock Count"),
                    KawnField("outlet"),
                    KawnField(
                        "stock_count_type",
                        template="dashboard/stocks/form_fields/stock_opname_type_radio_button.html",
                    ),
                    KawnField(
                        "include_sellable",
                        template="dashboard/stocks/form_fields/switch.html",
                    ),
                    KawnField(
                        "include_manufactured",
                        template="dashboard/stocks/form_fields/switch.html",
                    ),
                    KawnField("note"),
                )
            )

    def clean_note(self):
        note = self.cleaned_data["note"]
        if len(note) > 100:
            raise forms.ValidationError(
                _("Catatan tidak bisa lebih dari 100 karakter.")
            )
        return note


class StockCountDetailForm(forms.ModelForm):
    product = forms.ModelChoiceField(
        queryset=Product.objects.none(),
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy("stocks:product_inventory_autocomplete"),
            attrs={"data-placeholder": _("Type to search")},
            forward=(
                forward.JavaScript(handler="get_is_sellable", dst="is_sellable"),
                forward.JavaScript(
                    handler="get_is_include_manufactured", dst="is_include_manufactured"
                ),
            ),
        ),
    )
    stock_expectation = forms.CharField(
        label=_("Stock Expectation"),
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
    )

    class Meta:
        model = StockCountDetail
        exclude = ("quantity", "counted", "unit", "stock", "account")

    field_order = ["product", "stock_expectation"]

    def __init__(self, *args, **kwargs):
        self.outlet_permission = kwargs.pop("outlet_permission")
        self.include_manufactured = kwargs.pop("include_manufactured")
        self.include_sellable = kwargs.pop("include_sellable")

        super(StockCountDetailForm, self).__init__(*args, **kwargs)
        self.empty_permitted = False
        self.fields["product"].widget.attrs["class"] = "products-dropdown"
        self.fields["product"].label = _("Product")

        product_qs = Product.objects.filter(is_manage_stock=True, uom__isnull=False)
        if not self.include_manufactured:
            product_qs = product_qs.exclude(
                classification__in=[Product.PRODUCT_CLASSIFICATION.manufactured]
            )
        if not self.include_sellable:
            product_qs = product_qs.exclude(is_sellable=True)
        self.fields["product"].queryset = product_qs

        self.fields["stock_expectation"].widget.attrs[
            "style"
        ] = "background-color: #fff !important; border: 0 !important;"

        if not self.outlet_permission:
            del self.fields["stock_expectation"]

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["product"].initial = instance.stock.product

    def save(self, *args, **kwargs):
        product = self.cleaned_data.pop("product")
        stock_count = self.cleaned_data.pop("stock_count")

        if self.instance and self.instance.pk:
            stock = product.stocks.get(outlet=self.instance.stock_count.outlet)
            balance = stock.balance
            transaction_unit = product.uom

            # transaction = (
            #     stock.transactions.all()
            #     .select_related("unit", "stock__product", "stock__product__catalogue")
            #     .last()
            # )
            # try:
            #     balance = transaction.tail_sum
            #     transaction_unit = transaction.unit
            # except AttributeError:
            #     balance = 0
            #     transaction_unit = product.uom

            stock_count_detail = super().save(commit=True)
            stock_count_detail.stock = stock
            stock_count_detail.quantity = balance
            stock_count_detail.unit = transaction_unit
            stock_count_detail.save()
        else:
            stock_count_detail = stock_count.count_product_stock(product=product)
        return stock_count_detail


class StockCountDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(StockCountDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "stock_count_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Stock Count Detail")
        self.add_content = _("Add Product")


StockCountDetailFormset = inlineformset_factory(
    StockCount,
    StockCountDetail,
    form=StockCountDetailForm,
    formset=unique_field_formset("product"),
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=True,
)


class StockCountCountingForm(forms.ModelForm):
    product = forms.CharField(
        required=False, widget=forms.TextInput(attrs={"readonly": True})
    )
    total_qty = forms.CharField(
        required=False, widget=forms.TextInput(attrs={"readonly": True})
    )
    unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)

    class Meta:
        model = StockCountDetail
        fields = ("counted",)

    field_order = ["product", "total_qty", "counted", "unit"]

    def __init__(self, *args, **kwargs):
        self.outlet_permission = kwargs.pop("outlet_permission")
        super().__init__(*args, **kwargs)
        self.fields["total_qty"].label = "Quantity"

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["total_qty"].widget.attrs["style"] = styles
        self.fields["product"].widget.attrs["style"] = styles
        self.fields["counted"].initial = 0
        self.fields["counted"].widget.attrs["class"] = "counted-input"

        if not self.outlet_permission:
            del self.fields["total_qty"]

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["unit"].widget.attrs["class"] = "products-unit"
            self.fields["unit"].queryset = UOM.objects.filter(
                category=instance.stock.product.uom.category, is_active=True
            )
            self.fields["unit"].empty_label = None
            self.fields["product"].initial = instance.stock.product
            if self.outlet_permission:
                self.fields["total_qty"].initial = "{} {}".format(
                    get_normalized_decimal(instance.quantity),
                    instance.stock.product.uom,
                )
            self.fields["unit"].initial = instance.stock.product.uom
            if instance.unit:
                self.fields["unit"].initial = instance.unit

    def clean(self):
        cleaned_data = super().clean()
        counted = cleaned_data.get("counted")
        if counted:
            counted = Decimal(counted)
            digits = len(counted.as_tuple().digits) - abs(counted.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                self._errors["counted"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))]
                )
        return cleaned_data


class StockCountCountingFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(StockCountCountingFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.include_media = False
        self.disabled_csrf = True
        self.add_false = True
        self.template = "bootstrap4/formset_inline.html"


StockCountCountingFormset = inlineformset_factory(
    StockCount,
    StockCountDetail,
    form=StockCountCountingForm,
    extra=0,
    max_num=MAX_NUM_FORMS,
    can_delete=False,
)


class StockCountCompleteForm(forms.ModelForm):
    class Meta:
        model = StockCount
        fields = ("id",)

    def __init__(self, *args, **kwargs):
        super(StockCountCompleteForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False

    def save(self, *args, **kwargs):
        instance = super().save(commit=False)
        instance.date_of_completion = timezone.now()
        instance.save()
        return instance


class StockCountProductForm(forms.ModelForm):
    class Meta:
        model = StockCountDetail
        fields = ("counted", "unit")
        widgets = {
            "counted": forms.TextInput(),
        }

    def __init__(self, *args, **kwargs):
        super(StockCountProductForm, self).__init__(*args, **kwargs)
        self.fields["unit"].empty_label = None
        self.fields["unit"].widget.attrs["class"] = "products-unit"
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnField("counted"),
            KawnField("unit"),
        )

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["unit"].queryset = UOM.objects.filter(
                category=instance.unit.category, is_active=True
            )

    def clean_counted(self):
        counted = self.cleaned_data["counted"]
        if counted:
            counted = Decimal(counted)
            digits = len(counted.as_tuple().digits) - abs(counted.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                raise forms.ValidationError(
                    _("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))
                )
        return counted


class StockAdjustmentForm(forms.ModelForm):
    class Meta:
        model = StockAdjustment
        fields = ("status", "note", "outlet")
        widgets = {
            "outlet": autocomplete.ModelSelect2(
                url="stocks:outlet_inventory_autocomplete",
                attrs={"data-minimum-input-length": 0},
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["note"].widget.attrs["rows"] = 3
        self.fields["note"].widget.attrs["maxlength"] = 100

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Stock Adjustment"),
                KawnField("status", css_class="custom-select"),
                KawnField("outlet"),
                KawnField("note"),
            )
        )

    def clean_note(self):
        note = self.cleaned_data["note"]
        if len(note) > 100:
            raise forms.ValidationError(
                _("Catatan tidak bisa lebih dari 100 karakter.")
            )
        return note


class StockAdjustmentDetailForm(forms.ModelForm):
    product = forms.ModelChoiceField(
        queryset=Product.objects.none(),
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy("stocks:product_inventory_autocomplete"),
            attrs={"data-placeholder": _("Type to search")},
        ),
    )
    cost = forms.DecimalField(
        widget=forms.TextInput,
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        required=False,
    )
    total_cost = forms.CharField(
        label=_("Total Cost"),
        widget=forms.TextInput(
            attrs={"disabled": "disabled", "class": "products-total-cost"}
        ),
        required=False,
    )
    stock_available = forms.CharField(
        label=_("Stock Available"),
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
    )
    stock_final = forms.CharField(
        label=_("Stock Final"),
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
    )
    unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)

    class Meta:
        model = StockAdjustmentDetail
        fields = ("quantity", "stock_adjustment")

    field_order = [
        "product",
        "quantity",
        "unit",
        "stock_available",
        "stock_final",
        "cost",
        "total_cost",
    ]

    def __init__(self, *args, **kwargs):
        self.outlet_permission = kwargs.pop("outlet_permission")
        self.stock = None
        super().__init__(*args, **kwargs)
        self.fields["product"].widget.attrs["class"] = "products-dropdown"
        self.fields["product"].queryset = Product.objects.filter(is_manage_stock=True)
        self.fields["product"].label = _("Product")
        self.fields["quantity"].widget.attrs["class"] = "products-quantity"
        self.fields["quantity"].widget.attrs["pattern"] = "[0-9]+([\,|\.][0-9]+)?"
        self.fields["quantity"].widget.attrs["step"] = "0.001"
        self.fields["cost"].widget.attrs["class"] = "products-cost"
        self.fields["unit"].widget.attrs["class"] = "products-unit"
        self.fields["unit"].queryset = UOM.objects.filter(is_active=True)
        self.fields["unit"].empty_label = None
        self.fields["unit"].label = _("Unit")
        self.fields["cost"].label = _("Cost/Unit")

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["stock_final"].widget.attrs["style"] = styles
        self.fields["stock_available"].widget.attrs["style"] = styles
        self.fields["total_cost"].widget.attrs["style"] = styles

        if not self.outlet_permission:
            del self.fields["stock_final"]
            del self.fields["stock_available"]

    def clean(self):
        cleaned_data = super().clean()
        stock_adjustment = cleaned_data.get("stock_adjustment")
        deleted = cleaned_data.get("DELETE")
        outlet = stock_adjustment.outlet
        status = stock_adjustment.status
        quantity = cleaned_data.get("quantity")
        cost = cleaned_data.get("cost")
        unit = cleaned_data.get("unit")

        if not deleted:
            if hasattr(cleaned_data.get("product"), "stocks"):
                self.stock = cleaned_data.get("product").stocks.get(outlet=outlet)
                balance = self.stock.balance
                transaction_unit = cleaned_data.get("product").uom

                # transaction = (
                #     self.stock.transactions.all()
                #     .select_related(
                #         "unit", "stock__product", "stock__product__catalogue"
                #     )
                #     .last()
                # )
                # try:
                #     balance = transaction.tail_sum
                #     transaction_unit = transaction.unit
                # except AttributeError:
                #     balance = 0
                #     transaction_unit = cleaned_data.get("product").uom

                if status not in [
                    StockAdjustment.STATUS.stock_count,
                    StockAdjustment.STATUS.item_loss,
                    StockAdjustment.STATUS.item_damaged,
                    StockAdjustment.STATUS.waste_product,
                ]:
                    if cost is None:
                        self._errors["cost"] = self.error_class([""])

                if quantity:
                    quantity = Decimal(quantity)
                    digits = len(quantity.as_tuple().digits) - abs(
                        quantity.as_tuple().exponent
                    )
                    if digits > MAX_DIGITS_INVENTORY:
                        self._errors["quantity"] = self.error_class(
                            [
                                _(
                                    "Maksimal digit {} sebelum koma".format(
                                        MAX_DIGITS_INVENTORY
                                    )
                                )
                            ]
                        )
                    converted_qty = uom_conversion(unit, transaction_unit, quantity)

                    if status in [
                        StockAdjustment.STATUS.stock_count,
                        StockAdjustment.STATUS.item_receive,
                    ]:
                        if quantity < 0:
                            self._errors["quantity"] = self.error_class(
                                [_("Nilai harus lebih besar dari 0")]
                            )
                    elif status in [
                        StockAdjustment.STATUS.item_loss,
                        StockAdjustment.STATUS.item_damaged,
                        StockAdjustment.STATUS.waste_product,
                    ]:
                        if converted_qty > balance:
                            self._errors["quantity"] = self.error_class(
                                [_("Stok tidak cukup")]
                            )
                    else:
                        if not quantity > 0:
                            self._errors["quantity"] = self.error_class(
                                [_("Nilai harus lebih besar dari 0")]
                            )
                else:
                    if not status == StockAdjustment.STATUS.stock_count:
                        self._errors["quantity"] = self.error_class(
                            [_("Nilai harus lebih besar dari 0")]
                        )
        return cleaned_data

    def save(self, *args, **kwargs):
        quantity = self.cleaned_data.pop("quantity")
        cost = self.cleaned_data.pop("cost")
        if not cost:
            cost = 0
        stock_adjustment = self.cleaned_data.pop("stock_adjustment")
        unit = self.cleaned_data.pop("unit")

        if self.instance and self.instance.pk:
            stock_adjustment_detail = super().save(commit=False)
            stock_adjustment_detail.stock = self.stock
            stock_adjustment_detail.unit = unit
            stock_adjustment_detail.cost = cost
            stock_adjustment_detail.save()
        else:
            stock_adjustment_detail = stock_adjustment.adjust_product_stock(
                stock=self.stock, unit=unit, quantity=quantity, cost=cost
            )

        return stock_adjustment_detail


class StockAdjustmentDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(StockAdjustmentDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "stock_adjustment_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Stock Adjustment Detail")
        self.add_content = _("Add Product")
        self.form_grid = "col-lg-12"


StockAdjustmentDetailFormset = inlineformset_factory(
    StockAdjustment,
    StockAdjustmentDetail,
    form=StockAdjustmentDetailForm,
    formset=unique_field_formset("product"),
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=True,
)


class BatchForm(forms.ModelForm):

    quantity_to_assign = forms.CharField(
        max_length=100, required=False, widget=forms.TextInput(attrs={"readonly": True})
    )
    product_name = forms.CharField(
        required=False, widget=forms.TextInput(attrs={"readonly": True})
    )
    unit = forms.CharField(widget=forms.HiddenInput())
    total_quantity = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Batch
        fields = ("product",)
        widgets = {"product": forms.HiddenInput()}

    field_order = ["product", "product_name", "quantity_to_assign"]

    def __init__(self, *args, **kwargs):
        super(BatchForm, self).__init__(*args, **kwargs)
        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["product_name"].widget.attrs["style"] = styles
        self.fields["quantity_to_assign"].widget.attrs["style"] = styles

        self.empty_permitted = False


class BatchFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(BatchFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.include_media = False
        self.add_false = True
        self.template = "bootstrap4/formset_inline.html"


class BaseBatchFormset(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.status = kwargs.pop("status")
        self.outlet = kwargs.pop("outlet")
        super(BaseBatchFormset, self).__init__(*args, **kwargs)
        self.queryset = Product.objects.none()

    def add_fields(self, form, index):
        super(BaseBatchFormset, self).add_fields(form, index)
        form.nested = BatchDetailFormsetInline(
            instance=form.instance,
            data=form.data if form.is_bound else None,
            files=form.files if form.is_bound else None,
            prefix="{}-{}".format(
                form.prefix, BatchDetailFormsetInline.get_default_prefix()
            ),
            form_kwargs={
                "product": form.initial.get("product"),
                "status": self.status,
                "unit": form.initial.get("unit"),
                "form_prefix": form.prefix,
                "outlet": self.outlet,
            },
        )

    def is_valid(self):
        result = super(BaseBatchFormset, self).is_valid()
        if self.is_bound:
            for form in self.forms:
                if not self._should_delete_form(form):
                    result = result and form.nested.is_valid()
        return result

    def clean(self):
        cleaned_data = super().clean()
        for form in self.forms:
            total_qty = form.cleaned_data.get("total_quantity")
            product_name = form.cleaned_data.get("product_name")
            product = form.cleaned_data.get("product")
            unit_to = UOM.objects.get(pk=form.cleaned_data.get("unit"))
            total_quantity = 0
            if not hasattr(form, "nested") or self._should_delete_form(form):
                continue
            for i, nested in enumerate(form.nested):
                nested.empty_permitted = False
                if not nested.is_valid():
                    continue
                batch_id = nested.cleaned_data.get("batch_id")
                quantity = nested.cleaned_data.get("quantity")
                unit = nested.cleaned_data.get("unit")
                expiration_date = nested.cleaned_data.get("expiration_date")
                deleted = nested.cleaned_data.get("DELETE")
                if not deleted:
                    if not batch_id:
                        nested.add_error("batch_id", "")

                    if not quantity:
                        nested.add_error("quantity", "")
                    else:
                        try:
                            quantity = Decimal(quantity)
                            digits = len(quantity.as_tuple().digits) - abs(
                                quantity.as_tuple().exponent
                            )
                            if digits > MAX_DIGITS_INVENTORY:
                                nested.add_error(
                                    "quantity",
                                    _(
                                        "Maksimal digit {} sebelum koma".format(
                                            MAX_DIGITS_INVENTORY
                                        )
                                    ),
                                )
                            else:
                                total_quantity += uom_conversion(
                                    unit, unit_to, nested.cleaned_data.get("quantity")
                                )
                                total_quantity = Decimal(
                                    total_quantity.quantize(Decimal(".001"))
                                )
                        except InvalidOperation:
                            nested.add_error("quantity", _("Invalid Format"))

                    try:
                        batch_items = product.batches.get(
                            batch_id=batch_id
                        ).batch_items.get(outlet=self.outlet)

                        if self.status in [
                            StockAdjustment.STATUS.item_loss,
                            StockAdjustment.STATUS.item_damaged,
                            StockAdjustment.STATUS.waste_product,
                            TRANSACTION_TYPE.transfer,
                        ]:
                            converted_qty = uom_conversion(
                                unit, product.uom, nested.cleaned_data.get("quantity")
                            )
                            if batch_items.balance < converted_qty:
                                nested.add_error("quantity", "")
                                msg = mark_safe(
                                    _(
                                        f"""
                                        Jumlah tidak bisa lebih dari Stock
                                        <strong>Batch {batch_id} - {self.outlet.name}</strong>
                                        """
                                    )
                                )
                                self._non_form_errors.append(self.error_class([msg]))
                    except ObjectDoesNotExist:
                        pass
                    except AttributeError:
                        pass

                    exist_batch = Batch.objects.filter(
                        batch_id=batch_id, product=product
                    )
                    if not exist_batch:
                        if not expiration_date:
                            nested.add_error("expiration_date", "")

            total_qty = Decimal(total_qty)
            result_qty = total_qty - total_quantity
            result_qty = get_normalized_decimal(result_qty)
            if total_qty > 0:
                if result_qty > 0:
                    msg = mark_safe(
                        _(
                            f"""<strong>{product_name}</strong>
                            batch tracked quantity has {result_qty} {unit_to.unit} more to assign."""
                        )
                    )
                    self._non_form_errors.append(self.error_class([msg]))
                elif result_qty < 0:
                    msg = mark_safe(
                        _(
                            f"""<strong>{product_name}</strong> batch track total quantities: {total_quantity} {unit_to.unit},
                            is more than Quantity to assing: {total_qty} {unit_to.unit}"""
                        )
                    )
                    self._non_form_errors.append(self.error_class([msg]))

            if total_qty < 0:
                if result_qty < 0:
                    msg = mark_safe(
                        _(
                            f"""<strong>{product_name}</strong>
                            batch tracked quantity has {result_qty} {unit_to.unit} more to assign."""
                        )
                    )
                    self._non_form_errors.append(self.error_class([msg]))
                elif result_qty > 0:
                    msg = mark_safe(
                        _(
                            f"""<strong>{product_name}</strong> batch track total quantities: {total_quantity} {unit_to.unit},
                            is more than Quantity to assing: {total_qty} {unit_to.unit}"""
                        )
                    )
                    self._non_form_errors.append(self.error_class([msg]))
        return cleaned_data


BatchFormset = modelformset_factory(
    Batch, form=BatchForm, formset=BaseBatchFormset, can_delete=False
)


class BatchItemForm(forms.ModelForm):

    batch_id = forms.CharField(max_length=100, required=True)
    batch_quantity = forms.CharField(
        max_length=100, required=False, widget=forms.TextInput(attrs={"readonly": True})
    )
    unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)
    expiration_date = forms.CharField(max_length=100, required=False)
    quantity = forms.DecimalField(
        max_digits=8, decimal_places=3, widget=forms.TextInput(), required=True
    )

    class Meta:
        model = BatchItem
        fields = ("id",)

    field_order = [
        "id",
        "batch_id",
        "batch_quantity",
        "quantity",
        "unit",
        "expiration_date",
    ]

    def __init__(self, *args, **kwargs):
        product = kwargs.pop("product")
        status = kwargs.pop("status")
        unit = kwargs.pop("unit")
        form_prefix = kwargs.pop("form_prefix")
        outlet = kwargs.pop("outlet")
        super(BatchItemForm, self).__init__(*args, **kwargs)
        self.fields["batch_quantity"].initial = 0
        self.fields["batch_quantity"].label = "Batch Quantity"
        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["batch_quantity"].widget.attrs["style"] = styles

        autocomplete_attrs = {}
        if status in [
            StockAdjustment.STATUS.item_receive,
            StockAdjustment.STATUS.stock_count,
            TRANSACTION_TYPE.purchase_order,
            TRANSACTION_TYPE.productions,
            "missing_batch",
        ]:
            autocomplete_attrs = {"data-placeholder": _("Type to search or create new")}
            if product:
                self.fields["batch_id"].widget = autocomplete.Select2(
                    url=reverse_lazy(
                        "stocks:get_create_batch", kwargs={"pk": product.pk}
                    ),
                    attrs=autocomplete_attrs,
                )
            self.fields["expiration_date"].widget.attrs[
                "class"
            ] = "expiration_date-datepicker"
        else:
            autocomplete_attrs = {"data-placeholder": _("Type to search")}
            self.fields["expiration_date"].widget.attrs["readonly"] = True
            if product and outlet:
                self.fields["batch_id"].widget = autocomplete.Select2(
                    url=reverse_lazy(
                        "stocks:get_batch",
                        kwargs={"pk": product.pk, "outlet_pk": outlet.pk},
                    ),
                    attrs=autocomplete_attrs,
                )
        unit_queryset = UOM.objects.filter(is_active=True)
        if product:
            unit_queryset = UOM.objects.filter(
                category=product.uom.category, is_active=True
            )
        self.fields["unit"].queryset = unit_queryset
        self.fields["unit"].initial = unit

        self.fields["unit"].widget.attrs["class"] = (
            form_prefix + " batch_unit-select products-unit"
        )
        self.fields["unit"].empty_label = None
        self.fields["batch_id"].widget.attrs["class"] = (
            form_prefix + "-batch_id batch_id-select"
        )
        self.fields["quantity"].widget.attrs["class"] = (
            form_prefix + " batch_quantity-input"
        )


BatchDetailFormsetInline = inlineformset_factory(
    Batch, BatchItem, form=BatchItemForm, extra=0, min_num=1, can_delete=True
)


class SupplierForm(forms.ModelForm):
    class Meta:
        model = Supplier
        fields = ("name", "code", "contact", "email", "phone", "address", "is_active")

    def __init__(self, *args, **kwargs):
        super(SupplierForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.fields["is_active"].label = _("Status Aktif")

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["is_active"].initial = instance.is_active
        else:
            self.fields["is_active"].initial = True

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Supplier Detail"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("code"), css_class="col"),
                        Div(KawnField("email"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("contact"), css_class="col"),
                        Div(KawnField("phone"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                KawnField("address"),
                KawnField(
                    "is_active", template="dashboard/stocks/form_fields/switch.html"
                ),
            )
        )


class SupplierProductDetailForm(forms.ModelForm):
    class Meta:
        model = SupplierProductDetail
        fields = ("product", "quantity", "cost", "unit")
        widgets = {
            "product": autocomplete.ModelSelect2(
                url=reverse_lazy("stocks:purchase_order_autocomplete"),
                attrs={"data-placeholder": _("Type to search")},
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    field_order = ["product", "quantity", "unit", "cost"]

    def __init__(self, *args, **kwargs):
        super(SupplierProductDetailForm, self).__init__(*args, **kwargs)
        self.fields["product"].queryset = Product.objects.filter(is_manage_stock=True)
        self.fields["product"].widget.attrs["class"] = "products-dropdown"
        self.fields["product"].label = _("Product")
        self.fields["quantity"].widget.attrs["class"] = "products-quantity"
        self.fields["cost"].label = _("Cost")
        self.fields["cost"].widget.attrs["class"] = "products-cost"
        self.fields["unit"].queryset = UOM.objects.filter(is_active=True)
        self.fields["unit"].empty_label = None
        self.fields["unit"].label = _("Unit")
        self.fields["unit"].widget.attrs["class"] = "products-unit"

    def clean_quantity(self):
        quantity = self.cleaned_data.get("quantity")
        if not quantity:
            try:
                int(quantity)
                raise forms.ValidationError(_("Nilai harus lebih besar dari 0"))
            except TypeError:
                raise forms.ValidationError(_("This field is required."))
        else:
            quantity = Decimal(quantity)
            digits = len(quantity.as_tuple().digits) - abs(quantity.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                raise forms.ValidationError(
                    _("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))
                )
        return quantity

    def clean_product(self):
        product = self.cleaned_data.get("product")
        exclude = self._get_validation_exclusions()
        from django.core.exceptions import ValidationError

        try:
            self.instance.validate_unique(exclude=exclude)
        except ValidationError as e:
            raise forms.ValidationError(e)
        return product

    def save(self, *args, **kwargs):
        supplier = self.cleaned_data.pop("supplier")
        supplier_detail = super().save(commit=False)
        supplier_detail.supplier = supplier
        supplier_detail.save()
        return supplier_detail


class SupplierProductDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(SupplierProductDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "supplier_product_detial"
        self.template = "bootstrap4/formset_with_help.html"
        self.collapsible_title = _("Supplier menggunakan master data produk")
        self.collapsible_field_name = "master_product_data"
        self.legend = _("Data Produk Pemasok")
        self.add_content = _("Add Product")
        self.form_grid = "col-lg-10"


SupplierProductDetailFormset = inlineformset_factory(
    Supplier,
    SupplierProductDetail,
    form=SupplierProductDetailForm,
    formset=unique_field_formset("product"),
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=True,
)


class PurchaseOrderForm(forms.ModelForm):
    has_discount_tax = forms.BooleanField(
        initial=False, required=False, label=_("Purchase Order has discount and taxes")
    )

    class Meta:
        model = PurchaseOrder
        fields = (
            "supplier",
            "outlet",
            "date_of_purchase",
            "shipping_date",
            "note",
            "discount_type",
            "discount",
            "tax",
        )
        widgets = {
            "outlet": autocomplete.ModelSelect2(
                url="stocks:outlet_inventory_autocomplete",
                attrs={"data-minimum-input-length": 0},
            ),
            "supplier": autocomplete.ModelSelect2(
                url="stocks:stocks_get_supplier", attrs={"data-minimum-input-length": 0}
            ),
        }

    def __init__(self, *args, **kwargs):
        super(PurchaseOrderForm, self).__init__(*args, **kwargs)
        self.fields["note"].widget.attrs["rows"] = 3
        self.fields["note"].widget.attrs["maxlength"] = 100
        self.fields["supplier"].label = _("Supplier")

        self.fields["date_of_purchase"].label = _("Date of Purchase")
        self.fields["date_of_purchase"].widget.attrs["readonly"] = "True"
        self.fields["date_of_purchase"].widget.attrs["class"] = "bg-white"
        self.fields["date_of_purchase"].input_formats = ["%d/%m/%Y %H:%M"]
        self.fields["date_of_purchase"].widget.format = "%d/%m/%Y %H:%M"

        self.fields["shipping_date"].label = _("Shipping Date")
        self.fields["shipping_date"].widget.attrs["readonly"] = "True"
        self.fields["shipping_date"].widget.attrs["class"] = "bg-white"
        self.fields["shipping_date"].input_formats = ["%d/%m/%Y %H:%M"]
        self.fields["shipping_date"].widget.format = "%d/%m/%Y %H:%M"
        self.fields["shipping_date"].required = False
        self.fields["discount_type"].widget = forms.RadioSelect()

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Purchase Order"),
                Div(
                    Div(
                        Div(KawnField("date_of_purchase"), css_class="col"),
                        Div(KawnField("shipping_date"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("supplier"), css_class="col-lg-6"),
                        Div(KawnField("outlet"), css_class="col-lg-6"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                KawnField("note"),
            ),
        )
        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.helper.layout.append(
                KawnFieldSetWithHelpText(
                    _("Purchase Order discount and tax"),
                    Div(
                        Div(
                            Div(
                                KawnField("discount_type"),
                                KawnAppendedText(
                                    "discount", "IDR", css_id="div_id_discount"
                                ),
                                KawnAppendedText("tax", "%", css_id="div_id_tax"),
                                css_class="col",
                            ),
                            Div(
                                css_class="col",
                            ),
                            css_class="form-row",
                        ),
                        css_class="col-lg-12 px-0 mb-0",
                        css_id="div_purchase_order_disc_tax",
                    ),
                ),
            )
        else:
            self.helper.layout.append(
                KawnFieldSetWithHelpText(
                    "",
                    KawnField("has_discount_tax"),
                    Div(
                        Div(
                            Div(
                                KawnField("discount_type"),
                                KawnAppendedText(
                                    "discount", "IDR", css_id="div_id_discount"
                                ),
                                KawnAppendedText("tax", "%", css_id="div_id_tax"),
                                css_class="col",
                            ),
                            Div(
                                css_class="col",
                            ),
                            css_class="form-row",
                        ),
                        css_class="col-lg-12 px-0 mb-0",
                        css_id="div_purchase_order_disc_tax",
                    ),
                ),
            )

    def clean_discount(self):
        discount_type = self.cleaned_data["discount_type"]
        discount = self.cleaned_data["discount"]
        total_cost_list = [
            value for key, value in self.data.items() if "total_cost" in key
        ]
        total_po = 0
        for total_cost in total_cost_list:
            try:
                total_cost = Decimal(total_cost)
            except InvalidOperation:
                total_cost = Decimal("0")
            total_po += total_cost

        if discount_type == "percentage":
            if discount > Decimal("100"):
                raise forms.ValidationError(_("Maximum discount is 100 %"))
        else:
            if discount > total_po:
                raise forms.ValidationError(
                    _("Diskon melebihi total biaya Purchase Order")
                )
        return discount

    def clean_supplier(self):
        supplier = self.cleaned_data["supplier"]
        if not self.instance.pk:
            if not supplier.is_active:
                raise forms.ValidationError(_("Pemasok tidak aktif."))
        return supplier

    def clean_note(self):
        note = self.cleaned_data["note"]
        if len(note) > 100:
            raise forms.ValidationError(
                _("Catatan tidak bisa lebih dari 100 karakter.")
            )
        return note

    def clean_shipping_date(self):
        shipping_date = self.cleaned_data["shipping_date"]
        date_of_purchase = self.cleaned_data["date_of_purchase"]
        if shipping_date < date_of_purchase:
            raise forms.ValidationError(
                _("Tanggal pengiriman harus lebih besar dari tanggal pembelian")
            )
        return shipping_date


class PurchaseOrderDetailForm(forms.ModelForm):
    product = forms.ModelChoiceField(
        queryset=Product.objects.all(),
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy("stocks:product_supplier_autocomplete"),
            attrs={"data-placeholder": _("Type to search")},
            forward=(forward.JavaScript(handler="get_supplier", dst="supplier"),),
        ),
    )
    cost = forms.DecimalField(
        widget=forms.TextInput,
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        required=True,
    )

    total_cost = forms.DecimalField(
        widget=forms.TextInput(attrs={"readonly": True}),
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        required=False,
    )
    unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)

    class Meta:
        model = PurchaseOrderDetail
        fields = ("quantity", "purchase_order", "discount_type", "discount")

    field_order = [
        "product",
        "quantity",
        "unit",
        "discount_type",
        "discount",
        "cost",
        "total_cost",
    ]

    def __init__(self, *args, **kwargs):
        super(PurchaseOrderDetailForm, self).__init__(*args, **kwargs)
        self.fields["product"].queryset = Product.objects.filter(is_manage_stock=True)
        self.fields["product"].widget.attrs["class"] = "products-dropdown"
        self.fields["product"].label = _("Product")
        self.fields["quantity"].widget.attrs["class"] = "products-quantity"
        self.fields["quantity"].widget.attrs["pattern"] = "[0-9]+([\,|\.][0-9]+)?"
        self.fields["quantity"].widget.attrs["step"] = "0.001"
        self.fields["discount"].widget.attrs["class"] = "products-discount"
        self.fields["discount_type"].widget.attrs["class"] = "products-discount_type"

        self.fields["cost"].widget.attrs["class"] = "products-purchase_cost"
        self.fields["unit"].widget.attrs["class"] = "products-unit"
        self.fields["cost"].label = _("Cost")
        self.fields["unit"].queryset = UOM.objects.filter(is_active=True)
        self.fields["unit"].empty_label = None
        self.fields["unit"].label = _("Unit")

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["total_cost"].widget.attrs["style"] = styles

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["product"].initial = instance.stock.product
            self.fields["cost"].initial = int(instance.purchase_cost.amount)
            self.fields["total_cost"].initial = int(
                instance.quantity * self.fields["cost"].initial
            )
            self.fields["unit"].initial = instance.unit

    def clean(self):
        cleaned_data = super().clean()
        product = cleaned_data.get("product")
        quantity = cleaned_data.get("quantity")
        discount_type = cleaned_data.get("discount_type")
        discount = cleaned_data.get("discount")
        cost = cleaned_data.get("cost")
        unit = cleaned_data.get("unit")
        deleted = cleaned_data.get("DELETE")
        if deleted:
            if self.instance and self.instance.pk:
                if self.instance.is_received:
                    self._errors["product"] = self.error_class(
                        [_("Tidak bisa menghapus produk, karena sudah ada penerimaan")]
                    )
        if not quantity:
            try:
                quantity = int(quantity)
                self._errors["quantity"] = self.error_class(
                    [_("Nilai harus lebih besar dari 0")]
                )
            except TypeError:
                self._errors["quantity"] = self.error_class(
                    [_("This field is required.")]
                )
        else:
            quantity = Decimal(quantity)
            digits = len(quantity.as_tuple().digits) - abs(quantity.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                self._errors["quantity"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))]
                )

        if not cost:
            try:
                if Decimal(cost) < 0:
                    self._errors["cost"] = self.error_class(
                        [_("cost tidak bisa negatif")]
                    )
            except Exception:
                cost = 0

        if discount:
            discount = Decimal(discount)
            if discount_type == "percentage":
                if discount > Decimal("100"):
                    self._errors["discount"] = self.error_class(
                        [_("Maximum discount is 100 %")]
                    )
            else:
                if discount > Decimal(cost):
                    self._errors["discount"] = self.error_class(
                        [_("Diskon melebihi total biaya")]
                    )

        if product and unit:
            if unit.category != product.uom.category:
                self._errors["unit"] = self.error_class([_("Satuan tidak sesuai")])

        if self.instance and self.instance.pk:
            received_order = None
            received_order = self.instance.purchase_order.purchase_received.filter(
                purchase_order=self.instance.purchase_order, received=True
            )
            if received_order:
                if product != self.instance.stock.product:
                    self._errors["product"] = self.error_class(
                        [_("Tidak bisa mengubah produk, karena sudah ada penerimaan")]
                    )
                else:
                    total_received = 0
                    for received in received_order:
                        try:
                            received_detail = received.received_detail.get(
                                stock=self.instance.stock
                            )
                            converted_qty = uom_conversion(
                                received_detail.unit, unit, received_detail.quantity
                            )
                            total_received += converted_qty
                        except Exception:
                            total_received += 0

                    if quantity < total_received:
                        self._errors["quantity"] = self.error_class(
                            [
                                _(
                                    f"Jumlah tidak bisa lebih kecil dari jumlah penerimaan: {total_received} {unit}"
                                )
                            ]
                        )
        return cleaned_data

    def save(self, *args, **kwargs):
        product = self.cleaned_data.pop("product")
        quantity = self.cleaned_data.pop("quantity")
        cost = self.cleaned_data.pop("cost")
        purchase_order = self.cleaned_data.pop("purchase_order")
        unit = self.cleaned_data.pop("unit")
        discount_type = self.cleaned_data.pop("discount_type")
        discount = self.cleaned_data.pop("discount")

        if self.instance and self.instance.pk:
            stock = product.stocks.get(outlet=purchase_order.outlet)

            purchase_order_detail = super().save(commit=False)
            purchase_order_detail.stock = stock
            purchase_order_detail.unit = unit
            purchase_order_detail.purchase_cost = cost
            purchase_order_detail.save()
        else:
            purchase_order_detail = purchase_order.purchase_product_stock(
                product=product,
                quantity=quantity,
                unit=unit,
                purchase_cost=cost,
                discount_type=discount_type,
                discount=discount,
            )

        return purchase_order_detail


class PurchaseOrderDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(PurchaseOrderDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "purchase_order_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Purchase Order Detail")
        self.add_content = _("Add Product")
        self.form_grid = "col-lg-12"


class PurchaseOrderDetailSupplierForm(forms.ModelForm):
    product = forms.ModelChoiceField(
        queryset=Product.objects.all(),
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy("stocks:product_supplier_autocomplete"),
            attrs={"data-placeholder": _("Type to search")},
            forward=(forward.JavaScript(handler="get_supplier", dst="supplier"),),
        ),
    )
    cost = forms.CharField(
        max_length=100,
        required=False,
        label=_("Cost per Item"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    total_cost = forms.CharField(
        max_length=100,
        required=False,
        label=_("Total Cost"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    unit = forms.CharField(
        max_length=100,
        required=False,
        label=_("Unit"),
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
    )
    unit_id = forms.CharField(max_length=100, widget=forms.HiddenInput())

    class Meta:
        model = PurchaseOrderDetail
        fields = ("quantity", "purchase_order", "discount_type", "discount")

    field_order = [
        "product",
        "quantity",
        "unit",
        "discount_type",
        "discount",
        "cost",
        "total_cost",
    ]

    def __init__(self, *args, **kwargs):
        super(PurchaseOrderDetailSupplierForm, self).__init__(*args, **kwargs)
        self.fields["product"].queryset = Product.objects.filter(is_manage_stock=True)
        self.fields["product"].widget.attrs["class"] = "products-supplier"
        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["unit"].widget.attrs["style"] = styles
        self.fields["cost"].widget.attrs["style"] = styles
        self.fields["total_cost"].widget.attrs["style"] = styles
        self.fields["quantity"].widget = forms.TextInput(
            attrs={"class": "products-quantity"}
        )
        self.fields["discount"].widget.attrs["class"] = "products-discount"
        self.fields["discount_type"].widget.attrs["class"] = "products-discount_type"

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["quantity"].initial = get_normalized_decimal(instance.quantity)
            self.fields["product"].initial = instance.stock.product
            self.fields["cost"].initial = get_normalized_decimal(
                instance.purchase_cost.amount
            )
            self.fields["total_cost"].initial = get_normalized_decimal(
                instance.quantity * self.fields["cost"].initial
            )
            self.fields["unit"].initial = instance.unit
            self.fields["unit_id"].initial = instance.unit.pk

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        discount_type = cleaned_data.get("discount_type")
        discount = cleaned_data.get("discount")
        cost = cleaned_data.get("cost")
        deleted = cleaned_data.get("DELETE")

        if deleted:
            if self.instance and self.instance.pk:
                if self.instance.is_received:
                    self._errors["product"] = self.error_class(
                        [
                            _(
                                "Can't delete product. Product has been received full/partialy"
                            )
                        ]
                    )

        if not quantity:
            try:
                quantity = int(quantity)
                self._errors["quantity"] = self.error_class(
                    [_("Nilai harus lebih besar dari 0")]
                )
            except TypeError:
                self._errors["quantity"] = self.error_class(
                    [_("This field is required.")]
                )
        else:
            quantity = Decimal(quantity)
            digits = len(quantity.as_tuple().digits) - abs(quantity.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                self._errors["quantity"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))]
                )
        if discount:
            discount = Decimal(discount)
            if discount_type == "percentage":
                if discount > Decimal("100"):
                    self._errors["discount"] = self.error_class(
                        [_("Maximum discount is 100 %")]
                    )
            else:
                try:
                    cost = Decimal(cost)
                except InvalidOperation:
                    cost = Decimal("0")
                if discount > cost:
                    self._errors["discount"] = self.error_class(
                        [_("Diskon melebihi biaya produk")]
                    )
        return cleaned_data

    def save(self, *args, **kwargs):
        product = self.cleaned_data.pop("product")
        quantity = self.cleaned_data.pop("quantity")
        cost = self.cleaned_data.pop("cost")
        purchase_order = self.cleaned_data.pop("purchase_order")
        unit_id = self.cleaned_data.pop("unit_id")
        unit = get_object_or_404(UOM, pk=unit_id)
        discount_type = self.cleaned_data.pop("discount_type")
        discount = self.cleaned_data.pop("discount")

        if self.instance and self.instance.pk:
            stock = product.stocks.get(outlet=purchase_order.outlet)
            purchase_order_detail = super().save(commit=False)
            purchase_order_detail.stock = stock
            purchase_order_detail.unit = unit
            purchase_order_detail.purchase_cost = cost
            purchase_order_detail.save()
        else:
            purchase_order_detail = purchase_order.purchase_product_stock(
                product=product,
                quantity=quantity,
                unit=unit,
                purchase_cost=cost,
                discount_type=discount_type,
                discount=discount,
            )
        return purchase_order_detail


class PurchaseOrderDetailSupplierFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(PurchaseOrderDetailSupplierFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "supplier_purchase_order_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Purchase Order Detail")
        self.add_content = _("Add Product")
        self.form_grid = "col-lg-12"


PurchaseOrderDetailFormset = inlineformset_factory(
    PurchaseOrder,
    PurchaseOrderDetail,
    form=PurchaseOrderDetailForm,
    formset=unique_field_formset("product"),
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=True,
)


PurchaseOrderDetailSupplierFormset = inlineformset_factory(
    PurchaseOrder,
    PurchaseOrderDetail,
    form=PurchaseOrderDetailSupplierForm,
    formset=RequiredFormSet,
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=True,
)


class PurchaseOrderReceiveForm(forms.ModelForm):
    product = forms.CharField(
        required=False, widget=forms.TextInput(attrs={"readonly": True})
    )

    product_id = forms.CharField(widget=forms.HiddenInput())

    total_qty = forms.CharField(
        required=False, widget=forms.TextInput(attrs={"disabled": "disabled"})
    )
    quantity = forms.DecimalField(required=True)
    unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)
    received_date = forms.CharField()

    class Meta:
        model = PurchaseOrderDetail
        fields = ("id",)

    field_order = ["id", "product", "total_qty", "quantity", "unit", "received_date"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["total_qty"].label = _("Quantity")
        self.fields["quantity"].label = _("Received")
        self.fields["product"].label = _("Product")
        self.fields["unit"].label = _("Unit")
        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["total_qty"].widget.attrs["style"] = styles
        self.fields["product"].widget.attrs["style"] = styles

        self.fields["quantity"].widget.attrs["pattern"] = "[0-9]+([\,|\.][0-9]+)?"
        self.fields["quantity"].widget.attrs["step"] = "0.001"
        self.fields["quantity"].widget.attrs["class"] = "quantity-receive"

        self.fields["received_date"].widget.attrs["class"] = "received-date-input"
        self.fields["received_date"].required = True
        self.fields["received_date"].label = _("Received Date")

        instance = kwargs.get("instance")
        if instance and instance.pk and not instance.is_received:
            self.fields["unit"].widget.attrs["class"] = "products-unit"
            self.fields["unit"].queryset = UOM.objects.filter(
                category=instance.stock.product.uom.category, is_active=True
            )
            self.initial["quantity"] = get_normalized_decimal(instance.quantity)
            self.fields["product"].initial = instance.stock.product
            self.fields["product_id"].initial = instance.stock.product.id
            self.fields["total_qty"].initial = "{} {}".format(
                get_normalized_decimal(instance.quantity),
                instance.unit,
            )
            self.fields["unit"].empty_label = None
            self.fields["unit"].initial = instance.unit

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        unit = cleaned_data.get("unit")
        received_date = cleaned_data.get("received_date")

        if quantity:
            quantity = Decimal(quantity)
            digits = len(quantity.as_tuple().digits) - abs(quantity.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                self._errors["quantity"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))]
                )
            else:
                if not unit:
                    self._errors["unit"] = self.error_class(
                        [_("Satuan sudah dihapus/tidak aktif")]
                    )
                else:
                    converted_qty = uom_conversion(unit, self.instance.unit, quantity)
                    converted_qty = Decimal(converted_qty.quantize(Decimal(".001")))
                    if self.instance.quantity < converted_qty:
                        self._errors["quantity"] = self.error_class(
                            [
                                _(
                                    "Jumlah penerimaan tidak bisa lebih besar dari jumlah pembelian"
                                )
                            ]
                        )

            if quantity < 0:
                self._errors["quantity"] = self.error_class(
                    [_("Nilai tidak bisa negatif")]
                )

        if received_date:
            try:
                dateutil.parser.parse(received_date, dayfirst=True)
            except:
                self._errors["received_date"] = self.error_class(
                    [_("Format tanggal tidak sesuai")]
                )
        return cleaned_data


class PurchaseOrderReceiveFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(PurchaseOrderReceiveFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.include_media = False
        self.disabled_csrf = True
        self.add_false = True
        self.template = "bootstrap4/formset_inline.html"


PurchaseOrderReceiveFormset = inlineformset_factory(
    PurchaseOrder,
    PurchaseOrderDetail,
    form=PurchaseOrderReceiveForm,
    extra=0,
    max_num=MAX_NUM_FORMS,
    can_delete=False,
)


class InventorySettingForm(forms.ModelForm):
    class Meta:
        model = InventorySetting
        fields = (
            "inventory_code_format",
            "vendor_bill_accurate_excel",
        )

    def __init__(self, *args, **kwargs):
        super(InventorySettingForm, self).__init__(*args, **kwargs)
        self.fields["inventory_code_format"].widget = forms.RadioSelect()

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        help_text_inventory_code_format = ""
        instance = kwargs.get("instance")
        if instance.inventory_code_format == "comprehensive":
            help_text_inventory_code_format = """
                Contoh Kode tranasksi komprehensif untuk pemesanan pembelian (Kode Bisnis)/(Kode Outlet)/PO/(Tahun)/(Bulan)/0019
            """
        else:
            help_text_inventory_code_format = (
                "Contoh Kode transaksi simpel untuk pemesanan pembelian SPO-19"
            )
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField(
                    "inventory_code_format",
                ),
                HTML(
                    """
                        <span id='hint_id_inventory_code_format' class='d-lg-inline d-md-inline d-block text-muted'>
                            <small>"""
                    + help_text_inventory_code_format
                    + """</small>
                        </span>
                    """
                ),
                help_text=render_to_string("help_text/inventory_code_format.html"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField(
                    "vendor_bill_accurate_excel",
                    template="dashboard/table_management/switch_input/float_right_switch.html",
                ),
                help_text=render_to_string("help_text/vendor_bill_settings.html"),
            ),
        )


class PurchaseReceiveOrderForm(forms.ModelForm):
    purchase_order = forms.CharField(
        required=False, widget=forms.TextInput(attrs={"readonly": True})
    )

    class Meta:
        model = PurchaseOrderReceive
        fields = ("receipt_number",)

    def __init__(self, *args, **kwargs):
        purchase_order = kwargs.pop("purchase_order")
        try:
            self.purchase_order = PurchaseOrder.objects.get(pk=purchase_order)
        except (PurchaseOrder.DoesNotExist, ValueError):
            self.purchase_order = None
        super(PurchaseReceiveOrderForm, self).__init__(*args, **kwargs)
        self.fields["receipt_number"].required = True
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Receive Order"),
                KawnField("purchase_order"),
                KawnField("receipt_number"),
            )
        )
        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["purchase_order"].initial = instance.purchase_order

    def clean_receipt_number(self):
        receipt_number = self.cleaned_data["receipt_number"]
        if not receipt_number.isalnum():
            raise forms.ValidationError(_("Format harus Alphanumeric"))
        return receipt_number

    def save(self, commit=False):
        instance = super().save(commit=False)
        instance.purchase_order = self.purchase_order
        instance.save()
        return instance


class PurchaseOrderReceiveDetailForm(forms.ModelForm):
    product = forms.CharField(
        widget=forms.TextInput(attrs={"disabled": "disabled"}), required=False
    )

    total_qty = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
        label=_("Total Purchase"),
    )

    class Meta:
        model = PurchaseOrderReceiveDetail
        fields = ("quantity", "unit", "received", "stock")
        widgets = {"stock": forms.HiddenInput(attrs={"class": "received-stock"})}

    field_order = ["product", "total_qty", "quantity", "unit", "stock"]

    def __init__(self, *args, **kwargs):
        purchase_order = kwargs.pop("purchase_order")
        try:
            self.purchase_order = PurchaseOrder.objects.get(pk=purchase_order)
        except (PurchaseOrder.DoesNotExist, ValueError):
            self.purchase_order = None
        super(PurchaseOrderReceiveDetailForm, self).__init__(*args, **kwargs)
        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["total_qty"].widget.attrs["style"] = styles
        self.fields["product"].widget.attrs["style"] = styles

        self.fields["unit"].widget.attrs["class"] = "unit-dropdown"
        self.fields["unit"].empty_label = None
        self.fields["quantity"].help_text = _("Jumlah penerimaan tersisa: -")
        self.fields["quantity"].label = _("Receive Quantity")
        self.fields["unit"].queryset = UOM.objects.filter(is_active=True)
        self.fields["product"].label = _("Product")
        self.fields["unit"].label = _("Unit")

    def get_received_quantity(self, stock, unit):
        total_received = 0
        received = self.purchase_order.purchase_received.all()
        for receive in received:
            try:
                receive = receive.received_detail.get(stock=stock)
                converted_qty = uom_conversion(receive.unit, unit, receive.quantity)
                total_received += converted_qty
            except PurchaseOrderReceiveDetail.DoesNotExist:
                total_received += 0
        return total_received

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        stock = cleaned_data.get("stock")
        unit = cleaned_data.get("unit")
        if stock and self.purchase_order:
            try:
                purchase_order_detail = self.purchase_order.purchased_stock.get(
                    stock=stock
                )
            except (PurchaseOrderDetail.DoesNotExist, Exception):
                self._errors["product"] = self.error_class(
                    [_("Product does not exist in purchase order")]
                )
                self._errors["stock"] = self.error_class(
                    [_("Product/Stock does not exist in purchase order")]
                )

            if quantity:
                quantity = Decimal(quantity)
                digits = len(quantity.as_tuple().digits) - abs(
                    quantity.as_tuple().exponent
                )
                if digits > MAX_DIGITS_INVENTORY:
                    self._errors["quantity"] = self.error_class(
                        [
                            _(
                                "Maksimal digit {} sebelum koma".format(
                                    MAX_DIGITS_INVENTORY
                                )
                            )
                        ]
                    )
                else:
                    if not unit:
                        self._errors["unit"] = self.error_class(
                            [_("Satuan sudah dihapus/tidak aktif")]
                        )
                    else:
                        received_qty = self.get_received_quantity(
                            stock, purchase_order_detail.unit
                        )
                        converted_qty = uom_conversion(
                            unit, purchase_order_detail.unit, quantity
                        )
                        if purchase_order_detail.quantity < (
                            converted_qty + received_qty
                        ):
                            self._errors["quantity"] = self.error_class(
                                [
                                    _(
                                        "Jumlah penerimaan tidak bisa lebih besar dari jumlah pembelian"
                                    )
                                ]
                            )
                if quantity < 0:
                    self._errors["quantity"] = self.error_class(
                        [_("Nilai tidak bisa negatif")]
                    )
        else:
            self._errors["product"] = self.error_class([_("Product/Stock is invalid")])
            self._errors["stock"] = self.error_class([_("Product/Stock is invalid")])
        return cleaned_data

    def save(self, *args, **kwargs):
        stock = self.cleaned_data.pop("stock")
        quantity = self.cleaned_data.pop("quantity")
        unit = self.cleaned_data.pop("unit")
        received = self.cleaned_data.pop("received")

        if self.instance and self.instance.pk:
            received_detail = super().save(commit=False)
            received_detail.stock = stock
            received_detail.quantity = quantity
            received_detail.unit = unit
            received_detail.save()
        else:
            received_detail = received.receive_purchase(
                stock=stock, quantity=quantity, unit=unit
            )
        return received_detail


class PurchaseOrderReceiveDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(PurchaseOrderReceiveDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "received_order_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Received Order Detail")
        self.form_grid = "col-lg-12"
        self.add_false = True


PurchaseOrderReceiveDetailFormset = inlineformset_factory(
    PurchaseOrderReceive,
    PurchaseOrderReceiveDetail,
    form=PurchaseOrderReceiveDetailForm,
    formset=RequiredFormSet,
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=False,
)


class ReceivedOrderForm(forms.ModelForm):
    class Meta:
        model = PurchaseOrderReceive
        fields = (
            "purchase_order",
            "receipt_number",
        )
        widgets = {
            "purchase_order": autocomplete.ModelSelect2(
                url="stocks:get_purchase_order",
                attrs={"data-minimum-input-length": 0},
            )
        }

    def __init__(self, *args, **kwargs):
        super(ReceivedOrderForm, self).__init__(*args, **kwargs)
        self.fields["receipt_number"].required = True
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Receive Order"),
                KawnField("purchase_order"),
                KawnField("receipt_number"),
            )
        )

    def clean_purchase_order(self):
        purchase_order = self.cleaned_data.get("purchase_order")
        if not purchase_order:
            raise forms.ValidationError(_("This field is required"))
        if purchase_order.status in [
            PurchaseOrder.STATUS.received,
            PurchaseOrder.STATUS.closed,
        ]:
            raise forms.ValidationError(_("Purchase Order has been received/closed"))

        active_ro = PurchaseOrderReceive.objects.filter(
            purchase_order=purchase_order, received=False
        )
        if active_ro:
            raise forms.ValidationError(
                _("Terdapat penerimaan dengan PO ini yang belum diselesaikan.")
            )
        return purchase_order

    def clean_receipt_number(self):
        receipt_number = self.cleaned_data["receipt_number"]
        if not receipt_number.isalnum():
            raise forms.ValidationError(_("Format harus Alphanumeric"))
        return receipt_number


class ReceivedOrderDetailForm(forms.ModelForm):
    class Meta:
        model = PurchaseOrderReceiveDetail
        fields = (
            "quantity",
            "unit",
        )
        widgets = {
            "quantity": forms.TextInput(),
        }

    def __init__(self, *args, **kwargs):
        purchase_order = kwargs.pop("purchase_order")
        try:
            self.purchase_order = PurchaseOrder.objects.get(pk=purchase_order)
        except (PurchaseOrder.DoesNotExist, ValueError):
            self.purchase_order = None

        super(ReceivedOrderDetailForm, self).__init__(*args, **kwargs)
        self.fields["quantity"].help_text = _("Jumlah penerimaan tersisa: -")
        self.fields["quantity"].label = _("Receive Quantity")
        self.fields["unit"].empty_label = None
        self.fields["unit"].widget.attrs["class"] = "products-unit"
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnField("quantity"),
            KawnField("unit"),
        )

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["unit"].queryset = UOM.objects.filter(
                category=instance.unit.category, is_active=True
            )
            purchase_order_detail = self.purchase_order.purchased_stock.get(
                stock=self.instance.stock
            )
            received_qty = self.get_received_quantity(
                self.instance.stock, purchase_order_detail.unit
            )
            unreceived_qty = purchase_order_detail.quantity - received_qty
            self.fields["quantity"].help_text = _(
                "Jumlah penerimaan tersisa: {} {}".format(
                    get_normalized_decimal(unreceived_qty), purchase_order_detail.unit
                )
            )
            self.fields["unit"].initial = get_normalized_decimal(instance.quantity)

    def get_received_quantity(self, stock, unit):
        total_received = 0
        received = self.purchase_order.purchase_received.filter(received=True)
        for receive in received:
            try:
                receive = receive.received_detail.get(stock=stock)
                converted_qty = uom_conversion(receive.unit, unit, receive.quantity)
                total_received += converted_qty
            except PurchaseOrderReceiveDetail.DoesNotExist:
                total_received += 0
        return total_received

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        unit = cleaned_data.get("unit")
        purchase_order_detail = self.purchase_order.purchased_stock.get(
            stock=self.instance.stock
        )
        if quantity:
            quantity = Decimal(quantity)
            digits = len(quantity.as_tuple().digits) - abs(quantity.as_tuple().exponent)
            if digits > MAX_DIGITS_INVENTORY:
                self._errors["quantity"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_INVENTORY))]
                )
            else:
                received_qty = self.get_received_quantity(
                    self.instance.stock, purchase_order_detail.unit
                )
                converted_qty = uom_conversion(
                    unit, purchase_order_detail.unit, quantity
                )
                if purchase_order_detail.quantity < (converted_qty + received_qty):
                    self._errors["quantity"] = self.error_class(
                        [
                            _(
                                "Jumlah penerimaan tidak bisa lebih besar dari jumlah pembelian"
                            )
                        ]
                    )

            if quantity < 0:
                self._errors["quantity"] = self.error_class(
                    [_("Nilai tidak bisa negatif")]
                )
        return cleaned_data

    def save(self, commit=False):
        instance = super().save(commit)
        purchased = instance.received.purchase_order.purchased_stock.get(
            stock=instance.stock
        )
        purchased_unit = purchased.unit
        purchase_cost = purchased.purchase_cost.amount
        converted_cost = uom_cost_conversion(
            purchased_unit,
            instance.unit,
            instance.quantity,
            purchase_cost,
        )
        instance.purchase_cost = converted_cost
        instance.save()

        return instance


class ReceivedOrderCompleteForm(forms.ModelForm):
    class Meta:
        model = PurchaseOrderReceive
        fields = ("id",)

    def __init__(self, *args, **kwargs):
        super(ReceivedOrderCompleteForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"

    def save(self, *args, **kwargs):
        received_order = super().save(commit=False)
        received_order.received = True
        received_order.save()
        return received_order


class VendorBillForm(forms.ModelForm):
    class Meta:
        model = VendorBill
        fields = ("received_order", "bill_date", "due_date")
        widgets = {
            "received_order": autocomplete.ModelSelect2(
                url="stocks:get_received_order",
                attrs={"data-minimum-input-length": 0},
            )
        }

    def __init__(self, *args, **kwargs):
        super(VendorBillForm, self).__init__(*args, **kwargs)
        self.fields["received_order"].label = _("Received Order")
        self.fields["bill_date"].label = _("Bill Date")
        self.fields["bill_date"].widget.attrs["readonly"] = "True"
        self.fields["bill_date"].widget.attrs["class"] = "bg-white"
        self.fields["bill_date"].widget.format = "%d/%m/%Y"
        self.fields["due_date"].label = _("Due Date")
        self.fields["due_date"].widget.attrs["readonly"] = "True"
        self.fields["due_date"].widget.attrs["class"] = "bg-white"
        self.fields["due_date"].widget.format = "%d/%m/%Y"
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Vendor Bill"),
                KawnField("received_order"),
                KawnField("bill_date"),
                KawnField("due_date"),
            )
        )


class VendorBillUpdateForm(forms.ModelForm):
    received_order = forms.CharField(
        required=False, widget=forms.TextInput(attrs={"readonly": True})
    )

    class Meta:
        model = VendorBill
        fields = ("bill_date", "due_date")

    def __init__(self, *args, **kwargs):
        super(VendorBillUpdateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Vendor Bill"),
                KawnField("received_order"),
                KawnField("bill_date"),
                KawnField("due_date"),
            )
        )
        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["received_order"].initial = instance.received_order


class VendorBillDetailForm(forms.ModelForm):

    product_name = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
        label=_("Product"),
    )
    quantity = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
        label=_("Quantity"),
    )
    unit = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={"disabled": "disabled"}),
        label=_("Unit"),
    )
    purchase_cost = forms.DecimalField(
        widget=forms.TextInput,
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        required=True,
        label=_("Purchase Cost"),
    )

    class Meta:
        model = VendorBillDetail
        fields = (
            "product",
            "vendor_bill",
        )
        widgets = {"product": forms.HiddenInput()}

    field_order = ["product_name", "quantity", "unit", "purchase_cost"]

    def __init__(self, *args, **kwargs):
        super(VendorBillDetailForm, self).__init__(*args, **kwargs)
        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["product_name"].widget.attrs["style"] = styles
        self.fields["quantity"].widget.attrs["style"] = styles
        self.fields["unit"].widget.attrs["style"] = styles
        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["unit"].initial = instance.unit.unit
            self.fields["product_name"].initial = instance.product
            self.fields["quantity"].initial = get_normalized_decimal(instance.quantity)
            self.fields["purchase_cost"].initial = instance.purchase_cost.amount

    def clean_purchase_cost(self):
        purchase_cost = self.cleaned_data.get("purchase_cost")
        if not purchase_cost:
            raise forms.ValidationError(_("This field is required"))
        return purchase_cost

    def save(self, commit=False):
        product = self.cleaned_data.pop("product")
        purchase_cost = self.cleaned_data.pop("purchase_cost")
        vendor_bill = self.cleaned_data.pop("vendor_bill")

        if self.instance and self.instance.pk:
            vendor_bill_detail = super().save(commit=False)
            vendor_bill_detail.purchase_cost = purchase_cost
            vendor_bill_detail.save()
        else:
            vendor_bill_detail = vendor_bill.create_vendor_bill_detail(
                product, purchase_cost
            )
        return vendor_bill_detail


class VendorBillDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(VendorBillDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "vendor_bill_detail"
        self.legend = _("Vendor Bill")
        self.form_grid = "col-lg-12"
        self.add_false = True
        self.template = "bootstrap4/formset_inline.html"


VendorBillDetailFormset = inlineformset_factory(
    VendorBill,
    VendorBillDetail,
    form=VendorBillDetailForm,
    formset=RequiredFormSet,
    min_num=1,
    max_num=MAX_NUM_FORMS,
    extra=0,
    can_delete=False,
)


class InventoryFilterReportForm(forms.Form):
    report_type = forms.ChoiceField()
    range_date = forms.CharField()
    products = forms.ModelMultipleChoiceField(
        queryset=Product.objects.none(),
        widget=autocomplete.ModelSelect2Multiple(
            url="stocks:product_inventory_autocomplete",
            attrs={
                "data-placeholder": _("Type to search"),
                "data-minimum-input-length": 0,
            },
        ),
        required=False,
    )
    suppliers = forms.ModelMultipleChoiceField(
        queryset=Supplier.objects.none(),
        widget=autocomplete.ModelSelect2Multiple(
            url="stocks:stocks_get_supplier",
            attrs={
                "data-placeholder": _("Type to search"),
                "data-minimum-input-length": 0,
            },
        ),
        required=False,
    )
    outlets = forms.ModelMultipleChoiceField(
        queryset=Outlet.objects.none(),
        widget=autocomplete.ModelSelect2Multiple(
            url="stocks:outlet_inventory_autocomplete",
            attrs={
                "data-placeholder": _("Type to search"),
                "data-minimum-input-length": 0,
            },
        ),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.all_outlet_access = kwargs.pop("all_outlet_access")
        dashboard_permission = kwargs.pop("dashboard_permission")
        super(InventoryFilterReportForm, self).__init__(*args, **kwargs)
        self.fields["products"].queryset = Product.objects.filter(
            is_manage_stock=True, uom__isnull=False, archived=False
        )
        self.fields["suppliers"].queryset = Supplier.objects.all()
        self.fields["outlets"].queryset = Outlet.objects.all()

        if not self.all_outlet_access:
            self.fields["outlets"].required = True

        choices = []
        for permission in dashboard_permission:
            if "stock_transfer" in permission:
                choices.append(("stocks.stocktransferdetail", _("Stock Transfer")))
            if "stock_count" in permission:
                choices.append(("stocks.stockcountdetail", _("Stock Count")))
            if "stock_adjustment" in permission:
                choices.append(("stocks.stockadjustmentdetail", _("Stock Adjustment")))
            if "purchase_order" in permission:
                choices.append(("stocks.purchaseorderdetail", _("Purchase Order")))
            if "received_order" in permission:
                choices.append(
                    ("stocks.purchaseorderreceivedetail", _("Received Order"))
                )
            if "vendor_bill" in permission:
                choices.append(("stocks.vendorbilldetail", _("Vendor Bill")))

        self.fields["report_type"].choices = choices
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Inventory Report"),
                KawnField("report_type"),
                KawnField("range_date"),
                KawnField("products"),
                KawnField("suppliers"),
                KawnField("outlets"),
            )
        )

    def clean_outlets(self):
        outlets = self.cleaned_data.get("outlets")
        if not self.all_outlet_access:
            if not outlets:
                raise forms.ValidationError(_("This field is requied"))
        return outlets
