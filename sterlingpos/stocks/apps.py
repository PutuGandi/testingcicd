from django.apps import AppConfig
from django.db.models.signals import post_save


class StocksConfig(AppConfig):
    name = "sterlingpos.stocks"
    verbose_name = "Stocks"
    permission_name = "stock"

    def ready(self):

        from sterlingpos.stocks.signals import (
            update_product_stock,
            create_stock_for_product,
            create_stock_for_outlet,
            update_composite_detail_stock,
            update_addon_detail_stock,
            update_stock_transaction_tail_sum,
            update_hourly_stock_transaction_aggregate,
        )
        
        post_save.connect(update_product_stock, sender="sales.ProductOrder")
        post_save.connect(
            update_composite_detail_stock, sender="sales.ProductOrderCompositeDetail"
        )
        post_save.connect(
            update_addon_detail_stock, sender="sales.ProductOrderProductAddOnDetail"
        )
        post_save.connect(create_stock_for_product, sender="catalogue.Product")
        post_save.connect(create_stock_for_outlet, sender="outlet.Outlet")
        
        post_save.connect(
            update_hourly_stock_transaction_aggregate,
            sender="stocks.StockTransaction",
        )
