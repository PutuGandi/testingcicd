from django import template

register = template.Library()


@register.simple_tag
def get_product_quantity(obj, product):
    qty = obj.get_product_quantity(product)
    return qty
