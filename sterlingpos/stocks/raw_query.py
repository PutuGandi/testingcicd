stock_card_query = """
  SELECT stok_info.product_id as id,
        catalogue_product.sku,
        catalogue_catalogue.name as catalogue_name,
        catalogue_product.name as product_name,
        catalogue_category.name as category_name,
        catalogue_uom.unit,
        coalesce(sum(stok_info.stok_awal), 0) AS start_stock,
        coalesce(sum(stok_info.stok_masuk), 0) AS stock_in,
        coalesce(sum(stok_info.stok_operasi), 0) AS stock_out,
        coalesce(sum(stok_info.stok_produksi), 0) AS stock_productions,
        coalesce(sum(stok_info.stok_transaksi), 0) AS stock_sales,
        coalesce(sum(stok_awal), 0) + coalesce(sum(stok_masuk), 0) + coalesce(sum(stok_operasi), 0) + coalesce(sum(stok_produksi), 0) + coalesce(sum(stok_transaksi), 0) AS last_stock
  FROM (
      SELECT awal.product_id AS product_id,
            awal.stock_id,
            awal.outlet_id,
            coalesce(stok_awal, 0) AS stok_awal,
            coalesce(stok_masuk, 0) AS stok_masuk,
            coalesce(stok_operasi, 0) AS stok_operasi,
            coalesce(stok_produksi, 0) AS stok_produksi,
            coalesce(stok_transaksi, 0) AS stok_transaksi
      FROM
      (SELECT ss.product_id,
              ss.id AS stock_id,
              ss.outlet_id,
              coalesce(quantity, 0) AS stok_awal
        FROM stocks_stock AS ss
        LEFT JOIN
          (SELECT stock_id,
                  sum(quantity) AS quantity
          FROM stocks_dailystockbalancemv
          WHERE account_id = %(account_id)s
            
            AND aggregated_time < %(start_date)s
          GROUP BY 1) AS sh ON ss.id = sh.stock_id
        WHERE ss.account_id = %(account_id)s
          AND ss.deleted IS NULL) AS awal
      LEFT JOIN
      ( SELECT stock_id,
                coalesce(sum(quantity), 0) AS stok_masuk
        FROM stocks_hourlystocktransaction
        WHERE account_id = %(account_id)s
          AND quantity >= 0
          AND aggregated_time >= %(start_date)s
          AND aggregated_time <= %(end_date)s
        GROUP BY 1 ) AS masuk ON masuk.stock_id = awal.stock_id
      LEFT JOIN
      (SELECT stock_id,
              coalesce(sum(quantity), 0) AS stok_operasi
        FROM stocks_hourlystocktransaction
        WHERE account_id = %(account_id)s
          AND quantity < 0
          AND aggregated_time >= %(start_date)s
          AND aggregated_time <= %(end_date)s
          AND transaction_type != 'transaction'
          AND transaction_type != 'productions'
        GROUP BY 1 ) AS operasi ON operasi.stock_id = awal.stock_id
      LEFT JOIN
      (SELECT stock_id,
              coalesce(sum(quantity), 0) AS stok_produksi
        FROM stocks_hourlystocktransaction
        WHERE account_id = %(account_id)s
          AND quantity < 0
          AND auto_production IS FALSE
          AND aggregated_time >= %(start_date)s
          AND aggregated_time <= %(end_date)s
          AND transaction_type = 'productions'
        GROUP BY 1) AS produksi ON produksi.stock_id = awal.stock_id
      LEFT JOIN
      (SELECT stock_id,
              coalesce(sum(quantity), 0) AS stok_transaksi
        FROM stocks_hourlystocktransaction
        WHERE account_id = %(account_id)s
          AND quantity < 0
          AND aggregated_time >= %(start_date)s
          AND aggregated_time <= %(end_date)s
          AND (transaction_type = 'transaction'
              OR (transaction_type = 'productions'
                  AND auto_production IS TRUE))
        GROUP BY 1) AS transaksi ON transaksi.stock_id = awal.stock_id
  ) AS stok_info
  INNER JOIN catalogue_product ON (stok_info.product_id = catalogue_product.id)
  LEFT OUTER JOIN catalogue_catalogue ON (catalogue_product.catalogue_id = catalogue_catalogue.id
                                          AND catalogue_product.account_id = catalogue_catalogue.account_id)
  LEFT OUTER JOIN catalogue_category ON (catalogue_product.category_id = catalogue_category.id
                                        AND catalogue_product.account_id = catalogue_category.account_id)
  LEFT OUTER JOIN catalogue_uom ON (catalogue_product.uom_id = catalogue_uom.id
  AND catalogue_product.account_id = catalogue_uom.account_id)
  WHERE
    catalogue_product.account_id = %(account_id)s
"""
stock_card_outlet_filter = """
    AND stok_info.outlet_id IN %(outlet_ids)s
"""
stock_card_grouper = """
  GROUP BY stok_info.product_id,
          catalogue_product.sku,
          catalogue_catalogue.name,
          catalogue_product.name,
          catalogue_category.name,
          catalogue_uom.unit
"""
