from sterlingpos.push_notification.utils import PushNotificationBasicHandler, push_notification_adapter

from sterlingpos.stocks.models import StockTransaction, TRANSACTION_TYPE


class StockTransactionPushNotificationHandler(PushNotificationBasicHandler):

    @property
    def is_valid(self):
        not_from_transaction = self.instance.transaction_type != TRANSACTION_TYPE.transaction
        is_sellable_product = self.instance.stock.product.is_sellable

        return not_from_transaction and is_sellable_product

    def build_data_message(self, mode_name, status_code):
        data_message = super().build_data_message(mode_name, status_code)

        data_message.update({
            "title": "STOCK UPDATE",
            "object_id": self.instance.stock.product.pk,
            "outlet_id": self.instance.stock.outlet.id,
            "transaction_type": self.instance.transaction_type,
        })

        return data_message

    def get_mode_name(self, status_code):
        mode_name = "{}_{}".format(status_code, "STOCK")
        return mode_name

    def get_fcm_queryset(self):
        qs = super().get_fcm_queryset()
        qs = qs.filter(
            outlet=self.instance.stock.outlet)
        return qs

push_notification_adapter.register_handler(
    StockTransaction, StockTransactionPushNotificationHandler
)