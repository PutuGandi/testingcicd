import mimetypes
import os
import re
import json
import logging
import types

from django.template.loader import render_to_string
from django.conf import settings
from django.core import serializers
from django.apps import apps
from django.db.models import (
    Q,
    Sum,
    Value,
    DecimalField,
    BooleanField,
    Case,
    When,
    F,
    Sum,
    FilteredRelation,
    OuterRef,
)
from weasyprint import HTML, CSS
from celery import shared_task

from sterlingpos.accounts.models import Account

from sterlingpos.core.models import (
    set_current_tenant,
)

logger = logging.getLogger(__name__)


def render_to_pdf_with_header(request, template_src, context_dict={}):
    html_string = render_to_string(template_src, context_dict)
    html_pdf = HTML(
        string=html_string,
    )
    css = CSS(
        "{}{}{}".format(settings.APPS_DIR, settings.STATIC_URL, "sass/project.css")
    )
    return html_pdf.write_pdf(stylesheets=[css])


def acronym(phrases):
    acronym = ""
    for word in phrases.split():
        acronym = acronym + word[0].upper()
    return acronym


@shared_task(bind=True, max_retries=5)
def migrate_string_to_json(self, account_pk):
    try:
        account = Account.objects.get(pk=account_pk)
        set_current_tenant(account)
        for transaction in account.stocktransaction_set.all():
            try:
                reference = json.loads(transaction.reference)
                transaction.reference = reference
                transaction.save()
                try:
                    if (
                        transaction.reference[0]["model"]
                        == "productions.productionsorder"
                    ):
                        try:
                            reference = apps.get_model(
                                transaction.reference[0]["model"]
                            ).objects.get(pk=transaction.reference[0]["pk"])
                            reference = serializers.serialize("json", [reference])
                            reference = json.loads(reference)
                            transaction.reference = reference
                            transaction.save()
                        except Exception:
                            pass
                except KeyError:
                    pass
            except TypeError:
                pass
        set_current_tenant(None)
    except Exception as e:
        logged_data = "Unable to process migration: {}".format(e)
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


def migrate_stock_transaction_reference():
    for account in Account.objects.all():
        migrate_string_to_json.delay(account.pk)


@shared_task(bind=True, max_retries=5)
def fix_numbering_vendor_bill_code(self, account_pk):
    try:
        account = Account.objects.get(pk=account_pk)
        set_current_tenant(account)
        years_months = account.vendorbill_set.values(
            "bill_date__year", "bill_date__month"
        ).distinct()
        for year_month in years_months:
            vendorbills = account.vendorbill_set.filter(
                bill_date__year=year_month["bill_date__year"],
                bill_date__month=year_month["bill_date__month"],
            ).order_by("-created")
            count_bills = vendorbills.count()
            for vendorbill in vendorbills:
                count = "{0:0=4d}".format(count_bills)
                if (
                    account.inventorysetting_set.first().inventory_code_format
                    == "comprehensive"
                ):
                    code = vendorbill.code.split("/")
                    new_code = vendorbill.code.replace(code[-1], count)
                else:
                    new_code = "VB-{}".format(count)
                vendorbill.code = new_code
                vendorbill.save()
                count_bills -= 1
    except Exception as e:
        logged_data = "Unable to update vendor bill code: {}".format(e)
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


def fix_vendor_bill_code():
    for account in Account.objects.all():
        fix_numbering_vendor_bill_code.delay(account.pk)
