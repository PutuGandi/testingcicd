import datetime
import pytz
from dateutil.relativedelta import relativedelta, MO, SU

from django.utils import timezone
from django.conf import settings

TIMEZONE = getattr(settings, "TIME_ZONE", "")

DATE_MAP = {
    "today": {
        "start_date": relativedelta(hour=0, minute=0, second=0),
        "end_date": relativedelta(hour=23, minute=59),
    },
    "yesterday": {
        "start_date": relativedelta(days=-1, hour=0, minute=0, second=0),
        "end_date": relativedelta(hour=23, minute=59),
    },
    "last_week": {
        "start_date": relativedelta(weekday=MO(-2), hour=0, minute=0, second=0),
        "end_date": relativedelta(weekday=SU(1), hour=23, minute=59),
    },
    "last_month": {
        "start_date": relativedelta(day=1, months=-1, hour=0, minute=0, second=0),
        "end_date": relativedelta(day=1, months=+1, days=-1, hour=23, minute=59),
    },
    "last_year": {
        "start_date": relativedelta(
            years=-1, month=1, day=1, hour=0, minute=0, second=0
        ),
        "end_date": relativedelta(years=+1, days=-1, hour=23, minute=59),
    },
    "week_to_date": {
        "start_date": relativedelta(weekday=MO(-1), hour=0, minute=0, second=0),
        "end_date": relativedelta(hour=23, minute=59),
    },
    "month_to_date": {
        "start_date": relativedelta(day=1, hour=0, minute=0, second=0),
        "end_date": relativedelta(hour=23, minute=59),
    },
    "year_to_date": {
        "start_date": relativedelta(month=1, day=1, hour=0, minute=0, second=0),
        "end_date": relativedelta(hour=23, minute=59),
    },
}


def get_date_range_from_date_filter(date_filter):
    if date_filter not in DATE_MAP:
        return None, None

    start_date = timezone.localtime() + DATE_MAP[date_filter]["start_date"]
    if date_filter.endswith("_to_date"):
        end_date = timezone.localtime() + DATE_MAP[date_filter]["end_date"]
    else:
        end_date = start_date + DATE_MAP[date_filter]["end_date"]
    return start_date, end_date


def format_date(date_string):
    try:
        current_timezone = pytz.timezone(TIMEZONE)
        the_date = current_timezone.localize(
            datetime.datetime.strptime(date_string, "%Y-%m-%d %H:%M:%S"))

    except (TypeError, ValueError) as e:
        the_date = None

    return the_date


def get_date_range_from_custom_range(raw_start_date, raw_end_date):
    start_date = format_date(raw_start_date)
    end_date = format_date(raw_end_date)

    return start_date, end_date
