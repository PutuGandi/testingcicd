from datetime import datetime, time, timedelta
from decimal import Decimal, ROUND_DOWN
from dateutil.relativedelta import relativedelta

from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, View
from django.http import JsonResponse

from sterlingpos.outlet.models import Outlet
from sterlingpos.dashboard.utils import (
    get_date_range_from_date_filter,
    get_date_range_from_custom_range,
)

from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.sales.models import SalesOrder, ProductOrder


class DashboardMixin(object):
    @property
    def has_date_filter(self):
        return True if self.request.GET.get("date", None) else False

    @property
    def has_custom_date_range(self):
        return (
            True
            if self.request.GET.get("startdate", None)
            or self.request.GET.get("enddate", None)
            else None
        )

    def get_outlets(self):
        outlet_pk = self.kwargs.get("branch_pk", None)

        if self.request.user.is_owner and not outlet_pk:
            return None
       
        outlet_qs = get_outlet_of_user(self.request.user)

        if outlet_pk:
            outlet = get_object_or_404(outlet_qs, pk=outlet_pk)
            return [outlet.pk]

        return list(outlet_qs.values_list("id", flat=True))

    def get_date_range(self):
        start_date, end_date = None, None

        if self.has_date_filter:
            date_filter = self.request.GET.get("date")
            start_date, end_date = get_date_range_from_date_filter(date_filter)
        elif self.has_custom_date_range:
            raw_start_date = self.request.GET.get("startdate", None)
            raw_end_date = self.request.GET.get("enddate", None)
            start_date, end_date = get_date_range_from_custom_range(
                raw_start_date, raw_end_date
            )

        if not start_date:
            start_date = datetime.combine(datetime.today(), time.min)

        if not end_date:
            end_date = datetime.combine(datetime.today(), time.max)

        return start_date, end_date


class DashboardIndexView(DashboardMixin, TemplateView):
    template_name = "dashboard/dashboard.html"

    def get_percentage_delta(self, sales_summary, previous_sales_summary):
        result = {}

        for item in ["count", "amount", "average"]:
            if not previous_sales_summary[item]:
                diff_percentage = 0
            else:
                diff_percentage = (
                    (sales_summary[item] - previous_sales_summary[item])
                    / Decimal(previous_sales_summary[item])
                ) * 100
                diff_percentage = diff_percentage.quantize(
                    Decimal(".01"), rounding=ROUND_DOWN
                )

            result[item] = diff_percentage

        return result

    def get_context_data(self, **kwargs):
        context = super(DashboardIndexView, self).get_context_data(**kwargs)

        # Parameters
        outlets = self.get_outlets()

        # Get time Difference
        try:
            start_date, end_date = self.get_date_range()
            time_delta = end_date - (start_date - timedelta(days=1))
            previous_start_date = start_date - timedelta(days=time_delta.days)
        except (TypeError, OverflowError):
            start_date = datetime.combine(datetime.today(), time.min)
            end_date = datetime.combine(datetime.today(), time.max)
            time_delta = end_date - (start_date - timedelta(days=1))
            previous_start_date = start_date - timedelta(days=time_delta.days)

        # Sales order summary
        sales_summary = SalesOrder.objects.get_sales_summary(
            start_date, end_date, outlets
        )
        previous_sales_summary = SalesOrder.objects.get_sales_summary(
            previous_start_date, start_date, outlets
        )
        sales_summary_diff = self.get_percentage_delta(
            sales_summary, previous_sales_summary
        )

        context.update(
            {
                "sales_summary": sales_summary,
                "sales_summary_diff": sales_summary_diff,
                "previous_sales_summary": previous_sales_summary,
                "start_date": start_date,
                "end_date": end_date,
            }
        )

        return context


class DashboardSalesSummaryGroupView(SterlingRoleMixin, DashboardMixin, View):
    GROUP_BY_MONTH = "month"
    GROUP_BY_DATE = "date"
    GROUP_BY_HOUR = "hour"

    def get_series(self, start_date, end_date, group_by):
        relative_delta_format = {
            self.GROUP_BY_MONTH: dict(months=1),
            self.GROUP_BY_DATE: dict(days=1),
            self.GROUP_BY_HOUR: dict(hours=1)
        }

        the_delta = relative_delta_format.get(group_by, dict(hours=1))
        td = relativedelta(**the_delta)
        time_series = [start_date]
        start_date = start_date + td
        while start_date < end_date:
            time_series.append(start_date)
            start_date = start_date + td

        time_series.append(end_date)

        return time_series

    def format_date(self, date, group_by):
        date_format = {
            self.GROUP_BY_MONTH: "%b %Y",
            self.GROUP_BY_DATE: "%b %d, %Y",
            self.GROUP_BY_HOUR: "%H:%M"
        }

        the_format = date_format.get(group_by, "%H:%M")
        return date.strftime(the_format)

    def prepare_data_for_template(self, sales_summary_grouped_by_time_unit, group_by):
        sales_summary = list(sales_summary_grouped_by_time_unit)
        if not sales_summary:
            return []

        start_date = sales_summary[0]["time_unit"]
        end_date = sales_summary[-1]["time_unit"]

        # Generate series from this start_date and end_date based on group_by
        series = self.get_series(start_date, end_date, group_by)

        # Merge series with data and format date
        left = 0
        right = 0
        result = []

        while right < len(sales_summary) and left < len(series):
            if series[left] == sales_summary[right]["time_unit"]:
                result.append(
                    {
                        "date": self.format_date(sales_summary[right]["time_unit"], group_by),
                        "count": sales_summary[right]["count"],
                        "amount": int(sales_summary[right]["amount"]),
                    }
                )
                right += 1
            elif series[left] < sales_summary[right]["time_unit"]:
                result.append(
                    {
                        "date": self.format_date(series[left], group_by),
                        "count": 0,
                        "amount": 0,
                    }
                )
            left += 1

        return result

    def get_group_by(self, start_date, end_date):
        if relativedelta(end_date, start_date).months >= 1:
            return self.GROUP_BY_MONTH
        elif relativedelta(end_date, start_date).days >= 1:
            return self.GROUP_BY_DATE
        else:
            return self.GROUP_BY_HOUR

    def get(self, request, *args, **kwargs):
        # Parameters
        outlets = self.get_outlets()
        start_date, end_date = self.get_date_range()

        # Sales order grouped by time unit (day, month or year)
        group_by = self.get_group_by(start_date, end_date)
        sales_summary_grouped_by_time_unit = SalesOrder.objects.get_sales_summary_grouped_by_time(
            start_date, end_date, group_by, outlets
        )
        sales_data = self.prepare_data_for_template(
            sales_summary_grouped_by_time_unit, group_by
        )
        json_data = {"sales_data": sales_data}

        return JsonResponse(json_data, safe=False)


class DashboardChartView(SterlingRoleMixin, DashboardMixin, View):
    MAX_GROUPING = 5

    def prepare_data_for_template(self, sales_data):
        sales_data = list(sales_data)
        by_count = sorted(sales_data, key=lambda x: x["count"], reverse=True)[
            : self.MAX_GROUPING
        ]
        by_amount = sorted(sales_data, key=lambda x: x["amount"], reverse=True)[
            : self.MAX_GROUPING
        ]

        return {"by_count": by_count, "by_amount": by_amount}

    def get(self, request, *args, **kwargs):
        # Parameters
        outlets = self.get_outlets()
        start_date, end_date = self.get_date_range()

        # Product Group
        product_sales_summary = ProductOrder.objects.get_sales_summary(
            start_date, end_date, outlets
        )
        product_sales_data = self.prepare_data_for_template(product_sales_summary)
        json_data = {"product_sales_data": product_sales_data}

        return JsonResponse(json_data, safe=False)


class DashboardCategoryChartView(SterlingRoleMixin, DashboardMixin, View):
    def prepare_data_for_template(self, sales_data):
        sales_data = list(sales_data)
        by_count = sorted(sales_data, key=lambda x: x["count"], reverse=True)
        by_amount = sorted(sales_data, key=lambda x: x["amount"], reverse=True)

        return {"by_count": by_count, "by_amount": by_amount}

    def get(self, request, *args, **kwargs):
        outlets = self.get_outlets()
        start_date, end_date = self.get_date_range()

        category_sales_summary = ProductOrder.objects.get_sales_summary_by_category(
            start_date, end_date, outlets
        )
        category_sales_data = self.prepare_data_for_template(category_sales_summary)
        json_data = {"category_sales_data": category_sales_data}

        return JsonResponse(json_data, safe=False)
