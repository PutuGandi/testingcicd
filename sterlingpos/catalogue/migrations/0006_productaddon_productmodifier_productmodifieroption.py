# Generated by Django 2.0.4 on 2018-07-24 05:38

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import sterlingpos.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20180504_0849'),
        ('catalogue', '0005_auto_20180719_0532'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductAddOn',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('product', sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='addon_usage', to='catalogue.Product')),
                ('product_usage', sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='addon', to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'ProductAddOn',
                'verbose_name_plural': 'CompositeProducts',
            },
        ),
        migrations.CreateModel(
            name='ProductModifier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(max_length=50, verbose_name='Modifier Name')),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('product', sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='modifier', to='catalogue.Product')),
            ],
            options={
                'verbose_name': 'ProductModifier',
                'verbose_name_plural': 'ProductModifiers',
            },
        ),
        migrations.CreateModel(
            name='ProductModifierOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('value', models.CharField(max_length=50, verbose_name='Modifier Option')),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('product_modifier', sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='options', to='catalogue.ProductModifier')),
            ],
            options={
                'verbose_name': 'ProductAddOn',
                'verbose_name_plural': 'CompositeProducts',
            },
        ),
    ]
