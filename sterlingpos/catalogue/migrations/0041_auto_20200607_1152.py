# Generated by Django 2.2.9 on 2020-06-07 11:52

from decimal import Decimal
from django.db import migrations
import djmoney.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0040_auto_20200605_1027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='moving_average_cost',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=19, verbose_name='Moving Average Cost'),
        ),
    ]
