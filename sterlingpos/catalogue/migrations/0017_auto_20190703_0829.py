# Generated by Django 2.0.4 on 2019-07-03 08:29

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import sterlingpos.catalogue.models
import sterlingpos.catalogue.storage
import sterlingpos.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_account_registered_via'),
        ('catalogue', '0016_auto_20190117_0454'),
    ]

    operations = [
        migrations.CreateModel(
            name='Catalogue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(max_length=120, verbose_name='Name')),
                ('image_location', models.ImageField(blank=True, max_length=255, null=True, storage=sterlingpos.catalogue.storage.OverwriteStorage(), upload_to=sterlingpos.catalogue.models.get_catalogue_upload_path)),
                ('is_active', models.BooleanField(default=True)),
                ('is_sellable', models.BooleanField(default=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('category', sterlingpos.core.models.SterlingTenantForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='catalogue.Category')),
            ],
            options={
                'verbose_name': 'Catalogue',
                'verbose_name_plural': 'Catalogues',
            },
        ),
        migrations.AddField(
            model_name='product',
            name='catalogue',
            field=sterlingpos.core.models.SterlingTenantForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='variants', to='catalogue.Catalogue'),
        ),
        migrations.AlterUniqueTogether(
            name='Catalogue',
            unique_together={('account', 'id')},
        ),
    ]
