from django.apps import AppConfig
from django.db.models.signals import post_save


class CatalogueConfig(AppConfig):
    name = "sterlingpos.catalogue"
    verbose_name = "Catalogues"

    def ready(self):
        from sterlingpos.catalogue.signals import (
            create_product_availability,
            create_product_pricing
        )
        from sterlingpos.catalogue.push_notification import ProductPushNotificationHandler

        post_save.connect(create_product_availability, sender="catalogue.Product")
        post_save.connect(create_product_pricing, sender="catalogue.Product")
