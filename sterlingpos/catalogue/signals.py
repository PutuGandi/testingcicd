from django.conf import settings
from django.dispatch import Signal

from sterlingpos.catalogue.models import (
    ProductAvailability, ProductPricing
)


def create_product_availability(sender, **kwargs):
    product = kwargs.get('instance')
    created = kwargs.get('created')
    if created:
        product_availabilities = [
            ProductAvailability(
                product=product,
                outlet=outlet,
                account=product.account,
            )
            for outlet in product.account.outlet_set.all()
        ]
        ProductAvailability.objects.bulk_create(
            product_availabilities)


def create_product_pricing(sender, **kwargs):
    product = kwargs.get('instance')
    created = kwargs.get('created')
    if created:
        product_prices = [
            ProductPricing(
                product=product,
                outlet=outlet,
                account=product.account,
                price=product.price,
            )
            for outlet in product.account.outlet_set.all()
        ]
        ProductPricing.objects.bulk_create(product_prices)
