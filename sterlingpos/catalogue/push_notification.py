from sterlingpos.push_notification.utils import PushNotificationBasicHandler, push_notification_adapter

from sterlingpos.catalogue.models import (
    Product,
    ProductPricing,
    ProductAvailability,
)


class ProductPushNotificationHandler(PushNotificationBasicHandler):

    def get_mode_name(self, status_code):
        mode_name = super().get_mode_name(status_code)
        mode_name = "{}_{}".format(
            mode_name, self.instance.classification.upper())

        return mode_name


class ProductPricingAvailabilityPushNotificationHandler(ProductPushNotificationHandler):

    def get_mode_name(self, status_code):
        mode_name = super().get_mode_name(status_code)
        mode_name = "{}_{}".format(
            mode_name, self.instance.product.classification.upper())

        return mode_name

    def get_fcm_queryset(self):
        qs = super().get_fcm_queryset()
        qs = qs.filter(
            outlet=self.instance.outlet)
        return qs


push_notification_adapter.register_handler(
    Product, ProductPushNotificationHandler
)
push_notification_adapter.register_handler(
    ProductPricing, ProductPricingAvailabilityPushNotificationHandler
)
push_notification_adapter.register_handler(
    ProductAvailability, ProductPricingAvailabilityPushNotificationHandler
)
