from django.contrib import admin
from django.db.models import CharField, F, Value as V
from django.db.models.functions import Concat
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from treebeard.admin import TreeAdmin

from treebeard.forms import MoveNodeForm, movenodeform_factory

from import_export import resources
from import_export.fields import Field
from import_export.widgets import (
    ManyToManyWidget,
    ForeignKeyWidget,
    DecimalWidget,
    BooleanWidget,
)

from sterlingpos.push_notification.signals import push_update_signal
from sterlingpos.core.models import get_current_tenant
from sterlingpos.core.import_export import (
    CustomManyToManyWidget,
    CustomForeignKeyWidget,
    RequiredCharWidget,
    CustomDecimalWidget,
)

from .models import (
    UOM,
    ProductType,
    Category,
    Catalogue,
    Product,
    CompositeProduct,
    ProductModifier,
    ProductModifierOption,
    ProductModifierUsage,
    ProductAddOn,
)
from .categories import create_from_breadcrumbs
from sterlingpos.productions.models import BOM

from sterlingpos.push_notification.signals import resource_import


def prepare_catalogue(value, row):
    if not value:
        raise ValueError(_("Enter a valid catalogue name."))
    is_sellable = row["is_sellable"]
    if is_sellable and is_sellable.lower() == "true":
        is_sellable = True
    else:
        is_sellable = False
    data = {
        "name": value,
        "category": create_from_breadcrumbs(row["category"]),
        "is_sellable": is_sellable,
    }
    obj, _ = Catalogue.objects.get_or_create(**data)
    return obj


def prepare_catalogue_manufactured(value, row):
    if not value:
        raise ValueError(_("Enter a valid catalogue name."))
    is_sellable = row["is_sellable"]
    if is_sellable and is_sellable.lower() == "true":
        is_sellable = True
    else:
        is_sellable = False

    try:
        uom = UOM.objects.get(unit__iexact=row["uom"])
    except UOM.DoesNotExist:
        raise ValueError(_("UOM {} does not exist".format(row["uom"])))

    data = {
        "name": value,
        "category": create_from_breadcrumbs(row["category"]),
        "is_sellable": is_sellable,
    }
    obj, created = Catalogue.objects.get_or_create(**data)
    if not created:
        if obj.uom.category != uom.category:
            raise ValueError(
                _(
                    "Satuan: {} tidak sesuai dengan ketegori satuan katalog: {}".format(
                        uom.unit, obj.uom.unit
                    )
                )
            )
    else:
        obj.is_manage_stock = True
        obj.uom = uom
        obj.classification = Catalogue.PRODUCT_CLASSIFICATION.manufactured
        obj.save()
    return obj


def validate_uom(value, row):
    if not value:
        raise ValueError(_("Enter a valid UOM."))
    try:
        uom = UOM.objects.get(unit__iexact=row["uom"])
    except UOM.DoesNotExist:
        raise ValueError(_("UOM {} does not exist".format(row["uom"])))
    return uom


def prepare_category(value, row):
    if not value:
        raise ValueError(_("Enter a valid category name."))
    obj = create_from_breadcrumbs(value)
    return obj


class ProductResource(resources.ModelResource):

    sku = Field(column_name="sku", attribute="sku", widget=RequiredCharWidget(),)

    name = Field(column_name="name", attribute="name", widget=RequiredCharWidget(),)

    catalogue = Field(
        column_name="catalogue",
        attribute="catalogue",
        widget=CustomForeignKeyWidget(
            Catalogue, "name", create_function=prepare_catalogue,
        ),
    )

    category = Field(
        column_name="category",
        attribute="category",
        widget=CustomForeignKeyWidget(
            Category, "name", create_function=prepare_category,

        ),
    )

    price = Field(
        column_name="price",
        attribute="price__amount",
        default=0,
        widget=CustomDecimalWidget(),
    )

    cost = Field(
        column_name="cost",
        attribute="cost__amount",
        default=0,
        widget=CustomDecimalWidget(),
    )

    is_sellable = Field(
        column_name="is_sellable",
        attribute="is_sellable",
        widget=BooleanWidget(),
        default=True,
    )

    class Meta:
        model = Product
        import_id_fields = ("sku",)
        fields = (
            "sku",
            "catalogue",
            "name",
            "category",
            "price",
            "cost",
            "is_sellable",
        )
        exclude = ("account",)
        clean_model_instances = True

    def init_instance(self, row=None):
        instance = super().init_instance(row)
        instance.account = get_current_tenant()
        return instance

    def get_instance(self, instance_loader, row):
        try:
            instance = super().get_instance(instance_loader, row)
            return instance
        except Exception as e:
            raise ValueError(
                _("There are more than 2 existing Product under this SKU.")
            )

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        post_save.disconnect(push_update_signal, sender=Category)
        post_save.disconnect(push_update_signal, sender=Product)
        post_save.disconnect(push_update_signal, sender=Catalogue)

    def after_import(self, dataset, result, using_transactions, dry_run, **kwargs):
        post_save.connect(push_update_signal, sender=Category)
        post_save.connect(push_update_signal, sender=Product)
        post_save.connect(push_update_signal, sender=Catalogue)

        # Trigger manual updates
        product = Product.objects.order_by("modified").first()
        if product:
            product.save()
            resource_import.send(
                sender=Product,
                instance=product)


class ExportProductResource(ProductResource):
    def dehydrate_catalogue(self, obj):
        return obj.catalogue.name

    def dehydrate_category(self, obj):
        return obj.catalogue.category.full_name


class ManufacturedProductResource(resources.ModelResource):
    sku = Field(column_name="sku", attribute="sku", widget=RequiredCharWidget(),)
    name = Field(column_name="name", attribute="name", widget=RequiredCharWidget(),)
    catalogue = Field(
        column_name="catalogue",
        attribute="catalogue",
        widget=CustomForeignKeyWidget(
            Catalogue, "name", custom_function=prepare_catalogue_manufactured,
        ),
    )
    category = Field(
        column_name="category",
        attribute="category",
        widget=CustomForeignKeyWidget(
            Category, "name", create_function=prepare_category,
        ),
    )
    price = Field(
        column_name="price",
        attribute="price__amount",
        default=0,
        widget=CustomDecimalWidget(),
    )
    cost = Field(
        column_name="cost",
        attribute="cost__amount",
        default=0,
        widget=CustomDecimalWidget(),
    )
    is_sellable = Field(
        column_name="is_sellable",
        attribute="is_sellable",
        widget=BooleanWidget(),
        default=True,
    )
    uom = Field(
        column_name="uom",
        attribute="uom",
        widget=CustomForeignKeyWidget(UOM, "unit", custom_function=validate_uom),
    )

    class Meta:
        model = Product
        import_id_fields = ("sku",)
        fields = (
            "sku",
            "catalogue",
            "name",
            "category",
            "price",
            "cost",
            "uom",
            "is_sellable",
        )
        exclude = ("account",)
        clean_model_instances = True

    def init_instance(self, row=None):
        instance = super().init_instance(row)
        instance.account = get_current_tenant()
        instance.is_manage_stock = True
        instance.classification = Product.PRODUCT_CLASSIFICATION.manufactured
        return instance

    def save_instance(self, instance, using_transactions, dry_run, **kwargs):
        super().save_instance(instance, using_transactions, dry_run, **kwargs)
        instance.is_manage_stock = True
        instance.classification = Product.PRODUCT_CLASSIFICATION.manufactured
        instance.save()

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        post_save.disconnect(push_update_signal, sender=Category)
        post_save.disconnect(push_update_signal, sender=Product)
        post_save.disconnect(push_update_signal, sender=Catalogue)

    def after_import(self, dataset, result, using_transactions, dry_run, **kwargs):
        post_save.connect(push_update_signal, sender=Category)
        post_save.connect(push_update_signal, sender=Product)
        post_save.connect(push_update_signal, sender=Catalogue)

        # Trigger manual updates
        product = Product.objects.order_by("modified").first()
        if product:
            product.save()
            resource_import.send(
                sender=Product,
                instance=product)

class ExportManufacturedProductResource(ManufacturedProductResource):
    def dehydrate_catalogue(self, obj):
        return obj.catalogue.name

    def dehydrate_category(self, obj):
        return obj.catalogue.category.full_name

    def dehydrate_uom(self, obj):
        return obj.uom.unit
