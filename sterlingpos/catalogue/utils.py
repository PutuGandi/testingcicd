import math
import decimal

from sterlingpos.catalogue.models import UOM
from decimal import Decimal, DivisionByZero


DEFAULT_UOM = [
    {
        "unit": "kg",
        "category": UOM.UOM_CATEGORY.weight,
        "uom_type": UOM.UOM_TYPE.base,
        "ratio": 1.0,
    },
    {
        "unit": "g",
        "category": UOM.UOM_CATEGORY.weight,
        "uom_type": UOM.UOM_TYPE.smaller,
        "ratio": 1000.0,
    },
    {
        "unit": "ons",
        "category": UOM.UOM_CATEGORY.weight,
        "uom_type": UOM.UOM_TYPE.smaller,
        "ratio": 35.274,
    },
    {
        "unit": "lbs",
        "category": UOM.UOM_CATEGORY.weight,
        "uom_type": UOM.UOM_TYPE.smaller,
        "ratio": 2.20462,
    },
    {
        "unit": "ton",
        "category": UOM.UOM_CATEGORY.weight,
        "uom_type": UOM.UOM_TYPE.bigger,
        "ratio": 1000.0,
    },
    {
        "unit": "Liter",
        "category": UOM.UOM_CATEGORY.volume,
        "uom_type": UOM.UOM_TYPE.base,
        "ratio": 1.0,
    },
    {
        "unit": "ml",
        "category": UOM.UOM_CATEGORY.volume,
        "uom_type": UOM.UOM_TYPE.smaller,
        "ratio": 1000.0,
    },
    {
        "unit": "cc",
        "category": UOM.UOM_CATEGORY.volume,
        "uom_type": UOM.UOM_TYPE.smaller,
        "ratio": 1000.0,
    },
    {
        "unit": "gals",
        "category": UOM.UOM_CATEGORY.volume,
        "uom_type": UOM.UOM_TYPE.bigger,
        "ratio": 3.78541,
    },
    {
        "unit": "pcs",
        "category": UOM.UOM_CATEGORY.units,
        "uom_type": UOM.UOM_TYPE.base,
        "ratio": 1.0,
    },
]


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    result = math.floor(n * Decimal(multiplier) + Decimal(0.5)) / Decimal(multiplier)
    return result


def calculate_actual_ratio(unit_ratio):
    ratio_temp = Decimal(1) / unit_ratio
    try:
        ratio = ratio_temp.quantize(Decimal(".001"))
        ratio = Decimal(1) / ratio
    except DivisionByZero:
        ratio = Decimal(1) / ratio_temp
    return ratio


def conversion_up(unit, quantity):
    ratio = unit.ratio
    if not unit.category == UOM.UOM_CATEGORY.units:
        ratio = calculate_actual_ratio(unit.ratio)
    ratio = Decimal(1) / ratio
    result = Decimal(ratio) * Decimal(quantity)
    return result


def conversion_down(unit, quantity):
    ratio = unit.ratio
    if not unit.category == UOM.UOM_CATEGORY.units:
        ratio = calculate_actual_ratio(unit.ratio)
    ratio = ratio / Decimal(1)
    result = Decimal(ratio) * Decimal(quantity)
    return result


def uom_conversion(unit_from, unit_to, quantity):
    converted = 0
    if unit_from.uom_type == UOM.UOM_TYPE.base:
        if unit_to.uom_type == UOM.UOM_TYPE.base:
            converted = Decimal(quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.smaller:
            converted = conversion_down(unit_to, quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.bigger:
            converted = conversion_up(unit_to, quantity)
    elif unit_from.uom_type == UOM.UOM_TYPE.bigger:
        converted_quantity = conversion_down(unit_from, quantity)
        if unit_to.uom_type == UOM.UOM_TYPE.base:
            converted = converted_quantity
        elif unit_to.uom_type == UOM.UOM_TYPE.smaller:
            converted = conversion_down(unit_to, converted_quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.bigger:
            converted = conversion_up(unit_to, converted_quantity)
    elif unit_from.uom_type == UOM.UOM_TYPE.smaller:
        converted_quantity = conversion_up(unit_from, quantity)
        if unit_to.uom_type == UOM.UOM_TYPE.base:
            converted = converted_quantity
        elif unit_to.uom_type == UOM.UOM_TYPE.bigger:
            converted = conversion_up(unit_to, converted_quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.smaller:
            converted = conversion_down(unit_to, converted_quantity)
    return get_normalized_decimal(converted)


def uom_conversion_without_rounding(unit_from, unit_to, quantity):
    converted = 0
    if unit_from.uom_type == UOM.UOM_TYPE.base:
        if unit_to.uom_type == UOM.UOM_TYPE.base:
            converted = Decimal(quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.smaller:
            converted = conversion_down(unit_to, quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.bigger:
            converted = conversion_up(unit_to, quantity)
    elif unit_from.uom_type == UOM.UOM_TYPE.bigger:
        converted_quantity = conversion_down(unit_from, quantity)
        if unit_to.uom_type == UOM.UOM_TYPE.base:
            converted = converted_quantity
        elif unit_to.uom_type == UOM.UOM_TYPE.smaller:
            converted = conversion_down(unit_to, converted_quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.bigger:
            converted = conversion_up(unit_to, converted_quantity)
    elif unit_from.uom_type == UOM.UOM_TYPE.smaller:
        converted_quantity = conversion_up(unit_from, quantity)
        if unit_to.uom_type == UOM.UOM_TYPE.base:
            converted = converted_quantity
        elif unit_to.uom_type == UOM.UOM_TYPE.bigger:
            converted = conversion_up(unit_to, converted_quantity)
        elif unit_to.uom_type == UOM.UOM_TYPE.smaller:
            converted = conversion_down(unit_to, converted_quantity)
    return converted


def uom_cost_conversion(unit_from, unit_to, quantity, cost):
    total_cost = quantity * cost

    converted_quantity = uom_conversion(unit_from, unit_to, quantity)
    converted_quantity = get_normalized_decimal(converted_quantity)
    if not converted_quantity:
        return Decimal(0)
    converted_cost = total_cost / converted_quantity
    converted_cost = get_normalized_decimal(converted_cost)
    return converted_cost


def get_normalized_decimal(number):
    if not isinstance(number, Decimal):
        number = Decimal(number)
    frac = number % 1
    if frac == Decimal("0"):
        return Decimal(number.quantize(Decimal("1")))
    else:
        number = Decimal(
            number.quantize(Decimal(".001"), rounding=decimal.ROUND_HALF_UP)
        )
        return number.normalize()
