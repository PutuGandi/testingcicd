import logging

from django.apps import apps
from django.db.models import Sum, Value
from django.db.models.functions import Coalesce

from celery import shared_task
from decimal import Decimal
from datetime import datetime

from sterlingpos.core.models import get_current_tenant, set_current_tenant
from sterlingpos.accounts.models import Account
from sterlingpos.catalogue.utils import uom_conversion, uom_cost_conversion

from sterlingpos.stocks.tasks import populate_stock_transaction_aggregated_for_account
from sterlingpos.catalogue.models import Product

logger = logging.getLogger(__name__)


def convert_transactions_task(variant, uom):
    #TODO: Refactor to use database windows function 
    for stock in variant.stocks.all():
        for trans in stock.transactions.all():
            quantity = trans.quantity
            tail_sum = trans.tail_sum
            if uom != trans.unit:
                quantity = uom_conversion(trans.unit, uom, trans.quantity)
                if tail_sum:
                    tail_sum = uom_conversion(trans.unit, uom, tail_sum)
            trans.quantity = quantity
            trans.tail_sum = tail_sum
            trans.unit = uom
            trans.save()
        populate_stock_transaction_aggregated_for_account(
            stock.account,
            stock.created.date(),
            datetime.today().replace(
                hour=23, minute=59, second=59, microsecond=59
            ),
            stock.pk,
        )


def convert_batches_task(variant, is_batch_tracked, uom):
    if is_batch_tracked:
        for batch in variant.batches.all():
            for item in batch.batch_items.all():
                for history in item.histories.all():
                    quantity = history.quantity
                    if uom != history.unit:
                        quantity = uom_conversion(history.unit, uom, history.quantity)
                    history.quantity = quantity
                    history.unit = uom
                    history.save()


@shared_task(bind=True, max_retries=5)
def update_catalogue_variant_task(
    self,
    catalogue_id,
    uom_id,
    current_uom_id,
    is_manage_stock,
    is_batch_tracked,
    category_id,
    account_id,
):
    try:
        account = Account.objects.get(pk=account_id)
        set_current_tenant(account)
        try:
            Catalogue = apps.get_model("catalogue.Catalogue")
            catalogue = Catalogue.objects.get(pk=catalogue_id)
            UOM = apps.get_model("catalogue.UOM")
            try:
                uom = UOM.objects.get(pk=uom_id)
            except UOM.DoesNotExist:
                uom = None
            for variant in catalogue.variants.all():
                if variant.is_manage_stock and variant.uom:
                    if current_uom_id != uom_id:
                        convert_transactions_task(variant, uom)
                        convert_batches_task(variant, is_batch_tracked, uom)
        except Catalogue.DoesNotExist:
            pass
        set_current_tenant(None)
    except Exception as e:
        print(e)
        logged_data = "Unable to update catalogue variant {} : {}".format(
            get_current_tenant(), e
        )
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def update_variant_task(
    self,
    variant_id,
    uom_id,
    current_uom_id,
    is_manage_stock,
    is_batch_tracked,
    account_id,
):
    try:
        account = Account.objects.get(pk=account_id)
        set_current_tenant(account)
        try:
            Product = apps.get_model("catalogue.Product")
            variant = Product.objects.get(pk=variant_id)
            UOM = apps.get_model("catalogue.UOM")
            try:
                uom = UOM.objects.get(pk=uom_id)
            except UOM.DoesNotExist:
                uom = None
            if is_manage_stock and uom:
                if current_uom_id != uom_id:
                    convert_transactions_task(variant, uom)
                    convert_batches_task(variant, is_batch_tracked, uom)
        except Product.DoesNotExist:
            pass
        set_current_tenant(None)
    except Exception as e:
        logged_data = "Unable to update variant {} : {}".format(get_current_tenant(), e)
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def calculate_avarage_cost(self, product_id, quantity, unit_id, cost, account_id):
    try:
        account = Account.objects.get(pk=account_id)
        set_current_tenant(account)
        try:
            product = account.product_set.get(pk=product_id)
            unit = account.uom_set.get(pk=unit_id)
            current_balance = 0
            for stock in product.stocks.all():
                if stock.get_last_tail_sum:
                    current_balance += stock.get_last_tail_sum

            cost_by_uom = uom_cost_conversion(
                unit,
                product.uom,
                Decimal(quantity),
                Decimal(cost),
            )
            converted_qty = uom_conversion(unit, product.uom, Decimal(quantity))
            current_balance = current_balance - converted_qty
            current_balance = abs(current_balance)
            total_balance = current_balance + converted_qty
            if total_balance > 0:
                avg_cost = (
                    (current_balance * product.cost.amount) + (converted_qty * cost_by_uom)
                ) / (total_balance)
                product.moving_average_cost.amount = avg_cost
                product.save()
            else:
                product.moving_average_cost.amount = cost_by_uom
                product.save()
        except Product.DoesNotExist:
            logged_data = "Product is Deleted"
            logger.warning(logged_data)

        set_current_tenant(None)
    except Exception as e:
        logged_data = f"Unable to calculate avg cost {e}"
        logger.warning(logged_data)
        self.retry(countdown=5 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def calculate_produced_average_cost(
    self, product_id, components_cost, others_cost, produced_quantity, account_id
):
    from sterlingpos.stocks.models import StockTransaction
    try:
        account = Account.objects.get(pk=account_id)
        set_current_tenant(account)
        try:
            product = account.product_set.get(pk=product_id)
            current_cost = product.cost.amount

            current_balance = StockTransaction.objects.filter(
                stock__product=product_id
            ).aggregate(
                total=Coalesce(Sum("quantity"), Value(0))
            ).get("total")

            current_balance = current_balance - Decimal(produced_quantity)
            current_balance = abs(current_balance)
            total_balance = current_balance + Decimal(produced_quantity)
            avg_cost = (current_cost * current_balance) + (
                Decimal(components_cost) + Decimal(others_cost)
            )
            avg_cost = avg_cost / total_balance
            product.moving_average_cost = avg_cost
            product.save()
        except Product.DoesNotExist:
            logged_data = "Product is Deleted"
            logger.warning(logged_data)
        set_current_tenant(None)
    except Exception as e:
        logged_data = f"Unable to calculate produced product avg cost {e}"
        logger.warning(logged_data)
        self.retry(countdown=5 ** self.request.retries)
