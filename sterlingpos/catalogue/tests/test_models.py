from django.db import DataError, IntegrityError

from test_plus.test import TestCase

from model_mommy import mommy
from django.template.defaultfilters import slugify

from sterlingpos.users.tests.factories import UserFactory
from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.catalogue.models import (
    get_upload_path,
    Category, Product, Catalogue, UOM, ProductType,
    ProductAddOn, ProductModifier, ProductModifierUsage, ProductModifierOption,
)

from sterlingpos.catalogue.categories import create_from_sequence, create_from_breadcrumbs
from model_mommy import mommy


class UOMTest(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        uom = UOM(unit='pcs', uom_type="base", category="units")
        assert uom.__str__() == "pcs"


class ProductTypeTest(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        product_type = ProductType(name='test-product-type')
        self.assertEqual(product_type.__str__(), "test-product-type")


class CatalogueTest(TestCase):

    def test_prepare_catalogue_classmethod(self):
        account = mommy.make(
            'accounts.Account', name='account-1')

        single_product = Product.objects.create(
            sku='product-test',
            parent=None, account=account)

        parent_product = Product.objects.create(
            sku='product-test',
            parent=None, account=account)

        variant_product = Product.objects.create(
            sku='product-test-var1',
            parent=parent_product, account=account)

        variant_product_2 = Product.objects.create(
            sku='product-test-var2',
            parent=parent_product, account=account)

        self.assertEqual(
            Catalogue.prepare_catalogue(single_product).name,
            single_product.name
        )

        self.assertEqual(
            Catalogue.prepare_catalogue(parent_product).name,
            parent_product.name
        )
        self.assertEqual(
            Catalogue.prepare_catalogue(variant_product).name,
            parent_product.name
        )
        self.assertEqual(
            Catalogue.prepare_catalogue(variant_product_2).name,
            parent_product.name
        )


class ProductTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product = Product.objects.create(sku='product-test', parent=None, account=account)
        self.product_var1 = Product.objects.create(sku='product-test-var1', parent=self.product, account=account)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_upload_path(self):
        upload_path = get_upload_path(self.product, 'image.jpg')
        file_name = "{}_{}".format(self.product.sku, slugify(self.product.name))
        self.assertEqual(
            upload_path, '{}/products/{}.jpg'.format(self.product.account.pk, file_name)
        )

    def test__str__(self):
        self.assertEqual(
            self.product.__str__(),
            "{} - {}".format(self.product.name, self.product.sku)
        )

    def test_having_parent_while_owning_variant_will_raise_error(self):
        with self.assertRaises(DataError):
            self.product.parent = self.product
            self.product.save()

    def test_adding_new_variant_for_product(self):
        self.product_var2 = Product.objects.create(
            sku='product-test-var2',
            parent=self.product, account=get_current_tenant())
        self.assertIn(self.product_var2, self.product.variants.all())
        self.assertFalse(self.product_var2.variants.exists())

    def test_soft_delete(self):
        self.product.delete()
        self.assertIsNotNone(self.product.deleted)
        self.assertTrue(Product.objects.deleted_only().count(), 1)
        self.assertEqual(Product.objects.count(), 0)

    def test_product_availability_and_pricing_signal(self):
        outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1')
        product = Product.objects.create(
            sku='product-test', parent=None, price=5000)

        self.assertEqual(product.availability.count(), 1)
        self.assertEqual(product.product_pricing.count(), 1)
        self.assertEqual(product.product_pricing.filter(price=5000).count(), 1)


class CompositeProductTest(TestCase):

    def tearDown(self):
        set_current_tenant(None)

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)
        self.product = Product.objects.create(
            sku='product-test',
            parent=None, classification=Product.PRODUCT_CLASSIFICATION.composite)
        self.product_non_composite = Product.objects.create(sku='product-test-non-compo', parent=None)
        self.product_compo_1 = Product.objects.create(sku='product-test-compo-1', parent=None)
        self.product_compo_2 = Product.objects.create(sku='product-test-compo-2', parent=None)

    def test__str__(self):
        compo_product = self.product.composite_material.create(
            product=self.product_compo_1, product_quantity=1)

        self.assertEqual(
            compo_product.__str__(),
            "Composite material product-test | product-test-compo-1 - {}".format(1))

    def test_cannot_add_composite_material_for_non_composite_product(self):
        self.product.composite_material.create(product=self.product_compo_1, product_quantity=1)
        self.product.composite_material.create(product=self.product_compo_2, product_quantity=1)
        self.assertTrue(self.product.composite_material.count(), 2)

        with self.assertRaises(DataError):
            self.product_non_composite.composite_material.create(product=self.product_compo_1, product_quantity=1)


class CategoryTest(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)
        self.category = Category.add_root(name=u"Category")
        self.sub_category = self.category.add_child(name=u"Sub Category")

    def tearDown(self):
        set_current_tenant(None)

    def test_slugs_were_autogenerated(self):
        self.assertTrue(self.category.slug)
        self.assertTrue(self.sub_category.slug)

    # def test_enforces_slug_uniqueness(self):
    #     more_category = self.category.add_child(name=self.sub_category.name)
    #     self.assertTrue(
    #         more_category.slug and more_category.slug != self.sub_category.slug)

    def test_duplicate_slugs_allowed_for_non_siblings(self):
        more_category = Category.add_root(name=self.sub_category.name)
        self.assertEqual(more_category.slug, self.sub_category.slug)

    def test_get_root_nodes(self):
        account_1 = mommy.make('accounts.Account', name='account-1')
        account_2 = mommy.make('accounts.Account', name='account-2')

        # Add 1 root Category for each tenant account
        set_current_tenant(account_1)
        more_category = Category.add_root(name='Root 1')
        set_current_tenant(account_2)
        more_category = Category.add_root(name='Root 2')

        self.assertEqual(Category.get_root_nodes().count(), 3)

    def test_unique_category_add_root(self):
        account_1 = mommy.make('accounts.Account', name='account-1')

        set_current_tenant(account_1)
        more_category = Category.add_root(name='Root 1')
        
        with self.assertRaises(IntegrityError):
            Category.add_root(name='Root 1')


class TestMovingACategory(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

        breadcrumbs = (
            'Books > Fiction > Horror > Teen',
            'Books > Fiction > Horror > Gothic',
            'Books > Fiction > Comedy',
            'Books > Non-fiction > Biography',
            'Books > Non-fiction > Programming',
            'Books > Children',
        )
        for trail in breadcrumbs:
            create_from_breadcrumbs(trail)

        horror = Category.objects.get(name="Horror")
        programming = Category.objects.get(name="Programming")
        horror.move(programming)

        # Reload horror instance to pick up changes
        self.horror = Category.objects.get(name="Horror")

    def tearDown(self):
        set_current_tenant(None)

    def test_updates_instance_name(self):
        self.assertEqual('Books > Non-fiction > Horror', self.horror.full_name)

    def test_updates_subtree_names(self):
        teen = Category.objects.get(name="Teen")
        self.assertEqual('Books > Non-fiction > Horror > Teen',
                         teen.full_name)
        gothic = Category.objects.get(name="Gothic")
        self.assertEqual('Books > Non-fiction > Horror > Gothic',
                         gothic.full_name)

    def test_move_to_upper_depth(self):
        children = Category.objects.get(name="Children")
        books = Category.objects.get(name="Books")
        children.move(books, 'last-sibling')

        children = Category.objects.get(name="Children")
        self.assertEqual('Children', children.full_name)

    def test_move_to_first_child_with_exiting_child(self):
        biography = Category.objects.get(name="Biography")
        fiction = Category.objects.get(name="Fiction")
        biography.move(fiction, 'first-child')

        biography = Category.objects.get(name="Biography")
        self.assertEqual('Books > Fiction > Biography', biography.full_name)

        children = Category.objects.get(name="Children")
        children.move(fiction, 'first-child')
        children = Category.objects.get(name="Children")
        self.assertEqual('Books > Fiction > Children', children.full_name)


class TestMovingMultiRootCategory(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

        breadcrumbs = (
            'Books > Fiction > Horror',
            'Books > Fiction > Horror',
            'Novels > Fiction',
        )
        for trail in breadcrumbs:
            create_from_breadcrumbs(trail)
    
    def tearDown(self):
        set_current_tenant(None)

    def test_move_novels_fiction_to_books(self):
        books = Category.objects.get(name="Books")
        fiction = books.get_children().get(name='Fiction')

        novels = Category.objects.get(name="Novels")

        with self.assertRaises(IntegrityError):
            fiction.move(novels, 'first-child')


class TestCategoryFactory(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_can_create_single_level_category(self):
        trail = 'Books'
        category = create_from_breadcrumbs(trail)
        self.assertIsNotNone(category)
        self.assertEqual(category.name, 'Books')
        self.assertEqual(category.slug, 'books')

    def test_can_create_parent_and_child_categories(self):
        trail = 'Books > Science-Fiction'
        category = create_from_breadcrumbs(trail)

        self.assertIsNotNone(category)
        self.assertEqual(category.name, 'Science-Fiction')
        self.assertEqual(category.get_depth(), 2)
        self.assertEqual(category.get_parent().name, 'Books')
        self.assertEqual(2, Category.objects.count())
        self.assertEqual(category.full_slug, 'books/science-fiction')

    def test_can_create_multiple_categories(self):
        trail = 'Books > Science-Fiction > Star Trek'
        create_from_breadcrumbs(trail)
        trail = 'Books > Factual > Popular Science'
        category = create_from_breadcrumbs(trail)

        self.assertIsNotNone(category)
        self.assertEqual(category.name, 'Popular Science')
        self.assertEqual(category.get_depth(), 3)
        self.assertEqual(category.get_parent().name, 'Factual')
        self.assertEqual(5, Category.objects.count())
        self.assertEqual(category.full_slug, 'books/factual/popular-science', )

    def test_can_use_alternative_separator(self):
        trail = 'Food|Cheese|Blue'
        create_from_breadcrumbs(trail, separator='|')
        self.assertEqual(3, len(Category.objects.all()))

    def test_updating_subtree_slugs_when_moving_category_to_new_parent(self):
        trail = 'A > B > C'
        create_from_breadcrumbs(trail)
        trail = 'A > B > D'
        create_from_breadcrumbs(trail)
        trail = 'A > E > F'
        create_from_breadcrumbs(trail)
        trail = 'A > E > G'
        create_from_breadcrumbs(trail)

        trail = 'T'
        target = create_from_breadcrumbs(trail)
        category = Category.objects.get(name='A')

        category.move(target, pos='first-child')

        c1 = Category.objects.get(name='A')
        self.assertEqual(c1.full_slug, 't/a')
        self.assertEqual(c1.full_name, 'T > A')

        child = Category.objects.get(name='F')
        self.assertEqual(child.full_slug, 't/a/e/f')
        self.assertEqual(child.full_name, 'T > A > E > F')

        child = Category.objects.get(name='D')
        self.assertEqual(child.full_slug, 't/a/b/d')
        self.assertEqual(child.full_name, 'T > A > B > D')

    def test_updating_subtree_when_moving_category_to_new_sibling(self):
        trail = 'A > B > C'
        create_from_breadcrumbs(trail)
        trail = 'A > B > D'
        create_from_breadcrumbs(trail)
        trail = 'A > E > F'
        create_from_breadcrumbs(trail)
        trail = 'A > E > G'
        create_from_breadcrumbs(trail)

        category = Category.objects.get(name='E')
        target = Category.objects.get(name='A')

        category.move(target, pos='right')

        child = Category.objects.get(name='E')
        self.assertEqual(child.full_slug, 'e')
        self.assertEqual(child.full_name, 'E')

        child = Category.objects.get(name='F')
        self.assertEqual(child.full_slug, 'e/f')
        self.assertEqual(child.full_name, 'E > F')

    def test_multiple_tree_under_different_company(self):
        user_1 = mommy.make('accounts.Account', name='account-1')
        user_2 = mommy.make('accounts.Account', name='account-2')

        set_current_tenant(user_1)
        trail = 'A > B > C'
        create_from_breadcrumbs(trail)
        set_current_tenant(user_2)
        trail = 'A > B > C'
        create_from_breadcrumbs(trail)

        set_current_tenant(user_1)
        self.assertEqual(
            Category.objects.count(), 3)

        set_current_tenant(user_2)
        self.assertEqual(
            Category.objects.count(), 3)

        set_current_tenant(None)
        self.assertEqual(
            Category.objects.count(), 6)

    def test_create_multiple_categories_with_same_name_at_same_depth(self):
        Category.objects.create(depth=1, name='Category')
        with self.assertRaises(IntegrityError) as e:
            Category.objects.create(depth=1, name='Category', path='0002')

    def test_create_multiple_categories_with_same_name_at_same_parent(self):
        root = Category.objects.create(depth=1, name='A', path='0000')
        root.add_child(name='B')
        with self.assertRaises(IntegrityError) as e:
            root.add_child(name='B', path='0002')

    def test_create_child_categories_under_parent_after_deleting_older_child(self):
        root = Category.objects.create(depth=1, name='A')
        child = root.add_child(name='B', path='0002')
        child.delete()
        child2 = root.add_child(name='C', path='0003')
        self.assertEqual(root.get_last_child(), child2)

    def test_move_categories_under_parent_after_deleting_older_child(self):
        root = Category.add_root(name='A')
        to_be_moved = Category.add_root(name='B')

        child = root.add_child(name='B', path='0002')
        child.delete()

        to_be_moved.move(root, pos='last-child')
        self.assertEqual(root.get_last_child(), to_be_moved)


class ProductAddOnTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        product_complementary = Product.objects.create(
            classification=Product.PRODUCT_CLASSIFICATION.complementary,
            sku='product-complementary', parent=None)
        product_usage = Product.objects.create(
            sku='product-usage', parent=None)
        product_addon = ProductAddOn.objects.create(
            product_usage=product_usage, product=product_complementary)
        self.assertEqual(product_addon.__str__(), "Product AddOn product-complementary for product-usage")

    def test_cannot_create_product_addon_for_non_complementary_product(self):
        product_non_complementary = Product.objects.create(
            classification=Product.PRODUCT_CLASSIFICATION.standart,
            sku='product-complementary', parent=None)
        product_usage = Product.objects.create(
            sku='product-usage', parent=None)

        with self.assertRaises(DataError):
            product_addon = ProductAddOn.objects.create(
                product_usage=product_usage, product=product_non_complementary)


class ProductModifierTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        product_1 = Product.objects.create(
            classification=Product.PRODUCT_CLASSIFICATION.complementary,
            sku='product-1', parent=None)
        product_1_modifier = ProductModifier.objects.create(
            name='modifier-product-1', product=product_1)
        ProductModifierUsage.objects.create(
            product=product_1,
            product_modifier=product_1_modifier
        )
        self.assertEqual(
            product_1_modifier.__str__(),
            "modifier-product-1 (0) option(s)")


class ProductModifierOptionTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        product_1_modifier = ProductModifier.objects.create(
            name='modifier-product-1')
        product_1_modifier_option_1 = ProductModifierOption.objects.create(
            value='modifier-option-1', product_modifier=product_1_modifier)
        self.assertEqual(product_1_modifier_option_1.__str__(), "Product Modifier modifier-product-1 - modifier-option-1")
