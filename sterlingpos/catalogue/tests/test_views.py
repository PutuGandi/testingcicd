import json
from test_plus.test import TestCase

from model_mommy import mommy
from django.urls import reverse
from django.test import RequestFactory
from django.contrib.messages.storage.fallback import FallbackStorage
from django.test.client import Client
from django.utils.translation import ugettext as _

from sterlingpos.core.models import set_current_tenant
from sterlingpos.catalogue.views import ProductCreateView
from sterlingpos.catalogue.forms import (
    ProductForm,
    VariantProductFormset,
    ProductCreateModifierFormset,
)
from sterlingpos.catalogue.views import (
    CategoryListJson,
    ProductListJson,
    VariantProductListJson,
    VariantListJson,
    ProductAddOnListJson,
    ModifierListJson,
    ProductModifierListJson,
    UOMListJson,
    UOMCreateView,
    UOMUpdateView,
    UOMAutocompleteView,
    ProductAutocompleteView,
    UOMProductAjaxView,
)

from sterlingpos.catalogue.models import (
    Product,
    UOM,
    Catalogue,
)


class BaseCatalogueTest(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.account", name="test-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit_pcs = self.account.uom_set.filter(category="units", uom_type="base").first()


        mommy.make("outlet.Outlet", branch_id="Branch-1", account=self.account)

        self.category_1 = mommy.make(
            "catalogue.category", name="Makanan", account=self.account
        )
        self.category_2 = mommy.make(
            "catalogue.category", name="Minuman", account=self.account
        )
        self.category_3 = mommy.make(
            "catalogue.category", name="Tea", archived=True, account=self.account
        )

        self.product_1 = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category_1,
            account=self.account,
            uom=self.unit_pcs,
        )
        self.product_2 = mommy.make(
            "catalogue.product",
            name="Teh Manis",
            category=self.category_2,
            account=self.account,
        )
        self.product_3 = mommy.make(
            "catalogue.product",
            name="Teh Botol",
            archived=True,
            category=self.category_2,
            account=self.account,
        )

        self.catalogue_1 = Catalogue.prepare_catalogue(self.product_1)
        self.catalogue_2 = Catalogue.prepare_catalogue(self.product_2)
        self.catalogue_3 = Catalogue.prepare_catalogue(self.product_3)
        self.catalogue_3.is_active = False
        self.catalogue_3.save()

        self.variant_1 = mommy.make(
            "catalogue.product",
            name="Baso Ayam",
            category=self.category_1,
            account=self.account,
            parent=self.product_1,
            catalogue=self.catalogue_1,
        )
        self.variant_2 = mommy.make(
            "catalogue.product",
            name="Baso Sapi",
            category=self.category_1,
            account=self.account,
            parent=self.product_1,
            catalogue=self.catalogue_1,
        )
        self.variant_3 = mommy.make(
            "catalogue.product",
            name="Baso Ikan",
            category=self.category_1,
            archived=True,
            account=self.account,
            parent=self.product_1,
            catalogue=self.catalogue_1,
        )

        self.product_4 = mommy.make(
            "catalogue.product",
            name="Baso Daging",
            category=self.category_1,
            account=self.account,
            catalogue=self.catalogue_1,
        )
        self.product_5 = mommy.make(
            "catalogue.product",
            name="Baso Sapi",
            category=self.category_1,
            account=self.account,
            catalogue=self.catalogue_1,
        )
        self.product_6 = mommy.make(
            "catalogue.product",
            name="Baso Ikan",
            category=self.category_1,
            archived=True,
            account=self.account,
            catalogue=self.catalogue_1,
        )

        self.product_addon_1 = mommy.make(
            "catalogue.product",
            classification=Product.PRODUCT_CLASSIFICATION.complementary,
            name="Gorengan",
            category=self.category_1,
            catalogue=self.catalogue_1,
            account=self.account,
        )
        self.addon_1 = mommy.make(
            "catalogue.productaddon",
            product=self.product_addon_1,
            product_usage=self.product_1,
            account=self.account,
        )
        self.product_addon_2 = mommy.make(
            "catalogue.product",
            classification=Product.PRODUCT_CLASSIFICATION.complementary,
            name="Kerupuk",
            category=self.category_1,
            catalogue=self.catalogue_2,
            account=self.account,
        )
        self.addon_2 = mommy.make(
            "catalogue.productaddon",
            product=self.product_addon_2,
            product_usage=self.product_1,
            account=self.account,
        )
        self.product_addon_3 = mommy.make(
            "catalogue.product",
            classification=Product.PRODUCT_CLASSIFICATION.complementary,
            name="Nasi",
            archived=True,
            category=self.category_1,
            catalogue=self.catalogue_3,
            account=self.account,
        )
        self.addon_3 = mommy.make(
            "catalogue.productaddon",
            product=self.product_addon_3,
            product_usage=self.product_3,
            account=self.account,
        )

        self.product_modifier_1 = mommy.make(
            "catalogue.productmodifier",
            name="Level Pedas",
            product=self.product_1,
            archived=True,
            account=self.account,
        )
        self.product_modifieropt_1 = mommy.make(
            "catalogue.productmodifieroption",
            value="Level 1",
            product_modifier=self.product_modifier_1,
            account=self.account,
        )
        self.product_modifieropt_2 = mommy.make(
            "catalogue.productmodifieroption",
            value="Level 2",
            product_modifier=self.product_modifier_1,
            account=self.account,
        )
        self.product_modifieropt_3 = mommy.make(
            "catalogue.productmodifieroption",
            value="Level 3",
            product_modifier=self.product_modifier_1,
            account=self.account,
        )

        self.product_modifier_2 = mommy.make(
            "catalogue.productmodifier",
            name="Level Manis",
            product=self.product_1,
            account=self.account,
        )
        self.product_modifieropt_2_1 = mommy.make(
            "catalogue.productmodifieroption",
            value="Level 1",
            product_modifier=self.product_modifier_2,
            account=self.account,
        )
        self.product_modifieropt_2_2 = mommy.make(
            "catalogue.productmodifieroption",
            value="Level 2",
            product_modifier=self.product_modifier_2,
            account=self.account,
        )

        mommy.make(
            "catalogue.ProductModifierUsage",
            product=self.product_1,
            product_modifier=self.product_modifier_1,
            account=self.account,
        )
        mommy.make(
            "catalogue.ProductModifierUsage",
            product=self.product_1,
            product_modifier=self.product_modifier_2,
            account=self.account,
        )

        self.client = Client()
        self.client.login(email="example@test.com", password="1sampai6")

    def tearDown(self):
        set_current_tenant(None)


class TestUOMListJson(BaseCatalogueTest):
    view_class = UOMListJson
    factory = RequestFactory()

    def setUp(self):
        super(TestUOMListJson, self).setUp()
        self.url = reverse("catalogue:data_uom")

    def test_uom_list_json(self):
        expected_result_valid = {
            "recordsTotal": 10,
            "recordsFiltered": 10,
            "columnsTotal": 4,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_filter_category(self):
        data = {
            "columns[1][data]": 1,
            "columns[1][name]": "",
            "columns[1][searchable]": "true",
            "columns[1][orderable]": "true",
            "columns[1][search][value]": "units",
            "columns[1][search][regex]": "false",
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 10,
            "recordsFiltered": 1,
            "columnsTotal": 4,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_filter_uom_type(self):
        data = {
            "columns[2][data]": "2",
            "columns[2][name]": "",
            "columns[2][searchable]": "true",
            "columns[2][orderable]": "true",
            "columns[2][search][value]": "base",
            "columns[2][search][regex]": "false",
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 10,
            "recordsFiltered": 3,
            "columnsTotal": 4,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )


class TestUOMProductAutocomplete(BaseCatalogueTest):

    def setUp(self):
        super(TestUOMProductAutocomplete, self).setUp()
        self.url = reverse("catalogue:uom_product_autocomplete", kwargs={"pk": self.product_1.pk})

    def test_autocomplete_success_request(self):
        expected_result = {"autocomplete": "{}".format(self.unit_pcs.unit)}
        data = {"q": "pcs"}

        response = self.client.get(
            self.url,
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(response.content)
        categories = response_data["results"]

        self.assertEqual(categories[0]["text"], expected_result["autocomplete"])


class TestUOMCatalogueProductAutocomplete(BaseCatalogueTest):
    def setUp(self):
        super(TestUOMCatalogueProductAutocomplete, self).setUp()
        self.url = reverse("catalogue:uom_catalogue_autocomplete", kwargs={"pk": self.catalogue_1.pk})

    def test_autocomplete_success_request(self):
        expected_result = {"autocomplete": "{}".format(self.unit_pcs.unit)}
        data = {"q": "pcs"}

        response = self.client.get(
            self.url,
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(response.content)
        categories = response_data["results"]

        self.assertEqual(categories[0]["text"], expected_result["autocomplete"])


class TestUOMProductAjaxView(BaseCatalogueTest):
    view_class = UOMProductAjaxView
    factory = RequestFactory()

    def setUp(self):
        super(TestUOMProductAjaxView, self).setUp()
        self.url = reverse("catalogue:uom_product")
    
    def test_get(self):
        params = {
            "product": self.product_1.pk
        }

        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = self.view_class.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestUOMCreateView(BaseCatalogueTest):
    def setUp(self):
        super(TestUOMCreateView, self).setUp()
        self.factory = RequestFactory()
        self.url = reverse("catalogue:uom_create")

    def test_post(self):
        params = {
            "unit": "test",
            "category": "volume",
            "uom_type": "smaller",
            "ratio": 300.0,
            "is_active": True,
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user

        response = UOMCreateView.as_view()(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(UOM.objects.all().order_by("pk").last().unit, params["unit"])

    def test_create_uom_base_type(self):
        params = {
            "unit": "test base",
            "category": "volume",
            "uom_type": "base",
            "ratio": 1.0,
            "is_active": True,
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user

        response = UOMCreateView.as_view()(request)

        self.assertEqual(response.status_code, 200)


class TestUOMUpdateView(BaseCatalogueTest):
    def setUp(self):
        super(TestUOMUpdateView, self).setUp()
        self.factory = RequestFactory()
        self.uom_test = mommy.make(
            "catalogue.uom",
            unit="test uom",
            category="volume",
            ratio=20.5,
            uom_type="smaller",
        )
        self.url = reverse("catalogue:uom_update", kwargs={"pk": self.uom_test.pk})

    def test_post(self):
        params = {
            "unit": "test update uom",
            "category": "volume",
            "uom_type": "smaller",
            "ratio": 44.0,
            "is_active": True,
        }
        request = self.factory.post(self.url, data=params)
        request.user = self.user

        response = UOMUpdateView.as_view()(request, pk=self.uom_test.pk)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(UOM.objects.all().order_by("pk").last().unit, params["unit"])


class TestUOMDetailView(BaseCatalogueTest):
    def setUp(self):
        super(TestUOMDetailView, self).setUp()
        self.factory = RequestFactory()
        self.uom_test = mommy.make(
            "catalogue.uom",
            unit="test uom",
            category="volume",
            ratio=20.5,
            uom_type="smaller",
        )
        self.url = reverse("catalogue:uom_detail", kwargs={"pk": self.uom_test.pk})

    def test_detail(self):
        request = self.factory.get(self.url)
        request.user = self.user
        response = UOMCreateView.as_view()(request, pk=self.uom_test.pk)

        self.assertEqual(response.status_code, 200)


class TestUOMAutocompleteView(BaseCatalogueTest):
    def setUp(self):
        super(TestUOMAutocompleteView, self).setUp()
        self.factory = RequestFactory()
        self.url = reverse("catalogue:uom_autocomplete")

    def test_autocomplete_success(self):
        expected_result = {"autocomplete": "Liter"}
        params = {"q": "Lit"}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = UOMAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        uoms = response_data["results"]
        self.assertEqual(uoms[0]["text"], expected_result["autocomplete"])


class CategoryListJsonTest(BaseCatalogueTest):
    view_class = CategoryListJson
    factory = RequestFactory()

    def setUp(self):
        super(CategoryListJsonTest, self).setUp()
        self.url = reverse("catalogue:data_category")

    def test_category_list_json(self):

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 3,
            "columnsTotal": 5,
        }

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_category_list_json_filter_status_show_all(self):

        data = {
            "columns[3][data]": 3,
            "columns[3][name]": "",
            "columns[3][searchable]": True,
            "columns[3][orderable]": False,
            "columns[3][search][value]": "show",
            "columns[3][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 3,
            "data_columns_status": "{}".format(_("Active")),
        }
        archive_list = [x[3] for x in context["data"]]
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertIn(expected_result_valid["data_columns_status"], archive_list)

    def test_category_list_json_filter_status_archived(self):
        data = {
            "columns[3][data]": 3,
            "columns[3][name]": "",
            "columns[3][searchable]": True,
            "columns[3][orderable]": False,
            "columns[3][search][value]": "true",
            "columns[3][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 1,
            "data_columns_status": "{}".format(_("Archived")),
        }
        data = context["data"][0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[3], expected_result_valid["data_columns_status"])

    def test_category_list_json_filter_status_active(self):

        data = {
            "columns[3][data]": 3,
            "columns[3][name]": "",
            "columns[3][searchable]": True,
            "columns[3][orderable]": False,
            "columns[3][search][value]": "false",
            "columns[3][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 2,
            "data_columns_status": "{}".format(_("Active")),
        }
        data = context["data"][0]

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[3], expected_result_valid["data_columns_status"])


class TestCategoryProductArchivedView(BaseCatalogueTest):
    def test_archived_category_product_view(self):
        data = {"id": self.category_2.pk}

        response = self.client.post(
            reverse("catalogue:category_archive"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestCategoryProductActivateView(BaseCatalogueTest):
    def test_activate_category_product_view(self):
        data = {"id": self.category_2.pk}

        response = self.client.post(
            reverse("catalogue:category_activate"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestCategoryProductDeleteView(BaseCatalogueTest):
    def test_delete_category_product_view(self):
        data = {"id": self.category_2.pk}

        response = self.client.post(
            reverse("catalogue:category_delete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class CategoryAutocompleteTest(BaseCatalogueTest):
    def test_autocomplete_success_request(self):
        expected_result = {"autocomplete": "{}".format(self.category_2.name)}

        data = {"q": "Min"}

        response = self.client.get(
            reverse("catalogue:category_autocomplete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(response.content)
        categories = response_data["results"]

        self.assertEqual(categories[0]["text"], expected_result["autocomplete"])


class CategoryCompositeAutocompleteTest(BaseCatalogueTest):
    def test_autocomplete_success_request(self):
        composite_category = mommy.make(
            "catalogue.category",
            name="Composite",
            classification="Composite",
            account=self.account,
        )

        expected_result = {"autocomplete": "{}".format(composite_category.name)}

        data = {"q": "Com"}

        response = self.client.get(
            reverse("catalogue:category_composite_autocomplete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(response.content)
        categories = response_data["results"]

        self.assertEqual(categories[0]["text"], expected_result["autocomplete"])


class ProductListJsonTest(BaseCatalogueTest):
    view_class = ProductListJson
    factory = RequestFactory()

    def setUp(self):
        super(ProductListJsonTest, self).setUp()
        self.url = reverse("catalogue:data_product")

    def test_product_list_json(self):

        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 3,
            "columnsTotal": 7,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_product_list_json_filter(self):
        data = {
            "draw": 5,
            "columns[0][data]": 0,
            "columns[0][name]": "",
            "columns[0][searchable]": True,
            "columns[0][orderable]": True,
            "columns[0][search][value]": "Baso",
            "columns[0][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 1,
            "data_columns_name": '<a href="{}">{}</a>'.format(
                reverse(
                    "catalogue:product_variant_list", kwargs={"pk": self.catalogue_1.pk}
                ),
                self.catalogue_1.name,
            ),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[0], expected_result_valid["data_columns_name"])

    def test_product_list_json_filter_status_show_all(self):

        data = {
            "columns[5][data]": 5,
            "columns[5][name]": "",
            "columns[5][searchable]": True,
            "columns[5][orderable]": False,
            "columns[5][search][value]": "show",
            "columns[5][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 3,
            "data_columns_status": "{}".format(_("Active")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[5], expected_result_valid["data_columns_status"])

    def test_product_list_json_filter_status_archive(self):

        data = {
            "columns[5][data]": 5,
            "columns[5][name]": "",
            "columns[5][searchable]": True,
            "columns[5][orderable]": False,
            "columns[5][search][value]": "false",
            "columns[5][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 1,
            "data_columns_status": "{}".format(_("Archived")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[5], expected_result_valid["data_columns_status"])

    def test_product_list_json_filter_status_active(self):

        data = {
            "columns[5][data]": 5,
            "columns[5][name]": "",
            "columns[5][searchable]": True,
            "columns[5][orderable]": False,
            "columns[5][search][value]": "true",
            "columns[5][search][regex]": False,
        }
        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 2,
            "data_columns_status": "{}".format(_("Active")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[5], expected_result_valid["data_columns_status"])


class ProductAutocompleteTest(BaseCatalogueTest):
    def setUp(self):
        super(ProductAutocompleteTest, self).setUp()
        self.factory = RequestFactory()
        self.url = reverse("catalogue:product_autocomplete")

    def test_autocomplete_success_request(self):
        expected_result = {
            "autocomplete": "%s - %s" % (self.product_2.name, self.product_2.sku)
        }

        params = {"q": "Teh Manis"}

        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = ProductAutocompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(response.content)
        products = response_data["results"]
        self.assertEqual(products[0]["text"], expected_result["autocomplete"])


class TestProductCreateView(BaseCatalogueTest):
    def setUp(self):
        super(TestProductCreateView, self).setUp()
        self.factory = RequestFactory()
        self.url = reverse("catalogue:product_create")
        self.view = ProductCreateView()
        self.product_form = ProductForm
        self.variant_formset = VariantProductFormset
        self.modifier_formset = ProductCreateModifierFormset

    def test_product_create_view(self):
        request = self.factory.get(self.url)
        request.user = self.user

        response = ProductCreateView.as_view()(request)

        self.assertEqual(response.status_code, 200)


class TestProductUpdateView(BaseCatalogueTest):
    def setUp(self):
        super(TestProductUpdateView, self).setUp()
        self.view = ProductCreateView()
        self.product_form = ProductForm
        self.variant_formset = VariantProductFormset
        self.modifier_formset = ProductCreateModifierFormset

    def test_product_create_view(self):
        response = self.client.get(
            reverse("catalogue:product_update", kwargs={"pk": self.catalogue_1.pk})
        )
        self.assertEqual(response.status_code, 200)


class TestProductArchivedView(BaseCatalogueTest):
    def test_archived_product_view(self):
        data = {"id": self.product_3.catalogue.pk}

        response = self.client.post(
            reverse("catalogue:product_archive"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestProductActivateView(BaseCatalogueTest):
    def test_activate_product_view(self):
        data = {"id": self.product_3.catalogue.pk}

        response = self.client.post(
            reverse("catalogue:product_activate"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestProductDeleteView(BaseCatalogueTest):
    def test_delete_product_view(self):
        data = {"id": self.product_3.catalogue.pk}

        response = self.client.post(
            reverse("catalogue:product_delete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class UpdateProductFormsetTest(BaseCatalogueTest):
    def setUp(self):
        super(UpdateProductFormsetTest, self).setUp()
        self.url = reverse("catalogue:update_form")

    def test_update_form_variant(self):
        data = {
            "prefix": ["variants"],
            "total_form": ["2"],
            "initial_data": [
                '{"variants-0-sku":"","variants-0-name":"","variants-0-cost":"","variants-0-price":""}'
            ],
            "total_data": ["0"],
        }

        response = self.client.post(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")

    def test_update_form_inital_variant(self):
        data = {
            "prefix": ["variants"],
            "total_form": ["2"],
            "initial_data": [
                "{'variants-0-id': '8', 'variants-0-parent': '7', 'variants-0-sku': '1289020', 'variants-0-name': 'Tanderloin', 'variants-0-cost': '30000.00', 'variants-0-price': '45000.00'}"
            ],
            "total_data": ["1"],
        }

        response = self.client.post(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")

    def test_update_form_modifier(self):
        data = {
            "prefix": ["modifiers"],
            "total_form": ["2"],
            "initial_data": ['{"modifiers-0-name":"","modifiers-0-values":""}'],
            "total_data": ["0"],
        }

        response = self.client.post(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")

    def test_update_form_intial_modifier(self):
        data = {
            "prefix": ["modifiers"],
            "total_form": ["2"],
            "initial_data": [
                "{'modifiers-0-id': '3', 'modifiers-0-name': 'Tingkat Kematangan', 'modifiers-0-values': 'Well Done,Medium well,Medium Rare,Rare'}"
            ],
            "total_data": ["1"],
        }

        response = self.client.post(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class VariantProductListJsonTest(BaseCatalogueTest):
    view_class = VariantProductListJson
    factory = RequestFactory()

    def setUp(self):
        super(VariantProductListJsonTest, self).setUp()
        self.url = reverse(
            "catalogue:data_variant_product", kwargs={"pk": self.catalogue_1.pk}
        )

    def test_variant_product_list_json(self):
        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.catalogue_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 6,
            "recordsFiltered": 6,
            "columnsTotal": 6,
        }
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_variant_product_list_json_filter(self):
        data = {
            "draw": 5,
            "columns[0][data]": 0,
            "columns[0][name]": "",
            "columns[0][searchable]": True,
            "columns[0][orderable]": True,
            "columns[0][search][value]": "Baso Sapi",
            "columns[0][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.catalogue_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 6,
            "recordsFiltered": 2,
            "data_columns_name": '<a href="{}">{}</a>'.format(
                reverse("catalogue:variant_update", kwargs={"pk": self.variant_2.pk}),
                self.variant_2.name,
            ),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[0], expected_result_valid["data_columns_name"])

    def test_variant_product_list_json_filter_status_show_all(self):

        data = {
            "columns[4][data]": 4,
            "columns[4][name]": "",
            "columns[4][searchable]": True,
            "columns[4][orderable]": False,
            "columns[4][search][value]": "show",
            "columns[4][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.catalogue_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 6,
            "recordsFiltered": 6,
            "data_columns_status": "{}".format(_("Archived")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_variant_product_list_json_filter_status_archive(self):

        data = {
            "columns[4][data]": 4,
            "columns[4][name]": "",
            "columns[4][searchable]": True,
            "columns[4][orderable]": False,
            "columns[4][search][value]": "true",
            "columns[4][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.catalogue_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 6,
            "recordsFiltered": 2,
            "data_columns_status": "{}".format(_("Archived")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[4], expected_result_valid["data_columns_status"])

    def test_variant_product_list_json_filter_status_active(self):

        data = {
            "columns[4][data]": 4,
            "columns[4][name]": "",
            "columns[4][searchable]": True,
            "columns[4][orderable]": False,
            "columns[4][search][value]": "false",
            "columns[4][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.catalogue_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 6,
            "recordsFiltered": 4,
            "data_columns_status": "{}".format(_("Active")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[4], expected_result_valid["data_columns_status"])


class TestProductVariantUpdateView(BaseCatalogueTest):
    def test_product_variant_update_view(self):
        self.login(email=self.user.email, password="1sampai6")
        response = self.get(
            reverse(
                "catalogue:product_variant_update", kwargs={"pk": self.variant_1.pk}
            )
        )
        self.assertEqual(response.status_code, 200)
        # response = self.client.get(
        #     reverse(
        #         'catalogue:product_variant_update',
        #         kwargs={'pk': self.variant_1.pk}))
        # self.assertEqual(response.status_code, 200)


class TestProductVariantArchivedView(BaseCatalogueTest):
    def test_archived_product_variant_view(self):
        data = {"id": self.product_6.pk}

        response = self.client.post(
            reverse("catalogue:productvariant_archive"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestProductVariantActivateView(BaseCatalogueTest):
    def test_activate_product_variant_view(self):
        data = {"id": self.product_6.pk}

        response = self.client.post(
            reverse("catalogue:productvariant_activate"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestProductVariantDeleteView(BaseCatalogueTest):
    def test_delete_product_variant_view(self):
        data = {"id": self.product_6.pk}

        response = self.client.post(
            reverse("catalogue:product_variant_delete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class VariantListJsonTest(BaseCatalogueTest):
    view_class = VariantListJson
    factory = RequestFactory()

    def setUp(self):
        super(VariantListJsonTest, self).setUp()
        self.url = reverse("catalogue:data_variant")

    def test_variant_list_json(self):
        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 8,
            "recordsFiltered": 8,
            "columnsTotal": 8,
        }
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_variant_list_json_filter(self):
        data = {
            "draw": 5,
            "columns[0][data]": 0,
            "columns[0][name]": "",
            "columns[0][searchable]": True,
            "columns[0][orderable]": True,
            "columns[0][search][value]": "Baso Ayam",
            "columns[0][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 8,
            "recordsFiltered": 1,
            "data_columns_name": '<a href="{}">{}</a>'.format(
                reverse("catalogue:variant_update", kwargs={"pk": self.variant_1.pk}),
                self.variant_1.name,
            ),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[0], expected_result_valid["data_columns_name"])

    def test_variant_product_list_json_filter_status_show_all(self):

        data = {
            "columns[6][data]": 6,
            "columns[6][name]": "",
            "columns[6][searchable]": True,
            "columns[6][orderable]": False,
            "columns[6][search][value]": "show",
            "columns[6][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 8,
            "recordsFiltered": 8,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_variant_list_json_filter_status_archive(self):

        data = {
            "columns[6][data]": 6,
            "columns[6][name]": "",
            "columns[6][searchable]": True,
            "columns[6][orderable]": False,
            "columns[6][search][value]": "true",
            "columns[6][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 8,
            "recordsFiltered": 2,
            "data_columns_status": "{}".format(_("Archived")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[6], expected_result_valid["data_columns_status"])

    def test_variant_list_json_filter_status_active(self):

        data = {
            "columns[6][data]": 6,
            "columns[6][name]": "",
            "columns[6][searchable]": True,
            "columns[6][orderable]": False,
            "columns[6][search][value]": "false",
            "columns[6][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 8,
            "recordsFiltered": 6,
            "data_columns_status": "{}".format(_("Active")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[6], expected_result_valid["data_columns_status"])


class ProductAddOnListJsonTest(BaseCatalogueTest):
    view_class = ProductAddOnListJson
    factory = RequestFactory()

    def setUp(self):
        super(ProductAddOnListJsonTest, self).setUp()
        self.url = reverse("catalogue:data_product_addon")

    def test_product_addon_list_json(self):
        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 3,
            "columnsTotal": 7,
        }
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_variant_list_json_filter(self):
        data = {
            "draw": 5,
            "columns[0][data]": 0,
            "columns[0][name]": "",
            "columns[0][searchable]": True,
            "columns[0][orderable]": True,
            "columns[0][search][value]": "Gorengan",
            "columns[0][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 1,
            "data_columns_name": '<a href="{}">{}</a>'.format(
                reverse(
                    "catalogue:product_addon_update",
                    kwargs={"pk": self.product_addon_1.pk},
                ),
                self.product_addon_1.name,
            ),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[0], expected_result_valid["data_columns_name"])

    def test_addon_product_list_json_filter_status_show_all(self):

        data = {
            "columns[5][data]": 5,
            "columns[5][name]": "",
            "columns[5][searchable]": True,
            "columns[5][orderable]": False,
            "columns[5][search][value]": "show",
            "columns[5][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 3,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_addon_product_list_json_filter_status_archive(self):

        data = {
            "columns[5][data]": 5,
            "columns[5][name]": "",
            "columns[5][searchable]": True,
            "columns[5][orderable]": False,
            "columns[5][search][value]": "true",
            "columns[5][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 1,
            "data_columns_status": "{}".format(_("Archived")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[5], expected_result_valid["data_columns_status"])

    def test_addon_product_list_json_filter_status_active(self):

        data = {
            "columns[5][data]": 5,
            "columns[5][name]": "",
            "columns[5][searchable]": True,
            "columns[5][orderable]": False,
            "columns[5][search][value]": "false",
            "columns[5][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 3,
            "recordsFiltered": 2,
            "data_columns_status": "{}".format(_("Active")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[5], expected_result_valid["data_columns_status"])


class TestAddonProductArchivedView(BaseCatalogueTest):
    def test_archived_addon_product_view(self):
        data = {"id": self.product_addon_3.pk}

        response = self.client.post(
            reverse("catalogue:productaddon_archive"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestAddonProductActivateView(BaseCatalogueTest):
    def test_activate_addon_product_view(self):
        data = {"id": self.product_addon_3.pk}

        response = self.client.post(
            reverse("catalogue:productaddon_activate"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestAddonProductDeleteView(BaseCatalogueTest):
    def test_delete_addon_product_view(self):
        data = {"id": self.product_addon_3.pk}

        response = self.client.post(
            reverse("catalogue:product_addon_delete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class ProductUsageAutocompleteTest(BaseCatalogueTest):
    def test_product_usage_autocomplete_success_request(self):
        expected_result = {"autocomplete": "{}".format(self.product_1.name)}

        data = {"q": "Baso"}

        request = self.client.get(
            reverse("outlet:province_autocomplete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(request.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(request.content)

        product_list = response_data["results"]

        for product in product_list:
            self.assertEqual(product["text"], expected_result["autocomplete"])


class ModifierListJsonTest(BaseCatalogueTest):
    view_class = ModifierListJson
    factory = RequestFactory()

    def setUp(self):
        super(ModifierListJsonTest, self).setUp()
        self.url = reverse("catalogue:data_modifier")

    def test_modifier_list_json(self):
        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 4,
        }
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_modifier_list_json_filter(self):
        data = {
            "columns[0][data]": 1,
            "columns[0][name]": "",
            "columns[0][searchable]": True,
            "columns[0][orderable]": True,
            "columns[0][search][value]": "Level",
            "columns[0][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_modifier_list_json_filter_status_show_all(self):

        data = {
            "columns[2][data]": 2,
            "columns[2][name]": "",
            "columns[2][searchable]": True,
            "columns[2][orderable]": False,
            "columns[2][search][value]": "show",
            "columns[2][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_product_modifier_list_json_filter_status_archive(self):
        data = {
            "columns[2][data]": 2,
            "columns[2][name]": "",
            "columns[2][searchable]": True,
            "columns[2][orderable]": False,
            "columns[2][search][value]": "true",
            "columns[2][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
            "data_columns_status": "{}".format(_("Archived")),
        }
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[2], expected_result_valid["data_columns_status"])

    def test_product_modifier_list_json_filter_status_active(self):

        data = {
            "columns[2][data]": 2,
            "columns[2][name]": "",
            "columns[2][searchable]": True,
            "columns[2][orderable]": False,
            "columns[2][search][value]": "false",
            "columns[2][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
            "data_columns_status": "{}".format(_("Active")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[2], expected_result_valid["data_columns_status"])


class ProductModifierListJsonTest(BaseCatalogueTest):
    view_class = ProductModifierListJson
    factory = RequestFactory()

    def setUp(self):
        super(ProductModifierListJsonTest, self).setUp()
        self.url = reverse(
            "catalogue:data_product_modifier", kwargs={"pk": self.product_1.pk}
        )

    def test_productmodifier_list_json(self):
        request = self.factory.get(
            self.url, data={}, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.product_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 4,
        }
        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_productmodifier_list_json_filter(self):
        data = {
            "draw": 5,
            "columns[0][data]": 0,
            "columns[0][name]": "",
            "columns[0][searchable]": True,
            "columns[0][orderable]": True,
            "columns[0][search][value]": "Level Manis",
            "columns[0][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.product_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
            "data_columns_name": '<a href="{}">{}</a>'.format(
                reverse(
                    "catalogue:product_modifier_update",
                    kwargs={
                        "product_pk": self.product_1.pk,
                        "pk": self.product_modifier_2.pk,
                    },
                ),
                self.product_modifier_2.name,
            ),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[0], expected_result_valid["data_columns_name"])

    def test_product_modifier_list_json_filter_status_show_all(self):

        data = {
            "columns[2][data]": 2,
            "columns[2][name]": "",
            "columns[2][searchable]": True,
            "columns[2][orderable]": False,
            "columns[2][search][value]": "show",
            "columns[2][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.product_1.pk}

        context = view.get_context_data()

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )

    def test_product_modifier_list_json_filter_status_archive(self):

        data = {
            "columns[2][data]": 2,
            "columns[2][name]": "",
            "columns[2][searchable]": True,
            "columns[2][orderable]": False,
            "columns[2][search][value]": "true",
            "columns[2][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.product_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
            "data_columns_status": "{}".format(_("Archived")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[2], expected_result_valid["data_columns_status"])

    def test_product_modifier_list_json_filter_status_active(self):

        data = {
            "columns[2][data]": 2,
            "columns[2][name]": "",
            "columns[2][searchable]": True,
            "columns[2][orderable]": False,
            "columns[2][search][value]": "false",
            "columns[2][search][regex]": False,
        }

        request = self.factory.get(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user

        view = self.view_class()
        view.request = request
        view.kwargs = {"pk": self.product_1.pk}

        context = view.get_context_data()
        data = context.get("data")[0]

        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
            "data_columns_status": "{}".format(_("Active")),
        }

        self.assertEqual(context["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            context["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(data[2], expected_result_valid["data_columns_status"])


class TestModifierProductArchivedView(BaseCatalogueTest):
    def test_archived_modifier_product_view(self):
        data = {"id": self.product_modifier_2.pk}

        response = self.client.post(
            reverse("catalogue:productmodifier_archive"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestModifierProductActivateView(BaseCatalogueTest):
    def test_activate_modifier_product_view(self):
        data = {"id": self.product_modifier_2.pk}

        response = self.client.post(
            reverse("catalogue:productmodifier_activate"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class TestModifierProductDeleteView(BaseCatalogueTest):
    def test_delete_modifier_product_view(self):
        data = {"id": self.product_modifier_2.pk}

        response = self.client.post(
            reverse("catalogue:product_modifier_delete"),
            data=data,
            **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")


class UpdateModiferProductFormTest(BaseCatalogueTest):
    def setUp(self):
        super(UpdateModiferProductFormTest, self).setUp()
        self.url = reverse("catalogue:update_form_modifier")

    def test_update_form_modifier(self):
        data = {
            "prefix": ["modifiers"],
            "total_form": ["2"],
            "initial_data": [
                '{"modifiers-0-sku":"","modifiers-0-name":"","modifiers-0-cost":"","modifiers-0-price":""}'
            ],
            "modifieropt": ["0"],
        }

        response = self.client.post(
            self.url, data=data, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], "success")
