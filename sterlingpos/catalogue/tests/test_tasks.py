from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.catalogue.tasks import (
    calculate_avarage_cost,
    calculate_produced_average_cost,
    update_catalogue_variant_task,
    update_variant_task,
)
from sterlingpos.stocks.models import Stock
from sterlingpos.catalogue.models import Product, Catalogue


class TestTaskCalculateAvarageCost(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.account", name="test-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit_pcs = self.account.uom_set.filter(
            category="units", uom_type="base"
        ).first()
        self.category = mommy.make(
            "catalogue.category", name="Makanan", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_pcs,
            price=15000,
            cost=5000,
        )
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="Branch-1", account=self.account
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit_pcs)

    def tearDown(self):
        set_current_tenant(None)

    def test_task_success(self):
        task = calculate_avarage_cost.apply(
            kwargs={
                "product_id": self.product.pk,
                "quantity": 10,
                "unit_id": self.unit_pcs.pk,
                "cost": 7500,
                "account_id": self.account.pk,
            },
        )
        self.assertEqual(task.state, "SUCCESS")

    def test_task_deleted_product(self):
        product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_pcs,
            price=15000,
            cost=5000,
        )
        stock, _ = Stock.objects.get_or_create(
            product=product, outlet=self.outlet, account=self.account
        )
        stock.set_balance(100, unit=self.unit_pcs)
        product_pk = product.pk
        product.delete()
        task = calculate_avarage_cost.apply(
            kwargs={
                "product_id": product_pk,
                "quantity": 10,
                "unit_id": self.unit_pcs.pk,
                "cost": 7500,
                "account_id": self.account.pk,
            },
        )
        self.assertEqual(task.state, "SUCCESS")


class TestTaskCalculateProducedAverageCost(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.account", name="test-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit_pcs = self.account.uom_set.filter(
            category="units", uom_type="base"
        ).first()
        self.category = mommy.make(
            "catalogue.category", name="Makanan", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_pcs,
            price=15000,
            cost=5000,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
        )
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="Branch-1", account=self.account
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit_pcs)

    def test_task_success(self):
        task = calculate_produced_average_cost.apply(
            kwargs={
                "product_id": self.product.pk,
                "components_cost": 32000,
                "others_cost": 5000,
                "produced_quantity": 1,
                "account_id": self.account.pk,
            }
        )
        self.assertEqual(task.state, "SUCCESS")

    def test_task_deleted_product(self):
        product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_pcs,
            price=15000,
            cost=5000,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
        )
        stock, _ = Stock.objects.get_or_create(
            product=product, outlet=self.outlet, account=self.account
        )
        stock.set_balance(100, unit=self.unit_pcs)
        product_pk = product.pk
        product.delete()
        task = calculate_produced_average_cost.apply(
            kwargs={
                "product_id": product_pk,
                "components_cost": 32000,
                "others_cost": 5000,
                "produced_quantity": 1,
                "account_id": self.account.pk,
            },
        )
        self.assertEqual(task.state, "SUCCESS")


class TestUpdateCatalogueVariantTask(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.account", name="test-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit_kg = self.account.uom_set.filter(
            category="weight", uom_type="base"
        ).first()
        self.unit_gram = self.account.uom_set.filter(category=self.unit_kg.category, unit="g").first()

        self.category = mommy.make(
            "catalogue.category", name="Makanan", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_kg,
            price=15000,
            cost=5000,
        )
        self.catalogue = Catalogue.prepare_catalogue(self.product)
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="Branch-1", account=self.account
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit_kg)

    def tearDown(self):
        set_current_tenant(None)

    def test_task_update_catalogue(self):
        task = update_catalogue_variant_task.apply(
            kwargs={
                "catalogue_id": self.catalogue.pk,
                "uom_id": self.unit_gram.pk,
                "current_uom_id": self.unit_kg.pk,
                "is_manage_stock": self.catalogue.is_manage_stock,
                "is_batch_tracked": self.catalogue.is_batch_tracked,
                "category_id": self.catalogue.category.pk,
                "account_id": self.account.pk
            }
        )
        self.assertEqual(task.state, "SUCCESS")

    def test_task_update_with_deleted_catalogue(self):
        product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_kg,
            price=15000,
            cost=5000,
        )
        catalogue = Catalogue.prepare_catalogue(product)
        
        stock, _ = Stock.objects.get_or_create(
            product=product, outlet=self.outlet, account=self.account
        )
        stock.set_balance(100, unit=self.unit_kg)
        catalogue_pk = catalogue.pk
        catalogue_is_manage_stock = catalogue.is_manage_stock
        catalogue_is_batch_tracked = catalogue.is_batch_tracked
        catalogue_category_pk = catalogue.category.pk
        catalogue.delete()
        task = update_catalogue_variant_task.apply(
            kwargs={
                "catalogue_id": catalogue_pk,
                "uom_id": self.unit_gram.pk,
                "current_uom_id": self.unit_kg.pk,
                "is_manage_stock": catalogue_is_manage_stock,
                "is_batch_tracked": catalogue_is_batch_tracked,
                "category_id": catalogue_category_pk,
                "account_id": self.account.pk
            }
        )
        self.assertEqual(task.state, "SUCCESS")


class TestUpdateVariantTask(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.account", name="test-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit_kg = self.account.uom_set.filter(
            category="weight", uom_type="base"
        ).first()
        self.unit_gram = self.account.uom_set.filter(category=self.unit_kg.category, unit="g").first()

        self.category = mommy.make(
            "catalogue.category", name="Makanan", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_kg,
            price=15000,
            cost=5000,
        )
        self.catalogue = Catalogue.prepare_catalogue(self.product)
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="Branch-1", account=self.account
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit_kg)

    def tearDown(self):
        set_current_tenant(None)

    def test_update_variant(self):
        task = update_variant_task.apply(
            kwargs={
                "variant_id": self.product.pk,
                "uom_id": self.unit_gram.pk,
                "current_uom_id": self.unit_kg.pk,
                "is_manage_stock": self.product.is_manage_stock,
                "is_batch_tracked": self.product.is_batch_tracked,
                "account_id": self.account.pk
            }
        )
        self.assertEqual(task.state, "SUCCESS")

    def test_update_with_deleted_variant(self):
        product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_kg,
            price=15000,
            cost=5000,
        )        
        stock, _ = Stock.objects.get_or_create(
            product=product, outlet=self.outlet, account=self.account
        )
        stock.set_balance(100, unit=self.unit_kg)
        variant_id = product.pk
        variant_is_manage_stock = product.is_manage_stock
        variant_is_batch_tracked = product.is_batch_tracked
        product.delete()
        task = update_variant_task.apply(
            kwargs={
                "variant_id": variant_id,
                "uom_id": self.unit_gram.pk,
                "current_uom_id": self.unit_kg.pk,
                "is_manage_stock": variant_is_manage_stock,
                "is_batch_tracked": variant_is_batch_tracked,
                "account_id": self.account.pk
            }
        )
        self.assertEqual(task.state, "SUCCESS")
