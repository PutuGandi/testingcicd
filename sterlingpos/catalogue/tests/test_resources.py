import csv
import os
from test_plus.test import TestCase

from model_mommy import mommy

from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.translation import ugettext_lazy as _

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.core.import_export import SterlingImportForm
from sterlingpos.catalogue.resources import ProductResource, ManufacturedProductResource
from sterlingpos.catalogue.models import Catalogue, Product, Category


class ProductResourceTest(TestCase):
    def setUp(self):
        self.form_class = SterlingImportForm
        self.resource = ProductResource()
        self.headers = ProductResource.Meta.fields

        account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(account)

        raw_items = [
            ["prod-1", "catalogue 1", "var 1", "cat 1", 20000, 5000, "True"],
            ["prod-2", "catalogue 1", "var 1", "cat 1", 20000, 5000, "True"],
            ["prod-3", "catalogue 2", "var 1", "cat 2", 20000, 5000, "True"],
            ["prod-4", "catalogue 2", "var 1", "cat 2", 20000, 5000, "False"],
            ["prod-5", "catalogue 3", "var 1", "cat 2", 20000, 5000, "True"],
        ]

        with open("product_import_test.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                ["sku", "catalogue", "name", "category", "price", "cost", "is_sellable"]
            )
            for item in raw_items:
                writer.writerow(item)

        raw_items_bad = [
            ["", "", "", "", 0, 5000, "True"],
        ]

        with open("product_import_test_bad_file.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                ["aaa", "bbb", "yyy", "xxx", "price", "cost", "is_sellable"]
            )
            for item in raw_items_bad:
                writer.writerow(item)

        raw_items_bad = [
            ["", "", "", "", "ccc", "ddd", "TRUE"],
        ]

        with open("product_import_test_bad_file_2.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                ["sku", "catalogue", "yyy", "xxx", "price", "cost", "is_sellable"]
            )
            for item in raw_items_bad:
                writer.writerow(item)

        raw_items_bad = [
            ["sku-1", "", "", "", "ccc", "ddd", "TRUE"],
        ]

        with open("product_import_test_bad_file_3.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                ["sku", "catalogue", "yyy", "xxx", "price", "cost", "is_sellable"]
            )
            for item in raw_items_bad:
                writer.writerow(item)

    def tearDown(self):
        set_current_tenant(None)
        os.remove("product_import_test.csv")
        os.remove("product_import_test_bad_file.csv")
        os.remove("product_import_test_bad_file_2.csv")
        os.remove("product_import_test_bad_file_3.csv")

    def test_create_product(self):
        with open("product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        self.assertEqual(Catalogue.objects.count(), 3)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)
        self.assertEqual(Product.objects.filter(is_sellable=True).count(), 4)
        self.assertEqual(Catalogue.objects.filter(is_sellable=True).count(), 3)

    def test_create_product_multiple_account(self):
        with open("product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        self.assertEqual(Catalogue.objects.count(), 3)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)

        account_2 = mommy.make("accounts.Account", name="account-2")
        set_current_tenant(account_2)

        with open("product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        self.assertEqual(Catalogue.objects.count(), 3)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)

        account_3 = mommy.make("accounts.Account", name="account-3")
        set_current_tenant(account_3)
        with open("product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        self.assertEqual(Catalogue.objects.count(), 3)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)

    def test_create_product_with_bad_import_header(self):
        with open("product_import_test_bad_file.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))

    def test_create_product_with_bad_import_value(self):
        with open("product_import_test_bad_file_2.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))

    def test_create_product_with_bad_import_duplicate_value(self):

        mommy.make("catalogue.Product", sku="sku-1")

        mommy.make("catalogue.Product", sku="sku-1")

        with open("product_import_test_bad_file_3.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))
        self.assertEqual(
            form.result.row_errors()[0][1][0].error.__str__(),
            _("There are more than 2 existing Product under this SKU."),
        )


class ManufacturedProductResourceTest(TestCase):
    def setUp(self):
        self.form_class = SterlingImportForm
        self.resource = ManufacturedProductResource()
        self.headers = ManufacturedProductResource.Meta.fields
        account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(account)

        raw_items = [
            ["prod-1", "catalogue1", "var 1", "cat 1", 20000, 5000, "True", "pcs"],
            ["prod-2", "catalogue1", "var 1", "cat 1", 20000, 5000, "True", "pcs"],
            ["prod-3", "catalogue2", "var 1", "cat 2", 20000, 5000, "True", "pcs"],
            ["prod-4", "catalogue2", "var 1", "cat 2", 20000, 5000, "False", "pcs"],
            ["prod-5", "catalogue3", "var 1", "cat 2", 20000, 5000, "True", "pcs"],
        ]

        with open("manufactured_product_import_test.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "catalogue",
                    "name",
                    "category",
                    "price",
                    "cost",
                    "is_sellable",
                    "uom",
                ]
            )
            for item in raw_items:
                writer.writerow(item)

        raw_items_bad = [
            ["", "", "", "", 0, 5000, "True", ""],
        ]

        with open("manufactured_product_import_test_bad_file.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                ["aaa", "bbb", "yyy", "xxx", "price", "cost", "is_sellable", "uom"]
            )
            for item in raw_items_bad:
                writer.writerow(item)

        raw_items_bad = [
            ["", "", "", "", "ccc", "ddd", "TRUE", "xxx"],
        ]

        with open("manufactured_product_import_test_bad_file_2.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "catalogue",
                    "yyy",
                    "xxx",
                    "price",
                    "cost",
                    "is_sellable",
                    "ccc",
                ]
            )
            for item in raw_items_bad:
                writer.writerow(item)

        raw_items_bad = [
            ["sku-1", "", "", "", "ccc", "ddd", "TRUE", ""],
        ]

        with open("manufactured_product_import_test_bad_file_3.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "catalogue",
                    "yyy",
                    "xxx",
                    "price",
                    "cost",
                    "is_sellable",
                    "uom",
                ]
            )
            for item in raw_items_bad:
                writer.writerow(item)

    def tearDown(self):
        set_current_tenant(None)
        os.remove("manufactured_product_import_test.csv")
        os.remove("manufactured_product_import_test_bad_file.csv")
        os.remove("manufactured_product_import_test_bad_file_2.csv")
        os.remove("manufactured_product_import_test_bad_file_3.csv")

    def test_create_product(self):
        with open("manufactured_product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        print(Catalogue.objects.all())
        self.assertEqual(Catalogue.objects.count(), 4)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)
        self.assertEqual(Product.objects.filter(is_sellable=True).count(), 4)
        self.assertEqual(Catalogue.objects.filter(is_sellable=True).count(), 3)

    def test_create_product_multiple_account(self):
        with open("manufactured_product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        self.assertEqual(Catalogue.objects.count(), 4)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)

        account_2 = mommy.make("accounts.Account", name="account-2")
        set_current_tenant(account_2)

        with open("manufactured_product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        self.assertEqual(Catalogue.objects.count(), 4)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)

        account_3 = mommy.make("accounts.Account", name="account-3")
        set_current_tenant(account_3)
        with open("manufactured_product_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        result = form.save()
        self.assertEqual(Catalogue.objects.count(), 4)
        self.assertEqual(Product.objects.count(), 5)
        self.assertEqual(Category.objects.count(), 2)

    def test_create_product_with_bad_import_header(self):
        with open("manufactured_product_import_test_bad_file.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))

    def test_create_product_with_bad_import_value(self):
        with open("manufactured_product_import_test_bad_file_2.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))