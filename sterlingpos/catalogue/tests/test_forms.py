from test_plus.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.catalogue.models import Catalogue
from sterlingpos.catalogue.categories import create_from_breadcrumbs
from sterlingpos.catalogue.forms import (
    ProductForm,
    VariantProductForm,
    VariantProductUpdateForm,

    ProductComplementaryForm,
    ProductAddOnForm,

    ProductModifierForm,
    ProductModifierOptionForm,
    ProductModifierOptionFormSet,

    CompositeProductForm,
    CompositeMaterialProductForm,
    DynamicCompositeMaterialForm,
    CategoryCreateForm,
    CategoryForm,
)


class ProductFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = ProductForm
        self.product_parent = mommy.make('catalogue.Product', parent=None, account=account)

        self.product_var1 = mommy.make(
            'catalogue.Product',
            parent=self.product_parent, account=account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_product(self):
        prepared_product = mommy.prepare(
            'catalogue.Product',
            sku='PRODUCT-001', parent=None, price=1000, cost=500)
        category = create_from_breadcrumbs('Category A')
        product_data = prepared_product.__dict__
        product_data.update({
            'category': category.pk,
            'total_variant': 0,
            'total_modifier': 0,
            'price_0': product_data['price'],
            'price_1': "IDR",
            'cost_0': product_data['cost'],
            'cost_1': "IDR",
        })

        product_form = self.product_form(data=product_data)
        self.assertTrue(product_form.is_valid(), product_form.errors)
        product = product_form.save()
        self.assertEqual(product.cost.amount, product_data['cost'])
        self.assertEqual(product.price.amount, product_data['price'])
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.account, get_current_tenant())

    def test_create_product_huge_price_and_cost(self):
        prepared_product = mommy.prepare(
            'catalogue.Product', sku='PRODUCT-001', parent=None)
        category = create_from_breadcrumbs('Category A')
        product_data = prepared_product.__dict__
        product_data.update({
            'category': category.pk,
            'total_variant': 0,
            'total_modifier': 0,
            'price': 1200000000,
            'cost': 12000000000,
        })

        product_form = self.product_form(data=product_data)
        self.assertFalse(product_form.is_valid(), product_form.errors)
        self.assertIn("price", product_form.errors)
        self.assertIn("cost", product_form.errors)


class VariantProductFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = VariantProductForm
        self.product_parent = mommy.make(
            'catalogue.Product', sku='PRODUCT-001',
            parent=None, account=account)
        self.product_var1 = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-001-1',
            parent=self.product_parent, account=account)
        self.catalogue = Catalogue.prepare_catalogue(self.product_parent)
        
    def tearDown(self):
        set_current_tenant(None)

    def test_create_product(self):
        prepared_product = mommy.prepare(
            'catalogue.Product', sku='PRODUCT-001-2',
            price=1000, cost=500, parent=None)

        product_data = prepared_product.__dict__
        product_data.update({
            'parent': self.product_parent.pk,
            'catalogue': self.catalogue.pk,
            'price_0': product_data['price'],
            'price_1': product_data['price_currency'],
            'cost_0': product_data['cost'],
            'cost_1': product_data['cost_currency'],
        })

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertEqual(product.price.amount, 1000)
        self.assertEqual(product.cost.amount, 500)
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.catalogue, self.product_parent.catalogue)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertIsNotNone(product.parent, self.product_parent)
        self.assertEqual(product.category, self.product_parent.category)

    def test_create_product_with_huge_price_and_cost(self):
        prepared_product = mommy.prepare(
            'catalogue.Product', sku='PRODUCT-001-2',
            price=1000, cost=500, parent=None)

        product_data = prepared_product.__dict__
        product_data.update({
            'parent': self.product_parent.pk,
            'price': 120000000000,
            'cost': 120000000000,
        })

        product_form = self.product_form(data=product_data)
        self.assertFalse(product_form.is_valid(), product_form.data)
        self.assertIn("price", product_form.errors)
        self.assertIn("cost", product_form.errors)

    def test_create_variant_from_other_variant(self):
        prepared_product = mommy.prepare(
            'catalogue.Product', sku='PRODUCT-001-1-1', parent=None)

        new_product_var_data = prepared_product.__dict__
        new_product_var_data.update({
            'parent': self.product_var1.pk,
            'price_0': new_product_var_data['price'],
            'price_1': new_product_var_data['price_currency'],
            'cost_0': new_product_var_data['cost'],
            'cost_1': new_product_var_data['cost_currency'],
        })

        product_form = self.product_form(data=new_product_var_data)
        self.assertFalse(product_form.is_valid(), product_form.data)
        self.assertIn('parent', product_form.errors.keys(), product_form.errors)


class VariantProductUpdateFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = VariantProductUpdateForm
        self.product = mommy.make(
            'catalogue.Product', sku='PRODUCT-001',
            parent=None, account=account)
        self.catalogue = Catalogue.prepare_catalogue(self.product)
        self.product.catalogue = self.catalogue
        
    def tearDown(self):
        set_current_tenant(None)

    def test_update_product(self):
        category = create_from_breadcrumbs('Category A')
        data = {
            "name": "Product 001",
            "category": category.pk,
            "sku": "PRODUCT-001-UPDATED",
            "total_variant": 0,
            "price_0": 1000,
            'price_1': "IDR",
            "cost_0": 0,
            'cost_1': "IDR",
        }

        product_form = self.product_form(
            data=data, instance=self.product)
        self.assertTrue(
            product_form.is_valid(),
            (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertEqual(product.sku, "PRODUCT-001-UPDATED")
        self.assertIsNotNone(product.catalogue, self.product.catalogue)
        self.assertIsNotNone(product.account, get_current_tenant())


class CompositeProductFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = CompositeProductForm
        self.product_1 = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-001', parent=None, account=account, price=1000, cost=500)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_product(self):
        prepared_product = mommy.prepare(
            'catalogue.Product',
            sku='PRODUCT-COMPOSITE-001', parent=None, price=1000, cost=500)
        category = create_from_breadcrumbs('Category A')
        category.classification = 'Composite'
        category.save()
        product_data = prepared_product.__dict__
        product_data.update({
            'category': category.pk,
            'price_0': product_data['price'],
            'price_1': "IDR",
            'cost_0': product_data['cost'],
            'cost_1': "IDR",
        })

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertEqual(product.price.amount, 1000)
        self.assertEqual(product.cost.amount, 500)
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.catalogue)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertEqual(
            product.classification, product.__class__.PRODUCT_CLASSIFICATION.composite)

    def test_create_product_with_huge_price_and_cost(self):
        prepared_product = mommy.prepare(
            'catalogue.Product', sku='PRODUCT-COMPOSITE-001', parent=None)
        category = create_from_breadcrumbs('Category A')
        category.classification = 'Composite'
        category.save()
        product_data = prepared_product.__dict__
        product_data.update({
            'category': category.pk,
            'price': 12000000000,
            'cost': 12000000000,
        })

        product_form = self.product_form(data=product_data)
        self.assertFalse(product_form.is_valid(), product_form.data)
        self.assertIn("price", product_form.errors)
        self.assertIn("cost", product_form.errors)


class CompositeMaterialProductFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = CompositeMaterialProductForm

        self.category = create_from_breadcrumbs('Category A')

        self.product_composite = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-COMPOSITE-001', parent=None, account=account,
            classification='Composite')
        self.product_non_composite = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-NON_COMPOSITE-001', parent=None, account=account,
            classification='Standart')
        self.product_1 = mommy.make(
            'catalogue.Product',
            category=self.category,
            sku='PRODUCT-001', parent=None, account=account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_product(self):

        product_data = {
            'composite_product': self.product_composite.pk,
            'product': self.product_1.pk,
            'product_quantity': 2,
        }

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertEqual(self.product_composite.composite_material.count(), 1)


class DynamicCompositeMaterialFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = DynamicCompositeMaterialForm

        self.category = create_from_breadcrumbs('Category A')

        self.product_composite = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-COMPOSITE-001', parent=None, account=account,
            classification='Dynamic-Composite')
        self.product_non_composite = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-NON_COMPOSITE-001', parent=None, account=account,
            classification='Standart')
        self.product_1 = mommy.make(
            'catalogue.Product',
            category=self.category,
            sku='PRODUCT-001', parent=None, account=account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_composite_by_product(self):

        product_data = {
            'group_name': "Group Product",
            'composite_product': self.product_composite.pk,
            'product_lit': [self.product_1.pk,],
            'product_quantity': 2,
        }

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertEqual(self.product_composite.composite_material.count(), 1)

    def test_create_composite_by_category(self):

        product_data = {
            'group_name': "Group Category",
            "composite_product": self.product_composite.pk,
            "category": self.category.pk,
            "product_quantity": 2,
        }

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertEqual(self.product_composite.composite_material.count(), 1)

    def test_validate_dynamic_composite(self):

        product_data = {
            "group_name": "Group Mixed Fail",
            "composite_product": self.product_composite.pk,
            "product_list": [self.product_1.pk],
            "category": self.category.pk,
            "product_quantity": 2,
        }

        product_form = self.product_form(data=product_data)
        self.assertFalse(
            product_form.is_valid(), (product_form.data, product_form.errors))


class CategoryCreateFormTest(TestCase):
    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.category_form = CategoryCreateForm

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        form = self.category_form(data={'name': 'category'})
        self.assertTrue(form.is_valid(), form.errors)
        self.assertEqual(form.save().name, 'category')

    def test_save_existing_category(self):
        cat = mommy.make(
            'catalogue.Category', name='category', depth=1, account=get_current_tenant())

        form = self.category_form(data={'name': 'category'})
        self.assertTrue(form.is_valid(), form.errors)
        cat_2 = form.save()
        self.assertEqual(cat.pk, cat_2.pk)

    def test_save_composite(self):
        form = self.category_form(data={
            'name': 'paket-hemat',
            'composite_only': True,
        })
        self.assertTrue(form.is_valid(), form.errors)
        
        obj = form.save()

        self.assertEqual(obj.name, 'paket-hemat')
        self.assertIsNotNone(obj.get_parent())

    def test_save_composite_with_bad_composite_root_numchild(self):
        paketan = self.category_form.Meta.model.get_composite_root()
        paketan.numchild = 1
        paketan.save()

        form = self.category_form(data={
            'name': 'paket-hemat',
            'composite_only': True,
        })
        self.assertTrue(form.is_valid(), form.errors)
        
        obj = form.save()
        self.assertEqual(obj.name, 'paket-hemat')
        self.assertEqual(obj.get_parent(), paketan)
        self.assertEqual(obj.depth, 2)


class CategoryFormTest(TestCase):
    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)
        self.category_form = CategoryForm

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        cat = create_from_breadcrumbs("category")
        form = self.category_form(
            data={'name': 'category 3'},
            instance=cat
        )
        self.assertTrue(form.is_valid(), form.errors)
        self.assertEqual(form.save().name, 'category 3')

    def test_save_existing_category(self):
        cat = create_from_breadcrumbs("category")
        cat_2 = create_from_breadcrumbs("category 2")

        form = self.category_form(
            data={'name': 'category 2'},
            instance=cat,
        )

        self.assertFalse(
            form.is_valid(),
            form.data
        )
    
    def test_move_composite_category_to_existing_non_composite_category(self):
        cat = create_from_breadcrumbs("category xxx")
        cat_2 = create_from_breadcrumbs("category 2 xxx")

        form = self.category_form(
            data={
                'name': 'category xxx',
                'composite_only': True,
                },
            instance=cat_2,
        )

        self.assertTrue(form.is_valid(), form.errors)

        composite_cat = form.save()
        self.assertEqual(composite_cat.classification, "Composite")
        self.assertIsNotNone(composite_cat.get_parent())
        self.assertEqual(cat.name, composite_cat.name)
        self.assertNotEqual(cat.pk, composite_cat.pk)
        self.assertNotEqual(cat.depth, composite_cat.depth)

        form_2 = self.category_form(
            data={
                'name': 'category xxx',
                'composite_only': False,
                },
            instance=composite_cat,
        )
        self.assertFalse(form_2.is_valid(), form_2.data)


class ProductComplementaryFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = ProductComplementaryForm
        self.product_1 = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-001', parent=None)
        self.product_2 = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-002', parent=None)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_product(self):
        prepared_product = mommy.prepare(
            'catalogue.Product', sku='PRODUCT-COMPLEMENT-001',
            price=1000, cost=500, parent=None)
        category = create_from_breadcrumbs('Category A')
        product_data = prepared_product.__dict__
        product_data.update({
            'category': category.pk,
            'price_0': product_data['price'],
            'price_1': "IDR",
            'cost_0': product_data['cost'],
            'cost_1': "IDR",
            'product_usages': [self.product_1.pk, ]
        })

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertEqual(product.price.amount, 1000)
        self.assertEqual(product.cost.amount, 500)
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.catalogue)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertEqual(
            product.classification, product.__class__.PRODUCT_CLASSIFICATION.complementary)
        self.assertEqual(product.addon_usage.count(), 1)

        product_data.update({
            'product_usages': [self.product_2.pk, ]
        })
        product_form = self.product_form(data=product_data, instance=product)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertEqual(product.addon_usage.count(), 1)

    def test_create_product_with_huge_price_and_cost(self):
        prepared_product = mommy.prepare(
            'catalogue.Product', sku='PRODUCT-COMPLEMENT-001',
            price=1000, cost=500, parent=None)
        category = create_from_breadcrumbs('Category A')
        product_data = prepared_product.__dict__
        product_data.update({
            'category': category.pk,
            'price': 1200000000,
            'cost': 1200000000,
            'product_usages': [self.product_1.pk, ]
        })

        product_form = self.product_form(data=product_data)
        self.assertFalse(product_form.is_valid(), product_form.data)
        self.assertIn("price", product_form.errors)
        self.assertIn("cost", product_form.errors)


class ProductAddOnFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = ProductAddOnForm
        self.product_complementary = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-COMPOSITE-001', parent=None,
            classification='Complementary')
        self.product_non_complementary = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-NON_COMPOSITE-001', parent=None,
            classification='Standart')
        self.product_base = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-001', parent=None)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_product(self):

        product_data = {
            'product': self.product_complementary.pk,
            'product_usage': self.product_base.pk,
        }

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertEqual(self.product_base.addon.count(), 1)

    def test_create_composite_product_using_non_complementary_product(self):

        product_data = {
            'product': self.product_non_complementary.pk,
            'product_usage': self.product_base.pk,
        }

        product_form = self.product_form(data=product_data)
        self.assertFalse(product_form.is_valid(), product_form.data)
        self.assertIn('product', product_form.errors.keys(), product_form.errors)


class ProductModifierFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.form_class = ProductModifierForm
        self.product_base = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-001', parent=None)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_product_modifier(self):

        data = {
            'name': 'Modifier 1',
            'modifieropt': [
                'option-1', 'option-2'
            ],
            'product_usages': [
                self.product_base.pk,
            ]
        }

        form = self.form_class(data=data)
        self.assertTrue(
            form.is_valid(), (form.data, form.errors))
        modifier = form.save()
        
        self.assertIsNotNone(modifier.pk)
        self.assertIsNotNone(modifier.account, get_current_tenant())
        self.assertTrue(
            modifier.products.filter(pk=self.product_base.pk).exists())

    def test_create_product_modifier_with_huge_options(self):

        data = {
            'name': 'Modifier 1',
            'modifieropt': [
                '1234567890123456789012345678901234567890123456789012345678901234567890',
            ],
            'product_usages': [
                self.product_base.pk,
            ]
        }

        form = self.form_class(data=data)
        self.assertFalse(form.is_valid(), form.data)
        self.assertIsNotNone("modifieropt", form.errors)


class ProductModifierOptionFormTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

        self.product_form = ProductModifierOptionForm
        self.product_base = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-001', parent=None)
        self.product_modifier = mommy.make(
            'catalogue.ProductModifier',
            name='Modifier 1', product=self.product_base)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_product(self):

        product_data = {
            'product_modifier': self.product_modifier.pk,
            'value': 'Modifier Option 1',
        }

        product_form = self.product_form(data=product_data)
        self.assertTrue(
            product_form.is_valid(), (product_form.data, product_form.errors))
        product = product_form.save()
        self.assertIsNotNone(product.pk)
        self.assertIsNotNone(product.account, get_current_tenant())
        self.assertEqual(self.product_modifier.options.count(), 1)

    def test_create_product_using_formset(self):

        formset_data = {
            'options-0-value': 'PRODUCT-001 Modifier Option 1',
            'options-1-value': 'PRODUCT-001 Modifier Option 2',
            'options-TOTAL_FORMS': 3,
            'options-INITIAL_FORMS': 0,
            'options-MIN_NUM_FORMS': 0,
            'options-MAX_NUM_FORMS': 1000}

        formset = ProductModifierOptionFormSet(data=formset_data)
        self.assertTrue(
            formset.is_valid(), (formset.data, formset.errors))
        formset.instance = self.product_modifier
        modifier_options = formset.save()
        self.assertEqual(self.product_modifier.options.count(), 2)
