from django.contrib import admin
from django.db.models import CharField, F, Value as V
from django.db.models.functions import Concat
from treebeard.admin import TreeAdmin

from treebeard.forms import MoveNodeForm, movenodeform_factory

from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportMixin
from import_export.fields import Field
from import_export.widgets import (
    ManyToManyWidget, ForeignKeyWidget
)

from .models import (
    UOM,
    ProductType,
    Category,
    Catalogue,
    Product,
    CompositeProduct,

    ProductModifier,
    ProductModifierOption,
    ProductModifierUsage,

    ProductAddOn,
)
from .categories import create_from_breadcrumbs


class SterlingMoveNodeForm(MoveNodeForm):

    def save(self, commit=True):

        position_type, reference_node_id = self._clean_cleaned_data()

        if self.instance.pk is None:
            cl_data = {}
            for field in self.cleaned_data:
                if not isinstance(self.cleaned_data[field], (list, QuerySet)):
                    cl_data[field] = self.cleaned_data[field]
            if reference_node_id:
                reference_node = self._meta.model.all_objects.get(
                    pk=reference_node_id)
                self.instance = reference_node.add_child(**cl_data)
                self.instance.move(reference_node, pos=position_type)
            else:
                self.instance = self._meta.model.add_root(**cl_data)
        else:
            self.instance.save()
            if reference_node_id:
                reference_node = self._meta.model.all_objects.get(
                    pk=reference_node_id)
                self.instance.move(reference_node, pos=position_type)
            else:
                if self.is_sorted:
                    pos = 'sorted-sibling'
                else:
                    pos = 'first-sibling'
                self.instance.move(self._meta.model.get_first_root_node(), pos)
        # Reload the instance
        self.instance = self._meta.model.objects.get(pk=self.instance.pk)
        super(MoveNodeForm, self).save(commit=commit)
        return self.instance


class CustomManyToManyWidget(ManyToManyWidget):

    def clean(self, value, row=None, *args, **kwargs):
        if not value:
            return self.model.objects.none()
        if isinstance(value, (float, int)):
            ids = [int(value)]
        else:
            ids = value.split(self.separator)
            ids = filter(None, [i.strip() for i in ids])
        return self.model.objects.filter(**{
            '%s__in' % self.field: ids,
            'account': row['account']
        })


class CustomForeignKeyWidget(ForeignKeyWidget):

    def clean(self, value, row=None, *args, **kwargs):
        val = super(ForeignKeyWidget, self).clean(value)
        if val:
            data = {
                self.field: val,
                'account': row['account'],
            }
            return self.get_queryset(
                value, row, *args, **kwargs).get(**data)
        else:
            return None


class ProductAddOnWidget(ForeignKeyWidget):

    def clean(self, value, row=None, *args, **kwargs):
        val = super(ForeignKeyWidget, self).clean(value)
        if val:
            data = {
                self.field: val,
                'classification': Product.PRODUCT_CLASSIFICATION.complementary,
                'account': row['account'],
            }
            return self.get_queryset(
                value, row, *args, **kwargs).get(**data)
        else:
            return None


class ProductAddOnUsageWidget(ForeignKeyWidget):

    def clean(self, value, row=None, *args, **kwargs):
        val = super(ForeignKeyWidget, self).clean(value)
        if val:
            data = {
                self.field: val,
                'account': row['account'],
            }
            return self.get_queryset(
                value, row, *args, **kwargs
            ).exclude(
                classification=Product.PRODUCT_CLASSIFICATION.complementary,
            ).get(**data)
        else:
            return None


class ProductModifierForeignKeyWidget(ForeignKeyWidget):

    def clean(self, value, row=None, *args, **kwargs):
        data = {
            'name': value,
            'account': row['account'],
        }
        return self.get_queryset(
            value, row, *args, **kwargs
        ).get(**data)


class CategoryFullNameField(Field):

    def save(self, obj, data, is_m2m=False):
        cleaned = self.clean(data)
        setattr(obj, '_full_name', cleaned)


class CategoryResource(resources.ModelResource):

    full_name = CategoryFullNameField(
        attribute='full_name',
    )

    class Meta:
        model = Category
        import_id_fields = ('slug', 'account')
        export_order = ('full_name', )
        exclude = ('path', )
        fields = (
            'name', 'slug', 'full_name',
            'classification',
            'depth', 'account')

    def save_instance(self, instance, 
            using_transactions=True, dry_run=False):
        self.before_save_instance(instance, using_transactions, dry_run)
        if not using_transactions and dry_run:
            pass
        else:
            create_from_breadcrumbs(
                instance._full_name,
                account=instance.account)
        self.after_save_instance(instance, using_transactions, dry_run)


class CatalogueResource(resources.ModelResource):

    class Meta:
        model = Catalogue
        import_id_fields = ('name', 'classification', 'account')
        fields = (
            'name', 'classification', 
            'category', 'archived', 'is_active',
            'is_manage_stock',
            'is_sellable', 'is_batch_tracked', 'account')


class ProductResource(resources.ModelResource):

    category = Field(
        column_name='category',
        attribute='category',
        widget=CustomForeignKeyWidget(Category, 'name'))

    parent = Field(
        column_name='parent',
        attribute='parent',
        widget=CustomForeignKeyWidget(Product, 'sku'))

    catalogue = Field(
        column_name='catalogue',
        attribute='catalogue',
        widget=CustomForeignKeyWidget(Catalogue, 'name'))

    price = Field(
        column_name='price',
        attribute='price__amount')

    cost = Field(
        column_name='cost',
        attribute='cost__amount')

    class Meta:
        model = Product
        import_id_fields = ('sku', 'classification', 'account')
        fields = (
            'sku', 'classification', 'catalogue', 'name',
            'parent', 'image_location', 'released',
            'archived', 'price_incl_tax',
            'price', 'cost', 'category', 'account')


class CompositeProductResource(resources.ModelResource):

    product = Field(
        column_name='product',
        attribute='product',
        widget=CustomForeignKeyWidget(Product, 'sku'))

    composite_product = Field(
        column_name='composite_product',
        attribute='composite_product',
        widget=CustomForeignKeyWidget(Product, 'sku'))

    class Meta:
        model = CompositeProduct
        import_id_fields = (
            'composite_product', 'product', 'account')
        fields = (
            'product', 'composite_product', 
            'product_quantity', 'account')


class ProductAddOnResource(resources.ModelResource):

    product = Field(
        column_name='product',
        attribute='product',
        widget=ProductAddOnWidget(Product, 'sku'))

    product_usage = Field(
        column_name='product_usage',
        attribute='product_usage',
        widget=ProductAddOnUsageWidget(Product, 'sku'))

    class Meta:
        model = ProductAddOn
        import_id_fields = ('product', 'product_usage', 'account')
        fields = (
            'product', 'product_usage', 
            'archived', 'account')


class ProductModifierResource(resources.ModelResource):

    class Meta:
        model = ProductModifier
        import_id_fields = ('name', 'account')
        fields = ('name', 'archived', 'account')


class ProductModifierOptionResource(resources.ModelResource):
    product_modifier = Field(
        column_name="product_modifier",
        attribute="product_modifier",
        widget=ProductModifierForeignKeyWidget(
            ProductModifier
        )
    )

    class Meta:
        model = ProductModifierOption
        import_id_fields = (
            'product_modifier', 'value', 'account')
        fields = (
            'product_modifier', 'value', 'account')

    def dehydrate_product_modifier(self, obj):
        if obj.pk:
            return "{}".format(obj.product_modifier.name)


class ProductModifierUsageResource(resources.ModelResource):
    product_modifier = Field(
        column_name="product_modifier",
        attribute="product_modifier",
        widget=ProductModifierForeignKeyWidget(
            ProductModifier
        )
    )

    product = Field(
        column_name='product',
        attribute='product',
        widget=CustomForeignKeyWidget(Product, 'sku'))

    class Meta:
        model = ProductModifierUsage
        import_id_fields = (
            'product_modifier', 'product', 'account')
        fields = (
            'product_modifier', 'product', 'account')

    def dehydrate_product_modifier(self, obj):
        if obj.pk:
            return "{}".format(obj.product_modifier.name)


class CatalogueAdmin(admin.ModelAdmin):
    '''
        Admin View for Product
    '''
    list_display = (
        "name", "classification", "account",
        "is_active", "is_sellable", "is_manage_stock", "is_batch_tracked")
    search_fields = ("name", "classification", "account__name",)
    resource_class = CatalogueResource
    list_select_related = ("account",)
    raw_id_fields = ("account", "category",)


class UOMAdmin(admin.ModelAdmin):
    '''
        Admin View for UOM
    '''
    list_display = ('unit', 'category', 'uom_type', 'account')

    search_fields = ('unit', 'category', 'uom_type', 'account__name')


class ProductTypeAdmin(ImportExportModelAdmin):
    '''
        Admin View for ProductType
    '''
    list_display = ('name', 'account')
    search_fields = ('name', 'account__name')


class CategoryAdmin(ImportExportMixin, TreeAdmin):
    '''
        Admin View for ProductType
    '''
    form = movenodeform_factory(Category, form=SterlingMoveNodeForm)
    list_display = ('name', 'account', 'depth', 'path')
    search_fields = ('name', 'account__name', 'path')
    list_filter = ('depth',)

    resource_class = CategoryResource
    list_select_related = ("account",)
    raw_id_fields = ("account", )


class ProductAdmin(ImportExportModelAdmin):
    '''
        Admin View for Product
    '''
    list_display = ('sku', 'name', 'cost', 'price', 'parent', 'account')
    search_fields = ('sku', 'name', 'cost', 'price', 'account__name',)
    resource_class = ProductResource


class CompositeProductAdmin(ImportExportModelAdmin):
    '''
        Admin View for CompositeProduct
    '''
    list_display = (
        'product', 'composite_product', 
        'product_quantity', 'account')
    search_fields = (
        'product_quantity',
        'product__sku', 'product__name',
        'composite_product__sku', 'composite_product__name',
        'account__name',)

    resource_class = CompositeProductResource


class ProductAddOnAdmin(ImportExportModelAdmin):
    '''
        Admin View for ProductAddOn
    '''
    list_display = ('product', 'product_usage', 'archived', 'account')
    search_fields = ('product__sku', 'product__name', 'account__name',)
    resource_class = ProductAddOnResource


class ProductModifierOptionInline(admin.TabularInline):
    model = ProductModifierOption


class ProductModifierAdmin(ImportExportModelAdmin):
    '''
        Admin View for ProductModifier
    '''
    list_display = ('name', 'product', 'archived', 'account')
    search_fields = ('product__sku', 'product__name', 'name', 'account__name',)
    resource_class = ProductModifierResource

    list_filter = ('archived',)
    inlines = (
        ProductModifierOptionInline,
    )


class ProductModifierOptionAdmin(ImportExportModelAdmin):
    '''
        Admin View for ProductModifierOption
    '''
    list_display = ('product_modifier', 'value', 'account')
    search_fields = (
        'product_modifier__product__sku',
        'product_modifier__product__name', 'value',
        'account__name',)
    resource_class = ProductModifierOptionResource


class ProductModifierUsageAdmin(ImportExportModelAdmin):
    '''
        Admin View for ProductModifierUsage
    '''
    list_display = ('product_modifier', 'product', 'account')
    search_fields = (
        'product__sku',
        'product_modifier__name',
        'account__name',)
    resource_class = ProductModifierUsageResource


admin.site.register(Catalogue, CatalogueAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(UOM, UOMAdmin)
admin.site.register(ProductType, ProductTypeAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ProductModifier, ProductModifierAdmin)
admin.site.register(ProductModifierOption, ProductModifierOptionAdmin)
admin.site.register(ProductAddOn, ProductAddOnAdmin)
admin.site.register(CompositeProduct, CompositeProductAdmin)
admin.site.register(ProductModifierUsage, ProductModifierUsageAdmin)
