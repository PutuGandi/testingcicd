from django.conf import settings
from django.db import (
    models,
    transaction,
    DataError,
    IntegrityError,
)
from django.core.exceptions import ValidationError

from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from safedelete.models import HARD_DELETE
from django.core.exceptions import ObjectDoesNotExist

from model_utils import Choices
from model_utils.models import TimeStampedModel

from djmoney.money import Money
from djmoney.models.fields import MoneyField
from treebeard.mp_tree import MP_Node
from sterlingpos.core.models import (
    SterlingTenantModel, SterlingTenantForeignKey,
    get_current_tenant, set_current_tenant
)
from .storage import OverwriteStorage


def get_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = "{}_{}.{}".format(slugify(instance.sku[:50]), slugify(instance.name[:20]), ext)
    return '{}/products/{}'.format(instance.account.pk, new_filename)


def get_catalogue_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = "{}_{}.{}".format(instance.pk, slugify(instance.name[:20]), ext)
    return '{}/products/images/{}'.format(instance.account.pk, new_filename)


class UOM(SterlingTenantModel, TimeStampedModel):
    UOM_CATEGORY = Choices(
        ("units", _("Units")),
        ("volume", _("Volume")),
        ("weight", _("Weight"))
    )

    UOM_TYPE = Choices(
        ("base", _("Reference unit for this measure")),
        ("smaller", _("Smaller than reference unit")),
        ("bigger", _("Bigger than reference unit of measure")),
    )

    unit = models.CharField(_("Unit"), null=True, max_length=120)
    category = models.CharField(
        _("Category"),
        choices=UOM_CATEGORY,
        default=UOM_CATEGORY.units,
        max_length=50
    )
    uom_type = models.CharField(
        _("Type"),
        choices=UOM_TYPE,
        default=UOM_TYPE.base,
        max_length=50
    )
    ratio = models.DecimalField(_("Ratio"), max_digits=19, decimal_places=3)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "UOM"
        verbose_name_plural = "UOMs"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return self.unit


class ProductType(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(max_length=120)

    class Meta:
        verbose_name = "ProductType"
        verbose_name_plural = "ProductTypes"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return self.name


class Category(SterlingTenantModel, MP_Node, TimeStampedModel):

    CATEGORY_CLASSIFICATION = Choices(
        ('All', 'all', _('All')),
        ('Composite', 'composite', _('Composite')),
        ('Complementary', 'complementary', _('Complementary')),
    )

    name = models.CharField(_('Name'), max_length=255, db_index=True)
    description = models.TextField(_('Description'), blank=True)
    slug = models.SlugField(_('Slug'), max_length=255, db_index=True)

    classification = models.CharField(
        max_length=120, choices=CATEGORY_CLASSIFICATION,
        default=CATEGORY_CLASSIFICATION.all)

    archived = models.BooleanField(default=False)

    parent_path = models.CharField(
        _('Parent Path'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
        ordering = ['path']
        constraints = [
            models. UniqueConstraint(
                fields=["account", "id",],
                name="unique_category_account"),
            models. UniqueConstraint(
                fields=[
                    "account", "name",
                    "depth", "parent_path",
                    ],
                condition=models.Q(deleted__isnull=True),
                name="unique_category"),
        ]

    _slug_separator = '/'
    _full_name_separator = ' > '
    _parent_path = None
    _classification = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._classification = self.classification

    def __str__(self):
        return self.full_name

    def validate_unique(self, exclude=None):
        sid = transaction.savepoint()
        with transaction.atomic():
            try:
                if self.pk and self._classification != self.classification:
                    self.save()
                    self = self.move_to_composite_parent()          
            except (IntegrityError, ValidationError) as e:
                raise ValidationError(_("Unable to move category."))
        super().validate_unique(exclude)
        unique_checks = [(
            self.__class__,
            ('account', 'name', 'depth', 'parent_path')
        )]
        errors = self._perform_unique_checks(unique_checks)
        if errors:
            raise ValidationError(_("This Category already exists."))
        transaction.savepoint_rollback(sid)

    def move_to_composite_parent(self):

        if self.classification == "Composite":
            paketan = Category.get_composite_root()

            try:
                self.move(paketan, 'last-child')
            except AttributeError:
                paketan.numchild = paketan.get_children().count()
                paketan.save()
                self.move_to_composite_parent()
        else:
            curr_account = get_current_tenant()
            set_current_tenant(None)
            self.move(Category.get_last_root_node())
            if curr_account:
                set_current_tenant(curr_account)
        self = Category.objects.get(pk=self.pk)
        return self

    @classmethod
    def get_composite_root(cls):
        try:
            paketan = cls.objects.get(
                name='Paketan', depth=1,
                classification=cls.CATEGORY_CLASSIFICATION.composite)
        except cls.DoesNotExist:
            paketan = cls.add_root(
                name='Paketan',
                classification=cls.CATEGORY_CLASSIFICATION.composite)
        return paketan

    @classmethod
    def get_root_nodes(cls):
        _current_tenant = get_current_tenant()
        set_current_tenant(None)
        qs = cls.all_objects.filter(depth=1)
        set_current_tenant(_current_tenant)
        return qs

    def get_ancestors_and_self(self):
        return list(self.get_ancestors()) + [self]

    def get_children(self):
        if self.is_leaf():
            return self.__class__.all_objects.none()
        return self.__class__.all_objects.filter(
                depth=self.depth + 1,
                path__range=self._get_children_path_interval(self.path)
        ).order_by(
            'path'
        )

    def get_siblings(self):
        qset = self.__class__.all_objects.filter(
            depth=self.depth
        ).order_by(
            'path'
        )

        if self.depth > 1:
            # making sure the non-root nodes share a parent
            parentpath = self._get_basepath(self.path, self.depth - 1)
            qset = qset.filter(
                path__range=self._get_children_path_interval(parentpath))
        return qset

    @property
    def full_name(self):
        names = [category.name for category in self.get_ancestors_and_self()]
        return self._full_name_separator.join(names)

    @property
    def full_slug(self):
        slugs = [category.slug for category in self.get_ancestors_and_self()]
        return self._slug_separator.join(slugs)

    def generate_slug(self):
        return slugify(self.name)

    def ensure_slug_uniqueness(self):
        unique_slug = self.slug
        siblings = self.get_siblings().exclude(pk=self.pk)
        next_num = 2
        while siblings.filter(slug=unique_slug).exists():
            unique_slug = '{slug}_{end}'.format(slug=self.slug, end=next_num)
            next_num += 1

        if unique_slug != self.slug:
            self.slug = unique_slug
            self.save()

    def move(self, target, pos=None):
        try:
            with transaction.atomic():
                super().move(target, pos)
                obj = Category.objects.get(pk=self.pk)
                obj.parent_path = obj.path[:4*obj.depth-1]
                obj.save()
                return obj
        except IntegrityError as e:
            raise e

    def set_parent_path(self):
        if self._parent_path and self._parent_path != "----":
            return self._parent_path
        parent_path = self.path[:4*(self.depth-1)]
        return parent_path

    def save(self, *args, **kwargs):
        self.parent_path = self.set_parent_path()
        if self.slug:
            super(Category, self).save(*args, **kwargs)
        else:
            self.slug = self.generate_slug()
            super(Category, self).save(*args, **kwargs)
            self.ensure_slug_uniqueness()
        return self


class Catalogue(SterlingTenantModel, TimeStampedModel):

    PRODUCT_CLASSIFICATION = Choices(
        ('Standart', 'standart', _('Standart')),
        ('Non-Inventory', 'non_inventory', _('Non-Inventory')),
        ('Composite', 'composite', _('Composite')),
        ('Dynamic Composite', 'dynamic_composite', _('Dynamic Composite')),
        ('Serialized', 'serialized', _('Serialized')),
        ('Manufactured', 'manufactured', _('Manufactured')),
        ('Complementary', 'complementary', _('Complementary')),
        ('Multiple-UOM', 'multiple_uom', _('Multiple-UOM')),
    )

    classification = models.CharField(
        max_length=120, choices=PRODUCT_CLASSIFICATION,
        default=PRODUCT_CLASSIFICATION.standart)

    name = models.CharField(_('Name'), max_length=120)

    uom = SterlingTenantForeignKey('catalogue.UOM', blank=True, null=True, on_delete=models.CASCADE)
    category = SterlingTenantForeignKey(
        "catalogue.Category", blank=True,
        null=True, on_delete=models.CASCADE)
    image_location = models.ImageField(
        upload_to=get_catalogue_upload_path,
        storage=OverwriteStorage(), blank=True, null=True, 
        max_length=255)

    archived = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_manage_stock = models.BooleanField(default=False)
    is_sellable = models.BooleanField(default=True)
    is_batch_tracked = models.BooleanField(default=False)

    old_product_pk = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = "Catalogue"
        verbose_name_plural = "Catalogues"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return self.name

    @property
    def price_range(self):
        qs = self.variants.values_list('price', flat=True)
        max_value = Money(max(qs, default=0), "IDR")
        min_value = Money(min(qs, default=0), "IDR")
        if max_value == min_value:
            return max_value
        return "{} - {}".format(min_value, max_value)

    @property
    def cost_range(self):
        qs = self.variants.values_list('cost', flat=True)
        max_value = Money(max(qs, default=0), "IDR")
        min_value = Money(min(qs, default=0), "IDR")
        if max_value == min_value:
            return max_value
        return "{} - {}".format(min_value, max_value)

    @classmethod
    def prepare_catalogue(cls, product):
        def __prepare_catalogue(product):
            if product.parent:
                catalogue = __prepare_catalogue(
                    product.parent)
            else:
                catalogue = product.catalogue

            if not catalogue:
                data = {
                    "name": product.name,
                    "category": product.category,
                    "classification": product.classification,
                    "image_location": product.image_location,
                    "account": product.account,
                    "is_sellable": product.is_sellable,
                    "is_manage_stock": product.is_manage_stock,
                    "is_batch_tracked": product.is_batch_tracked,
                    "uom": product.uom,
                }
                catalogue = cls.objects.create(**data)
            catalogue.variants.add(product)

            return catalogue

        return __prepare_catalogue(product)


class Product(SterlingTenantModel, TimeStampedModel):

    PRODUCT_CLASSIFICATION = Choices(
        ('Standart', 'standart', _('Standart')),
        ('Non-Inventory', 'non_inventory', _('Non-Inventory')),
        ('Composite', 'composite', _('Composite')),
        ('Dynamic-Composite', 'dynamic_composite', _('Dynamic Composite')),
        ('Serialized', 'serialized', _('Serialized')),
        ('Manufactured', 'manufactured', _('Manufactured')),
        ('Complementary', 'complementary', _('Complementary')),
        ('Multiple-UOM', 'multiple_uom', _('Multiple-UOM')),
    )

    classification = models.CharField(
        max_length=120, choices=PRODUCT_CLASSIFICATION,
        default=PRODUCT_CLASSIFICATION.standart)
    sku = models.CharField(_('SKU'), max_length=120)
    name = models.CharField(_('Name'), max_length=120)

    cost = MoneyField(
        _('Cost'),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')
    moving_average_cost = MoneyField(
        _('Moving Average Cost'),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR',
        default=0.0)
    price = MoneyField(
        _('Price'),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')

    uom = SterlingTenantForeignKey('catalogue.UOM', blank=True, null=True, on_delete=models.CASCADE)
    category = SterlingTenantForeignKey("catalogue.Category", blank=True, null=True, on_delete=models.CASCADE)
    product_type = SterlingTenantForeignKey("catalogue.ProductType", blank=True, null=True, on_delete=models.CASCADE)

    parent = SterlingTenantForeignKey(
        "self", 
        related_name='variants',
        blank=True, null=True, on_delete=models.CASCADE)

    catalogue = SterlingTenantForeignKey(
        Catalogue,
        related_name='variants',
        blank=True, null=True, on_delete=models.CASCADE)

    image_location = models.ImageField(
        upload_to=get_upload_path,
        storage=OverwriteStorage(),
        blank=True, null=True, max_length=255)

    archived = models.BooleanField(default=False)

    is_manage_stock = models.BooleanField(default=False)
    is_sellable = models.BooleanField(default=True)
    is_batch_tracked = models.BooleanField(default=False)

    price_incl_tax = models.BooleanField(default=False)
    released = models.DateTimeField(auto_now_add=True)

    min_stock_alert = models.IntegerField(default=0)
    keep_selling_sold_out = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
        unique_together = (('account', 'id'),)

    def __str__(self):
        if self.catalogue and self.catalogue.name not in self.name:
            return f"{self.catalogue} - {self.name} - {self.sku}"
        return f"{self.name} - {self.sku}"

    def create_missing_batch(self):
        missing_name = "Missing Batch"
        missing_batch, _ = self.batches.get_or_create(
            is_missing_batch=True, batch_id=missing_name
        )
        return missing_batch

    def update_missing_batch(self, batch_outlet, balance):
        if balance < 0:
            batch_outlet.substract_batch_balance(
                balance,
                unit=self.uom,
                source_object=None,
            )
        else:
            batch_outlet.set_batch_balance(
                balance,
                unit=self.uom,
                source_object=None,
            )

    def check_batches(self):
        stocks = self.stocks.all()
        batches = self.batches.all()
        if batches:
            missing_batch = None
            for stock in stocks:
                balance = stock.balance
                outlet = stock.outlet
                total_balance = 0
                for batch in batches:
                    try:
                        total_balance += batch.batch_items.get(outlet=outlet).balance
                    except ObjectDoesNotExist:
                        pass
                balance -= total_balance
                if balance != 0:
                    missing_batch = missing_batch if missing_batch else self.create_missing_batch()
                    batch_outlet, _ = missing_batch.batch_items.get_or_create(
                        outlet=outlet
                    )
                    self.update_missing_batch(batch_outlet, balance)
        else:
            for stock in stocks:
                balance = stock.balance
                outlet = stock.outlet
                if stock.balance != 0:
                    missing_batch = self.create_missing_batch()
                    batch_outlet = missing_batch.batch_items.create(
                        outlet=outlet
                    )
                    self.update_missing_batch(batch_outlet, balance)

    def save(self, *args, **kwargs):
        if self.variants.exists() and self.parent is not None:
            raise DataError(
                _("Product with variants cannot own a parent product and Variants cannot be a parent product"))
        if self.pk and self.is_manage_stock and self.is_batch_tracked and not self.bom.count():
            self.check_batches()
        return super(Product, self).save(*args, **kwargs)

    @property
    def display_name(self):
        if self.catalogue and self.catalogue.name not in self.name:
            displayed_name = f"{self.catalogue.name} {self.name}"
        displayed_name = self.name
        return displayed_name

class ProductAvailability(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    product = SterlingTenantForeignKey(
        'catalogue.Product',
        related_name='availability', on_delete=models.CASCADE)
    outlet = SterlingTenantForeignKey(
        'outlet.Outlet',
        related_name='product_availability', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "ProductAvailability"
        verbose_name_plural = "ProductAvailabilities"
        unique_together = (('account', 'id'),)
        ordering = ("outlet__name", )


class ProductPricing(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    product = SterlingTenantForeignKey(
        'catalogue.Product',
        related_name='product_pricing', on_delete=models.CASCADE)
    outlet = SterlingTenantForeignKey(
        'outlet.Outlet',
        related_name='outlet_pricing', on_delete=models.CASCADE)

    is_active = models.BooleanField(default=False)

    price = MoneyField(
        _('Price'),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')

    class Meta:
        verbose_name = "ProductPricing"
        verbose_name_plural = "ProductPricings"
        unique_together = (('account', 'id'),)
        ordering = ("outlet__name", )


class ProductAddOn(SterlingTenantModel, TimeStampedModel):
    """
    ProductAddOn must be a Product with classification as Complementary
    """
    _safedelete_policy = HARD_DELETE

    product = SterlingTenantForeignKey(
        'catalogue.Product', related_name='addon_usage', on_delete=models.CASCADE)

    catalogue_usage = SterlingTenantForeignKey(
        'catalogue.Catalogue',
        blank=True, null=True,
        related_name='addon', on_delete=models.CASCADE)

    product_usage = SterlingTenantForeignKey(
        'catalogue.Product', related_name='addon', on_delete=models.CASCADE)
    archived = models.BooleanField(default=False)

    class Meta:
        verbose_name = "ProductAddOn"
        verbose_name_plural = "ProductAddOns"
        unique_together = (('account', 'id'),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "Product AddOn {} for {}".format(
            self.product.sku, self.product_usage.sku)

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)

        return string

    def save(self, *args, **kwargs):
        if self.product.classification != Product.PRODUCT_CLASSIFICATION.complementary:
            raise DataError(
                _("Only Product with Complementary classification can be Product AddOn"))
        return super(ProductAddOn, self).save(*args, **kwargs)


class ProductModifierUsage(SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    product = SterlingTenantForeignKey(
        'catalogue.Product', on_delete=models.CASCADE)
    product_modifier = SterlingTenantForeignKey(
        'catalogue.ProductModifier', related_name='usages', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "ProductModifierUsage"
        verbose_name_plural = "ProductModifierUsages"
        unique_together = (('account', 'id'),)


class ProductModifier(SterlingTenantModel, TimeStampedModel):
    """
    ProductModifier must be a Product with classification as Complementary
    """
    products = models.ManyToManyField(
        "catalogue.Product",
        blank=True,
        through="ProductModifierUsage",
        related_name='modifiers')

    product = SterlingTenantForeignKey(
        'catalogue.Product',
        null=True,
        related_name='modifier', on_delete=models.CASCADE)

    name = models.CharField(_('Modifier Name'), max_length=50)
    archived = models.BooleanField(default=False)

    class Meta:
        verbose_name = "ProductModifier"
        verbose_name_plural = "ProductModifiers"
        unique_together = (('account', 'id'),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "{} ({}) option(s)".format(self.name, self.options.count())

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)

        return string


class ProductModifierOption(SterlingTenantModel, TimeStampedModel):

    product_modifier = SterlingTenantForeignKey(
        'catalogue.ProductModifier', related_name='options', on_delete=models.CASCADE)
    value = models.CharField(_('Modifier Option'), max_length=50)

    class Meta:
        verbose_name = "ProductModifierOption"
        verbose_name_plural = "ProductModifierOptions"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "Product Modifier {} - {}".format(
            self.product_modifier.name, self.value)


class CompositeProduct(SterlingTenantModel, TimeStampedModel):
    group_name = models.CharField(
        _('Group Name'), max_length=120,
        blank=True, null=True)
    
    product = SterlingTenantForeignKey(
        'catalogue.Product',
        related_name='composite_usage',
        null=True, on_delete=models.CASCADE)
    product_list = models.ManyToManyField(
        "catalogue.Product",
        blank=True,
        related_name='composite_usage_list')
    category = SterlingTenantForeignKey(
        'catalogue.Category',
        related_name='composite_category_usage',
        null=True, on_delete=models.CASCADE)
    product_quantity = models.PositiveSmallIntegerField(default=1)
    composite_product = SterlingTenantForeignKey('catalogue.Product', related_name='composite_material', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "CompositeProduct"
        verbose_name_plural = "CompositeProducts"
        unique_together = (('account', 'id'),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        if self.product:
            string = "Composite material {} | {} - {}".format(
                self.composite_product.sku, self.product.sku, self.product_quantity)

        if self.category:
            string = "Composite material {} | Category {} - {}".format(
                self.composite_product.sku, self.category.name, self.product_quantity)

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)

        return string

    def save(self, *args, **kwargs):
        if self.composite_product.classification not in [
                Product.PRODUCT_CLASSIFICATION.composite,
                Product.PRODUCT_CLASSIFICATION.dynamic_composite,
        ]:
            raise DataError(
                _("Only Product with Composite classification can have composite product"))
        return super(CompositeProduct, self).save(*args, **kwargs)
