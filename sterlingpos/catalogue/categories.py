from django_multitenant.utils import get_current_tenant

from sterlingpos.catalogue.models import Category


def create_from_sequence(bits, account=None):
    """
    Create categories from an iterable
    """
    current_tenant = get_current_tenant()

    if current_tenant:
        account = current_tenant

    if len(bits) == 1:
        # Get or create root node
        name = bits[0]
        try:
            # Category names should be unique at the depth=1
            kwargs = {
                Category.tenant_id: getattr(account, 'id', None),
                'depth': 1,
                'name': name
            }
            root = Category.objects.get(**kwargs)
        except Category.DoesNotExist:
            root = Category.add_root(name=name, account=account)
        except Category.MultipleObjectsReturned:
            raise ValueError((
                "There are more than one categories with name "
                "%s at depth=1") % name)
        return [root]
    else:
        parents = create_from_sequence(bits[:-1], account)
        parent, name = parents[-1], bits[-1]
        try:
            kwargs = {
                Category.tenant_id: getattr(account, 'id', None),
                'name': name
            }
            child = parent.get_children().get(**kwargs)
        except Category.DoesNotExist:
            child = parent.add_child(name=name, account=account)
        except Category.MultipleObjectsReturned:
            raise ValueError((
                "There are more than one categories with name "
                "%s which are children of %s") % (name, parent))
        parents.append(child)
        return parents


def create_from_breadcrumbs(breadcrumb_str, separator='>', account=None):
    """
    Create categories from a breadcrumb string
    """
    category_names = [x.strip() for x in breadcrumb_str.split(separator)]
    categories = create_from_sequence(category_names, account)
    return categories[-1]
