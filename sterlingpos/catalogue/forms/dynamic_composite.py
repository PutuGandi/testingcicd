from django.conf import settings
from django import forms
from django.forms import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string

from djmoney.money import Money
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, TEMPLATE_PACK
from crispy_forms.bootstrap import PrependedText
from dal import autocomplete
from djmoney.forms.fields import MoneyField

from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from sterlingpos.core.widgets import MoneyInputField
from sterlingpos.core.models import get_current_tenant, set_current_tenant

from sterlingpos.catalogue.models import (
    Product, Catalogue,
    CompositeProduct, Category
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs
from sterlingpos.catalogue.forms.composite import (
    CompositeProductForm,
    CompositeMaterialProductForm,
)

MAX_UPLOAD_SIZE = 4194304  # 4MB


class DynamicCompositeProductForm(CompositeProductForm):

    def save(self, *args, **kwargs):
        obj = super(
            CompositeProductForm, self).save(commit=False)
        obj.classification = Product.PRODUCT_CLASSIFICATION.dynamic_composite
        obj.save()

        if not obj.catalogue:
            obj.catalogue = Catalogue.prepare_catalogue(obj)
        if obj.catalogue:
            Catalogue.objects.filter(
                pk=obj.catalogue.pk
            ).invalidated_update(
                name=obj.name,
                category=obj.category,
                image_location=obj.image_location,
            )

        return obj


class DynamicCompositeMaterialFormHelper(FormHelper):
    form_tag = False
    include_media = False
    layout = Layout(
        KawnField("id"),
        KawnField(
            "DELETE",
            template="bootstrap4/formset_delete_field.html",
        ),
        KawnField("group_name"),
        KawnField("composite_type"),
        KawnField(
            "product_quantity",
            css_class="col-2",
        ),
        KawnField("product_list"),
        KawnField("category"),
    )


class DynamicCompositeMaterialForm(forms.ModelForm):
    COMPOSITE_TYPE = [
        ("product", _("Product")),
        ("category", _("Category")),
    ]
    composite_type = forms.ChoiceField(
        label=_("Dynamic Type"),
        choices=COMPOSITE_TYPE,
        initial=COMPOSITE_TYPE[0][0],
        widget=forms.widgets.RadioSelect(
            attrs={
                "class": "composite-type-select",
            }
        ),
        required=False)    
    
    helper = DynamicCompositeMaterialFormHelper()

    class Meta:
        model = CompositeProduct
        fields = '__all__'
        exclude = ('account', 'price',)
        widgets = {
            'product_list': autocomplete.ModelSelect2Multiple(
                url='catalogue:product_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },               
            ),
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_autocomplete_read_only',
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["group_name"].required = True
        self.fields['product'].required = False
        self.fields['product_list'].required = False
        self.fields['product_list'].label = _('Product')
        self.fields['product_list'].queryset = Product.objects.exclude(
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.composite,
                Product.PRODUCT_CLASSIFICATION.dynamic_composite,
                Product.PRODUCT_CLASSIFICATION.complementary,
            ]
        )
        
        self.fields['category'].required = False
        self.fields['category'].label = _('Category')
        self.fields['product_quantity'].label = _('Product Quantity')

        instance = kwargs.get('instance', None)
        if instance and instance.pk:
            self.initial['product_quantity'] = int(instance.product_quantity)

            if instance.product:
                self.initial['composite_type'] = "product"
            if instance.category:
                self.initial['composite_type'] = "category"

    def clean(self):
        composite_type = self.cleaned_data.get("composite_type")

        if composite_type == "category":
            self.fields['category'].required = True

        cleaned_data = super().clean()

        product_qs = self.cleaned_data.get("product_list")

        if composite_type == "product" and not product_qs.exists():
            self.add_error(
                "product_list",
                forms.ValidationError(
                    self.fields["product_list"].error_messages['list'])) 

        if cleaned_data.get("product_list") and cleaned_data.get("category"):
            raise forms.ValidationError(
                _("You can only choose either product or dynamic product by category."),
                code="invalid_composite",
            )
        return cleaned_data


BaseDynamicCompositeMaterialFormSet = inlineformset_factory(
    Product, CompositeProduct,
    fk_name='composite_product',
    form=DynamicCompositeMaterialForm,
    min_num=1, extra=0, can_delete=True
)


class DynamicCompositeMaterialFormSet(BaseDynamicCompositeMaterialFormSet):

    def clean(self):
        cleaned_data = super().clean()

        if self.errors:
            return cleaned_data

        composite_list = [x for x in self.cleaned_data if not x["DELETE"]]

        if not composite_list:
            raise forms.ValidationError(
                _("Dynamic Composite should have at least one composite material.")
            )
        return cleaned_data

    @property
    def initial_forms(self):
       return self.forms[:self.get_queryset().count()]

    @property
    def extra_forms(self):
       return self.forms[self.get_queryset().count():]

    def initial_form_count(self):
        return len(self.get_queryset())


class DynamicCompositeMaterialFormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.tabular_formset = True
        self.form_id = "product-composite-form"
        self.template = "bootstrap4/tabular_formset_with_help.html"
        self.title = _("Dynamic Composite Material")
        self.add_content = _("Add Product")
