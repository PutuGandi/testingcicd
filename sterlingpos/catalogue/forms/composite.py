from django.conf import settings
from django import forms
from django.forms import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string

from djmoney.money import Money
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, TEMPLATE_PACK
from crispy_forms.bootstrap import PrependedText
from dal import autocomplete
from djmoney.forms.fields import MoneyField

from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from sterlingpos.core.widgets import MoneyInputField
from sterlingpos.core.models import get_current_tenant, set_current_tenant

from sterlingpos.catalogue.models import (
    Product, Catalogue, ProductAddOn,
    ProductPricing, ProductAvailability,
    ProductModifier, ProductModifierUsage, ProductModifierOption,
    CompositeProduct, Category
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs

MAX_UPLOAD_SIZE = 4194304  # 4MB


class CompositeProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = (
            "name",
            "sku",
            "category",
            "price",
            "cost",
            "is_sellable",
            "image_location"
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_composite_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):
        super(CompositeProductForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.filter(
            classification=Category.CATEGORY_CLASSIFICATION.composite,
        )
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields["is_sellable"].label = _("Sellable")
        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("sku"), css_class="col"),
                        Div(KawnField("category"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
            ),
        )

    def clean_price(self):
        price = self.cleaned_data['price']
        if price.amount and price.amount <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return price

    def clean_cost(self):
        cost = self.cleaned_data['cost']
        if cost.amount and cost.amount <= 0:
            raise forms.ValidationError("Cost value must be greater than 0.")
        return cost

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def save(self, *args, **kwargs):
        obj = super().save(commit=False)
        obj.classification = Product.PRODUCT_CLASSIFICATION.composite
        obj.save()

        if not obj.catalogue:
            obj.catalogue = Catalogue.prepare_catalogue(obj)

        return obj


class CompositeMaterialProductForm(forms.ModelForm):
    product_quantity = forms.CharField(widget=forms.TextInput)
    price = forms.DecimalField(
        widget=forms.TextInput(attrs={'disabled': True}),
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )

    class Meta:
        model = CompositeProduct
        fields = '__all__'
        exclude = ('account', 'price', 'category')
        widgets = {
            'product': autocomplete.ModelSelect2(
                url='catalogue:product_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields['product'].label = _('Product')
        self.fields['price'].label = _('Price')
        self.fields['product_quantity'].label = _('Product Quantity')
        self.fields['price'].required = False
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.initial['price'] = kwargs['instance'].product.price
            self.initial['product_quantity'] = int(kwargs['instance'].product_quantity)


BaseCompositeMaterialProductFormFormSet = inlineformset_factory(
    Product, CompositeProduct,
    fk_name='composite_product',
    form=CompositeMaterialProductForm, extra=0, can_delete=True)


class CompositeMaterialProductFormFormSet(BaseCompositeMaterialProductFormFormSet):

    @property
    def initial_forms(self):
       return self.forms[:self.get_queryset().count()]

    @property
    def extra_forms(self):
       return self.forms[self.get_queryset().count():]

    def initial_form_count(self):
        return len(self.get_queryset())


class ProductCompositeMaterialForm(forms.ModelForm):
    price = forms.DecimalField(
        widget=forms.TextInput(attrs={'disabled': True, "class": "product-price"}),
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        required=False)

    class Meta:
        model = CompositeProduct
        fields = ("product", "product_quantity")
        widgets = {
            'product': autocomplete.ModelSelect2(
                url='catalogue:product_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "product_quantity": forms.TextInput(),
        }
    field_order = ["product", "price", "product_quantity"]

    def __init__(self, *args, **kwargs):
        super(ProductCompositeMaterialForm, self).__init__(*args, **kwargs)
        self.fields["product"].widget.attrs["class"] = "product-select"
        self.empty_permitted = False
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.initial['price'] = kwargs['instance'].product.price


class ProductCompositeMaterialFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ProductCompositeMaterialFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "product-composite-form"
        self.template = "bootstrap4/formset_with_help.html"
        self.title = _("Product Composite Material")
        self.add_content = _("Add Product")


ProductCompositeMaterialFormset = inlineformset_factory(
    Product, CompositeProduct, fk_name='composite_product', form=ProductCompositeMaterialForm, min_num=1, extra=0, can_delete=True
)
