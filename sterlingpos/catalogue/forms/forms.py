from django.conf import settings
from django.db import transaction
from django import forms
from django.forms import inlineformset_factory
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe

from djmoney.money import Money
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, TEMPLATE_PACK
from dal import autocomplete
from djmoney.forms.fields import MoneyField

from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from sterlingpos.core.widgets import MoneyInputField
from sterlingpos.core.models import get_current_tenant, set_current_tenant

from sterlingpos.catalogue.models import (
    Product, Catalogue, ProductAddOn,
    ProductPricing, ProductAvailability,
    ProductModifier, ProductModifierUsage, ProductModifierOption,
    CompositeProduct, Category, UOM
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs

MAX_UPLOAD_SIZE = 4194304  # 4MB


class BaseFormsetLayout(object):
    template = '%s/formset.html' % TEMPLATE_PACK

    def __init__(self, formset, context, template=None):
        self.formset = formset
        self.context = context
        if template:
            self.template = template

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, **kwargs):
        return render_to_string(
            self.template,
            {'wrapper': self, 'formset': self.formset, 'context': self.context})


class CatalogueForm(forms.ModelForm):

    class Meta:
        model = Catalogue
        fields = (
            "name",
            "category",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "image_location",
            "uom",
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "uom": autocomplete.ModelSelect2(
                url="catalogue:uom_autocomplete",
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        super(CatalogueForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields['category'].queryset = Category.objects.all()
        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")

        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))
        self.fields["uom"].label = _("Unit of Measure")
        self.fields["uom"].required = False
        self.fields["uom"].help_text = _("Semua varian akan mengikuti satuan pada produk ini")
        self.fields["uom"].queryset = UOM.objects.filter(is_active=True).order_by("category")

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                KawnField("category"),
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
                KawnField("is_manage_stock"),
                KawnField("is_batch_tracked"),
                Div(
                    Div(
                        Div(KawnField("uom"), css_class="col"),
                        Div(css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
        )

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            if instance.uom:
                self.fields["uom"].queryset = UOM.objects.filter(category=instance.uom.category, is_active=True).order_by("category")
                self.fields["uom"].widget._url = reverse_lazy("catalogue:uom_catalogue_autocomplete", kwargs={"pk": instance.pk})

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image
    
    def clean_uom(self):
        uom = self.cleaned_data.get("uom")
        manage_stock = self.cleaned_data.get("is_manage_stock")

        if self.instance.classification == Catalogue.PRODUCT_CLASSIFICATION.manufactured:
            if not uom:
                raise forms.ValidationError(_("This field is required."))

        if manage_stock:
            if not uom:
                raise forms.ValidationError(_("This field is required."))
        return uom


class CatalogueCreateForm(forms.ModelForm):
    keep_selling_sold_out = forms.BooleanField(required=False)
    min_stock_alert = forms.IntegerField(required=False)
    sku = forms.CharField(max_length=100, required=True)
    cost = MoneyField(widget=MoneyInputField(default_currency="IDR", currency_widget=forms.HiddenInput))
    price = MoneyField(widget=MoneyInputField(default_currency="IDR", currency_widget=forms.HiddenInput))
    modifiers = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_modifier_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=ProductModifier.objects.none()
    )
    product_modifiers = forms.BooleanField(initial=False, label=_("This product have modifiers"))

    class Meta:
        model = Catalogue
        fields = (
            "name",
            "category",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "image_location",
            "modifiers",
            "product_modifiers",
            "uom",
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "uom": autocomplete.ModelSelect2(
                url="catalogue:uom_autocomplete",
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        super(CatalogueCreateForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields['category'].queryset = Category.objects.all()
        self.fields['modifiers'].queryset = ProductModifier.objects.filter(
            archived=False)
        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")
        self.fields["keep_selling_sold_out"].label = _("Keep Selling Past 0")
        self.fields["min_stock_alert"].label = _("Alert at Stock")
        self.fields["min_stock_alert"].required = False
        self.fields["product_modifiers"].required = False

        self.fields["price"].initial = [0, "IDR"]
        self.fields["cost"].initial = [0, "IDR"]
        self.fields["min_stock_alert"].initial = 0

        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))
        self.fields["uom"].label = _("Unit of Measure")
        self.fields["uom"].help_text = _("Semua varian akan mengikuti satuan pada produk ini")
        self.fields["uom"].queryset = UOM.objects.filter(is_active=True).order_by("category")

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("sku"), css_class="col"),
                        Div(KawnField("category"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
                KawnField("is_manage_stock"),
                KawnField("is_batch_tracked"),
                KawnField("keep_selling_sold_out"),
                Div(
                    Div(
                        Div(KawnField("min_stock_alert"), css_class="col"),
                        Div(KawnField("uom"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField("product_modifiers"),
                KawnField("modifiers"),
                form_grid="col-lg-7",
                css_id='product_modifiers',
            )
        )

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def clean_uom(self):
        uom = self.cleaned_data.get("uom")
        manage_stock = self.cleaned_data.get("is_manage_stock")

        if manage_stock:
            if not uom:
                raise forms.ValidationError(_("This field is required."))
        return uom

    def clean_min_stock_alert(self):
        min_stock_alert = self.cleaned_data.get("min_stock_alert")
        if not min_stock_alert or min_stock_alert < 0:
            min_stock_alert = 0

        manage_stock = self.cleaned_data.get("is_manage_stock")

        if manage_stock:
            if not min_stock_alert:
                if not min_stock_alert == 0:
                    raise forms.ValidationError(_("This field is required."))            
        return min_stock_alert


class CatalogueManufacturedCreateForm(forms.ModelForm):
    keep_selling_sold_out = forms.BooleanField(required=False, label=_("Keep Selling Past 0"))
    min_stock_alert = forms.IntegerField(required=False, label=_("Alert at Stock"))
    sku = forms.CharField(max_length=100, required=True)
    cost = MoneyField(widget=MoneyInputField(default_currency="IDR", currency_widget=forms.HiddenInput))
    price = MoneyField(widget=MoneyInputField(default_currency="IDR", currency_widget=forms.HiddenInput))
    modifiers = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_modifier_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=ProductModifier.objects.none()
    )
    product_modifiers = forms.BooleanField(initial=False, label=_("This product have modifiers"))

    class Meta:
        model = Catalogue
        fields = (
            "name",
            "category",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "image_location",
            "uom"
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "uom": autocomplete.ModelSelect2(
                url="catalogue:uom_autocomplete",
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        super(CatalogueManufacturedCreateForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields['category'].queryset = Category.objects.all()
        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")
        self.fields["keep_selling_sold_out"].label = _("Keep Selling Past 0")
        self.fields["min_stock_alert"].label = _("Alert at Stock")
        self.fields["min_stock_alert"].required = False
        self.fields["is_manage_stock"].initial = True

        self.fields["product_modifiers"].required = False
        self.fields['modifiers'].queryset = ProductModifier.objects.filter(
            archived=False)

        self.fields["price"].initial = [0, "IDR"]
        self.fields["cost"].initial = [0, "IDR"]
        self.fields["min_stock_alert"].initial = 0

        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))
        self.fields["uom"].label = _("Unit of Measure")
        self.fields["uom"].help_text = _("Semua varian akan mengikuti satuan pada produk ini")
        self.fields["uom"].queryset = UOM.objects.filter(is_active=True).order_by("category")

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("sku"), css_class="col"),
                        Div(KawnField("category"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
                KawnField("is_manage_stock"),
                KawnField("is_batch_tracked"),
                KawnField("keep_selling_sold_out"),
                Div(
                    Div(
                        Div(KawnField("min_stock_alert"), css_class="col"),
                        Div(KawnField("uom"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField("product_modifiers"),
                KawnField("modifiers"),
                form_grid="col-lg-7",
                css_id='product_modifiers',
            )
        )

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def clean_min_stock_alert(self):
        min_stock_alert = self.cleaned_data.get("min_stock_alert")
        if not min_stock_alert:
            if not min_stock_alert == 0:
                raise forms.ValidationError(_("This field is required."))
        return min_stock_alert

    def clean_uom(self):
        uom = self.cleaned_data["uom"]
        if not uom:
            raise forms.ValidationError(_("Manufactured Product must has Unit of Measure"))
        return uom


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = (
            "name",
            "sku",
            "category",
            "price",
            "cost",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "keep_selling_sold_out",
            "min_stock_alert",
            "image_location",
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields['category'].queryset = Category.objects.all()
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["keep_selling_sold_out"].label = _("Keep Selling Past 0")
        self.fields["min_stock_alert"].label = _("Alert at Stock")
        self.fields["min_stock_alert"].required = False

        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")
        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("sku"), css_class="col"),
                        Div(KawnField("category"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
                form_grid="col-lg-7",
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
                KawnField("is_manage_stock"),
                KawnField("is_batch_tracked"),
                KawnField("keep_selling_sold_out"),
                KawnField("min_stock_alert"),
                form_grid="col-lg-7",
            ),
        )

    def clean_price(self):
        price = self.cleaned_data['price']
        if price.amount and price.amount <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return price

    def clean_cost(self):
        cost = self.cleaned_data['cost']
        if cost.amount and cost.amount <= 0:
            raise forms.ValidationError("Cost value must be greater than 0.")
        return cost

    def clean_min_stock_alert(self):
        min_stock_alert = self.cleaned_data.get("min_stock_alert")
        if not min_stock_alert or min_stock_alert < 0:
            min_stock_alert = 0
        manage_stock = self.cleaned_data.get("is_manage_stock")

        if manage_stock:
            if not min_stock_alert:
                if not min_stock_alert == 0:
                    raise forms.ValidationError(_("This field is required."))
        return min_stock_alert

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image


class VariantItemForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = ("name", "sku", "price", "cost")
        widgets = {
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }
    field_order = ["name", "sku", "price", "cost"]

    def __init__(self, *args, **kwargs):
        super(VariantItemForm, self).__init__(*args, **kwargs)
        self.fields["price"].widget.attrs["class"] = "price-data"
        self.fields["cost"].widget.attrs["class"] = "cost-data"

        self.empty_permitted = False

    def clean_price(self):
        price = self.cleaned_data['price']
        if price.amount and price.amount <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return price


class VariantFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(VariantFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "product_variant"
        self.template = "bootstrap4/formset_with_help.html"
        self.collapsible_title = _("This products has multiple variants")
        self.collapsible_field_name = "has_variants"
        self.legend = _("Product Variant")
        self.add_content = _("Add Variant")
        self.form_grid = "col-lg-7"


VariantFormset = inlineformset_factory(
    Catalogue, Product, form=VariantItemForm, min_num=1, extra=0, can_delete=True
)


class VariantForm(forms.ModelForm):
    modifiers = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_modifier_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=ProductModifier.objects.none()
    )
    product_modifiers = forms.BooleanField(initial=False, label=_("This product have modifiers"))

    class Meta:
        model = Product
        fields = (
            "name",
            "sku",
            "price",
            "cost",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "keep_selling_sold_out",
            "min_stock_alert",
            "image_location",
            "modifiers",
            "product_modifiers",
            "uom",
        )
        widgets = {
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "uom": autocomplete.ModelSelect2(
                url="catalogue:uom_autocomplete",
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        catalogue_pk = kwargs.pop("catalogue_pk")
        super(VariantForm, self).__init__(*args, **kwargs)
        self.fields['modifiers'].queryset = ProductModifier.objects.filter(
            archived=False)
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")
        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))
        self.fields["product_modifiers"].required = False
        self.fields["keep_selling_sold_out"].label = _("Keep Selling Past 0")
        self.fields["min_stock_alert"].label = _("Alert at Stock")
        self.fields["min_stock_alert"].required = False

        self.fields["uom"].label = _("Unit of Measure")
        self.fields["uom"].required = False

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                Div(
                    Div(
                        Div(KawnField("name"), css_class="col"),
                        Div(KawnField("sku"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField("product_modifiers"),
                KawnField("modifiers"),
                css_id='product_modifiers',
            )
        )

        catalogue = Catalogue.objects.get(pk=catalogue_pk)
        if catalogue.is_manage_stock and catalogue.uom:
            self.helper.layout[2].insert(2, KawnField("is_manage_stock"))
            self.helper.layout[2].insert(3, KawnField("is_batch_tracked"))
            self.helper.layout[2].insert(4, KawnField("keep_selling_sold_out"))
            self.helper.layout[2].insert(
                5,
                Div(
                    Div(
                        Div(KawnField("min_stock_alert"), css_class="col"),
                        Div(KawnField("uom"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                )
            )
            self.fields["uom"].queryset = UOM.objects.filter(category=catalogue.uom.category, is_active=True).order_by("category")
            self.fields["uom"].initial = catalogue.uom
            self.fields["uom"].widget._url = reverse_lazy("catalogue:uom_catalogue_autocomplete", kwargs={"pk": catalogue_pk})
            self.fields["is_manage_stock"].initial = catalogue.is_manage_stock
            self.fields["is_batch_tracked"].initial = catalogue.is_batch_tracked

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            if instance.is_manage_stock and instance.moving_average_cost.amount and (instance.cost.amount != instance.moving_average_cost.amount):
                self.fields["cost"].help_text = _(mark_safe("Update Cost using <a href='#' data-toggle='modal' data-target='#mac'>Moving Average Cost</a>"))
            self.initial['modifiers'] = instance.modifiers.all()
            if instance.uom:
                catalogue_uom = instance.catalogue.uom
                if catalogue_uom:
                    self.fields["uom"].queryset = UOM.objects.filter(category=catalogue_uom.category, is_active=True).order_by("category")
                else:
                    self.fields["uom"].queryset = UOM.objects.filter(category=instance.uom.category, is_active=True).order_by("category")
                self.fields["uom"].widget._url = reverse_lazy("catalogue:uom_catalogue_autocomplete", kwargs={"pk": catalogue_pk})

    def clean_price(self):
        price = self.cleaned_data['price']
        if price.amount and price.amount <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return price

    def clean_uom(self):
        uom = self.cleaned_data.get("uom")
        manage_stock = self.cleaned_data.get("is_manage_stock")

        if manage_stock:
            if not uom:
                raise forms.ValidationError(_("This field is required."))
        return uom

    def clean_min_stock_alert(self):
        min_stock_alert = self.cleaned_data.get("min_stock_alert")
        if not min_stock_alert or min_stock_alert < 0:
            min_stock_alert = 0
        manage_stock = self.cleaned_data.get("is_manage_stock")

        if manage_stock:
            if not min_stock_alert:
                if not min_stock_alert == 0:
                    raise forms.ValidationError(_("This field is required."))
        return min_stock_alert

    def clean_cost(self):
        cost = self.cleaned_data['cost']
        if cost.amount and cost.amount <= 0:
            raise forms.ValidationError("Cost value must be greater than 0.")
        return cost

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def update_m2m(self, instance, object_list, rel_field):
        if len(object_list) != 0:
            current_mod = rel_field.all()
            add_objects = object_list.exclude(pk__in=current_mod)
            deleted_objects = current_mod.exclude(pk__in=object_list)
            for obj in add_objects:
                obj.products.add(instance, through_defaults={'account': instance.account})
            for obj in deleted_objects:
                obj.products.remove(instance)
        else:
            rel_field.clear()

    def save(self, commit=False):
        modifiers = self.cleaned_data.pop('modifiers')
        uom = self.cleaned_data.pop("uom")

        instance = super(VariantForm, self).save(commit=commit)
        instance.uom = uom
        instance.save()

        self.update_m2m(
            instance,
            modifiers,
            instance.modifiers,
        )

        return instance


class VariantManufacturedForm(forms.ModelForm):
    modifiers = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_modifier_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=ProductModifier.objects.none()
    )
    product_modifiers = forms.BooleanField(initial=False, label=_("This product have modifiers"))

    class Meta:
        model = Product
        fields = (
            "name",
            "sku",
            "price",
            "cost",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "keep_selling_sold_out",
            "min_stock_alert",
            "image_location",
            "uom",
        )
        widgets = {
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "uom": autocomplete.ModelSelect2(
                url="catalogue:uom_autocomplete",
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        catalogue_pk = kwargs.pop("catalogue_pk")
        super(VariantManufacturedForm, self).__init__(*args, **kwargs)
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["is_manage_stock"].initial = True
        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")
        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))
        self.fields['modifiers'].queryset = ProductModifier.objects.filter(
            archived=False)

        catalogue = Catalogue.objects.get(pk=catalogue_pk)
        self.fields["product_modifiers"].required = False
        self.fields["uom"].label = _("Unit of Measure")
        if catalogue.uom:
            self.fields["uom"].queryset = UOM.objects.filter(category=catalogue.uom.category, is_active=True).order_by("category")
            self.fields["uom"].initial = catalogue.uom
            self.fields["uom"].widget._url = reverse_lazy("catalogue:uom_catalogue_autocomplete", kwargs={"pk": catalogue_pk})

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                Div(
                    Div(
                        Div(KawnField("name"), css_class="col"),
                        Div(KawnField("sku"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
                KawnField("is_manage_stock", ),
                KawnField("is_batch_tracked"),
                KawnField("keep_selling_sold_out"),
                Div(
                    Div(
                        Div(KawnField("min_stock_alert"), css_class="col"),
                        Div(KawnField("uom"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField("product_modifiers"),
                KawnField("modifiers"),
                css_id='product_modifiers',
            ),
        )

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            if instance.is_manage_stock and instance.moving_average_cost.amount and (instance.cost.amount != instance.moving_average_cost.amount):
                self.fields["cost"].help_text = _(mark_safe("Update Cost using <a href='#' data-toggle='modal' data-target='#mac'>Moving Average Cost</a>"))
            self.initial['modifiers'] = instance.modifiers.all()
            if instance.uom:
                self.fields["uom"].queryset = UOM.objects.filter(category=instance.uom.category, is_active=True).order_by("category")
                self.fields["uom"].widget._url = reverse_lazy("catalogue:uom_product_autocomplete", kwargs={"pk": instance.pk})

    def clean_price(self):
        price = self.cleaned_data['price']
        if price.amount and price.amount <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return price

    def clean_cost(self):
        cost = self.cleaned_data['cost']
        if cost.amount and cost.amount <= 0:
            raise forms.ValidationError("Cost value must be greater than 0.")
        return cost

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def update_m2m(self, instance, object_list, rel_field):
        if len(object_list) != 0:
            current_mod = rel_field.all()
            add_objects = object_list.exclude(pk__in=current_mod)
            deleted_objects = current_mod.exclude(pk__in=object_list)
            for obj in add_objects:
                obj.products.add(instance, through_defaults={'account': instance.account})
            for obj in deleted_objects:
                obj.products.remove(instance)
        else:
            rel_field.clear()

    def clean_uom(self):
        uom = self.cleaned_data.get("uom")
        if not uom:
            raise forms.ValidationError(_("This field is required."))
        return uom

    def save(self, commit=False):
        modifiers = self.cleaned_data.pop('modifiers')

        instance = super(VariantManufacturedForm, self).save(commit=commit)
        instance.is_manage_stock = True
        instance.save()

        self.update_m2m(
            instance,
            modifiers,
            instance.modifiers,
        )

        return instance


class VariantProductUpdateForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = (
            "name",
            "sku",
            "category",
            "price",
            "cost",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "image_location"
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):
        super(VariantProductUpdateForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields['category'].queryset = Category.objects.all()
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")
        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("sku"), css_class="col"),
                        Div(KawnField("category"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
                KawnField("is_manage_stock"),
                KawnField("is_batch_tracked"),
            ),
        )


class VariantProductForm(forms.ModelForm):
    cost = forms.DecimalField(
        widget=forms.TextInput,
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )
    price = forms.DecimalField(
        widget=forms.TextInput,
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )

    class Meta:
        model = Product
        fields = ('id', 'parent', 'sku', 'name', 'catalogue', 'price', 'cost')


    def clean_parent(self):
        if self.instance.pk and \
            self.instance.variants.exists() and \
                self.cleaned_data.get('parent') is not None or \
                self.cleaned_data.get('parent') and \
                self.cleaned_data.get('parent').parent is not None:
            raise forms.ValidationError(
                _("Product with variants cannot own a parent product and Variants cannot be a parent product"),
                code="invalid_product_parent"
            )
        return self.cleaned_data.get('parent')

    def clean_price(self):
        price = self.cleaned_data['price']
        if price and price <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return Money(price, "IDR")

    def clean_cost(self):
        cost = self.cleaned_data['cost']
        if not cost:
            cost = 0
        return Money(cost, "IDR")

    def __init__(self, *args, **kwargs):
        super(VariantProductForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.fields['price'].label = _('Price')
        self.fields['cost'].label = _('Cost')
        self.fields['cost'].required = False
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.initial['cost'] = int(kwargs['instance'].cost.amount)
            self.initial['price'] = int(kwargs['instance'].price.amount)

    def save(self, commit=True):
        self.instance.category = self.instance.parent.category
        self.instance.catalogue = self.instance.parent.catalogue
        return super().save(commit)


BaseVariantProductFormset = inlineformset_factory(
    Catalogue, Product, form=VariantProductForm,
    extra=0, can_delete=True)


class VariantProductFormset(BaseVariantProductFormset):

    @property
    def initial_forms(self):
       return self.forms[:self.get_queryset().count()]

    @property
    def extra_forms(self):
       return self.forms[self.get_queryset().count():]

    def initial_form_count(self):
        return len(self.get_queryset())


class CompositeProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = (
            "name",
            "sku",
            "category",
            "price",
            "cost",
            "is_sellable",
            "image_location"
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_composite_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):
        super(CompositeProductForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.filter(
            classification=Category.CATEGORY_CLASSIFICATION.composite,
        )
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields["is_sellable"].label = _("Sellable")
        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = "%s" % (_("""
            Drag an image here or browse for an image to upload (Maximum size: 4 MB).
        """))

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("sku"), css_class="col"),
                        Div(KawnField("category"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
            ),
        )

    def clean_price(self):
        price = self.cleaned_data['price']
        if price.amount and price.amount <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return price

    def clean_cost(self):
        cost = self.cleaned_data['cost']
        if cost.amount and cost.amount <= 0:
            raise forms.ValidationError("Cost value must be greater than 0.")
        return cost

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def save(self, *args, **kwargs):
        obj = super().save(commit=False)
        obj.classification = Product.PRODUCT_CLASSIFICATION.composite
        obj.save()

        if not obj.catalogue:
            obj.catalogue = Catalogue.prepare_catalogue(obj)

        return obj


class CompositeMaterialProductForm(forms.ModelForm):
    product_quantity = forms.CharField(widget=forms.TextInput)
    price = forms.DecimalField(
        widget=forms.TextInput(attrs={'disabled': True}),
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )

    class Meta:
        model = CompositeProduct
        fields = '__all__'
        exclude = ('account', 'price', 'category')
        widgets = {
            'product': autocomplete.ModelSelect2(
                url='catalogue:product_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields['product'].label = _('Product')
        self.fields['price'].label = _('Price')
        self.fields['product_quantity'].label = _('Product Quantity')
        self.fields['price'].required = False
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.initial['price'] = kwargs['instance'].product.price
            self.initial['product_quantity'] = int(kwargs['instance'].product_quantity)


class CategoryForm(forms.ModelForm):
    composite_only = forms.BooleanField(
        required=False,
        label=_('Composite Only'),
        help_text=_('Make this category only available for composite product.')
    )

    class Meta:
        model = Category
        fields = ['name', 'description', 'composite_only']

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget.attrs['rows'] = 3
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Category"),
                KawnField("name"),
                KawnField("description"),
                KawnField("composite_only"),
            ),
        )

        if self.instance and self.instance.classification == self.Meta.model.CATEGORY_CLASSIFICATION.composite:
            self.fields['composite_only'].initial = True
        else:
            self.fields['composite_only'].initial = False

    def move_to_composite_parent(self, obj):

        if self.cleaned_data.get('composite_only'):
            paketan = Category.get_composite_root()

            try:
                obj.move(paketan, 'last-child')
            except AttributeError:
                paketan.numchild = paketan.get_children().count()
                paketan.save()
                self.move_to_composite_parent(obj)
        else:
            curr_account = get_current_tenant()
            set_current_tenant(None)
            obj.move(Category.get_last_root_node())
            if curr_account:
                set_current_tenant(curr_account)
        obj = Category.objects.get(pk=obj.pk)
        return obj

    def clean(self):
        cleaned_data = super().clean()
        self.instance.classification = "All"
        if cleaned_data["composite_only"]:
            self.instance.classification = "Composite"
        return cleaned_data

    def save(self, **kwargs):
        obj = super(CategoryForm, self).save(commit=False)

        if self.cleaned_data['composite_only']:
            obj.classification = self.Meta.model.CATEGORY_CLASSIFICATION.composite
            obj._parent_path = "----"
        else:
            obj.classification = self.Meta.model.CATEGORY_CLASSIFICATION.all

        obj.description = self.cleaned_data['description']
        obj.save()

        if 'composite_only' in self.changed_data:
            self.move_to_composite_parent(obj)

        #  Reload the objects due to treebeard caveats
        obj = Category.objects.get(pk=obj.pk)

        return obj


class CategoryCreateForm(CategoryForm):
    class Meta:
        model = Category
        fields = ['name', 'description', 'composite_only']

    def __init__(self, *args, **kwargs):
        super(CategoryCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Category"),
                KawnField("name"),
                KawnField("description"),
                KawnField("composite_only"),
            ),
        )

    def clean(self):
        cleaned_data = super().clean()
        sid = transaction.savepoint()
        with transaction.atomic():
            try:
                category = create_from_breadcrumbs(self.cleaned_data['name'])
                if category and 'composite_only' in self.changed_data:
                    self.move_to_composite_parent(category)          
            except:
                raise forms.ValidationError(_("Unable to move category."))
        transaction.savepoint_rollback(sid)
        return cleaned_data

    def save(self):
        category = create_from_breadcrumbs(self.cleaned_data['name'])
        category.description = self.cleaned_data['description']

        if self.cleaned_data['composite_only']:
            category.classification = self.Meta.model.CATEGORY_CLASSIFICATION.composite
            category.save()
            self.move_to_composite_parent(category)
        else:
            category.classification = self.Meta.model.CATEGORY_CLASSIFICATION.all
            category.save()

        #  Reload the objects due to treebeard caveats
        category = Category.objects.get(pk=category.pk)

        return category


class ProductComplementaryForm(forms.ModelForm):

    product_usages = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )

    class Meta:
        model = Product
        fields = (
            "name",
            "sku",
            "category",
            "price",
            "cost",
            "is_manage_stock",
            "is_sellable",
            "is_batch_tracked",
            "keep_selling_sold_out",
            "min_stock_alert",
            "image_location",
            "uom",
            "product_usages",
        )
        widgets = {
            'category': autocomplete.ModelSelect2(
                url='catalogue:category_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "price": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "cost": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "uom": autocomplete.ModelSelect2(
                url="catalogue:uom_autocomplete",
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['product_usages'].queryset = Product.objects.filter(
            archived=False,
            deleted__isnull=True
        ).exclude(
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.complementary,
                Product.PRODUCT_CLASSIFICATION.composite
            ]
        )
        self.fields['category'].queryset = Category.objects.all()
        self.fields['category'].required = True
        self.fields['category'].label = _('Category')
        self.fields["is_manage_stock"].label = _("Manage Stock")
        self.fields["is_sellable"].label = _("Sellable")
        self.fields["is_batch_tracked"].label = _("Batch Tracked")
        self.fields['image_location'].label = _('Add Image')
        self.fields['image_location'].help_text = _("Drag an image here or browse for an image to upload (Maximum size: 4 MB).")
        self.fields['product_usages'].label = _('Product Usages')
        self.fields["keep_selling_sold_out"].label = _("Keep Selling Past 0")
        self.fields["min_stock_alert"].label = _("Alert at Stock")
        self.fields["min_stock_alert"].required = False

        self.fields["uom"].label = _("Unit of Measure")
        self.fields["uom"].required = False
        self.fields["uom"].queryset = UOM.objects.filter(is_active=True).order_by("category")

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Information"),
                KawnField("name"),
                Div(
                    Div(
                        Div(KawnField("sku"), css_class="col"),
                        Div(KawnField("category"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
                Div(
                    Div(
                        Div(KawnField("price"), css_class="col"),
                        Div(KawnField("cost"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Product Image"),
                KawnField("image_location", template='dashboard/image_upload/image_fields_vertical.html'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Configuration"),
                KawnField("is_sellable"),
                KawnField("is_manage_stock"),
                KawnField("is_batch_tracked"),
                KawnField("keep_selling_sold_out"),
                Div(
                    Div(
                        Div(KawnField("min_stock_alert"), css_class="col"),
                        Div(KawnField("uom"), css_class="col"),
                        css_class="form-row",
                    ),
                    css_class="col-lg-12 px-0 mb-0",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Product Usages"),
                KawnField("product_usages"),
            ),
        )
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['product_usages'].initial = Product.objects.filter(
                addon__product__pk=instance.pk)
            if instance.is_manage_stock and instance.moving_average_cost.amount and (instance.cost.amount != instance.moving_average_cost.amount):
                self.fields["cost"].help_text = _(mark_safe("Update Cost using <a href='#' data-toggle='modal' data-target='#mac'>Moving Average Cost</a>"))
            if instance.uom:
                self.fields["uom"].queryset = UOM.objects.filter(category=instance.uom.category, is_active=True).order_by("category")
                self.fields["uom"].widget._url = reverse_lazy("catalogue:uom_product_autocomplete", kwargs={"pk": instance.pk})

    def clean_price(self):
        price = self.cleaned_data['price']
        if price.amount and price.amount <= 0:
            raise forms.ValidationError("Price value must be greater than 0.")
        return price

    def clean_uom(self):
        uom = self.cleaned_data.get("uom")
        manage_stock = self.cleaned_data.get("is_manage_stock")

        if manage_stock:
            if not uom:
                raise forms.ValidationError(_("This field is required."))
        return uom

    def clean_min_stock_alert(self):
        min_stock_alert = self.cleaned_data.get("min_stock_alert")
        if not min_stock_alert or min_stock_alert < 0:
            min_stock_alert = 0
        manage_stock = self.cleaned_data.get("is_manage_stock")

        if manage_stock:
            if not min_stock_alert:
                if not min_stock_alert == 0:
                    raise forms.ValidationError(_("This field is required."))
        return min_stock_alert

    def clean_cost(self):
        cost = self.cleaned_data['cost']
        if cost.amount and cost.amount <= 0:
            raise forms.ValidationError("Cost value must be greater than 0.")
        return cost

    def clean_image_location(self):
        image = self.cleaned_data['image_location']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def save(self, commit=False):
        product = super().save(commit=False)
        product.classification = self.Meta.model.PRODUCT_CLASSIFICATION.complementary
        product.save()
        product.catalogue = Catalogue.prepare_catalogue(product)

        product_usages = self.cleaned_data['product_usages']

        if product.addon_usage.exists():
            product.addon_usage.exclude(
                product_usage__pk__in=product_usages
            ).delete()

        for product_addon in product_usages:
            product.addon_usage.get_or_create(
                product=product, product_usage=product_addon)
        return product


class ProductAddOnForm(forms.ModelForm):

    class Meta:
        model = ProductAddOn
        fields = '__all__'
        exclude = ('account', )

    def __init__(self, *args, **kwargs):
        super(ProductAddOnForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Div(
                    Field('catalogue_usage', css_class='custom-select'),
                    css_class="col-lg-6"
                ),
                css_class="row"
            ),
        )

    def clean_product(self):
        if self.cleaned_data.get('product').classification != Product.PRODUCT_CLASSIFICATION.complementary:
            raise forms.ValidationError(
                _("Only Product with Complementary classification can become product addon"),
                code="invalid_product_addon_classification"
            )
        return self.cleaned_data.get('product')


class BaseModifierForm(forms.ModelForm):
    class Meta:
        model = ProductModifier
        fields = '__all__'
        exclude = ('account', 'product')


class ProductModifierForm(BaseModifierForm):

    modifieropt = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'ex: opsi 1, opsi 2, ...'
            }
        )
    )

    product_usages = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )

    class Meta:
        model = ProductModifier
        fields = '__all__'
        exclude = ('account', 'product')


    class Media:
        css = {
            'all': (
                'bootstrap-tagsinput/bootstrap-tagsinput.css',
            )
        }

        js = (
            'bootstrap-tagsinput/bootstrap-tagsinput.js',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['modifieropt'].label = _('Options')
        self.fields['modifieropt'].help_text = _('Modifier values split with comma (",")')

        self.fields['product_usages'].label = _('Product Usages')
        self.fields['product_usages'].queryset = Product.objects.filter(
            archived=False,
            deleted__isnull=True
        ).exclude(
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.complementary,
                Product.PRODUCT_CLASSIFICATION.composite,
                Product.PRODUCT_CLASSIFICATION.dynamic_composite,
            ],
        )
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product Modifier"),
                KawnField("name"),
                KawnField("modifieropt", css_class='tags-input'),
            ),
            KawnFieldSetWithHelpText(
                _("Product Usages"),
                KawnField("product_usages"),
            ),
        )

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            str_val = [
                    x for x in
                    instance.options.values_list('value', flat=True)
                ]
            self.initial['modifieropt'] = ",".join(str_val)
            self.fields['product_usages'].initial = Product.objects.filter(
                modifiers__pk=instance.pk)

    def update_m2m(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        if len(object_list) != 0:
            obj_ids = set(
                object_list.values_list("id", flat=True))
            current_ids = set(
                rel_field.values_list("id", flat=True)
            )
            add_ids = obj_ids - current_ids
            delete_ids = current_ids - obj_ids
            model_objects = [
                intermediate_model(
                    **{
                        "{}_id".format(rel_field.target_field_name): obj_id,
                        "{}".format(intermediate_model_ref.field.name): instance,
                        "account": instance.account
                    }
                )
                for obj_id in add_ids
            ]
            intermediate_model.objects.bulk_create(model_objects)
            intermediate_model.objects.complex_filter({
                "{}__pk__in".format(rel_field.target_field_name): delete_ids,
                "{}".format(intermediate_model_ref.field.name): instance,
            }).delete()
        else:
            rel_field.clear()

    def clean_modifieropt(self):
        value = self.cleaned_data.get('modifieropt')
        for val in value.split(','):
            ProductModifierOptionForm.base_fields['value'].clean(val)
        return value

    def save(self, commit=False):
        modifier_options = self.cleaned_data.pop('modifieropt')
        product_usages = self.cleaned_data.pop('product_usages')

        product_modifier = super(ProductModifierForm, self).save(commit=False)
        product_modifier.save()

        self.update_m2m(
            product_modifier,
            product_usages,
            product_modifier.products,
            ProductModifierUsage,
            ProductModifierUsage.product_modifier)

        product_modifier.options.exclude(
            value__in=modifier_options.split(',')).delete()

        for modifier_option in modifier_options.split(','):
            ProductModifierOption.objects.get_or_create(
                value=modifier_option,
                product_modifier=product_modifier,
                account=product_modifier.account
            )

        return product_modifier


class ProductModifierOptionForm(forms.ModelForm):

    class Meta:
        model = ProductModifierOption
        fields = '__all__'
        exclude = ('account',)

    def __init__(self, *args, **kwargs):
        super(ProductModifierOptionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False


class ProductCreateModifierForm(BaseModifierForm):
    values = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'ex: opsi 1, opsi 2, ...'
            }
        )
    )

    class Media:
        css = {
            'all': (
                'bootstrap-tagsinput/bootstrap-tagsinput.css',
            )
        }

        js = (
            'https://code.jquery.com/jquery-1.12.4.js',
            'bootstrap-tagsinput/bootstrap-tagsinput.js',
            'js/product_modifier_formset.js',
        )

    def __init__(self, *args, **kwargs):
        super(ProductCreateModifierForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields['values'].label = _('Options')
        self.fields['values'].help_text = _('Modifier values split with comma (",")')

        self.helper.layout = Layout(
            Field('id', type='hidden'),
            Div(
                Div(
                    'name',
                    css_class='col-lg-3 p-2'
                ),
                Div(
                    Field('values', css_class='tags-input'),
                    css_class='col-lg-9 p-2',
                ),
                css_class='row',
                css_id='modifiers_formset'
            ),
        )
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            values = instance.options.order_by('value')
            str_val = ''
            for idx, value in enumerate(values):
                if idx < len(values):
                    str_val += value.value + ','
                else:
                    str_val += value.value

            self.initial['values'] = str_val


class ProductPricingForm(forms.ModelForm):

    default_price = forms.IntegerField(
        widget=forms.TextInput, disabled=True)
    price = forms.DecimalField(
        widget=forms.TextInput,
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )

    class Meta:
        model = ProductPricing
        fields = ('id', 'outlet', 'default_price', 'price', 'is_active')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['default_price'].initial = self.instance.product.price.amount
        self.fields['outlet'].disabled = True
        self.fields['outlet'].hidden = True
        self.initial['price'] = int(self.instance.price.amount)

        if self.instance and self.instance.pk:
            self.fields['price'].required = False

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            'id',
            Field(
                'price',
                template='dashboard/product/form_fields/pricing.html', css_class='text-right'),
            Field(
                'is_active',
                template='dashboard/product/form_fields/pricing_switch.html', css_class='text-right'),
        )

    def clean(self):
        is_active = self.cleaned_data.get("is_active")
        price = self.cleaned_data.get("price")

        if is_active:
            self.fields["price"].required = True

        cleaned_data = super().clean()

        if not is_active:
            cleaned_data["price"] = self.initial['price']
            self.fields["price"].initial = self.initial['price']

        return cleaned_data


class ProductAvailabilityForm(forms.ModelForm):

    class Meta:
        model = ProductAvailability
        fields = ('id', 'outlet', 'is_active')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['outlet'].disabled = True

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            Div(
                Div(
                    'id',
                    Field(
                        'is_active',
                        template='dashboard/product/form_fields/switch.html'),
                    css_class="col-lg-12"
                ),
                css_class="row"
            ),
        )


ProductCreateModifierFormset = inlineformset_factory(
    Product, ProductModifier,
    form=ProductCreateModifierForm,
    extra=0, can_delete=True)

ProductModifierOptionFormSet = inlineformset_factory(
    ProductModifier, ProductModifierOption,
    form=ProductModifierOptionForm,
    extra=0, can_delete=True)

BaseCompositeMaterialProductFormFormSet = inlineformset_factory(
    Product, CompositeProduct,
    fk_name='composite_product',
    form=CompositeMaterialProductForm, extra=0, can_delete=True)


class CompositeMaterialProductFormFormSet(BaseCompositeMaterialProductFormFormSet):

    @property
    def initial_forms(self):
       return self.forms[:self.get_queryset().count()]

    @property
    def extra_forms(self):
       return self.forms[self.get_queryset().count():]

    def initial_form_count(self):
        return len(self.get_queryset())


ProductPricingFormSet = inlineformset_factory(
    Product, ProductPricing,
    form=ProductPricingForm,
    extra=0, can_delete=False)

ProductAvailabilityFormSet = inlineformset_factory(
    Product, ProductAvailability,
    form=ProductAvailabilityForm,
    extra=0, can_delete=False)


class ProductCompositeMaterialForm(forms.ModelForm):
    price = forms.DecimalField(
        widget=forms.TextInput(attrs={'disabled': True, "class": "product-price"}),
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        required=False)

    class Meta:
        model = CompositeProduct
        fields = ("product", "product_quantity")
        widgets = {
            'product': autocomplete.ModelSelect2(
                url='catalogue:product_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            "product_quantity": forms.TextInput(),
        }
    field_order = ["product", "price", "product_quantity"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["price"].label = _("Price")
        self.fields["product_quantity"].label = _("Product Quantity")

        self.fields["product"].widget.attrs["class"] = "product-select"
        self.empty_permitted = False
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.initial['price'] = kwargs['instance'].product.price


class ProductCompositeMaterialFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ProductCompositeMaterialFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "product-composite-form"
        self.template = "bootstrap4/formset_with_help.html"
        self.title = _("Product Composite Material")
        self.add_content = _("Add Product")


ProductCompositeMaterialFormset = inlineformset_factory(
    Product, CompositeProduct, fk_name='composite_product', form=ProductCompositeMaterialForm, min_num=1, extra=0, can_delete=True
)


class UOMForm(forms.ModelForm):

    class Meta:
        model = UOM
        fields = ("unit", "category", "uom_type", "ratio", "is_active")

    def __init__(self, *args, **kwargs):
        super(UOMForm, self).__init__(*args, **kwargs)
        self.fields['ratio'].help_text = _("e.g: 1*(this unit)=ratio*(reference unit)")
        self.fields["is_active"].label = _("Active")
        self.fields["uom_type"].label = _("Measurement Type")
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Units of Measurement"),
                KawnField("unit"),
                KawnField("category", css_class="custom-select"),
                KawnField("uom_type", css_class="custom-select"),
                KawnField("ratio"),
                KawnField(
                    "is_active",
                    template='dashboard/form_fields/switch.html',
                ),
                form_grid="col-lg-6",
            )
        )
        self.fields["ratio"].initial = 1

    def clean_uom_type(self):
        uom_type = self.cleaned_data.get("uom_type")
        category = self.cleaned_data.get("category")

        unit_exist = UOM.objects.filter(
            uom_type=uom_type, category=category,
        ).exclude(pk=self.instance.pk).exists()
        if uom_type == UOM.UOM_TYPE.base:
            if unit_exist:
                raise forms.ValidationError(
                    _('Unit of Measure with category %s should only have one reference' % category)
                )
        return uom_type

    def clean_is_active(self):
        is_active = self.cleaned_data.get("is_active")
        if self.instance.product_set.all() and not is_active:
            raise forms.ValidationError(
                _("Satuan tidak dapat di non-aktifkan karena ada produk yang menggunakan.")
            )
        return is_active
