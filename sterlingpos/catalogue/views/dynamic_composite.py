import json
import csv
from datetime import datetime, timedelta

from django.conf import settings

from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.db import IntegrityError
from django.db.models import FilteredRelation, Q, Count, Value, Max, Min, Sum as BaseSum
from django.contrib.postgres.aggregates.general import StringAgg
from django.db.models.functions import Coalesce, Substr, Length
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.safestring import mark_safe
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.utils.encoding import smart_str
from django.utils.html import escape
from django.views.generic import (
    ListView, DetailView, View, TemplateView,
    CreateView, UpdateView, DeleteView, View,
)
from django.http import HttpResponse
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db import transaction

from braces.views import JSONResponseMixin, AjaxResponseMixin, MessageMixin
from dal import autocomplete
from formtools.wizard.views import SessionWizardView

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.core.mixins import FormsetMixin

from sterlingpos.catalogue.models import (
    UOM, Product, Catalogue,
    CompositeProduct,
    ProductModifier,
    ProductModifierUsage,
    ProductType,
    Category,
    ProductAddOn,
    ProductModifierOption,
    ProductAvailability,
    ProductPricing,
)
from sterlingpos.catalogue.forms import (
    DynamicCompositeProductForm,
    DynamicCompositeMaterialForm,
    DynamicCompositeMaterialFormSet,
    DynamicCompositeMaterialFormSetHelper,
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs


class DynamicCompositeProductListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'sku', 'price', 'cost', 'archived', 'action']
    order_columns = ['name', 'sku', 'price', 'cost', 'archived', 'action']

    model = Product

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.filter(
            classification=self.model.PRODUCT_CLASSIFICATION.dynamic_composite)
        return qs

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)

    def render_action(self, obj):
        action_data = {
            'id': obj.id,
            'name': escape(obj.name),
            'edit': reverse_lazy(
                "catalogue:dynamic_composite_update",
                kwargs={'pk': obj.pk}),
            'archived': obj.archived
        }
        return action_data

    def render_archived(self, obj):
        if obj.archived:
            return '{}'.format(_('Archived'))
        else:
            return '{}'.format(_('Active'))

    def render_price(self, obj):
        return obj.price.amount

    def render_cost(self, obj):
        return obj.cost.amount

    def render_name(self, obj):
        return '<a href="{}">{}</a>'.format(
            reverse_lazy(
                "catalogue:dynamic_composite_update",
                kwargs={'pk': obj.pk}), escape(obj.name))


class DynamicCompositeProductListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/product_dynamic_composite_list.html'


class UpdateDynamicCompositeProductForm(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            forms = {}
            deleted_data = []
            prefix = request.POST.get('prefix', '')
            total_form = request.POST.get('total_form', '')
            initial_data = json.loads(request.POST.get('initial_data', ''))
            total_product = request.POST.get('total_product', '')
            product_selected = request.POST.get('product_selected', '')
            context = {}
            context['total_product'] = int(total_product)
            data = {
                prefix + '-TOTAL_FORMS': total_form,
                prefix + '-INITIAL_FORMS': '0',
                prefix + '-MAX_NUM_FORMS': '',
            }

            product = None
            if product_selected:
                product = Product.objects.get(
                    pk=initial_data[product_selected],
                    classification=Product.PRODUCT_CLASSIFICATION.standart)
                split_val = product_selected.split('-')
                data_prefix = split_val[0] + "-" + split_val[1] + "-price"

            for i in initial_data:
                if product_selected:
                    if data_prefix in i:
                        if product:
                            data.update({i: product.price})
                        else:
                            data.update({i: initial_data[i]})
                    else:
                        data.update({i: initial_data[i]})
                else:
                    data.update({i: initial_data[i]})

            for i in data:
                if "DELETE" in i:
                    if data[i]:
                        deleted_data.append(i)

            formset = DynamicCompositeMaterialForm(
                data, prefix=prefix)
            template = 'dashboard/product/product_composite_formset.html'
            form = render_to_string(template, {'formset': formset, 'context': context})
            forms['form'] = form
            forms['deleted_data'] = deleted_data
            response['data'] = forms
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductDynamicCompositeMaterailPriceView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            product_pk = request.GET.get("product_pk", "")
            product = Product.objects.get(pk=product_pk)
            response["status"] = "success"
            response["result"]["price"] = "%s" % product.price
            status_code = 200
        except Product.DoesNotExist:
            pass
        except ValueError:
            status_code = 200
            response["result"]["price"] = ""

        return self.render_json_response(response, status=status_code)


class DynamicCompositeProductCreateView(
        SterlingRoleMixin, MessageMixin,
        FormsetMixin, CreateView):

    template_name = 'dashboard/product/product_dynamic_composite_create.html'
    model = Product
    form_class = DynamicCompositeProductForm
    success_url = reverse_lazy("catalogue:dynamic_composite_list")
    formsets = {
        "formset": DynamicCompositeMaterialFormSet,
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.method == "POST":
            context["formset"] = self.populate_formset(
                self.formsets.get("formset"))
        else:
            context["formset"] = self.formsets.get("formset")
        context["formset_helper"] = DynamicCompositeMaterialFormSetHelper
        return context


class DynamicCompositeProductUpdateView(
        SterlingRoleMixin, MessageMixin,
        FormsetMixin, UpdateView):

    template_name = 'dashboard/product/product_dynamic_composite_update.html'
    model = Product
    form_class = DynamicCompositeProductForm
    success_url = reverse_lazy("catalogue:dynamic_composite_list")
    formsets = {
        "formset": DynamicCompositeMaterialFormSet,
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.method == "POST":
            context["formset"] = self.populate_formset(
                self.formsets.get("formset"), self.object
            )
        else:
            context["formset"] = self.formsets.get("formset")(
                instance=self.object,
            )
        context["formset_helper"] = DynamicCompositeMaterialFormSetHelper
        
        return context


class DynamicCompositeProductArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Product.objects.get(
                pk=product_id,
                classification=Product.PRODUCT_CLASSIFICATION.dynamic_composite)
            product.archived = True
            product.save()
            messages.success(self.request, '{}'.format(
                _('Product Composite has been archived.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class DynamicCompositeProductActivateView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Product.objects.get(
                pk=product_id,
                classification=Product.PRODUCT_CLASSIFICATION.dynamic_composite)
            product.archived = False
            product.save()
            messages.success(self.request, '{}'.format(
                _('Product Composite has been re-activated.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class DynamicCompositeProductDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Product.objects.get(
                pk=product_id,
                classification=Product.PRODUCT_CLASSIFICATION.dynamic_composite)
            product.delete()
            messages.error(self.request, '{}'.format(
                _('Product Composite has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
