import json
import csv
from datetime import datetime, timedelta
import logging

from django.conf import settings

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.db import IntegrityError
from django.db.models import FilteredRelation, Q, Count, Value, Max, Min, Sum as BaseSum
from django.contrib.postgres.aggregates.general import StringAgg
from django.db.models.functions import Coalesce, Substr, Length
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.safestring import mark_safe
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.utils.encoding import smart_str
from django.utils.html import escape
from django.views.generic import (
    ListView, DetailView, View, TemplateView,
    CreateView, UpdateView, DeleteView, FormView,
)
from django.http import HttpResponse
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db import transaction
from django.db.models import Subquery, OuterRef

from braces.views import JSONResponseMixin, AjaxResponseMixin, MessageMixin
from dal import autocomplete
from formtools.wizard.views import SessionWizardView
from decimal import Decimal

from sterlingpos.core.import_export import SterlingImportMixin
from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.catalogue.utils import uom_conversion, uom_cost_conversion

from sterlingpos.catalogue.models import (
    UOM, Product, Catalogue,
    CompositeProduct,
    ProductModifier,
    ProductModifierUsage,
    ProductType,
    Category,
    ProductAddOn,
    ProductModifierOption,
    ProductAvailability,
    ProductPricing,
)

from sterlingpos.catalogue.forms import (
    CatalogueForm,
    CatalogueCreateForm,
    CategoryCreateForm,
    CategoryForm,
    # BaseProductForm,
    ProductForm,
    VariantForm,
    VariantProductFormset,
    VariantProductUpdateForm,
    ProductComplementaryForm,
    ProductModifierForm,
    CompositeProductForm,
    ProductModifierOptionFormSet,
    ProductCreateModifierFormset,
    CompositeMaterialProductFormFormSet,
    ProductPricingFormSet,
    ProductAvailabilityFormSet,
    VariantFormset,
    VariantFormsetHelper,
    ProductCompositeMaterialFormset,
    ProductCompositeMaterialFormsetHelper,
    UOMForm,
    CatalogueManufacturedCreateForm,
    VariantManufacturedForm
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs
from sterlingpos.catalogue.resources import ProductResource, ManufacturedProductResource
from sterlingpos.catalogue.tasks import (
    update_catalogue_variant_task,
    update_variant_task,
)

logger = logging.getLogger(__name__)


class Sum(BaseSum):
    # https://github.com/django/django/commit/5f24e7158e1d5a7e40fa0ae270639f6a171bb18e
    allow_distinct=True


# Manage Uom
class UOMDetailView(SterlingRoleMixin, DetailView):
    template_name = 'dashboard/uom/uom_detail.html'
    model = UOM


class UOMListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/uom/uom_list.html'


class UOMCreateView(SterlingRoleMixin, CreateView):
    template_name = 'dashboard/uom/uom_form.html'
    model = UOM
    success_url = reverse_lazy("catalogue:uom_list")
    form_class = UOMForm


class UOMUpdateView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/uom/uom_form.html'
    model = UOM
    success_url = reverse_lazy("catalogue:uom_list")
    form_class = UOMForm


class UOMListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['unit', 'category', 'uom_type', "is_active"]
    order_columns = columns
    model = UOM

    csv_filename = 'uom'
    csv_header = ['unit', 'category', 'uom_type', 'is_active']

    def filter_is_active(self, value):
        if value != "show":
            return Q(is_active__istartswith=value)
        return Q()

    def filter_category(self, value):
        if value != "show":
            return Q(category=value)
        return Q()

    def filter_uom_type(self, value):
        if value != "show":
            return Q(uom_type=value)
        return Q()

    def render_column(self, row, column):
        if column == "unit":
            url = reverse_lazy("catalogue:uom_detail", kwargs={"pk": row.id})
            return "<a href='%s'>%s</a>" % (url, row.unit)
        elif column == "is_active":
            if row.is_active:
                return _("Active")
            return _("Inactive")
        else:
            return super().render_column(row, column)


class UOMAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = UOM.objects.filter(is_active=True)
        if self.q:
            qs = qs.filter(unit__istartswith=self.q)
        return qs


class UOMProductAutocomplete(UOMAutocompleteView):
    def get_queryset(self):
        product_pk = self.kwargs.get("pk")
        category = Product.objects.get(pk=product_pk).uom.category
        qs = UOM.objects.filter(is_active=True, category=category)
        if self.q:
            qs = qs.filter(unit__istartswith=self.q)
        return qs


class UOMCatalogueProductAutocomplete(UOMAutocompleteView):
    def get_queryset(self):
        catalogue_pk = self.kwargs.get("pk")
        category = Catalogue.objects.get(pk=catalogue_pk).uom.category
        qs = UOM.objects.filter(is_active=True, category=category)
        if self.q:
            qs = qs.filter(unit__istartswith=self.q)
        return qs


class UOMProductAjaxView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            product_pk = request.GET.get("product", "")
            product = Product.objects.get(pk=product_pk)
            if product.uom:
                unit = UOM.objects.filter(category=product.uom.category, is_active=True).values("id", "unit")
                response["result"]["product_unit"] = product.uom.pk
            else:
                unit = UOM.objects.none()
            response["status"] = "success"
            response["result"]["units"] = list(unit)
            status_code = 200
        except Product.DoesNotExist:
            pass

        return self.render_json_response(response, status=status_code)


class ProductListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'category.name', 'price', 'cost', 'variant_count', 'is_active',  'action']
    order_columns = ['name', 'category__name', 'min_price', 'min_cost', 'variant_count', 'is_active',
                     'action']

    model = Catalogue

    csv_filename = 'product_list'
    csv_header = ['id', 'name', 'price', 'cost', 'variant_count', 'is_active']

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().select_related('category')

        salability = self.kwargs.get("salability")
        if salability == "sellable":
            qs = qs.filter(is_sellable=True)
        elif salability == "unsellable":
            qs = qs.filter(is_sellable=False)

        qs = qs.annotate(
            max_price=Max('variants__price'),
            min_price=Min('variants__price'),

            max_cost=Max('variants__cost'),
            min_cost=Min('variants__cost'),
        )
        qs = qs.exclude(classification__in=[
            self.model.PRODUCT_CLASSIFICATION.complementary,
            self.model.PRODUCT_CLASSIFICATION.composite,
        ])
        qs = qs.annotate(
            filtered_variant=FilteredRelation('variants', condition=Q(variants__deleted__isnull=True,
                                                                      variants__parent__isnull=False)),
            variant_count=Coalesce(
                Count('filtered_variant', allow_distinct=True, distinct=True),
                Value(0)
            ),
        )
        return qs

    def filter_is_active(self, value):
        if value != "show":
            return Q(is_active__istartswith=value)
        return Q()

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)
        return Q()

    def filter_cost(self, value):
        return Q(variants__cost__istartswith=value)

    def filter_price(self, value):
        return Q(variants__price__istartswith=value)

    def filter_name(self, value):
        return Q(name__icontains=value)

    def filter_category__name(self, value):
        return Q(category__name__icontains=value)

    def render_action(self, obj):
        action_data = {
            'id': obj.id,
            'name': escape(obj.name),
            'edit': reverse_lazy(
                "catalogue:product_variant_list",
                kwargs={'pk': obj.pk}),
            'archived': not obj.is_active
        }
        return action_data

    def render_variant_count(self, obj):
        if obj.variants.count() > 1:
            return '<a href="%s">%s Variants</a>' % (
                reverse_lazy(
                    "catalogue:product_variant_list",
                    kwargs={'pk': obj.pk}),
                obj.variants.count()
            )
        elif obj.variants.count() == 1:
            return '<a href="%s">%s Variant</a>' % (
                reverse_lazy(
                    "catalogue:product_variant_list",
                    kwargs={'pk': obj.pk}),
                obj.variants.count()
            )
        else:
            return '-'

    def render_is_active(self, obj):
        if obj.is_active:
            return '{}'.format(_('Active'))
        else:
            return '{}'.format(_('Archived'))

    def render_name(self, obj):
        return '<a href="{}">{}</a>'.format(
            reverse_lazy(
                "catalogue:product_variant_list",
                kwargs={'pk': obj.pk}), escape(obj.name))

    def render_column(self, row, column):
        if column in ['price', 'cost']:
            min_value = getattr(row, "min_{}".format(column))
            max_value = getattr(row, "max_{}".format(column))
            if min_value == max_value:
                return "{}".format(max_value)
            return "{} - {}".format(
                min_value,
                max_value
            )
        else:
            return super().render_column(row, column)


class ProductListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/product_list.html'


class UpdateProductFormset(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            forms = {}
            deleted_data = []
            prefix = request.POST.get('prefix', '')
            total_form = request.POST.get('total_form', '')
            initial_data = json.loads(request.POST.get('initial_data', ''))
            total_data = request.POST.get('total_data', '')
            context = {}

            data = {
                prefix + '-TOTAL_FORMS': total_form,
                prefix + '-INITIAL_FORMS': '0',
                prefix + '-MAX_NUM_FORMS': '',
            }
            for i in initial_data:
                data.update({i: initial_data[i]})

            for i in data:
                if "DELETE" in i:
                    if data[i]:
                        deleted_data.append(i)

            if prefix == 'variants':
                formset = VariantProductFormset(data, prefix=prefix)
                context['total_variant'] = int(total_data)
                template = 'dashboard/product/product_variant_formset.html'
            else:
                formset = ProductCreateModifierFormset(data, prefix=prefix)
                context['total_modifier'] = int(total_data)
                template = 'dashboard/product/product_modifier_formset.html'

            form = render_to_string(template, {'formset': formset, 'context': context})
            forms['prefix'] = prefix
            forms['form'] = form
            forms['deleted_data'] = deleted_data
            response['data'] = forms
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class VariantFormMixin(object):
    formsets = None
    modifier_list = None

    def process_variant_form(self, variant_formset):
        variant_formset.save(commit=True)

    def formsets_all_valid(self):
        valid = True
        if self.formsets is not None:
            for key, formset in self.formsets.items():
                valid = valid and formset.is_valid()
        return valid

    def form_valid(self, form):
        if not self.formsets_all_valid():
            return self.form_invalid(form)
        created = self.object is None
        self.object = form.save()

        self.formsets['variant_formset'].instance = self.object
        self.process_variant_form(self.formsets['variant_formset'])

        for variant in self.object.variants.all():
            self.update_m2m(
                variant,
                self.object.modifiers.all(),
                variant.modifiers,
                ProductModifierUsage,
                ProductModifierUsage.product)

        if not created:
            messages.success(self.request, '{}'.format(
                _('Product has been updated.')))
        else:
            messages.success(self.request, '{}'.format(
                _('Product has been created.')))

        return HttpResponseRedirect(self.get_success_url())

    def update_m2m(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        if len(object_list) != 0:
            obj_ids = set(
                object_list.values_list("id", flat=True))
            current_ids = set(
                rel_field.values_list("id", flat=True)
            )
            add_ids = obj_ids - current_ids
            delete_ids = current_ids - obj_ids
            model_objects = [
                intermediate_model(
                    **{
                        "{}_id".format(rel_field.target_field_name): obj_id,
                        "{}".format(intermediate_model_ref.field.name): instance,
                        "account": instance.account
                    }
                )
                for obj_id in add_ids
            ]
            intermediate_model.objects.bulk_create(model_objects)
            intermediate_model.objects.complex_filter({
                "{}__pk__in".format(rel_field.target_field_name): delete_ids,
                "{}".format(intermediate_model_ref.field.name): instance,
            }).delete()
        else:
            rel_field.clear()


class ProductAutocompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Product.objects.all()
        qs = qs.filter(archived=False)
        qs = qs.exclude(classification__in=[
            Product.PRODUCT_CLASSIFICATION.complementary,
            Product.PRODUCT_CLASSIFICATION.composite,
            Product.PRODUCT_CLASSIFICATION.dynamic_composite,
        ])

        if self.q:
            qs = qs.filter(Q(name__istartswith=self.q, archived=False) | Q(sku__istartswith=self.q,
                                                                           archived=False) | Q(
                catalogue__name__istartswith=self.q, archived=False))
        return qs

    def get_result_label(self, result):
        return result.__str__()


class ComponentsManufacturedAutocompleteView(ProductAutocompleteView):

    def get_queryset(self):

        manufactured = (Q(bom__isnull=False) & Q(bom__deleted__isnull=True))
        stardart = (Q(classification=Product.PRODUCT_CLASSIFICATION.standart))
        qs = Product.objects.all()
        qs = qs.filter(archived=False, is_manage_stock=True, deleted__isnull=True)
        qs = qs.filter(manufactured | stardart)
        if self.q:
            qs = qs.filter(Q(name__istartswith=self.q, archived=False) | Q(sku__istartswith=self.q,
                                                                           archived=False) | Q(
                catalogue__name__istartswith=self.q, archived=False))
        return qs


class ProductPromoAutoCompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Product.objects.all()
        qs = qs.filter(archived=False)
        qs = qs.exclude(classification__in=[
            Product.PRODUCT_CLASSIFICATION.complementary
        ])

        if self.q:
            qs = qs.filter(Q(name__istartswith=self.q, archived=False) | Q(sku__istartswith=self.q, archived=False))
        return qs


class ProductManufacturedAutoCompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Product.objects.filter(
            archived=False,
            is_manage_stock=True,
            uom__isnull=False,
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.manufactured,
                Product.PRODUCT_CLASSIFICATION.complementary,
            ],
        )
        excluded = []
        for q in qs:
            if q.bom.all():
                excluded.append(q.pk)

        qs = qs.exclude(pk__in=excluded)

        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(sku__icontains=self.q)
                | Q(catalogue__name__icontains=self.q)
            )
        return qs


class ProductManufactureCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = "dashboard/product/manufacture_create.html"
    success_url = reverse_lazy("catalogue:product_list")
    model = Catalogue
    form_class = CatalogueManufacturedCreateForm

    def get_context_data(self, **kwargs):
        context = super(ProductManufactureCreateView, self).get_context_data(**kwargs)
        context["formset"] = VariantFormset
        context["formset_helper"] = VariantFormsetHelper
        return context

    def create_variants(self, instance, parent, keep_sell_past_zero, min_stock_alert, uom):
        instance.category = parent.category
        instance.is_manage_stock = parent.is_manage_stock
        instance.is_sellable = parent.is_sellable
        instance.is_batch_tracked = parent.is_batch_tracked
        instance.catalogue = parent
        instance.keep_selling_sold_out = keep_sell_past_zero
        instance.min_stock_alert = min_stock_alert
        instance.uom = uom
        instance.classification = Product.PRODUCT_CLASSIFICATION.manufactured
        instance.save()
        return instance

    def create_modifiers(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        if len(object_list) != 0:
            obj_ids = set(object_list.values_list("id", flat=True))
            current_ids = set(rel_field.values_list("id", flat=True))
            add_ids = obj_ids - current_ids
            delete_ids = current_ids - obj_ids
            model_objects = [
                intermediate_model(
                    **{
                        "{}_id".format(rel_field.target_field_name): obj_id,
                        "{}".format(intermediate_model_ref.field.name): instance,
                        "account": instance.account
                    }
                )
                for obj_id in add_ids
            ]
            intermediate_model.objects.bulk_create(model_objects)
            intermediate_model.objects.complex_filter({
                "{}__pk__in".format(rel_field.target_field_name): delete_ids,
                "{}".format(intermediate_model_ref.field.name): instance,
            }).delete()
        else:
            rel_field.clear()

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = VariantFormset(form_params, instance=form.instance)

        if not formset.is_valid() and "has_variants" in form_params:
            self.messages.error(
                _(u"""
                    There was a problem creating this %s.
                    Please review the fields marked red and resubmit. 
                """)
                % self.model._meta.verbose_name
            )
            return self.form_invalid(form)

        with transaction.atomic():
            catalogue = form.save(commit=False)
            catalogue.classification = Product.PRODUCT_CLASSIFICATION.manufactured
            catalogue.is_manage_stock = True
            catalogue.save()

            if "has_variants" in form_params:
                keep_selling_sold_out = form.cleaned_data.get("keep_selling_sold_out")
                min_stock_alert = form.cleaned_data.get("min_stock_alert")
                uom = form.cleaned_data.get("uom")
                modifiers = form.cleaned_data.get("modifiers")
                for form_variant in formset:
                    deleted = form_variant.cleaned_data.get("DELETE")
                    if form_variant.is_valid() and not deleted:
                        variant = form_variant.save(commit=False)
                        self.create_variants(
                            variant, catalogue, keep_selling_sold_out, min_stock_alert, uom
                        )
                        self.create_modifiers(
                            variant, modifiers, variant.modifiers, ProductModifierUsage, ProductModifierUsage.product
                        )
            else:
                modifiers = form.cleaned_data.get("modifiers")
                data = {
                    "sku": form.cleaned_data.get("sku"),
                    "price": form.cleaned_data.get("price"),
                    "cost": form.cleaned_data.get("cost"),
                    "keep_selling_sold_out": form.cleaned_data.get("keep_selling_sold_out"),
                    "min_stock_alert": form.cleaned_data.get("min_stock_alert"),
                    "is_sellable": catalogue.is_sellable,
                    "is_manage_stock": catalogue.is_manage_stock,
                    "is_batch_tracked": catalogue.is_batch_tracked,
                    "catalogue": catalogue,
                    "category": catalogue.category,
                    "classification": Product.PRODUCT_CLASSIFICATION.manufactured,
                    "name": catalogue.name,
                    "uom": form.cleaned_data.get("uom")
                }
                variant = catalogue.variants.create(**data)
                self.create_modifiers(
                    variant, modifiers, variant.modifiers, ProductModifierUsage, ProductModifierUsage.product
                )
        self.messages.success(
            _("Manufactured Product %s was successfully created." % catalogue)
        )
        return super(ProductManufactureCreateView, self).form_valid(form)


class ProductManufacturedUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/product/manufacture_update.html"
    model = Catalogue
    success_url = reverse_lazy("catalogue:product_list")
    form_class = CatalogueForm

    def dispatch(self, request, *args, **kwargs):
        self.current_uom = self.get_object().uom
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_manage_stock = True
        self.object.save()
        for variant in self.object.variants.all():
            variant.category = self.object.category
            variant.is_manage_stock = self.object.is_manage_stock
            variant.is_batch_tracked = self.object.is_batch_tracked
            variant.uom = self.object.uom
            variant.save()

        uom = self.object.uom.pk if self.object.uom else None
        current_uom = self.current_uom.pk if self.current_uom else None
        update_catalogue_variant_task.apply_async(
            count_down=5,
            kwargs={
                "catalogue_id": self.object.pk,
                "uom_id": uom,
                "current_uom_id": current_uom,
                "is_manage_stock": True,
                "is_batch_tracked": self.object.is_batch_tracked,
                "category_id": self.object.category.pk,
                "account_id": self.request.user.account.pk
            }
        )

        self.messages.success(
            _("Manufactured Product %s was successfully update" % self.object)
        )
        return super(ProductManufacturedUpdateView, self).form_valid(form)


class ProductCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/product/product_create.html'
    model = Catalogue
    success_url = reverse_lazy("catalogue:product_list")
    form_class = CatalogueCreateForm

    def get_context_data(self, **kwargs):
        context = super(ProductCreateView, self).get_context_data(**kwargs)
        context["formset"] = VariantFormset
        context["formset_helper"] = VariantFormsetHelper
        if "is_manage_stock" in self.request.POST:
            context["is_manage_stock"] = True
        return context

    def create_variants(self, instance, parent, keep_sell_past_zero, min_stock_alert, uom):
        instance.category = parent.category
        instance.is_manage_stock = parent.is_manage_stock
        instance.is_sellable = parent.is_sellable
        instance.is_batch_tracked = parent.is_batch_tracked
        instance.catalogue = parent
        instance.keep_selling_sold_out = keep_sell_past_zero
        instance.min_stock_alert = min_stock_alert
        instance.uom = uom
        instance.save()
        return instance

    def create_modifiers(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        if len(object_list) != 0:
            obj_ids = set(object_list.values_list("id", flat=True))
            current_ids = set(rel_field.values_list("id", flat=True))
            add_ids = obj_ids - current_ids
            delete_ids = current_ids - obj_ids
            model_objects = [
                intermediate_model(
                    **{
                        "{}_id".format(rel_field.target_field_name): obj_id,
                        "{}".format(intermediate_model_ref.field.name): instance,
                        "account": instance.account
                    }
                )
                for obj_id in add_ids
            ]
            intermediate_model.objects.bulk_create(model_objects)
            intermediate_model.objects.complex_filter({
                "{}__pk__in".format(rel_field.target_field_name): delete_ids,
                "{}".format(intermediate_model_ref.field.name): instance,
            }).delete()
        else:
            rel_field.clear()

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = VariantFormset(form_params, instance=form.instance)

        if not formset.is_valid() and "has_variants" in form_params:
            self.messages.error(
                _(
                    u"""There was a problem creating this %s.
                    Please review the fields marked red and resubmit."""
                )
                % self.model._meta.verbose_name
            )
            return self.form_invalid(form)

        with transaction.atomic():
            catalogue = form.save(commit=True)

            if "has_variants" in form_params:
                keep_selling_sold_out = form.cleaned_data.get("keep_selling_sold_out")
                min_stock_alert = form.cleaned_data.get("min_stock_alert")
                uom = form.cleaned_data.get("uom")
                modifiers = form.cleaned_data.get("modifiers")
                for form_variant in formset:
                    deleted = form_variant.cleaned_data.get("DELETE")
                    if form_variant.is_valid() and not deleted:
                        variant = form_variant.save(commit=False)
                        self.create_variants(
                            variant, catalogue, keep_selling_sold_out, min_stock_alert, uom
                        )
                        modifier = self.create_modifiers(variant, modifiers, variant.modifiers,
                                                         ProductModifierUsage, ProductModifierUsage.product)
            else:
                modifiers = form.cleaned_data.get("modifiers")
                data = {
                    "sku": form.cleaned_data.get("sku"),
                    "price": form.cleaned_data.get("price"),
                    "cost": form.cleaned_data.get("cost"),
                    "keep_selling_sold_out": form.cleaned_data.get("keep_selling_sold_out"),
                    "min_stock_alert": form.cleaned_data.get("min_stock_alert"),
                    "is_sellable": catalogue.is_sellable,
                    "is_manage_stock": catalogue.is_manage_stock,
                    "is_batch_tracked": catalogue.is_batch_tracked,
                    "catalogue": catalogue,
                    "category": catalogue.category,
                    "classification": catalogue.classification,
                    "name": catalogue.name,
                    "uom": form.cleaned_data.get("uom")
                }
                variant = catalogue.variants.create(**data)
                modifier = self.create_modifiers(variant, modifiers, variant.modifiers,
                                                 ProductModifierUsage, ProductModifierUsage.product)

        self.messages.success(
            _("Product %s was successfully created." % catalogue)
        )
        return super(ProductCreateView, self).form_valid(form)


class ProductUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/product/product_update.html'
    model = Catalogue
    success_url = reverse_lazy("catalogue:product_list")
    form_class = CatalogueForm

    def dispatch(self, request, *args, **kwargs):
        self.current_uom = self.get_object().uom
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stock_managed"] = self.object.is_manage_stock
        if "is_manage_stock" in self.request.POST:
            context["is_manage_stock"] = True
        return context

    def form_valid(self, form):
        self.object = form.save()
        for variant in self.object.variants.all():
            variant.category = self.object.category
            variant.is_manage_stock = self.object.is_manage_stock
            variant.is_batch_tracked = self.object.is_batch_tracked
            variant.uom = self.object.uom
            variant.save()
        uom = self.object.uom.pk if self.object.uom else None
        current_uom = self.current_uom.pk if self.current_uom else None
        update_catalogue_variant_task.apply_async(
            count_down=5,
            kwargs={
                "catalogue_id": self.object.pk,
                "uom_id": uom,
                "current_uom_id": current_uom,
                "is_manage_stock": self.object.is_manage_stock,
                "is_batch_tracked": self.object.is_batch_tracked,
                "category_id": self.object.category.pk,
                "account_id": self.request.user.account.pk
            }
        )
        self.messages.success(
            _("Product %s was successfully updated." % self.object)
        )
        return super(ProductUpdateView, self).form_valid(form)


class ProductAvailabilityView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/product/product_availability.html'
    model = Product
    form_class = ProductAvailabilityFormSet

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset
        return queryset

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user.is_owner:
            qs = self.object.availability.all()
        else:
            qs = self.object.availability.filter(
                outlet__in=get_outlet_of_user(self.request.user)
            )
        kwargs["queryset"] = qs.select_related(
            "outlet", "outlet__account")

        return kwargs

    def get_success_url(self):
        url = reverse(
            "catalogue:product_availability",
            kwargs={'pk': self.kwargs.get("pk")})
        return url

    def form_valid(self, form):
        response = super().form_valid(form)
        self.get_object().save()
        messages.success(self.request, '{}'.format(
            _('Product Availability has been updated.')))
        return response


class ProductPricingView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/product/product_pricing.html'
    model = Product
    form_class = ProductPricingFormSet

    def get_queryset(self):
        qs = super().get_queryset().prefetch_related(
            "product_pricing",
            "product_pricing__outlet",
        )
        return qs

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user.is_owner:
            qs = self.object.product_pricing.all()
        else:
            qs = self.object.product_pricing.filter(
                outlet__in=get_outlet_of_user(self.request.user)
            )
        kwargs["queryset"] = qs.select_related(
            "outlet", "outlet__account")
        return kwargs

    def get_success_url(self):
        url = reverse(
            "catalogue:product_pricing", kwargs={'pk': self.kwargs.get("pk")})
        return url

    def form_valid(self, form):
        response = super().form_valid(form)
        self.get_object().save()
        messages.success(self.request, '{}'.format(
            _('Product Pricing has been updated.')))
        return response


class ProductArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            catalogue = Catalogue.objects.get(pk=product_id)

            for variant in catalogue.variants.all():
                variant.archived = True
                variant.save()

            addons = ProductAddOn.objects.filter(
                product_usage__in=catalogue.variants.all())
            for addon in addons:
                addon.archived = True
                addon.save()

            modifiers = ProductModifier.objects.filter(
                product__in=catalogue.variants.all()
            )
            for modifier in modifiers:
                modifier.archived = True
                modifier.save()

            catalogue.archived = True
            catalogue.is_active = False
            catalogue.save()
            messages.success(self.request, '{}'.format(
                _('Product has been archived.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductActiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Catalogue.objects.get(pk=product_id)
            variants = Product.objects.filter(catalogue=product)
            addons = ProductAddOn.objects.filter(
                product_usage__in=variants)
            for addon in addons:
                addon.archived = False
                addon.save()

            for variant in variants:
                variant_addons = ProductAddOn.objects.filter(
                    product_usage=variant)
                for addon in variant_addons:
                    addon.archived = False
                    addon.save()
                variant.archived = False
                variant.save()

            modifiers = ProductModifier.objects.filter(
                product__in=variants)
            for modifier in modifiers:
                modifier.archived = False
                modifier.save()

            product.is_active = True
            product.save()
            messages.success(self.request, '{}'.format(
                _('Product has been re-activated.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def check_production_order(self, variant):
        return variant.productions_item.filter(productions__status__in=["finalized", "in_production"]).count()

    def check_bom_exist(self, variant):
        return variant.bom.all().count()

    def check_bom_item_exist(self, variant):
        return variant.bom_item.all().count()

    def can_delete_product(self, product):
        delete = True
        total_bom = 0
        total_bom_item = 0
        total_productions = 0
        for variant in product.variants.all():
            total_bom += self.check_bom_exist(variant)
            total_bom_item += self.check_bom_item_exist(variant)
            total_productions += self.check_production_order(variant)

        if total_bom:
            delete = False
        if total_bom_item:
            delete = False
        if total_productions:
            delete = False
        return delete

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Catalogue.objects.get(pk=product_id)
            if self.can_delete_product(product):
                product.delete()
                messages.error(self.request, '{}'.format(
                    _('Product has been deleted.')))
            else:
                messages.warning(
                    self.request, 
                    '{}'.format(
                        _(
                            "Tidak bisa menghapus produk {}. Salah satu varian dari produk ini sedang digunakan dalam proses produksi".format(
                                product
                            )
                        )
                    )
                )
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductDetailView(SterlingRoleMixin, DetailView):
    template_name = 'dashboard/product/product_detail.html'
    model = Catalogue
    context_object_name = 'product'


class VariantProductCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/product/product_variant_create.html'
    model = Product
    form_class = VariantForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        catalogue = get_object_or_404(Catalogue, pk=self.kwargs.get("pk"))
        kwargs.update({
            "catalogue_pk": catalogue.pk
        })
        return kwargs

    def get_success_url(self):
        success_url = reverse(
            "catalogue:product_variant_list",
            kwargs={'pk': self.object.catalogue.pk})
        return success_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            "catalogue": get_object_or_404(
                Catalogue, pk=self.kwargs.get("pk"))
        })
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        catalogue = get_object_or_404(Catalogue, pk=self.kwargs.get("pk"))
        self.object.classification = catalogue.classification
        self.object.catalogue = catalogue
        self.object.category = catalogue.category
        self.object.save()

        self.messages.success(
            _("Product Variant %s was successfully created." % self.object)
        )
        return HttpResponseRedirect(self.get_success_url())


class VariantProductManufacturedCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/product/manufactured_variant_create.html'
    model = Product
    form_class = VariantManufacturedForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        catalogue = get_object_or_404(Catalogue, pk=self.kwargs.get("pk"))
        kwargs.update({
            "catalogue_pk": catalogue.pk
        })
        return kwargs

    def get_success_url(self):
        success_url = reverse(
            "catalogue:product_variant_list",
            kwargs={'pk': self.object.catalogue.pk})
        return success_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            "catalogue": get_object_or_404(
                Catalogue, pk=self.kwargs.get("pk"))
        })
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        catalogue = get_object_or_404(Catalogue, pk=self.kwargs.get("pk"))
        self.object.catalogue = catalogue
        self.object.category = catalogue.category
        self.object.classification = Product.PRODUCT_CLASSIFICATION.manufactured
        self.object.is_manage_stock = True
        self.object.save()

        self.messages.success(
            _("Product Variant %s was successfully created." % self.object)
        )
        return HttpResponseRedirect(self.get_success_url())


class VariantDatatableRenderMixins:

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)
        return Q()

    def render_cost(self, obj):
        return obj.cost.amount

    def render_price(self, obj):
        return obj.price.amount

    def render_archived(self, obj):
        if obj.archived:
            return '{}'.format(_('Archived'))
        else:
            return '{}'.format(_('Active'))

    def render_name(self, obj):
        return '<a href="{}">{}</a>'.format(
            self.get_update_url(obj),
            escape(obj.name))

    def get_update_url(self, obj):
        if obj.classification == Product.PRODUCT_CLASSIFICATION.manufactured:
            url = reverse_lazy(
                "catalogue:variant_manufactured_update",
                kwargs={'pk': obj.pk})
        else:
            url = reverse_lazy(
                "catalogue:variant_update",
                kwargs={'pk': obj.pk})
        return url

    def render_action(self, obj):
        action_data = {
            'id': obj.id,
            'sku': escape(obj.sku),
            'name': escape(obj.name),
            'edit': self.get_update_url(obj),
            'archived': obj.archived,
            'parent': obj.catalogue.id
        }
        return action_data


class VariantProductListJson(SterlingRoleMixin,
                             VariantDatatableRenderMixins,
                             DatatableMixins, BaseDatatableView):

    columns = ['name', 'sku', 'price', 'cost', 'archived', 'action']
    order_columns = ['name', 'sku', 'price', 'cost', 'archived', 'action']

    model = Product

    def get_initial_queryset(self):
        qs = super(VariantProductListJson, self).get_initial_queryset().select_related('category')
        qs = qs.filter(catalogue=self.kwargs['pk']).annotate(
            var_count=Count(
                'variants',
                filter=Q(variants__deleted__isnull=True),
            )
        ).exclude(
            classification__in=[
                self.model.PRODUCT_CLASSIFICATION.complementary,
                self.model.PRODUCT_CLASSIFICATION.composite,
            ],
        ).exclude(
            var_count__gt=0,
        )
        return qs

    def render_action(self, obj):
        action_data = super().render_action(obj)
        action_data.update({
            'edit': reverse_lazy(
                "catalogue:product_variant_update",
                kwargs={'pk': obj.pk}),
        })
        return action_data


class VariantProductListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/product_variant_list.html'

    def get_context_data(self, **kwargs):
        context = super(VariantProductListView, self).get_context_data(**kwargs)
        product = get_object_or_404(Catalogue, pk=self.kwargs.get('pk'))
        variants = product.variants.all()
        context['product'] = product
        context['total_variants'] = variants.count()
        return context


class VariantListJson(SterlingRoleMixin,
                      VariantDatatableRenderMixins,
                      DatatableMixins, BaseDatatableView):

    columns = ['sku', 'catalogue.name', 'name', 'category.name', 'price', 'cost', 'archived', 'action']
    order_columns = ['sku', 'catalogue__name', 'name', 'category__name', 'price', 'cost', 'archived', 'action']

    model = Product

    csv_filename = 'variant_list'
    csv_header = ['sku', 'catalogue', 'name', 'category', 'price', 'cost', 'archived', ]

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().select_related('category')

        salability = self.kwargs.get("salability")
        if salability == "sellable":
            qs = qs.filter(is_sellable=True)
        elif salability == "unsellable":
            qs = qs.filter(is_sellable=False)

        qs = qs.filter(catalogue__is_active=True)
        qs = qs.exclude(classification__in=[
            self.model.PRODUCT_CLASSIFICATION.complementary,
            self.model.PRODUCT_CLASSIFICATION.composite,
        ]).exclude(
            variants__isnull=True,
            catalogue__isnull=True)
        return qs

    def filter_name(self, value):
        return Q(name__icontains=value)

    def filter_category__name(self, value):
        return Q(category__name__icontains=value)

    def filter_catalogue__name(self, value):
        return Q(catalogue__name__icontains=value)


class VariantListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/variant_list.html'


class VariantUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/product/variant_update.html'
    success_url = reverse_lazy("catalogue:variant_list")
    model = Product
    form_class = VariantForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            "catalogue_pk": self.object.catalogue.pk
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stock_managed"] = self.object.is_manage_stock
        if "is_manage_stock" in self.request.POST:
            context["is_manage_stock"] = True
        return context

    def dispatch(self, request, *args, **kwargs):
        self.current_uom = self.get_object().uom
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        uom = self.object.uom.pk if self.object.uom else None
        current_uom = self.current_uom.pk if self.current_uom else None

        update_variant_task.apply_async(
            count_down=5,
            kwargs={
                "variant_id": self.object.pk,
                "uom_id": uom,
                "current_uom_id": current_uom,
                "is_manage_stock": self.object.is_manage_stock,
                "is_batch_tracked": self.object.is_batch_tracked,
                "account_id": self.request.user.account.pk
            }
        )
        self.messages.success(
            _(f"Variant {self.object} was successfully updated.")
        )
        return response


class ProductVariantUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/product/product_variant_update.html'
    success_url = reverse_lazy("catalogue:variant_list")
    model = Product
    form_class = VariantForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            "catalogue_pk": self.object.catalogue.pk
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stock_managed"] = self.object.is_manage_stock
        if "is_manage_stock" in self.request.POST:
            context["is_manage_stock"] = True
        return context

    def dispatch(self, request, *args, **kwargs):
        self.current_uom = self.get_object().uom
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        uom = self.object.uom.pk if self.object.uom else None
        current_uom = self.current_uom.pk if self.current_uom else None

        update_variant_task.apply_async(
            count_down=5,
            kwargs={
                "variant_id": self.object.pk,
                "uom_id": uom,
                "current_uom_id": current_uom,
                "is_manage_stock": self.object.is_manage_stock,
                "is_batch_tracked": self.object.is_batch_tracked,
                "account_id": self.request.user.account.pk
            }
        )
        self.messages.success(
            _(f"Variant {self.object} was successfully updated.")
        )
        return response


class ProductVariantManufacturedUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/product/manufactured_variant_update.html'
    model = Product
    form_class = VariantManufacturedForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            "catalogue_pk": self.object.catalogue.pk
        })
        return kwargs

    def dispatch(self, request, *args, **kwargs):
        self.current_uom = self.get_object().uom
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        success_url = reverse(
            "catalogue:product_variant_list", kwargs={'pk': self.object.catalogue.pk})
        return success_url

    def form_valid(self, form):
        response = super().form_valid(form)
        uom = self.object.uom.pk if self.object.uom else None
        current_uom = self.current_uom.pk if self.current_uom else None
        update_variant_task.apply_async(
            count_down=5,
            kwargs={
                "variant_id": self.object.pk,
                "uom_id": uom,
                "current_uom_id": current_uom,
                "is_manage_stock": self.object.is_manage_stock,
                "is_batch_tracked": self.object.is_batch_tracked,
                "account_id": self.request.user.account.pk
            }
        )

        self.messages.success(
            _("Product Variant %s was successfully updated." % self.object)
        )
        return response


class VariantArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            variant_id = request.POST.get('id', '')
            catalogue_id = request.POST.get('parent', '')
            variant = Product.objects.get(pk=variant_id, catalogue__pk=catalogue_id)
            variant.archived = True
            variant.save()
            messages.success(self.request, '{}'.format(
                _('Product Variant has been archived.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class VariantActiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            variant_id = request.POST.get('id', '')
            catalogue_id = request.POST.get('parent', '')
            variant = Product.objects.get(pk=variant_id, catalogue__pk=catalogue_id)
            if not variant.catalogue.is_active:
                messages.error(self.request, '{} {} {}'.format(
                    _('Product'), variant.parent, _('is archived.')))
            else:
                variant.archived = False
                variant.save()
                messages.success(self.request, '{}'.format(
                    _('Product Variant has been re-activated.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class VariantDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def check_production_order(self, variant):
        return variant.productions_item.filter(productions__status__in=["finalized", "in_production"]).count()

    def check_bom_item_exist(self, variant):
        return variant.bom_item.all().count()

    def check_bom_exist(self, variant):
        return variant.bom.all().count()

    def can_delete_product(self, variant):
        delete = True
        total_bom = self.check_bom_exist(variant)
        total_bom_item = self.check_bom_item_exist(variant)
        total_productions = self.check_production_order(variant)

        if total_bom:
            delete = False
        if total_bom_item:
            delete = False
        if total_productions:
            delete = False
        return delete

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product_parent = request.POST.get('parent', '')
            product = Product.objects.get(pk=product_id, catalogue__pk=product_parent)
            if self.can_delete_product(product):
                product.delete()
                messages.error(self.request, '{}'.format(
                    _('Product Variant has been deleted.')))
            else:
                messages.warning(
                    self.request,
                    '{}'.format(
                        _("Tidak bisa hapus varian {}. Varian ini sedang digunakan dalam proses produksi".format(product))
                    )
                )
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


# Product AddOns
class ProductAddOnListJson(SterlingRoleMixin,
                           VariantDatatableRenderMixins,
                           DatatableMixins, BaseDatatableView):
    columns = ['name', 'sku', 'price', 'cost', 'product_list', 'archived', 'action']
    order_columns = ['name', 'sku', 'price', 'cost', 'product_list', 'archived', 'action']

    model = Product

    csv_filename = 'product_list'
    csv_header = ['sku', 'name', 'price', 'cost', 'archived']

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().select_related("catalogue")
        qs = qs.filter(classification=self.model.PRODUCT_CLASSIFICATION.complementary)
        qs = qs.annotate(
            product_list=StringAgg(
                'addon_usage__product_usage__name',
                delimiter=', ',
                filter=Q(
                    addon_usage__in=Subquery(
                        ProductAddOn.objects.filter(
                            archived=False,
                            deleted__isnull=True,
                            product=OuterRef('pk'),
                        ).values('pk')[:3]
                    )
                )
            )
        )
        return qs

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)
        return Q()

    def filter_product_list(self, value):
        return Q(product_list__icontains=value)

    def filter_name(self, value):
        return Q(name__icontains=value)

    def render_action(self, obj):
        action_data = super().render_action(obj)
        action_data.update({
            'edit': reverse_lazy(
                "catalogue:product_addon_update",
                kwargs={'pk': obj.pk}),
        })
        return action_data

    def render_name(self, obj):
        return '<a href="{}">{}</a>'.format(
            reverse_lazy(
                "catalogue:product_addon_update",
                kwargs={'pk': obj.pk}),
            escape(obj.name))


class ProductAddOnListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/product_addon_list.html'


class ProductUsageAutocompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Product.objects.exclude(
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.complementary,
                Product.PRODUCT_CLASSIFICATION.composite]).filter(archived=False)

        if self.q:
            qs = qs.filter(
                name__istartswith=self.q) | qs.filter(name__icontains=self.q) | qs.filter(sku__istartswith=self.q)
        return qs

    def get_result_label(self, item):
        return "{} - {}".format(item.sku, item.name)


class ProductAddOnCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/product/product_addon_create.html'
    model = Product
    success_url = reverse_lazy("catalogue:product_addon_list")
    form_class = ProductComplementaryForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "is_manage_stock" in self.request.POST:
            context["is_manage_stock"] = True
        return context

    def form_valid(self, form):
        response = super(ProductAddOnCreateView, self).form_valid(form)
        self.messages.success(
            _("Product Add On %s was successfully created." % self.object)
        )
        return response


class ProductAddOnUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/product/product_addon_update.html'
    model = Product
    success_url = reverse_lazy("catalogue:product_addon_list")
    form_class = ProductComplementaryForm
    product_usages_list = None

    def dispatch(self, request, *args, **kwargs):
        self.current_uom = self.get_object().uom
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stock_managed"] = self.object.is_manage_stock
        if "is_manage_stock" in self.request.POST:
            context["is_manage_stock"] = True
        return context

    def form_valid(self, form):
        response = super(ProductAddOnUpdateView, self).form_valid(form)
        uom = self.object.uom.pk if self.object.uom else None
        current_uom = self.current_uom.pk if self.current_uom else None
        update_variant_task.apply_async(
            count_down=5,
            kwargs={
                "variant_id": self.object.pk,
                "uom_id": uom,
                "current_uom_id": current_uom,
                "is_manage_stock": self.object.is_manage_stock,
                "is_batch_tracked": self.object.is_batch_tracked,
                "account_id": self.request.user.account.pk
            }
        )

        self.messages.success(
            _(f"Product Add On {self.object} was successfully updated.")
        )
        return response


class ProductAddonArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            productaddon_id = request.POST.get('id', '')
            productaddon = Product.objects.get(id=productaddon_id,
                                               classification=Product.PRODUCT_CLASSIFICATION.complementary)
            addons = ProductAddOn.objects.filter(product=productaddon)
            for addon in addons:
                addon.archived = True
                addon.save()

            productaddon.archived = True
            productaddon.save()
            messages.success(self.request, '{}'.format(
                _('Product Addon has been archived.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductAddonActiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            productaddon_id = request.POST.get('id', '')
            productaddon = Product.objects.get(id=productaddon_id,
                                               classification=Product.PRODUCT_CLASSIFICATION.complementary)
            productaddon.archived = False
            productaddon.save()
            addons = ProductAddOn.objects.filter(product=productaddon)
            for addon in addons:
                addon.archived = False
                addon.save()
            messages.success(self.request, '{}'.format(
                _('Product Addon has been re-activated.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductAddOnDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            productaddon_id = request.POST.get('id', '')
            productaddon = Product.objects.get(id=productaddon_id,
                                               classification=Product.PRODUCT_CLASSIFICATION.complementary)
            productaddon.delete()
            messages.error(self.request, '{}'.format(
                _('Product Addon has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


# Product Modifier
class ModifierListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'option_values', 'archived', 'action']
    order_columns = ['name', 'option_values', 'archived', 'action']

    model = ProductModifier

    def get_initial_queryset(self):
        qs = super(ModifierListJson, self).get_initial_queryset()
        qs = qs.annotate(
            option_values=StringAgg(
                'options__value',
                delimiter=', ',
                filter=Q(
                    options__in=Subquery(
                        ProductModifierOption.objects.filter(
                            deleted__isnull=True,
                            product_modifier=OuterRef('pk'),
                        ).values('pk')[:3]
                    )
                )
            ),
        )
        return qs

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)
        return Q()

    def filter_option_values(self, value):
        return Q(option_values__icontains=value)
    
    def filter_name(self, value):
        return Q(name__icontains=value)

    def render_action(self, obj):
        action_data = {
            'id': obj.id,
            'name': escape(obj.name),
            'edit': reverse_lazy(
                "catalogue:modifier_update",
                kwargs={'pk': obj.pk}),
            'archived': obj.archived
        }
        return action_data

    def render_archived(self, obj):
        if obj.archived:
            return '{}'.format(_('Archived'))
        else:
            return '{}'.format(_('Active'))

    def render_name(self, obj):
        return '<a href="{}">{}</a>'.format(
            reverse_lazy(
                "catalogue:modifier_update",
                kwargs={'pk': obj.pk}),
            escape(obj.name))


class ModifierListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/modifier_list.html'


class ModifierCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/product/modifier_create.html'
    model = ProductModifier
    form_class = ProductModifierForm
    success_url = reverse_lazy("catalogue:modifier_list")

    def get_success_url(self):
        self.messages.success(
            _("Product Modifier %s was successfully created." % self.object)
        )
        return self.success_url


class ProductModifierListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'option_values', 'archived', 'action']
    order_columns = ['name', 'option_values', 'archived', 'action']

    model = ProductModifier

    def get_initial_queryset(self):
        qs = super(ProductModifierListJson, self).get_initial_queryset()
        qs = qs.filter(usages__product__pk=self.kwargs.get('pk'))
        qs = qs.annotate(option_values=StringAgg(
            'options__value', delimiter=', ',
            filter=Q(options__deleted__isnull=True)))
        return qs

    def filter_archived(self, value):
        if value != 'show':
            return Q(archived__istartswith=value)
        return Q()

    def filter_option_values(self, value):
        return Q(option_values__icontains=value)

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy(
                    "catalogue:product_modifier_update",
                    kwargs={
                        'product_pk': self.kwargs.get('pk'),
                        'pk': row.pk}),
                'archived': row.archived
            }
            return action_data
        elif column == 'archived':
            if row.archived:
                return '{}'.format(_('Archived'))
            else:
                return '{}'.format(_('Active'))
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "catalogue:product_modifier_update",
                    kwargs={
                        'product_pk': self.kwargs.get('pk'),
                        'pk': row.pk
                    }), escape(row.name))

        else:
            return super(ProductModifierListJson, self).render_column(row, column)


class ProductModifierListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/product_modifier_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProductModifierListView, self).get_context_data(**kwargs)
        product = get_object_or_404(Product, pk=self.kwargs.get('pk'))
        modifiers = product.modifier.filter(archived=False)
        kwargs['product'] = product
        kwargs['total_modifiers'] = modifiers.count()
        context.update(kwargs)
        return context


class UpdateModiferProductForm(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            forms = {}
            deleted_data = []
            prefix = request.POST.get('prefix', '')
            total_form = request.POST.get('total_form', '')
            initial_data = json.loads(request.POST.get('initial_data', ''))
            modifieropt = request.POST.get('modifieropt', '')
            context = {}
            context['modifieropt'] = int(modifieropt)
            data = {
                prefix + '-TOTAL_FORMS': total_form,
                prefix + '-INITIAL_FORMS': '0',
                prefix + '-MAX_NUM_FORMS': '',
            }
            for i in initial_data:
                data.update({i: initial_data[i]})

            for i in data:
                if "DELETE" in i:
                    if data[i]:
                        deleted_data.append(i)

            formset = ProductModifierOptionFormSet(data, prefix=prefix)
            template = 'dashboard/product/product_modifieroption_formset.html'
            form = render_to_string(template, {'formset': formset, 'context': context})
            forms['form'] = form
            forms['deleted_data'] = deleted_data
            response['data'] = forms
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductModifierTypeaheadView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'success',
            'data': {}
        }
        status_code = 200
        modifier_name = []
        data = {}
        modifiers = ProductModifier.objects.filter(archived=False)
        for modifier in modifiers:
            modifier_name.append(modifier.name)

        data['data_modifier'] = modifier_name
        response['data'] = data

        return self.render_json_response(response, status=status_code)


class ProductModifierUpdateView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/product/modifier_update.html'
    model = ProductModifier
    form_class = ProductModifierForm

    def get_success_url(self):
        success_url = reverse(
            "catalogue:product_modifier_list",
            kwargs={'pk': self.kwargs.get('product_pk')})
        return success_url

    def get_context_data(self, **kwargs):
        context = super(ProductModifierUpdateView, self).get_context_data(**kwargs)
        self.product = get_object_or_404(Product, pk=self.kwargs.get('product_pk'))
        context.update({
            'product': self.product,
        })
        return context


class ModifierUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/product/modifier_update.html'
    model = ProductModifier
    form_class = ProductModifierForm
    success_url = reverse_lazy("catalogue:modifier_list")

    def form_valid(self, form):
        self.messages.success(
            _("Product Modifier %s was successfully updated." % self.object)
        )
        return super().form_valid(form)


class ProductModifierArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            modifier_id = request.POST.get('id', '')
            modifier = ProductModifier.objects.get(id=modifier_id)
            modifier.archived = True
            modifier.save()
            messages.success(self.request, '{}'.format(
                _('Product Modifier has been archived.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductModifierActiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            modifier_id = request.POST.get('id', '')
            modifier = ProductModifier.objects.get(id=modifier_id)
            modifier.archived = False
            modifier.save()
            messages.success(self.request, '{}'.format(
                _('Product Modifier has been re-activated.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductModifierDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            modifier_id = request.POST.get('id', '')
            modifier = ProductModifier.objects.get(id=modifier_id)
            modifier.delete()
            messages.error(self.request, '{}'.format(
                _('Product Modifier has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductModifierAutocompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = ProductModifier.objects.all()
        qs = qs.filter(archived=False)
        if self.q:
            qs = qs.filter(name__istartswith=self.q, archived=False)
        return qs


# Composite Product
class CompositeProductListJson(SterlingRoleMixin,
                               VariantDatatableRenderMixins,
                               DatatableMixins, BaseDatatableView):

    columns = ['name', 'sku', 'price', 'cost', 'archived', 'action']
    order_columns = ['name', 'sku', 'price', 'cost', 'archived', 'action']

    model = Product

    def get_initial_queryset(self):
        qs = super(CompositeProductListJson, self).get_initial_queryset()
        qs = qs.filter(classification=self.model.PRODUCT_CLASSIFICATION.composite)
        return qs

    def filter_name(self, value):
        return Q(name__icontains=value)

    def render_action(self, obj):
        action_data = super().render_action(obj)
        action_data.update({
            'edit': reverse_lazy(
                "catalogue:product_composite_update",
                kwargs={'pk': obj.pk}),
        })
        return action_data

    def render_name(self, obj):
        return '<a href="{}">{}</a>'.format(
            reverse_lazy(
                "catalogue:product_composite_update",
                kwargs={'pk': obj.pk}),
            escape(obj.name)
        )


class CompositeProductListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product/product_composite_list.html'


class UpdateCompositeProductForm(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            forms = {}
            deleted_data = []
            prefix = request.POST.get('prefix', '')
            total_form = request.POST.get('total_form', '')
            initial_data = json.loads(request.POST.get('initial_data', ''))
            total_product = request.POST.get('total_product', '')
            product_selected = request.POST.get('product_selected', '')
            context = {}
            context['total_product'] = int(total_product)
            data = {
                prefix + '-TOTAL_FORMS': total_form,
                prefix + '-INITIAL_FORMS': '0',
                prefix + '-MAX_NUM_FORMS': '',
            }

            product = None
            if product_selected:
                product = Product.objects.get(pk=initial_data[product_selected],
                                              classification=Product.PRODUCT_CLASSIFICATION.standart)
                split_val = product_selected.split('-')
                data_prefix = split_val[0] + "-" + split_val[1] + "-price"

            for i in initial_data:
                if product_selected:
                    if data_prefix in i:
                        if product:
                            data.update({i: product.price})
                        else:
                            data.update({i: initial_data[i]})
                    else:
                        data.update({i: initial_data[i]})
                else:
                    data.update({i: initial_data[i]})

            for i in data:
                if "DELETE" in i:
                    if data[i]:
                        deleted_data.append(i)

            formset = CompositeMaterialProductFormFormSet(data, prefix=prefix)
            template = 'dashboard/product/product_composite_formset.html'
            form = render_to_string(template, {'formset': formset, 'context': context})
            forms['form'] = form
            forms['deleted_data'] = deleted_data
            response['data'] = forms
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CompositeProductFormMixin(object):
    formsets = None

    def process_product_form(self, product_formset):
        products = product_formset.save(commit=True)

    def formsets_all_valid(self):
        valid = True
        if self.formsets is not None:
            for key, formset in self.formsets.items():
                valid = valid and formset.is_valid()
        return valid

    def form_valid(self, form):
        if not self.formsets_all_valid():
            return self.form_invalid(form)

        created = self.object is None
        self.object = form.save()

        self.formsets['product_formset'].instance = self.object
        self.process_product_form(self.formsets['product_formset'])

        if not created:
            messages.success(self.request, '{}'.format(
                _('Product Composite has been updated.')))
        else:
            messages.success(self.request, '{}'.format(
                _('Product Composite has been created.')))
        return HttpResponseRedirect(self.get_success_url())


class ProductCompositeMaterailPriceView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            product_pk = request.GET.get("product_pk", "")
            product = Product.objects.get(pk=product_pk)
            response["status"] = "success"
            response["result"]["price"] = "%s" % product.price
            status_code = 200
        except Product.DoesNotExist:
            pass
        except ValueError:
            status_code = 200
            response["result"]["price"] = ""

        return self.render_json_response(response, status=status_code)


class CompositeProductCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/product/product_composite_create.html'
    model = Product
    form_class = CompositeProductForm
    success_url = reverse_lazy("catalogue:product_composite_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["formset"] = ProductCompositeMaterialFormset
        context["formset_helper"] = ProductCompositeMaterialFormsetHelper
        return context

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = ProductCompositeMaterialFormset(form_params, instance=form.instance)

        if not formset.is_valid():
            self.messages.error(
                _(
                    u"""There was a problem creating this %s.
                    Please review the fields marked red and resubmit."""
                )
                % self.model._meta.verbose_name
            )
            return self.form_invalid(form)

        with transaction.atomic():
            product = form.instance
            product.classification = Product.PRODUCT_CLASSIFICATION.composite
            product.save()
            product.catalogue = Catalogue.prepare_catalogue(product)
            product.save()

            for composite_material in formset:
                deleted = composite_material.cleaned_data.get("DELETE")
                if composite_material.is_valid() and not deleted:
                    composite = composite_material.save(commit=False)
                    composite.composite_product = product
                    composite.save()

        self.messages.success(
            _("Product Composite %s was successfully created." % product)
        )
        return super().form_valid(form)


class CompositeProductUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/product/product_composite_update.html'
    model = Product
    form_class = CompositeProductForm
    success_url = reverse_lazy("catalogue:product_composite_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["formset"] = ProductCompositeMaterialFormset(
            instance=self.object,
            queryset=CompositeProduct.objects.filter(
                product__archived=False,
                product__deleted__isnull=True
            )
        )
        context["formset_helper"] = ProductCompositeMaterialFormsetHelper
        return context

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = ProductCompositeMaterialFormset(form_params, instance=form.instance)

        if not formset.is_valid():
            self.messages.error(
                _(
                    u"""There was a problem creating this %s.
                    Please review the fields marked red and resubmit."""
                )
                % self.model._meta.verbose_name
            )
            return self.form_invalid(form)

        with transaction.atomic():
            product = form.instance
            product.save()
            product.catalogue.name = product.name
            product.catalogue.image_location = product.image_location
            product.catalogue.category = product.category
            product.catalogue.save()

            for deleted_form in formset.deleted_forms:
                if deleted_form.instance and deleted_form.instance.pk:
                    deleted_form.instance.delete()

            instances = formset.save(commit=False)
            for composite in instances:
                composite.composite_product = product
                composite.save()

        self.messages.success(
            _("Product Composite %s was successfully updated." % product)
        )

        return super().form_valid(form)


class CompositeProductArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Product.objects.get(pk=product_id, classification=Product.PRODUCT_CLASSIFICATION.composite)
            product.archived = True
            product.save()
            messages.success(self.request, '{}'.format(
                _('Product Composite has been archived.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CompositeProductActivateView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Product.objects.get(pk=product_id, classification=Product.PRODUCT_CLASSIFICATION.composite)
            product.archived = False
            product.save()
            messages.success(self.request, '{}'.format(
                _('Product Composite has been re-activated.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CompositeProductDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            product_id = request.POST.get('id', '')
            product = Product.objects.get(pk=product_id, classification=Product.PRODUCT_CLASSIFICATION.composite)
            product.delete()
            messages.error(self.request, '{}'.format(
                _('Product Composite has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


#  Product Type
class ProductTypeListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name']
    order_columns = ['name']
    model = ProductType

    csv_filename = 'product_type'
    csv_header = ['name']


class ProductTypeListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product_type/product_type_list.html'


class ProductTypeDetailView(SterlingRoleMixin, DetailView):
    template_name = 'dashboard/product_type/product_type_detail.html'
    model = ProductType


#  Category
class CategoryListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'product_count', 'description', 'archived', 'action']
    order_columns = ['name', 'product_count', 'description', 'archived', 'action']
    model = Category

    def get_initial_queryset(self):
        qs = super(CategoryListJson, self).get_initial_queryset().prefetch_related('product_set')
        qs = qs.exclude(
            name="Paketan",
            classification=Category.CATEGORY_CLASSIFICATION.composite, depth=1)
        qs = qs.annotate(
            product_count=Coalesce(
                Count('product', filter=Q(product__deleted__isnull=True)
                      ), Value(0))
        ).order_by('-path')
        return qs

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)
        return Q()

    def filter_name(self, value):
        return Q(name__icontains=value)

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'slug': row.slug,
                'name': escape(row.name),
                'edit': reverse_lazy("catalogue:category_update", kwargs={'pk': row.pk}),
                'archived': row.archived
            }
            return action_data
        elif column == 'archived':
            if row.archived:
                return '{}'.format(_('Archived'))
            else:
                return '{}'.format(_('Active'))
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "catalogue:category_update", kwargs={'pk': row.pk}),
                escape(row.full_name))
        else:
            return super(CategoryListJson, self).render_column(row, column)


class CategoryAutocompleteReadOnlyView(autocomplete.Select2QuerySetView):
    
    def get_base_queryset(self):
        qs = Category.objects.filter(archived=False)

        if self.q:
            qs = qs.filter(name__istartswith=self.q, archived=False)

        return qs

    def get_queryset(self):
        qs = self.get_base_queryset().exclude(
            classification=Category.CATEGORY_CLASSIFICATION.composite)
        return qs


class CategoryAutocompleteView(CategoryAutocompleteReadOnlyView):

    def create_object(self, text):
        try:
            obj = Category.objects.get(name=text)
        except Category.DoesNotExist:
            obj = self.form.save()
        return obj

    def post(self, request):
        text = request.POST.get('text', None)
        self.form = CategoryCreateForm(data={"name": text, })
        if not self.form.is_valid():
            return HttpResponseBadRequest()
        return super().post(request)

    def has_add_permission(self, request):
        if request.user.is_authenticated:
            return True
        return False


class CategoryCompositeAutocompleteView(CategoryAutocompleteView):

    def get_queryset(self):
        qs = super(CategoryCompositeAutocompleteView, self).get_base_queryset()
        qs = qs.filter(classification=Category.CATEGORY_CLASSIFICATION.composite)
        return qs

    def create_object(self, text):
        text = text.split(">")[-1].lstrip()
        paketan = Category.get_composite_root()

        try:
            obj = Category.objects.get(name=text)
        except Category.DoesNotExist:
            obj = create_from_breadcrumbs(text)
            obj.classification = Category.CATEGORY_CLASSIFICATION.composite
            obj.save()

            if obj.get_ancestors().exists():
                obj.get_ancestors().first().move(paketan, 'last-children')
            else:
                obj.move(paketan, 'last-child')
        return obj

    def post(self, request):
        text = request.POST.get('text', None)
        self.form = CategoryCreateForm(data={"name": text, "composite_only": True})
        if not self.form.is_valid():
            return HttpResponseBadRequest()
        try:
            res = super().post(request)
            return res
        except:
            # Unable to create composite root category
            return HttpResponseBadRequest()


class CategoryPromoAutoCompleteView(autocomplete.Select2QuerySetView):
    def get_base_queryset(self):
        qs = Category.objects.filter(archived=False)

        if self.q:
            qs = qs.filter(name__istartswith=self.q, archived=False)

        return qs

    def get_queryset(self):
        qs = self.get_base_queryset().exclude(
            classification=Category.CATEGORY_CLASSIFICATION.complementary)
        return qs


class CategoryListView(SterlingRoleMixin, ListView):
    template_name = 'dashboard/category/category_list.html'
    model = Category


class CategoryDetailView(SterlingRoleMixin, DetailView):
    template_name = 'dashboard/category/category_detail.html'
    model = Category


class CategoryCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/category/category_create.html'
    model = Category
    success_url = reverse_lazy("catalogue:category_list")
    form_class = CategoryCreateForm

    def form_valid(self, form):
        response = super().form_valid(form)
        self.messages.success(
            _("Category %s was successfully created." % self.object)
        )
        return response


class CategoryArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            category_id = request.POST.get('id', '')
            category = Category.objects.get(pk=category_id)
            category.archived = True
            category.save()
            category.catalogue_set.invalidated_update(
                is_active=False)
            category.product_set.invalidated_update(
                archived=True)
            messages.success(self.request, '{}'.format(
                _('Category has been archived.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CategoryActivatedView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            category_id = request.POST.get('id', '')
            category = Category.objects.get(pk=category_id)
            category.archived = False
            category.save()
            category.catalogue_set.invalidated_update(
                is_active=True)
            category.product_set.invalidated_update(
                archived=False)
            messages.success(self.request, '{}'.format(
                _('Category has been re-activated.')))

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CategoryDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def check_production_order(self, variant):
        return variant.productions_item.filter(productions__status__in=["finalized", "in_production"]).count()

    def check_bom_item_exist(self, variant):
        return variant.bom_item.all().count()

    def check_bom_exist(self, variant):
        return variant.bom.all().count()

    def can_delete_product(self, category):
        delete = True
        total_bom = 0
        total_bom_item = 0
        total_productions = 0

        for product in category.catalogue_set.all():
            for variant in product.variants.all():
                total_bom += self.check_bom_exist(variant)
                total_bom_item += self.check_bom_item_exist(variant)
                total_productions += self.check_production_order(variant)

        if total_bom:
            delete = False
        if total_bom_item:
            delete = False
        if total_productions:
            delete = False
        return delete

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            category_id = request.POST.get('id', '')
            category = Category.objects.get(pk=category_id)
            if self.can_delete_product(category):
                category.delete()
                messages.error(self.request, '{}'.format(
                    _('Category has been deleted.')))
            else:
                messages.warning(
                    self.request, 
                    '{}'.format(
                        _(
                            "Tidak bisa hapus kategori {}. Salah satu produk/varian sedang digunakan dalam proses produksi".format(
                                category
                            )
                        )
                    )
                )
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CategoryUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/category/category_update.html'
    model = Category
    success_url = reverse_lazy("catalogue:category_list")
    form_class = CategoryForm

    def form_valid(self, form):
        self.messages.success(
            _("Category %s was successfully updated." % self.object)
        )
        return super(CategoryUpdateView, self).form_valid(form)


class ProductCostView(JSONResponseMixin, AjaxResponseMixin, View):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            product_pk = request.GET.get("product", "")
            product = Product.objects.get(pk=product_pk)
            response["status"] = "success"
            response["result"]["cost"] = int(product.cost.amount)
            status_code = 200
        except Product.DoesNotExist:
            pass

        return self.render_json_response(response, status=status_code)


class VariantMovingAverageCostUpdateView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def get_object(self):
        obj = get_object_or_404(
            Product, pk=self.kwargs.get("pk")
        )
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        instance = self.get_object()
        mac = instance.moving_average_cost
        new_mac = 0
        if instance:
            instance.cost = mac
            instance.moving_average_cost = new_mac
            instance.save()
            response["status"] = "success"
            status_code = 200
        return self.render_json_response(response, status=status_code)


class ProductImportTemplateView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/product/product_import.html"


class ProductImportView(SterlingRoleMixin, SterlingImportMixin):
    template_name = "dashboard/product/product_import_form.html"
    resource_class = ProductResource
    file_headers = [
        'sku', 'catalogue', 'name', 'category', 'price', 'cost'
    ]
    success_url = reverse_lazy("catalogue:product_import")


class ManufacturedProductImportTemplateView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/product/manufactured_product_import.html"


class ManufacturedProductImportView(SterlingRoleMixin, SterlingImportMixin):
    template_name = "dashboard/product/manufactured_product_import_form.html"
    resource_class = ManufacturedProductResource
    file_headers = [
        "sku",
        'catalogue',
        'name',
        'category',
        'price',
        'cost',
        "uom",
    ]
    success_url = reverse_lazy("catalogue:manufactured_product_import")
