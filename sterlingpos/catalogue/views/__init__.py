from sterlingpos.catalogue.views.views import *
from sterlingpos.catalogue.views.dynamic_composite import (
    DynamicCompositeProductListJson,
    DynamicCompositeProductListView,
    
    ProductDynamicCompositeMaterailPriceView,

    UpdateDynamicCompositeProductForm,

    DynamicCompositeProductCreateView,
    DynamicCompositeProductUpdateView,
    DynamicCompositeProductArchiveView,
    DynamicCompositeProductActivateView,
    DynamicCompositeProductDeleteView,
)
