from sterlingpos.role.models import Role


def get_outlet_of_user(user):
    outlet_qs = user.account.outlet_set.none()

    if user.is_owner:
        outlet_qs = user.account.outlet_set.all()
    else:
        if hasattr(user, "device_user"):
            outlet_qs = user.device_user.outlet.all()
    return outlet_qs

def create_initial_roles_for_account(account):
    manager, _ = Role.objects.get_or_create(
        name="Manager",
        is_supervisor=True,
        is_protected=True,
        mobile_app_permission=[
            "can_access_application",

            "can_access_transaction_report",
            "can_access_product_report",

            "can_create_transaction",
            "can_cancel_printed_item",
            "can_delete_printed_item",
            "can_edit_printed_item",
            "can_print_closed_bill",
            "can_delete_seat",
            "can_delete_discount_loyalty",
            "can_delete_cart",
            "can_void_transaction",
            "can_disable_product",
            "can_open_drawer",

            "can_edit_global_discount",
        ],
        dashboard_permission=[
            "can_access_dashboard",
            "can_access_catalogue",
            "can_access_promo",
            "can_access_device",
            "can_access_application",
            "can_access_sales",
            "can_access_sales_settings",
            "can_access_cash_balance",
            "can_access_outlet",
            "can_access_account",
            "can_access_receipt",
            "can_access_surcharges",
            "can_access_role",
            "can_access_stock",
            "can_access_customer",
        ],
        account=account,
    )

    staff, _ = Role.objects.get_or_create(
        name="Staff",
        is_supervisor=False,
        is_protected=True,
        mobile_app_permission=[
            "can_access_application",

            "can_access_transaction_report",
            "can_access_product_report",

            "can_create_transaction",
            "can_cancel_printed_item",
            "can_delete_printed_item",
            "can_edit_printed_item",
            "can_delete_cart",
            "can_void_transaction",
            "can_disable_product",
            "can_open_drawer",
            "can_print_closed_bill",
            "can_delete_seat",
            "can_delete_discount_loyalty",

            "can_edit_global_discount",
        ],
        dashboard_permission=[],
        account=account,
    )

    cashier, _ = Role.objects.get_or_create(
        name="Cashier",
        is_supervisor=False,
        is_protected=True,
        mobile_app_permission=[
            "can_access_application",

            "can_access_transaction_report",
            "can_access_product_report",

            "can_create_transaction",

            "can_edit_global_discount",
        ],
        dashboard_permission=[],
        account=account,
    )

    if not account.rolesettings_set.exists():
        _ = account.rolesettings_set.create(account=account)

    return (manager, staff, cashier)

