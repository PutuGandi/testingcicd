import rules
from django.test import TestCase

from model_mommy import mommy

from sterlingpos.role.models import Role


class TestPredicates(TestCase):

    def test_can_access_view(self):
        account = mommy.make('accounts.account', name='test-1')
        user = mommy.make(
            'users.User',
            email='example@test.com',
            account=account)

        role = Role.objects.create(
            name='Test Role',
            dashboard_permission=[
                "can_access_dashboard",
                "can_access_catalogue",
            ],
            account=account,
        )

        device_user = mommy.make(
            "device.DeviceUser",
            account=account,
            user=user,
            role=role,
        )

        self.assertTrue(
            rules.test_rule('can_access_view', user, "catalogue"))
