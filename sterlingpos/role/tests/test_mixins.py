from django.test import TestCase

from model_mommy import mommy

from sterlingpos.role.mixins import SterlingRoleMixin


class TestSterlingRoleMixin(TestCase):

    def test_get_app_name(self):
        view = SterlingRoleMixin()
        self.assertEqual(
            view.get_app_name(), "role")

    def test_get_permission_name(self):
        view = SterlingRoleMixin()
        self.assertEqual(
            view.get_permission_name(), "role")
