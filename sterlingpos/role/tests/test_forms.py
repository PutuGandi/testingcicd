from test_plus.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.role.forms import RoleForm


class RoleFormTest(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Test Account')
        set_current_tenant(self.account)
        self.form_class = RoleForm

    def tearDown(self):
        set_current_tenant(None)

    def test_create_role(self):
        data = {
            "name": "New Role",
            "is_supervisor": True,
            "can_access_dashboard": True,
            "can_access_apps": True,
            "dashboard_permission": [],
            "mobile_app_permission": [],
        }

        form = self.form_class(data=data)
        self.assertTrue(
            form.is_valid(), form.errors)
        role = form.save()
        self.assertIsNotNone(role.pk)
        self.assertTrue(role.is_supervisor)
        self.assertEqual(role.account, get_current_tenant())
