from django.test import TestCase

from model_mommy import mommy

from sterlingpos.role.utils import create_initial_roles_for_account


class TestUtils(TestCase):

    def test_create_initial_role_for_account(self):
        account = mommy.make(
            'accounts.Account', name='Test Account')
        manager, staff, cashier = create_initial_roles_for_account(account)
        self.assertEqual(account.role_set.count(), 3)
