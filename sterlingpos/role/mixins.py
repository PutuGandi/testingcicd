import rules
from django.apps import apps as django_apps
from django.conf import settings
from django.http.response import HttpResponseRedirect
from django.http import Http404

from braces.views import UserPassesTestMixin

from sterlingpos.role.models import Role


class SterlingRoleMixin(UserPassesTestMixin):

    def get_app_name(self):
        return self.__module__.split(".")[1]
    
    def get_permission_name(self):

        if hasattr(self, "role_permission_name"):
            return self.role_permission_name

        app_name = self.get_app_name()
        try:
            app_config = django_apps.get_app_config(app_name)
        except LookupError:
            app_config = None

        permission_name = getattr(
            app_config, "permission_name", app_name)
        return permission_name

    def test_func(self, user):
        permission_name = self.get_permission_name()
        has_permission = rules.test_rule(
            'can_access_view', user, permission_name)
        return has_permission

    def no_permissions_fail(self, request=None):
        raise Http404
