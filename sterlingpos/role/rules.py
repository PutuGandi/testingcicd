import rules


@rules.predicate
def has_access(user, module_name):
    permission = "can_access_{}".format(module_name)
    if hasattr(user, "device_user") and user.device_user.role:
        return permission in user.device_user.role.dashboard_permission
    return False

@rules.predicate
def is_owner(user):
    return user.is_owner


rules.set_rule('can_access_view', is_owner | has_access)
