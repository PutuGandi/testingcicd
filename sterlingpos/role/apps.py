from django.apps import AppConfig
from django.db.models.signals import post_save


class RoleConfig(AppConfig):
    name = 'sterlingpos.role'
    verbose_name = "Roles"

    def ready(self):
        pass
