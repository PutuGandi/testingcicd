from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views


app_name = 'role'
urlpatterns = [
    url(r'^$', views.RoleListView.as_view(),
        name='list'),
    url(r'^(?P<pk>\d+)/$', views.RoleUpdateView.as_view(),
        name='update'),
    url(r'^delete/$', views.RoleDeleteView.as_view(),
        name='delete'),
    url(r'^create/$', views.RoleCreateView.as_view(),
        name='create'),
    url(r'^settings/$', views.RoleSettingUpdateView.as_view(),
        name='settings'),

    url(r'^data/$', views.RoleListJsonView.as_view(),
        name='data'),
]
