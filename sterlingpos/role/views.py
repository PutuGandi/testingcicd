import datetime
import os
import uuid

from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.views.generic import (
    ListView, DetailView, View, TemplateView,
    CreateView, UpdateView, DeleteView, View
)
from braces.views import (
    LoginRequiredMixin, JSONResponseMixin, AjaxResponseMixin
)
from formtools.wizard.views import SessionWizardView
from dal import autocomplete

from sterlingpos.core.models import get_current_tenant
from sterlingpos.core.mixins import DatatableMixins

from sterlingpos.role.models import Role, RoleSettings
from sterlingpos.role.forms import RoleForm, RoleSettingsForm
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.core.mixins import OwnerOnlyMixin


class RoleListJsonView(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "name", "is_supervisor", "action"]
    order_columns = [
        "name", "is_supervisor", "action"]
    datetime_col = ["created",]

    model = Role

    def filter_is_supervisor(self, value):
        if value != "show":
            return Q(is_supervisor__istartswith=value)


    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'is_protected': row.is_protected,
                'edit': reverse_lazy(
                    "role:update", kwargs={'pk': row.pk}),
            }
            return action_data
        elif column =='is_supervisor':
            if row.is_supervisor:
                return _("Yes")
            else:
                return _("No")
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "role:update", kwargs={'pk': row.pk}), escape(row.name))
        else:
            return super().render_column(row, column)


class RoleListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/role/role_list.html"


class RoleUpdateView(SterlingRoleMixin, UpdateView):
    template_name = "dashboard/role/role_create.html"
    model = Role
    form_class = RoleForm

    def get_success_url(self):
        return reverse_lazy("role:list")


class RoleCreateView(SterlingRoleMixin, CreateView):
    template_name = "dashboard/role/role_create.html"
    model = Role
    form_class = RoleForm

    def get_success_url(self):
        return reverse_lazy("role:list")


class RoleDeleteView(
        SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            role_id = request.POST.get('id', '')
            role = Role.objects.get(pk=role_id)
            role.delete()
            messages.error(self.request, '{}'.format(
                _('Role has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class RoleSettingUpdateView(
        SterlingRoleMixin, UpdateView):
    model = RoleSettings
    template_name = "dashboard/role/role_settings.html"
    form_class = RoleSettingsForm

    def get_success_url(self):
        success_url = reverse("role:settings")
        return success_url

    def get_object(self, queryset=None):
        obj, _ = RoleSettings.objects.get_or_create(
            account=self.request.user.account
        )
        return obj

    def form_valid(self, form):
        form.save()
        messages.success(self.request, '{}'.format(
            _('Role Settings has been updated.')))
        return HttpResponseRedirect(self.get_success_url())

