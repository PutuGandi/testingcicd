from django.contrib import admin
from django.forms import widgets
from django.forms.fields import TypedChoiceField
from django.contrib.postgres.forms.array import SimpleArrayField

from sterlingpos.role.models import (
    Role,
    RoleSettings
)


class RoleAdmin(admin.ModelAdmin):

    list_display = (
        "name", "is_supervisor",
        "is_protected", "account")
    search_fields = ("account__name",)

    raw_id_fields = ("account",)
    list_filter = ("is_supervisor", "is_protected",)

    def get_form(self, *args, **kwargs):
        form_class = super().get_form_class(*args, **kwargs)
        form_class.base_fields["mobile_app_permission"].widget = admin.widgets.FilteredSelectMultiple(
            verbose_name=form_class.base_fields["mobile_app_permission"].label,
            is_stacked=False,
            choices=form_class.base_fields['mobile_app_permission'].base_field._choices)
        form_class.base_fields["dashboard_permission"].widget = admin.widgets.FilteredSelectMultiple(
            verbose_name=form_class.base_fields["dashboard_permission"].label,
            is_stacked=False,
            choices=form_class.base_fields['dashboard_permission'].base_field._choices)
        return form_class


class RoleSettingsAdmin(admin.ModelAdmin):

    list_display = ("account",)
    search_fields = ("account__name",)


admin.site.register(
    Role, RoleAdmin)
admin.site.register(
    RoleSettings, RoleSettingsAdmin)
