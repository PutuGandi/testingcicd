from django import forms
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, ugettext
from django.template.loader import render_to_string

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Submit
from dal import autocomplete

from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from sterlingpos.role.models import (
    Role,
    RoleSettings,
    DASHBOARD_PERMISSION,
    MOBILE_APP_PERMISSION,
)


class RoleForm(forms.ModelForm):

    dashboard_permission = forms.MultipleChoiceField(
        choices=DASHBOARD_PERMISSION,
        widget=forms.CheckboxSelectMultiple,
        label='',
        required=False,
    )
    mobile_app_permission = forms.MultipleChoiceField(
        choices=MOBILE_APP_PERMISSION,
        widget=forms.CheckboxSelectMultiple,
        label='',
        required=False,
    )

    class Meta:
        model = Role
        exclude = ("account", "is_protected")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.include_media = False

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Role Detail"),
                KawnField('name'),
                KawnField(
                    'is_supervisor',
                    template='dashboard/form_fields/switch.html'
                ),
            ),
            KawnFieldSetWithHelpText(
                _("POS Access"),
                KawnField(
                    'mobile_app_permission',
                    template='dashboard/receipt/switch_input/multiple_visibility_switch.html',
                    id="mobile_app_permission_choice",
                ),
            ),
            KawnFieldSetWithHelpText(
                _("Dashboard Access"),
                KawnField(
                    'dashboard_permission',
                    template='dashboard/receipt/switch_input/multiple_visibility_switch.html',
                    id="dashboard_permission_choice",                
                ),
            ),
        )


class RoleSettingsForm(forms.ModelForm):

    class Meta:
        model = RoleSettings
        exclude = ("account",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["is_overrideable"].help_text = _("If activated, Supervisor Role can override staff POS action which lack permission.")
        self.fields["enforce_pin"].help_text = _("If activated, Will enforce users PIN on POS action.")
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.include_media = False

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField(
                    'is_overrideable',
                    template='dashboard/form_fields/switch.html'
                ),
                KawnField(
                    'enforce_pin',
                    template='dashboard/form_fields/switch.html'
                ),
            ),
        )
