from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from django.contrib.postgres.fields import ArrayField

from model_utils import Choices
from model_utils.models import TimeStampedModel
from safedelete.models import HARD_DELETE

from sterlingpos.core.models import (
    SterlingTenantModel, SterlingTenantForeignKey,
    SterlingTenantOneToOneField,
)


MOBILE_APP_PERMISSION = Choices(
    ('can_access_application', _('Can Access Application')),

    ('can_access_transaction_report', _('Can Access Mobile Transaction Report')),
    ('can_access_product_report', _('Can Access Mobile Product Report')),

    ('can_create_transaction', _('Can Create Transaction')),
    ('can_cancel_printed_item', _('Cancel Printed Item')),
    ('can_delete_printed_item', _('Delete Printed Item')),
    ('can_edit_printed_item', _('Edit printed Item')),

    ('can_print_closed_bill', _('Print Closed Bill')),
    ('can_delete_seat', _('Delete Seat')),
    ('can_delete_discount_loyalty', _('Delete Loyalty Discount')),

    ('cannot_create_and_update_item_cart', _('Disable Create and Update Item Cart')),
    ('cannot_edit_unprinted_item', _('Disable Create and Update Unprinted Item Cart')),

    ('can_delete_cart', _('Delete cart')),
    ('can_void_transaction', _('Void transaction')),
    ('can_disable_product', _('Disable product')),
    ('can_open_drawer', _('Open Drawer')),

    ('can_edit_global_discount', _('Apply Global Discount')),
)

DASHBOARD_PERMISSION = Choices(
    ('can_access_dashboard', _('View Dashboard')),
    ('can_access_catalogue', _('Manage Catalogue')),
    ('can_access_promo', _('Manage Promo')),
    ('can_access_device', _('Manage Staff')),
    ('can_access_application', _('Manage Application')),
    ('can_access_sales', _('View Sales Report')),
    ('can_access_sales_settings', _('Manage Sales Settings')),
    ('can_access_customer', _('View Customers')),

    ('can_access_cash_balance', _('Manage Cash Balance')),
    ('can_access_outlet', _('Manage Outlet')),
    ('can_access_account', _('Manage Account')),
    ('can_access_receipt', _('Manage Receipt')),
    ('can_access_surcharges', _('Manage Surcharge')),
    ('can_access_role', _('Manage Role')),
    ('can_access_stock', _('Manage Stock')),
    ('can_access_stock_transfer', _("Manage Stock Transfer")),
    ('can_access_stock_count', _("Manage Stock Count")),
    ('can_access_stock_adjustment', _("Manage Stock Adjustment")),
    ('can_access_purchase_order', _("Manage Purchase Order")),
    ('can_access_received_order', _("Manage Received Order")),
    ('can_access_vendor_bill', _("Manage Vendor Bill")),
    ('can_access_uom', _("Manage Units of Measure")),
    ('can_access_productions', _("Manage Manufactured")),
    ('can_access_supplier', _("Manage Supplier")),
    ('can_access_inventory_setting', _("Inventory Settings")),
    ('can_access_notification_setting', _("Notification Settings")),

)


class Role(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_("Role Name"), blank=True, max_length=255)

    is_supervisor = models.BooleanField(default=False)

    is_protected = models.BooleanField(default=False)

    mobile_app_permission = ArrayField(
        models.CharField(
            max_length=255,
            choices=MOBILE_APP_PERMISSION,
        ),
        blank=True,
        null=True,
    )

    dashboard_permission = ArrayField(
        models.CharField(
            max_length=255,
            choices=DASHBOARD_PERMISSION,
        ),
        blank=True,
        null=True,
    )

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        unique_together = (('account', 'id'),)


class RoleSettings(SterlingTenantModel, TimeStampedModel):
    is_overrideable = models.BooleanField(default=False)
    enforce_pin = models.BooleanField(default=False)

    def __str__(self):
        return "Role Settings for {}".format(self.account.name)

    class Meta:
        unique_together = (('account', 'id'),)
