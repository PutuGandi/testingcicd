from django.db.models import Prefetch
from django.contrib import admin

from .models import (
    SubscriptionPlan,
    Subscription,
    OtherSubscription,
    DeviceUserLiteSubscription,
    Billing,
    OrderPayment,
    Voucher,
    Event
)
from .forms import SubscriptionPlanForm


class SubscriptionPlanAdmin(admin.ModelAdmin):
    form = SubscriptionPlanForm
    prepopulated_fields = {"slug": ("name",)}

    list_display = (
        'name', 'price', 'trial_unit',
        'trial_period', 'recurrence_unit',
        'recurrence_period', 'is_public'
    )
    list_filter = ('is_public', )
    search_fields = ('name', 'price')


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = (
        'outlet', 'subscription_plan',
        'expires', 'active', 'is_trial', 'is_free',
        'account',
    )
    list_filter = (
        'expires', 'active', 'is_free',
        'subscription_plan', 'is_trial')
    search_fields = ('subscription_plan__name', 'account__name')
    list_select_related = ("account", "subscription_plan", "outlet", "outlet__account")
    raw_id_fields = ("account", "outlet", "subscription_plan")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        list_select_related = self.get_list_select_related(request)
        queryset = queryset.select_related(*list_select_related).select_related("voucher")
        return queryset


class DeviceUserLiteSubscriptionAdmin(admin.ModelAdmin):
    list_display = (
        'account', 'device_user', 'subscription_plan',
        'expires', 'active', 'is_trial', 'is_free'
    )
    list_filter = (
        'expires', 'active',  'is_free', 
        'subscription_plan', 'is_trial')
    search_fields = ('subscription_plan__name', 'account__name')
    list_select_related = ("account", "subscription_plan", "device_user", "device_user__account")
    raw_id_fields = ("account", "device_user", "subscription_plan")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        list_select_related = self.get_list_select_related(request)
        queryset = queryset.select_related(*list_select_related).select_related("voucher")
        return queryset


class OtherSubscriptionAdmin(admin.ModelAdmin):
    list_display = (
        'account', 'subscription_plan',
        'expires', 'active', 'is_trial', 'is_free'
    )
    list_filter = (
        'expires', 'active',  'is_free', 
        'subscription_plan', 'is_trial')
    search_fields = ('subscription_plan__name', 'account__name')
    list_select_related = ("account", "subscription_plan",)
    raw_id_fields = ("account", "subscription_plan")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        list_select_related = self.get_list_select_related(request)
        queryset = queryset.select_related(*list_select_related).select_related("voucher")
        return queryset


class OrderPaymentInline(admin.TabularInline):
    model = OrderPayment


class BillingAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'price', 'due_date', 'status', 'created', 'account'
    )
    list_filter = ('status', 'created', 'due_date')
    search_fields = ('id', 'price', 'account__name')
    inlines = (
        OrderPaymentInline,
    )
    list_select_related = ("account",)
    raw_id_fields = ("account", "subscriptions",)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        list_select_related = self.get_list_select_related(request)
        queryset = queryset.select_related(
            *list_select_related
        ).prefetch_related(
            Prefetch(
                "subscriptions",
                queryset=Subscription.objects.all()
            ),
            "subscription_detail",
            "orderpayment_set",
        )
        return queryset


class VoucherAdmin(admin.ModelAdmin):
    list_display = (
        'voucher_type', 'voucher_code', 'value', 'expires', 'event', 'created'
    )
    list_filter = ('voucher_type', 'created')
    search_fields = ('value', 'voucher_type', 'event__name')


class EventAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'event_date', 'created'
    )

    list_filter = ('created', 'event_date')
    search_fields = ('name',)


admin.site.register(SubscriptionPlan, SubscriptionPlanAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(DeviceUserLiteSubscription, DeviceUserLiteSubscriptionAdmin)
admin.site.register(OtherSubscription, OtherSubscriptionAdmin)
admin.site.register(Billing, BillingAdmin)
admin.site.register(Voucher, VoucherAdmin)
admin.site.register(Event, EventAdmin)
