import datetime

from django.test import TestCase
from django.db import IntegrityError
from django.utils.translation import ugettext as _

from dateutil.relativedelta import relativedelta
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.accounts.models import Account
from sterlingpos.users.models import User
from ..models import (
    SubscriptionPlan, Subscription,
    Billing, Outlet, OrderPayment,

    GooglePlaySubscriptionLog,
    DeviceUserLiteSubscription,
    OutletLiteSubscription,

    SubscriptionJournal,
)
from ..utils import extend_by_date


class TestSubscriptionPlan(TestCase):

    def test_plan__str__(self):
        self.plan = SubscriptionPlan.objects.create(
            name="Add Outlet",
            description="Add new outlet for your brand",
            price=2000000.00,
            trial_period=7,
            trial_unit='D',
            recurrence_period=1,
            recurrence_unit='M'
        )

        self.assertEqual(
            self.plan.__str__(),
            self.plan.name
        )

    def test_plan_get_pricing_display(self):
        """
            Test Subscription Plan Pricing
        """
        self.plan = SubscriptionPlan.objects.create(
            name="Add Outlet",
            description="Add new outlet for your brand",
            price=2000000.00,
            trial_period=7,
            trial_unit='D',
            recurrence_period=1,
            recurrence_unit='M'
        )
        self.assertEqual(
            self.plan.get_pricing_display(),
            '%(price).02f / %(unit)s' % {
                'price': self.plan.price,
                'unit': self.plan.get_recurrence_unit_display(),
            }
        )

    def test_plan_one_time_fee_pricing_display(self):
        """
            Test Subscription Plan One Time Fee Pricing
        """
        self.one_time_fee_plan = SubscriptionPlan.objects.create(
            name="Add Outlet",
            description="Add new outlet for your brand",
            price=2000000.00,
            trial_period=7,
            trial_unit='D',
            recurrence_period=0
        )

        self.assertEqual(
            self.one_time_fee_plan.get_pricing_display(),
            '%(price).02f one-time fee' % {'price': self.one_time_fee_plan.price}
        )

    def test_get_trial_display(self):
        """
            Test Subscription Plan Trial Display
        """
        self.plan = SubscriptionPlan.objects.create(
            name="Add Outlet",
            description="Add new outlet for your brand",
            price=2000000.00,
            trial_period=7,
            trial_unit='D',
            recurrence_period=1,
            recurrence_unit='M'
        )
        self.assertEqual(
            self.plan.get_trial_display(),
            '%(period)d %(unit_plural)s' % {
                'unit_plural': _(self.plan._PLURAL_UNITS[self.plan.trial_unit],),
                'period': self.plan.trial_period
            }
        )

    def test_no_trial_display(self):
        """
            Test Subscription Plan with no trial
        """
        self.no_trial_plan = SubscriptionPlan.objects.create(
            name="No Trial",
            description="No Trial Plan",
            price=200000.00,
            trial_period=0,
            trial_unit='0',
            recurrence_period=1,
            recurrence_unit='M'
        )

        self.assertEqual(
            self.no_trial_plan.get_trial_display(),
            _('No trial')
        )


class TestSubscription(TestCase):

    def setUp(self):
        self.account = mommy.make(Account, name='Test Account')
        set_current_tenant(self.account)
        self.user = mommy.make(User, email="testuser@example.com", password="example", account=self.account)
        self.outlet = mommy.make(Outlet, branch_id='branch-01', account=self.account)
        self.plan = mommy.make(SubscriptionPlan)

    def tearDown(self):
        set_current_tenant(None)

    def test_subscription__str__(self):
        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan,
            expires=datetime.date.today() + datetime.timedelta(days=5)
        )

        self.assertEqual(
            self.subscription.__str__(),
            "{} - {} - {}".format(self.account.name, self.outlet.name, self.plan.name)
        )

    def test_subscription_expired__str__(self):
        self.outlet_exp = mommy.make(
            Outlet,
            account=self.account
        )
        self.plan = mommy.make(SubscriptionPlan)
        self.subscription_exp = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet_exp,
            subscription_plan=self.plan,
            expires=datetime.datetime.strptime('2018-04-05', "%Y-%m-%d").date()
        )

        self.assertEqual(
            self.subscription_exp.__str__(),
            "{} - {} - {} ({})".format(
                self.account.name,
                self.outlet_exp.name,
                self.plan.name,
                'expired'
            )
        )

    def test_subscription_correctness(self):
        """
            Test Subscription Correctness, using many subscription plans
            and check outlet with correct subscription.
        """
        self.plan_1 = mommy.make(SubscriptionPlan)
        self.plan_2 = mommy.make(SubscriptionPlan)
        self.plan_3 = mommy.make(SubscriptionPlan)

        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan_2,
            expires=datetime.date.today() + datetime.timedelta(days=5)
        )

        self.assertEqual(
            self.subscription.__str__(),
            "{} - {} - {}".format(self.account.name, self.outlet.name, self.plan_2.name)
        )

    def test_subscribe_with_same_outlet_plan(self):
        """
            Test Subscription with same outlet and same subscription plan,
            it should be raise Integrity Error.
        """
        self.outlet = mommy.make(
            Outlet, branch_id='branch-1', account=self.account)
        self.plan = mommy.make(SubscriptionPlan)
        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan
        )

        with self.assertRaises(IntegrityError):
            self.new_subscription = Subscription.objects.create(
                account=self.account,
                outlet=self.outlet,
                subscription_plan=self.plan
            )

    def test_subscribe_with_new_outlet(self):
        """
            Test Subscribe with new outlet.
        """
        self.new_outlet = mommy.make(
            Outlet,
            account=self.account
        )
        self.plan = mommy.make(SubscriptionPlan)
        self.new_subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.new_outlet,
            subscription_plan=self.plan,
            expires=datetime.date.today() + datetime.timedelta(days=5)
        )

        self.assertEqual(
            self.new_subscription.__str__(),
            "{} - {} - {}".format(self.account.name, self.new_outlet.name, self.plan.name)
        )

    def test_subscription_not_expired(self):
        """
            Test Subscription Expired Date.
            Result False expires date + grace timedelta greater than today.
        """
        self.outlet = mommy.make(Outlet, account=self.account)
        self.plan = mommy.make(SubscriptionPlan)
        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan,
            expires=datetime.date.today()
        )

        self.assertFalse(self.subscription.expired)

    def test_subscription_expired(self):
        """
            Test Subscription Expired Date.
            Result True expires date + grace timedelta less than today.
        """
        self.outlet_exp = mommy.make(
            Outlet,
            account=self.account
        )
        self.plan = mommy.make(SubscriptionPlan)
        self.subscription_exp = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet_exp,
            subscription_plan=self.plan,
            expires=datetime.datetime.strptime('2018-04-05', "%Y-%m-%d").date()
        )

        self.assertTrue(self.subscription_exp.expired)

    def test_subscription_extend_by_subscription_plan(self):
        """
            Test Subscription Extend based on Subscription Plan.
        """
        self.outlet = mommy.make(Outlet, account=self.account)
        self.plan = SubscriptionPlan.objects.create(
            name="Add Outlet",
            description="Add new outlet for your brand",
            price=2000000.00,
            trial_period=7,
            trial_unit='D',
            recurrence_period=1,
            recurrence_unit='M'
        )
        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan
        )

        self.assertTrue(self.subscription.expired)
        self.subscription.extend()
        self.assertEqual(
            self.subscription.expires,
            extend_by_date(
                datetime.date.today(),
                self.subscription.subscription_plan.recurrence_period,
                self.subscription.subscription_plan.recurrence_unit
            )
        )

    def test_subscription_extend_expired_subscription_by_subscription_plan(self):
        """
            Test Subscription Extend an expired subscription based on Subscription Plan.
        """
        self.outlet = mommy.make(Outlet, account=self.account)
        self.plan = SubscriptionPlan.objects.create(
            name="Add Outlet",
            description="Add new outlet for your brand",
            price=2000000.00,
            trial_period=7,
            trial_unit='D',
            recurrence_period=1,
            recurrence_unit='M'
        )
        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan,
            expires=datetime.date.today() - datetime.timedelta(days=60)
        )

        self.assertTrue(self.subscription.expired)

        self.before_extend = self.subscription.expires
        self.subscription.extend()
        self.assertEqual(
            self.subscription.expires,
            extend_by_date(
                datetime.date.today(),
                self.subscription.subscription_plan.recurrence_period,
                self.subscription.subscription_plan.recurrence_unit
            )
        )

    def test_subscription_extend_by_timedelta(self):
        """
            Test Subscription Extend using time delta.
        """
        self.outlet = mommy.make(Outlet, account=self.account)
        self.plan = mommy.make(SubscriptionPlan)
        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan
        )
        self.assertTrue(self.subscription.expired)

        self.time_delta = 5
        self.before_extend = self.subscription.expires
        self.subscription.extend(self.time_delta)
        self.assertEqual(
            self.subscription.expires,
            datetime.date.today() + datetime.timedelta(self.time_delta)
        )

    def test_subscription_subscription_price(self):
        """
            Test Subscription subscription_price property.
        """
        subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan
        )

        self.assertEqual(
            subscription.subscription_price,
            subscription.subscription_plan.price
        )

    def test_first_mobile_lite_subscription_is_free(self):
        outlet = mommy.make(
            Outlet,
            branch_id='BRANCH-1',
            account=self.account
        )
        plan = mommy.make(
            SubscriptionPlan,
            slug="mobile-lite")
        subscription = Subscription.objects.create(
            account=self.account,
            outlet=outlet,
            subscription_plan=plan,
            expires=datetime.datetime.strptime('2018-04-05', "%Y-%m-%d").date()
        )
        self.assertFalse(subscription.expired)

        outlet_2 = mommy.make(
            Outlet,
            branch_id='BRANCH-2',
            account=self.account
        )
        subscription_2 = Subscription.objects.create(
            account=self.account,
            outlet=outlet_2,
            subscription_plan=plan,
            expires=datetime.datetime.strptime('2018-04-05', "%Y-%m-%d").date()
        )
        self.assertTrue(subscription_2.expired)

    def test_subscription_expiracy_with_one_time_subscription(self):
        """
            Test Subscription Expiracy when using one time subscription plan.
        """
        self.outlet = mommy.make(
            Outlet,
            account=self.account
        )
        self.plan = mommy.make(
            SubscriptionPlan,
            slug="one-time")

        self.subscription = Subscription.objects.create(
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan,
        )
        self.subscription.expires = None
        self.subscription.save()

        self.assertFalse(self.subscription.expired)


class TestBilling(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.user = mommy.make(User, email="testuser@example.com", password="example", account=self.account)
        self.outlet = mommy.make(Outlet, account=self.account)
        self.plan = mommy.make(
            SubscriptionPlan,
            recurrence_period=1,
            recurrence_unit='M')
        self.subscription = mommy.make(
            Subscription,
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan,
            expires=datetime.date.today(),
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_billing__str__(self):
        self.billing = Billing.objects.create(
            account=self.account,
        )
        self.billing.subscriptions.add(self.subscription)
        self.assertEqual(
            self.billing.__str__(),
            "{} - {} - {}".format(self.billing.created, self.account.name, self.billing.price)
        )

    def test_is_open_property(self):
        billing = Billing.objects.create(
            account=self.account,
        )
        billing.subscriptions.add(self.subscription)

        self.assertTrue(billing.is_open)

        order_payment = OrderPayment.objects.create(
            account=self.account,
            code='CODE01',
            billing=billing,
            amount=billing.price,
            status='paid'
        )
        self.assertFalse(billing.is_open)

    def test_billing_changing_billing_status_to_settld_will_extend_subscription(self):
        billing = Billing.objects.create(
            account=self.account,
        )
        billing.subscriptions.add(self.subscription)
        SubscriptionJournal.objects.create(
            billing=billing,
            subscription=self.subscription,
            voucher=self.subscription.voucher,
            current_plan=self.subscription.subscription_plan,
            choosen_plan=self.plan,
        )

        self.assertEqual(billing.status, Billing.STATUS.unpaid)
        subscription_expiracy = self.subscription.expires
        billing.status = Billing.STATUS.settled
        billing.save()
        updated_subscription = billing.subscriptions.first()
        self.assertNotEqual(updated_subscription.expires, subscription_expiracy)
        self.assertEqual(
            relativedelta(updated_subscription.expires, subscription_expiracy).months, 1)


class TestPaymentAttempt(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.user = mommy.make(User, email="testuser@example.com", password="example", account=self.account)
        self.outlet = mommy.make(Outlet, account=self.account)
        self.plan = mommy.make(
            SubscriptionPlan,
            recurrence_period=1,
            recurrence_unit='M')
        self.subscription = mommy.make(
            Subscription,
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan,
            expires=datetime.date.today(),
        )

        self.billing = Billing.objects.create(
            account=self.account,
        )
        self.billing.subscriptions.add(self.subscription)
        SubscriptionJournal.objects.create(
            billing=self.billing,
            subscription=self.subscription,
            current_plan=self.subscription.subscription_plan,
            choosen_plan=self.plan,
            voucher=self.subscription.voucher,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_payment_attempt__str__(self):
        order_payment = OrderPayment.objects.create(
            account=self.account,
            code='CODE01',
            billing=self.billing,
            amount=self.billing.price,
        )

        self.assertEqual(
            order_payment.__str__(),
            "Payment Attempt {} | CODE01 (Others) - {}".format(
                order_payment.created,
                self.billing.price,
                order_payment.amount)
        )

    def test_changing_payment_attempt_status_to_settle_will_update_billing_status_and_also_extend_subscription(self):
        order_payment = OrderPayment.objects.create(
            account=self.account,
            code='CODE01',
            billing=self.billing,
            amount=self.billing.price,
        )

        self.assertEqual(order_payment.status, OrderPayment.STATUS.unpaid)
        subscription_expiracy = self.subscription.expires
        order_payment.status = OrderPayment.STATUS.settled
        order_payment.save()
        updated_subscription = order_payment.billing.subscriptions.first()
        self.assertNotEqual(updated_subscription.expires, subscription_expiracy)
        self.assertEqual(
            relativedelta(updated_subscription.expires, subscription_expiracy).months, 1)
