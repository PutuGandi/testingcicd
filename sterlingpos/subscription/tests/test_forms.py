import datetime
from django.test import TestCase
from model_mommy import mommy

from django.utils.translation import ugettext as _

from sterlingpos.accounts.models import Account

from ..forms import (
    SubscriptionForm,
)
from ..models import (
    SubscriptionPlan, Subscription,
    Billing, Outlet,
    _recurrence_unit_days,
)
from ..utils import extend_by_date


class TestSubscriptionForm(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)

        self.outlet = mommy.make(Outlet, account=self.account)
        self.plan = mommy.make(
            SubscriptionPlan,
            recurrence_period=3, recurrence_unit='M',
            slug='subscription-test',
            trial_period=1, trial_unit='M')
        self.form_class = SubscriptionForm
        self.expected_expiracy = (
            datetime.date.today() +
            datetime.timedelta(
                self.plan.trial_period * _recurrence_unit_days[self.plan.trial_unit]))

    def test_create_object(self):
        data = {
            'subscription_plan': self.plan.slug,
        }
        form_kwargs = {
            'outlet': self.outlet,
            'account': self.account,
        }

        form = self.form_class(data, **form_kwargs)
        self.assertTrue(form.is_valid(), form.errors)
        obj = form.save()
        self.assertFalse(obj.cancelled)
        self.assertFalse(obj.expired)
        self.assertEqual(obj.expires, self.expected_expiracy)

    def test_create_object_using_subscription_plan_pk(self):
        data = {
            'subscription_plan': self.plan.pk,
        }
        form_kwargs = {
            'outlet': self.outlet,
            'account': self.account,
        }

        form = self.form_class(data, **form_kwargs)
        self.assertTrue(form.is_valid(), form.errors)
        obj = form.save()
        self.assertFalse(obj.cancelled)
        self.assertFalse(obj.expired)
        self.assertEqual(obj.expires, self.expected_expiracy)


# class TestSubscriptionJournalForm(TestCase):
    
#     def setUp(self):
#         self.account = mommy.make(Account)
#         self.form_class = None

#         self.outlet = mommy.make(Outlet, account=self.account)
#         self.plan_1 = mommy.make(
#             SubscriptionPlan,
#             recurrence_period=1, recurrence_unit='M',
#             slug='subscription-plan-1',
#             trial_period=1, trial_unit='M')
#         self.plan_2 = mommy.make(
#             SubscriptionPlan,
#             recurrence_period=3, recurrence_unit='M',
#             slug='subscription-plan-2',
#             trial_period=1, trial_unit='M')
#         self.subscription = mommy.make(
#             Subscription,
#             account=self.account,
#             outlet=self.outlet,
#             subscription_plan=self.plan_1,
#             expires=datetime.date.today(),
#         )

#     def test_create_subscription_journal(self):
#         data = {
#             "subscription": self.subscription,
#             "current_plan": self.subscription.subscription_plan,
#             "choosen_plan": self.plan_2,
#         }
#         form = self.form_class(data)
#         self.assertTrue(form.is_valid(), form.errors)
#         subscription_journal = form.save(billing=billing)
