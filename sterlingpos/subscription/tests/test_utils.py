import datetime
import calendar
from django.test import TestCase

from sterlingpos.subscription.utils import extend_by_date


class TestUtils(TestCase):

    def setUp(self):
        self.date = datetime.datetime.strptime('2018-05-11', "%Y-%m-%d").date()
        self.amount = 4

    def test_extend_by_date_days(self):
        self.assertEqual(
            extend_by_date(self.date, self.amount, 'D'),
            self.date + datetime.timedelta(1 * self.amount)
        )

    def test_extend_by_date_weeks(self):
        self.assertEqual(
            extend_by_date(self.date, self.amount, 'W'),
            self.date + datetime.timedelta(7 * self.amount)
        )

    def test_extend_by_date_months(self):
        y, m, d = self.date.year, self.date.month, self.date.day
        m += self.amount
        y += int(m / 12)
        m %= 12
        self.assertEqual(
            extend_by_date(self.date, self.amount, 'M'),
            datetime.date(y, m, d)
        )
    """
        Test Utils if current month + amount month for extends more than 12.
    """
    def test_extend_by_date_months_more_than_12(self):
        self.other_date = datetime.datetime.strptime('2017-12-11', "%Y-%m-%d").date()
        y, m, d = self.other_date.year, self.other_date.month, self.other_date.day
        m += self.amount
        y += int(m / 12)
        m %= 12
        self.assertEqual(
            extend_by_date(self.other_date, self.amount, 'M'),
            datetime.date(y, m, d)
        )

    """
        Test Utils if current month + amount month for extends equals zero.
    """
    def test_extend_by_date_months_is_zero(self):
        self.other_date = datetime.datetime.strptime('2017-1-11', "%Y-%m-%d").date()
        self.other_amount = 11
        y, m, d = self.other_date.year, self.other_date.month, self.other_date.day
        m += self.other_amount
        y += int(m / 12)
        m %= 12
        m, y = 12, y-1
        self.assertEqual(
            extend_by_date(self.other_date, self.other_amount, 'M'),
            datetime.date(y, m, d)
        )

    """
        Test Utils if days in current month bigger than days in extends month,
        for example:
            current month = 2018-01-31
            extends 1 month --> 2018-02-28
            range days in february is less than range days in january.
    """
    def test_extend_by_date_months_days_more_than_rangemonth(self):
        self.other_date = datetime.datetime.strptime('2017-1-31', "%Y-%m-%d").date()
        self.other_amount = 1
        y, m, d = self.other_date.year, self.other_date.month, self.other_date.day
        m += self.other_amount
        y += int(m / 12)
        m %= 12
        r = calendar.monthrange(y, m)[1]
        d = r
        self.assertEqual(
            extend_by_date(self.other_date, self.other_amount, 'M'),
            datetime.date(y, m, d)
        )

    def test_extend_by_date_years(self):
        y, m, d = self.date.year, self.date.month, self.date.day
        self.assertEqual(
            extend_by_date(self.date, self.amount, 'Y'),
            datetime.date(y+self.amount, m, d)
        )

    """
        Test utils if unit is wrong.
    """
    def test_extend_by_date_wrong_unit(self):

        with self.assertRaises(ValueError):
            extend_by_date(self.date, self.amount, 'Q')