import os
import csv
import datetime

from django.core.management.base import BaseCommand, CommandError
from django.shortcuts import get_object_or_404

from ...models import Voucher, Event, SubscriptionPlan
from sterlingpos.users.models import User

class Command(BaseCommand):
    help = "Send email voucher code"
    args = "csvfile [csvfile ...]"

    def add_arguments(self, parser):
        parser.add_argument('csvfile')

        parser.add_argument(
            '--eventcode', dest='event_code', required=True,
            help="Event Code"
        )

        parser.add_argument(
            '--vouchertype', dest='voucher_type', required=True,
            help="Voucher Type (monetary or percentage)"
        )

        parser.add_argument(
            '--value', dest='value', required=True,
            help="Value of Discount"
        )

    def handle(self, *args, **options):
        event_code = options['event_code']
        value = options['value']
        voucher_type = options['voucher_type']

        subscriptions = SubscriptionPlan.objects.filter(is_public=True).exclude(recurrence_period=1, recurrence_unit='M')

        try:
            event = Event.objects.get(code_name=event_code)
        except Event.DoesNotExist:
            self.stderr.write("Event does not exist.")


        if voucher_type == Voucher.VOUCHER_TYPE.percentage or voucher_type == Voucher.VOUCHER_TYPE.monetary:
            with open(options['csvfile'], "rt", encoding='utf8') as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                for i, row in enumerate(reader):
                    if i > 0:
                        user = get_object_or_404(User, email=row[2])
                        
                        voucher = Voucher.objects.create(
                            voucher_type=voucher_type,
                            value=value,
                            account=user.account,
                            event=event,
                            expires=datetime.date.today() + datetime.timedelta(30)
                        )
                        
                        for subscription in subscriptions:
                            voucher.available_subscription.add(subscription)
        else:
            self.stderr.write("Voucher Type only provide monetary or percentage.")


