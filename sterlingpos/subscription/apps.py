from django.apps import AppConfig
from django.db.models.signals import post_save


class SubscriptionConfig(AppConfig):
    name = 'sterlingpos.subscription'
    verbose_name = "Subscription"

    def ready(self):
        pass