import datetime

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Submit

from .models import SubscriptionPlan, Subscription, _recurrence_unit_days
from sterlingpos.outlet.models import Outlet


class SubscriptionPlanForm(forms.ModelForm):

    class Meta:
        model = SubscriptionPlan
        fields = '__all__'

    def clean(self):
        cleaned_data = super(SubscriptionPlanForm, self).clean()

        error_dict = {}

        trial_period = self.cleaned_data.get('trial_period')
        trial_unit = self.cleaned_data.get('trial_unit')
        recurrence_unit = self.cleaned_data.get('recurrence_unit')
        recurrence_period = self.cleaned_data.get('recurrence_period')
        price = self.cleaned_data.get('price')

        if recurrence_unit and recurrence_period:
            if not price:
                error_dict['price'] = ValidationError("Price value must be greater than 0.")

        if recurrence_period:
            if not recurrence_unit:
                error_dict['recurrence_unit'] = ValidationError("Recurrence unit value is invalid.")

        if recurrence_unit:
            if not recurrence_period:
                error_dict['recurrence_period'] = ValidationError("Recurrence period value is invalid.")

        if trial_period:
            if not trial_unit:
                error_dict['trial_unit'] = ValidationError("Trial unit value is invalid.")

        if trial_unit:
            if not trial_period:
                error_dict['trial_period'] = ValidationError("Trial period value is invalid.")

        if error_dict:
            raise ValidationError(error_dict)

        return cleaned_data


class SubscriptionForm(forms.ModelForm):

    class Meta:
        model = Subscription
        fields = ('subscription_plan',)
        widgets = {
            'subscription_plan': forms.RadioSelect(),
        }

    def __init__(self, *args, **kwargs):
        self.outlet = kwargs.pop('outlet', None)
        self.account = kwargs.pop('account', None)
        super(SubscriptionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['subscription_plan'].label = _('Subscription Plan')
        self.fields['subscription_plan'].required = True
        self.fields['subscription_plan'].empty_label = None
        self.fields['subscription_plan'].queryset = self.fields['subscription_plan'].queryset.filter(
            plan_type="outlet_subscription")

        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field('subscription_plan', template='dashboard/radio_button/custom_radio_button.html'),
        )

        if self.data:
            try:
                int(self.data.get('subscription_plan'))
                self.fields['subscription_plan'].to_field_name = 'pk'
            except (TypeError, ValueError):
                self.fields['subscription_plan'].to_field_name = 'slug'

    def save(self, *args, **kwargs):
        kwargs['commit'] = False
        obj = super(SubscriptionForm, self).save(*args, **kwargs)
        obj.account = self.account

        if self.outlet:
            obj.outlet = self.outlet
            if obj.subscription_plan.trial_period:
                obj.expires = (
                datetime.date.today() +
                datetime.timedelta(
                    obj.subscription_plan.trial_period * _recurrence_unit_days[obj.subscription_plan.trial_unit]))
            else:
                obj.expires = (
                datetime.date.today() +
                datetime.timedelta(
                    obj.subscription_plan.recurrence_period * _recurrence_unit_days[obj.subscription_plan.recurrence_unit]))
        else:
            outlet = Outlet()
            outlet.save()
            obj.outlet = outlet
        obj.cancelled = False
        obj.save()
        return obj
