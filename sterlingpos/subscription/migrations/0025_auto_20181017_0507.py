# Generated by Django 2.0.4 on 2018-10-17 05:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_auto_20181012_0549'),
        ('subscription', '0024_auto_20181015_1102'),
    ]

    operations = [
        migrations.AddField(
            model_name='billing',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='orderpayment',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AddField(
            model_name='subscription',
            name='deleted',
            field=models.DateTimeField(editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='billing',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account'),
        ),
        migrations.AlterField(
            model_name='orderpayment',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account'),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account'),
        ),
        migrations.AlterUniqueTogether(
            name='subscription',
            unique_together={('account', 'id')},
        ),
    ]
