# Generated by Django 2.0.4 on 2019-03-18 06:03

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager
import django.utils.timezone
import model_utils.fields
import sterlingpos.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_auto_20181211_1035'),
        ('subscription', '0025_auto_20181017_0507'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubscriptionJournal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('voucher_type', models.CharField(blank=True, max_length=20, verbose_name='Voucher Type')),
                ('voucher_value', models.DecimalField(decimal_places=2, default=0, max_digits=64)),
                ('price', models.DecimalField(decimal_places=2, default=0, max_digits=64)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('billing', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subscription.Billing')),
                ('choosen_plan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='choosen_journal', to='subscription.SubscriptionPlan')),
                ('current_plan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subscription.SubscriptionPlan')),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.AlterModelManagers(
            name='subscription',
            managers=[
                ('active_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.AddField(
            model_name='subscriptionjournal',
            name='subscription',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subscription.Subscription'),
        ),
        migrations.AddField(
            model_name='subscriptionjournal',
            name='voucher',
            field=sterlingpos.core.models.SterlingTenantForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='subscription.Voucher'),
        ),
        migrations.AddField(
            model_name='billing',
            name='subscription_detail',
            field=models.ManyToManyField(related_name='billing_detail', through='subscription.SubscriptionJournal', to='subscription.Subscription'),
        ),
        migrations.AlterUniqueTogether(
            name='subscriptionjournal',
            unique_together={('account', 'id')},
        ),
    ]
