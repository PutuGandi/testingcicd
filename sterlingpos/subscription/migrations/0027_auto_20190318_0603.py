# Generated by Django 2.0.4 on 2019-03-18 06:03

from django.db import migrations

def populate_subscription_journey(apps, schema_editor):
    Billing = apps.get_model("subscription", "Billing")
    SubscriptionJournal = apps.get_model("subscription", "SubscriptionJournal")

    for billing in Billing.objects.all():
        for subscription in billing.subscriptions.all():
            kwargs = {
                "billing": billing,
                "subscription": subscription,
                "current_plan": subscription.subscription_plan,
                "choosen_plan": subscription.subscription_plan,
                "account": subscription.account,
            }
            if subscription.voucher:
                kwargs.update({
                    "voucher": subscription.voucher,
                    "voucher_type": subscription.voucher.voucher_type,
                    "voucher_value": subscription.voucher.value,
                })
            SubscriptionJournal.objects.create(**kwargs)


class Migration(migrations.Migration):

    dependencies = [
        ('subscription', '0026_auto_20190318_0603'),
    ]

    operations = [
        migrations.RunPython(
            populate_subscription_journey, migrations.RunPython.noop),
    ]
