import datetime
import uuid
from django.db import models

from django.db.models.functions import Coalesce
from django.urls import reverse

from django.utils.translation import ugettext as _, ungettext, ugettext_lazy
from model_utils.models import TimeStampedModel
from model_utils import Choices

from allauth.utils import build_absolute_uri

from sterlingpos.users.adapters import AccountAdapter
from sterlingpos.core.models import (
    SterlingTenantModel, 
    SterlingTenantForeignKey,
    SterlingTenantOneToOneField
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.subscription.utils import extend_by_date

_recurrence_unit_days = {
    'D': 1.,
    'W': 7.,
    'M': 30.4368,
    'Y': 365.2425,
}

_TIME_UNIT_CHOICES = (
    ('D', ugettext_lazy('Day')),
    ('W', ugettext_lazy('Week')),
    ('M', ugettext_lazy('Month')),
    ('Y', ugettext_lazy('Year')),
)

SUBSCRIPTION_GRACE_PERIOD = 2


def expiring_datetime():
    return datetime.datetime.today() - datetime.timedelta(
        days=SUBSCRIPTION_GRACE_PERIOD + 1)

def expiring_date():
    return datetime.date.today() - datetime.timedelta(
        days=SUBSCRIPTION_GRACE_PERIOD + 1)


class SubscriptionPlan(TimeStampedModel):

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=64, decimal_places=2)
    trial_period = models.PositiveIntegerField()
    trial_unit = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        choices=((None, ugettext_lazy("No trial")),) + _TIME_UNIT_CHOICES
    )

    recurrence_period = models.PositiveIntegerField()
    recurrence_unit = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        choices=((None, ugettext_lazy("No recurrence")),) + _TIME_UNIT_CHOICES
    )

    slug = models.SlugField(max_length=50, blank=True)
    is_public = models.BooleanField(default=True)

    TYPE = Choices(
        ('outlet_subscription', _('Outlet Subscription')),
        ('device_user_subscription', _('Device User Subscription')),
        ('others', _('Others')),
    )
    plan_type = models.CharField(
        _("Subscription Type"), max_length=100,
        choices=TYPE, default=TYPE.outlet_subscription, db_index=True)

    _PLURAL_UNITS = {
        '0': ugettext_lazy('No trial'),
        'D': 'days',
        'W': 'weeks',
        'M': 'months',
        'Y': 'years',
    }

    class Meta:
        ordering = ('price',)

    def __str__(self):
        return self.name

    def get_pricing_display(self):
        if self.recurrence_period:
            return ungettext(
                '%(price).02f / %(unit)s',
                '%(price).02f / %(period)d %(unit_plural)s',
                self.recurrence_period
            ) % {
                'price': self.price,
                'unit': self.get_recurrence_unit_display(),
                'unit_plural': _(self._PLURAL_UNITS[self.recurrence_unit],),
                'period': self.recurrence_period,
            }
        else:
            return _('%(price).02f one-time fee') % {'price': self.price}

    def get_trial_display(self):
        if self.trial_period:
            return ungettext(
                'One %(unit)s',
                '%(period)d %(unit_plural)s',
                self.trial_period
            ) % {
                'unit': self.get_trial_unit_display().lower(),
                'unit_plural': _(self._PLURAL_UNITS[self.trial_unit],),
                'period': self.trial_period,
            }
        else:
            return _("No trial")


class ActiveUSManager(models.Manager):

    def get_query_set(self):
        return super(ActiveUSManager, self).get_query_set().filter(active=True)


class BaseSubscription(SterlingTenantModel, TimeStampedModel):
    subscription_plan = models.ForeignKey(
        SubscriptionPlan, null=True, on_delete=models.SET_NULL)
    expires = models.DateField(null=True, default=expiring_date)
    billing_date = models.DateField(blank=True, null=True)

    active = models.BooleanField(default=True)
    cancelled = models.BooleanField(default=True)

    is_trial = models.BooleanField(default=False)
    is_free = models.BooleanField(default=False)

    active_objects = ActiveUSManager()

    grace_period = datetime.timedelta(SUBSCRIPTION_GRACE_PERIOD)
    voucher = SterlingTenantForeignKey(
        'subscription.Voucher', null=True, blank=True,
        on_delete=models.CASCADE)

    class Meta:
        abstract = True

    @property
    def expired(self):
        if self.is_free:
            return False
        return self.expires is not None and (
            self.expires + self.grace_period < datetime.date.today()
        )

    @classmethod
    def calculate_price(cls, subscription, subscription_plan, voucher=None):
        # TPDO : Calculate extra Subscription-AddOn Price
        price = subscription_plan.price
        if (voucher and
                voucher.voucher_type == Voucher.VOUCHER_TYPE.percentage):
            price -= (price * (voucher.value/100))
        elif (voucher and
                voucher.voucher_type == Voucher.VOUCHER_TYPE.monetary):
            price -= voucher.value
        return price

    @property
    def subscription_price(self):
        price = BaseSubscription.calculate_price(
            self, self.subscription_plan, self.voucher
        )
        return price

    def extend(self, timedelta=None):
        if not self.expired:
            current_date = self.expires
        else:
            current_date = datetime.date.today()

        if timedelta is not None:
            self.expires = current_date + datetime.timedelta(timedelta)
        else:
            if self.subscription_plan.recurrence_unit:
                self.expires = extend_by_date(
                    current_date,
                    self.subscription_plan.recurrence_period,
                    self.subscription_plan.recurrence_unit
                )
            else:
                self.expires = None

    def subscribe(self):
        self.is_trial = False

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False


class Subscription(BaseSubscription):
    outlet = models.OneToOneField(Outlet, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Subscription"
        verbose_name_plural = "Subscriptions"
        unique_together = (('account', 'id'),)

    def __str__(self):
        if self.expired:
            return "{} - {} - {} ({})".format(
                self.account.name,
                self.outlet.name,
                self.subscription_plan.name,
                'expired'
            )
        return "{} - {} - {}".format(
            self.account.name, self.outlet.name,
            self.subscription_plan.name)

    def save(self, *args, **kwargs):
        if (not self.pk and
                self.subscription_plan.slug == "mobile-lite"):
            if not Subscription.objects.filter(
                account=self.outlet.account,
                subscription_plan__slug="mobile-lite").exists():
                    self.is_free = True

        super(Subscription, self).save(*args, **kwargs)


class OtherSubscription(BaseSubscription):

    class Meta:
        verbose_name = "OtherSubscription"
        verbose_name_plural = "OtherSubscriptions"
        unique_together = (('account', 'id'),)

    def __str__(self):
        if self.expired:
            return "{} - {} ({})".format(
                self.account.name,
                self.subscription_plan.name,
                'expired'
            )
        return "{} - {}".format(
            self.account.name,
            self.subscription_plan.name)

    def save(self, *args, **kwargs):
        # TODO: Does all subscription plan under type `others` is one time only ?
        self.expires = None

        if not self.pk:
            self.active = False

        super(OtherSubscription, self).save(*args, **kwargs)


class SubscriptionJournal(SterlingTenantModel, TimeStampedModel):
    billing = models.ForeignKey(
        "subscription.Billing", on_delete=models.CASCADE)

    subscription = models.ForeignKey(
        "subscription.Subscription",
        null=True, blank=True,
        on_delete=models.CASCADE)
    device_user_subscription = SterlingTenantForeignKey(
        'subscription.DeviceUserLiteSubscription',
        null=True, blank=True,
        on_delete=models.CASCADE
    )
    other_subscription = SterlingTenantForeignKey(
        'subscription.OtherSubscription',
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    voucher = SterlingTenantForeignKey(
        'subscription.Voucher', null=True, blank=True,
        on_delete=models.CASCADE)
    voucher_type = models.CharField(
        _("Voucher Type"), blank=True, max_length=20)
    voucher_value = models.DecimalField(
        max_digits=64, decimal_places=2, default=0)

    current_plan = models.ForeignKey(
        "subscription.SubscriptionPlan", null=True, blank=True, 
        on_delete=models.CASCADE)
    choosen_plan = models.ForeignKey(
        "subscription.SubscriptionPlan",
        on_delete=models.CASCADE, related_name="choosen_journal")

    price = models.DecimalField(
        max_digits=64, decimal_places=2, default=0)

    class Meta:
        ordering = ('created',)
        unique_together = (('account', 'id'),)

    def get_subscription(self):
        if self.subscription:
            return self.subscription
        elif self.device_user_subscription:
            return self.device_user_subscription
        elif self.other_subscription:
            return self.other_subscription

    def get_current_plan(self):
        subscription = self.get_subscription()
        current_plan = subscription.subscription_plan
        return current_plan

    def save(self, *args, **kwargs):
        if not hasattr(self, "current_plan"):
            self.current_plan = self.get_current_plan()

        if not self.price:
            self.price = BaseSubscription.calculate_price(
                self.get_subscription(),
                self.choosen_plan,
                self.voucher,
            )
        super(SubscriptionJournal, self).save(*args, **kwargs)


class Billing(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ('canceled', _('dibatalkan')),
        ('unpaid', _('menunggu pembayaran')),
        ('paid', _('menunggu verifikasi')),
        ('settled', _('selesai')),
    )

    status = models.CharField(
        _("Billing Status"), max_length=15,
        choices=STATUS, default=STATUS.unpaid, db_index=True)
    price = models.DecimalField(max_digits=64, decimal_places=2, default=0)

    subscriptions = models.ManyToManyField(
        'subscription.Subscription', related_name='billing')
    subscription_detail = models.ManyToManyField(
        'subscription.Subscription', through='SubscriptionJournal',
        related_name='billing_detail')

    due_date = models.DateField(blank=True, null=True)

    class Meta:
        ordering = ('created',)
        unique_together = (('account', 'id'),)

    __billing_status = None

    def __init__(self, *args, **kwargs):
        super(Billing, self).__init__(*args, **kwargs)
        self.__billing_status = self.status

    def __str__(self):
        return "{} - {} - {}".format(
            self.created, self.account.name, self.price)

    @property
    def is_open(self):
        paid_payattempt = self.orderpayment_set.filter(
            status=OrderPayment.STATUS.paid).exists()
        return self.status == Billing.STATUS.unpaid and not paid_payattempt

    def generate_payment_attempt_code(self):
        code = "{}-{}{}{}{}{}".format(
            self.pk,
            datetime.datetime.today().day,
            datetime.datetime.today().month,
            datetime.datetime.today().year,
            datetime.datetime.today().hour,
            datetime.datetime.today().minute,
            datetime.datetime.today().second,
        )
        return code

    def update_status(self):
        accumulated_payment = self.orderpayment_set.filter(
            status=OrderPayment.STATUS.settled).aggregate(
                amount_sum=Coalesce(models.Sum('amount'), models.Value(0))
            ).get('amount_sum')
        if self.price >= accumulated_payment:
            self.status = Billing.STATUS.settled
            self.save()

    def notify_invoice(self, status=None):
        adapter = AccountAdapter()
        email = self.account.user_set.first().email

        if status == 'settlement':
            ctx = {
                "user": self.account.user_set.first(),
                "invoice": self,
            }
            email_template = 'invoice/email/invoice_settled'
        elif status == 'created':
            activate_url = build_absolute_uri(
                None,
                reverse('subscription:billing_list')
            )
            ctx = {
                "user": self.account.user_set.first(),
                "activate_url": activate_url,
                "invoice": self,
            }
            email_template = 'invoice/email/invoice_created'

        adapter.send_mail(email_template, email, ctx)

    def save(self, *args, **kwargs):
        notify_invoice = kwargs.pop("notify_invoice", False)
        recalculate = kwargs.pop('recalculate', False)

        if self.pk and recalculate:
            self.price = self.calculate_price()

        super(Billing, self).save(*args, **kwargs)

        if notify_invoice:
            self.notify_invoice(status='created')

        if self.__billing_status != self.status and self.status == Billing.STATUS.settled:
            self.__billing_status = self.status
            self._update_related_subscription()
            self.notify_invoice(status='settlement')

    def _update_related_subscription(self):            
        for sub_journal in self.subscriptionjournal_set.all():
            subscription = sub_journal.get_subscription()
            subscription.subscription_plan = sub_journal.choosen_plan
            subscription.extend()
            if subscription.is_trial:
                subscription.subscribe()
            if not subscription.active:
                subscription.activate()
            subscription.voucher = None
            subscription.save()

    def calculate_price(self):
        if not self.subscriptionjournal_set.exists():
            total_price = sum([
                subscription.subscription_price
                for subscription in self.subscriptions.all()
            ])
        else:
            total_price = sum([
                sub_journal.price
                for sub_journal in self.subscriptionjournal_set.all()
            ])
        return total_price


class OrderPayment(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ('expired', _('lewat jatuh tempo')),
        ('canceled', _('transaksi gagal')),
        ('unpaid', _('belum dibayar')),
        ('paid', _('menunggu verifikasi')),
        ('settled', _('selesai')),
    )

    METHOD = Choices(
        ('others', _('Others')),
        ('midtrans', _('Midtrans')),
    )

    PAYMENT_TYPE = Choices(
        ('bank_transfer', _('Bank Transfer')),
        ('gopay', _('Go Pay')),
        ('echannel', _('E-Channel')),
    )

    code = models.CharField(
        _("Order ID"), max_length=120, db_index=True)
    status = models.CharField(
        _("Order Status"), max_length=15,
        choices=STATUS, default=STATUS.unpaid, db_index=True)
    method = models.CharField(
        _("Order Method"), max_length=15,
        choices=METHOD, default=METHOD.others, db_index=True)

    payment_type = models.CharField(
        _("Payment Method"), max_length=15,
        choices=PAYMENT_TYPE,
        default=PAYMENT_TYPE.bank_transfer, db_index=True)
    va_number =models.CharField(
        _("Payment Method"),
        max_length=120, blank=True, null=True)

    bill_key =models.CharField(
        _("Mandiri Bill Key"),
        max_length=120, blank=True, null=True)

    bank_name =models.CharField(
        _("Bank Name"),
        max_length=120, blank=True, null=True)

    transaction_id =models.CharField(
        _("MIDTRANS Transaction ID"),
        max_length=120, blank=True, null=True)

    transaction_time = models.DateTimeField(
         _("Transaction Time"),
        blank=True, null=True, db_index=True)
    settlement_time = models.DateTimeField(
         _("Settlement Time"),
        blank=True, null=True, db_index=True)

    cancelation_time = models.DateTimeField(
         _("Cancelation Time"),
        blank=True, null=True, db_index=True)

    billing = models.ForeignKey(Billing, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=64, decimal_places=2, default=0)

    class Meta:
        ordering = ('created',)
        verbose_name = 'OrderPayment'
        unique_together = (('account', 'id'),)

    __payment_attempt_status = None

    def __init__(self, *args, **kwargs):
        super(OrderPayment, self).__init__(*args, **kwargs)
        self.__payment_attempt_status = self.status

    def __str__(self):
        return "Payment Attempt {} | {} ({}) - {}".format(
            self.created, self.code, self.get_method_display(),
            self.amount)

    def save(self, *args, **kwargs):
        super(OrderPayment, self).save(*args, **kwargs)

        if self.__payment_attempt_status != self.status and self.status == OrderPayment.STATUS.settled:
            self.__payment_attempt_status = self.status
            self.billing.update_status()


class Event(TimeStampedModel):
    name = models.CharField(_("Event Name"), max_length=255)
    code_name = models.SlugField(max_length=50, blank=True)
    event_date = models.DateField(null=True, default=datetime.date.today)
    event_email_copy = models.TextField()

    def __str__(self):
        return "Event {} | {}".format(self.name, self.event_date)


class Voucher(SterlingTenantModel, TimeStampedModel):
    VOUCHER_TYPE = Choices(
        ('monetary', _('Money based voucher')),
        ('percentage', _('Percentage discount')),
    )
    voucher_type = models.CharField(_("Voucher Type"), max_length=20, choices=VOUCHER_TYPE)
    voucher_code = models.CharField(_("Voucher Code"), max_length=20)
    expires = models.DateField(null=True, default=datetime.date.today)
    value = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    is_redeem = models.BooleanField(default=False)
    event = models.ForeignKey('subscription.Event', on_delete=models.CASCADE)
    available_subscription = models.ManyToManyField('subscription.SubscriptionPlan', related_name='voucher')

    class Meta:
        ordering = ('created',)
        verbose_name = _("Voucher")
        verbose_name_plural = _("Vouchers")

    @property
    def expired(self):
        if (self.outletlitesubscription and
                self.outletlitesubscription.is_free):
            return False
        return self.expires is not None and (
            self.expires < datetime.date.today()
        )

    def save(self, *args, **kwargs):
        is_created = False
        if not self.voucher_code:
            self.voucher_code = Voucher.generate_voucher_code()
            is_created = True
        super(Voucher, self).save(*args, **kwargs)

        if is_created:
            self.notify_voucher()

    def notify_voucher(self):
        adapter = AccountAdapter()
        email = self.account.user_set.first().email

        activate_url = build_absolute_uri(
            None,
            reverse('subscription:billing')
        )

        discount = None
        if self.voucher_type == Voucher.VOUCHER_TYPE.percentage:
            discount = "{} {}".format(self.value, '%')
        else:
            discount = "{} {}".format('Rp.', self.value)

        ctx = {
            "user": self.account.user_set.first(),
            "activate_url": activate_url,
            "voucher_discount": discount,
            "voucher_code": self.voucher_code,
            "event_email_copy": self.event.event_email_copy
        }
        email_template = 'voucher/email/voucher_created'
        adapter.send_mail(email_template, email, ctx)

    def redeem(self):
        self.is_redeem = True
        self.save()

    def cancel(self):
        self.is_redeem = False
        self.save()

    @classmethod
    def generate_voucher_code(self):
        voucher_code = uuid.uuid4().hex[:8].upper()
        return voucher_code


class OutletLiteSubscription(SterlingTenantModel, TimeStampedModel):
    subscription = SterlingTenantOneToOneField(
        "subscription.Subscription", on_delete=models.CASCADE)
    expiracy = models.DateTimeField(default=expiring_datetime)
    is_free = models.BooleanField(default=False)

    class Meta:
        ordering = ('created',)
        verbose_name = _("OutletLiteSubscription")
        verbose_name_plural = _("OutletLiteSubscriptions")

    def save(self, *args, **kwargs):
        super(OutletLiteSubscription, self).save(*args, **kwargs)
        self.subscription.expires = self.expiracy.date()
        self.subscription.save()


class DeviceUserLiteSubscription(BaseSubscription):
    device_user = SterlingTenantOneToOneField(
        "device.DeviceUser", on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)
        verbose_name = _("DeviceUserLiteSubscription")
        verbose_name_plural = _("DeviceUserLiteSubscriptions")

    def __str__(self):
        if self.subscription_plan:
            subscription_plan_name = self.subscription_plan.name
        else:
            subscription_plan_name = None

        if self.expired:
            return "{} - {} - {} ({})".format(
                self.account.name,
                self.device_user.name,
                subscription_plan_name,
                'expired'
            )
        return "{} - {} - {}".format(
            self.account.name, self.device_user.name,
            subscription_plan_name,
            )


class GooglePlaySubscriptionLog(SterlingTenantModel, TimeStampedModel):
    SUBSCRIPTION_TYPE = Choices(
        (
            'outlet_subscription',
            _('Additional Mobile-lite Outlet Subscription')),
        (
            'device_user_subscription',
            _('Additional Mobile-lite Device User Subscription')),
        (
            'split_bill_subscription',
            _('Additional Mobile-lite Split Bill Subscription')),
        (
            'table_management_subscription',
            _('Additional Mobile-lite Table Management Subscription')),
    )
    subscription_type = models.CharField(_("Subscription Type"), max_length=50, choices=SUBSCRIPTION_TYPE)

    payload_code = models.CharField(_("Payload Code"), max_length=255)

    order_id = models.CharField(_("Order ID"), max_length=255, blank=True, null=True)
    product_id = models.CharField(_("Product ID"), max_length=255, blank=True, null=True)
    purchase_token = models.CharField(_("Purchase Token"), max_length=255, blank=True, null=True)

    outlet_lite_subscription = SterlingTenantForeignKey(
        "subscription.OutletLiteSubscription",
        null=True, blank=True, on_delete=models.CASCADE)
    device_user_lite_subscription = SterlingTenantForeignKey(
        "subscription.DeviceUserLiteSubscription",
        null=True, blank=True, on_delete=models.CASCADE)
    expiracy = models.DateTimeField(blank=True, null=True)
    purchase_time = models.DateTimeField()

    class Meta:
        ordering = ('created',)
        verbose_name = _("GooglePlaySubscriptionLog")
        verbose_name_plural = _("GooglePlaySubscriptionLogs")

    def save(self, *args, **kwargs):
        super(GooglePlaySubscriptionLog, self).save(*args, **kwargs)

        if self.outlet_lite_subscription:
            self.outlet_lite_subscription.expiracy = self.expiracy
            self.outlet_lite_subscription.save()
        elif self.device_user_lite_subscription:
            self.device_user_lite_subscription.expiracy = self.expiracy
            self.device_user_lite_subscription.save()

        return self
