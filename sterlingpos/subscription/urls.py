from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = 'subscription'
urlpatterns = [
    url(regex=r'^order-list/$', view=views.BillingListView.as_view(),
        name='billing_list'),
    
    url(regex=r'^billing/$', view=views.CreateInvoicePaymentView.as_view(),
        name='billing'),
    url(regex=r'^billing/(?P<pk>[0-9]+)/$', view=views.InvoicePaymentView.as_view(),
        name='complete_payment'),
    url(regex=r'^billing/(?P<pk>[0-9]+)/cancel/$', view=views.CancelPaymentView.as_view(),
        name='cancel_payment'),
    url(regex=r'^billing/voucher-redeem/$', view=views.VoucherRedeemView.as_view(),
        name='voucher_redeem'),
    url(regex=r'^billing/voucher-cancel/$', view=views.VoucherCancelView.as_view(),
        name='voucher_cancel'),

    url(regex=r'^data/billing/$', view=views.BillingListJson.as_view(),
        name='data_billing_list'),
    url(regex=r'^data/order-list/$', view=views.OrderPaymentList.as_view(),
        name='data_order_list'),
]
