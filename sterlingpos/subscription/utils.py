import datetime
import calendar


def extend_by_date(date, amount, unit):
    if unit == 'D':
        return date + datetime.timedelta(1 * amount)
    elif unit == 'W':
        return date + datetime.timedelta(7 * amount)
    elif unit == 'M':
        year, month, day = date.year, date.month, date.day
        month += amount
        year += int(month / 12)
        month %= 12
        if not month:
            month, year = 12, year - 1
        range_days = calendar.monthrange(year, month)[1]
        if day > range_days:
            day = range_days
        return datetime.date(year, month, day)
    elif unit == 'Y':
        year, month, day = date.year, date.month, date.day
        return datetime.date(year + amount, month, day)
    else:
        raise ValueError("Unknown unit.")
