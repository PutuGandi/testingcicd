import datetime
# from datetime import datetime

from django.views import generic
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _, ugettext
from django.db.models import Q, Sum
from django.contrib.humanize.templatetags.humanize import intcomma

from braces.views import JSONResponseMixin, AjaxResponseMixin
from django_datatables_view.base_datatable_view import BaseDatatableView

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.core.midtrans.adapter import midtrans_adapter
from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.role.mixins import SterlingRoleMixin

from .models import (
    SubscriptionPlan, Billing, OrderPayment, Voucher, Subscription,
    SubscriptionJournal
)

class BillingListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):

    columns = ['id', 'status', 'price', 'due_date', 'action']
    order_columns = ['id', 'status', 'price', 'due_date', 'actions']
    datetime_col = ['due_date', ]

    model = Billing

    def get_initial_queryset(self):
        qs = super(BillingListJson, self).get_initial_queryset()
        try:
            start_date = datetime.datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = None

        try:
            end_date = datetime.datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = None

        if start_date:
            qs = qs.filter(due_date__gte=start_date)
        if end_date:
            qs = qs.filter(due_date__lte=end_date)

        return qs.order_by('-due_date')

    def render_column(self, row, column):
        if column == "action":
            action_data = {
                'id': row.id,
                'edit': reverse_lazy("subscription:complete_payment", kwargs={'pk': row.pk}),
            }
            return action_data
        elif column == "id":
            subscription_journals = row.subscriptionjournal_set.all()
            res = ""
            for i, subscription_journal in enumerate(subscription_journals):
                if i < 2:
                    if subscription_journal.subscription:
                        res += "{}, ".format(
                            subscription_journal.subscription.outlet.name)
                    elif subscription_journal.device_user_subscription:
                        res += "User Device Subscription - {}, ".format(
                            subscription_journal.device_user_subscription.device_user.name)
                    elif subscription_journal.other_subscription:
                        res += "Subscription - {}, ".format(
                            subscription_journal.other_subscription.subscription_plan.name)
                elif i == 2:
                    res += "..."
            return res
        elif column == 'price':
            return intcomma(int(row.price))
        return super(BillingListJson, self).render_column(row, column)


class BillingListView(SterlingRoleMixin, generic.ListView):
    template_name = "dashboard/subscription/invoice_list.html"
    model = Billing


class OrderPaymentList(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['code', 'billing.price']
    order_columns = ['code', 'billing.price']
    datetime_col = ['billing.due_date', ]
    model = OrderPayment

    def get_initial_queryset(self):
        qs = super(OrderPaymentList, self).get_initial_queryset()
        qs = qs.filter(status=OrderPayment.STATUS.settled)
        try:
            start_date = datetime.datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = None

        try:
            end_date = datetime.datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = None

        if start_date:
            qs = qs.filter(due_date__gte=start_date)
        if end_date:
            qs = qs.filter(due_date__lte=end_date)

        return qs.order_by('-due_date')

    def render_column(self, row, column):
        if column == "billing.price":
            return intcomma(int(row.billing.price))
        return super(OrderPaymentList, self).render_column(row, column)


class CreateInvoicePaymentView(SterlingRoleMixin, generic.CreateView):
    model = Billing
    fields = ['price', 'subscriptions']
    template_name = "dashboard/subscription/billing.html"

    def get_context_data(self, **kwargs):
        context = super(
            CreateInvoicePaymentView, self).get_context_data(**kwargs)
        subscriptions = Subscription.objects.filter(
            Q(active=False)
            | Q(is_trial=True)
            | Q(expires__lt=datetime.date.today())
        ).select_related('subscription_plan', 'outlet')

        paid_subscriptions = Subscription.objects.filter(
            active=True, is_trial=False,
            expires__gte=datetime.date.today()
        ).select_related('subscription_plan', 'outlet')

        order_payment = OrderPayment.objects.all()
        subscription_plan = SubscriptionPlan.objects.filter(
            plan_type=SubscriptionPlan.TYPE.outlet_subscription,
            is_public=True)
        get_voucher = False
        voucher_type = 'percentage'
        voucher_value = 0
        voucher_code = ''
        for subscription in subscriptions:
            if not (
                subscription.subscription_plan.recurrence_period == 1 and
                    subscription.subscription_plan.recurrence_unit == 'M'):
                if subscription.voucher:
                    get_voucher = True
                    voucher_type = subscription.voucher.voucher_type
                    voucher_value = subscription.voucher.value
                    voucher_code = subscription.voucher.voucher_code

        context.update({
            'subscriptions': subscriptions,
            'paid_subscriptions': paid_subscriptions,
            'subscription_plan': subscription_plan,
            'get_voucher': get_voucher,
            'voucher_type': voucher_type,
            'voucher_value': voucher_value,
            'voucher_code': voucher_code,
            'order_payment': order_payment
        })
        return context

    def form_valid(self, form):
        billing = form.save(commit=False)
        billing.save()
        due_date = None
        subscriptions = form.cleaned_data['subscriptions']

        for idx, subscription in enumerate(subscriptions, 1):
            if (
                not subscription.subscription_plan.is_public and
                subscription.subscription_plan.recurrence_period > 0 and
                    subscription.subscription_plan.recurrence_period):
                due_date = subscription.expires + subscription.grace_period
                billing.subscriptions.add(subscription)
                subscription_data = {
                    "billing": billing,
                    "subscription": subscription,
                    "current_plan": subscription.subscription_plan,
                    "choosen_plan": subscription.subscription_plan
                }
                if subscription.voucher:
                    subscription_data.update({
                        "voucher": subscription.voucher,
                        "voucher_type": subscription.voucher.voucher_type,
                        "voucher_value": subscription.voucher.voucher_value,
                    })
                SubscriptionJournal.objects.create(**subscription_data)

            else:
                billing_cycle = self.request.POST.get(
                    'billing_cycle_' + str(idx), None)

                if billing_cycle:
                    try:
                        plan = SubscriptionPlan.objects.get(pk=billing_cycle)
                    except SubscriptionPlan.DoesNotExist:
                        plan = subscription.subscription_plan
                else:
                    plan = subscription.subscription_plan

                if subscription.voucher:
                    self.voucher_validation(
                        subscription, plan,
                        subscription.voucher.available_subscription.all())

                due_date = subscription.expires + subscription.grace_period
                billing.subscriptions.add(subscription)
                SubscriptionJournal.objects.create(
                    billing=billing,
                    subscription=subscription,
                    voucher=subscription.voucher,
                    current_plan=subscription.subscription_plan,
                    choosen_plan=plan
                )

        billing.due_date = due_date
        billing.save(recalculate=True)

        return HttpResponseRedirect(
            reverse_lazy('subscription:complete_payment',
            kwargs={'pk': billing.pk}))

    def voucher_validation(self, subscription, plan, allow_voucher):
        if plan not in allow_voucher:
            subscription.voucher = None
            subscription.save()


class InvoicePaymentView(SterlingRoleMixin, generic.CreateView):
    template_name = "dashboard/subscription/payment_attempt_create.html"
    model = OrderPayment
    fields = ['code', 'billing']
    success_url = reverse_lazy('subscription:billing_list')

    def get_context_data(self, **kwargs):
        context = super(InvoicePaymentView, self).get_context_data(**kwargs)
        billing = get_object_or_404(Billing, pk=self.kwargs.get('pk'))

        payment_attempt_code = billing.generate_payment_attempt_code()
        snap_token = midtrans_adapter.get_snap_charge_token(
            billing, order_id=payment_attempt_code, view_request=self.request)
        context.update({
            'invoice': billing,
            'payment_attempt_code': payment_attempt_code,
            'snap_token': snap_token,
            'midtrans_url': midtrans_adapter.snap_url,
            'midtrans_client_key': midtrans_adapter.client.client_key,
        })
        return context

    def form_valid(self, form):

        if self.model.objects.filter(code=form.data.get('code')).exists():
            form.instance = self.model.objects.filter(code=form.data.get('code')).first()

        res = super(InvoicePaymentView, self).form_valid(form)
        midtrans_response = midtrans_adapter.update_payment_attempt_status(self.object)
        return res


class VoucherRedeemView(SterlingRoleMixin, AjaxResponseMixin, JSONResponseMixin, generic.View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            voucher_code = request.POST.get('voucher_code', '')
            invoice_id = request.POST.get('invoice_id', '')
            try:
                voucher = Voucher.objects.get(
                    voucher_code=voucher_code,
                    is_redeem=False, expires__gte=datetime.date.today())
                subscriptions = Subscription.objects.all().filter(
                    Q(active=False)
                    | Q(is_trial=True)
                    | Q(expires__lt=datetime.date.today())
                ).filter(account=get_current_tenant())

                for subscription in subscriptions:
                    subscription.voucher = voucher
                    subscription.save()
                voucher.redeem()
                status_code = 200
                response['status'] = 'success'

            except Voucher.DoesNotExist:
                response['invalid_voucher'] = _("Invalid Voucher Code")
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class VoucherCancelView(SterlingRoleMixin, AjaxResponseMixin, JSONResponseMixin, generic.View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            voucher_code = request.POST.get('voucher_code', '')
            billing_id = request.POST.get('invoice_id', '')

            voucher = Voucher.objects.get(
                voucher_code=voucher_code,
                is_redeem=True, expires__gte=datetime.date.today())
            subscriptions = Subscription.objects.all().filter(
                Q(active=False)
                | Q(is_trial=True)
                | Q(expires__lt=datetime.date.today())
            ).filter(account=get_current_tenant())

            for subscription in subscriptions:
                subscription.voucher = None
                subscription.save()

            voucher.cancel()

            status_code = 200
            response['status'] = 'success'

        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CancelPaymentView(SterlingRoleMixin, AjaxResponseMixin, JSONResponseMixin, generic.View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            billing_id = request.POST.get('billing_id', '')
            billing = Billing.objects.get(pk=billing_id)
            subscriptions = billing.subscriptions.all()
            for subscription in subscriptions:
                if subscription.voucher:
                    voucher = subscription.voucher
                    voucher.cancel()
                    voucher.save()
                subscription.voucher = None
                subscription.save()
            billing.status = Billing.STATUS.canceled
            billing.save()

            status_code = 200
            response['status'] = 'success'
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
