from django.contrib import admin

from sterlingpos.notification.models import (
    NotificationSetting,
)


class NotificationSettingAdmin(admin.ModelAdmin):
    list_display = ('account', 'notification_type',)
    search_fields = ('account', 'notification_type',)


admin.site.register(NotificationSetting, NotificationSettingAdmin)
