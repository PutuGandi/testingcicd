from django.urls import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView

from braces.views import MessageMixin
from dal import autocomplete

from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.notification.forms import NotificationSettingFormSet
from sterlingpos.notification.models import NotificationSetting
from sterlingpos.users.models import User


class NotificationSettingsView(SterlingRoleMixin, MessageMixin, FormView):
    template_name = "dashboard/notification/settings.html"
    model = NotificationSetting
    form_class = NotificationSettingFormSet

    def get_success_url(self):
        url = reverse("notification:settings")
        return url
    
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            "queryset": self.request.user.account.notificationsetting_set.all()
        })
        return kwargs

    def form_valid(self, form):
        form.save(commit=True)
        messages.success(
            self.request, "{}".format(_("Notification settings has been updated."))
        )
        return super(NotificationSettingsView, self).form_valid(form)


class UserEmailAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = User.objects.all()
        if self.q:
            qs = qs.filter(email__istartswith=self.q)
        return qs

    def get_result_label(self, item):
        return item.email
