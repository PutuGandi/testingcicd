from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms import modelformset_factory

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from dal import autocomplete

from sterlingpos.notification.models import NotificationSetting
from sterlingpos.core.bootstrap import KawnField, KawnFieldSetWithHelpText


class NotificationSettingForm(forms.ModelForm):
    class Meta:
        model = NotificationSetting
        fields = ("is_enabled", "user_recipients", "id")
        widgets = {
            "user_recipients": autocomplete.ModelSelect2Multiple(
                url="notification:user_email_autocomplete",
                attrs={"data-minimum-input-length": 0},
            )
        }

    def __init__(self, *args, **kwargs):
        notification_type = None
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.fields["is_enabled"].widget.attrs["class"] = "enabled-notifications"
        self.fields["user_recipients"].label = _("Recipients")
        self.fields["user_recipients"].required = False

        instance = kwargs.get("instance")
        if instance and instance.pk:
            notification_type = instance.get_notification_type_display()
            self.fields["is_enabled"].label = notification_type

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField("is_enabled", template="widgets/float_right_switch.html"),
                KawnField("user_recipients"),
                "id",
            )
        )

    def clean_user_recipients(self):
        is_enabled = self.cleaned_data["is_enabled"]
        user_recipients = self.cleaned_data["user_recipients"]
        if is_enabled:
            if not user_recipients:
                raise forms.ValidationError("This field is required.")
        return user_recipients


class NotificationSettingsFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(NotificationSettingsFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.include_media = False
        self.add_false = True
        self.template = "bootstrap4/formset_inline.html"


NotificationSettingFormSet = modelformset_factory(
    NotificationSetting, form=NotificationSettingForm, extra=0, can_delete=False
)
