# Generated by Django 2.2.16 on 2021-02-09 07:48

from django.db import migrations, models
from sterlingpos.notification.models import NotificationSetting as NotificationSettingModel


def populate_notification_settings(apps, schema_editor):
    Account = apps.get_model("accounts", "Account")
    NotificationSetting = apps.get_model("notification", "NotificationSetting")

    for account in Account.objects.all():
        for notification_type in NotificationSettingModel.NOTIFICATION_TYPES:
            NotificationSetting.objects.get_or_create(
                account=account,
                notification_type=notification_type[0],
            )


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notificationsetting',
            name='notification_type',
            field=models.CharField(choices=[('low_stock_notification', 'Low Stock Notification'), ('expired_stock_notification', 'Expired Stock Notification')], max_length=255),
        ),
        migrations.RunPython(
            populate_notification_settings,
            migrations.RunPython.noop
        ),
    ]
