from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.core.models import SterlingTenantModel


class NotificationSetting(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    NOTIFICATION_TYPES = Choices(
        (
            "low_stock_notification",
            "low_stock_notification",
            _("Low Stock Notification"),
        ),
        (
            "expired_stock_notification",
            "expired_stock_notification",
            _("Expired Stock Notification"),
        ),
    )

    notification_type = models.CharField(max_length=255, choices=NOTIFICATION_TYPES)

    is_enabled = models.BooleanField(default=False)

    user_recipients = models.ManyToManyField("users.User", related_name="notifications")

    class Meta:
        verbose_name = "NotificationSetting"
        verbose_name_plural = "NotificationSettings"
        constraints = [
            models.UniqueConstraint(
                fields=["account", "id"], name="unique_notification_setting_account"
            ),
            models.UniqueConstraint(
                fields=["account", "notification_type",],
                name="unique_notification_setting_type_per_account",
            ),
        ]

    @classmethod
    def get_email_recipients_for_type(cls, notification_type, account):
        data = {
            "notification_type": notification_type,
            "account": account,
            "is_enabled": True,
        }

        try:
            instance = cls.objects.get(**data)
            recipients = instance.email_recipients
        except:
            recipients = []

        return recipients

    @property
    def email_recipients(self):
        recipients = self.user_recipients.values_list("email", flat=True)
        return list(recipients)
