from django.apps import AppConfig
from django.db.models.signals import post_save


class NotificationConfig(AppConfig):
    name = 'sterlingpos.notification'
    verbose_name = "Notification"
    permission_name = "notification"

    def ready(self):
        pass
