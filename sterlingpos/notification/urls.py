# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "notification"
urlpatterns = [
    url(
        regex=r"^settings/$",
        view=views.NotificationSettingsView.as_view(),
        name="settings",
    ),
    url(
        regex=r"^user-email/autocomplete/$",
        view=views.UserEmailAutocomplete.as_view(),
        name="user_email_autocomplete",
    ),
]
