import datetime

from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives

from celery import shared_task
from dateutil.relativedelta import relativedelta

from sterlingpos.accounts.models import Account
from sterlingpos.stocks.models import Batch
from sterlingpos.notification.models import NotificationSetting


@shared_task(bind=True, max_retries=5)
def notify_stocks_email(self, product_name, stock_balance, outlet, recipients):
    try:
        text_content = {
            "product_name": product_name,
            "stock_balance": stock_balance,
            "outlet": outlet,
        }
        text_content = "{}".format(text_content)
        context = {
            "product_name": product_name,
            "stock_balance": stock_balance,
            "outlet": outlet,
        }
        html_content = render_to_string(
            "dashboard/stocks/email/notify_stocks.html", context
        )

        mail = EmailMultiAlternatives(
            subject="Peringatan Stok untuk produk {} - [{}]".format(
                product_name, outlet
            ),
            body=text_content,
            to=recipients,
        )
        mail.attach_alternative(html_content, "text/html")
        mail.send()
    except Exception:
        self.retry(countdown=2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def notify_expired_email(self):
    try:
        today = datetime.date.today()
        expired_at = today + relativedelta(months=3)
        accounts = Account.objects.all()

        for account in accounts:
            batches = Batch.objects.filter(
                expiration_date=expired_at, account=account
            ).select_related("product", "product__uom")
            if batches:
                recipients = NotificationSetting.get_email_recipients_for_type(
                    NotificationSetting.NOTIFICATION_TYPES.expired_stock_notification,
                    account,
                )
                context = []

                for batch in batches:
                    data = {
                        "batch ID": batch.batch_id,
                        "product": batch.product.__str__(),
                        "expired_date": batch.expiration_date.strftime("%d/%m/%Y"),
                        "locations": [],
                    }
                    for item in batch.batch_items.all().select_related("outlet"):
                        data["locations"].append(
                            {
                                "outlet": item.outlet.name,
                                "balance": item.balance,
                                "unit": batch.product.uom.unit,
                            }
                        )

                    context.append(data)

                text_content = "{}".format({"data": context})
                html_content = render_to_string(
                    "dashboard/stocks/email/notify_expired_date.html", {"data": context}
                )
                mail = EmailMultiAlternatives(
                    subject="Peringatan Stok Kadaluarsa : Account- {}".format(account),
                    body=text_content,
                    to=recipients,
                )
                mail.attach_alternative(html_content, "text/html")
                mail.send()
    except Exception as e:
        print(e)
        self.retry(countdown=2 ** self.request.retries)
