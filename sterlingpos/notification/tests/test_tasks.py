import datetime
from django.test import TestCase

from model_mommy import mommy
from django_multitenant.models import set_current_tenant
from dateutil.relativedelta import relativedelta

from sterlingpos.notification.tasks import notify_stocks_email, notify_expired_email
from sterlingpos.notification.models import NotificationSetting
from sterlingpos.stocks.models import Stock


class TestTaskNotifyStocksEmail(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.account", name="test-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit_pcs = self.account.uom_set.filter(
            category="units", uom_type="base"
        ).first()
        self.category = mommy.make(
            "catalogue.category", name="Makanan", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_pcs,
            price=15000,
            cost=5000,
        )
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="Branch-1", account=self.account
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit_pcs)

        self.notification_setting, _ = NotificationSetting.objects.get_or_create(
            account=self.account,
            notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
        )
        self.notification_setting.user_recipients.add(self.user)

    def tearDown(self):
        set_current_tenant(None)

    def test_task_success(self):
        recipients = NotificationSetting.get_email_recipients_for_type(
            NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
            self.account,
        )
        task = notify_stocks_email.apply(
            kwargs={
                "product_name": self.product.name,
                "stock_balance": self.stock.balance,
                "outlet": self.outlet.name,
                "recipients": recipients,
            }
        )
        self.assertEqual(task.state, "SUCCESS")


class TestTaskNotifyExpiredEmail(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.account", name="test-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.user", email="example@test.com", account=self.account
        )
        self.user.set_password("1sampai6")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        self.unit_pcs = self.account.uom_set.filter(
            category="units", uom_type="base"
        ).first()
        self.category = mommy.make(
            "catalogue.category", name="Makanan", account=self.account
        )
        self.product = mommy.make(
            "catalogue.product",
            name="Baso",
            category=self.category,
            account=self.account,
            uom=self.unit_pcs,
            price=15000,
            cost=5000,
        )
        self.outlet = mommy.make(
            "outlet.Outlet", branch_id="Branch-1", account=self.account
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(100, unit=self.unit_pcs)

        self.notification_setting, _ = NotificationSetting.objects.get_or_create(
            account=self.account,
            notification_type=NotificationSetting.NOTIFICATION_TYPES.expired_stock_notification,
        )
        self.notification_setting.user_recipients.add(self.user)

        self.batch = mommy.make(
            "stocks.Batch",
            account=self.account,
            product=self.product,
            batch_id="Batch-01",
            expiration_date=datetime.date.today() + relativedelta(months=3),
        )
        self.item_batch = mommy.make(
            "stocks.BatchItem",
            account=self.account,
            batch=self.batch,
            outlet=self.outlet,
        )
        self.item_batch.set_batch_balance(100, self.unit_pcs)

    def tearDown(self):
        set_current_tenant(None)

    def test_task_success(self):
        task = notify_expired_email.apply()
        self.assertEqual(task.state, "SUCCESS")
