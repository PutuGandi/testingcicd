from django.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.notification.forms import NotificationSettingForm
from sterlingpos.notification.models import NotificationSetting


class TestNotificationSettingForm(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="Account")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.form_class = NotificationSettingForm

    def tearDown(self):
        set_current_tenant(None)

    def test_update(self):
        instance, _ = NotificationSetting.objects.get_or_create(
            account=self.account,
            notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
        )
        data = {
            "is_enabled": True,
            "user_recipients": [self.user.pk,],
        }
        form = self.form_class(data, instance=instance)

        self.assertTrue(form.is_valid(), form.errors)
        new_instance = form.save()

        self.assertEqual(new_instance.email_recipients, ["test@email.com"])

    def test_update_recipients(self):
        instance, _ = NotificationSetting.objects.get_or_create(
            account=self.account,
            notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
        )
        data = {
            "user_recipients": [self.user.pk,],
        }
        form = self.form_class(data, instance=instance)

        self.assertTrue(form.is_valid(), form.errors)
        new_instance = form.save()

        self.assertEqual(new_instance.email_recipients, ["test@email.com"])
