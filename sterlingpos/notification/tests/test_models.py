from django.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.notification.models import NotificationSetting


class TestNotificationSetting(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="Account")

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_email_recipients_for_type(self):
        instance, _ = NotificationSetting.objects.get_or_create(
            account=self.account,
            notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
        )
        instance.is_enabled = True
        instance.save()
        instance.user_recipients.add(self.user)

        email_recipients = NotificationSetting.get_email_recipients_for_type(
            NotificationSetting.NOTIFICATION_TYPES.low_stock_notification, self.account,
        )
        self.assertEqual(email_recipients, ["test@email.com"])

    def test_get_email_recipients_for_type_from_disabled_notif(self):
        instance, _ = NotificationSetting.objects.get_or_create(
            account=self.account,
            notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
        )
        instance.is_enabled = False
        instance.save()
        instance.user_recipients.add(self.user)

        email_recipients = NotificationSetting.get_email_recipients_for_type(
            NotificationSetting.NOTIFICATION_TYPES.low_stock_notification, self.account,
        )
        self.assertEqual(email_recipients, [])

    def test_get_email_recipients(self):
        instance, _ = NotificationSetting.objects.get_or_create(
            account=self.account,
            notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
        )
        instance.user_recipients.add(self.user)

        self.assertEqual(instance.email_recipients, ["test@email.com"])
