from django.conf import settings
from django.core.cache import cache
from django_multitenant.utils import set_current_tenant, get_current_tenant


class SetCurrentTenantFromUser(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def get_account(self, request):
        account_key = "user-{}-account".format(request.user.pk)
        account = cache.get(account_key)

        if not account:
            account = request.user.account
            cache.set(account_key, request.user.account)

        return account

    def __call__(self, request):

        if request.path.strip('/').split('/')[0] == settings.ADMIN_URL.strip('^/'):
            set_current_tenant(None)
        else:
            if request.user.is_authenticated:
                account = self.get_account(request)
                set_current_tenant(account)
            else:
                set_current_tenant(None)
            request.current_tenant = get_current_tenant()

        response = self.get_response(request)
        set_current_tenant(None)
        return response
