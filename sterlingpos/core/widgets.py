from djmoney.forms.widgets import MoneyWidget


class MoneyInputField(MoneyWidget):
    template_name = "widgets/money.html"