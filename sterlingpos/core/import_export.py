import io
import tablib
import logging
import csv
import importlib

from django import forms
from django.views.generic import FormView
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import force_str

from djmoney.money import Money

from import_export import resources
from import_export.admin import TMP_STORAGE_CLASS
from import_export.formats.base_formats import CSV
from import_export.fields import Field
from import_export.widgets import (
    ManyToManyWidget, ForeignKeyWidget,
    CharWidget, DecimalWidget,
)
from decimal import Decimal, InvalidOperation
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, TEMPLATE_PACK

from sterlingpos.core.models import get_current_tenant
from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from sterlingpos.core.paginator import LastSeenPaginator

logger = logging.getLogger(__name__)


class CustomManyToManyWidget(ManyToManyWidget):

    def clean(self, value, row=None, *args, **kwargs):
        if not value:
            return self.model.objects.none()
        if isinstance(value, (float, int)):
            ids = [int(value)]
        else:
            ids = value.split(self.separator)
            ids = filter(None, [i.strip() for i in ids])
        return self.model.objects.filter(**{
            '%s__in' % self.field: ids,
        })


class CustomForeignKeyWidget(ForeignKeyWidget):

    def __init__(self, *args, **kwargs):
        self.create_function = kwargs.pop("create_function", None)
        self.custom_function = kwargs.pop("custom_function", None)
        self.required = kwargs.pop("required", False)
        super().__init__(*args, **kwargs)

    def clean(self, value, row=None, *args, **kwargs):
        if value:
            if self.custom_function:
                value = self.custom_function(value, row)
                return value
            try:
                return self.get_queryset(
                    value, row, *args, **kwargs
                ).get(**{self.field: value})
            except self.model.DoesNotExist:
                if self.create_function:
                    value = self.create_function(value, row)
                return value
            except self.model.MultipleObjectsReturned:
                raise ValueError(
                    _(f"There are more than 2 existing {self.model.__class__.__name__} {self.field.column_name} under this value."))
        else:
            if self.required:
                raise ValueError(_("Enter a valid value."))


class RequiredCharWidget(CharWidget):

    def clean(self, value, row=None, *args, **kwargs):
        if not value:
            raise ValueError(_("Enter a valid char value."))
        return value


class CustomDecimalWidget(DecimalWidget):

    def clean(self, value, row=None, *args, **kwargs):
        if not value:
            raise ValueError(_("Enter a valid decimal value"))
        try:
            value = Decimal(value)
        except InvalidOperation:
            raise ValueError(_("{} invalid syntax"))
        return value


class SterlingImportForm(forms.Form):

    from_encoding = "utf-8"
    
    upload_file = forms.FileField(required=True)
    
    def __init__(self, *args, **kwargs):
        self.resource = kwargs.pop("resource")
        self.headers = kwargs.pop("headers")
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnField("upload_file"),
        )

    def get_tmp_storage_class(self):
        return TMP_STORAGE_CLASS

    def write_to_tmp_storage(self, import_file, input_format):
        tmp_storage = self.get_tmp_storage_class()()
        data = bytes()
        for chunk in import_file.chunks():
            data += chunk

        tmp_storage.save(data, input_format.get_read_mode())
        return tmp_storage

    def display_invalid_row(self, invalid_row):
        error_list = set(
            err_detail.__str__()
            for err in invalid_row
            for err_detail in err.error
        )
        return error_list

    def display_error(self, row_errors):
        error_list = set(
            err_detail.error.__str__()
            for err in row_errors
            for err_detail in err[1]
        )
        return error_list

    def clean(self):
        cleaned_data = super().clean()
        if not self.files.get("upload_file"):
            raise forms.ValidationError(_("No upload file found."))

        import_file = self.files.get("upload_file")
        input_format = CSV()
        tmp_storage = self.write_to_tmp_storage(
            import_file, input_format)
        try:
            data = tmp_storage.read(
                input_format.get_read_mode())
            if not input_format.is_binary() and self.from_encoding:
                data = force_str(data, self.from_encoding)

            csv.Sniffer().sniff(data, delimiters=",")

            dataset = tablib.Dataset(
                headers=self.headers).load(
                    data, format="csv")
        except UnicodeDecodeError as e:
            raise forms.ValidationError(_(u"Imported file has a wrong encoding: %s" % e))
        except Exception as e:
            logged_data = f"{self.resource.__class__.__name__} Unable to import data {get_current_tenant()} : {e}"
            logger.warning(logged_data)
            raise forms.ValidationError(
                _(u"Invalid file format, make sure uploaded file is a CSV file with a 'comma' (,) delimiter.")
            )

        result = self.resource.import_data(
            dataset,
            dry_run=True,
            raise_errors=False,
            file_name=import_file.name,
        )

        self.dataset = dataset
        self.tmp_storage = tmp_storage
        self.result = result

        is_valid = not result.has_errors() and not result.has_validation_errors()

        if not is_valid:
            logged_data = f"{self.resource.__class__.__name__} Unable to import data {get_current_tenant()} : {self.display_error(result.row_errors())}{self.display_invalid_row(result.invalid_rows)}"
            logger.warning(logged_data)
            raise forms.ValidationError(
                _("Invalid data, make sure header and data format match the example template given.")
            )

        return cleaned_data

    def save(self):
        self.result = self.resource.import_data(
            self.dataset,
            dry_run=False,
            raise_errors=True,
            file_name=self.files.get("upload_file").name,
        )
        self.tmp_storage.remove()
        return self.result


class SterlingImportMixin(FormView):
    resource_class = None
    form_class = SterlingImportForm
    file_headers = []

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["resource"] = self.get_resource()
        kwargs["headers"] = self.get_headers()
        return kwargs
    
    def get_headers(self):
        return self.file_headers

    def get_resource(self):
        if not self.resource_class:
            raise NotImplementedError(
                '`resource_class` must be set and is an instance of resources.ModelResource or resources.Resource.')
        return self.resource_class()

    def form_valid(self, form):
        self.result = form.save()
        result_data = _("Total Rows = {}, New = {}, Updated = {}".format(
            self.result.total_rows,
            self.result.totals['new'],
            self.result.totals['update'],
        ))
        messages.success(self.request, '{} {}'.format(
            _('Import has been completed.'),
            result_data
            )
        )
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["result"] = getattr(self, "result", None)
        return context


class LastSeenIterMixin:
    
    def iter_queryset(self, queryset):
        if queryset._prefetch_related_lookups:
            # Django's queryset.iterator ignores prefetch_related which might result
            # in an excessive amount of db calls. Therefore we use pagination
            # as a work-around
            if not queryset.query.order_by:
                # Paginator() throws a warning if there is no sorting
                # attached to the queryset
                queryset = queryset.order_by('pk')
            paginator = LastSeenPaginator(
                queryset, self.get_chunk_size())
            last_seen = 0
            while paginator.has_next(last_seen):
                yield from paginator.page(last_seen)
                last_seen = paginator.get_last_seen_from(last_seen)
        else:
            yield from queryset.iterator(
                chunk_size=self.get_chunk_size())
