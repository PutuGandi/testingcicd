from crispy_forms.layout import Fieldset, Field
from crispy_forms.utils import (
    TEMPLATE_PACK,
    render_field, flatatt)


class KawnField(Field):
    template = "bootstrap4/layout/kawn_field.html"

    def __init__(self, field, appended_html=None, *args, **kwargs):
        self.field = field
        self.appended_html = appended_html
        if 'active' in kwargs:
            self.active = kwargs.pop('active')

        super(KawnField, self).__init__(field, *args, **kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, extra_context=None, **kwargs):
        if extra_context is None:
            extra_context = {}
        if hasattr(self, 'wrapper_class'):
            extra_context['wrapper_class'] = self.wrapper_class

        template = self.get_template_name(template_pack)

        return self.get_rendered_fields(
            form, form_style, context, template_pack,
            template=template, attrs=self.attrs, extra_context=extra_context,
            **kwargs
        )


class KawnFieldSet(Fieldset):
    template = "bootstrap4/layout/kawn_fieldset.html"

    def __init__(self, legend, *fields, **kwargs):
        super(KawnFieldSet, self).__init__(legend, *fields, **kwargs)
        self.title_info = kwargs.get('title_info', None)
        self.title_class = kwargs.pop('title_class', '')
        self.css_class = kwargs.pop('css_class', '')
        self.css_id = kwargs.pop('css_id', None)


class KawnFieldSetWithHelpText(Fieldset):
    template = "bootstrap4/layout/kawn_fieldset_with_help_text.html"

    def __init__(self, legend, *fields, **kwargs):
        super(KawnFieldSetWithHelpText, self).__init__(legend, *fields, **kwargs)
        self.title_info = kwargs.get('title_info', None)
        self.help_text = kwargs.get('help_text')
        self.title_class = kwargs.pop('title_class', '')
        self.fields = list(fields)
        self.legend = legend
        self.css_class = kwargs.pop('css_class', '')
        self.css_id = kwargs.pop('css_id', None)
        self.small_desc = kwargs.pop('small_desc', None)
        self.template = kwargs.pop('template', self.template)
        self.flat_attrs = flatatt(kwargs)
        self.form_grid = kwargs.get('form_grid', None)



class KawnPrependedAppendedText(Field):
    template = "bootstrap4/layout/kawn_prepend_appended_text.html"

    def __init__(self, field, prepended_text=None, appended_text=None, *args, **kwargs):
        self.field = field
        self.appended_text = appended_text
        self.prepended_text = prepended_text
        if 'active' in kwargs:
            self.active = kwargs.pop('active')

        self.input_size = None
        css_class = kwargs.get('css_class', '')
        if 'input-lg' in css_class:
            self.input_size = 'input-lg'
        if 'input-sm' in css_class:
            self.input_size = 'input-sm'

        super(KawnPrependedAppendedText, self).__init__(field, *args, **kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, extra_context=None, **kwargs):
        extra_context = extra_context.copy() if extra_context is not None else {}
        extra_context.update({
            'crispy_appended_text': self.appended_text,
            'crispy_prepended_text': self.prepended_text,
            'input_size': self.input_size,
            'active': getattr(self, "active", False)
        })
        if hasattr(self, 'wrapper_class'):
            extra_context['wrapper_class'] = self.wrapper_class
        template = self.get_template_name(template_pack)
        return render_field(
            self.field, form, form_style, context,
            template=template, attrs=self.attrs,
            template_pack=template_pack, extra_context=extra_context, **kwargs
        )


class KawnAppendedText(KawnPrependedAppendedText):
    def __init__(self, field, text, *args, **kwargs):
        kwargs.pop('appended_text', None)
        kwargs.pop('prepended_text', None)
        self.text = text
        super(KawnAppendedText, self).__init__(field, appended_text=text, **kwargs)


class KawnPrependedText(KawnPrependedAppendedText):
    def __init__(self, field, text, *args, **kwargs):
        kwargs.pop('appended_text', None)
        kwargs.pop('prepended_text', None)
        self.text = text
        super(KawnPrependedText, self).__init__(field, prepended_text=text, **kwargs)
