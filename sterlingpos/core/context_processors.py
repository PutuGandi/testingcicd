from django.conf import settings


def staging(request):
    data = {}
    data['STAGING'] = settings.STAGING
    return data
