import json

from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View


class ShopeepayNotificationHandlingView(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        response_body = request.readlines()
        response_json = json.loads(
            response_body[0].decode().replace("'", '"'))

        with open("shopee-response", "a+") as shooperes:
            shooperes.writelines(json.dumps(response_json))
            shooperes.writelines("\n")

        return HttpResponse("OK")
