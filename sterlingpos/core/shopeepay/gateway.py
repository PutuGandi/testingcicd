from .response import Response

class Core:
    """
    Core API gateway
    """

    def __init__(self, client):
        self.client = client

    def call(self, method, path, parameters=dict()):
        if not path[0] == "/":
            path = "/" + path

        url = self.client.environment_type.api_url + path
        json_resp = self.client.call(method, url, parameters)
        return Response(**json_resp)

    def get_qr(self, charge_request):
        return self.call(
            "post", "/v2/qr/merchant-host/get",
            charge_request)

    def check_payment(self, charge_request):
        return self.call(
            "post", "/v2/merchant-host/transaction/check/payment",
            charge_request)
