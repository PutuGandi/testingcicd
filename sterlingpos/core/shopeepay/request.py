import uuid


class IQRSRequest:

    def __init__(self,
                 amount,
                 merchant_ext_id,
                 store_ext_id,
                 reference_id,
                 terminal_id=None):

        self.request_id=uuid.uuid4().__str__()

        self.amount=amount * 100
        self.currency="IDR"
        self.merchant_ext_id=merchant_ext_id
        self.store_ext_id=store_ext_id
        self.reference_id=reference_id

        self.convenience_fee_percentage=0
        self.convenience_fee_fixed=0
        self.convenience_fee_indicator="03"

        if terminal_id:
            self.terminal_id =terminal_id

    def serialize(self):
        return self.__dict__

    def __repr__(self):
        return ("<Shopeepay.IQRSRequest({} [{}])>".format(
            self.request_id, self.amount))


class PaymentCheckRequest:

    def __init__(self,
                 payment_reference_id,
                 merchant_ext_id,
                 store_ext_id):

        self.request_id=uuid.uuid4().__str__()

        self.payment_reference_id=payment_reference_id
        self.merchant_ext_id=merchant_ext_id
        self.store_ext_id=store_ext_id

    def serialize(self):
        return self.__dict__

    def __repr__(self):
        return ("<Shopeepay.PaymentCheckRequest({} [{}])>".format(
            self.request_id, self.payment_reference_id))
