from datetime import datetime


class Response:

    PAYMENT_STATUS_VERBOSE = {
        1 : "PAYMENT_COMPLETED",
        2 : "PAYMENT_NOT_FOUND",
        3 : "PAYMENT_REFUNDED",
    }

    def __init__(self, *args, **kwargs):

        self.request_id = kwargs.get('request_id')
        self.errcode = int(kwargs.get('errcode'))
        self.debug_msg = kwargs.get('debug_msg')

        if "qr_url" in kwargs:
            self.qr_content = kwargs.get('qr_content')
            self.qr_url = kwargs.get('qr_url')

        if "payment_status" in kwargs:
            self.payment_status = int(kwargs.get("payment_status"))
            self.payment_status_verbose = self.PAYMENT_STATUS_VERBOSE[self.payment_status]
            self.transaction_list = [
                TransactionDetail(**transaction)
                for transaction in kwargs.get("transaction_list", [])
            ]

    def __repr__(self):

        if self.errcode > 0:
            data = "<Shopeepay.Response({} [err={}, detail={}])>".format(
                self.request_id, self.errcode, self.debug_msg)
        elif hasattr(self, "qr_url"):
            data = "<Shopeepay.Response({} [{}, url={}])>".format(
                self.request_id, self.qr_content, self.qr_url)
        elif hasattr(self, "payment_status"):
            data = "<Shopeepay.Response({} [{}])>".format(
                self.request_id, self.payment_status_verbose)

        return (data)


class TransactionDetail:

    TRANSACTION_TYPE_VERBOSE = {
        13: "PAYMENT_RECEIVED",
        15: "REFUND_SENT",
    }

    def __init__(self, *args, **kwargs):
        self.reference_id = kwargs.get("reference_id")
        self.amount = int(kwargs.get("amount"))
        self.transaction_sn = kwargs.get("transaction_sn")
        self.transaction_type = int(kwargs.get("transaction_type"))
        self.transaction_type_verbose = self.TRANSACTION_TYPE_VERBOSE[self.transaction_type]

        self.create_time = kwargs.get("create_time")
        self.update_time = kwargs.get("update_time")

        self.merchant_ext_id = kwargs.get("merchant_ext_id")
        self.store_ext_id = kwargs.get("store_ext_id")
        self.terminal_id = kwargs.get("terminal_id", None)

    def __repr__(self):
        data = "<Shopeepay.TransactionDetail({}-{} [{}])>".format(
            self.reference_id,
            self.amount,
            self.transaction_type_verbose)
        return (data)
