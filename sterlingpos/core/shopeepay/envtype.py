class EnvironmentType:
    def __init__(self):
        self.envname = ""
        self.api_url = ""

    def __repr__(self):
        return "EnvType: {0}".format(self.envname)

SANDBOX = EnvironmentType()
SANDBOX.envname = "SANDBOX"
SANDBOX.api_url = "https://api.uat.wallet.airpay.co.id"

PRODUCTION = EnvironmentType()
PRODUCTION.envname = "PRODUCTION"
PRODUCTION.api_url = "https://api.wallet.airpay.co.id "
