from .envtype import SANDBOX
import json
import requests
import logging
import hmac 
import hashlib 
import base64

logger = logging.getLogger(__name__)


class Client:
    """
    A basic driver
    """

    def __init__(self,
                 client_key,
                 server_key,
                 environment_type=SANDBOX):

        self.environment_type = environment_type
        self.client_key = client_key
        self.server_key = server_key

    def generate_headers(self, payload):
        request_signature = self.generate_signature(payload)
        headers = {
            'content-type': 'application/json',
            'accept': 'application/json',
            "X-Airpay-ClientId": self.client_key, 
            "X-Airpay-Req-H": request_signature 
        }
        return headers

    def generate_signature(self, payload): 
        digest_signature = hmac.new(
            bytes(self.server_key, "utf-8"),
            payload,
            hashlib.sha256
        ).digest()
        signature = base64.b64encode(
            digest_signature).decode().rstrip("\n")
        return signature

    def call(self, method, full_url, parameters=dict()):
        payload = json.dumps(parameters).encode("utf-8")
        headers = self.generate_headers(payload)

        response = requests.request(
            method,
            full_url,
            auth=(self.server_key, ''),
            data=payload,
            headers=headers,
            allow_redirects=True,
            verify=False if self.environment_type == SANDBOX else True,
        )

        try:
            json_response = response.json()
            return json_response
        except:
            log_data = "Shopeepay Request Aborted method={}, url={}, payload={}, response={}".format(
                method,
                full_url,
                payload,
                response,
            )
            logger.error(log_data)

    def __repr__(self):
        return ("<Shopeepay.Client({0})>".format(self.environment_type.envname))
