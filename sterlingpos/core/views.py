import json
import csv
from datetime import datetime, timedelta

from django.conf import settings
from django.http import HttpResponseRedirect
from django.forms.utils import ErrorDict
from django.db.models import FilteredRelation, Q, Count, Value, Max, Min
from django.contrib.postgres.aggregates.general import StringAgg
from django.db.models.functions import Coalesce, Substr, Length
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _, ugettext
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.utils.encoding import smart_str
from django.views.generic import View
from django.http import HttpResponse

from sterlingpos.stocks.forms import (
    StockTransferDetailFormset,
    StockCountDetailFormset,
    StockCountCountingFormset,
    StockAdjustmentDetailFormset,
)

from braces.views import JSONResponseMixin, AjaxResponseMixin


class UpdateFormset(
    JSONResponseMixin, AjaxResponseMixin, View):

    formset_config = {
        "transfer_details": {
            "class": StockTransferDetailFormset,
            "template": "dashboard/stocks/transfer_detail_formset.html",
        },
        "stock_count_details": {
            "class": StockCountDetailFormset,
            "template": "dashboard/stocks/transfer_detail_formset.html",
        },
        "stock_adjustment_details": {
            "class": StockAdjustmentDetailFormset,
            "template": "dashboard/stocks/transfer_detail_formset.html",
        },
    }

    def process_formset(
        self, prefix, data, response):

        config = self.formset_config.get(prefix)
        if config:
            formset = config.get("class")(
                data=data, prefix=prefix)

            formset._errors = []
            for form in formset.forms:
                form._errors = ErrorDict()

            form = render_to_string(
                config.get("template"),
                {'formset': formset})

            return form

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            forms = {}
            deleted_data = []
            prefix = request.POST.get('prefix', '')
            total_form = request.POST.get('total_form', '')
            initial_data = json.loads(request.POST.get('initial_data', ''))
            total_data = request.POST.get('total_data', '')
            initial_form = request.POST.get('initial_form', '0')

            data = {
                prefix + '-TOTAL_FORMS': total_form,
                prefix + '-INITIAL_FORMS': initial_form,
                prefix + '-MAX_NUM_FORMS': '',
            }
            for i in initial_data:
                data.update({i: initial_data[i]})

            for i in data:
                if "DELETE" in i:
                    if data[i]:
                        deleted_data.append(i)

            form = self.process_formset(
                prefix, data, response)

            forms['prefix'] = prefix
            forms['form'] = form
            forms['deleted_data'] = deleted_data
            response['data'] = forms
            response['status'] = 'success'
            status_code = 200

        except ValueError:
            pass

        return self.render_json_response(
                response, status=status_code)

