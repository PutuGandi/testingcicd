import csv
import re
import logging

from decimal import Decimal, ROUND_DOWN
from itertools import chain

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta, MO, SU

from django.http import (
    HttpResponseRedirect,
    StreamingHttpResponse
)

from django.conf import settings
from django.utils.html import escape
from django.utils.text import slugify
from django.shortcuts import get_object_or_404

from django.db.models import Q

from braces.views import UserPassesTestMixin

from sterlingpos.role.utils import get_outlet_of_user

logger = logging.getLogger(__name__)


class OwnerOnlyMixin(UserPassesTestMixin):

    def test_func(self, user):
        return user.is_owner


class Echo(object):
    """
    https://docs.djangoproject.com/id/2.0/howto/outputting-csv/#streaming-large-csv-files
    An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


class ReportFilterMixins(object):

    def get_outlet(self, outlet_pk, context):
        if outlet_pk:
            outlet_qs = get_outlet_of_user(self.request.user)

            outlet = get_object_or_404(
                outlet_qs,
                pk=outlet_pk)
            context['outlet'] = outlet
        else:
            outlet = None
        return outlet

    def get_start_date_and_end_date(self, date_filter, start_date_raw, end_date_raw):
        start_date = end_date = None

        if date_filter == 'today':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
            end_date = start_date + relativedelta(hour=23, minute=59)
        elif date_filter == 'yesterday':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) - relativedelta(days=1)
            end_date = start_date + relativedelta(hour=23, minute=59)
        elif date_filter == 'last_week':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-2))
            end_date = start_date + relativedelta(weekday=SU(1), hour=23, minute=59)
        elif date_filter == 'last_month':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) + relativedelta(day=1, months=-1)
            end_date = start_date + relativedelta(day=1, months=+1, days=-1, hour=23, minute=59)
        elif date_filter == 'week_to_date':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
            end_date = datetime.today().replace(hour=23, minute=59)
        elif date_filter == 'month_to_date':
            start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
            end_date = datetime.today().replace(hour=23, minute=59)
        else:
            start_date, end_date = self.get_start_date_and_end_date_by_custom_range(
                start_date_raw, end_date_raw)
        return start_date, end_date

    def get_start_date_and_end_date_by_custom_range(self, start_date_raw, end_date_raw):
        try:
            start_date = datetime.strptime(
                start_date_raw, "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)

        try:
            end_date = datetime.strptime(
                end_date_raw, "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hours=23, minutes=59, second=59)
        return start_date, end_date

    def get_diff_percentage(self, current_value, compared_value):
        if compared_value == 0:
            diff_percentage = 0
        else:
            diff_percentage = ((Decimal(current_value) - Decimal(compared_value)) / Decimal(compared_value)) * 100
            diff_percentage = diff_percentage.quantize(Decimal('.01'), rounding=ROUND_DOWN)
        return diff_percentage


class CSVExportMixin(object):
    csv_columns = []
    model = None
    has_aggregated_data = False
    resource_class = None

    def __init__(self, *args, **kwargs):
        if not hasattr(self, 'csv_header'):
            raise NotImplementedError('`csv_header` must be implemented as list of text header for CSV file.')
        super(CSVExportMixin, self).__init__(*args, **kwargs)

    def get_csv_columns(self):
        return self.csv_columns

    def render_csv_column(self, row, column):
        """ Renders a column on a row
        """
        if hasattr(row, 'get_%s_display' % column):
            # It's a choice field
            text = getattr(row, 'get_%s_display' % column)()
        else:
            try:
                text = getattr(row, column)
            except AttributeError:
                obj = row
                for part in column.split('.'):
                    if obj is None:
                        break
                    obj = getattr(obj, part)

                text = obj
        if text is None:
            text = self.none_string

        if text and hasattr(row, 'get_absolute_url'):
            return '<a href="%s">%s</a>' % (row.get_absolute_url(), text)
        else:
            return text

    def render_result(self, qs):

        if self.resource_class:
            resource = self.resource_class()
            resource.before_export(qs)
            data_generators = (
                resource.export_resource(obj)
                for obj in resource.iter_queryset(qs)
            )
        else:
            data_generators = (
                (
                    self.render_csv_column(item, column) 
                    for column in self.get_csv_columns()
                )
                for item in qs
            )

        data = chain(
            iter([self.csv_header]),
            data_generators
        )

        if self.has_aggregated_data:
            data = chain(
                data,
                iter(self.get_aggregated_data(data))
            )

        return data

    def get_aggregated_data(self):
        if self.has_aggregated_data:
            raise NotImplementedError(
                "Subclasses should implement `get_aggregated_data`")

    def get_csv_filename(self):
        if not hasattr(self, 'csv_filename'):
            raise NotImplementedError(
                "Subclasses should implement `get_csv_filename` or set `csv_filename` !")
        return self.csv_filename

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs)
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def get(self, request, *args, **kwargs):
        if request.GET.get('export_file') == 'csv':
            context = self.get_context_data(**kwargs)
            csv_data = self.get_csv_data(context)

            pseudo_buffer = Echo()
            writer = csv.writer(pseudo_buffer)
            csv_response = (writer.writerow(row) for row in csv_data)

            response = StreamingHttpResponse(
                csv_response,
                content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename={}.csv'.format(
                self.get_csv_filename())
            return response
        else:
            return super(CSVExportMixin, self).get(request, *args, **kwargs)


class ReportCSVExportMixin(CSVExportMixin):
    """
    TODO: 
        > override get_csv_data to work with completed calculation data 
          from context
        > get_csv_fieldname to work with keys from context dict 
    """

    def get_csv_filename(self):
        if not hasattr(self, 'csv_filename'):
            raise NotImplementedError(
                "Subclasses should implement `get_csv_filename` or set `csv_filename` !")

        csv_filename = self.csv_filename

        context = self.get_context_data(**self.kwargs)
        if context.get("outlet"):
            csv_filename = "{}_{}".format(
                csv_filename, slugify(context.get("outlet").name))
        
        if context.get("start_date"):
            csv_filename = "{}_{}".format(
                csv_filename, context.get("start_date").strftime("%Y/%m/%d--%H.%M.%S"))

        if context.get("end_date"):
            csv_filename = "{}_{}".format(
                csv_filename, context.get("end_date").strftime("%Y/%m/%d--%H.%M.%S"))

        return csv_filename

    def get_csv_fieldname(self):
        if not hasattr(self, 'csv_fieldname'):
            raise NotImplementedError('`csv_fieldname` must be implemented as list of field data for CSV file.')
        return self.csv_fieldname

    def get_csv_data(self, context):
        csv_dict = context.copy()
        csv_data_list = [
            csv_dict.get(fieldname)
            for fieldname in self.get_csv_fieldname()
        ]
        return csv_data_list        


class DatatableMixins(object):
    datetime_col = []
    DISPLAY_PAGINATION = True

    def extract_datatables_column_data(self):
        request_dict = self._querydict
        col_data = []
        if not self.pre_camel_case_notation:
            r = re.compile("columns\[\d+\]\[name\]")
            r2 = re.compile("columns\[(?P<counter>\d+)\]\[name\]")
            values = filter(r.match, request_dict.keys())

            for data_name_key in values:
                counter = r2.match(data_name_key).group('counter')
                searchable = True if request_dict.get('columns[{0}][searchable]'.format(counter)) == 'true' else False
                orderable = True if request_dict.get('columns[{0}][orderable]'.format(counter)) == 'true' else False

                col_data.append({
                    'name': request_dict.get(data_name_key),
                    'data': int(request_dict.get('columns[{0}][data]'.format(counter))),
                    'searchable': searchable,
                    'orderable': orderable,
                    'search.value': request_dict.get('columns[{0}][search][value]'.format(counter)),
                    'search.regex': request_dict.get('columns[{0}][search][regex]'.format(counter)),
                })
        return col_data

    def filter_queryset(self, qs, **kwargs):
        exclude_col = kwargs.get('exclude_col', [])

        if not self.pre_camel_case_notation:
            search = self._querydict.get('search[value]', None)
            col_data = self.extract_datatables_column_data()

            q = Q()
            for index, col in enumerate(col_data):
                column_name = self.order_columns[col.get('data')]
                if column_name in exclude_col:
                    continue

                if search and col['searchable']:
                    q |= Q(**{'{0}__istartswith'.format(column_name): search})
            
                if column_name in self.datetime_col and col['search.value']:
                    try:
                        start_range = datetime.strptime(
                            col['search.value'].split(' - ')[0],
                            "%Y-%m-%d %H:%M:%S"
                        )
                    except (TypeError, ValueError) as e:
                        start_range = None

                    try:
                        end_range = datetime.strptime(
                            col['search.value'].split(' - ')[-1],
                            "%Y-%m-%d %H:%M:%S"
                        )
                    except (TypeError, ValueError) as e:
                        end_range = None
                    qs = qs.filter(**{'{0}__gte'.format(column_name): start_range})
                    qs = qs.filter(**{'{0}__lte'.format(column_name): end_range})
                elif hasattr(self, "filter_{}".format(column_name)) and col['search.value']:
                    func = getattr(self, "filter_{}".format(column_name))
                    queries = func(col['search.value'])
                    if not queries:
                        queries = Q()
                    qs = qs.filter(queries)
                elif col['search.value']:
                    qs = qs.filter(**{'{0}__istartswith'.format(column_name): col['search.value']})
            qs = qs.filter(q)
        return qs

    def render_column(self, row, column):
        if hasattr(self, "render_{}".format(column)):
            func = getattr(
                self, "render_{}".format(column))
            value = func(row)
            return value
        else:
            value = super().render_column(row, column)
            value = escape(value)

            return value

    def get_total_records(self, qs):
        if self.DISPLAY_PAGINATION:
            return qs.count()
        return 0

    def get_context_data(self, *args, **kwargs):
        try:
            self.initialize(*args, **kwargs)

            qs = self.get_initial_queryset()

            # number of records before filtering
            # total_records = self.get_total_records(qs)

            qs = self.filter_queryset(qs)

            # number of records after filtering
            total_records = total_display_records = self.get_total_records(qs)
            
            qs = self.ordering(qs)
            qs = self.paging(qs)

            # prepare output data
            if self.pre_camel_case_notation:
                aaData = self.prepare_results(qs)

                ret = {'sEcho': int(self._querydict.get('sEcho', 0)),
                       'iTotalRecords': total_records,
                       'iTotalDisplayRecords': total_display_records,
                       'aaData': aaData
                       }
            else:
                data = self.prepare_results(qs)

                ret = {'draw': int(self._querydict.get('draw', 0)),
                       'recordsTotal': total_records,
                       'recordsFiltered': total_display_records,
                       'data': data
                }
        except Exception as e:
            logger.exception(str(e))

            if settings.DEBUG:
                import sys
                from django.views.debug import ExceptionReporter
                reporter = ExceptionReporter(None, *sys.exc_info())
                text = "\n" + reporter.get_traceback_text()
            else:
                text = "\nAn error occured while processing an AJAX request."

            if self.pre_camel_case_notation:
                ret = {'result': 'error',
                       'sError': text,
                       'text': text,
                       'aaData': [],
                       'sEcho': int(self._querydict.get('sEcho', 0)),
                       'iTotalRecords': 0,
                       'iTotalDisplayRecords': 0, }
            else:
                ret = {'error': text,
                       'data': [],
                       'recordsTotal': 0,
                       'recordsFiltered': 0,
                       'draw': int(self._querydict.get('draw', 0))}
        return ret


class FormsetMixin(object):
    formsets = None

    def process_formset(self, formset):
        return formset.save()

    def populate_formset(self, formset_class, instance=None):
        formset = formset_class(
            self.request.POST,
            self.request.FILES,
            instance=instance)
        return formset

    def process_formsets(self):
        for key, formset_class in self.formsets.items():
            formset = self.populate_formset(
                formset_class, self.object)
            formset.is_valid()
            self.process_formset(formset)

    def formsets_all_valid(self, form):
        valid = True
        if self.formsets is not None:
            for key, formset_class in self.formsets.items():
                formset = self.populate_formset(
                    formset_class, form.instance)
                valid = valid and formset.is_valid()
        return valid

    def form_valid(self, form):
        if not self.formsets_all_valid(form):
            return self.form_invalid(form)

        created = self.object is None
        self.object = form.save()

        self.process_formsets()
        
        return HttpResponseRedirect(self.get_success_url())
