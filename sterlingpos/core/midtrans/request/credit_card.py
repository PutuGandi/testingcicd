from ..serializable import Serializable


class CreditCard(Serializable):
    """
    Object representing the customer address
    """

    def __init__(self, **kwargs):
        self.secure = kwargs.get('secure', True)
        self.bank = kwargs.get('bank', 'bca')
        self.channel = kwargs.get('channel', 'migs')

    def __repr__(self):
        return "CreditCard('{} | {} | {}')".format(
            self.bank, self.channel, self.secure)
