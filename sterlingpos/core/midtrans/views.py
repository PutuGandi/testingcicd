import json

from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from sterlingpos.core.midtrans.adapter import midtrans_adapter
from sterlingpos.subscription.models import OrderPayment, Billing


class MidtransNotificationHandlingView(View):

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(
            MidtransNotificationHandlingView, self).dispatch(
                request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        response_body = request.readlines()
        response_json = json.loads(
            response_body[0].decode().replace("'", '"'))

        if not isinstance(response_json, dict):
            response_json = json.loads(response_json)

        if midtrans_adapter.check_signature_key(response_json):
            billing_id = response_json.get('order_id').split('-')[0]

            try:
                payment_attempt = OrderPayment.objects.get(
                    code=response_json.get('order_id'),
                    method=OrderPayment.METHOD.midtrans,)

            except OrderPayment.DoesNotExist:

                billing = Billing.objects.get(pk=billing_id)
                payment_attempt = OrderPayment.objects.create(
                    billing=billing,
                    method=OrderPayment.METHOD.midtrans,
                    code=response_json.get('order_id'),
                    account=billing.account,
                )

            midtrans_adapter.update_payment_attempt_status(payment_attempt)
        return HttpResponse("OK")
