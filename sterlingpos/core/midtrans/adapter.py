import hashlib
import logging

from django.conf import settings

from sterlingpos.subscription.models import Billing, OrderPayment, Voucher
from . import Client, SANDBOX, PRODUCTION, gateway, response, request

if settings.DEBUG or settings.MIDTRANS_DEBUG:
    MIDTRANS_ENVIROMENT_TYPE = SANDBOX
else:
    MIDTRANS_ENVIROMENT_TYPE = PRODUCTION

logger = logging.getLogger(__name__)


class MidtransAdapter(object):
    """docstring for MidtransAdapter"""

    def __init__(self, *args, **kwargs):
        super(MidtransAdapter, self).__init__()
        self.client = Client(
            client_key=settings.MIDTRANS_CLIENT_KEY,
            server_key=settings.MIDTRANS_SERVER_KEY,
            environment_tyoe=MIDTRANS_ENVIROMENT_TYPE
        )
        if MIDTRANS_ENVIROMENT_TYPE == SANDBOX:
            self.snap_url = "https://app.sandbox.midtrans.com/snap/snap.js"
        elif MIDTRANS_ENVIROMENT_TYPE == PRODUCTION:
            self.snap_url = "https://app.midtrans.com/snap/snap.js"

    def get_snap_charge_token(self, billing, order_id, view_request):
        snap = gateway.Snap(client=self.client)
        snap_charge = request.SnapChargeReq(
            order_id=order_id,
            gross_amount=billing.price
        )

        # CC Related
        snap_charge.credit_card["secure"] = True

        # Append item detail
        for sub_journal in billing.subscriptionjournal_set.all():
            price = sub_journal.choosen_plan.price

            if sub_journal.subscription:
                product_name = "{} - {}".format(
                    sub_journal.choosen_plan.name,
                    sub_journal.subscription.outlet.name,
                )
            elif sub_journal.device_user_subscription:
                product_name = "{} - {}".format(
                    sub_journal.choosen_plan.name,
                    sub_journal.device_user_subscription.device_user.name,
                )
            else:
                product_name = "{} - {}".format(
                    sub_journal.choosen_plan.name,
                    sub_journal.account.name,
                )

            item_detail = request.ItemDetail(
                id=sub_journal.choosen_plan.pk,
                price=price,
                quantity=1,
                name=product_name,
            )

            item_detail.price = int(item_detail.price)
            snap_charge.add_item(item_detail)

        # Added customer details
        snap_charge.customer_details = request.CustomerDetails(
            first_name=billing.account.name,
            billing_address=billing.account.address,
            phone=billing.account.phone,
            email=view_request.user.email
        )
        res = snap.get_token(snap_charge)
        
        if not res.token:
            log_data = f"MIDTRANS_SNAP : Failed to fetch snap charge token, reasons : {res.error_messages}"
            logger.info(log_data)

        token = res.token
        return token

    def get_payment_attempt_status(self, payment_attempt):
        core = gateway.Core(client=self.client)
        return core.status(payment_attempt.code)

    def check_signature_key(self, response_json):
        signature_key = response_json.get('signature_key')
        if signature_key:
            token = "{}{}{}{}".format(
                response_json.get('order_id'),
                response_json.get('status_code'),
                response_json.get('gross_amount'),
                settings.MIDTRANS_SERVER_KEY)
            sign_key = hashlib.sha512(token.encode('utf-8')).hexdigest()
            if signature_key == sign_key:
                return True
        return False

    def _get_bank_name(self, payment_attempt_status):
        bank_name = ""
        if payment_attempt_status.payment_type == OrderPayment.PAYMENT_TYPE.gopay:
            bank_name = "Go Pay"
        elif payment_attempt_status.payment_type == OrderPayment.PAYMENT_TYPE.echannel:
            bank_name = "Bank Mandiri"
        elif payment_attempt_status.payment_type == OrderPayment.PAYMENT_TYPE.bank_transfer:
            if payment_attempt_status.permata_va_number:
                bank_name = "Bank Permata"
            else:
                bank_name = payment_attempt_status.va_numbers[0].get("bank")
        return bank_name

    def _get_va_number(self, payment_attempt_status):
        va_number = ""
        if payment_attempt_status.payment_type == OrderPayment.PAYMENT_TYPE.bank_transfer:
            if payment_attempt_status.permata_va_number:
                va_number = payment_attempt_status.permata_va_number
            else:
                va_number = payment_attempt_status.va_numbers[0].get("va_number")
        return va_number

    def _update_payment_attempt_status(self, payment_attempt_status, payment_attempt):
        if (payment_attempt_status
            and payment_attempt_status.status_code != 404):

            if not payment_attempt.transaction_id:
                payment_attempt.transaction_id = payment_attempt_status.transaction_id

            if not payment_attempt.payment_type:
                payment_attempt.payment_type = payment_attempt_status.payment_type

            if payment_attempt_status.payment_amounts:
                if isinstance(payment_attempt_status.payment_amounts, list):
                    payment_attempt.amount = payment_attempt_status.payment_amounts[0].get('amount')
                else:
                    payment_attempt.amount = payment_attempt_status.payment_amounts

            if not payment_attempt.bank_name:
                payment_attempt.bank_name = self._get_bank_name(payment_attempt_status)

            if payment_attempt_status.payment_type == OrderPayment.PAYMENT_TYPE.bank_transfer:
                payment_attempt.va_number = self._get_va_number(payment_attempt_status)
            elif payment_attempt_status.payment_type == OrderPayment.PAYMENT_TYPE.echannel:
                payment_attempt.bill_key = payment_attempt_status.bill_key

            if payment_attempt_status.is_settled:
                payment_attempt.settlement_time = payment_attempt_status.transaction_time
                payment_attempt.status = OrderPayment.STATUS.settled
                payment_attempt.save()
            if payment_attempt_status.transaction_status == 'pending':
                payment_attempt.transaction_time = payment_attempt_status.transaction_time
                payment_attempt.status = OrderPayment.STATUS.paid
                payment_attempt.save()
            if payment_attempt_status.transaction_status == 'expire':
                payment_attempt.status = OrderPayment.STATUS.expired
                payment_attempt.save()
            if payment_attempt_status.transaction_status in ['cancel', 'deny']:
                payment_attempt.cancelation_time = payment_attempt_status.transaction_time
                payment_attempt.status = OrderPayment.STATUS.canceled
                payment_attempt.save()

        return payment_attempt

    def update_payment_attempt_status(self, payment_attempt):
        payment_attempt_status = self.get_payment_attempt_status(payment_attempt)
        payment_attempt = self._update_payment_attempt_status(
            payment_attempt_status, payment_attempt)

        return payment_attempt.status

midtrans_adapter = MidtransAdapter()
