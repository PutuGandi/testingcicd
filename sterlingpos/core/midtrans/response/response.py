from datetime import datetime


class Response(object):

    def __init__(self, *args, **kwargs):

        self.status_code = int(kwargs.get('status_code'))
        if 'transaction_time' in kwargs:
            self.transaction_time = datetime.strptime(
                kwargs.get('transaction_time'), "%Y-%m-%d %H:%M:%S")

        self.status_message = kwargs.get('status_message')
        self.transaction_id = kwargs.get('transaction_id', None)
        self.order_id = kwargs.get('order_id', None)
        self.payment_type = kwargs.get('payment_type', None)
        self.transaction_status = kwargs.get('transaction_status', None)
        self.fraud_status = kwargs.get('fraud_status', None)
        self.gross_amount = int(kwargs.get('gross_amount', "0").split(".")[0])
        self.payment_amounts = kwargs.get('payment_amounts', None)
        self.va_numbers = kwargs.get('va_numbers', None)

        '''
        Additional fields exist on specific payment type
        '''
        self.masked_card = kwargs.get('masked_card', None)
        self.permata_va_number = kwargs.get('permata_va_number', None)
        self.sign_key = kwargs.get('signature_key', None)
        self.card_token = kwargs.get('token_id', None)
        self.saved_card_token = kwargs.get('saved_token_id', None)
        self.saved_token_exp_at = kwargs.get('saved_token_id_expired_at', None)
        self.secure_token = kwargs.get('secure_token', None)
        self.bank = kwargs.get('bank', None)
        self.biller_code = kwargs.get('biller_code', None)
        self.bill_key = kwargs.get('bill_key', None)
        self.xl_tunai_order_id = kwargs.get('xl_tunai_order_id', None)
        self.bii_va_number = kwargs.get('bii_va_number', None)
        self.re_url = kwargs.get('redirect_url', None)
        self.eci = kwargs.get('eci', None)
        self.val_messages = kwargs.get('validation_messages', None)
        self.page = kwargs.get('page', None)
        self.total_page = kwargs.get('total_page', None)
        self.total_record = kwargs.get('total_record', None)

    @property
    def is_settled(self):
        # status code: Should be 200 for successful transactions
        # fraud status: ACCEPT
        # transaction status : settlement/capture

        return (self.fraud_status == 'accept' and
                self.status_code == 200 and
                self.transaction_status in ['capture', 'settlement'])
