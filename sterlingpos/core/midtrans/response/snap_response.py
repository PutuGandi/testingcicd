class SnapResponse(object):

    def __init__(self, *args, **kwargs):

        self.status_code = kwargs.get('status_code', None)
        self.token = kwargs.get('token', None)
        self.error_messages = kwargs.get('error_messages', None)
        self.redirect_url = kwargs.get('redirect_url', None)
