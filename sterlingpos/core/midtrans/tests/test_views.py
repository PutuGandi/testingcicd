import json
import datetime
import hashlib

from test_plus.test import TestCase

from model_mommy import mommy
from django.urls import reverse
from django.conf import settings
from django.test import override_settings, RequestFactory
from django.test.client import Client
from django.utils.translation import ugettext_lazy as _, ugettext

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.core.midtrans.adapter import midtrans_adapter
from sterlingpos.subscription.models import (
    OrderPayment, SubscriptionPlan, Subscription, Billing
)


class TestMidtransNotificationView(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account-1')
        self.user = mommy.make(
            'users.User', email='test@email.com', account=self.account)
        self.outlet = mommy.make('outlet.Outlet', branch_id='Branch-1', account=self.account)
        self.plan = mommy.make(
            SubscriptionPlan,
            recurrence_period=1,
            recurrence_unit='M')

        self.subscription = mommy.make(
            Subscription,
            account=self.account,
            outlet=self.outlet,
            subscription_plan=self.plan,
            expires=datetime.date.today(),
        )
        self.billing = Billing.objects.create(account=self.account)
        self.billing.subscriptions.add(self.subscription)
        self.sign_key = self.build_signature_key(
            "{}-31020181557".format(self.billing.pk), '200', '100000.00')

    def build_signature_key(self, order_id, status_code, gross_amount):
        token = "{}{}{}{}".format(
                order_id,
                status_code,
                gross_amount,
                settings.MIDTRANS_SERVER_KEY)
        sign_key = hashlib.sha512(token.encode('utf-8')).hexdigest()
        return sign_key

    def tearDown(self):
        set_current_tenant(None)

    def test_midtrans_notification_view(self):
        payment_attempt = OrderPayment.objects.create(
            account=self.account,
            code='{}-31020181557'.format(self.billing.pk),
            billing=self.billing,
            amount=self.billing.price,
        )
        response = self.client.post(path='/midtrans/notification/handling/',
                                    data={
                                        "transaction_time": "2018-10-03 15:57:59",
                                        "transaction_status": "settlement",
                                        "transaction_id": "baa955c9-534f-49f5-b36e-1b61d0ec4b42",
                                        "status_message": "midtrans payment notification",
                                        "status_code": "200",
                                        "signature_key": "ca0b6e363e9ce72d618017f28d521d98849438e58723a86e64da1eff4036ed6aca0fbbfa9600c21fd4b0706634b1e80b0d086ab0db21d885efbce09f857c087d",
                                        "permata_va_number": "586007028402806",
                                        "payment_type": "bank_transfer",
                                        "order_id": "{}-31020181557".format(self.billing.pk),
                                        "gross_amount": "100000.00"},
                                    content_type='application/json')
        self.assertTrue(response.status_code, 200)

    def test_midtrans_notification_view_denied_card(self):
        response = self.client.post(path='/midtrans/notification/handling/',
                                    data={
                                        "transaction_time": "2018-10-03 15:57:59",
                                        "transaction_status": "deny",
                                        "transaction_id": "baa955c9-534f-49f5-b36e-1b61d0ec4b42",
                                        "status_message": "midtrans payment notification",
                                        "status_code": "200",
                                        "signature_key": self.sign_key,
                                        "permata_va_number": "586007028402806",
                                        "payment_type": "bank_transfer",
                                        "order_id": "{}-31020181557".format(self.billing.pk),
                                        "gross_amount": "100000.00"},
                                    content_type='application/json')
        self.assertTrue(response.status_code, 200)

    def test_midtrans_adapter_denied_signature_key(self):
        data = {
            "transaction_time": "2018-10-03 15:57:59",
            "transaction_status": "deny",
            "transaction_id": "baa955c9-534f-49f5-b36e-1b61d0ec4b42",
            "status_message": "midtrans payment notification",
            "status_code": "200",
            "signature_key": "bad-key",
            "permata_va_number": "586007028402806",
            "payment_type": "bank_transfer",
            "order_id": "{}-31020181557".format(self.billing.pk),
            "gross_amount": "100000.00"}
        self.assertFalse(midtrans_adapter.check_signature_key(data))

    def test_midtrans_notification_view_with_non_existing_payment_attempt(self):
        response = self.client.post(path='/midtrans/notification/handling/',
                                    data={
                                        "transaction_time": "2018-10-03 15:57:59",
                                        "transaction_status": "settlement",
                                        "transaction_id": "baa955c9-534f-49f5-b36e-1b61d0ec4b42",
                                        "status_message": "midtrans payment notification",
                                        "status_code": "200",
                                        "signature_key": self.sign_key,
                                        "permata_va_number": "586007028402806",
                                        "payment_type": "bank_transfer",
                                        "order_id": "{}-31020181557".format(self.billing.pk),
                                        "gross_amount": "100000.00"},
                                    content_type='application/json')
        self.assertTrue(response.status_code, 200)

    def test_midtrans_notification_view_bni(self):
        payment_attempt = OrderPayment.objects.create(
            account=self.account,
            code='{}-31020181557'.format(self.billing.pk),
            billing=self.billing,
            amount=self.billing.price,
        )
        response = self.client.post(path='/midtrans/notification/handling/',
                                    data={
                                    'currency': 'IDR', 
                                    'fraud_status': 'accept', 
                                    'gross_amount': '100000.00', 
                                    'order_id': '{}-31020181557'.format(self.billing.pk),
                                    'payment_amounts': [{
                                        'amount': '100000.00', 
                                        'paid_at': '2019-07-09 23:36:51'
                                    }], 
                                    'payment_type': 'bank_transfer', 
                                    'settlement_time': '2019-07-09 23:37:20', 
                                    'signature_key': self.sign_key,                                    
                                    'status_code': '200', 
                                    'status_message': 'midtrans payment notification', 
                                    'transaction_id': 'baa955c9-534f-49f5-b36e-1b61d0ec4b42',
                                    'transaction_status': 'settlement', 
                                    'transaction_time': '2019-07-09 23:34:42', 
                                    'va_numbers': [{
                                        'bank': 'bni', 
                                        'va_number': '8578519787656428'
                                    }]
                                    },
                                    content_type='application/json')
        self.assertTrue(response.status_code, 200)