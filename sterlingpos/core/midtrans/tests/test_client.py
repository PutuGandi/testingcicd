from django.test import TestCase
from sterlingpos.core.midtrans import midtrans


class TestEnvironmentType(TestCase):

    def test_environment_type(self):
        self.assertTrue(midtrans.SANDBOX.envname == "SANDBOX")
        self.assertTrue(midtrans.SANDBOX.api_url == "https://api.sandbox.midtrans.com")
        self.assertTrue(midtrans.SANDBOX.app_url == "https://app.sandbox.midtrans.com")

        self.assertTrue(midtrans.PRODUCTION.envname == "PRODUCTION")
        self.assertTrue(midtrans.PRODUCTION.api_url == "https://api.midtrans.com")
        self.assertTrue(midtrans.PRODUCTION.app_url == "https://app.midtrans.com")
