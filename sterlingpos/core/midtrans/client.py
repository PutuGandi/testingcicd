from .envtype import SANDBOX
import json
import requests
import logging

logger = logging.getLogger(__name__)


class Client:
    """
    A basic driver
    """

    def __init__(self,
                 client_key,
                 server_key,
                 environment_tyoe=SANDBOX):

        self.environment_type = environment_tyoe
        self.client_key = client_key
        self.server_key = server_key

    def call(self, method, full_url, parameters=dict()):
        payload = json.dumps(parameters)
        headers = {
            'content-type': 'application/json',
            'accept': 'application/json'
        }

        response = requests.request(
            method,
            full_url,
            auth=(self.server_key, ''),
            data=payload,
            headers=headers,
            allow_redirects=True,
            verify=False if self.environment_type == SANDBOX else True,
        )

        try:
            json_response = response.json()
            return json_response
        except Exception as exc:
            log_data = "Midtrans Get Token Aborted method={}, url={}, payload={}, response={}, exception={}".format(
                method,
                full_url,
                payload,
                response.__dict__,
                exc,
            )
            logger.error(log_data)
            json_error_response = {
                "status_code": response.status_code,
                "error_messages": "Unable to process billings.",
            }
            return json_error_response

    def __repr__(self):
        return ("<Midtrans.Client({0})>".format(self.environment_type.envname))
