from django.core.paginator import Paginator


class LastSeenPaginator(Paginator):
    """
    This pagination utilize last pk from previous pagination as filter instead of offset 
    """
    def get_last_seen_from(self, last_seen=0):
        try:
            return self.object_list.filter(
                pk__gt=last_seen
            )[:self.per_page][self.per_page-1].pk
        except:
            pass

    def has_next(self, last_seen=0):
        return self.object_list.filter(
                pk__gt=last_seen).exists()

    def page(self, last_seen):
        return self._get_page(
            self.object_list.filter(
                pk__gt=last_seen)[:self.per_page],
            last_seen, self)
