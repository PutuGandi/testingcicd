from django.template.defaulttags import register
from django.contrib.humanize.templatetags.humanize import intcomma

from decimal import Decimal


@register.filter
def get_item(dictionary, key):
    return dictionary[key]


@register.simple_tag
def get_normalized_decimal(number):
    number = Decimal(number)
    frac = number % 1
    if frac == Decimal("0"):
        result = Decimal(number.quantize(Decimal("1")))
        return intcomma(str(result))
    else:
        number = Decimal(number.quantize(Decimal(".001")))
        result = number.normalize()
        decimal_num = str(result).split(".")
        place_val = intcomma(decimal_num[0])
        try:
            result = "{},{}".format(place_val, decimal_num[1])
        except IndexError:
            result = "{}".format(place_val)
        return result
