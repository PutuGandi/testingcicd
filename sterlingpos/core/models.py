from django.db import models
from safedelete.models import SafeDeleteModel, SOFT_DELETE_CASCADE
from safedelete.queryset import SafeDeleteQueryset
from safedelete.managers import (
    SafeDeleteManager,
    SafeDeleteAllManager,
    SafeDeleteDeletedManager
)

from django_multitenant.fields import TenantForeignKey, TenantOneToOneField
from django_multitenant.models import TenantQuerySet, TenantModel, TenantManager
from django_multitenant.utils import (
    get_current_user,
    set_current_tenant, get_current_tenant
)


class SterlingTenantQuerySet(SafeDeleteQueryset, TenantQuerySet):
    pass


class SterlingTenantManager(
        SterlingTenantQuerySet.as_manager().__class__, TenantManager, SafeDeleteManager):

    def get_queryset(self):
        queryset = super(SterlingTenantManager, self).get_queryset()
        return queryset


class SterlingSafeDeleteAllManager(
        SterlingTenantQuerySet.as_manager().__class__, TenantManager, SafeDeleteAllManager):
    pass


class SterlingSafeDeleteDeletedManager(
        SterlingTenantQuerySet.as_manager().__class__, TenantManager, SafeDeleteDeletedManager):
    pass


class SterlingTenantForeignKey(TenantForeignKey):
    pass


class SterlingTenantOneToOneField(TenantOneToOneField):
    pass


class SterlingTenantModel(SafeDeleteModel, TenantModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    account = models.ForeignKey(
        'accounts.Account', on_delete=models.CASCADE)

    tenant_id = 'account_id'
    objects = SterlingTenantManager()
    all_objects = SterlingSafeDeleteAllManager()
    deleted_objects = SterlingSafeDeleteDeletedManager()

    class Meta:
        abstract = True
        unique_together = (('account', 'id'),)

    def save(self, *args, **kwargs):
        current_tenant = get_current_tenant()

        if not self.pk and current_tenant:
            self.account = current_tenant

        if not getattr(self, 'account', None) and current_tenant:
            self.account = current_tenant

        return super(SterlingTenantModel, self).save(*args, **kwargs)

    def _do_update(self, base_qs, using, pk_val, values, update_fields, forced_update):
        # Do not filter when saving/updating parent model incase of inheritance
        if isinstance(self, base_qs.model) and not hasattr(base_qs.model, 'account'):
            return super(TenantModel, self)._do_update(base_qs, using, pk_val, values, update_fields, forced_update)
        return super(SterlingTenantModel, self)._do_update(base_qs, using, pk_val, values, update_fields, forced_update)
