from django.contrib import admin

from .models import (
    CashBalance,
    CashBalanceLog,
    BalanceDetail,
)

class BalanceDetailInline(admin.TabularInline):
    model = BalanceDetail


class CashBalanceLogAdmin(admin.ModelAdmin):
    """
        Admin View for CashBalanceLog
    """
    list_display = (
        "device_user", "outlet", "status", "transaction_date",
        "balance", "description", "account")
    list_filter = ("status", "transaction_date")
    search_fields = (
        "device_user__name", "outlet__name",
        "account__name", "description")


class CashBalanceAdmin(admin.ModelAdmin):
    """
        Admin View for CashBalance
    """
    list_display = (
        "device_user", "outlet", "status", "start_date", "end_date",
        "opening_balance", "closing_balance", "accumulated_balance",
        "account")
    list_filter = ("status", "start_date")
    search_fields = ("device_user__name", "outlet__name", "account__name")
    inlines = (
        BalanceDetailInline,
    )

admin.site.register(CashBalanceLog, CashBalanceLogAdmin)
admin.site.register(CashBalance, CashBalanceAdmin)
