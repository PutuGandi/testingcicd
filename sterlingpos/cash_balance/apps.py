from django.apps import AppConfig
from django.db.models.signals import post_save


class CashBalanceConfig(AppConfig):
    name = "sterlingpos.cash_balance"
    verbose_name = "CashBalance"

    def ready(self):
        from .models import CashBalanceLog
        from .signals import (
            update_cash_balance_handler
        )
        post_save.connect(update_cash_balance_handler, sender=CashBalanceLog)
