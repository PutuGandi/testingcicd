from django.conf import settings
from django.dispatch import Signal
from django.db.models.functions import Coalesce
from django.db.models import Sum, F, Case, DecimalField, Value, When
from sterlingpos.cash_balance.models import CashBalance, CashBalanceLog


def update_cash_balance_handler(sender, **kwargs):

    instance = kwargs.get('instance')
    created = kwargs.get('created')

    if created and instance.status == CashBalanceLog.STATUS.shift_start:
        cash_balance = CashBalance(
            device_user=instance.device_user,
            outlet=instance.outlet,
            start_date=instance.transaction_date,
            opening_balance=instance.balance,
            account=instance.account
        )
        cash_balance.save()

    if created and instance.status == CashBalanceLog.STATUS.shift_end:
        cash_balance = CashBalance.all_objects.filter(end_date=None, device_user=instance.device_user).last()

        accumulated_balance = instance.device_user.cashbalancelog_set.filter(
            transaction_date__gte=cash_balance.start_date,
            transaction_date__lte=instance.transaction_date,
            status__in=[CashBalanceLog.STATUS.cash_in, CashBalanceLog.STATUS.cash_out]
        ).annotate(
            credit_debit=Case(
                When(status=CashBalanceLog.STATUS.cash_out, then=F('balance') * (-1)),
                default=F('balance'),
                output_field=DecimalField(),
            )
        ).aggregate(
            credit_debit_sum=Coalesce(
                Sum('credit_debit', output_field=DecimalField()), Value(0))
        ).get('credit_debit_sum', 0)

        sales_order_qs = instance.outlet.salesorder_set.filter(
            transaction_date__gte=cash_balance.start_date,
            transaction_date__lte=instance.transaction_date,
            status="settled",
            cashier_name=instance.device_user.name,
        )

        transaction_balance = sales_order_qs.aggregate(
            total_cost_sum=Coalesce(
                Sum('total_cost', output_field=DecimalField()), Value(0))
        ).get('total_cost_sum', 0)
        transaction_count = sales_order_qs.count()

        cash_balance.accumulated_balance = accumulated_balance
        cash_balance.transaction_balance = transaction_balance
        cash_balance.transaction_count = transaction_count

        cash_balance.end_date = instance.transaction_date
        cash_balance.closing_balance = instance.balance
        cash_balance.status = CashBalance.STATUS.closed
        cash_balance.save()
