import pytz

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta, MO, SU
from django.db.models.functions import Coalesce
from django.db.models import (
    ExpressionWrapper, Subquery, OuterRef, Count, Sum, F, Q, Case, Value, When,
    DurationField,
    CharField, DecimalField, 
)
from sterlingpos.cash_balance.models import CashBalance, CashBalanceLog

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _, ugettext
from django.contrib.humanize.templatetags.humanize import intcomma
from django.shortcuts import redirect

from django.views.generic import (
    ListView, DetailView, View, UpdateView,
    TemplateView
)

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import JSONResponseMixin, AjaxResponseMixin

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.outlet.models import Outlet
from sterlingpos.sales.models import SalesOrder, PaymentMethod
from .forms import CashBalanceSettingForm, EnableCashBalanceSettingForm, CashBalanceOutletSettingForm
from .models import (
    CashBalanceSetting, CashBalance, CashBalanceLog
)


class DateMixins(object):
    
    def get_date_from_request(self):
        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = None
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = None

        return start_date, end_date
 

class CashBalanceListJson(
        SterlingRoleMixin, DatatableMixins,
        CSVExportMixin, DateMixins, BaseDatatableView):

    columns = [
        'device_user.name', 'outlet.name',
        'start_date', 'end_date',
        'opening_balance.amount', 'income_balance', 'closing_balance.amount',
        'difference']
    order_columns = [
        'device_user__name', 'outlet__name',
        'start_date', 'end_date',
        'opening_balance', 'income_balance', 'closing_balance',
        'difference']
    exclude_col = ['start_date', 'end_date']
    csv_header = [
        'User', 'Outlet',
        'Start Date', 'End Date',
        'Opening Balance', 'Income Balance', 'Closing Balance',
        'Difference']
    csv_columns = [
        'device_user.name', 'outlet.name',
        'start_date', 'end_date',
        'opening_balance.amount', 'income_balance', 'closing_balance.amount',
        'difference']
    model = CashBalance

    def filter_outlet__name(self, search_value):
        return Q(outlet=search_value)

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.exclude(
            end_date__isnull=True
        ).select_related('device_user', 'outlet')

        outlet_pk = self.kwargs.get("outlet_pk", None)
        if outlet_pk:
            qs = qs.filter(outlet=outlet_pk)

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        qs = qs.annotate(

            income_balance=Coalesce(
                F('opening_balance') + F('accumulated_balance') + F('transaction_balance'),
                Value(0),
                output_field=DecimalField(),
            ),

            difference=Coalesce(
                F('closing_balance') - (
                    F('opening_balance') + F('accumulated_balance') + F('transaction_balance')),
                Value(0),
                output_field=DecimalField(),
            ), 
        )

        start_date, end_date = self.get_date_from_request()

        if start_date:
            qs = qs.filter(start_date__gte=start_date)
        if end_date:
            qs = qs.filter(start_date__lte=end_date)
            qs = qs.filter(
                Q(end_date__lte=end_date)
            )
        return qs

    def render_column(self, obj, column):
        if column == "device_user.name":
            return (
                reverse('cash_balance:detail', kwargs={"pk": obj.pk}),
                escape(obj.device_user.name)
            )
        return super().render_column(obj, column)

    def get_csv_filename(self):
        csv_filename = "Cash_Balance"

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename


class CashBalanceActiveListJson(SterlingRoleMixin, DatatableMixins, DateMixins, BaseDatatableView):
    columns = [
        'device_user.name', 'outlet.name',
        'start_date',
        'opening_balance.amount', 'income_balance', 'closing_balance.amount',
        'difference']
    order_columns = [
        'device_user__name', 'outlet__name',
        'start_date',
        'opening_balance', 'income_balance', 'closing_balance',
        'difference']
    exclude_col = ['start_date',]

    model = CashBalance

    def filter_outlet__name(self, search_value):
        return Q(outlet=search_value)

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.annotate(
            delta=ExpressionWrapper(
                timezone.now() - F("start_date"),
                output_field=DurationField())
        ).filter(
            end_date__isnull=True,
            delta__lt=timedelta(days=5)
        ).select_related('device_user', 'outlet')

        outlet_pk = self.kwargs.get("outlet_pk", None)
        if outlet_pk:
            qs = qs.filter(outlet=outlet_pk)

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        qs = qs.annotate(
            cash_in_balance=Coalesce(
                Sum(
                    F("device_user__cashbalancelog__balance"),
                    filter=(
                        Q(device_user__cashbalancelog__status=CashBalanceLog.STATUS.cash_in) and
                        Q(device_user__cashbalancelog__transaction_date__gte=F("start_date"))
                    ),
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            cash_out_balance=Coalesce(
                Sum(
                    F("device_user__cashbalancelog__balance"),
                    filter=(
                        Q(device_user__cashbalancelog__status=CashBalanceLog.STATUS.cash_out) and
                        Q(device_user__cashbalancelog__transaction_date__gte=F("start_date"))
                    ),
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            accumulated_balance_temp=Coalesce(
                F("cash_in_balance") - F("cash_out_balance"),
                Value(0)
            ),
            transaction_balance_temp=Case(
                When(
                    end_date__isnull=True, 
                    then=Subquery(
                        SalesOrder.all_objects.filter(
                            status="settled",
                            transaction_date__gte=OuterRef("start_date"),
                            outlet=OuterRef("outlet"),
                        ).order_by().values(
                            "outlet"
                        ).annotate(
                            total=Sum('total_cost')
                        ).values('total'),
                        output_field=DecimalField()
                    )
                ),
                default=F('transaction_balance'),
                output_field=DecimalField(),
            ),
            income_balance=Coalesce(
                F('opening_balance') + F('accumulated_balance_temp') + F('transaction_balance_temp'),
                Value(0),
                output_field=DecimalField(),
            ),
            difference=Coalesce(
                F('closing_balance') - (
                    F('opening_balance') + F('accumulated_balance_temp') + F('transaction_balance_temp')),
                Value(0),
                output_field=DecimalField(),
            ),
        )

        start_date, _ = self.get_date_from_request()

        if start_date:
            qs = qs.filter(start_date__gte=start_date)

        return qs

    def render_column(self, obj, column):
        if column == "device_user.name":
            return (
                reverse('cash_balance:detail', kwargs={"pk": obj.pk}),
                escape(obj.device_user.name)
            )
        return super().render_column(obj, column)


class CashBalanceListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/cash_balance/cash_balance_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        active_qs = CashBalance.objects.annotate(
            delta=ExpressionWrapper(
                timezone.now() - F("start_date"),
                output_field=DurationField())
        ).filter(
            end_date__isnull=True,
            delta__lt=timedelta(days=5)
        )

        if not self.request.user.is_owner:
            active_qs = active_qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        context["active_cash_balances"] = active_qs
        context["has_cash_balance"] = self.request.user.account.cashbalancesetting_set.filter(is_enabled=True).exists
        return context


class CashBalanceActiveListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/cash_balance/cash_balance_active_list.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.account.cashbalancesetting_set.filter(is_enabled=True).exists():
            return super().dispatch(request, *args, **kwargs)
        else:
            return redirect(reverse_lazy("cash_balance:settings_disable"))


class CashBalanceDetailView(SterlingRoleMixin, DetailView):
    template_name = 'dashboard/cash_balance/cash_balance_detail.html'
    model = CashBalance

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_owner:
            queryset = queryset.filter(
                outlet__in=get_outlet_of_user(self.request.user))
        return queryset

    def get_context_data(self, **kwargs):
        context = super(CashBalanceDetailView, self).get_context_data(**kwargs)
        qs = CashBalanceLog.all_objects.filter(
            outlet=self.object.outlet,
            device_user=self.object.device_user,
            transaction_date__gte=self.object.start_date,
            status__in=['cash_in', 'cash_out'],
        )

        if self.object.end_date:
            qs = qs.filter(transaction_date__lte=self.object.end_date)

            voided_sales_order_qs = self.object.outlet.salesorder_set.filter(
                transaction_date__gte=self.object.start_date,
                transaction_date__lte=self.object.end_date,
                status="unsettled",
                cashier_name=self.object.device_user.name,
            )
            total_voided_transaction = voided_sales_order_qs.aggregate(
                total_voided_transaction=Sum('total_cost')).get('total_voided_transaction', 0)

            payment_list = PaymentMethod.objects.payment_sales(
                self.object.start_date, self.object.end_date,
                self.object.outlet, exclude_zero_sales=True
            )
            context.update({
                'voided_sales_order': voided_sales_order_qs,
                'total_voided_transaction': total_voided_transaction,
                'payment_list': payment_list,
            })

        context.update({
            'object_list': qs,
        })
        return context


class CashBalanceDetailJSON(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        try:
            data_pk = request.GET.get('id', '')
            cash_balance = CashBalance.all_objects.get(pk=data_pk)
            qs = CashBalanceLog.all_objects.filter(
                outlet=cash_balance.outlet,
                device_user=cash_balance.device_user,
                transaction_date__gte=cash_balance.start_date,
            )

            if cash_balance.end_date:
                qs = qs.filter(
                    transaction_date__lte=cash_balance.end_date,
                )

            cash_in_data = []
            cash_out_data = []
            cash_in = qs.filter(status=CashBalanceLog.STATUS.cash_in)
            for cash in cash_in:
                cash_in_data.append({'description': cash.description, 'balance': intcomma(int(cash.balance.amount))})

            total_cash_in = cash_in.aggregate(Sum('balance'))

            cash_out = qs.filter(status=CashBalanceLog.STATUS.cash_out)
            for cash in cash_out:
                cash_out_data.append({'description': cash.description, 'balance': intcomma(int(cash.balance.amount))})

            total_cash_out = cash_out.aggregate(Sum('balance'))

            total_in = cash_balance.opening_balance.amount
            if total_cash_in['balance__sum']:
                total_in += total_cash_in['balance__sum']

            total_out = 0
            if total_cash_out['balance__sum']:
                total_out = total_cash_out['balance__sum']

            cash_balance_in_system = (int(cash_balance.transaction_balance.amount) + int(total_in)) - int(total_out)
            differece_balance = int(cash_balance.closing_balance.amount) - cash_balance_in_system
            balance_list = [
                {
                    "payment_name": balance_detail.get_payment_name_display(),
                    "balance": intcomma(int(balance_detail.balance.amount))
                }
                for balance_detail in cash_balance.balance_list.all()
            ]

            cash_balance_data = {
                'device': cash_balance.device_user.name,
                'transaction': intcomma(int(cash_balance.transaction_balance.amount)),
                'transaction_count': cash_balance.transaction_count,
                'start_date': datetime.strftime(cash_balance.start_date.astimezone(pytz.timezone('Asia/Jakarta')),
                                                "%d/%m/%Y %H:%M:%S"),
                'cash_balance_in_system': intcomma(int(cash_balance_in_system)),
                'total_cash_in': intcomma(int(total_in)),
                'total_cash_out': intcomma(int(total_out)),
                'cash_in': cash_in_data,
                'cash_out': cash_out_data,
                'starting_cash_in': intcomma(int(cash_balance.opening_balance.amount)),
                'actual_balance': intcomma(int(cash_balance.closing_balance.amount)),
                'differece_balance': intcomma(int(differece_balance)),
                'balance_list': balance_list,
            }

            if cash_balance.end_date:
                cash_balance_data.update({
                    'end_date': datetime.strftime(cash_balance.end_date.astimezone(pytz.timezone('Asia/Jakarta')),
                                                  "%d/%m/%Y %H:%M:%S"),
                })

            status_code = 200
            response['status'] = 'success'
            response['data'] = cash_balance_data
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CashBalanceSettingUpdateView(SterlingRoleMixin, UpdateView):
    model = CashBalanceSetting
    template_name = "dashboard/cash_balance/settings.html"
    form_class = CashBalanceSettingForm

    def get_success_url(self):
        success_url = reverse("cash_balance:settings")
        return success_url

    def get_object(self, queryset=None):
        obj = self.request.user.account.cashbalancesetting_set.first()
        return obj

    def get_initial(self):
        outlet = self.object.outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(outlet)

        initial = {
            'all_outlet': disabled_outlet.count() == 0,
            'outlet': outlet
        }
        return initial

    def form_valid(self, form):
        response = super(CashBalanceSettingUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Cash Balance is successfully updated.')))
        return response


class CashBalanceOutletSettingsUpdateView(SterlingRoleMixin, UpdateView):
    model = CashBalanceSetting
    template_name = 'dashboard/cash_balance/outlet_settings.html'
    form_class = CashBalanceOutletSettingForm

    def get_object(self, queryset=None):
        obj = self.request.user.account.cashbalancesetting_set.first()
        return obj

    def get_success_url(self):
        success_url = reverse('cash_balance:settings_outlet')
        return success_url

    def get_initial(self):
        enabled_outlet = self.object.outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(enabled_outlet)
        initial = {
            'all_outlet': disabled_outlet.count() == 0,
        }
        return initial

    def form_valid(self, form):
        form.save()
        messages.success(self.request, '{}'.format(
            _('Cash Balance has been updated.')))
        return HttpResponseRedirect(self.get_success_url())


class CashBalanceEnableUpdateViewJSON(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_object(self):
        obj = self.request.user.account.cashbalancesetting_set.first()
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }

        form = EnableCashBalanceSettingForm(
            data=request.POST,
            instance=self.get_object()
        )

        if form.is_valid():
            cash_balance = form.save()
            status_code = 200
            response['status'] = 'success'
            response['data'] = cash_balance.is_enabled
        else:
            response['data'] = form.errors
            status_code = 400

        return self.render_json_response(response, status=status_code)


class CashBalanceDisableView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/cash_balance/disable.html'
