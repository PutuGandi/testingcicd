from django import forms
from django.template.loader import render_to_string

from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from sterlingpos.outlet.models import Outlet

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, HTML
from cacheops import invalidate_obj

from .models import CashBalanceSetting, CashBalanceOutlet
from django.utils.translation import ugettext_lazy as _


class EnableCashBalanceSettingForm(forms.ModelForm):
    class Meta:
        model = CashBalanceSetting
        fields = ('is_enabled',)
        exclude = ('account', 'summary_on_closing', 'report_fields')

    def __init__(self, *args, **kwargs):
        super(EnableCashBalanceSettingForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            KawnField('is_enabled', template='dashboard/table_management/switch_input/switch.html')
        )


class CashBalanceSettingForm(forms.ModelForm):
    all_outlet = forms.BooleanField(initial=True, label="Semua Lokasi")
    report_fields = forms.MultipleChoiceField(
        choices=CashBalanceSetting._REPORT_FIELDS,
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )

    class Meta:
        model = CashBalanceSetting
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        super(CashBalanceSettingForm, self).__init__(*args, **kwargs)
        self.fields['outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['outlet'].label = ''
        self.fields['all_outlet'].required = False
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = 'col-12 p-0'
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "Laporan Rekap Kas",
                HTML('<label class="col-form-label"> Pilih field yang ingin di tampilkan.</label>'),
                KawnField('report_fields', template='dashboard/table_management/switch_input/multiple_switch.html'),
            ),
            KawnField('all_outlet', type='hidden'),
            KawnField('outlet', type='hidden')

        )

    def update_m2m(self, cash_balance, object_list, rel_field, intermediate_model):

        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "cash_balance_setting": cash_balance,
                    "account": cash_balance.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        obj = super(CashBalanceSettingForm, self).save(*args, **kwargs)
        self.update_m2m(
            obj, outlets, obj.outlet, CashBalanceOutlet)
        return obj


class CashBalanceOutletSettingForm(forms.ModelForm):
    all_outlet = forms.BooleanField(initial=True, label='Semua Lokasi')
    report_fields = forms.MultipleChoiceField(
        choices=CashBalanceSetting._REPORT_FIELDS,
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )

    class Meta:
        model = CashBalanceSetting
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        super(CashBalanceOutletSettingForm, self).__init__(*args, **kwargs)
        self.fields['outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['outlet'].label = ''
        self.fields['all_outlet'].required = False

        self.fields['all_outlet'].help_text = _(
            '( Jika centang ini dihapus, manajemen shift ini tidak berlaku pada gerai yang baru. )'
        )
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = 'col-12 p-0'
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            KawnField('report_fields',type='hidden'),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet', template='dashboard/table_management/switch_input/float_right_switch.html'),
                KawnField('outlet', template='dashboard/table_management/switch_input/multiple_switch.html'),
                help_text=render_to_string('help_text/cash_balance_outlet.html')
            ),
        )

    def update_m2m(self, cash_balance, object_list, rel_field, intermediate_model):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "cash_balance_setting": cash_balance,
                    "account": cash_balance.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        obj = super(CashBalanceOutletSettingForm, self).save(*args, **kwargs)
        self.update_m2m(
            obj, outlets, obj.outlet, CashBalanceOutlet)
        return obj
