# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "cash_balance"
urlpatterns = [
    url(regex=r'^$', view=views.CashBalanceListView.as_view(),
        name='list'),
    url(regex=r'^outlet/(?P<outlet_pk>\d+)/$', view=views.CashBalanceListView.as_view(),
        name='list'),

    url(regex=r'^active/$', view=views.CashBalanceActiveListView.as_view(),
        name='active_list'),
    url(regex=r'^active/outlet/(?P<outlet_pk>\d+)/$', view=views.CashBalanceActiveListView.as_view(),
        name='active_list'),

    url(regex=r'^(?P<pk>\d+)/$', view=views.CashBalanceDetailView.as_view(),
        name='detail'),

    url(regex=r'^settings/$', view=views.CashBalanceSettingUpdateView.as_view(),
        name='settings'),
    url(r'^settings/enable/$', view=views.CashBalanceEnableUpdateViewJSON.as_view(),
        name='settings_enable'),
    url(regex=r'^settings/disable/$', view=views.CashBalanceDisableView.as_view(),
        name='settings_disable'),
    url(regex=r'^settings/outlet/$', view=views.CashBalanceOutletSettingsUpdateView.as_view(),
        name='settings_outlet'),

    url(regex=r'^data/outlet/(?P<outlet_pk>\d+)/$', view=views.CashBalanceListJson.as_view(),
        name='data_cash_balance'),
    url(regex=r'^data/$', view=views.CashBalanceListJson.as_view(),
        name='data_cash_balance'),

    url(regex=r'^active/data/outlet/(?P<outlet_pk>\d+)/$', view=views.CashBalanceActiveListJson.as_view(),
        name='data_active_cash_balance'),
    url(regex=r'^active/data/$', view=views.CashBalanceActiveListJson.as_view(),
        name='data_active_cash_balance'),
        
]
