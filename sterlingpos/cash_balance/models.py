from django.conf import settings
from django.db import models, DataError
from django.db.models.functions import Coalesce
from django.db.models import Sum, F, Case, DecimalField, Value, When

from django.contrib.postgres.fields import ArrayField
from django.utils.translation import ugettext_lazy as _, ugettext

from djmoney.money import Money
from djmoney.models.fields import MoneyField
from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.core.models import (
    SterlingTenantModel, SterlingTenantForeignKey,
)


class CashBalanceSetting(SterlingTenantModel, TimeStampedModel):

    _REPORT_FIELDS = Choices(
        ('user', _('User')),
        ('opening_balance', _('Kas Awal')),
        ('total_sales', _('Total Sales')),
        ('total_sales_void', _('Total Sales Void')),
        ('cash_in', _('Kas Masuk')),
        ('cash_out', _('Kas Keluar')),
        ('balance_expectation', _('Ekspektasi Kas')),
        ('balance_difference', _('Selisih')),

        ('debit_balance', _('Kartu Debit')),
        ('credit_balance', _('Kartu Kredit')),
        ('gopay', _('GO-PAY')),
        ('ovo', _('OVO')),
        ('goresto', _('GO-RESTO')),
        ('grab', _('GRAB')),
        ('dana', _('DANA')),
        ('shopeepay', _('ShopeePay')),
    )

    is_enabled = models.BooleanField(default=False)
    summary_on_closing = models.BooleanField(default=True)

    report_fields = ArrayField(
        models.CharField(
            max_length=255,
            choices=_REPORT_FIELDS,
        ),
        default=[
            "user", "opening_balance", "total_sales", "cash_in", "cash_out",
            "balance_expectation", "balance_difference",
            "debit_balance", "credit_balance", "gopay", "ovo"],
        blank=True,
        null=True,
    )

    outlet = models.ManyToManyField(
        "outlet.Outlet", blank=True,
        through='CashBalanceOutlet',
        related_name='cash_balance_setting')

    class Meta:
        verbose_name = 'CashBalanceSetting'
        verbose_name_plural = 'CashBalanceSettings'
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "Cash Balance Settings for {}".format(account)


class CashBalanceOutlet(SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    cash_balance_setting = models.ForeignKey(
        CashBalanceSetting, on_delete=models.CASCADE)
    outlet = models.ForeignKey("outlet.Outlet", on_delete=models.CASCADE)


class CashBalance(SterlingTenantModel, TimeStampedModel):

    STATUS = Choices(
        ('open', _('Shift Open')),
        ('closed', _('Shift Closed')),
    )

    status = models.CharField(_("Status"), max_length=15, choices=STATUS, default=STATUS.open)
    start_date = models.DateTimeField(_("Shift Start"), db_index=True)
    end_date = models.DateTimeField(_("Shift End"), blank=True, null=True, db_index=True)

    opening_balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')
    closing_balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')

    accumulated_balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')
    transaction_balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')
    transaction_count = models.PositiveIntegerField(default=0)

    device_user = SterlingTenantForeignKey('device.DeviceUser', on_delete=models.CASCADE)

    outlet = SterlingTenantForeignKey('outlet.Outlet', on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = "CashBalance"
        verbose_name_plural = "CashBalances"
        unique_together = (('account', 'id'),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        outlet_name = self.outlet.name
        device_user_name = self.device_user.name

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)
        return "Cash Balance {} of {} - {} | {}, by {}".format(
            self.get_status_display(), outlet_name,
            self.opening_balance.amount,
            self.closing_balance.amount,
            device_user_name)

    def temp_transactions(self):
        if not self.end_date:
            sales_order_qs = self.outlet.salesorder_set.filter(
                transaction_date__gte=self.start_date,
                status="settled",
            )
        else:
            sales_order_qs = self.outlet.salesorder_set.filter(
                transaction_date__gte=self.start_date,
                transaction_date__lte=self.end_date,
                status="settled",
            )

        return sales_order_qs

    @property
    def temp_accumulated_balance(self):
        if not self.end_date:
            qs = self.device_user.cashbalancelog_set.filter(
                transaction_date__gte=self.start_date,
                status__in=[CashBalanceLog.STATUS.cash_in, CashBalanceLog.STATUS.cash_out]
            )
        else:
            qs = self.device_user.cashbalancelog_set.filter(
                transaction_date__gte=self.start_date,
                transaction_date__lte=self.end_date,
                status__in=[CashBalanceLog.STATUS.cash_in, CashBalanceLog.STATUS.cash_out]
            )

        accumulated_balance = qs.annotate(
            credit_debit=Case(
                When(status=CashBalanceLog.STATUS.cash_out, then=F('balance') * (-1)),
                default=F('balance'),
                output_field=DecimalField(),
            )
        ).aggregate(
            credit_debit_sum=Coalesce(
                Sum('credit_debit', output_field=DecimalField()), Value(0))
        ).get('credit_debit_sum', 0)

        return Money(accumulated_balance, "IDR")

    @property
    def temp_transaction_balance(self):
        qs = self.temp_transactions()
        transaction_balance = qs.aggregate(
            total_cost_sum=Coalesce(
                Sum('total_cost', output_field=DecimalField()), Value(0))
        ).get('total_cost_sum', 0)

        return Money(transaction_balance, "IDR")

    @property
    def temp_transaction_count(self):
        return self.temp_transactions().count()

    @property
    def cash_income(self):
        if not self.end_date:
            return (
                self.opening_balance + self.temp_accumulated_balance + self.temp_transaction_balance)

        return (self.opening_balance + self.accumulated_balance + self.transaction_balance)

    @property
    def cash_difference(self):
        return self.closing_balance - self.cash_income


class CashBalanceLog(SterlingTenantModel, TimeStampedModel):

    STATUS = Choices(
        ('shift_start', _('Shift Start')),
        ('shift_end', _('Shift End')),
        ('cash_in', _('Cash In')),
        ('cash_out', _('Cash Out')),
    )

    transaction_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(_("Status"), max_length=15, choices=STATUS, default=STATUS.shift_start)
    balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')
    device_user = SterlingTenantForeignKey('device.DeviceUser', on_delete=models.CASCADE)
    outlet = SterlingTenantForeignKey('outlet.Outlet', on_delete=models.CASCADE)
    description = models.CharField(_("Description"), max_length=255, blank=True)

    class Meta:
        verbose_name = "CashBalanceLog"
        verbose_name_plural = "CashBalanceLogs"
        unique_together = (('account', 'id'),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        outlet_name = self.outlet.name
        device_user_name = self.device_user.name

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)

        return "Cash Balance Log {} of {} - {} ({}), by {}".format(
            self.get_status_display(), outlet_name,
            self.balance.amount, self.description,
            device_user_name)

    def save(self, *args, **kwargs):

        if not self.pk and self.status == self.STATUS.shift_start and (
                CashBalance.all_objects.filter(
                    device_user=self.device_user, status=CashBalance.STATUS.open).exists()):
            raise DataError(
                _("Cannot open new shift for this device user, please close existing one."))

        if not self.pk and self.status == self.STATUS.shift_end and (
                not CashBalance.all_objects.filter(
                    device_user=self.device_user, status=CashBalance.STATUS.open).exists()):
            raise DataError(
                _("Cannot close shift for this device user, please open a new one."))

        if not self.pk and self.status in [self.STATUS.cash_in, self.STATUS.cash_out] and not CashBalance.all_objects.filter(
                device_user=self.device_user, status=CashBalance.STATUS.open).exists():
            raise DataError(
                _("Cannot process transaction. No open shift is found."))

        return super(CashBalanceLog, self).save(*args, **kwargs)


class BalanceDetail(SterlingTenantModel, TimeStampedModel):
    PAYMENT_NAME = Choices(
        ('cash', _('Cash')),
        ('debit_card', _('Kartu Debit')),
        ('credit_card', _('Kartu Kredit')),
        ('gopay', _('GO-PAY')),
        ('ovo', _('OVO')),
        ('goresto', _('GO-RESTO')),
        ('grab', _('GRAB')),
        ('dana', _('DANA')),
        ('shopeepay', _('ShopeePay')),
    )

    cash_balance_log = SterlingTenantForeignKey(
        'cash_balance.CashBalanceLog',
        related_name="balance_list", on_delete=models.CASCADE)

    cash_balance = SterlingTenantForeignKey(
        'cash_balance.CashBalance',
        related_name="balance_list", null=True, on_delete=models.CASCADE)

    payment_name = models.CharField(
        max_length=255, choices=PAYMENT_NAME, default=PAYMENT_NAME.cash)
    balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency='IDR')

    class Meta:
        verbose_name = 'BalanceDetail'
        verbose_name_plural = 'BalanceDetails'
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "Balance Detail {} {} - {} ({})".format(
            self.cash_balance_log.outlet.name,
            self.cash_balance_log.transaction_date,
            self.balance,
            self.get_payment_name_display(),
        )

    def save(self, *args, **kwargs):
        cash_balance = CashBalance.objects.filter(
            status=CashBalance.STATUS.closed,
            end_date=self.cash_balance_log.transaction_date,
            device_user=self.cash_balance_log.device_user,
            outlet=self.cash_balance_log.outlet,
        ).first()
        self.cash_balance = cash_balance
        obj = super(BalanceDetail, self).save(*args, **kwargs)
        return obj
