# Generated by Django 2.2.12 on 2020-07-14 08:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cash_balance', '0013_auto_20200713_1200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='balancedetail',
            name='payment_name',
            field=models.CharField(choices=[('cash', 'Cash'), ('debit_card', 'Kartu Debit'), ('credit_card', 'Kartu Kredit'), ('gopay', 'GO-PAY'), ('ovo', 'OVO'), ('goresto', 'GO-RESTO'), ('grab', 'GRAB'), ('dana', 'DANA'), ('shopeepay', 'ShopeePay')], default='cash', max_length=255),
        ),
    ]
