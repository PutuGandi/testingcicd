from django.test import TestCase
from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.cash_balance.forms import (
    CashBalanceSettingForm,
    EnableCashBalanceSettingForm,
)


class TestCashBalanceSettingForm(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account')
        self.outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1', account=self.account)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', branch_id='branch-2', account=self.account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save_enable_cash_balance_form(self):
        form_data = {
            'is_enabled': True,
            'outlet': [self.outlet.pk],
        }

        form = EnableCashBalanceSettingForm(form_data)
        self.assertTrue(form.is_valid())
        cash_balance_setting = form.save()

        self.assertTrue(cash_balance_setting.is_enabled)

    def test_save_update_cash_balance_settings(self):
        form_data = {
            'outlet': [self.outlet.pk],
        }

        form = CashBalanceSettingForm(form_data)
        self.assertTrue(form.is_valid())
        cash_balance_setting = form.save()

        self.assertEqual(cash_balance_setting.outlet.count(), 1)
        self.assertFalse(
            cash_balance_setting.outlet.filter(pk=self.outlet_2.pk).exists())
