from decimal import Decimal
from datetime import datetime, timedelta

from django.test import override_settings
from django.utils import timezone
from django.db import DataError

from test_plus.test import TestCase

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.cash_balance.models import (
    CashBalanceLog, CashBalance, BalanceDetail,
)

from model_mommy import mommy


class TestCashBalance(TestCase):

    def test__str__(self):
        start_date = timezone.now()
        end_date = start_date + timedelta(hours=1)
        account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(account)
        outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=account)
        device_user = account.deviceuser_set.first()

        cash_balance = CashBalance.objects.create(
            device_user=device_user,
            outlet=outlet,
            start_date=start_date,
            end_date=end_date,
            opening_balance=500,
            closing_balance=1000,
            accumulated_balance=500,
            status=CashBalance.STATUS.closed,
        )
        self.assertEqual(
            cash_balance.__str__(),
            'Cash Balance Shift Closed of Outlet 1 - 500 | 1000, by admin')

    def tearDown(self):
        set_current_tenant(None)


class TestCashBalanceLog(TestCase):

    def setUp(self):
        self.start_date = timezone.now()
        self.end_date = self.start_date + timedelta(hours=4)
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=self.account)
        self.device_user = self.account.deviceuser_set.first()

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):

        cash_balance_log = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date,
            balance=500,
            description="Open Shift"
        )
        self.assertEqual(
            cash_balance_log.__str__(),
            'Cash Balance Log Shift Start of Outlet 1 - 500 (Open Shift), by admin'
        )

    def test_add_cash_in_cash_balance_log_when_no_shift_is_open(self):

        cash_balance_log = CashBalanceLog(
            device_user=self.device_user,
            outlet=self.outlet,
            status=CashBalanceLog.STATUS.cash_in,
            transaction_date=self.start_date,
            balance=500,
            description="Cash In"
        )

        with self.assertRaises(DataError):
            cash_balance_log.save()

    def test_add_shift_start_cash_balance_log_when_shift_is_open(self):

        cash_balance_log = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date,
            balance=500,
            description="Open Shift"
        )
        cash_balance_log_2 = CashBalanceLog(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date,
            balance=500,
            description="Open Shift"
        )

        with self.assertRaises(DataError):
            cash_balance_log_2.save()

    def test_add_shift_close_cash_balance_log_when_no_shift_is_open(self):

        cash_balance_log = CashBalanceLog(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.end_date,
            status=CashBalanceLog.STATUS.shift_end,
            balance=500,
            description="Close Shift"
        )
        with self.assertRaises(DataError):
            cash_balance_log.save()

    def test_add_shift_close_cash_balance_log_when_shift_is_open(self):
        cash_balance_log = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date,
            balance=500,
            description="Open Shift"
        )
        self.assertEqual(CashBalance.objects.filter(
            status=CashBalance.STATUS.open).count(), 1)

        cash_balance_log = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.end_date,
            status=CashBalanceLog.STATUS.shift_end,
            balance=1000,
            description="Close Shift"
        )
        cash_balance = CashBalance.objects.first()

        self.assertEqual(cash_balance.status, CashBalance.STATUS.closed)
        self.assertEqual(cash_balance.opening_balance.amount, 500)
        self.assertEqual(cash_balance.closing_balance.amount, 1000)

    def test_add_shift_close_cash_balance_log_when_device_user_is_deleted(self):
        cash_balance_log = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date,
            balance=500,
            description="Open Shift"
        )
        self.assertEqual(CashBalance.objects.filter(
            status=CashBalance.STATUS.open).count(), 1)

        self.device_user.delete()

        cash_balance_log = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.end_date,
            status=CashBalanceLog.STATUS.shift_end,
            balance=1000,
            description="Close Shift"
        )
        cash_balance = CashBalance.objects.first()

        self.assertEqual(cash_balance.status, CashBalance.STATUS.closed)
        self.assertEqual(cash_balance.opening_balance.amount, 500)
        self.assertEqual(cash_balance.closing_balance.amount, 1000)


    @override_settings(AUTO_UPDATE_COST=False)
    def test_signal_update_cash_balance_after_save(self):

        cash_balance_log = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date,
            balance=500,
            description="Open Shift"
        )

        CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date + timedelta(hours=1),
            status=CashBalanceLog.STATUS.cash_in,
            balance=700,
            description="Cash In"
        )

        CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date + timedelta(hours=2),
            status=CashBalanceLog.STATUS.cash_out,
            balance=200,
            description="Cash Out"
        )

        mommy.make('sales.SalesOrder',
        
                   transaction_date=self.start_date + timedelta(hours=1),
                   served_by=self.device_user.name,
                   cashier_name=self.device_user.name,
                   status="settled",
                   outlet=self.outlet,
                   total_cost=5000,
                   )

        cash_balance_log_2 = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.end_date,
            status=CashBalanceLog.STATUS.shift_end,
            balance=1000,
            description="Close Shift"
        )
        cash_balance = CashBalance.objects.first()
        self.assertEqual(cash_balance.status, CashBalance.STATUS.closed)
        self.assertEqual(cash_balance.opening_balance.amount, 500)
        self.assertEqual(cash_balance.closing_balance.amount, 1000)
        self.assertEqual(cash_balance.accumulated_balance.amount, 500)
        self.assertEqual(cash_balance.transaction_balance.amount, 5000)
        self.assertEqual(cash_balance.transaction_count, 1)
        self.assertEqual(cash_balance.cash_difference.amount, 1000 - (500 + 500 + 5000))
        self.assertEqual(cash_balance.account, self.account)


class TestBalanceDetail(TestCase):

    def setUp(self):
        self.start_date = timezone.now()
        self.end_date = self.start_date + timedelta(hours=4)

        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=self.account)
        self.device_user = self.account.deviceuser_set.first()

        self.cash_balance_log_open = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.start_date,
            balance=500,
            description="Open Shift"
        )
        self.cash_balance_log_close = CashBalanceLog.objects.create(
            device_user=self.device_user,
            outlet=self.outlet,
            transaction_date=self.end_date,
            status=CashBalanceLog.STATUS.shift_end,
            balance=500,
            description="Close Shift"
        )
        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)
    
    @override_settings(
        LANGUAGE_CODE='en-US', 
        LANGUAGES=(('en', 'English'),))
    def test__str__(self):
        balance_detail = BalanceDetail.objects.create(
            cash_balance_log=self.cash_balance_log_close,
            payment_name=BalanceDetail.PAYMENT_NAME.cash,
            balance=500,
            account=self.account,
        )

        self.assertEqual(
            balance_detail.__str__(),
            'Balance Detail Outlet 1 {} - Rp500.00 (Cash)'.format(
                self.cash_balance_log_close.transaction_date)
        )

    def test_saving_balance_detail_will_hook_it_to_cash_balance(self):
        balance_detail = BalanceDetail.objects.create(
            cash_balance_log=self.cash_balance_log_close,
            payment_name=BalanceDetail.PAYMENT_NAME.cash,
            balance=500,
            account=self.account,
        )
        self.assertEqual(
            balance_detail.cash_balance.status,
            CashBalance.STATUS.closed)
        self.assertEqual(
            balance_detail.cash_balance.end_date,
            balance_detail.cash_balance_log.transaction_date)
