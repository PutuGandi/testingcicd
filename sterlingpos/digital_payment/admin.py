from django.contrib import admin

from sterlingpos.digital_payment.models import (
    DigitalPaymentSettings,
    DigitalPaymentConfiguration
)


class DigitalPaymentSettingsAdmin(admin.ModelAdmin):

    list_display = ("account", "is_active",)
    search_fields = ("account__name",)

    raw_id_fields = ("account",)
    list_filter = ("is_active",)


class DigitalPaymentConfigurationAdmin(admin.ModelAdmin):

    list_display = ("name",)
    search_fields = ("name",)


admin.site.register(
    DigitalPaymentSettings, DigitalPaymentSettingsAdmin)
admin.site.register(
    DigitalPaymentConfiguration, DigitalPaymentConfigurationAdmin)
