import rules

from sterlingpos.accounts.models import Account
from sterlingpos.digital_payment.models import DigitalPaymentSettings

@rules.predicate
def has_access(account : Account, digital_payment : str):
    digital_payment_setting = account.digitalpaymentsettings_set.filter(
        status=DigitalPaymentSettings.STATUS.completed).first()
 
    if digital_payment_setting:
        return digital_payment in digital_payment_setting.active_payment
    return False


@rules.predicate
def is_active(account : Account):
    digital_payment_setting = account.digitalpaymentsettings_set.filter(
        status=DigitalPaymentSettings.STATUS.completed).first()

    if digital_payment_setting:
        return digital_payment_setting.is_active

    return False


rules.set_rule('can_use_digital_payment', is_active & has_access)
