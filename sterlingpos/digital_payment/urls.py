from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from sterlingpos.digital_payment import views

app_name = 'digital_payment'
urlpatterns = [
    url(regex=r'^$',
        view=views.DigitalPaymentRedirectView.as_view(),
        name='index'),

    url(regex=r'^update/$',
        view=views.DigitalPaymentUpdateView.as_view(),
        name='update'),

    url(regex=r'^registration/$',
        view=views.DigitalPaymentRegistrationView.as_view(),
        name='registration'),
    url(regex=r'^registration/form/$',
        view=views.DigitalPaymentRegistrationFormView.as_view(),
        name='registration-form'),
]
