from django.db import DataError, IntegrityError

from test_plus.test import TestCase

from model_mommy import mommy
from django.template.defaultfilters import slugify

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.digital_payment.models import DigitalPaymentSettings

from sterlingpos.integration.shopeepay.models import (
    ShopeePaySettings, ShopeePayOutletStore
)

from model_mommy import mommy


class DigitalPaymentSettingsTest(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_generate_settings(self):
        digital_payment_settings = DigitalPaymentSettings.generate_settings(
            account=self.account,
            digital_payment=DigitalPaymentSettings.DIGITAL_PAYMENTS.shopeepay
        )
        self.assertTrue(
            isinstance(
                digital_payment_settings,
                ShopeePaySettings)
        )

    def test_generate_settings_with_bad_payment(self):
        with self.assertRaises(ValueError):
            digital_payment_settings = DigitalPaymentSettings.generate_settings(
                account=self.account,
                digital_payment="bad_payment"
            )

    def test_generate_outlet_settings(self):
        outlet = mommy.make('outlet.Outlet', branch_id='branch-1')
        outlet_2 = mommy.make('outlet.Outlet', branch_id='branch-2')
        
        digital_payment_outlet_settings = DigitalPaymentSettings.generate_outlet_settings(
            account=self.account,
            digital_payment=DigitalPaymentSettings.DIGITAL_PAYMENTS.shopeepay,
        )
        self.assertTrue(
            isinstance(
                digital_payment_outlet_settings,
                list)
        )
        self.assertEqual(
            len(digital_payment_outlet_settings),
            2
        )
        self.assertTrue(
            isinstance(
                digital_payment_outlet_settings[0],
                ShopeePayOutletStore)
        )

    def test_generate_outlet_setting(self):
        outlet = mommy.make('outlet.Outlet', branch_id='branch-1')
        
        digital_payment_outlet_setting = DigitalPaymentSettings.generate_outlet_setting(
            account=self.account,
            digital_payment=DigitalPaymentSettings.DIGITAL_PAYMENTS.shopeepay,
            outlet=outlet
        )
        self.assertTrue(
            isinstance(
                digital_payment_outlet_setting,
                ShopeePayOutletStore)
        )
