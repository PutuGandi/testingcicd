from datetime import datetime, timedelta

from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.db.models import Q
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.contrib import messages
from django.views.generic import (
    ListView, CreateView,
    UpdateView, DeleteView,
    FormView,
    TemplateView, View,
    RedirectView,
)

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import JSONResponseMixin, AjaxResponseMixin
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.core.mixins import OwnerOnlyMixin, DatatableMixins

from sterlingpos.digital_payment.models import DigitalPaymentSettings
from sterlingpos.digital_payment.forms import (
    DigitalPaymentForm,
)


class DigitalPaymentUpdateView(OwnerOnlyMixin, SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/digital_payment/update.html'
    success_url = reverse_lazy("digital_payment:update")
    model = DigitalPaymentSettings
    form_class = DigitalPaymentForm

    def get_object(self, queryset=None):
        instance = get_object_or_404(
            DigitalPaymentSettings,
            account=self.request.user.account,
            status=DigitalPaymentSettings.STATUS.completed,
        )
        return instance

    def form_valid(self, form):
        res = super().form_valid(form)
        messages.success(
            self.request, '{}'.format(
            _('Digital Payment has been successfully updated.'))
        )
        return res


class DigitalPaymentRedirectView(OwnerOnlyMixin, SterlingRoleMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.account.digitalpaymentsettings_set.exists():
            return reverse("digital_payment:update")
        else:
            return reverse("digital_payment:registration")


class DigitalPaymentRegistrationView(OwnerOnlyMixin, SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/digital_payment/registration.html'


class DigitalPaymentRegistrationFormView(OwnerOnlyMixin, SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/digital_payment/registration_form.html'
    success_url = reverse_lazy("digital_payment:update")
    model = DigitalPaymentSettings
    form_class = DigitalPaymentForm

    def dispatch(self, *args, **kwargs):
        if self.request.user.account.digitalpaymentsettings_set.exists():
            return HttpResponseRedirect(
                reverse("digital_payment:update")
            )
        res = super().dispatch(*args, **kwargs)
        return res

    def get_object(self, queryset=None):
        instance = DigitalPaymentSettings.objects.create(
            account=self.request.user.account,
            status=DigitalPaymentSettings.STATUS.un_reviewed,
            is_active=False,
        )
        return instance

    def form_valid(self, form):
        res = super().form_valid(form)
        messages.success(
            self.request, '{}'.format(
            _('Digital Payment has been successfully created.'))
        )
        return res