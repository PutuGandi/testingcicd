from django.conf import settings

from django.db import models, DataError
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _, ugettext
from django.contrib.postgres.fields import ArrayField

from django.contrib.postgres.fields import JSONField

from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.outlet.models import Outlet
from sterlingpos.core.models import (
    SterlingTenantModel,
    SterlingTenantForeignKey,
)

from sterlingpos.integration.shopeepay.models import shopeepay_handler


def _get_handler(digital_payment):
    mapped_digital_payment = {
        "shopeepay": shopeepay_handler,
    }
    return mapped_digital_payment.get(digital_payment)


class DigitalPaymentSettings(SterlingTenantModel, TimeStampedModel):

    DIGITAL_PAYMENTS = Choices(
        ("shopeepay", "Shopeepay"),
        ("go_pay", "GOPAY"),
        ("ovo", "OVO"),
        ("cashlezz_edz", "Cashlezz EDC"),
    )

    STATUS = Choices(
        ("un_reviewed", "Waiting for Review"),
        ("on_progress", "On Progress"),
        ("completed", "Completed"),
        ("rejected", "Rejected"),
    )

    status = models.CharField(
        _("Digital Payment Status"),
        max_length=50,
        choices=STATUS,
        default=STATUS.un_reviewed,
        db_index=True,
    )
    is_active = models.BooleanField(default=False)

    active_payment = ArrayField(
        models.CharField(
            max_length=255,
            choices=DIGITAL_PAYMENTS,
        ),
        null=True,
        blank=True,
    )

    registered_payment = ArrayField(
        models.CharField(
            max_length=255,
            choices=DIGITAL_PAYMENTS,
        ),
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = "DigitalPaymentSettings"
        verbose_name_plural = "DigitalPaymentSettings"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "DigitalPayment {}".format(self.account, self.is_active)

    @classmethod
    def generate_settings(cls, account, digital_payment):
        if digital_payment not in cls.DIGITAL_PAYMENTS:
            raise ValueError(_("Invalid Digital Payment"))

        handler = _get_handler(digital_payment)
        settings = handler.generate_settings(account)
        return settings        

    @classmethod
    def generate_outlet_setting(
            cls, account, digital_payment, outlet, payment_setting=None):
        handler = _get_handler(digital_payment)
        
        if not payment_setting:
            payment_setting = cls.generate_settings(
                account, digital_payment)

        outlet_setting = handler.generate_outlet_setting(
            payment_setting, outlet)

        return outlet_setting        

    @classmethod
    def generate_outlet_settings(cls, account, digital_payment):
        handler = _get_handler(digital_payment)
        payment_setting = cls.generate_settings(
            account, digital_payment)

        setting_list = [
            cls.generate_outlet_setting(
                account, digital_payment, outlet,
                payment_setting
            )
            for outlet in account.outlet_set.all() 
        ]
        return setting_list        


class DigitalPaymentConfiguration(TimeStampedModel):

    name = models.CharField(max_length=120)
    slug = models.SlugField(max_length=120)
    settings = JSONField()

    class Meta:
        verbose_name = "DigitalPaymentConfiguration"
        verbose_name_plural = "DigitalPaymentConfigurations"

    def __str__(self):
        return f"{self.name} Digital Payment"
