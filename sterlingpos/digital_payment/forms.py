from django import forms
from django.utils.translation import ugettext_lazy as _, ugettext

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Fieldset
from crispy_forms.bootstrap import PrependedText

from sterlingpos.core.bootstrap import KawnField, KawnFieldSetWithHelpText
from sterlingpos.digital_payment.models import DigitalPaymentSettings


class DigitalPaymentForm(forms.ModelForm):

    active_payment = forms.MultipleChoiceField(
        choices=[],
        widget=forms.CheckboxSelectMultiple,
        label='Active Digital Payment',
        required=False,
    )

    class Meta:
        model = DigitalPaymentSettings
        fields = ('active_payment',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance and self.instance.registered_payment:
            self.fields['active_payment'].choices = [
                x for x in DigitalPaymentSettings.DIGITAL_PAYMENTS
                if x[0] in self.instance.registered_payment
            ]

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-12 px-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Digital Payment"),
                KawnField(
                    "active_payment",
                    template="dashboard/receipt/switch_input/multiple_visibility_switch.html"),
            ),
        )