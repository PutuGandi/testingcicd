from test_plus.test import TestCase
from django.db import IntegrityError
from django.db import transaction

from model_mommy import mommy
from sterlingpos.customer.models import Customer, GENDER, CustomerGroup, CustomerOrganization
from sterlingpos.accounts.models import Account
from sterlingpos.core.models import set_current_tenant


class TestCustomer(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.group = mommy.make(CustomerGroup, account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_simple_customer(self):
        customer = Customer.objects.create(
            name='TestUser',
            account=self.account,
        )

        self.assertEqual(
            customer.__str__(),
            '{} [{}]'.format(customer.name, customer.handphone)
        )

    def test_create_customer(self):
        customer = Customer.objects.create(
            name='TestUser',
            birth_date='2010-10-10',
            gender=GENDER.male,
            handphone='08123456789',
            group=self.group,
            account=self.account
        )
        self.assertEqual(
            customer.__str__(),
            '{} [{}]'.format(customer.name, customer.handphone)
        )

    def test_create_customer_with_name_only(self):
        customer = Customer.objects.create(
            name='TestUser',
            account=self.account
        )
        self.assertEqual(
            customer.__str__(),
            '{} [{}]'.format(customer.name, customer.handphone)
        )

    def test_create_customer_with_none_value(self):
        customer = Customer.objects.create(
            name='TestUser',
            gender=GENDER.male,
            handphone=None,
            email=None,
            group=self.group,
            account=self.account
        )
        self.assertEqual(
            customer.__str__(),
            '{} [{}]'.format(customer.name, customer.handphone)
        )


    def test_customer_group(self):
        customer_1 = Customer.objects.create(
            name='TestUser',
            birth_date='2010-10-10',
            gender=GENDER.male,
            handphone='08123456789',
            group=self.group,
            account=self.account
        )
        customer_2 = Customer.objects.create(
            name='TestUser2',
            birth_date='2010-10-10',
            gender=GENDER.female,
            handphone='08123456788',
            group=self.group,
            account=self.account
        )
        self.assertEqual(customer_1.group, self.group)
        self.assertEqual(customer_2.group, self.group)


class TestCustomerGroup(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_customer_group(self):
        customer_group = CustomerGroup.objects.create(
            name='TestCustomerGroup',
            account=self.account
        )
        self.assertEqual(
            customer_group.__str__(),
            customer_group.name
        )


class TestCustomerOrganization(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_customer_organization(self):
        customer_organization = CustomerOrganization.objects.create(
            name='TestCustomerOrganization',
            phone='021987654',
            address='St. TestAddress',
            account=self.account
        )
        self.assertEqual(
            customer_organization.__str__(),
            customer_organization.name
        )

