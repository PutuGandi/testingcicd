import json

from django.test import override_settings, RequestFactory
from django.urls import reverse, reverse_lazy

from test_plus.test import TestCase
from django.test.client import Client

from allauth.account.adapter import get_adapter
from model_mommy import mommy
from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.device.models import DeviceUser


class BaseCustomerTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make('accounts.Account', name='account-1')
        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        device = DeviceUser.objects.first()
        device.user = self.user
        device.save()
        set_current_tenant(account)
        self.customer_a = mommy.make('customer.Customer', name='CustomerA', account=account)
        self.customer_b = mommy.make('customer.Customer', name='CustomerB', account=account)
        self.url = reverse('customers:customer_data')
        self.request_header = {
            "extra": {
                "HTTP_X_REQUESTED_WITH": "XMLHttpRequest"
            }
        }
        self.client = Client()
        self.client.login(email='test@email.com', password='test')

    def tearDown(self):
        set_current_tenant(None)


class TestCustomerListJson(BaseCustomerTest):

    def test_customer_list_json(self):
        self.login(email=self.user.email, password='test')
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content['data'][0]
        expected_result_valid = {
            'recordsTotal': 2,
            'recordsFiltered': 2,
            'columnsTotal': 7
        }

        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])
        self.assertEqual(len(data), expected_result_valid['columnsTotal'])

    def test_customer_list_json_filter(self):
        self.login(email=self.user.email, password='test')
        total_column = 6
        data = {
            'draw': 2
        }
        for i in range(0, total_column):
            data.update({'columns[{}][data]'.format(i): i})
            data.update({'columns[{}][name]'.format(i): ''})
            data.update({'columns[{}][searchable]'.format(i): True})
            if (i == 2):
                data.update({'columns[{}][orderable]'.format(i): False})
            else:
                data.update({'columns[{}][orderable]'.format(i): True})
            data.update({'columns[{}][value]'.format(i): ''})
            data.update({'columns[{}][regex]'.format(i): False})

        data.update({
            'columns[0][data]': 0,
            'columns[0][name]': '',
            'columns[0][searchable]': True,
            'columns[0][orderable]': True,
            'columns[0][search][value]': 'CustomerA',
            'columns[0][search][regex]': False,
        })

        response = self.get(self.url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            'recordsTotal': 2,
            'recordsFiltered': 1,
        }
        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])


class TestCustomerDeactivateView(BaseCustomerTest):

    def test_deactivate_customer_view(self):
        data = {
            'id': self.customer_a.pk
        }
        response = self.client.post(reverse('customers:customer_deactivate'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class TestCustomerActivateView(BaseCustomerTest):

    def test_activate_customer_view(self):
        data = {
            'id': self.customer_b.pk
        }
        response = self.client.post(reverse('customers:customer_activate'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class TestCustomerDeleteView(BaseCustomerTest):

    def test_delete_customer_view(self):
        data = {
            'id': self.customer_a.pk
        }
        response = self.client.post(reverse('customers:customer_delete'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class BaseCustomerGroupTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make('accounts.Account', name='account-1')
        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        device = DeviceUser.objects.first()
        device.user = self.user
        device.save()
        set_current_tenant(account)
        self.customer_group_a = mommy.make('customer.CustomerGroup', name='GroupA', account=account)
        self.customer_group_b = mommy.make('customer.CustomerGroup', name='GroupB', account=account)
        self.customer_a = mommy.make('customer.Customer', name='CustomerA', group=self.customer_group_a,
                                     account=account)
        self.customer_b = mommy.make('customer.Customer', name='CustomerB', group=self.customer_group_a,
                                     account=account)
        self.url = reverse('customers:customer_group_data')
        self.request_header = {
            "extra": {
                "HTTP_X_REQUESTED_WITH": "XMLHttpRequest"
            }
        }
        self.client = Client()
        self.client.login(email='test@email.com', password='test')

    def tearDown(self):
        set_current_tenant(None)


class TestCustomerGroupListJson(BaseCustomerGroupTest):

    def test_customer_group_list_json(self):
        self.login(email=self.user.email, password='test')
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content['data'][0]
        expected_result_valid = {
            'recordsTotal': 3,
            'recordsFiltered': 3,
            'columnsTotal': 4
        }

        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])
        self.assertEqual(len(data), expected_result_valid['columnsTotal'])

    def test_customer_list_json_filter(self):
        self.login(email=self.user.email, password='test')
        total_column = 4
        data = {
            'draw': 2
        }
        for i in range(0, total_column):
            data.update({'columns[{}][data]'.format(i): i})
            data.update({'columns[{}][name]'.format(i): ''})
            data.update({'columns[{}][searchable]'.format(i): True})
            if (i == 2):
                data.update({'columns[{}][orderable]'.format(i): False})
            else:
                data.update({'columns[{}][orderable]'.format(i): True})
            data.update({'columns[{}][value]'.format(i): ''})
            data.update({'columns[{}][regex]'.format(i): False})

        data.update({
            'columns[1][data]': 1,
            'columns[1][name]': '',
            'columns[1][searchable]': True,
            'columns[1][orderable]': True,
            'columns[1][search][value]': '2',
            'columns[1][search][regex]': False,
        })

        response = self.get(self.url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            'recordsTotal': 3,
            'recordsFiltered': 1,
        }
        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])


class TestCustomerGroupDeactivateView(BaseCustomerGroupTest):

    def test_deactivate_customer_group_view(self):
        data = {
            'id': self.customer_group_a.pk
        }
        response = self.client.post(reverse('customers:customer_group_deactivate'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class TestCustomerGroupActivateView(BaseCustomerGroupTest):

    def test_activate_customer_view(self):
        data = {
            'id': self.customer_group_b.pk
        }
        response = self.client.post(reverse('customers:customer_group_activate'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class TestCustomerGroupDeleteView(BaseCustomerGroupTest):

    def test_delete_customer_view(self):
        data = {
            'id': self.customer_group_a.pk
        }
        response = self.client.post(reverse('customers:customer_group_delete'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class BaseCustomerOrganizationTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make('accounts.Account', name='account-1')
        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        device = DeviceUser.objects.first()
        device.user = self.user
        device.save()
        set_current_tenant(account)
        self.customer_organization_a = mommy.make('customer.CustomerOrganization', name='OrganizationA',
                                                  account=account)
        self.customer_organization_b = mommy.make('customer.CustomerOrganization', name='OrganizationB',
                                                  account=account)
        self.url = reverse('customers:customer_organization_data')
        self.request_header = {
            "extra": {
                "HTTP_X_REQUESTED_WITH": "XMLHttpRequest"
            }
        }
        self.client = Client()
        self.client.login(email='test@email.com', password='test')

    def tearDown(self):
        set_current_tenant(None)


class TestCustomerOrganizationListJson(BaseCustomerOrganizationTest):

    def test_customer_organization_list_json(self):
        self.login(email=self.user.email, password='test')
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content['data'][0]
        expected_result_valid = {
            'recordsTotal': 2,
            'recordsFiltered': 2,
            'columnsTotal': 5
        }

        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])
        self.assertEqual(len(data), expected_result_valid['columnsTotal'])

    def test_customer_list_json_filter(self):
        self.login(email=self.user.email, password='test')
        total_column = 4
        data = {
            'draw': 2
        }
        for i in range(0, total_column):
            data.update({'columns[{}][data]'.format(i): i})
            data.update({'columns[{}][name]'.format(i): ''})
            data.update({'columns[{}][searchable]'.format(i): True})
            if (i == 2):
                data.update({'columns[{}][orderable]'.format(i): False})
            else:
                data.update({'columns[{}][orderable]'.format(i): True})
            data.update({'columns[{}][value]'.format(i): ''})
            data.update({'columns[{}][regex]'.format(i): False})

        data.update({
            'columns[0][data]': 0,
            'columns[0][name]': '',
            'columns[0][searchable]': True,
            'columns[0][orderable]': True,
            'columns[0][search][value]': 'OrganizationB',
            'columns[0][search][regex]': False,
        })

        response = self.get(self.url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            'recordsTotal': 2,
            'recordsFiltered': 1,
        }
        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])


class TestCustomerOrganizationDeactivateView(BaseCustomerOrganizationTest):

    def test_deactivate_customer_group_view(self):
        data = {
            'id': self.customer_organization_a.pk
        }
        response = self.client.post(reverse('customers:customer_organization_deactivate'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class TestCustomerOrganizationActivateView(BaseCustomerOrganizationTest):

    def test_activate_customer_view(self):
        data = {
            'id': self.customer_organization_b.pk
        }
        response = self.client.post(reverse('customers:customer_organization_activate'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class TestCustomerOrganizationDeleteView(BaseCustomerOrganizationTest):

    def test_delete_customer_view(self):
        data = {
            'id': self.customer_organization_a.pk
        }
        response = self.client.post(reverse('customers:customer_organization_delete'), data=data,
                                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")
