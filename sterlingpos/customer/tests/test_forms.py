from test_plus.test import TestCase
from django.db import IntegrityError, transaction

from model_mommy import mommy
from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.customer.models import CustomerOrganization, CustomerGroup, Customer, GENDER
from sterlingpos.customer.forms import CustomerForm, CustomerGroupForm, CustomerOrganizationForm
from sterlingpos.accounts.models import Account


class CustomerFormTest(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)

        self.customer_form = CustomerForm
        self.group = mommy.make(CustomerGroup, account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_customer(self):
        customer_data = {
            'name': "Test",
            'handphone': "08123456789",
            'gender': "male",
            'group': self.group.pk,
        }

        customer_form = self.customer_form(data=customer_data)
        self.assertTrue(customer_form.is_valid(), customer_form.errors)
        customers = customer_form.save()
        self.assertIsNotNone(customers.pk)
        self.assertIsNotNone(customers.account, get_current_tenant())

    def test_validate_handphone_for_new_user(self):
        customer = mommy.make(
            'customer.Customer',
            name="Test", handphone="1234",
        )

        customer_data = {
            'name': "Test",
            'handphone': "1234",
        }

        customer_form = self.customer_form(data=customer_data)
        self.assertFalse(
            customer_form.is_valid(), customer_form.data)
        self.assertIn("handphone", customer_form.errors)

    def test_validate_handphone_for_existing_user(self):
        customer_1 = mommy.make(
            'customer.Customer',
            name="Test", handphone="1234",
        )
        customer_2 = mommy.make(
            'customer.Customer',
            name="Test", handphone="5678",
        )

        customer_data = {
            "name": "Test Update",
            "handphone": "5678"
        }
        customer_form = self.customer_form(
            data=customer_data, instance=customer_1)
        self.assertFalse(
            customer_form.is_valid(), customer_form.data)
        self.assertIn("handphone", customer_form.errors)

    def test_birth_date_none_if_not_exists(self):
        customer_data = {
            "name": "Test Update",
            "handphone": "5678",
            "gender": "male",
            "group": self.group.pk,
        }
        customer_form = self.customer_form(data=customer_data)
        self.assertTrue(
            customer_form.is_valid(), customer_form.data)
        self.assertIsNone(
            customer_form.cleaned_data.get("birth_date"),
            customer_form.cleaned_data)


class CustomerGroupFormTest(TestCase):
    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.customer_group_form = CustomerGroupForm

    def tearDown(self):
        set_current_tenant(None)

    def test_create_customer_group(self):
        customer_group = mommy.make(CustomerGroup, account=self.account)

        customer_group_data = customer_group.__dict__
        customer_group_data.update({
            'name': customer_group_data['name'],
        })
        customer_group_form = self.customer_group_form(data=customer_group_data)
        self.assertTrue(customer_group_form.is_valid(), customer_group_form.errors)
        customers_group = customer_group_form.save()
        self.assertIsNotNone(customers_group.pk)
        self.assertIsNotNone(customers_group.account, get_current_tenant())


class CustomerOrganizationFormTest(TestCase):
    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.customer_organization_form = CustomerOrganizationForm

    def tearDown(self):
        set_current_tenant(None)

    def test_create_customer_organization(self):
        customer_organization = mommy.make(CustomerOrganization, account=self.account)
        customer_organization_data = customer_organization.__dict__
        customer_organization_data.update({
            'name': customer_organization_data['name'],
            'phone': '021123456'
        })
        customer_organization_form = self.customer_organization_form(data=customer_organization_data)
        self.assertTrue(customer_organization_form.is_valid(), customer_organization_form.errors)
        customers_organization = customer_organization_form.save()
        self.assertIsNotNone(customers_organization.pk)
        self.assertIsNotNone(customers_organization.account, get_current_tenant())
