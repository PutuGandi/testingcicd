from django import forms
from crispy_forms.helper import FormHelper
from django.template.loader import render_to_string

from sterlingpos.customer.models import Customer, CustomerGroup, CustomerOrganization, GENDER
from crispy_forms.layout import Layout
from django.utils.translation import ugettext_lazy as _, ugettext
from sterlingpos.core.bootstrap import KawnFieldSet, KawnField, KawnFieldSetWithHelpText, KawnPrependedText
from dal import autocomplete

MAX_UPLOAD_SIZE = 4194304 # 4MB

class CustomerForm(forms.ModelForm):
    birth_date = forms.CharField(
        required=False,
        widget=forms.TextInput, label='Tanggal Lahir')
    gender = forms.ChoiceField(
        choices=GENDER,
        widget=forms.RadioSelect,
        label=_('Gender')
    )
    
    class Meta:
        model = Customer
        fields = '__all__'
        exclude = ('account', 'is_active')
        widgets = {
            'group': autocomplete.ModelSelect2(
                url='customers:customer_group_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            'organization': autocomplete.ModelSelect2(
                url='customers:customer_organization_autocomplete',
                attrs={
                    'data-minimum-input-length': 0,
                },
            ),
            'address': forms.widgets.Textarea(),
        }

    def __init__(self, *args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['email'].required = False

        self.fields['group'].required = True
        self.fields['handphone'].required = False
        self.fields['organization'].required = False

        if not self.instance.pk:
            self.fields['gender'].initial = 'male'

        self.fields['handphone'].label = _('Phone Number')
        self.fields['group'].label = _('Group')
        self.fields['organization'].label = _('Organization')
        self.fields['group'].queryset = CustomerGroup.objects.all()
        self.fields['organization'].queryset = CustomerOrganization.objects.all()
        self.fields['image'].label = _('Add Image')
        self.fields['image'].help_text = _("Drag an image here or browse for an image to upload (Maximum size: 4 MB).")
        self.fields['identification_number'].help_text = _("eg. No. KTP, ID Passport, etc.")

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 pl-0 mb-3"
        self.helper.label_class = "pt-0"

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField('name'),
                KawnField('birth_date'),
                KawnField('gender', css_class='custom-select'),
                KawnField('handphone'),
                KawnField('email'),
                KawnField('group', css_class='custom-select'),
                KawnField('organization', css_class='custom-select'),
                KawnField('identification_number'),
                KawnField('address'),
                
                KawnField('image', template='dashboard/image_upload/image_fields_vertical.html'),
                
                help_text=render_to_string('help_text/customer_update.html')
            )
        )

    def validate_handphone(self, handphone):
        if handphone:
            qs = Customer.objects.filter(handphone__iexact=handphone)

            if self.instance.pk:
                qs = qs.exclude(id=self.instance.pk)

            if qs.exists():
                raise forms.ValidationError(
                    _("This Phone Number has already been registered")
                )

    def clean_handphone(self):
        handphone = self.cleaned_data['handphone']
        if handphone:
            try:
                self.validate_handphone(handphone)
            except ValueError:
                pass
        return handphone

    def clean_birth_date(self):
        birth_date = self.cleaned_data['birth_date']
        if birth_date == "":
            return None
        return birth_date

    def clean_image(self):
        image = self.cleaned_data['image']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")
        return image


class CustomerGroupForm(forms.ModelForm):
    class Meta:
        model = CustomerGroup
        fields = ['name',]
        exclude = ('account', 'is_active')

    def __init__(self, *args, **kwargs):
        super(CustomerGroupForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.helper = FormHelper()
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 pl-0 mb-3"
        self.helper.label_class = "pt-0"
        self.helper.form_tag = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField('name'),
            )
        )


class CustomerOrganizationForm(forms.ModelForm):
    class Meta:
        model = CustomerOrganization
        fields = ['name', 'phone', 'address']
        exclude = ('account', 'is_active')

    def __init__(self, *args, **kwargs):
        super(CustomerOrganizationForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['phone'].label = _('Phone Number')
        self.helper = FormHelper()
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 pl-0 mb-3"
        self.helper.label_class = "pt-0"
        self.helper.form_tag = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField('name'),
                KawnField('phone'),
                KawnField('address'),
            )
        )
