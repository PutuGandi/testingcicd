import sys
import random
import string
import qrcode
import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from io import BytesIO

from sterlingpos.core.models import SterlingTenantModel, SterlingTenantForeignKey
from model_utils.models import TimeStampedModel
from model_utils import Choices
from django.core.files.uploadedfile import InMemoryUploadedFile
from django_multitenant.utils import get_current_tenant

GENDER = Choices(('male', _('Male')), ('female', _('Female')))

def get_upload_path(
        instance, filename):
    ext = filename.split('.')[-1]
    randomizer = uuid.uuid4().__str__()[:10]
    new_filename = "{}_{}.{}".format(
        randomizer,
        slugify(instance.image.name[:20]), ext)
    return '{}/customer/{}'.format(
        instance.account.pk,
        new_filename)


class Customer(SterlingTenantModel, TimeStampedModel):
    code = models.SlugField(_('Code'), max_length=10, editable=False)
    name = models.CharField(_('Name'), max_length=255, default='')
    handphone = models.CharField(_("Handphone"), blank=True, null=True, max_length=128)
    gender = models.CharField(
        _('Gender'), max_length=30, choices=GENDER,
        blank=True, default='male',
    )
    birth_date = models.DateField(_('Birth Date'), blank=True, null=True)
    email = models.EmailField(_('Email Address'), unique=False, blank=True, null=True)
    group = SterlingTenantForeignKey(
        'customer.CustomerGroup',
        blank=True, null=True, on_delete=models.CASCADE)
    organization = SterlingTenantForeignKey(
        'customer.CustomerOrganization',
        blank=True, null=True, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    qrcode = models.ImageField(upload_to='qrcode', blank=True, null=True)
    identification_number = models.CharField(
        _("Identification Number"),
        blank=True, null=True, max_length=20)

    address = models.CharField(
        _("Address"),
        blank=True, null=True, max_length=120)

    image = models.ImageField(
        upload_to=get_upload_path,
        blank=True, null=True, max_length=255)

    class Meta:
        verbose_name = _('customer')
        verbose_name_plural = _('customers')
        unique_together = [('account', 'code')]

    def __str__(self):
        return '{} [{}]'.format(self.name, self.handphone)

    def generate_qrcode(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_M,
            box_size=6,
            border=0,
        )
        customer_info = 'CUID|{}'.format(self.code)
        qr.add_data(customer_info)
        qr.make(fit=True)
        img = qr.make_image()
        buffer = BytesIO()
        img.save(buffer, 'PNG')
        filename = 'customer-%s.png' % (self.handphone)
        filebuffer = InMemoryUploadedFile(
            buffer, None, filename, 'image/png', sys.getsizeof(buffer), None)
        self.qrcode.save(filename, filebuffer, save=False)

    def generate_customer_code(self):
        current_tenant = get_current_tenant()
        var_char = string.digits + string.ascii_uppercase
        length = 10
        code = ''.join(random.choices(var_char, k=length))
        code = code.lower()
        while Customer.objects.filter(account=current_tenant, code=code).exists():
            var_char = string.digits + string.ascii_uppercase
            length = 10
            code = ''.join(random.choices(var_char, k=length))
            code = code.lower()
        return code

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = self.generate_customer_code()
        self.generate_qrcode()
        super(Customer, self).save(*args, **kwargs)


class CustomerGroup(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_('Name'), max_length=255, db_index=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'CustomerGroup'
        verbose_name_plural = 'CustomersGroup'
        unique_together = (('account', 'id'))

    def __str__(self):
        return self.name


class CustomerOrganization(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_('Name'), max_length=255, db_index=True)
    phone = models.CharField(_("Organization Phone"), blank=True, max_length=128)
    address = models.TextField(_("Address"), blank=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'CustomerOrganization'
        verbose_name_plural = 'CustomersOrganization'
        unique_together = (('account', 'id'))

    def __str__(self):
        return self.name
