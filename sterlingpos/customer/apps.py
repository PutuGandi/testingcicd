from django.apps import AppConfig


class CustomerConfig(AppConfig):
    name = 'sterlingpos.customer'
    verbose_name = 'Customers'

    def ready(self):
        try:
            import customer.signals
        except ImportError:
            pass
