import csv, io

from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.db.models import Count, Sum, Value, Q, CharField
from django.db.models.functions import Coalesce, Substr, Length
from django.db.models.signals import post_save
from sterlingpos.push_notification.signals import push_update_signal

from django.shortcuts import render, get_object_or_404

from django.http import HttpResponse, HttpResponseBadRequest

from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django_datatables_view.base_datatable_view import BaseDatatableView

from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin
)
from dal import autocomplete
from django.views.generic import (
    TemplateView, View, UpdateView, CreateView, DetailView)

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin

from sterlingpos.customer.utils import render_to_pdf
from sterlingpos.sales.models import SalesOrder, CashierActivity

from sterlingpos.push_notification.signals import resource_import
from sterlingpos.role.utils import get_outlet_of_user

from .models import Customer, CustomerGroup, CustomerOrganization
from sterlingpos.customer.forms import CustomerForm, CustomerGroupForm, CustomerOrganizationForm


class PDFExportMixin(object):
    def get(self, request, *args, **kwargs):
        if request.GET.get('export_file') == 'pdf':
            pdf_data = self.get_pdf_data()
            data = {
                'customers': pdf_data,
                'header_name': self.request.user.account.name
            }
            pdf = render_to_pdf('dashboard/customers/export_template.html', data)
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Customer_%s.pdf" % (self.request.user.account.name)
            content = 'attachment; filename="%s"' % (filename)
            response['Content-Disposition'] = content
            return response
        else:
            return super(PDFExportMixin, self).get(request, *args, **kwargs)


class CustomerListJson(SterlingRoleMixin, PDFExportMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'email', 'handphone', 'group.name', 'organization.name', 'is_active', 'action']
    order_columns = ['name', 'email', 'handphone', 'group__name', 'organization__name', 'is_active', 'action']

    model = Customer

    def get_initial_queryset(self):
        qs = super(
            CustomerListJson, self).get_initial_queryset().select_related('group')
        return qs

    def get_pdf_data(self):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs)
        qs = self.ordering(qs)
        qs = self.paging(qs)
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy("customers:customer_update", kwargs={'pk': row.pk}),
                'active': row.is_active
            }
            return action_data
        elif column == 'handphone':
            return '{}'.format(escape(row.handphone))
        elif column == 'name':
            if hasattr(row, 'image') and row.image._file:
                image_url = row.image.url,
            else:
                image_url = None

            action_data = {
                'username': escape(row.name),
                'image': image_url,
                'url': reverse_lazy("customers:customer_update", kwargs={'pk': row.pk}),
            }
            return action_data
        elif column == 'is_active':
            if row.is_active:
                return '{}'.format(_('Active'))
            else:
                return '{}'.format(_('Deactive'))
        else:
            return super().render_column(row, column)


class CustomerListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/customers/customer_list.html'

    valid_keys = [
        "name",
        "handphone",
        "gender",
        "birth_date",
        "email",
        "group_name",
        "organization_name",
    ]

    required_keys = [
        "name",
        "gender",
    ]

    def validation_header_csv(self, keys, first_sheet):
        if len(keys) < len(first_sheet):
            return False

        try:
            for key in self.valid_keys:
                for row_index in range(1):
                    d = {
                        keys[col_index]: first_sheet[col_index]
                        for col_index in range(len(first_sheet))
                    }
                    d[key]
        except KeyError:
            return False
        return True

    def import_csv_validator(self, csv_file):
        try:
            dialect = csv.Sniffer().sniff(csv_file)
        except csv.Error:
            messages.error(self.request, 'This not a csv file')
        reader = csv.reader(csv_file.splitlines(), dialect)
        csv_headers = []
        valid_headers = [header_name for header_name in self.valid_keys]
        required_field = [header_name for header_name in self.required_keys]
        for y_index, row in enumerate(reader):
            if y_index == 0:
                csv_headers = [header_name.lower().replace(" ", "_") for header_name in row if header_name]
                validation = self.validation_header_csv(csv_headers, valid_headers)
                missing_headers = set(valid_headers) - set([r.lower().replace(" ", "_") for r in row])
                if not validation or missing_headers:
                    missing_headers_str = ', '.join(missing_headers)
                    messages.error(
                        self.request,
                        mark_safe(
                            """
                                Invalid or missing header <strong>%s</strong> field while importing
                            """ % (missing_headers_str)
                        )
                    )
                    return False
            if not ''.join(str(x) for x in row):
                continue
            for x_index, cell_value in enumerate(row):
                try:
                    csv_headers[x_index]
                except IndexError:
                    continue

                if csv_headers[x_index] in required_field:
                    if not cell_value:
                        return False
        return True

    def csv_data_validate(self, csv_data):
        gender = ['male', 'female']
        valid = True
        for column in csv.reader(csv_data, delimiter=',', quotechar="|"):
            if column[0] == '':
                return False
            if column[2]:
                if column[2].lower() in gender:
                    valid = True
                else:
                    return False
            if column[3]:
                try:
                    datetime.strptime(column[3], "%d/%m/%Y")
                except ValueError:
                    return False

        return valid

    def post(self, request, **kwargs):
        template = 'dashboard/customers/customer_list.html'

        if self.request.method == "GET":
            return render(self.request, template)

        csv_file = self.request.FILES['file']
        try:
            data_set = csv_file.read().decode('UTF-8')
        except UnicodeDecodeError:
            messages.error(
                self.request,
                mark_safe(
                    """
                        Invalid file type. Please upload a Valid CSV file using the correct format.
                    """
                )
            )
            return render(self.request, template, {})

        io_string = io.StringIO(data_set)
        io_data = io.StringIO(data_set)
        next(io_string)
        next(io_data)
        valid = self.import_csv_validator(data_set)
        data = self.csv_data_validate(io_data)
        if not valid or not data:
            messages.error(
                self.request,
                mark_safe(
                    """
                        Invalid data while importing. Valid fields are <strong>name, handphone, gender, birth_date,
                        email, group_name, and organization_name</strong>.
                        <br> Note: 
                        <br> 1. Our Birth Date format is DD/MM/YYYY 
                        <br> 2. Our gender choice is male or female
                    """
                )
            )
        else:
            post_save.disconnect(push_update_signal, sender=Customer)
            post_save.disconnect(push_update_signal, sender=CustomerGroup)
            post_save.disconnect(push_update_signal, sender=CustomerOrganization)

            for column in csv.reader(io_string, delimiter=',', quotechar="|"):
                group = None
                organization = None
                d1 = None
                if column[3]:
                    d1 = datetime.strptime(column[3], "%d/%m/%Y").strftime("%Y-%m-%d")

                if column[5]:
                    group, created = CustomerGroup.objects.update_or_create(
                        name=column[5],
                    )
                if column[6]:
                    organization, created = CustomerOrganization.objects.update_or_create(
                        name=column[6]
                    )

                _, created = Customer.objects.update_or_create(
                    name=column[0],
                    gender=column[2].lower(),
                    birth_date=d1,
                    organization=organization,
                    defaults={'handphone': column[1],
                              'email': column[4],
                              'group': group}
                )

            post_save.connect(push_update_signal, sender=Customer)
            post_save.connect(push_update_signal, sender=CustomerGroup)
            post_save.connect(push_update_signal, sender=CustomerOrganization)

            for model_class in [Customer, CustomerGroup, CustomerOrganization]:
                instance = model_class.objects.first()
                if instance:
                    instance.save()
                resource_import.send(
                    sender=model_class,
                    instance=instance)

            messages.success(
                self.request,
                mark_safe(
                    """
                        We have successfully imported <strong>Customers</strong>.
                    """
                )
            )

        context = {}
        return render(self.request, template, context)


class CustomerCreateView(SterlingRoleMixin, CreateView):
    template_name = "dashboard/customers/customer_create.html"
    success_url = reverse_lazy("customers:customer_list")
    model = Customer
    form_class = CustomerForm

    def form_valid(self, form):
        response = super(CustomerCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success create customer.')))
        return response


class CustomerUpdateView(SterlingRoleMixin, UpdateView):
    template_name = "dashboard/customers/customer_update.html"
    success_url = reverse_lazy("customers:customer_list")
    model = Customer
    form_class = CustomerForm

    def form_valid(self, form):
        response = super(CustomerUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success update customer.')))
        return response


class CustomerDeleteJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_id = request.POST.get('id', '')
            customer = Customer.objects.get(pk=customer_id)
            customer.delete()
            status_code = 200
            response['status'] = 'success'
            messages.error(self.request, '{}'.format(_('Success delete customer.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CustomerActivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_id = request.POST.get('id', '')
            customer = Customer.objects.get(pk=customer_id)
            customer.is_active = True
            customer.save()
            status_code = 200
            response['status'] = 'success'
            messages.success(self.request, '{}'.format(_('Success activate customers.')))
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class CustomerDeactivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_id = request.POST.get('id', '')
            customer = Customer.objects.get(pk=customer_id)
            customer.is_active = False
            customer.save()
            status_code = 200
            response['status'] = 'success'
            messages.success(self.request, '{}'.format(_('Success deactivate customer')))
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class CustomerAutoCompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Customer.objects.all()
        qs = qs.filter(is_active=True)
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class CustomerGroupListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'customer_count', 'is_active', 'action']
    order_columns = ['name', 'customer_count', 'is_active', 'action']
    model = CustomerGroup

    def get_initial_queryset(self):
        qs = super(CustomerGroupListJson, self).get_initial_queryset()
        qs = qs.annotate(customer_count=Coalesce(Count('customer', filter=Q(customer__deleted__isnull=True)), Value(0)))
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy("customers:customer_group_update", kwargs={'pk': row.pk}),
                'active': row.is_active
            }
            return action_data
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(reverse_lazy("customers:customer_group_update", kwargs={'pk': row.pk}),
                                                escape(row.name))
        elif column == 'is_active':
            if row.is_active:
                return '{}'.format(_('Active'))
            else:
                return '{}'.format(_('Deactive'))
        else:
            return super(CustomerGroupListJson, self).render_column(row, column)


class CustomerGroupAutoCompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = CustomerGroup.objects.filter(is_active=True)
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

    def create_object(self, text):
        try:
            obj = CustomerGroup.objects.get(name=text)
        except CustomerGroup.DoesNotExist:
            obj = self.form.save()
            obj.save()
        return obj

    def post(self, request):
        text = request.POST.get('text', None)
        self.form = CustomerGroupForm(data={"name": text, })
        if not self.form.is_valid():
            return HttpResponseBadRequest()
        return super().post(request)

    def has_add_permission(self, request):
        if request.user.is_authenticated:
            return True
        return False


class CustomerGroupPromoAutoCompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = CustomerGroup.objects.all()
        qs = qs.filter(is_active=True)
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class CustomerGroupListView(TemplateView):
    template_name = 'dashboard/customers/customer_group_list.html'


class CustomerGroupCreateView(SterlingRoleMixin, CreateView):
    template_name = "dashboard/customers/customer_group_create.html"
    success_url = reverse_lazy("customers:customer_group_list")
    model = CustomerGroup
    form_class = CustomerGroupForm

    def form_valid(self, form):
        response = super(CustomerGroupCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success create group.')))
        return response


class CustomerGroupUpdateView(SterlingRoleMixin, UpdateView):
    template_name = "dashboard/customers/customer_group_update.html"
    success_url = reverse_lazy("customers:customer_group_list")
    model = CustomerGroup
    form_class = CustomerGroupForm

    def form_valid(self, form):
        response = super(CustomerGroupUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success update group.')))
        return response


class CustomerGroupDeleteJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_group_id = request.POST.get('id', '')
            customer_group = CustomerGroup.objects.get(pk=customer_group_id)
            customer_group.delete()
            status_code = 200
            response['status'] = 'success'
            messages.error(self.request, '{}'.format(_('Success delete group.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CustomerGroupActivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_group_id = request.POST.get('id', '')
            customer_group = CustomerGroup.objects.get(pk=customer_group_id)
            customer_group.is_active = True
            customer_group.save()
            response['status'] = 'success'
            status_code = 200
            messages.success(self.request, '{}'.format(_('Success activate group')))
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class CustomerGroupDeactivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_group_id = request.POST.get('id', '')
            customer_group = CustomerGroup.objects.get(pk=customer_group_id)
            customer_group.is_active = False
            customer_group.save()
            response['status'] = 'success'
            status_code = 200
            messages.success(self.request, '{}'.format(_('Success deactivate group')))
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class CustomerOrganizationAutoCompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = CustomerOrganization.objects.filter(is_active=True)
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class CustomerOrganizationListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'phone', 'address', 'is_active', 'action']
    order_columns = ['name', 'phone', 'address', 'is_active', 'action']
    model = CustomerOrganization

    def get_initial_queryset(self):
        qs = super(CustomerOrganizationListJson, self).get_initial_queryset()
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy("customers:customer_organization_update", kwargs={'pk': row.pk}),
                'active': row.is_active
            }
            return action_data
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy("customers:customer_organization_update", kwargs={'pk': row.pk}),
                escape(row.name))
        elif column == 'is_active':
            if row.is_active:
                return '{}'.format(_('Active'))
            else:
                return '{}'.format(_('Deactive'))
        else:
            return super(CustomerOrganizationListJson, self).render_column(row, column)


class CustomerOrganizationListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/customers/customer_organization_list.html'


class CustomerOrganizationCreateView(SterlingRoleMixin, CreateView):
    template_name = "dashboard/customers/customer_organization_create.html"
    success_url = reverse_lazy("customers:customer_organization_list")
    model = CustomerOrganization
    form_class = CustomerOrganizationForm

    def form_valid(self, form):
        response = super(CustomerOrganizationCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success create organization.')))
        return response


class CustomerOrganizationUpdateView(SterlingRoleMixin, UpdateView):
    template_name = "dashboard/customers/customer_organization_update.html"
    success_url = reverse_lazy("customers:customer_organization_list")
    model = CustomerOrganization
    form_class = CustomerOrganizationForm

    def form_valid(self, form):
        response = super(CustomerOrganizationUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success update organization.')))
        return response


class CustomerOrganizationDeleteJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_organization_id = request.POST.get('id', '')
            customer_organization = CustomerOrganization.objects.get(pk=customer_organization_id)
            customer_organization.delete()
            status_code = 200
            response['status'] = 'success'
            messages.error(self.request, '{}'.format(_('Success delete organzation.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class CustomerOrganizationActivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_organization_id = request.POST.get('id', '')
            customer_organization = CustomerOrganization.objects.get(pk=customer_organization_id)
            customer_organization.is_active = True
            customer_organization.save()
            response['status'] = 'success'
            status_code = 200
            messages.success(self.request, '{}'.format(_('Success activate organization')))
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class CustomerOrganizationDeactivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            customer_organization_id = request.POST.get('id', '')
            customer_organization = CustomerOrganization.objects.get(pk=customer_organization_id)
            customer_organization.is_active = False
            customer_organization.save()
            response['status'] = 'success'
            status_code = 200
            messages.success(self.request, '{}'.format(_('Success deactivate organization')))
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class CustomerSalesListJson(DatatableMixins, BaseDatatableView):
    columns = ["small_code", "transaction_date", "outlet.name", "sub_total.amount", "global_discount_price.amount",
               "total_cost.amount", "detail_url"]
    order_columns = [
        "small_code",
        "transaction_date",
        "outlet__name",
        "sub_total",
        "global_discount_price",
        "total_cost",
        "detail_url"
    ]
    datetime_col = ['transaction_date', ]
    model = SalesOrder

    def filter_outlet__name(self, search_value):
        return Q(outlet=search_value)

    def get_initial_queryset(self):
        customer_pk = self.kwargs.get("customer_pk")
        qs = super(CustomerSalesListJson, self).get_initial_queryset()
        qs = (
            qs.filter(customer__pk=customer_pk)
                .select_related("outlet")
                .prefetch_related("order_option",
                                  "payment_detail",
                                  "payment_detail__payment_method")
        )

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = None

        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = None

        if start_date:
            qs = qs.filter(transaction_date__gte=start_date)
        if end_date:
            qs = qs.filter(transaction_date__lte=end_date)

        qs = qs.select_related('payment_method')
        qs = qs.annotate(
            small_code=Substr(
                'code', (Length('code') - 5), 6, output_field=CharField()
            ),
        )
        return qs.order_by('-transaction_date')

    def render_column(self, row, column):
        if column == "detail_url":
            return reverse("customers:customer_sales_detail", kwargs={'pk': row.pk})

        return super(CustomerSalesListJson, self).render_column(row, column)


class CustomerSalesListView(TemplateView):
    template_name = "dashboard/customers/customer_sales_list.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerSalesListView, self).get_context_data(**kwargs)
        context["customer"] = get_object_or_404(
            Customer, pk=self.kwargs.get("customer_pk")
        )
        return context


class CustomerSalesDetailView(SterlingRoleMixin, DetailView):
    template_name = "dashboard/customers/customer_sales_detail.html"
    model = SalesOrder

    def get_queryset(self):
        qs = super(CustomerSalesDetailView, self).get_queryset()

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        qs = qs.annotate(
            small_code=Substr(
                'code', (Length('code') - 5), 6, output_field=CharField()
            ),
        )
        qs = qs.select_related(
            'outlet',
            'transaction_type',
        ).prefetch_related(
            'items',
            'items__product',
            'items__addon_detail',
            'payment_detail',
            'payment_detail__payment_method',
        )

        return qs

    def get_context_data(self, **kwargs):
        context = super(CustomerSalesDetailView, self).get_context_data(**kwargs)
        if self.object.status == SalesOrder.STATUS.unsettled:
            cashier_activity = CashierActivity.objects.get(
                activity_type=CashierActivity.TYPE.CART_VOID,
                code=self.object.code,
                outlet=self.object.outlet,
            )
            context.update({
                "cashier_activity": cashier_activity
            })
        return context


class CustomerReportView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/customers/customer_report.html'


class CustomerReportJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'transaction_count', 'total_transaction', ]
    order_columns = ['name', 'transaction_count', 'total_transaction', ]
    model = Customer

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()

        outlet_pk = self.kwargs.get('outlet_pk')
        outlet_qs = get_outlet_of_user(self.request.user)

        sales_filter = Q(salesorder__status='settled') & Q(salesorder__outlet__in=outlet_qs)

        if outlet_pk:
            outlet = get_object_or_404(
                outlet_qs,
                pk=outlet_pk)
            sales_filter &= Q(salesorder__outlet=outlet)
        else:
            outlet = None

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hour=23, minute=59)

        sales_filter &= Q(
            salesorder__transaction_date__range=[start_date, end_date])

        qs = qs.prefetch_related(
            'salesorder_set',
        ).annotate(
            transaction_count=Coalesce(
                Count('salesorder', filter=sales_filter), Value(0)),
            total_transaction=Coalesce(
                Sum('salesorder__total_cost', filter=sales_filter), Value(0)),
        )
        return qs

    def render_column(self, row, column):
        if column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "customers:customer_update", kwargs={'pk': row.pk}),
                escape(row.name)
            )
        else:
            return super().render_column(row, column)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hour=23, minute=59)

        qs = self.get_initial_queryset()
        context.update({
            "total_new_customer": qs.aggregate(
                count_new_customer=Coalesce(
                    Count("pk", filter=Q(created__range=[start_date, end_date])), Value(0))).get("count_new_customer"),
            "total_transaction_count": qs.aggregate(
                sum_transaction_count=Coalesce(
                    Sum('transaction_count'), Value(0))).get("sum_transaction_count"),
            "total_transaction": qs.aggregate(
                sum_total_transaction=Coalesce(
                    Sum('total_transaction'), Value(0))).get("sum_total_transaction"),
        })
        return context


class EmployeeListJson(SterlingRoleMixin, PDFExportMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'email', 'handphone', 'group.name', 'organization.name', 'is_active', 'action']
    order_columns = ['name', 'email', 'handphone', 'group__name', 'organization__name', 'is_active', 'action']

    model = Customer

    def get_initial_queryset(self):
        qs = super(
            EmployeeListJson, self).get_initial_queryset().select_related('group').filter(organization__isnull=False)
        return qs

    def get_pdf_data(self):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs)
        qs = self.ordering(qs)
        qs = self.paging(qs)
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy("customers:employee_update", kwargs={'pk': row.pk}),
                'active': row.is_active
            }
            return action_data
        elif column == 'handphone':
            return '{}'.format(escape(row.handphone))
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(reverse_lazy("customers:employee_update", kwargs={'pk': row.pk}),
                                                escape(row.name))
        elif column == 'is_active':
            if row.is_active:
                return '{}'.format(_('Active'))
            else:
                return '{}'.format(_('Deactive'))
        else:
            return super(EmployeeListJson, self).render_column(row, column)


class EmployeeListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/employee/employee_list.html'

    valid_keys = [
        "name",
        "handphone",
        "gender",
        "birth_date",
        "email",
        "group_name",
        "organization_name",
    ]

    required_keys = [
        "name",
        "gender",
    ]

    def validation_header_csv(self, keys, first_sheet):
        try:
            for key in self.valid_keys:
                for row_index in range(1):
                    d = {
                        keys[col_index]: first_sheet[col_index]
                        for col_index in range(len(first_sheet))
                    }
                    d[key]
        except KeyError:
            return False
        return True

    def import_csv_validator(self, csv_file):
        try:
            dialect = csv.Sniffer().sniff(csv_file)
        except csv.Error:
            messages.error(self.request, 'This not a csv file')
        reader = csv.reader(csv_file.splitlines(), dialect)
        csv_headers = []
        valid_headers = [header_name for header_name in self.valid_keys]
        required_field = [header_name for header_name in self.required_keys]
        for y_index, row in enumerate(reader):
            if y_index == 0:
                csv_headers = [header_name.lower().replace(" ", "_") for header_name in row if header_name]
                validation = self.validation_header_csv(csv_headers, valid_headers)
                missing_headers = set(valid_headers) - set([r.lower().replace(" ", "_") for r in row])
                if not validation or missing_headers:
                    missing_headers_str = ', '.join(missing_headers)
                    messages.error(
                        self.request,
                        mark_safe(
                            """
                                Invalid or missing header <strong>%s</strong> field while importing
                            """ % (missing_headers_str)
                        )
                    )
                    return False
            if not ''.join(str(x) for x in row):
                continue
            for x_index, cell_value in enumerate(row):
                try:
                    csv_headers[x_index]
                except IndexError:
                    continue

                if csv_headers[x_index] in required_field:
                    if not cell_value:
                        return False
        return True

    def csv_data_validate(self, csv_data):
        gender = ['male', 'female']
        try:
            for column in csv.reader(csv_data, delimiter=',', quotechar="|"):
                if column[0] == '':
                    return False
                if column[2]:
                    if column[2].lower() in gender:
                        valid = True
                    else:
                        return False
                if column[3]:
                    datetime.strptime(column[3], "%d/%m/%Y")
            return True
        except (ValueError, IndexError):
            return False

    def post(self, request, **kwargs):
        template = 'dashboard/customers/customer_list.html'

        if self.request.method == "GET":
            return render(self.request, template)

        csv_file = self.request.FILES['file']
        try:
            data_set = csv_file.read().decode('UTF-8')
        except UnicodeDecodeError:
            messages.error(
                self.request,
                mark_safe(
                    """
                        Invalid file type. Please upload a Valid CSV file using the correct format.
                    """
                )
            )
            return render(self.request, template, {})

        io_string = io.StringIO(data_set)
        io_data = io.StringIO(data_set)
        next(io_string)
        next(io_data)
        valid = self.import_csv_validator(data_set)
        data = self.csv_data_validate(io_data)
        if not valid or not data:
            messages.error(
                self.request,
                mark_safe(
                    """
                        Invalid data while importing. Valid fields are <strong>name, handphone, gender, birth_date,
                        email, group_name, and organization_name</strong>.
                        <br> Note: 
                        <br> 1. Our Birth Date format is DD/MM/YYYY 
                        <br> 2. Our gender choice is male or female
                    """
                )
            )
        else:
            for column in csv.reader(io_string, delimiter=',', quotechar="|"):
                group = None
                organization = None
                d1 = None
                if column[3]:
                    d1 = datetime.strptime(column[3], "%d/%m/%Y").strftime("%Y-%m-%d")

                if column[5]:
                    group, created = CustomerGroup.objects.update_or_create(
                        name=column[5],
                    )
                if column[6]:
                    organization, created = CustomerOrganization.objects.update_or_create(
                        name=column[6]
                    )

                _, created = Customer.objects.update_or_create(
                    name=column[0],
                    gender=column[2].lower(),
                    birth_date=d1,
                    organization=organization,
                    defaults={'handphone': column[1],
                              'email': column[4],
                              'group': group}
                )
            messages.success(
                self.request,
                mark_safe(
                    """
                        We have successfully imported <strong>Customers</strong>.
                    """
                )
            )
        context = {}
        return render(self.request, template, context)


class EmployeeCreateView(SterlingRoleMixin, CreateView):
    template_name = "dashboard/employee/employee_create.html"
    success_url = reverse_lazy("customers:employee_list")
    model = Customer
    form_class = CustomerForm

    def form_valid(self, form):
        response = super(EmployeeCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success create customer.')))
        return response


class EmployeeUpdateView(SterlingRoleMixin, UpdateView):
    template_name = "dashboard/employee/employee_update.html"
    success_url = reverse_lazy("customers:employee_list")
    model = Customer
    form_class = CustomerForm

    def form_valid(self, form):
        response = super(EmployeeUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success update customer.')))
        return response


class EmployeeReportView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/employee/employee_report.html'


class EmployeeReportJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'organization.name', 'transaction_count', 'total_transaction', ]
    order_columns = ['name', 'organization__name', 'transaction_count', 'total_transaction', ]
    model = Customer

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()

        outlet_pk = self.kwargs.get('outlet_pk')
        outlet_qs = get_outlet_of_user(self.request.user)

        sales_filter = Q(salesorder__status='settled') & Q(salesorder__outlet__in=outlet_qs)

        if outlet_pk:
            outlet = get_object_or_404(
                outlet_qs,
                pk=outlet_pk)
            sales_filter &= Q(salesorder__outlet=outlet)
        else:
            outlet = None

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hour=23, minute=59)

        sales_filter &= Q(
            salesorder__transaction_date__range=[start_date, end_date])

        qs = qs.select_related(
            "organization",
        ).prefetch_related(
            'salesorder_set',
        ).annotate(
            transaction_count=Coalesce(
                Count('salesorder', filter=sales_filter), Value(0)),
            total_transaction=Coalesce(
                Sum('salesorder__total_cost', filter=sales_filter), Value(0)),
        ).filter(organization__isnull=False)
        return qs

    def render_column(self, row, column):
        if column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "customers:employee_update", kwargs={'pk': row.pk}),
                escape(row.name)
            )
        else:
            return super().render_column(row, column)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hour=23, minute=59)

        qs = self.get_initial_queryset()
        context.update({
            "total_new_customer": qs.aggregate(
                count_new_customer=Coalesce(
                    Count("pk", filter=Q(created__range=[start_date, end_date])), Value(0))).get("count_new_customer"),
            "total_transaction_count": qs.aggregate(
                sum_transaction_count=Coalesce(
                    Sum('transaction_count'), Value(0))).get("sum_transaction_count"),
            "total_transaction": qs.aggregate(
                sum_total_transaction=Coalesce(
                    Sum('total_transaction'), Value(0))).get("sum_total_transaction"),
        })
        return context
