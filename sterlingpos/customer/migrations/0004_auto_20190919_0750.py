# Generated by Django 2.0.4 on 2019-09-19 07:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_account_registered_via'),
        ('customer', '0003_auto_20190919_0739'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='customer',
            unique_together={('account', 'id')},
        ),
    ]
