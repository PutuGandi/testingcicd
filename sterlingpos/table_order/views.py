import copy
from decimal import Decimal, ROUND_DOWN
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta, MO, SU

from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.db.models.functions import Coalesce
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.db.models import (
    Prefetch, Sum, Count, Q, F, Func, Value, Avg
)
from django.views.generic import (
    TemplateView, ListView, DetailView, UpdateView, DeleteView, View
)

from django_datatables_view.base_datatable_view import BaseDatatableView

from braces.views import LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin

from sterlingpos.core.models import get_current_tenant
from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.sales.models import SalesOrder
from .forms import (
    TableOrderSettingsForm,
    EnableTableOrderSettingsForm,
)
from .models import (
    TableOrderSettings,
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user


class TableOrderSettingsUpdateView(SterlingRoleMixin, UpdateView):
    model = TableOrderSettings
    template_name = "dashboard/table_order/settings.html"
    form_class = TableOrderSettingsForm
    success_url = reverse_lazy("table_order:settings")

    def get_object(self, queryset=None):
        obj = self.request.user.account.tableordersettings_set.first()
        return obj

    def get_initial(self):
        outlet = self.object.outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(outlet)

        initial = {
            'all_outlet': disabled_outlet.count() == 0,
            'outlet': outlet
        }
        return initial

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Table Order has been updated.')))
        return response


class TableOrderSettingsUpdateViewJSON(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_object(self):
        obj = self.request.user.account.tableordersettings_set.first()
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }

        form = EnableTableOrderSettingsForm(
            data=request.POST,
            instance=self.get_object()
        )

        if form.is_valid():
            table_settings = form.save()
            status_code = 200
            response['status'] = 'success'
            response['data'] = table_settings.is_enabled
        else:
            response['data'] = form.errors
            status_code = 400

        return self.render_json_response(response, status=status_code)


class TableOrderDisableView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/table_order/disable.html'
