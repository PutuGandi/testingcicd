from django import forms
from django.forms import inlineformset_factory, BaseInlineFormSet
from django.template.loader import render_to_string

from sterlingpos.core.bootstrap import KawnField, KawnFieldSet, KawnFieldSetWithHelpText
from sterlingpos.outlet.models import Outlet

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, HTML

from sterlingpos.catalogue.forms import BaseFormsetLayout
from sterlingpos.sales.models import SalesOrderOption
from .models import (
    TableOrderSettings,
    TableOrderOutlet
)
from django.utils.translation import ugettext_lazy as _


class EnableTableOrderSettingsForm(forms.ModelForm):
    class Meta:
        model = TableOrderSettings
        fields = ('is_enabled',)
        exclude = ('account',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.fields['is_enabled'].label = _('Enable Self Ordering')
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            KawnField('is_enabled', template='dashboard/table_management/switch_input/switch.html')
        )

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        if not obj.is_enabled:
            obj.outlet.clear()
        else:
            outlets = [
                TableOrderOutlet(
                    outlet=outlet,
                    table_order_setting=obj,
                    account=obj.account
                )
                for outlet in obj.account.outlet_set.all()
            ]
            TableOrderOutlet.objects.bulk_create(outlets)
            obj.outlet.invalidated_update()
        return obj


class TableOrderSettingsForm(forms.ModelForm):
    all_outlet = forms.BooleanField(
        initial=True, label="Semua Outlet"
    )

    class Meta:
        model = TableOrderSettings
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        self.context = {}
        super().__init__(*args, **kwargs)
        self.fields['order_option'].queryset = SalesOrderOption.objects.all()
        self.fields['order_option'].required = True
        self.fields['outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['outlet'].label = ''
        self.fields['all_outlet'].required = False
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout= Layout(
            KawnFieldSetWithHelpText(
                _("Order Option"),
                KawnField("order_option"),
                KawnField("pin"),
                form_grid="col-lg-12",
            ),
            KawnFieldSet(
                "",
                KawnField('all_outlet', template='dashboard/table_management/switch_input/float_right_switch.html'),
                KawnField('outlet', template='dashboard/table_management/switch_input/multiple_switch.html'),
            )

        )

    def update_m2m(self, table_order, object_list, rel_field, intermediate_model):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "table_order_setting": table_order,
                    "account": table_order.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        kwargs['commit'] = False
        obj = super().save(*args, **kwargs)
        obj.save()
        self.update_m2m(
            obj, outlets, obj.outlet, TableOrderOutlet)
        return obj
