from __future__ import unicode_literals, absolute_import

from datetime import datetime

from django.db import models, DataError
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.core.models import SterlingTenantModel, SterlingTenantForeignKey


class TableOrderSettings(SterlingTenantModel, TimeStampedModel):

    is_enabled = models.BooleanField(default=False)

    outlet = models.ManyToManyField(
        "outlet.Outlet", blank=True,
        through='TableOrderOutlet',
        related_name='table_order_setting')

    order_option = SterlingTenantForeignKey(
        "sales.SalesOrderOption", blank=True, null=True,
        on_delete=models.SET_NULL)

    pin = models.CharField(
        _("Pin"), max_length=6, default="123654")

    class Meta:
        verbose_name = "TableOrderSetting"
        verbose_name_plural = "TableOrderSettings"
        unique_together = (('account', 'id'),)


class TableOrderOutlet(SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    table_order_setting = models.ForeignKey(
        TableOrderSettings, on_delete=models.CASCADE)
    outlet = models.ForeignKey(
        "outlet.Outlet", on_delete=models.CASCADE)

