from django.apps import AppConfig
from django.db.models.signals import post_save


class TableOrderConfig(AppConfig):
    name = 'sterlingpos.table_order'
    verbose_name = "Table Order"
    permission_name = "application"

    def ready(self):
        pass
