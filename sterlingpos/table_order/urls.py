# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "table_order"
urlpatterns = [
    url(regex=r'^settings/$', view=views.TableOrderSettingsUpdateView.as_view(),
        name='settings'),
    url(r'^settings/enable/$', view=views.TableOrderSettingsUpdateViewJSON.as_view(),
        name='settings_enable'),
    url(regex=r'^settings/disable/$', view=views.TableOrderDisableView.as_view(),
        name='settings_disable'),
]

