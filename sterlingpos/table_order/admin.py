from django.contrib import admin

from .models import (
    TableOrderSettings,
)


class TableOrderSettingsAdmin(admin.ModelAdmin):

    list_display = (
        "account", "is_enabled",)
    list_filter = ("is_enabled",)
    search_fields = ("account__name",)

admin.site.register(TableOrderSettings, TableOrderSettingsAdmin)
