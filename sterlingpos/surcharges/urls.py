# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "surcharges"
urlpatterns = [
    url(regex=r'^surcharges/$', view=views.SurchargeListView.as_view(),
        name='surcharge_list'),
    url(regex=r'^surcharges/data/$', view=views.SurchargeListJson.as_view(),
        name='surcharge_data'),

    url(r'^deactive/$', views.SurchargeDeactiveJson.as_view(),
        name='surcharge_deactive'),
    url(r'^activate/$', views.SurchargeActivateJson.as_view(),
        name='surcharge_activate'),
    url(r'^delete/$', views.SurchargeDeleteJson.as_view(),
        name='surcharge_delete'),
    url(regex=r'^surcharge/(?P<pk>\d+)/update/$', view=views.SurchargeUpdateView.as_view(),
        name='surcharge_update'),

]
