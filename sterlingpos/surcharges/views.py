import datetime
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django_datatables_view.base_datatable_view import BaseDatatableView

from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin
)
from django.views.generic import (
    TemplateView, View, UpdateView)

from sterlingpos.core.mixins import DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.surcharges.forms import SurchargeForm

from .models import (
    Surcharge
)


class SurchargeListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['order_option__name', 'is_active', 'surcharge', 'action', 'surcharge_type']
    order_columns = ['order_option__name', 'is_active', 'surcharge', 'action', 'surcharge_type']

    model = Surcharge

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.exclude(deleted__isnull=False)
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy("surcharges:surcharge_update", kwargs={'pk': row.pk}),
                'archived': row.is_active
            }
            return action_data
        elif column == 'order_option__name':
            return '<a href="{}">{}</a>'.format(reverse_lazy("surcharges:surcharge_update", kwargs={'pk': row.pk}),
                                                escape(row.order_option.name))
        elif column == 'is_active':
            if row.is_active:
                return '{}'.format(_('Active'))
            else:
                return '{}'.format(_('Inactive'))
        else:
            return super(SurchargeListJson, self).render_column(row, column)


class SurchargeListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/surcharges/surcharge_list.html"


class SurchargeUpdateView(SterlingRoleMixin, UpdateView):
    template_name = "dashboard/surcharges/surcharge_update.html"
    success_url = reverse_lazy("surcharges:surcharge_list")
    model = Surcharge
    form_class = SurchargeForm

    def get_initial(self):
        initial = {
            'surcharge_type': self.object.surcharge_type,
            'surcharge': self.object.surcharge,
        }
        return initial

    def form_valid(self, form):
        response = super(SurchargeUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success update surcharge.')))
        return response


class SurchargeDeactiveJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            surcharge_id = request.POST.get('id', '')
            surcharge = Surcharge.objects.get(pk=surcharge_id)
            surcharge.is_active = False
            surcharge.save()
            status_code = 200
            response['status'] = 'success'
            messages.success(self.request, '{}'.format(_('Success deactivate surcharge.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class SurchargeActivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            surcharge_id = request.POST.get('id', '')
            surcharge = Surcharge.objects.get(pk=surcharge_id)
            surcharge.is_active = True
            surcharge.save()
            status_code = 200
            response['status'] = 'success'
            messages.success(self.request, '{}'.format(_('Success activate surcharge.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class SurchargeDeleteJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            surcharge_id = request.POST.get('id', '')
            surcharge = Surcharge.objects.get(pk=surcharge_id)
            surcharge.delete()
            status_code = 200
            response['status'] = 'success'
            messages.error(self.request, '{}'.format(_('Success delete surcharge.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
