import datetime

from django.conf import settings
from django import forms
from crispy_forms.helper import FormHelper
from django.template.loader import render_to_string

from sterlingpos.sales.models import SalesOrderOption
from .models import Surcharge
from sterlingpos.surcharges.models import _SURCHARGE_TYPE
from crispy_forms.layout import Layout, Div, Field, HTML, Submit, Fieldset
from django.utils.translation import ugettext_lazy as _, ugettext
from sterlingpos.core.bootstrap import KawnFieldSet, KawnField, KawnFieldSetWithHelpText, KawnPrependedText


class SurchargeForm(forms.ModelForm):
    surcharge_type = forms.ChoiceField(choices=_SURCHARGE_TYPE, widget=forms.Select, label='')

    surcharge = forms.DecimalField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        label='')

    class Meta:
        model = Surcharge
        fields = '__all__'
        exclude = ('account',)

    def clean_order_option(self):
        order_option = self.cleaned_data['order_option']
        if not self.instance.pk and Surcharge.objects.filter(order_option=order_option).exists():
            raise forms.ValidationError(_("Order Option already been used"), code='order_option_has_been_used')
        return order_option

    def clean(self):
        cleaned_data = super(SurchargeForm, self).clean()
        if (cleaned_data.get('surcharge_type') == _SURCHARGE_TYPE.percentage
            and cleaned_data.get('surcharge', 0) > 100):
            raise forms.ValidationError(
                _("Invalid Surcharge Percentage Value."),
                code='invalid_surcharge_percentage')
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(SurchargeForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['surcharge_type'].required = True
        self.fields['order_option'].required = True
        self.fields['order_option'].queryset = SalesOrderOption.objects.all()
        self.fields['name'].label = _('Label on Receipt')
        self.fields['is_active'].label = _('Active')
        self.fields['apply_to_product'].label = _('Apply Surcharge to Product')
        self.fields['exclude_gratuity'].label = _('Exclude Service')
        self.helper = FormHelper()
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 pl-0 mb-3"
        self.helper.label_class = "pt-0"
        self.helper.form_tag = False

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField('is_active', template='dashboard/surcharges/switch_input/switch.html'),
                KawnField('order_option', css_class='custom-select'),
                KawnField('name'),
                KawnPrependedText('surcharge', _('+'), css_id='div_id_surcharge'),
                KawnField('surcharge_type', css_class='custom-select',
                          template='dashboard/form_fields/surcharge_type_radio_button.html'),
                KawnField('apply_to_product'),
                KawnField('exclude_gratuity'),
                help_text=render_to_string("help_text/surcharge_help_text.html")
            ),
        )

    def update_surcharge_amount(self, surcharges):
        surcharge_type = self.cleaned_data['surcharge_type']
        surcharge = self.cleaned_data['surcharge']
        if not hasattr(surcharges, 'surcharge'):
            surcharges = Surcharge.objects.create(
                surcharge_type=surcharge_type,
                surcharge=surcharge,
            )
        else:
            surcharges.surcharge_type = surcharge_type
            surcharges.surcharge = surcharge
            surcharges.save()

    def save(self, *args, **kwargs):
        surcharges = super(SurchargeForm, self).save(*args, **kwargs)
        self.update_surcharge_amount(surcharges)
        return surcharges
