from django.conf import settings
from django.db import models
from model_utils import Choices
from model_utils.models import TimeStampedModel
from django.utils.translation import ugettext_lazy as _, ugettext
from sterlingpos.core.models import (
    SterlingTenantModel, SterlingTenantForeignKey
)

_SURCHARGE_TYPE = Choices(
    ('percentage', _('%')),
    ('fixed_price', _('Rp')),
)


class Surcharge(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(max_length=255, default='Surcharge')
    surcharge_fixed_price = models.DecimalField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        blank=True, null=True)
    surcharge_percentage = models.DecimalField(
        max_digits=5, decimal_places=2,
        blank=True, null=True)
    surcharge_type = models.CharField(_("Surcharge Type"), max_length=15, choices=_SURCHARGE_TYPE,
                                      default=_SURCHARGE_TYPE.fixed_price)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    start_hours = models.TimeField(blank=True, null=True)
    end_hours = models.TimeField(blank=True, null=True)
    is_active = models.BooleanField(default=False)
    order_option = SterlingTenantForeignKey('sales.SalesOrderOption', null=True,
                                            on_delete=models.CASCADE)
    apply_to_product = models.BooleanField(default=False)
    exclude_gratuity = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Surcharge"
        verbose_name_plural = "Surcharges"
        unique_together = (('account', 'id'))

    def __str__(self):
        return "{} {} {}".format(self.name, self.surcharge_type, self.surcharge)

    def surcharge():

        def fget(self):
            if self.surcharge_type == _SURCHARGE_TYPE.percentage:
                return self.surcharge_percentage
            else:
                return self.surcharge_fixed_price

        def fset(self, value):
            if self.surcharge_type == _SURCHARGE_TYPE.percentage:
                self.surchage_fixed_price = None
                self.surcharge_percentage = value
            else:
                self.surcharge_fixed_price = value
                self.surcharge_percentage = None

        def fdel(self):
            self.surcharge_fixed_price = None
            self.surcharge_percentage = None

        return locals()

    surcharge = property(**surcharge())
