import json

from django.test import override_settings, RequestFactory
from django.urls import reverse, reverse_lazy

from test_plus.test import TestCase

from allauth.account.adapter import get_adapter
from model_mommy import mommy
from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.sales.models import SalesOrderOption
from sterlingpos.surcharges.models import _SURCHARGE_TYPE


class TestSurchagerListJson(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make('accounts.Account', name='account-1')

        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        set_current_tenant(account)
        sales_order_option_a = mommy.make('sales.SalesOrderOption', name='Take Away',
                                          account=account)
        sales_order_option_b = mommy.make('sales.SalesOrderOption', name='Go Food',
                                          account=account
                                          )
        self.surcharge_a = mommy.make('surcharges.Surcharge', surcharge_type=_SURCHARGE_TYPE.fixed_price,
                                      surcharge=1000, order_option=sales_order_option_a, is_active=True)
        self.surcharge_b = mommy.make('surcharges.Surcharge', surcharge_type=_SURCHARGE_TYPE.percentage,
                                      surcharge=3, order_option=sales_order_option_b)
        self.url = reverse('surcharges:surcharge_data')

        self.request_header = {
            "extra": {
                "HTTP_X_REQUESTED_WITH": "XMLHttpRequest"
            }
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_surcharge_list_json(self):
        self.login(email=self.user.email, password='test')
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content['data'][0]
        expected_result_valid = {
            'recordsTotal': 9,
            'recordsFiltered': 9,
            'columnsTotal': 5
        }

        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])
        self.assertEqual(len(data), expected_result_valid['columnsTotal'])

    def test_surcharge_list_json_filter(self):
        self.login(email=self.user.email, password='test')
        total_column = 3
        data = {
            'draw': 2
        }
        for i in range(0, total_column):
            data.update({'columns[{}][data]'.format(i): i})
            data.update({'columns[{}][name]'.format(i): ''})
            data.update({'columns[{}][searchable]'.format(i): True})
            if (i == 2):
                data.update({'columns[{}][orderable]'.format(i): False})
            else:
                data.update({'columns[{}][orderable]'.format(i): True})
            data.update({'columns[{}][value]'.format(i): ''})
            data.update({'columns[{}][regex]'.format(i): False})

        data.update({
            'columns[1][data]': 1,
            'columns[1][name]': '',
            'columns[1][searchable]': True,
            'columns[1][orderable]': False,
            'columns[1][search][value]': True,
            'columns[1][search][regex]': False,
        })

        response = self.get(self.url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            'recordsTotal': 9,
            'recordsFiltered': 1,
            'data_columns_name': '<a href="{}">{}</a>'.format(
                reverse_lazy("surcharges:surcharge_update", kwargs={'pk': self.surcharge_b.pk}),
                self.surcharge_b.name)
        }

        # data = content['data'][0]
        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])
        # self.assertEqual(data[0], expected_result_valid['data_columns_name'])


class TestSalesOrderOptionUpdateView(TestCase):
    url_string = 'surcharges:surcharge_update'

    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make('accounts.Account', name='account-1')

        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        set_current_tenant(account)
        self.surcharge_a = mommy.make('surcharges.Surcharge', surcharge_type=_SURCHARGE_TYPE.fixed_price,
                                      surcharge=1000, start_date='2019-08-01', end_date='2019-08-30')
        self.surcharge_b = mommy.make('surcharges.Surcharge', surcharge_type=_SURCHARGE_TYPE.percentage,
                                      surcharge=3, start_date='2019-08-01', end_date='2019-08-30')

        self.request_header = {
            "extra": {
                "HTTP_X_REQUESTED_WITH": "XMLHttpRequest"
            }
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_update_surcharge_view(self):
        data = {
            "surcharge_type": "percentage",
            "surcharge": 50,
        }
        url = self.reverse(self.url_string, pk=self.surcharge_a.pk)
        self.login(email=self.user.email, password='test')
        response = self.post(url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)
