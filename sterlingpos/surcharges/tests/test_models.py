from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.surcharges.models import Surcharge, _SURCHARGE_TYPE
from sterlingpos.core.models import set_current_tenant


class TestSurcharge(TestCase):

    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.sales_order_option_a = mommy.make('sales.SalesOrderOption', name='Take Away',
                                               account=self.account)
        self.surcharge = Surcharge.objects.create(name='Fixed 3000', surcharge_fixed_price=3000,
                                                  surcharge_type=_SURCHARGE_TYPE.fixed_price,
                                                  order_option=self.sales_order_option_a,
                                                  account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        self.assertEqual(
            self.surcharge.__str__(),
            "{} {} {}".format(self.surcharge.name, self.surcharge.surcharge_type, self.surcharge.surcharge)
        )

    def test_surcharge_amount(self):
        surcharge, _ = Surcharge.objects.get_or_create(
            name=self.surcharge.name,
            account=self.account
        )

        self.assertEqual(surcharge.surcharge, 3000)

    def test_update_surcharge_amount(self):
        surcharge, _ = Surcharge.objects.get_or_create(
            name=self.surcharge.name,
            account=self.account
        )
        surcharge.surcharge = 2000
        surcharge.save()
        self.assertEqual(surcharge.surcharge, 2000)

    def test_update_surcharge_type(self):
        surcharge, _ = Surcharge.objects.get_or_create(
            name=self.surcharge.name,
            account=self.account
        )
        surcharge.surcharge_type = _SURCHARGE_TYPE.percentage
        surcharge.surcharge = 3
        surcharge.save()
        self.assertEqual(surcharge.surcharge_type, _SURCHARGE_TYPE.percentage)
