from django.test import TestCase
from model_mommy import mommy
from sterlingpos.surcharges.forms import SurchargeForm
from sterlingpos.accounts.models import Account
from sterlingpos.surcharges.models import Surcharge, _SURCHARGE_TYPE
from sterlingpos.core.models import set_current_tenant


class TestSurchargeConfigurationForm(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        self.sales_order_option_a = mommy.make('sales.SalesOrderOption', name='Take Away',
                                          account=self.account)
        self.sales_order_option_b = mommy.make('sales.SalesOrderOption', name='Go Food',
                                          account=self.account
                                          )
        self.surcharge_1 = Surcharge.objects.create(name='Fixed 3000', surcharge=3000,
                                                    surcharge_type=_SURCHARGE_TYPE.fixed_price,
                                                    order_option=self.sales_order_option_a,
                                                    account=self.account)
        self.surcharge_2 = Surcharge.objects.create(name='Percen 3%', surcharge=3,
                                                    surcharge_type=_SURCHARGE_TYPE.percentage,
                                                    order_option=self.sales_order_option_b,
                                                    account=self.account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save_surcharge(self):
        valid_data = {
            'name': 'Fixed 1000',
            'surcharge_type': _SURCHARGE_TYPE.fixed_price,
            'surcharge': 1000,
            'order_option': self.sales_order_option_a.pk,
        }
        form = SurchargeForm(valid_data, instance=self.surcharge_1)
        self.assertTrue(form.is_valid(), form.errors)
        surcharge = form.save()
        self.assertEqual(surcharge.surcharge_type, _SURCHARGE_TYPE.fixed_price)

    def test_configuration_surcharge_percentage(self):
        valid_data = {
            'name': 'Disc 30',
            'surcharge_type': _SURCHARGE_TYPE.percentage,
            'surcharge': 30,
            'order_option': self.sales_order_option_b.pk,
        }
        form = SurchargeForm(valid_data, instance=self.surcharge_2)
        self.assertTrue(form.is_valid(), form.errors)
        surcharge = form.save()
        self.assertEqual(surcharge.surcharge_type, _SURCHARGE_TYPE.percentage)
        self.assertEqual(surcharge.surcharge_percentage, 30.00)
        self.assertEqual(surcharge.surcharge_fixed_price, None)
        self.assertEqual(surcharge.surcharge, 30.00)

    def test_configuration_surcharge_fixed_price(self):
        valid_data = {
            'name': 'Fixed 1500',
            'surcharge_type': _SURCHARGE_TYPE.fixed_price,
            'surcharge': 1500,
            'order_option': self.sales_order_option_b.pk,
        }
        form = SurchargeForm(valid_data, instance=self.surcharge_2)
        self.assertTrue(form.is_valid(), form.errors)
        surcharge = form.save()
        self.assertEqual(surcharge.surcharge_type, _SURCHARGE_TYPE.fixed_price)
        self.assertEqual(surcharge.surcharge_fixed_price, 1500.00)
        self.assertEqual(surcharge.surcharge_percentage, None)
        self.assertEqual(surcharge.surcharge, 1500.00)

    def test_name_invalid(self):
        valid_data = {
            'name': '',
            'surcharge_type': _SURCHARGE_TYPE.fixed_price,
            'surcharge': 1500,
            'order_option': self.sales_order_option_a.pk,
        }
        form = SurchargeForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('name', form.errors)

    def test_surcharge_invalid(self):
        valid_data = {
            'name': 'Surcharge with empty value',
            'surcharge_type': _SURCHARGE_TYPE.fixed_price,
            'surcharge': '',
            'order_option': self.sales_order_option_b.pk,
        }
        form = SurchargeForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('surcharge', form.errors)

    def test_surcharge_percentage_invalid_value(self):
        valid_data = {
            'name': 'Surcharge with invalid value',
            'surcharge_type': _SURCHARGE_TYPE.percentage,
            'surcharge': 150,
            'order_option': self.sales_order_option_a.pk,
        }
        form = SurchargeForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_surcharge_with_null_date(self):
        valid_data = {
            'name': 'Surcharge with no date',
            'surcharge_type': _SURCHARGE_TYPE.percentage,
            'surcharge': 85,
            'order_option': self.sales_order_option_b.pk,
        }
        form = SurchargeForm(valid_data, instance=self.surcharge_2)
        self.assertTrue(form.is_valid(), form.errors)
        surcharge = form.save()
        self.assertEqual(surcharge.start_date, None)
        self.assertEqual(surcharge.end_date, None)
