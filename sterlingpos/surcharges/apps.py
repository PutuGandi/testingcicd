from django.apps import AppConfig


class SurchargeConfig(AppConfig):
    name = 'sterlingpos.surcharges'
    verbose_name = 'Surcharges'

    def ready(self):
        try:
            import surcharges.signals
        except ImportError:
            pass
