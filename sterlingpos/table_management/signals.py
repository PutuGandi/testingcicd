from django.conf import settings
from django.dispatch import Signal
from sterlingpos.table_management.models import TableActivity, TableActivityLog


def update_table_activity_handler(sender, **kwargs):

    instance = kwargs.get('instance')
    created = kwargs.get('created')

    if created and instance.status == TableActivityLog.STATUS.unavailable:
        table_activity = TableActivity(
            table=instance.table,
            start_date=instance.activity_date,
            customer_count=instance.customer_count,
        )
        table_activity.save()

    if created and instance.status == TableActivityLog.STATUS.available:
        table_activity = instance.table.tableactivity_set.filter(end_date=None).last()
        transaction_count = instance.table.salesorder_set.filter(
            transaction_date__gte=table_activity.start_date,
            transaction_date__lte=instance.activity_date).count()
        table_activity.end_date = instance.activity_date
        table_activity.transaction_count = transaction_count
        table_activity.save()
