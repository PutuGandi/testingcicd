from django.test import TestCase
from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.table_management.forms import (
    TableManagementSettingsForm,
)


class TestTableManagementSettingsForm(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account')
        self.outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1', account=self.account)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', branch_id='branch-2', account=self.account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        form_data = {
            'is_enabled': True,
            'outlet': [self.outlet.pk],
        }

        form = TableManagementSettingsForm(form_data)
        self.assertTrue(form.is_valid())
        table_management_setting = form.save()

        self.assertTrue(table_management_setting.is_enabled)
        self.assertEqual(table_management_setting.outlet.count(), 1)
        self.assertFalse(
            table_management_setting.outlet.filter(pk=self.outlet_2.pk).exists())
        self.assertEqual(
            table_management_setting.color_display,
            'normal')

    def test_save_fill_mode(self):
        form_data = {
            'fill_mode': True,
        }

        form = TableManagementSettingsForm(form_data)
        self.assertTrue(form.is_valid())
        table_management_setting = form.save()

        self.assertEqual(
            table_management_setting.color_display,
            'fill')
