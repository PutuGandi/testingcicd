from datetime import datetime, timedelta

from django.utils import timezone
from django.test import TestCase
from django.db import DataError

from model_mommy import mommy
from sterlingpos.core.models import set_current_tenant
from sterlingpos.table_management.models import (
    TableArea,
    Table,
    TableActivity,
    TableActivityLog,
    ColorStatus,
)

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


class TestTableArea(TestCase):

    def test__str__(self):
        account = mommy.make('accounts.Account', name='Account-1')
        outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=account)

        table_area = TableArea.objects.create(
            name="Floor 1",
            outlet=outlet,
            account=account,
        )
        self.assertEqual(
            table_area.__str__(),
            'Table Area Floor 1 - Account-1'
        )


class TestTable(TestCase):

    def setUp(self):
        self.start_date = timezone.now()
        self.end_date = self.start_date + timedelta(hours=1)
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=self.account)
        self.table_area = TableArea.objects.create(
            name='Floor 1', outlet=self.outlet, account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        table = Table.objects.create(
            table_area=self.table_area,
            name="Table 1",
            pax=4,
            account=self.account,
        )
        self.assertEqual(
            table.__str__(),
            'Table Table 1'
        )

    def test_is_available(self):
        table = Table.objects.create(
            table_area=self.table_area,
            name="Table 1",
            pax=4,
            account=self.account,
        )
        self.assertTrue(table.is_available)

        table_activity_log = TableActivityLog.objects.create(
            table=table,
            table_name=table.name,
            outlet_name=table.table_area.outlet.name,
            activity_date=self.start_date,
            customer_count=1,
            account=self.account,
        )
        self.assertFalse(table.is_available)

        table_activity_log = TableActivityLog.objects.create(
            table=table,
            table_name=table.name,
            outlet_name=table.table_area.outlet.name,
            activity_date=self.end_date,
            status='available',
            customer_count=1,
            account=self.account,
        )
        self.assertTrue(table.is_available)


class TestTableActivity(TestCase):

    def test__str__(self):
        start_date = timezone.now()
        end_date = start_date + timedelta(hours=1)
        account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(account)
        outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=account)
        table_area = TableArea.objects.create(
            name='Floor 1', outlet=outlet, account=account)
        table = Table.objects.create(
            table_area=table_area,
            name="Table 1",
            pax=4,
        )
        table_activity = TableActivity.objects.create(
            table=table,
            start_date=start_date,
            end_date=end_date,
            customer_count=1,
            transaction_count=1,
        )
        self.assertEqual(
            table_activity.__str__(),
            'Table Activity Table Table 1 {} s/d {} (1 transaction(s), 1 customer(s))'.format(
                datetime.strftime(start_date, '%Y-%m-%d %H:%M:%S'),
                datetime.strftime(end_date, '%Y-%m-%d %H:%M:%S'))
        )

    def tearDown(self):
        set_current_tenant(None)


class TestTableActivityLog(TestCase):

    def setUp(self):
        self.start_date = timezone.now()
        self.end_date = self.start_date + timedelta(hours=1)
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=self.account)
        self.table_area = TableArea.objects.create(
            name='Floor 1', outlet=self.outlet, account=self.account)
        self.table = Table.objects.create(
            table_area=self.table_area,
            name="Table 1",
            pax=4,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):

        table_activity_log = TableActivityLog.objects.create(
            table=self.table,
            table_name=self.table.name,
            outlet_name=self.table.table_area.outlet.name,
            activity_date=self.start_date,
            customer_count=1,
        )
        self.assertEqual(
            table_activity_log.__str__(),
            'Table Activity Log Table 1 Outlet 1 - unavailable'
        )

    def test_save_table_activity_log_as_available_when_its_not_unavailable(self):

        table_activity_log = TableActivityLog(
            table=self.table,
            table_name=self.table.name,
            outlet_name=self.table.table_area.outlet.name,
            activity_date=self.start_date,
            status='available',
            customer_count=1,
        )
        with self.assertRaises(DataError):
            table_activity_log.save()

    def test_save_table_activity_log_as_unvailable_when_its_not_available(self):

        table_activity_log = TableActivityLog.objects.create(
            table=self.table,
            table_name=self.table.name,
            outlet_name=self.table.table_area.outlet.name,
            activity_date=self.start_date,
            customer_count=1,
        )
        table_activity_log_2 = TableActivityLog(
            table=self.table,
            table_name=self.table.name,
            outlet_name=self.table.table_area.outlet.name,
            activity_date=self.start_date,
        )
        with self.assertRaises(DataError):
            table_activity_log_2.save()

    def test_signal_update_table_activity_after_save(self):

        table_activity_log = TableActivityLog.objects.create(
            table=self.table,
            table_name=self.table.name,
            outlet_name=self.table.table_area.outlet.name,
            activity_date=self.start_date,
            customer_count=1,
        )

        sales_order = mommy.make(
            'sales.SalesOrder',
            account=self.account, outlet=self.outlet, table=self.table,
            transaction_date=self.start_date,
            __fill_optional=True)
        sales_order_2 = mommy.make(
            'sales.SalesOrder',
            account=self.account, outlet=self.outlet, table=self.table,
            transaction_date=self.start_date + timedelta(minutes=30),
            __fill_optional=True)

        table_activity_log_2 = TableActivityLog.objects.create(
            table=self.table,
            table_name=self.table.name,
            outlet_name=self.table.table_area.outlet.name,
            activity_date=self.end_date,
            status='available',
        )

        self.assertEqual(self.table.tableactivity_set.count(), 1)
        table_activity = self.table.tableactivity_set.first()
        self.assertEqual(table_activity.start_date, self.start_date)
        self.assertEqual(table_activity.end_date, self.end_date)
        self.assertEqual(table_activity.customer_count, 1)
        self.assertEqual(table_activity.transaction_count, 2)


class TestColorStatus(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        obj = ColorStatus.objects.create(
            table_management_setting=self.account.tablemanagementsettings_set.first(),
            name="Table 1",
            color_code="#ffffff",
            account=self.account,
        )
        self.assertEqual(
            obj.__str__(),
            'Color Status Table 1 - 0 minutes [custom] (#ffffff)'
        )

    def test_set_text_color_black_on_non_fill(self):
        self.account.tablemanagementsettings_set.update(color_display='normal')
        obj = ColorStatus.objects.create(
            table_management_setting=self.account.tablemanagementsettings_set.first(),
            name="Table 1",
            color_code="#ffffff",
            account=self.account,
        )
        self.assertEqual(
            obj.text_color,
            '#353749'
        )

    def test_set_text_color_white_on_non_yellow_fill(self):
        self.account.tablemanagementsettings_set.update(color_display='fill')
        obj = ColorStatus.objects.create(
            table_management_setting=self.account.tablemanagementsettings_set.first(),
            name="Table 1",
            color_code="#f9a633",
            account=self.account,
        )

        self.assertEqual(
            obj.text_color,
            '#ffffff'
        )

    def test_set_text_color_black_on_yellow_fill(self):
        self.account.tablemanagementsettings_set.update(color_display='fill')
        obj = ColorStatus.objects.create(
            table_management_setting=self.account.tablemanagementsettings_set.first(),
            name="Table 1",
            color_code="#f5ce33",
            account=self.account,
        )
        self.assertEqual(
            obj.text_color,
            '#353749'
        )