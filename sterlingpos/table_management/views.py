import copy
from decimal import Decimal, ROUND_DOWN
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta, MO, SU

from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.db.models.functions import Coalesce
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.db.models import (
    Prefetch, Sum, Count, Q, F, Func, Value, Avg
)
from django.views.generic import (
    TemplateView, ListView, DetailView, UpdateView, DeleteView, View
)

from django_datatables_view.base_datatable_view import BaseDatatableView

from braces.views import LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin

from sterlingpos.core.models import get_current_tenant
from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.sales.models import SalesOrder
from .forms import (
    TableManagementSettingsForm,
    ColorStatusFormSet,
    FixedColorStatusFormSet,
    EnableTableManagementSettingsForm,
    TableManagementOutletSettingsForm
)
from .models import (
    TableArea, Table, TableActivity,
    TableManagementSettings,
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user


class TableAreaListJson(SterlingRoleMixin, CSVExportMixin, DatatableMixins, BaseDatatableView):
    columns = ['name', 'outlet.name', 'active', 'table_count', 'action']
    order_columns = ['name', 'outlet__name', 'active', 'table_count', 'action']

    model = TableArea

    csv_filename = 'table_area'
    csv_header = ['name', 'outlet.display_name', 'active', 'table_count']

    def get_initial_queryset(self):
        qs = super(TableAreaListJson, self).get_initial_queryset()
        qs = qs.select_related(
            'outlet'
        ).annotate(
            table_count=Count('table', filter=Q(table__deleted__isnull=True))
        )

        return qs.exclude(archived=True)

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'edit': reverse_lazy("table_management:table_area_detail", kwargs={'pk': row.pk}),
                'remove': reverse_lazy("table_management:table_area_delete", kwargs={'pk': row.pk}),
                'archived': row.archived
            }
            return action_data
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "table_management:table_area_detail",
                    kwargs={'pk': row.pk}),
                escape(row.name.upper()))
        elif column == 'outlet.name':
            return '{}'.format(escape(row.outlet.name.upper()))
        elif column == 'active':
            if row.active:
                return '{}'.format(_('Active'))
            else:
                return '{}'.format(_('Inactive'))
        return super(TableAreaListJson, self).render_column(row, column)


class TableAreaListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/table_management/table_area_list.html'


class TableAreaCreateView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/table_management/table_area_detail.html'


class TableAreaDetailView(SterlingRoleMixin, DetailView):
    model = TableArea
    template_name = 'dashboard/table_management/table_area_detail.html'


class TableAreaDeleteView(SterlingRoleMixin, DeleteView):
    template_name = 'dashboard/table_management/table_area_delete.html'
    model = TableArea
    success_url = reverse_lazy("table_management:table_area_list")

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.archived = True
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_queryset(self):
        qs = super(TableAreaDeleteView, self).get_queryset()
        qs = qs.filter(archived=False)
        return qs


class TableManagementReportTemplateView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/report/table_management_report.html'

    def get_diff_percentage(self, current_value, compared_value):
        if compared_value == 0:
            diff_percentage = 0
        else:
            diff_percentage = ((Decimal(current_value) - Decimal(compared_value)) / Decimal(compared_value)) * 100
            diff_percentage = diff_percentage.quantize(Decimal('.01'), rounding=ROUND_DOWN)
        return diff_percentage

    def get_context_data(self, **kwargs):
        context = super(TableManagementReportTemplateView, self).get_context_data(**kwargs)
        outlet_pk = kwargs.get('outlet_pk')
        outlet_qs = get_outlet_of_user(self.request.user)

        if outlet_pk:
            outlet = get_object_or_404(
                outlet_qs,
                pk=outlet_pk)
            context['outlet'] = outlet
        else:
            outlet = None

        try:
            start_date = datetime.strptime(
                self.request.GET.get('startdate', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)

        try:
            end_date = datetime.strptime(
                self.request.GET.get('enddate', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hours=23, minutes=59)

        date_filter = self.request.GET.get('date', None)
        if date_filter == 'today':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
            end_date = start_date + relativedelta(hour=23, minute=59)
        elif date_filter == 'yesterday':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) - relativedelta(days=1)
            end_date = start_date + relativedelta(hour=23, minute=59)
        elif date_filter == 'last_week':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-2))
            end_date = start_date + relativedelta(weekday=SU(1), hour=23, minute=59)
        elif date_filter == 'last_month':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) + relativedelta(day=1, months=-1)
            end_date = start_date + relativedelta(day=1, months=+1, days=-1, hour=23, minute=59)
        elif date_filter == 'week_to_date':
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
            end_date = datetime.today().replace(hour=23, minute=59)
        elif date_filter == 'month_to_date':
            start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
            end_date = datetime.today().replace(hour=23, minute=59)

        table_usage = Table.objects.filter(archived=False)
        table_area_usage = TableArea.objects.filter(archived=False, active=True)

        table_activities_comp = table_activities = TableActivity.objects.all()

        if start_date:
            table_activities = table_activities.filter(start_date__gte=start_date)

        if end_date:
            table_activities = table_activities.filter(end_date__lte=end_date)

        if outlet:
            table_activities = table_activities.filter(table__table_area__outlet=outlet)
            table_area_usage = table_area_usage.filter(outlet=outlet)
            table_usage = table_usage.filter(table_area__outlet=outlet)

        time_delta = end_date - start_date
        start_date_comp = start_date - timedelta(days=time_delta.days or 1)

        table_activities_comp = table_activities_comp.filter(
            start_date__gte=start_date_comp, end_date__lte=start_date)

        average_usage_per_session = table_activities.annotate(
            duration=Func(F('end_date'), F('start_date'), function='age')
        ).aggregate(
            average_usage_per_session=Coalesce(Avg('duration'), Value(timedelta()))
        ).get('average_usage_per_session')
        average_usage_per_session = average_usage_per_session.seconds / 60

        average_usage_per_session_comp = table_activities_comp.annotate(
            duration=Func(F('end_date'), F('start_date'), function='age')
        ).aggregate(
            average_usage_per_session=Coalesce(Avg('duration'), Value(timedelta()))
        ).get('average_usage_per_session')
        average_usage_per_session_comp = average_usage_per_session_comp.seconds / 60

        total_customer_served = table_activities.aggregate(
            total_customer_served=Coalesce(Sum('customer_count'), Value(0))
        ).get('total_customer_served')
        total_customer_served_comp = table_activities_comp.aggregate(
            total_customer_served=Coalesce(Sum('customer_count'), Value(0))
        ).get('total_customer_served')

        total_customer_served = table_activities.aggregate(
            total_customer_served=Coalesce(Sum('customer_count'), Value(0))
        ).get('total_customer_served')
        total_customer_served_comp = table_activities_comp.aggregate(
            total_customer_served=Coalesce(Sum('customer_count'), Value(0))
        ).get('total_customer_served')

        average_order = table_activities.aggregate(
            transaction_count=Coalesce(Avg('transaction_count'), Value(0))
        ).get('transaction_count')
        average_order_comp = table_activities_comp.aggregate(
            transaction_count=Coalesce(Avg('transaction_count'), Value(0))
        ).get('transaction_count')

        table_area_usage = table_area_usage.annotate(
            table_activities=Count('table__tableactivity')
        ).order_by('-table_activities')[:5]

        table_usage_duration = table_usage.annotate(
            duration=Func(
                F('tableactivity__end_date'),
                F('tableactivity__start_date'), function='age')
        ).annotate(
            duration_sum=Coalesce(Sum('duration'), Value(timedelta()))
        ).order_by('-duration_sum')[:5]

        table_usage = table_usage.annotate(
            table_activities=Count('tableactivity')
        ).order_by('-table_activities')[:5]

        context.update({
            'start_date': start_date,
            'end_date': end_date,

            'average_usage_per_session': average_usage_per_session,
            'average_usage_per_session_comp': average_usage_per_session_comp,
            'average_usage_per_session_diff': self.get_diff_percentage(
                average_usage_per_session, average_usage_per_session_comp),

            'total_customer_served': total_customer_served,
            'total_customer_served_comp': total_customer_served_comp,
            'total_customer_served_diff': self.get_diff_percentage(
                total_customer_served, total_customer_served_comp),

            'average_order': average_order,
            'average_order_comp': average_order_comp,
            'average_order_diff': self.get_diff_percentage(
                average_order, average_order_comp),

            'table_usage_duration': table_usage_duration,
            'table_area_usage': table_area_usage,
            'table_usage': table_usage,
        })

        return context


class TableManagementSettingsUpdateView(SterlingRoleMixin, UpdateView):
    model = TableManagementSettings
    template_name = "dashboard/table_management/settings.html"
    form_class = TableManagementSettingsForm
    formset_class = ColorStatusFormSet
    fixed_formset_class = FixedColorStatusFormSet

    def get_form_kwargs(self, **kwargs):
        kwargs = super(TableManagementSettingsUpdateView, self).get_form_kwargs()

        formset_kwargs = copy.copy(kwargs)
        del formset_kwargs['initial']

        self.formsets = {
            'formset': self.get_formset(
                formset_kwargs=formset_kwargs),
            'fixed_formset': self.get_formset(
                self.fixed_formset_class, formset_kwargs=formset_kwargs, prefix='fixed')
        }
        kwargs['formsets'] = self.formsets
        return kwargs

    def get_formset(self, class_name=None, formset_kwargs={}, **extra_kwargs):

        if extra_kwargs:
            formset_kwargs.update(extra_kwargs)

        if class_name:
            formset = class_name(**formset_kwargs)
            return formset

        return self.formset_class(**formset_kwargs)

    def get_success_url(self):
        success_url = reverse("table_management:settings")
        return success_url

    def get_object(self, queryset=None):
        obj = self.request.user.account.tablemanagementsettings_set.first()
        return obj

    def get_initial(self):
        outlet = self.object.outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(outlet)

        initial = {
            'all_outlet': disabled_outlet.count() == 0,
            'outlet': outlet
        }
        return initial

    def formsets_all_valid(self):
        valid = True
        if self.formsets is not None:
            for key, formset in self.formsets.items():
                valid = valid and formset.is_valid()
        return valid

    def form_valid(self, form):

        if not self.formsets_all_valid():
            return self.form_invalid(form)

        response = super(TableManagementSettingsUpdateView, self).form_valid(form)

        for formset in form._formsets:
            form._formsets[formset].save()

        messages.success(self.request, '{}'.format(
            _('Table Management has been updated.')))
        return response


class TableManagementOutletSettingsUpdateView(SterlingRoleMixin, UpdateView):
    model = TableManagementSettings
    template_name = "dashboard/table_management/outlet_settings.html"
    form_class = TableManagementOutletSettingsForm

    def get_object(self, queryset=None):
        obj = self.request.user.account.tablemanagementsettings_set.first()
        return obj

    def get_success_url(self):
        success_url = reverse('table_management:settings_outlet')
        return success_url

    def get_initial(self):
        outlet = self.object.outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(outlet)
        initial = {
            'all_outlet': disabled_outlet.count() == 0,
            'outlet': outlet
        }
        return initial

    def form_valid(self, form):
        response = super(TableManagementOutletSettingsUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Table Management settings has been updated.')))
        return response


class TableManagementSettingsUpdateViewJSON(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_object(self):
        obj = self.request.user.account.tablemanagementsettings_set.first()
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }

        form = EnableTableManagementSettingsForm(
            data=request.POST,
            instance=self.get_object()
        )

        if form.is_valid():
            table_settings = form.save()
            status_code = 200
            response['status'] = 'success'
            response['data'] = table_settings.is_enabled
        else:
            response['data'] = form.errors
            status_code = 400

        return self.render_json_response(response, status=status_code)


class TableManagementDisableView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/table_management/disable.html'
