# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "table_management"
urlpatterns = [
    url(regex=r'^report/$', view=views.TableManagementReportTemplateView.as_view(),
        name='table_report'),
    url(regex=r'^report/outlet/(?P<outlet_pk>\d+)/$', view=views.TableManagementReportTemplateView.as_view(),
        name='table_report'),

    url(regex=r'^settings/$', view=views.TableManagementSettingsUpdateView.as_view(),
        name='settings'),
    url(r'^settings/enable/$', view=views.TableManagementSettingsUpdateViewJSON.as_view(),
        name='settings_enable'),
    url(regex=r'^settings/outlet/$', view=views.TableManagementOutletSettingsUpdateView.as_view(),
        name='settings_outlet'),
    url(regex=r'^settings/disable/$', view=views.TableManagementDisableView.as_view(),
        name='settings_disable'),

    url(regex=r'^table-area/$', view=views.TableAreaListView.as_view(),
        name='table_area_list'),
    url(regex=r'^table-area/create/$', view=views.TableAreaCreateView.as_view(),
        name='table_area_create'),
    url(regex=r'^table-area/(?P<pk>\d+)/$', view=views.TableAreaDetailView.as_view(),
        name='table_area_detail'),
    url(regex=r'^table-area/(?P<pk>\d+)/delete/$', view=views.TableAreaDeleteView.as_view(),
        name='table_area_delete'),

    url(regex=r'^table-area/data/$', view=views.TableAreaListJson.as_view(),
        name='data_table_area'),

]

