# Generated by Django 2.0.4 on 2019-08-07 05:34

from django.db import migrations, models


def migrate_text_color(apps, schema_editor):
    ColorStatus = apps.get_model("table_management", "ColorStatus")
    ColorStatus.objects.update(
        text_color="#353749"
    )

class Migration(migrations.Migration):

    dependencies = [
        ('table_management', '0014_auto_20190621_1236'),
    ]

    operations = [
        migrations.AddField(
            model_name='colorstatus',
            name='text_color',
            field=models.CharField(blank=True, max_length=15, verbose_name='Text Color'),
        ),
        migrations.AddField(
            model_name='tablemanagementsettings',
            name='color_display',
            field=models.CharField(choices=[('normal', 'Normal'), ('fill', 'Fill')], default='normal', max_length=15, verbose_name='Color Display'),
        ),
        migrations.RunPython(
            migrate_text_color, migrations.RunPython.noop),
    ]
