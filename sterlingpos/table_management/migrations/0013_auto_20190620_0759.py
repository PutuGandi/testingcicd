# Generated by Django 2.0.4 on 2019-06-20 07:59

from django.db import migrations, models
import django.db.models.deletion
import sterlingpos.core.models


def migrate_color_status(apps, schema_editor):
    TableManagementSettings = apps.get_model("table_management", "TableManagementSettings")

    for table_management_setting in TableManagementSettings.objects.all():
        table_management_setting.colorstatus_set.get_or_create(
            name="Open",
            status_type="open",
            account=table_management_setting.account
        )
        table_management_setting.colorstatus_set.get_or_create(
            name="Occupied",
            status_type="occupied",
            color_code="#ff6259",
            account=table_management_setting.account
        )


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_account_registered_via'),
        ('table_management', '0012_auto_20190610_1014'),
    ]

    operations = [
        migrations.CreateModel(
            name='ColorStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('name', models.CharField(blank=True, max_length=255, verbose_name='Status Name')),
                ('color_code', models.CharField(choices=[('red', 'Red'), ('blue', 'Blue'), ('green', 'Green'), ('white', 'White'), ('black', 'Black')], max_length=15, verbose_name='Color')),
                ('status_type', models.CharField(choices=[('open', 'Open'), ('occupied', 'Occupied'), ('custom', 'Custom')], default='custom', max_length=15, verbose_name='Status Type')),
                ('status_time', models.IntegerField(default=0, verbose_name='Status Time')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='tablemanagementsettings',
            name='display_color_status',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='colorstatus',
            name='table_management_setting',
            field=sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, to='table_management.TableManagementSettings'),
        ),
        migrations.AlterUniqueTogether(
            name='colorstatus',
            unique_together={('account', 'id')},
        ),
        migrations.RunPython(
            migrate_color_status, migrations.RunPython.noop),
    ]
