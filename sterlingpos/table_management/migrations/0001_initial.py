# Generated by Django 2.0.4 on 2018-07-31 07:27

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import sterlingpos.core.models
import sterlingpos.table_management.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounts', '0003_auto_20180504_0849'),
        ('outlet', '0003_auto_20180704_0527'),
    ]

    operations = [
        migrations.CreateModel(
            name='Table',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(blank=True, max_length=255, verbose_name='Table Name')),
                ('pax', models.SmallIntegerField(verbose_name='Pax')),
                ('shape', models.CharField(choices=[('square', 'Square'), ('circle', 'Circle'), ('rectangle', 'Rectangle')], default='square', max_length=15, verbose_name='Table Shape')),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TableActivity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField(db_index=True, verbose_name='Table Usage Start')),
                ('end_date', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Table Usage End')),
                ('customer_count', models.SmallIntegerField(default=0, verbose_name='Customer Count')),
                ('trasaction_count', models.SmallIntegerField(default=0, verbose_name='Transaction Count')),
                ('table', sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, to='table_management.Table')),
            ],
            options={
                'verbose_name': 'TableActivity',
                'verbose_name_plural': 'TableActivities',
            },
        ),
        migrations.CreateModel(
            name='TableActivityLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('table_name', models.CharField(blank=True, max_length=255, verbose_name='Table Name')),
                ('outlet_name', models.CharField(blank=True, max_length=255, verbose_name='Outlet Name')),
                ('status', models.CharField(choices=[('unavailable', 'Unavailable'), ('available', 'Available')], default='unavailable', max_length=20, verbose_name='Table Status')),
                ('activity_date', models.DateTimeField(db_index=True)),
                ('customer_count', models.SmallIntegerField(default=0, verbose_name='Customer Count')),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('table', sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, to='table_management.Table')),
            ],
            options={
                'verbose_name': 'TableActivityLog',
                'verbose_name_plural': 'TableActivityLogs',
            },
        ),
        migrations.CreateModel(
            name='TableArea',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(blank=True, max_length=255, verbose_name='Table Area Name')),
                ('active', models.BooleanField(default=True, verbose_name='Is Active')),
                ('layout_image', models.ImageField(blank=True, null=True, upload_to=sterlingpos.table_management.models.get_upload_path, verbose_name='Layout Image')),
                ('archived', models.BooleanField(default=False)),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('outlet', sterlingpos.core.models.SterlingTenantForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='outlet.Outlet')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='table',
            name='table_area',
            field=sterlingpos.core.models.SterlingTenantForeignKey(on_delete=django.db.models.deletion.CASCADE, to='table_management.TableArea'),
        ),
    ]
