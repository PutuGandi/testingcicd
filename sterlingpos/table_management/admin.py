from django.contrib import admin

from .models import (
    TableArea,
    Table,
    TableActivity,
    TableActivityLog,
    TableManagementSettings,
)


class TableInline(admin.TabularInline):
    model = Table


class TableManagementSettingsAdmin(admin.ModelAdmin):
    """
        Admin View for TableManagementSettings
    """
    list_display = (
        "account", "prefix",
        "is_enabled", "seat_management")
    list_filter = ("is_enabled", "seat_management",)
    search_fields = ("account__name", "prefix")


class TableAreaAdmin(admin.ModelAdmin):
    """
        Admin View for TableArea
    """
    list_display = (
        "name", "outlet", "active",
        "archived", "account")
    list_filter = ("active", "archived")
    search_fields = (
        "name", "outlet__name", "account__name")
    inlines = (
        TableInline,
    )

    def get_queryset(self, request):
        qs = super(TableAreaAdmin, self).get_queryset(request)
        qs = qs.select_related(
            "account",
            "outlet")
        return qs


class TableAdmin(admin.ModelAdmin):
    """
        Admin View for TableArea
    """
    list_display = (
        "name", "pax", "shape", "view_table_area", "view_outlet", "account")
    list_filter = ("shape",)
    search_fields = (
        "name", "shape",
        "table_area__name",
        "table_area__outlet__name",
        "account__name")

    def view_outlet(self, obj):
        return obj.table_area.outlet.name

    def view_table_area(self, obj):
        return obj.table_area.name

    def get_queryset(self, request):
        qs = super(TableAdmin, self).get_queryset(request)
        qs = qs.select_related(
            "account",
            "table_area",
            "table_area__outlet")
        return qs


class TableActivityAdmin(admin.ModelAdmin):
    """
        Admin View for TableActivity
    """
    list_display = (
        "table", "view_table_area", "view_outlet", "created", "start_date", "end_date",
        "customer_count", "transaction_count", "account")
    list_filter = ("created", "start_date", "end_date",)
    search_fields = (
        "table__name", "account__name",
        "table__table_area__name",
        "table__table_area__outlet__name")

    def view_outlet(self, obj):
        return obj.table.table_area.outlet.name

    def view_table_area(self, obj):
        return obj.table.table_area.name

    def get_queryset(self, request):
        qs = super(TableActivityAdmin, self).get_queryset(request)
        qs = qs.select_related(
            "account",
            "table__table_area",
            "table__table_area__outlet")
        return qs


class TableActivityLogAdmin(admin.ModelAdmin):
    """
        Admin View for TableActivityLog
    """
    list_display = (
        "table_name", "outlet_name", "customer_count", "created",
        "status", "activity_date", "account")
    list_filter = ("status", "created", "activity_date")
    search_fields = (
        "table_name", "outlet_name", "status", "account__name")

admin.site.register(TableArea, TableAreaAdmin)
admin.site.register(Table, TableAdmin)
admin.site.register(TableActivity, TableActivityAdmin)
admin.site.register(TableActivityLog, TableActivityLogAdmin)
admin.site.register(TableManagementSettings, TableManagementSettingsAdmin)
