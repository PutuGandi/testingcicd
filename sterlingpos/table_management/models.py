from __future__ import unicode_literals, absolute_import

import hashlib
import base64

from datetime import datetime

from django.db import models, DataError
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.core.models import SterlingTenantModel, SterlingTenantForeignKey


def get_upload_path(instance, filename):
    return '{}/table_area/outlet-{}/{}'.format(instance.account.name, instance.outlet.pk, filename)

def _decode_base64string(base64string):
    try:
        data = base64.urlsafe_b64decode(base64string)
        hashes, table_pk = data.decode("utf-8").split(".")
    except:
        return (None, None)
    return (hashes, table_pk)


class TableManagementSettings(SterlingTenantModel, TimeStampedModel):
    COLOR_DISPLAY = Choices(
        ('normal', _('Normal')),
        ('fill', _('Fill')),
    )

    prefix = models.CharField(_("Table Name Prefix"), blank=True, max_length=255)
    is_enabled = models.BooleanField(default=True)
    seat_management = models.BooleanField(default=True)
    display_color_status = models.BooleanField(default=False)
    color_display = models.CharField(
        _("Color Display"), max_length=15,
        choices=COLOR_DISPLAY, default=COLOR_DISPLAY.normal)

    outlet = models.ManyToManyField(
        "outlet.Outlet", blank=True,
        through='TableManagementOutlet',
        related_name='table_management_setting')

    order_option = SterlingTenantForeignKey(
        "sales.SalesOrderOption", blank=True, null=True,
        on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "TableManagementSetting"
        verbose_name_plural = "TableManagementSettings"
        unique_together = (('account', 'id'),)


class ColorStatus(SterlingTenantModel):

    STATUS_TYPE = Choices(
        ('open', _('Open')),
        ('occupied', _('Occupied')),
        ('custom', _('Custom')),
    )

    table_management_setting = SterlingTenantForeignKey(
        TableManagementSettings, on_delete=models.CASCADE)
    name = models.CharField(_("Status Name"), blank=True, max_length=255)
    color_code = models.CharField(_("Color"), max_length=15, blank=True)
    text_color = models.CharField(_("Text Color"), max_length=15, blank=True)
    status_type = models.CharField(
        _("Status Type"), max_length=15,
        choices=STATUS_TYPE, default=STATUS_TYPE.custom)
    status_time = models.IntegerField(
        _("Status Time"), default=0
    )
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return "Color Status {} - {} minutes [{}] ({})".format(
            self.name,
            self.status_time,
            self.status_type,
            self.color_code,
        )

    def save(self, **kwargs):
        if self.table_management_setting.color_display == TableManagementSettings.COLOR_DISPLAY.fill and self.color_code in ["#f5ce33", "#ffffff"]:
            self.text_color = "#353749"
        elif self.table_management_setting.color_display == TableManagementSettings.COLOR_DISPLAY.fill:
            self.text_color = "#ffffff"
        else:
            self.text_color = "#353749"
        return super(ColorStatus, self).save(**kwargs)


class TableManagementOutlet(SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    table_management_setting = models.ForeignKey(
        TableManagementSettings, on_delete=models.CASCADE)
    outlet = models.ForeignKey("outlet.Outlet", on_delete=models.CASCADE)


class TableArea(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_("Table Area Name"), blank=True, max_length=255)
    outlet = SterlingTenantForeignKey(
        'outlet.Outlet', blank=True, null=True, on_delete=models.CASCADE)
    active = models.BooleanField(_("Is Active"), default=True)
    layout_image = models.ImageField(_("Layout Image"), upload_to=get_upload_path, blank=True, null=True)

    archived = models.BooleanField(default=False)

    class Meta:
        verbose_name = "TableArea"
        verbose_name_plural = "TableAreas"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "Table Area {} - {}".format(self.name, self.account.name)


class Table(SterlingTenantModel, TimeStampedModel):
    SHAPE = Choices(
        ('square', _('Square')),
        ('circle', _('Circle')),
        ('rectangle', _('Rectangle')),
    )

    table_area = SterlingTenantForeignKey('table_management.TableArea', on_delete=models.CASCADE)
    name = models.CharField(_("Table Name"), blank=True, max_length=255)
    suffix = models.CharField(_("Table Name Suffix"), blank=True, max_length=255)
    pax = models.SmallIntegerField(_("Pax"))
    shape = models.CharField(
        _("Table Shape"), max_length=15, choices=SHAPE, default=SHAPE.square)

    location_x = models.SmallIntegerField(_("Location X"), default=-1)
    location_y = models.SmallIntegerField(_("Location Y"), default=-1)

    width = models.SmallIntegerField(_("Width"), default=0)
    height = models.SmallIntegerField(_("Height"), default=0)

    archived = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Table"
        verbose_name_plural = "Tables"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "Table {}".format(self.name)

    def generate_hash(self):
        hashes = hashlib.sha224(
            bytes(
                f"public-menu::{self.pk}::{self.table_area.outlet.pk}::{self.account.pk}",
                "utf-8"
            )
        ).hexdigest()
        return hashes

    def generate_base64string(self):
        hashes = self.generate_hash()
        bytestring = bytes(f"{hashes}.{self.pk}", "utf-8")
        base64string = base64.urlsafe_b64encode(bytestring)
        return base64string.decode()
    
    def generate_url(self):
        base64string = self.generate_base64string()
        return reverse(
            "v2.1:public_outlet_product_list",
            kwargs={"outlet_base64": base64string}
        )

    @classmethod
    def get_from_base64string(cls, base64string):
        hashes, table_pk = _decode_base64string(base64string)
        try:
            obj = cls.objects.get(pk=table_pk)
            if hashes == obj.generate_hash():
                return obj
        except:
            pass

    @property
    def is_available(self):
        table_activity_log = self.tableactivitylog_set.last()
        if table_activity_log:
            is_available = table_activity_log.status == self.tableactivitylog_set.model.STATUS.available
        else:
            is_available = True
        return is_available


class TableActivity(SterlingTenantModel, TimeStampedModel):
    table = SterlingTenantForeignKey('table_management.Table', on_delete=models.CASCADE)
    start_date = models.DateTimeField(_("Table Usage Start"), db_index=True)
    end_date = models.DateTimeField(_("Table Usage End"), blank=True, null=True, db_index=True)
    customer_count = models.SmallIntegerField(_("Customer Count"), default=0)
    transaction_count = models.SmallIntegerField(_("Transaction Count"), default=0)

    class Meta:
        verbose_name = "TableActivity"
        verbose_name_plural = "TableActivities"
        unique_together = (('account', 'id'),)

    def __str__(self):
        start_date = datetime.strftime(self.start_date, '%Y-%m-%d %H:%M:%S')

        if self.end_date:
            end_date = datetime.strftime(self.end_date, '%Y-%m-%d %H:%M:%S')
        else:
            end_date = None

        return "Table Activity {} {} s/d {} ({} transaction(s), {} customer(s))".format(
            self.table,
            start_date,
            end_date,
            self.transaction_count, self.customer_count)


class TableActivityLog(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ('unavailable', _('Unavailable')),
        ('available', _('Available')),
    )

    table = SterlingTenantForeignKey('table_management.Table', on_delete=models.CASCADE)
    table_name = models.CharField(_("Table Name"), blank=True, max_length=255)
    outlet_name = models.CharField(_("Outlet Name"), blank=True, max_length=255)
    status = models.CharField(_("Table Status"), max_length=20, choices=STATUS, default=STATUS.unavailable)
    activity_date = models.DateTimeField(db_index=True)
    customer_count = models.SmallIntegerField(_("Customer Count"), default=0)

    class Meta:
        verbose_name = "TableActivityLog"
        verbose_name_plural = "TableActivityLogs"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "Table Activity Log {} {} - {}".format(
            self.table_name, self.outlet_name, self.status)

    def save(self, *args, **kwargs):

        if not self.pk and self.status == self.STATUS.available and (
                not self.__class__.objects.filter(table=self.table, status=self.STATUS.unavailable).exists() or
                self.__class__.objects.filter(table=self.table).last() and
                self.__class__.objects.filter(table=self.table).last().status != self.STATUS.unavailable):
            raise DataError(
                _("Table is already available. No Lastest Table Unavailable Activity Found."))

        if not self.pk and self.status == self.STATUS.unavailable and \
                self.__class__.objects.filter(table=self.table).last() and \
                self.__class__.objects.filter(table=self.table).last().status != self.STATUS.available:
            raise DataError(
                _("Table is already unavailable. No Lastest Table Available Activity Found."))

        return super(TableActivityLog, self).save(*args, **kwargs)
