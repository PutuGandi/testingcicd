from django import forms
from django.forms import inlineformset_factory, BaseInlineFormSet
from django.template.loader import render_to_string

from sterlingpos.core.bootstrap import KawnField, KawnFieldSetWithHelpText
from sterlingpos.outlet.models import Outlet

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, HTML

from sterlingpos.catalogue.forms import BaseFormsetLayout

from .models import (
    TableManagementSettings,
    ColorStatus,
    TableManagementOutlet
)
from django.utils.translation import ugettext_lazy as _


class ColorSelectWidget(forms.widgets.Select):
    template_name = 'widgets/color_select.html'
    option_template_name = 'widgets/color_select_option.html'


class EnableTableManagementSettingsForm(forms.ModelForm):
    class Meta:
        model = TableManagementSettings
        fields = ('is_enabled',)
        exclude = ('account', 'fill_mode', 'color_display', 'seat_management',
                   'prefix', 'display_color_status',)

    def __init__(self, *args, **kwargs):
        super(EnableTableManagementSettingsForm, self).__init__(*args,**kwargs)
        self.fields['is_enabled'].label = _('Enable Table Management')
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            KawnField('is_enabled', template='dashboard/table_management/switch_input/switch.html')
        )


    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        if not obj.is_enabled:
            obj.outlet.clear()
        else:
            outlets = [
                TableManagementOutlet(
                    outlet=outlet,
                    table_management_setting=obj,
                    account=obj.account
                )
                for outlet in obj.account.outlet_set.all()
            ]
            TableManagementOutlet.objects.bulk_create(outlets)
            obj.outlet.invalidated_update()
        return obj





class TableManagementOutletSettingsForm(forms.ModelForm):
    all_outlet = forms.BooleanField(
        initial=True, label="Semua Outlet"
    )
    fill_mode = forms.BooleanField(
        required=False, initial=False,
        label='Tampilkan dalam mode blok warna'
    )

    class Meta:
        model = TableManagementSettings
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        self.context = {}
        super(TableManagementOutletSettingsForm, self).__init__(*args, **kwargs)
        self.fields['outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['outlet'].label = ''
        self.fields['all_outlet'].required = False
        self.fields['color_display'].required = False
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout= Layout(
            KawnField("prefix", type='hidden'),
            KawnField('seat_management', type='hidden'),
            KawnField('display_color_status', type='hidden'),
            KawnField('fill_mode', type='hidden'),
            KawnField('color_display', type='hidden'),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet', template='dashboard/table_management/switch_input/float_right_switch.html'),
                KawnField('outlet', template='dashboard/table_management/switch_input/multiple_switch.html'),
                help_text=render_to_string('help_text/table_management_outlet.html')
            )

        )

    def update_m2m(self, table_management, object_list, rel_field, intermediate_model):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "table_management_setting": table_management,
                    "account": table_management.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        kwargs['commit'] = False
        obj = super(TableManagementOutletSettingsForm, self).save(*args, **kwargs)
        obj.save()
        self.update_m2m(
            obj, outlets, obj.outlet, TableManagementOutlet)
        return obj


class TableManagementSettingsForm(forms.ModelForm):
    all_outlet = forms.BooleanField(
        initial=True, label="Semua Lokasi")
    fill_mode = forms.BooleanField(
        required=False, initial=False,
        label="Tampilkan dalam mode blok warna")

    class Meta:
        model = TableManagementSettings
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        self.context = {}
        self._formsets = kwargs.pop('formsets', {})

        super(TableManagementSettingsForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.color_display == TableManagementSettings.COLOR_DISPLAY.fill:
            self.fields['fill_mode'].initial = True
        self.fields['color_display'].required = False

        self.fields['outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['seat_management'].label = _('Aktifkan Seat Management')
        self.fields['seat_management'].help_text = _(
            '( Jika centang ini dihapus, setiap pesanan dijadikan satu meja. )')
        self.fields['display_color_status'].label = _("Tampilkan waktu di dalam kolom transaksi")
        self.fields['outlet'].label = ''
        self.fields['all_outlet'].required = False
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 p-0 mb-2"
        self.helper.label_class = "col-lg-12 p-0 mb-2"
        self.helper.form_tag = False
        self.helper.help_text_inline = True

        dummy_card = Div(
            Div(
                HTML("<h3 class='card-title mb-4'><span></span>#01</h3>"),
                HTML("<p class='card-subtitle'>4 Pax</p>"),
                HTML("<p class='card-text'></p>"),
                css_class="card-body px-3 py-3 rounded-bottom"
            ),
            css_class="card dummy-card mr-3 mt-3",
            css_id="display-time-table"
        )
        default_dummy_card = Div(
            Div(
                HTML("<h3 class='card-title mb-4'><span></span>#01</h3>"),
                HTML("<p class='card-subtitle'>4 Pax</p>"),
                HTML("<p class='card-text'>AVAILABLE</p>"),
                css_class="card-body px-3 py-3 rounded-bottom"
            ),
            css_class="card dummy-card mr-3 mt-3",
            css_id="default-time-table"
        )

        self.helper.layout = Layout(
            KawnField("all_outlet", type='hidden'),
            KawnField('outlet', type='hidden'),
            KawnFieldSetWithHelpText(
                "",
                KawnField("prefix"),
                KawnField('seat_management'),
                help_text=render_to_string("help_text/table_management_settings.html"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('display_color_status'),
                KawnField(default_dummy_card),
                KawnField(dummy_card),
                help_text=render_to_string("help_text/table_management_display_time.html")
            ),
            KawnFieldSetWithHelpText(
                "",
                BaseFormsetLayout(
                    self._formsets.get('fixed_formset'),
                    "col-lg-10 row p-0",
                    'dashboard/table_management/forms/fixed_color_status_formset.html'),
                KawnField('fill_mode'),
                Div(
                    HTML(
                        '<h4> \
                        Notifikasi Waktu \
                        <small class="d-lg-inline d-md-inline d-block text-muted">( Notifikasi waktu ini akan tampil ketika customer sudah melewati waktu yang ditentukan.)</small> \
                        </h4> \
                        <hr/>'
                    ),
                    css_class="col-lg-12 mt-3 p-0"
                ),
                BaseFormsetLayout(
                    self._formsets.get('formset'),
                    self.context,
                    'dashboard/table_management/forms/color_status_formset.html'),
                help_text=render_to_string("help_text/table_management_color_settings.html"),
            ),
        )

    def update_m2m(self, table_management, object_list, rel_field, intermediate_model):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "table_management_setting": table_management,
                    "account": table_management.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        fill_mode = self.cleaned_data.pop('fill_mode')
        kwargs['commit'] = False

        obj = super(TableManagementSettingsForm, self).save(*args, **kwargs)

        if fill_mode:
            obj.color_display = TableManagementSettings.COLOR_DISPLAY.fill
        else:
            obj.color_display = TableManagementSettings.COLOR_DISPLAY.normal

        obj.save()

        self.update_m2m(
            obj, outlets, obj.outlet, TableManagementOutlet)
        return obj


class ColorStatusForm(forms.ModelForm):
    COLORS = (
        ("#ffffff", "White"),
        ("#ff6259", "Red"),
        ("#f9a633", "Orange"),
        ("#f5ce33", "Yellow"),
        ("#6dcc4a", "Green"),
        ("#45b9f4", "Blue"),
        ("#d189e1", "Pink"),
        ("#a5a5a7", "Gray")
    )
    color_code = forms.ChoiceField(
        choices=COLORS,
        initial=COLORS[0],
        widget=ColorSelectWidget)

    class Meta:
        model = ColorStatus
        exclude = ('account', 'status_type')

    def __init__(self, *args, **kwargs):
        super(ColorStatusForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = "col-lg-3 pl-0"
        self.helper.field_class = "col-lg-12"
        self.helper.label_class = "col-lg-12"
        self.helper.layout = Layout(
            "table_management_setting",
            "id",
            "name",
            'status_time',
            "color_code",
            "is_active",
        )


class FixedColorStatusForm(forms.ModelForm):
    COLORS = (
        ("#ffffff", "White"),
        ("#ff6259", "Red"),
        ("#f9a633", "Orange"),
        ("#f5ce33", "Yellow"),
        ("#6dcc4a", "Green"),
        ("#45b9f4", "Blue"),
        ("#d189e1", "Pink"),
        ("#a5a5a7", "Gray")
    )
    color_code = forms.ChoiceField(
        choices=COLORS,
        initial=COLORS[0],
        widget=ColorSelectWidget)

    class Meta:
        model = ColorStatus
        exclude = ('account', 'status_type', 'status_time')

    def __init__(self, *args, **kwargs):
        super(FixedColorStatusForm, self).__init__(*args, **kwargs)
        self.fields['name'].disabled = True
        self.fields['is_active'].initial = True
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = "col-lg-5 pl-0"
        self.helper.field_class = "col-lg-12"
        self.helper.label_class = "col-lg-12"
        self.helper.layout = Layout(
            KawnField("table_management_setting"),
            KawnField("id"),
            KawnField("name"),
            KawnField("color_code"),
            KawnField("is_active", type='hidden'),

        )


class BaseFixedColorStatusFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queryset = self.instance.colorstatus_set.exclude(status_type='custom')


class BaseColorStatusFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queryset = self.instance.colorstatus_set.filter(status_type='custom')


FixedColorStatusFormSet = inlineformset_factory(
    TableManagementSettings, ColorStatus,
    form=FixedColorStatusForm,
    formset=BaseFixedColorStatusFormSet,
    max_num=2, can_delete=False)

ColorStatusFormSet = inlineformset_factory(
    TableManagementSettings, ColorStatus,
    form=ColorStatusForm,
    formset=BaseColorStatusFormSet,
    extra=4, max_num=4, can_delete=True)
