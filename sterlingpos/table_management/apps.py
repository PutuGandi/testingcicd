from django.apps import AppConfig
from django.db.models.signals import post_save


class TableManagementConfig(AppConfig):
    name = 'sterlingpos.table_management'
    verbose_name = "Table Management"
    permission_name = "application"

    def ready(self):
        from .models import TableActivityLog
        from .signals import update_table_activity_handler

        post_save.connect(
            update_table_activity_handler,
            sender="table_management.TableActivityLog")
