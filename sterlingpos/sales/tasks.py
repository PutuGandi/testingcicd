import datetime
import subprocess
import csv

from celery import shared_task

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.db import models
from django.db.models import (
    Avg, Sum, Count, Q, IntegerField, Value
)
from django.db.models.functions import Coalesce

from sterlingpos.accounts.models import Account
from sterlingpos.sales.utils import sent_product_order_report


@shared_task(bind=True, max_retries=5)
def automatic_outlet_transaction_export(self):
    try:
        with open("outlet_sales_export.csv", "r") as csvfile:
            csv_reader = csv.DictReader(
                csvfile,
                fieldnames=["outlet_pk"])

            for i, row in enumerate(csv_reader):
                if i == 0:
                    continue
                subprocess.check_call([
                    "./utility/outlet_transaction_backup.sh",
                    str(row["outlet_pk"]),
                    str(datetime.date.today() - datetime.timedelta(days=1)),
                    str(datetime.date.today())])
    except:
        self.retry(countdown= 2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def daily_transaction_report(self):
    try:
        start_date = datetime.date.today() - datetime.timedelta(days=1)
        end_date = datetime.date.today()

        basic_filter = Q(pk__isnull=False)
        if start_date:
            basic_filter &= Q(salesorder__transaction_date__gte=start_date)
        if end_date:
            basic_filter &= Q(salesorder__transaction_date__lte=end_date)

        canceled_bill_filter = ~Q(salesorder__status='unsettled')

        accounts = Account.objects.annotate(
            transaction_count=Coalesce(
                Count('salesorder',
                      filter=basic_filter & canceled_bill_filter,
                      output_field=IntegerField()), Value(0)),
            transaction_sum=Coalesce(
                Sum('salesorder__total_cost',
                      filter=basic_filter & canceled_bill_filter,
                      output_field=IntegerField()), Value(0)),
            transaction_avg=Coalesce(
                Avg('salesorder__total_cost',
                      filter=basic_filter & canceled_bill_filter,
                      output_field=IntegerField()), Value(0)),
        ).filter(
            transaction_count__gt=0
        ).order_by("-transaction_count", "name")

        subject = "KAWN Account Transactions Report {} s/d {}".format(
            start_date, end_date
        )
        to = settings.CUSTOM_REPORT_RECIPIENTS.get("account_transaction_report")
        text_content = '{}'.format(
            accounts.values(
                "name",
                "transaction_count", "transaction_sum",
                "transaction_avg")
        )
        template_name = "dashboard/sales_order/email/transaction_report.html"
        html_content = render_to_string(
            template_name, {
                'object_list': accounts,
                'start_date': start_date,
                'end_date': end_date
            })

        msg = EmailMultiAlternatives(
            subject=subject, 
            body=text_content, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    except:
        self.retry(countdown= 2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def delayed_sent_product_order_report(
        self, email, start_date, end_date):
    try:
        sent_product_order_report(email, start_date, end_date)
    except:
        self.retry(countdown= 2 ** self.request.retries)
