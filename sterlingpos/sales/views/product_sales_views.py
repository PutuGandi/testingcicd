from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.urls import reverse
from django.utils.text import slugify
from django.shortcuts import get_object_or_404
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models.functions import Concat
from django.db.models import Q, F, CharField, Case, Value, When

from django.views.generic import TemplateView, ListView
from django.utils.translation import ugettext as _

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.sales.models import (
    ProductSalesProxy as Product,
    CategorySalesProxy as Category,
)

from .mixins import ReportFilterMixins


class ProductSalesListJson(SterlingRoleMixin, CSVExportMixin, DatatableMixins, BaseDatatableView):
    columns = [
        'sku', 'catalogue.name', 'display_name', 'category.name',
        'product_sales_count', 'product_total_sales',
        'product_sales_surcharge',
        'product_sales_addon_cost',
        'product_sales_discount',
        'product_sales_foc',
        'product_sales_net']
    order_columns = [
        'sku', 'catalogue__name', 'display_name', 'category__name',
        'product_sales_count', 'product_total_sales',
        'product_sales_surcharge',
        'product_sales_addon_cost',
        'product_sales_discount',
        'product_sales_foc',
        'product_sales_net']

    model = Product

    csv_header = [
        'SKU', 'Product', 'Variant', 'Category',
        'Product Sales Qty', 'Product Total Sales',
        'Product Surcharge',
        'Product Addons',
        'Product Sales Discount',
        'Product Sales foc Qty', 'Product Sales foc',
        'Product Sales non-foc Qty', 'Product Sales Net']

    csv_columns = [
        'sku', 'catalogue.name','name', 'category.name',
        'product_sales_count', 'product_total_sales',
        'product_sales_surcharge',
        'product_sales_addon_cost',
        'product_sales_discount',
        'product_sales_foc_count', 'product_sales_foc',
        'product_sales_non_foc_count', 'product_sales_net']

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs)
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def get_csv_filename(self):
        csv_filename = "Product_Sales"
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get('outlet_pk'):
            outlet = get_object_or_404(
                outlet_qs,
                pk=self.kwargs.get('outlet_pk'))
            csv_filename = "{}_{}".format(
                csv_filename, slugify(outlet.name))

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().filter(
                is_sellable=True,
            ).select_related(
                "catalogue"
            ).prefetch_related(
                "category"
            )

        outlet_pk = self.kwargs.get('outlet_pk')
        outlet_qs = get_outlet_of_user(self.request.user)

        if outlet_pk:
            outlet = get_object_or_404(
                outlet_qs,
                pk=outlet_pk)
        else:
            outlet = None

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hour=23, minute=59)

        if self.request.user.is_owner:
            qs = qs.product_sales(
                start_date, end_date,
                outlet, settings.EXCLUDE_ZERO_SALES
            ).order_by('-code')
        else:
            qs = qs.product_sales(
                start_date, end_date,
                outlet, settings.EXCLUDE_ZERO_SALES,
                outlet_qs=outlet_qs
            ).order_by('-code')

        return qs


class ProductSalesListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product_sales/product_sales_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'categories': Category.objects.all(),
        })
        return context


class ProductAddonSalesListJson(SterlingRoleMixin, CSVExportMixin, DatatableMixins, BaseDatatableView):
    columns = [
        'sku', 'name', 'category.name',
        'addon_usage_count', 'addon_usage_sales', ]
    order_columns = [
        'sku', 'name', 'category__name',
        'addon_usage_count', 'addon_usage_sales', ]

    model = Product

    csv_header = [
        'SKU', 'Name', 'Category',
        'Addon Usage Qty', 'Addon Usage Sales', ]
    csv_columns = [
        'sku', 'name', 'category.name',
        'addon_usage_count', 'addon_usage_sales', ]

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs)
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def get_csv_filename(self):
        csv_filename = "product_addon_sales"

        if self.kwargs.get('outlet_pk'):
            outlet_qs = get_outlet_of_user(self.request.user)

            outlet = get_object_or_404(
                outlet_qs,
                pk=self.kwargs.get('outlet_pk'))
            csv_filename = "{}_{}".format(
                csv_filename, slugify(outlet.name))

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = super(
            ProductAddonSalesListJson, self).get_initial_queryset().prefetch_related('category')

        outlet_pk = self.kwargs.get('outlet_pk')
        outlet_qs = get_outlet_of_user(self.request.user)
        if outlet_pk:
            outlet = get_object_or_404(
                outlet_qs,
                pk=outlet_pk)
        else:
            outlet = None

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(hour=23, minute=59)

        if self.request.user.is_owner:
            qs = qs.addon_sales(
                start_date, end_date,
                outlet, settings.EXCLUDE_ZERO_SALES
            ).order_by('-code')
        else:
            qs = qs.addon_sales(
                start_date, end_date,
                outlet, settings.EXCLUDE_ZERO_SALES,
                outlet_qs=outlet_qs
            ).order_by('-code')
        return qs


class ProductAddonSalesListView(ProductSalesListView):
    template_name = 'dashboard/product_sales/product_addon_sales_list.html'
