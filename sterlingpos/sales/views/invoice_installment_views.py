from datetime import datetime

from django.conf import settings
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.utils.timezone import localtime
from django.utils.text import slugify
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db import transaction
from django.db.models.functions import Substr, Length
from django.db.models import Q, CharField, Value
from django.views.generic import TemplateView, ListView, DetailView, CreateView, View
from django.utils.translation import ugettext as _

from braces.views import JSONResponseMixin, AjaxResponseMixin, MessageMixin

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins, ReportFilterMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.sales.forms import InstallmentPaymentDetailForm, PaymentDetailFormSet, InstallmentPaymentDetailFormSet
from sterlingpos.sales.models import (
    SalesOrder,
    SalesOrderOption,
    CashierActivity,
    PaymentDetail,
    InvoiceInstallment,
)


class InvoiceInstallmentListJson(
    SterlingRoleMixin,
    CSVExportMixin,
    ReportFilterMixins,
    DatatableMixins,
    BaseDatatableView,
):

    columns = [
        "invoice.code",
        "invoice.date",
        "invoice.customer.name",
        "tenor_count",
        "invoice.sales_order.total_cost.amount",
        "paid",
        "invoice.sales_order.status",
        "detail_url",
    ]
    order_columns = [
        "invoice__code",
        "invoice__date",
        "invoice__customer__name",
        "tenor_count",
        "invoice__sales_order__total_cost__amount",
        "paid",
        "invoice__sales_order__status",
        "detail_url",
    ]
    datetime_col = [
        "date",
    ]

    model = InvoiceInstallment

    csv_header = [
        "Code",
        "Date",
        "Customer",
        "Status",
        "Outlet",
        "Period",
    ]
    csv_columns = [
        "invoice.code",
        "invoice.date",
        "invoice.customer.name",
        "invoice.sales_order.status",
        "invoice.sales_order.outlet.name",
        "period",
    ]

    _default_start_date = "start_date"
    _default_end_date = "end_date"

    def filter_code(self, search_value):
        return Q(code__icontains=search_value)

    # def filter_outlet__name(self, search_value):
    #     return Q(outlet=search_value)

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs, exclude_col=["date"])
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def render_csv_column(self, row, column):
        if column == "date":
            transaction_date = getattr(row, column)
            local_time = localtime(transaction_date)
            return local_time.strftime("%Y-%m-%d %H:%M:%S")

        return super().render_csv_column(row, column)

    def get_csv_filename(self):
        csv_filename = "Invoice"

        if self.request.GET.get("columns[2][search][value]"):
            csv_filename = "{}_{}".format(
                csv_filename, slugify(self.request.GET.get("columns[2][search][value]"))
            )

        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get("start_date", None),
            self.request.GET.get("end_date", None),
        )

        if start_date:
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, start_date)

        if end_date:
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = (
            super()
            .get_initial_queryset()
            .select_related(
                "invoice",
            )
        )
        return qs.exclude(invoice__sales_order=None).order_by("-pk")

    def render_column(self, row, column):
        if column == "detail_url":
            return reverse("sales:invoice_installment_detail", kwargs={"pk": row.pk})
        return super().render_column(row, column)


class InvoiceInstallmentListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/sales_order/invoice_installment_list.html"


class InvoiceInstallmentDetailView(SterlingRoleMixin, DetailView):
    template_name = "dashboard/sales_order/invoice_installment_detail.html"
    model = InvoiceInstallment

    def get_queryset(self):
        qs = super().get_queryset()

        if not self.request.user.is_owner:
            qs = qs.filter(
                invoice__sales_order__outlet__in=get_outlet_of_user(self.request.user)
            )

        qs = qs.select_related(
            "invoice",
        )
        return qs


class InstallmentPaymentDetailListJson(
    SterlingRoleMixin,
    CSVExportMixin,
    ReportFilterMixins,
    DatatableMixins,
    BaseDatatableView,
):

    columns = [
        "sales_order.code",
        "sales_order.transaction_date",
        "created",
        "amount.amount",
        "description",
        "detail_url",
    ]
    order_columns = [
        "sales_order__code",
        "sales_order__transaction_date",
        "created",
        "amount__amount",
        "description",
        "detail_url",
    ]
    datetime_col = [
        "sales_order.transaction_date",
    ]

    model = PaymentDetail

    csv_header = [
        "Code",
        "Date",
        "Payment Date",
        "Amount",
        "Description",
    ]
    csv_columns = [
        "sales_order.code",
        "sales_order.transaction_date",
        "created",
        "amount.amount",
        "description",
    ]

    _default_start_date = "start_date"
    _default_end_date = "end_date"

    def filter_sales_order__code(self, search_value):
        return Q(invoice__sales_order__code__icontains=search_value)

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs, exclude_col=["invoice__sales_order__transaction_date"])
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def render_csv_column(self, row, column):
        if column == "invoice.sales_order.transaction_date":
            transaction_date = getattr(row, column)
            local_time = localtime(transaction_date)
            return local_time.strftime("%Y-%m-%d %H:%M:%S")

        return super().render_csv_column(row, column)

    def get_csv_filename(self):
        csv_filename = "Payment Detail"

        if self.request.GET.get("columns[2][search][value]"):
            csv_filename = "{}_{}".format(
                csv_filename, slugify(self.request.GET.get("columns[2][search][value]"))
            )

        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get("start_date", None),
            self.request.GET.get("end_date", None),
        )

        if start_date:
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, start_date)

        if end_date:
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = (
            super()
            .get_initial_queryset()
            .filter(sales_order__invoice__installment=self.kwargs.get("installment_pk"))
            .select_related(
                "sales_order",
            )
        )
        return qs.order_by("-pk")

    def render_column(self, row, column):
        if column == "detail_url":
            return reverse(
                "sales:sales_order_detail", kwargs={"pk": row.sales_order.pk}
            )
        return super().render_column(row, column)


class InstallmentPaymentDetailCreateView(SterlingRoleMixin, CreateView):
    model = PaymentDetail
    template_name = "dashboard/sales_order/installment_payment_create.html"
    form_class = InstallmentPaymentDetailForm

    def get_installment(self):
        installment_pk = self.kwargs.get("installment_pk")
        invoice = get_object_or_404(
            InvoiceInstallment.objects.exclude(
                invoice__sales_order__status__in=["unsettled", "settled"]
            ),
            pk=installment_pk,
        )
        return invoice

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        invoice = self.get_installment()
        context["installment"] = invoice
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        invoice = self.get_installment()
        kwargs.update(
            {
                "sales_order": invoice.invoice.sales_order,
            }
        )
        return kwargs

    def get_success_url(self):
        url = reverse(
            "sales:invoice_installment_detail", kwargs={"pk": self.kwargs.get("installment_pk")}
        )
        return url


class InstallmentPaymentDetailDeleteView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}
        status_code = 404
        try:
            status_code = 200
            response["status"] = "success"
            payment_id = request.POST.get("id", "")
            payment_detail = get_object_or_404(
                PaymentDetail.objects.select_related("sales_order").exclude(
                    sales_order__status__in=["unsettled", "settled"]
                ),
                pk=payment_id,
            )
            payment_detail.delete()
            messages.error(
                self.request, "{}".format(_("Payment Detail has been deleted."))
            )
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
