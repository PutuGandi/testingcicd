from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View, UpdateView

from django.contrib import messages
from django.utils.translation import ugettext as _

from braces.views import (
    JSONResponseMixin, AjaxResponseMixin, MessageMixin
)

from sterlingpos.accounts.models import Account
from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.sales.forms import SalesReportSettingForm
from sterlingpos.sales.models import SalesReportSetting


class SalesReportSettingsUpdateView(SterlingRoleMixin, UpdateView):
    model = SalesReportSetting
    template_name = "dashboard/report/settings.html"
    form_class = SalesReportSettingForm

    def get_success_url(self):
        success_url = reverse("sales:settings")
        return success_url

    def get_object(self, queryset=None):
        obj = self.request.user.account.salesreportsetting_set.first()
        return obj

    def form_valid(self, form):
        response = super(SalesReportSettingsUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(_('Success update sales report settings')))
        return HttpResponseRedirect(self.get_success_url())


class SalesReportSettingInitialView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data' : {}
        }
        status_code = 404

        try:
            data = {}
            account = Account.objects.get(pk=self.request.user.account.id)
            try:
                salesreportsetting = SalesReportSetting.objects.get(account=account)
                data['report_type'] = salesreportsetting.report_type
            except SalesReportSetting.DoesNotExist:
                data['report_type'] = SalesReportSetting.TYPE.simple
            response['data'] = data
            status_code = 200
            response['status'] = 'success'
        except ValueError:
            pass

        return self.render_json_response(response, status_code)
