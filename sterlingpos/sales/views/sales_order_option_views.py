from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.utils.html import escape
from django_datatables_view.base_datatable_view import BaseDatatableView

from django.views.generic import (
    TemplateView,
    ListView,
    CreateView,
    UpdateView,
    View,
)
from django.contrib import messages
from django.utils.translation import ugettext as _

from braces.views import (
    JSONResponseMixin, AjaxResponseMixin, MessageMixin
)

from sterlingpos.accounts.models import Account
from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.sales.forms import SalesOrderOptionForm
from sterlingpos.sales.models import SalesOrderOption


class SalesOrderOptionListJson(SterlingRoleMixin, CSVExportMixin, DatatableMixins, BaseDatatableView):
    columns = ["name", "action"]
    order_columns = ["name", "action"]
    model = SalesOrderOption

    csv_header = ["Name", ]
    csv_columns = ["name", ]

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.exclude(deleted__isnull=False)
        return qs

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs)
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def get_csv_filename(self):
        csv_filename = "Sales_Order_Option"
        return csv_filename
 
    def render_column(self, row, column):
        if column == "detail_url":
            return reverse("sales:sales_order_detail", kwargs={'pk': row.pk})
        if column == "action":
            return {
                "name": escape(row.name),
                "id": row.id,
                "edit": reverse("sales:sales_order_option_edit", kwargs={'pk': row.pk}),
                "delete": reverse("sales:sales_order_option_delete"),
            }
        return super().render_column(row, column)


class SalesOrderOptionListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/sales_order_option/sales_order_option_list.html'


class SalesOrderOptionCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = 'dashboard/sales_order_option/sales_order_option_create.html'
    model = SalesOrderOption
    success_url = reverse_lazy("sales:sales_order_option_list")
    form_class = SalesOrderOptionForm

    def get_success_url(self):
        self.messages.success(
            _("Sales Order Option %s was successfully created." % self.object)
        )
        return self.success_url


class SalesOrderOptionUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = 'dashboard/sales_order_option/sales_order_option_update.html'
    model = SalesOrderOption
    success_url = reverse_lazy("sales:sales_order_option_list")
    form_class = SalesOrderOptionForm

    def get_success_url(self):
        self.messages.success(
            _("Sales Order Option %s was successfully updated." % self.object)
        )
        return self.success_url


class SalesOrderOptionDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        object_id = request.POST.get('id', '')
        try:
            used_on_table_order = request.user.account.tableordersettings_set.filter(
                order_option=object_id).exists()
            if SalesOrderOption.objects.count() > 1 and not used_on_table_order:            
                status_code = 200
                response['status'] = 'success'
                obj = SalesOrderOption.objects.get(id=object_id,)
                obj.delete()
                messages.error(self.request, '{}'.format(
                    _(f'Sales Order Option {obj.name} has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
