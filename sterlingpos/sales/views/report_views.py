import pytz
from decimal import Decimal, ROUND_DOWN
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta, MO, SU

from django.utils.timezone import localtime
from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.text import slugify
from django.utils.html import escape
from django.shortcuts import get_object_or_404
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models.functions import (
    Coalesce, Substr, Length, Concat,
    TruncDate, TruncMonth, TruncDay, TruncHour,
    ExtractWeekDay,
)
from django.db.models import (
    Prefetch, Sum, Count, Q, F, Func, CharField, DecimalField, Case,
    Value, When, ExpressionWrapper, Avg,
    OuterRef, Subquery, TimeField, DateField
)
from django.views.generic import (
    TemplateView,
    ListView, View, DetailView,
    CreateView, UpdateView,
    FormView)
from django.contrib import messages
from django.utils.translation import ugettext as _

from braces.views import (
    JSONResponseMixin, AjaxResponseMixin, MessageMixin
)

from dal import autocomplete

from sterlingpos.accounts.models import Account
from sterlingpos.core.mixins import (
    CSVExportMixin, DatatableMixins
)
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.sales.forms import (
    EnabledPaymentMethodForm,
    SalesReportSettingForm,
    SalesOrderOptionForm,
)
from sterlingpos.sales.models import (
    SalesOrder, SalesOrderOption, ProductOrder,
    CashierActivity, CashierActivityReason,
    PaymentMethod, EnabledPaymentMethod,
    ProductSalesProxy as Product,
    CategorySalesProxy as Category,
    SalesReportSetting
)

from sterlingpos.sales.views.mixins import (
    ReportFilterMixins,
    ReportCSVExportMixin
)


class SalesSummaryTemplateView(SterlingRoleMixin, ReportFilterMixins, TemplateView):
    template_name = 'dashboard/report/sales_summary.html'

    def get_context_data(self, **kwargs):
        context = super(SalesSummaryTemplateView, self).get_context_data(**kwargs)
        outlet = self.get_outlet(kwargs.get('outlet_pk'), context)

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('startdate', None),
            self.request.GET.get('enddate', None)
        )

        outlet_qs = get_outlet_of_user(self.request.user)
        if not self.request.user.is_owner:
            sales_orders = SalesOrder.objects.filter(
                outlet__in=outlet_qs
            ).select_related('transaction_type')

            product_order = ProductOrder.objects.filter(
                sales_order__outlet__in=outlet_qs)
        else:
            sales_orders = SalesOrder.objects.select_related('transaction_type')
            product_order = ProductOrder.objects.all()

        categories = Category.objects.all()
        payment_type = EnabledPaymentMethod.objects.first().payment_methods

        sales_order_comp = sales_orders = sales_orders.exclude(
            status=SalesOrder.STATUS.unsettled)
        product_order_comp = product_order = product_order.exclude(
            sales_order__status=SalesOrder.STATUS.unsettled)

        if start_date:
            sales_orders = sales_orders.filter(transaction_date__gte=start_date)
            product_order = product_order.filter(
                sales_order__transaction_date__gte=start_date)
        if end_date:
            sales_orders = sales_orders.filter(transaction_date__lte=end_date)
            product_order = product_order.filter(
                sales_order__transaction_date__lte=end_date)

        if outlet:
            sales_orders = sales_orders.filter(outlet=outlet)
            sales_order_comp = sales_order_comp.filter(outlet=outlet)
            product_order = product_order.filter(sales_order__outlet=outlet)

        time_delta = end_date - (start_date - timedelta(days=1))
        start_date_comp = start_date - timedelta(days=time_delta.days or 1)

        sales_order_comp = sales_order_comp.filter(
            transaction_date__gte=start_date_comp,
            transaction_date__lte=start_date)
        product_order_comp = product_order_comp.filter(
            sales_order__transaction_date__gte=start_date_comp,
            sales_order__transaction_date__lte=start_date)

        if not self.request.user.is_owner:
            payment_type_net = payment_type.payment_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES, outlet_qs=outlet_qs)
            payment_type_net_comp = payment_type.payment_sales(
                start_date_comp, start_date, outlet, settings.EXCLUDE_ZERO_SALES, outlet_qs=outlet_qs)
        else:
            payment_type_net = payment_type.payment_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES)
            payment_type_net_comp = payment_type.payment_sales(
                start_date_comp, start_date, outlet, settings.EXCLUDE_ZERO_SALES)

        total_guest_number = sales_orders.aggregate(
            total_guest_number=Coalesce(Sum('guest_number'), Value(0))
        ).get('total_guest_number')
        total_guest_number_comp = sales_order_comp.aggregate(
            total_guest_number_comp=Coalesce(Sum('guest_number'), Value(0))
        ).get('total_guest_number_comp')

        total_payment_type_net = payment_type_net.aggregate(
            total_payment_type_net=Coalesce(Sum('payment_net'), Value(0))
        ).get('total_payment_type_net')
        total_payment_type_net_comp = payment_type_net_comp.aggregate(
            total_payment_type_net_comp=Coalesce(Sum('payment_net'), Value(0))
        ).get('total_payment_type_net_comp')

        sales_order_count = sales_orders.count()
        sales_order_comp_count = sales_order_comp.count()

        if not self.request.user.is_owner:
            category_sales = categories.category_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES, outlet_qs=outlet_qs)
            category_sales_comp = categories.category_sales(
                start_date_comp, start_date, outlet, settings.EXCLUDE_ZERO_SALES, outlet_qs=outlet_qs)
        else:
            category_sales = categories.category_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES)
            category_sales_comp = categories.category_sales(
                start_date_comp, start_date, outlet, settings.EXCLUDE_ZERO_SALES)

        average_revenue = sales_orders.aggregate(
            total_cost__avg=Coalesce(Func(Avg(
                'total_cost', output_field=DecimalField()
            ), function='ROUND'), Value(0))
        ).get('total_cost__avg')
        average_revenue_comp = sales_order_comp.aggregate(
            total_cost__avg=Coalesce(Func(Avg(
                'total_cost', output_field=DecimalField()
            ), function='ROUND'), Value(0))
        ).get('total_cost__avg')

        total_category_sales = category_sales.aggregate(
            total_category_total_sales=Coalesce(
                Sum('category_total_sales',
                    output_field=DecimalField()), Value(0))
        ).get('total_category_total_sales')
        total_category_sales_comp = category_sales_comp.aggregate(
            total_category_total_sales_comp=Coalesce(
                Sum('category_total_sales',
                    output_field=DecimalField()), Value(0))
        ).get('total_category_total_sales_comp')

        foc_sales = sales_orders.filter(transaction_type__transaction_type='foc')
        foc_sales_comp = sales_order_comp.filter(
            transaction_type__transaction_type='foc')

        total_foc_sales = foc_sales.aggregate(
            total_foc_sales=Coalesce(
                Sum('total_cost'), Value(0))).get('total_foc_sales')
        total_foc_sales_comp = foc_sales_comp.aggregate(
            total_foc_sales_comp=Coalesce(
                Sum('total_cost'), Value(0))).get('total_foc_sales_comp')

        total_sales_after_tax = sales_orders.exclude(
            transaction_type__transaction_type='foc'
        ).aggregate(
            total_sales_after_tax=Coalesce(
                Sum('total_cost'), Value(0))).get('total_sales_after_tax')
        total_sales_after_tax_comp = sales_order_comp.exclude(
            transaction_type__transaction_type='foc'
        ).aggregate(
            total_sales_after_tax_comp=Coalesce(
                Sum('total_cost'), Value(0))).get('total_sales_after_tax_comp')

        context.update({
            'total_sales_after_tax': total_sales_after_tax,
            'total_sales_after_tax_comp': total_sales_after_tax_comp,
            'total_sales_after_tax_diff': self.get_diff_percentage(
                total_sales_after_tax, total_sales_after_tax_comp),

            'payment_type_net': payment_type_net,
            'total_payment_type_net': total_payment_type_net,
            'total_payment_type_net_comp': total_payment_type_net_comp,
            'total_payment_type_net_diff': self.get_diff_percentage(
                total_payment_type_net, total_payment_type_net_comp),

            'category_sales': category_sales,
            'total_category_sales': total_category_sales,
            'total_category_sales_comp': total_category_sales_comp,
            'total_category_sales_diff': self.get_diff_percentage(
                total_category_sales, total_category_sales_comp),

            'foc_sales': foc_sales,
            'total_foc_sales': total_foc_sales,
            'total_foc_sales_comp': total_foc_sales_comp,
            'total_foc_sales_diff': self.get_diff_percentage(
                total_foc_sales, total_foc_sales_comp),

            'average_revenue': average_revenue,
            'average_revenue_comp': average_revenue_comp,
            'average_revenue_diff': self.get_diff_percentage(
                average_revenue, average_revenue_comp),

            'sales_orders': sales_orders,
            'sales_order_comp': sales_order_comp,
            'sales_order_count': sales_order_count,
            'sales_order_comp_count': sales_order_comp_count,
            'sales_order_diff': self.get_diff_percentage(
                sales_order_count, sales_order_comp_count),

            'total_guest_number': total_guest_number,
            'total_guest_number_comp': total_guest_number_comp,
            'total_guest_number_diff': self.get_diff_percentage(
                total_guest_number, total_guest_number_comp),

            'start_date': start_date,
            'end_date': end_date,

        })

        return context


class SalesReportTemplateView(
        SterlingRoleMixin, ReportFilterMixins, TemplateView):
    template_name = 'dashboard/report/sales_report.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        outlet = self.get_outlet(kwargs.get('outlet_pk'), context)
        outlet_qs = get_outlet_of_user(self.request.user)

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('startdate', None),
            self.request.GET.get('enddate', None)
        )

        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
        else:
            outlet_qs = None

        view_detail = self.kwargs.get("detail")

        sales_orders = SalesOrder.objects.get_summary_grouped_by(
            start_date, end_date, view_detail, outlet_qs)
        sales_orders = sales_orders.select_related(
            "transaction_type",
            "outlet",
        ).prefetch_related(
            "items",
        )

        if outlet:
            sales_orders = sales_orders.filter(outlet=outlet)

        sales_orders = sales_orders.values(
            "time_unit"
        ).annotate(
            sales_before_tax=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    sales_before_tax=Coalesce(Sum(
                        F('items__base_cost') * F('items__quantity') + F('items__addon_cost'),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('sales_before_tax'),
                output_field=DecimalField(),
            )),

            product_surcharge=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    product_surcharge=Coalesce(Sum(
                        F('items__surcharge_price'),
                        filter=~Q(items__surcharge_price__lte=0),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('product_surcharge'),
                output_field=DecimalField(),
            )),

            product_disc=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    product_disc=Coalesce(Sum(
                        F('items__discount_price'),
                        filter=~Q(items__discount_price__lte=0),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('product_disc'),
                output_field=DecimalField(),
            )),

            product_order_foc=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    product_order_foc=Coalesce(Sum(
                        F('items__base_cost') * F('items__quantity') - F('items__total_cost'),
                        filter=~Q(items__discount_price__gt=0) & Q(items__total_cost__lt=F('items__base_cost') * F('items__quantity')),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('product_order_foc'),
                output_field=DecimalField(),
            )),

            sales_count=Coalesce(Count(
                F('pk'),
                output_field=DecimalField()), Value(0)
            ),

            sales_order_disc=Coalesce(Sum(
                F('global_discount_price'),
                output_field=DecimalField()), Value(0)
            ),

            sales_order_foc=Coalesce(Sum(
                F('total_cost'),
                filter=Q(transaction_type__transaction_type='foc'),
                output_field=DecimalField()), Value(0),
            ),

            sales_non_tax=Coalesce(ExpressionWrapper(
                F('sales_before_tax') + F('product_surcharge') - (
                    F('sales_order_foc') + F('product_order_foc') + F('product_disc') + F('sales_order_disc')
                ),
                output_field=DecimalField()), Value(0)
            ),

            sales_order_tax=Coalesce(Func(Sum(
                F('total_cost') - (
                    F('total_cost') / ((Value(100) + F('tax')) / Value(100))
                )
            ), function='ROUND'), Value(0), output_field=DecimalField()
            ),

            sales_service_charge=Coalesce(Func(Sum(
                (F('sub_total') - F('global_discount_price')) * F('gratuity') / Value(100)
            ), function='ROUND'), Value(0), output_field=DecimalField()
            ),

            sales_order_round=Coalesce(Sum(
                F('round_digit'),
                output_field=DecimalField()), Value(0)
            ),

            sales_total_sales=Coalesce(ExpressionWrapper(
                F('sales_non_tax') + F('sales_service_charge') + F('sales_order_tax'),
                output_field=DecimalField()), Value(0)
            ),
        ).values(
            "time_unit",
            "sales_order_foc", "sales_order_disc",
            "sales_count",
            "sales_before_tax",
            "sales_non_tax",
            "product_surcharge", "product_disc", "product_order_foc",
            "sales_order_tax", "sales_service_charge", "sales_order_round",
            "sales_total_sales",
        ).order_by(
            "time_unit",
        )

        sales_order_aggregate = sales_orders.aggregate(
            total_round_digit=Coalesce(
                Sum('sales_order_round'), Value(0)),
            total_sales_before_tax=Coalesce(
                Sum(F('sales_before_tax'), output_field=DecimalField()), Value(0)
            ),

            total_sales_order_disc=Coalesce(
                Sum(F('sales_order_disc'), output_field=DecimalField()), Value(0)
            ),
            total_product_disc=Coalesce(
                Sum(F('product_disc'), output_field=DecimalField()), Value(0)
            ),
            total_product_surcharge=Coalesce(
                Sum(F('product_surcharge'), output_field=DecimalField()), Value(0)
            ),

            total_product_order_foc=Coalesce(
                Sum(F('product_order_foc'),
                output_field=DecimalField()), Value(0)
            ),
            total_sales_order_foc=Coalesce(
                Sum(F('total_cost'),
                filter=Q(
                    transaction_type__transaction_type='foc'),
                ), Value(0)
            ),

            total_tax=Coalesce(
                Sum(F('sales_order_tax')), Value(0)
            ),
            total_service_charge=Coalesce(
                Sum(F('sales_service_charge')), Value(0)
            ),
        )

        total_round = sales_order_aggregate.get('total_round_digit')
        total_sales_before_tax = sales_order_aggregate.get('total_sales_before_tax')
        total_sales_order_disc = sales_order_aggregate.get('total_sales_order_disc')
        total_product_disc = sales_order_aggregate.get('total_product_disc')
        total_product_order_surcharge = sales_order_aggregate.get('total_product_surcharge')

        total_product_order_foc = sales_order_aggregate.get('total_product_order_foc')
        total_sales_order_foc = sales_order_aggregate.get('total_sales_order_foc')

        total_tax = sales_order_aggregate.get('total_tax')
        total_service_charge = sales_order_aggregate.get('total_service_charge')

        context.update({

            'total_sales_before_tax': total_sales_before_tax,

            'total_product_disc': total_product_disc,
            'total_sales_order_disc': total_sales_order_disc,

            'total_product_order_foc': total_product_order_foc,
            'total_product_order_surcharge': total_product_order_surcharge,

            'total_sales_order_foc': total_sales_order_foc,
            'total_sales_non_tax': total_sales_before_tax + total_product_order_surcharge - (
                total_sales_order_foc + total_product_order_foc + total_product_disc + total_sales_order_disc),
            'total_round': total_round,
            'total_tax': total_tax,
            'total_service_charge': total_service_charge,

            'total_sales': total_sales_before_tax + total_product_order_surcharge + total_tax + total_service_charge - (
                total_sales_order_foc + total_product_order_foc + total_product_disc + total_sales_order_disc),

            'sales_orders': sales_orders,
            'start_date': start_date,
            'end_date': end_date,
        })

        return context


class SalesByCategoryTemplateView(
        SterlingRoleMixin, ReportFilterMixins,
        ReportCSVExportMixin, TemplateView):
    template_name = 'dashboard/report/sales_by_category.html'

    csv_filename = "sales_by_category"
    csv_header = ["name", "sales"]

    def get_csv_data(self, context):
        csv_dict = context.copy()
        csv_data_list = [
            [category_sales.name, category_sales.category_total_sales]
            for category_sales in context.get("category_sales")
        ]
        return csv_data_list  

    def get_context_data(self, **kwargs):
        context = super(SalesByCategoryTemplateView, self).get_context_data(**kwargs)
        outlet = self.get_outlet(kwargs.get('outlet_pk'), context)

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('startdate', None),
            self.request.GET.get('enddate', None)
        )

        categories = Category.objects.order_by('path')
        outlet_qs = get_outlet_of_user(self.request.user)
        if not self.request.user.is_owner:
            category_sales = categories.category_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES, outlet_qs=outlet_qs)
        else:
            category_sales = categories.category_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES)

        context.update({
            'category_sales': category_sales,
            'start_date': start_date,
            'end_date': end_date,
        })

        return context


class SalesByOrderTypeTemplateView(
        SterlingRoleMixin, ReportFilterMixins,
        ReportCSVExportMixin, TemplateView):
    template_name = 'dashboard/report/sales_by_order_type.html'

    csv_filename = "sales_by_order_type"
    csv_header = ["name", "sales"]

    def get_csv_data(self, context):
        csv_dict = context.copy()
        csv_data_list = [
            [sales.name, sales.order_type_sales_net]
            for sales in context.get("order_type_net")
        ]
        return csv_data_list  

    def get_context_data(self, **kwargs):
        context = super(SalesByOrderTypeTemplateView, self).get_context_data(**kwargs)
        outlet = self.get_outlet(kwargs.get('outlet_pk'), context)

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('startdate', None),
            self.request.GET.get('enddate', None)
        )

        order_type = SalesOrderOption.objects.all()
        outlet_qs = get_outlet_of_user(self.request.user)
        if not self.request.user.is_owner:
            order_type_net = order_type.order_type_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES, outlet_qs=outlet_qs)
        else:
            order_type_net = order_type.order_type_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES)

        context.update({
            'order_type_net': order_type_net,
            'start_date': start_date,
            'end_date': end_date,

        })

        return context


class SalesByPaymentTemplateView(
        SterlingRoleMixin, ReportFilterMixins,
        ReportCSVExportMixin, TemplateView):
    template_name = 'dashboard/report/sales_by_payment.html'

    csv_filename = "sales_by_payment_type"
    csv_header = ["name", "usage", "net"]

    def get_csv_data(self, context):
        csv_dict = context.copy()
        csv_data_list = [
            [
                sales.payment_name,
                sales.payment_usage,
                sales.payment_net,
            ]
            for sales in context.get("payment_type_net")
        ]
        return csv_data_list  

    def get_context_data(self, **kwargs):
        context = super(SalesByPaymentTemplateView, self).get_context_data(**kwargs)
        outlet = self.get_outlet(kwargs.get('outlet_pk'), context)

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('startdate', None),
            self.request.GET.get('enddate', None)
        )

        payment_type = EnabledPaymentMethod.objects.first().payment_methods

        outlet_qs = get_outlet_of_user(self.request.user)
        if not self.request.user.is_owner:
            payment_type_net = payment_type.payment_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES, outlet_qs=outlet_qs)
        else:
            payment_type_net = payment_type.payment_sales(
                start_date, end_date, outlet, settings.EXCLUDE_ZERO_SALES)

        total_payment_type_net = payment_type_net.aggregate(
            total_payment_type_net=Coalesce(Sum('payment_net'), Value(0))).get('total_payment_type_net')

        total_payment_type_usage = payment_type_net.aggregate(
            total_payment_type_usage=Coalesce(Count('payment_detail'), Value(0))).get('total_payment_type_usage')

        context.update({
            'payment_type_net': payment_type_net,
            'total_payment_type_net': total_payment_type_net,
            'total_payment_type_usage': total_payment_type_usage,
            'start_date': start_date,
            'end_date': end_date,
        })

        return context


class SalesByTransactionTypeListJson(
        SterlingRoleMixin, DatatableMixins,
        CSVExportMixin, ReportFilterMixins,
        BaseDatatableView):
    columns = [
        'code', 'outlet.name', 'transaction_date',
        'transaction_type.transaction_type',
        'transaction_type.name',
        'guest_name',
        'total_cost.amount',]
    order_columns = [
       'code', 'outlet__name', 'transaction_date',
        'transaction_type__transaction_type',
        'transaction_type__name',
        'guest_name',
        'total_cost',]
    exclude_col = ['transaction_date',]

    csv_header = [
        "Code", "Outlet", "Date",
        "Type", "Description",
        "Guest Name",
        "Total Cost",]
    csv_columns = [
        'code', 'outlet.name', 'transaction_date',
        'transaction_type.transaction_type',
        'transaction_type.name',
        'guest_name',
        'total_cost.amount',]

    model = SalesOrder

    def get_csv_filename(self):
        csv_filename = "sales_by_transaction_type"

        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get('outlet_pk'):
            outlet = get_object_or_404(
                outlet_qs,
                pk=self.kwargs.get('outlet_pk'))
            csv_filename = "{}_{}".format(
                csv_filename, slugify(outlet.name))

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None)
        )

        if start_date:
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if end_date:
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def filter_outlet__name(self, search_value):
        return Q(outlet=search_value)

    def get_initial_queryset(self):

        outlet = self.get_outlet(
            self.kwargs.get('outlet_pk'), {})

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None)
        )

        outlet_qs = get_outlet_of_user(self.request.user)
        if not self.request.user.is_owner:
            sales_order = SalesOrder.objects.select_related('transaction_type').filter(
                transaction_type__isnull=False,
                outlet__in=outlet_qs,
            ).order_by('transaction_type__transaction_type')
        else:
            sales_order = SalesOrder.objects.select_related('transaction_type').filter(
                transaction_type__isnull=False).order_by('transaction_type__transaction_type')

        if outlet:
            sales_order = sales_order.filter(outlet=outlet)
        if start_date:
            sales_order = sales_order.filter(
                transaction_date__gte=start_date)
        if end_date:
            sales_order = sales_order.filter(
                transaction_date__lte=end_date)

        return sales_order

    def render_column(self, row, column):

        if column == "code":
            return (
                row.code,
                reverse("sales:sales_order_detail", kwargs={'pk': row.pk})
            )

        return super().render_column(row, column)


class SalesByTransactionTypeTemplateView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/report/sales_by_type.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class GrossProfitTemplateView(
        SterlingRoleMixin, ReportFilterMixins, TemplateView):
    template_name = "dashboard/report/sales_by_gross_profit.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        outlet = self.get_outlet(
            kwargs.get('outlet_pk'), context)

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get('date', None),
            self.request.GET.get('startdate', None),
            self.request.GET.get('enddate', None)
        )

        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
        else:
            outlet_qs = None

        view_detail = self.kwargs.get("detail")

        sales_orders = SalesOrder.objects.get_summary_grouped_by(
            start_date, end_date, view_detail, outlet_qs)
        sales_orders = sales_orders.select_related(
            "transaction_type",
            "outlet",
        ).prefetch_related(
            "items",
        )

        if outlet:
            sales_orders = sales_orders.filter(outlet=outlet)

        sales_orders = sales_orders.values(
            "time_unit"
        ).annotate(
            sales_before_tax=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    sales_before_tax=Coalesce(Sum(
                        F('items__base_cost') * F('items__quantity') + F('items__addon_cost'),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('sales_before_tax'),
                output_field=DecimalField(),
            )),

            cost_of_goods_sold=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    cost_of_goods_sold=Coalesce(Sum(
                        F('items__product_cost') * F('items__quantity'),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('cost_of_goods_sold'),
                output_field=DecimalField(),
            )),

            product_surcharge=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    product_surcharge=Coalesce(Sum(
                        F('items__surcharge_price'),
                        filter=~Q(items__surcharge_price__lte=0),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('product_surcharge'),
                output_field=DecimalField(),
            )),

            product_disc=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    product_disc=Coalesce(Sum(
                        F('items__discount_price'),
                        filter=~Q(items__discount_price__lte=0),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('product_disc'),
                output_field=DecimalField(),
            )),

            product_order_foc=Sum(Subquery(
                SalesOrder.objects.filter(
                    pk=OuterRef('pk')
                ).annotate(
                    product_order_foc=Coalesce(Sum(
                        F('items__base_cost') * F('items__quantity') - F('items__total_cost'),
                        filter=~Q(items__discount_price__gt=0) & Q(items__total_cost__lt=F('items__base_cost') * F('items__quantity')),
                        output_field=DecimalField()), Value(0)
                    ),
                ).values('product_order_foc'),
                output_field=DecimalField(),
            )),

            sales_count=Coalesce(Count(
                F('pk'),
                output_field=DecimalField()), Value(0)
            ),

            sales_order_disc=Coalesce(Sum(
                F('global_discount_price'),
                output_field=DecimalField()), Value(0)
            ),

            sales_order_foc=Coalesce(Sum(
                F('total_cost'),
                filter=Q(transaction_type__transaction_type='foc'),
                output_field=DecimalField()), Value(0),
            ),

            sales_non_tax=Coalesce(ExpressionWrapper(
                F('sales_before_tax') + F('product_surcharge') - (
                    F('sales_order_foc') + F('product_order_foc') + F('product_disc') + F('sales_order_disc')
                ),
                output_field=DecimalField()), Value(0)
            ),

            gross_profit=Coalesce(ExpressionWrapper(
                F('sales_non_tax') - F('cost_of_goods_sold'),
                output_field=DecimalField()), Value(0)
            ),
        ).values(
            "time_unit",
            "sales_order_foc", "sales_order_disc",
            "sales_count",
            "sales_before_tax",
            "cost_of_goods_sold",
            "product_surcharge", "product_disc", "product_order_foc",
            "sales_non_tax", "gross_profit",
        ).order_by(
            "time_unit",
        )

        sales_order_aggregate = sales_orders.aggregate(
            total_sales_before_tax=Coalesce(
                Sum(F('sales_before_tax'), output_field=DecimalField()), Value(0)
            ),
            total_cost_of_goods_sold=Coalesce(
                Sum(F('cost_of_goods_sold'), output_field=DecimalField()), Value(0)
            ),
            total_sales_order_disc=Coalesce(
                Sum(F('sales_order_disc'), output_field=DecimalField()), Value(0)
            ),
            total_product_disc=Coalesce(
                Sum(F('product_disc'), output_field=DecimalField()), Value(0)
            ),
            total_product_surcharge=Coalesce(
                Sum(F('product_surcharge'), output_field=DecimalField()), Value(0)
            ),
            total_product_order_foc=Coalesce(
                Sum(F('product_order_foc'),
                output_field=DecimalField()), Value(0)
            ),
            total_sales_order_foc=Coalesce(
                Sum(F('total_cost'),
                filter=Q(
                    transaction_type__transaction_type='foc'),
                ), Value(0)
            ),
        )

        total_sales_before_tax = sales_order_aggregate.get('total_sales_before_tax')
        total_cost_of_goods_sold = sales_order_aggregate.get('total_cost_of_goods_sold')
        total_sales_order_disc = sales_order_aggregate.get('total_sales_order_disc')
        total_product_order_surcharge = sales_order_aggregate.get('total_product_surcharge')
        total_product_order_disc = sales_order_aggregate.get('total_product_disc')
        total_product_order_foc = sales_order_aggregate.get('total_product_order_foc')
        total_sales_order_foc = sales_order_aggregate.get('total_sales_order_foc')

        context.update({
            'total_sales_before_tax': total_sales_before_tax,

            'total_sales_order_disc': total_sales_order_disc,

            'total_product_order_disc': total_product_order_disc,

            'total_product_order_foc': total_product_order_foc,

            'total_product_order_surcharge': total_product_order_surcharge,

            'total_sales_order_foc': total_sales_order_foc,

            'total_sales_non_tax': total_sales_before_tax + total_product_order_surcharge - (
                total_sales_order_foc + total_product_order_foc + total_product_order_disc + total_sales_order_disc
            ),

            'total_cost_of_goods_sold': total_cost_of_goods_sold,

            'gross_profit': total_sales_before_tax + total_product_order_surcharge - (
                total_sales_order_foc + total_product_order_foc + total_product_order_disc + total_sales_order_disc
            ) - total_cost_of_goods_sold,

            'sales_order_groups': sales_orders,

            'start_date': start_date,
            'end_date': end_date,
        })
        return context
