from datetime import datetime

from django.utils.timezone import localtime
from django.utils.text import slugify
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q, Value

from django.views.generic import TemplateView, ListView
from django.utils.translation import ugettext as _

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins, ReportFilterMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.sales.models import ProductOrder
from sterlingpos.sales.resources import ProductOrderResource


class ProductOrderListJson(
        SterlingRoleMixin, CSVExportMixin,
        ReportFilterMixins, DatatableMixins, BaseDatatableView):

    columns = [
        'sales_order.code', 'sales_order.transaction_date', 'sales_order.outlet.name', 'product.catalogue.name',
        'product.name', 'quantity', 'base_cost.amount', 'sub_total.amount', 'addon_cost.amount',
        'discount_price.amount', 'total_cost.amount']
    order_columns = [
        'sales_order__code', 'sales_order__transaction_date', 'sales_order__outlet__pk',
        'product__catalogue__name', 'product__name', 'quantity', 'base_cost',
        'sub_total', 'addon_cost', 'discount_price', 'total_cost']
    datetime_col = ['sales_order__transaction_date', ]

    model = ProductOrder
    resource_class = ProductOrderResource

    csv_header = [
        "Code", "Date", "Outlet", "Product", "Variant", "Quantity", "Base Cost",
        "Sub Total", "Addon Cost", "Discount", "Total Cost", "Addons", "Modifiers"]
    csv_columns = [
        "sales_order.code", "sales_order.transaction_date", "sales_order.outlet.name", "product.catalogue.name",
        "product.name", "quantity", "base_cost.amount", "sub_total.amount", "addon_cost.amount",
        "discount_price.amount", "total_cost.amount", "addon_detail", "modifier_detail"]

    def filter_sales_order__outlet__pk(self, search_value):
        return Q(sales_order__outlet__pk=search_value)

    def filter_sales_order__code(self, search_value):
        return Q(sales_order__code__icontains=search_value)

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs, exclude_col=['sales_order__transaction_date'])
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def render_csv_column(self, row, column):
        if column == "addon_detail":
            instance = getattr(row, column)
            list_instances = [
                "{} x{}".format(obj.product.name, obj.quantity)
                for obj in instance.all()
            ]
            render_list = ', '.join(list_instances)
            return render_list

        if column == "modifier_detail":
            instance = getattr(row, column)
            list_instances = [
                obj.option.value
                for obj in instance.all()
            ]
            render_list = ', '.join(list_instances)
            return render_list

        if column == "sales_order.transaction_date":
            sales_order = getattr(row, 'sales_order')
            local_time = localtime(sales_order.transaction_date)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')

        return super(ProductOrderListJson, self).render_csv_column(row, column)

    def get_csv_filename(self):
        csv_filename = "product_order"

        if self.request.GET.get('columns[2][search][value]'):
            csv_filename = "{}_{}".format(
                csv_filename, slugify(self.request.GET.get('columns[2][search][value]')))

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = super(ProductOrderListJson, self).get_initial_queryset().select_related(
            'sales_order',
            'sales_order__outlet',
            'product',
            'product__catalogue',
        ).prefetch_related(
            'addon_detail',
            'modifier_detail'
        )

        if not self.request.user.is_owner:
            qs = qs.filter(
                sales_order__outlet__in=get_outlet_of_user(self.request.user))

        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )

        if start_date:
            qs = qs.filter(sales_order__transaction_date__gte=start_date)
        if end_date:
            qs = qs.filter(sales_order__transaction_date__lte=end_date)

        return qs.order_by('-sales_order__transaction_date')


class ProductOrderListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/product_order/product_order_list.html'
    model = ProductOrder

