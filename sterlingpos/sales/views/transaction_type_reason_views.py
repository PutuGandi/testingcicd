from django.urls import reverse, reverse_lazy
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q, Value, Count
from django.utils.html import escape

from django.views.generic import (
    TemplateView,
    ListView,
    CreateView,
    UpdateView,
    View,
)
from django.contrib import messages
from django.utils.translation import ugettext as _

from braces.views import (
    JSONResponseMixin, AjaxResponseMixin, MessageMixin
)

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins, ReportFilterMixins
from sterlingpos.role.mixins import SterlingRoleMixin

from sterlingpos.sales.models import TransactionTypeReason
from sterlingpos.sales.forms import TransactionTypeReasonForm



class TransactionTypeReasonListJson(
        SterlingRoleMixin, DatatableMixins,
        BaseDatatableView):

    columns = [
        'description', 'usages_count',
        'archived', 'action']
    order_columns = [
        'description', 'usages_count',
        'archived', 'action']

    model = TransactionTypeReason

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().annotate(
            usages_count=Count('usages'),
        )
        return qs

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)
        return Q()

    def render_column(self, row, column):
        if column == "action":
            return {
                "description": escape(row.description),
                "id": row.id,
                "archived": row.archived,
                "edit": reverse(
                    "sales:transaction_type_reason_update",
                    kwargs={'pk': row.pk}),
                "delete": reverse("sales:transaction_type_reason_delete"),
                "deleteable": True if row.usages_count <= 0 else False,
            }
        elif column == 'archived':
            if row.archived:
                return '{}'.format(_('Archived'))
            else:
                return '{}'.format(_('Active'))

        return super().render_column(row, column)


class TransactionTypeReasonListView(
        SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/transaction_type/transaction_type_reason_list.html'
    model = TransactionTypeReason


class TransactionTypeReasonCreateView(
        SterlingRoleMixin, MessageMixin,
        CreateView):

    template_name = 'dashboard/transaction_type/transaction_type_reason_form.html'
    model = TransactionTypeReason
    form_class = TransactionTypeReasonForm
    success_url = reverse_lazy("sales:transaction_type_reason_list")


class TransactionTypeReasonUpdateView(
        SterlingRoleMixin, MessageMixin,
        UpdateView):

    template_name = 'dashboard/transaction_type/transaction_type_reason_form.html'
    model = TransactionTypeReason
    form_class = TransactionTypeReasonForm
    success_url = reverse_lazy("sales:transaction_type_reason_list")


class TransactionTypeReasonArchiveView(
        SterlingRoleMixin, JSONResponseMixin,
        AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        try:
            status_code = 200
            response['status'] = 'success'
            object_id = request.POST.get('id', '')
            obj = TransactionTypeReason.objects.get(
                pk=object_id)
            obj.archived = True
            obj.save()
            messages.success(self.request, '{}'.format(
                _('TransactionType Reason has been archived.')))

        except (ValueError, TransactionTypeReason.DoesNotExist):
            pass

        return self.render_json_response(response, status=status_code)


class TransactionTypeReasonActivateView(
        SterlingRoleMixin, JSONResponseMixin,
        AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        try:
            status_code = 200
            response['status'] = 'success'
            object_id = request.POST.get('id', '')
            obj = TransactionTypeReason.objects.get(
                pk=object_id)
            obj.archived = False
            obj.save()
            messages.success(self.request, '{}'.format(
                _('TransactionType Reason has been re-activated.')))

        except (ValueError, TransactionTypeReason.DoesNotExist):
            pass

        return self.render_json_response(response, status=status_code)


class TransactionTypeReasonDeleteView(
        SterlingRoleMixin, JSONResponseMixin,
        AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            object_id = request.POST.get('id', '')
            obj = TransactionTypeReason.objects.annotate(
                usages_count=Count('usages')
            ).filter(
                usages_count=0
            ).get(
                pk=object_id
            )
            obj.delete()
            messages.error(self.request, '{}'.format(
                _('TransactionType Reason has been deleted.')))
        except (ValueError, TransactionTypeReason.DoesNotExist):
            pass

        return self.render_json_response(response, status=status_code)
