from datetime import datetime

from django.urls import reverse, reverse_lazy
from django.utils.text import slugify
from django.utils.html import escape
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q, Value
from django.views.generic import (
    TemplateView,
    ListView, View, DetailView,
    CreateView, UpdateView,
)

from django.contrib import messages
from django.utils.translation import ugettext as _

from braces.views import (
    JSONResponseMixin, AjaxResponseMixin, MessageMixin
)

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins, ReportFilterMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.sales.forms import CashierActivityReasonForm

from sterlingpos.sales.models import (
    CashierActivity, CashierActivityReason,
)


class CashierActivityListJson(
        SterlingRoleMixin, CSVExportMixin, 
        ReportFilterMixins, DatatableMixins, BaseDatatableView):
    role_permission_name = "sales_settings"

    columns = ["code", "activity_type", "user", "outlet.name", "date", "description", "reason",]
    order_columns = ["code", "activity_type", "user", "outlet__name", "date", "description", "reason",]
    model = CashierActivity

    datetime_col = ["date", ]

    csv_header = ["Code", "Activity Type", "User", "Outlet", "Date", "Description", "Reason",]
    csv_columns = ["code", "activity_type", "user", "outlet.name", "date", "description", "reason",]

    _default_start_date = "start_date"
    _default_end_date = "end_date"

    def filter_outlet__name(self, search_value):
        return Q(outlet=search_value)

    def filter_code(self, search_value):
        return Q(code__icontains=search_value)
    
    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def get_csv_filename(self):
        csv_filename = "Cashier_Activity"

        if self.request.GET.get("columns[2][search][value]"):
            csv_filename = "{}_{}".format(
                csv_filename, slugify(self.request.GET.get("columns[2][search][value]")))

        if self.request.GET.get("start_date"):
            start_date = datetime.strptime(
                self.request.GET.get("start_date"),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get("end_date"):
            end_date = datetime.strptime(
                self.request.GET.get("end_date"),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().select_related(
            "outlet",
        )

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )

        if start_date:
            qs = qs.filter(date__gte=start_date)
        if end_date:
            qs = qs.filter(date__lte=end_date)

        return qs.order_by("-date")


class CashierActivityListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/cashier_activity/cashier_activity_list.html"
    role_permission_name = "sales_settings"


class CashierActivityDetailView(SterlingRoleMixin, ListView):
    template_name = "dashboard/cashier_activity/cashier_activity_list.html"
    model = CashierActivity
    role_permission_name = "sales_settings"

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(code=self.kwargs.get("code"))
        return queryset



class CashierActivityReasonListJson(SterlingRoleMixin, CSVExportMixin, DatatableMixins, BaseDatatableView):
    columns = ["name", "description", "created", "action"]
    order_columns = ["name", "description",  "created", "action"]
    model = CashierActivityReason
    role_permission_name = "sales_settings"

    datetime_col = ["created", ]

    csv_header = ["name", "description", "created",]
    csv_columns = ["name", "description", "created",]

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def get_csv_filename(self):
        csv_filename = "Cashier_Activity"

        if self.request.GET.get("columns[2][search][value]"):
            csv_filename = "{}_{}".format(
                csv_filename, slugify(self.request.GET.get("columns[2][search][value]")))

        if self.request.GET.get("start_date"):
            start_date = datetime.strptime(
                self.request.GET.get("start_date"),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get("end_date"):
            end_date = datetime.strptime(
                self.request.GET.get("end_date"),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        
        try:
            start_date = datetime.strptime(
                self.request.GET.get("start_date", None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = None

        try:
            end_date = datetime.strptime(
                self.request.GET.get("end_date", None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = None

        if start_date:
            qs = qs.filter(created__gte=start_date)
        if end_date:
            qs = qs.filter(created__lte=end_date)

        return qs

    def render_column(self, row, column):
        if column == "action":
            return {
                "name": escape(row.name),
                "id": row.id,
                "edit": reverse(
                    "sales:cashier_activity_reason_update",
                    kwargs={'pk': row.pk}),
                "delete": reverse(
                    "sales:cashier_activity_reason_delete"),
            }
        return super().render_column(row, column)


class CashierActivityReasonListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/cashier_activity/cashier_activity_reason_list.html"
    role_permission_name = "sales_settings"


class CashierActivityReasonCreateView(SterlingRoleMixin, CreateView):
    model = CashierActivityReason
    form_class = CashierActivityReasonForm
    success_url = reverse_lazy("sales:cashier_activity_reason_list")
    template_name = "dashboard/cashier_activity/cashier_activity_reason_form.html"
    role_permission_name = "sales_settings"


class CashierActivityReasonUpdateView(SterlingRoleMixin, UpdateView):
    model = CashierActivityReason
    form_class = CashierActivityReasonForm
    success_url = reverse_lazy("sales:cashier_activity_reason_list")
    template_name = "dashboard/cashier_activity/cashier_activity_reason_form.html"
    role_permission_name = "sales_settings"


class CashierActivityReasonDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    role_permission_name = "sales_settings"

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            object_id = request.POST.get('id', '')
            obj = CashierActivityReason.objects.get(id=object_id,)
            obj.delete()
            messages.error(self.request, '{}'.format(
                _(f'Cashier Activity Reason {obj.name} has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
