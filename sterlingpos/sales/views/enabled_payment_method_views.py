from django.db.models import Q
from django.urls import reverse
from django.views.generic import UpdateView
from django.contrib import messages
from django.utils.translation import ugettext as _

from dal import autocomplete

from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.sales.forms import EnabledPaymentMethodForm
from sterlingpos.sales.models import PaymentMethod, EnabledPaymentMethod


class AllPaymentMethodAutoCompleteView(autocomplete.Select2QuerySetView):
    role_permission_name = "sales_settings"

    def get_queryset(self):
        qs = PaymentMethod.objects.all()

        if self.q:
            qs = qs.filter(Q(name__istartswith=self.q) | Q(payment_sub__istartswith=self.q) | Q(
                payment_item__istartswith=self.q))
        return qs

class PaymentMethodAutoCompleteView(AllPaymentMethodAutoCompleteView):

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.user.is_authenticated:
            qs = qs.filter(
                pk__in=self.request.user.account.paymentmethodusage_set.values_list("payment_method")
            )

        return qs


class EnabledPaymentMethodUpdateView(SterlingRoleMixin, UpdateView):
    model = EnabledPaymentMethod
    template_name = "dashboard/payment_method/form.html"
    form_class = EnabledPaymentMethodForm
    role_permission_name = "sales_settings"

    def get_success_url(self):
        success_url = reverse("sales:payment_method_update")
        return success_url

    def get_object(self, queryset=None):
        obj = self.request.user.account.enabledpaymentmethod_set.first()
        return obj

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, '{}'.format(_('Success updating payment methods')))
        return response
