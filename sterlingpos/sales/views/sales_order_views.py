from datetime import datetime

from django.conf import settings
from django.urls import reverse
from django.utils.timezone import localtime
from django.utils.text import slugify
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models.functions import Substr, Length
from django.db.models import Q, CharField, Value
from django.views.generic import TemplateView, ListView, DetailView

from django.utils.translation import ugettext as _

from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins, ReportFilterMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.sales.models import (
    SalesOrder,
    SalesOrderOption,
    CashierActivity,
)

from sterlingpos.sales.resources import SalesOrderResource


class SalesOrderListJson(
        SterlingRoleMixin, CSVExportMixin,
        ReportFilterMixins, DatatableMixins, BaseDatatableView):

    columns = ['code', 'transaction_date', 'status', 'outlet.name', 'payment_detail', 'order_option.name',
               'sub_total.amount', 'global_discount_price.amount', 'total_cost.amount', 'detail_url']
    order_columns = ['code', 'transaction_date', 'status', 'outlet__name', 'payment_detail__payment_method__pk',
                     'order_option__name', 'sub_total', 'global_discount_price', 'total_cost', 'detail_url']
    datetime_col = ['transaction_date', ]
    model = SalesOrder

    resource_class = SalesOrderResource
    csv_header = [
        "Code", "Date", "Status", "Outlet", "Payment Detail", "Order Type",
        "Sub Total", "Global Product Discount", "Total Cost", "Guest Name"]
    csv_columns = ['code', 'transaction_date', 'status', 'outlet.name', 'payment_detail',
                   'order_option.name',
                   'sub_total.amount', 'global_discount_price.amount', 'total_cost.amount']

    _default_start_date = "start_date"
    _default_end_date = "end_date"

    def filter_outlet__name(self, search_value):
        return Q(outlet=search_value)

    def filter_code(self, search_value):
        return Q(code__icontains=search_value)
    
    def filter_payment_detail__payment_method__pk(self, search_value):
        return Q(payment_detail__payment_method__pk=search_value)

    def get_csv_columns(self):
        column = super().get_csv_columns()
        new_column = column.copy()
        new_column += ["guest_name"]
        return new_column

    def get_csv_data(self, context):
        qs = self.get_initial_queryset()
        qs = self.filter_queryset(qs, exclude_col=['transaction_date'])
        qs = self.ordering(qs)
        qs = self.paging(qs)
        export_data = self.render_result(qs)
        return export_data

    def render_csv_column(self, row, column):
        if column == "payment_detail":
            payment_detail = getattr(row, column)
            list_payment_detail = [pd.payment_method.payment_name for pd in payment_detail.all()]
            render_list = ', '.join(list_payment_detail)
            return render_list

        if column == "transaction_date":
            transaction_date = getattr(row, column)
            local_time = localtime(transaction_date)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')

        return super(SalesOrderListJson, self).render_csv_column(row, column)

    def get_csv_filename(self):
        csv_filename = "Sales_Order"

        if self.request.GET.get('columns[2][search][value]'):
            csv_filename = "{}_{}".format(
                csv_filename, slugify(self.request.GET.get('columns[2][search][value]')))
        
        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )

        if start_date:
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if end_date:
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        qs = super(SalesOrderListJson, self).get_initial_queryset().select_related(
            'outlet',
        ).prefetch_related(
            'order_option',
            'payment_detail',
            'payment_detail__payment_method',
        )

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )

        if start_date:
            qs = qs.filter(transaction_date__gte=start_date)
        if end_date:
            qs = qs.filter(transaction_date__lte=end_date)

        qs = qs.select_related('payment_method')
        return qs.order_by('-transaction_date')

    def render_column(self, row, column):
        if column == "payment_detail":
            payment_detail = getattr(row, column)
            return [pd.payment_method.payment_name for pd in payment_detail.all()]

        if column == "detail_url":
            return reverse("sales:sales_order_detail", kwargs={'pk': row.pk})

        return super(SalesOrderListJson, self).render_column(row, column)


class SalesOrderListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/sales_order/sales_order_list.html'

    def get_context_data(self, **kwargs):
        context = super(SalesOrderListView, self).get_context_data(**kwargs)
        enabled_payment_methods = self.request.user.account.enabledpaymentmethod_set.first().payment_methods.all()
        context['payment_types'] = enabled_payment_methods
        context['order_types'] = SalesOrderOption.objects.all()
        return context


class SalesOrderDetailView(SterlingRoleMixin, DetailView):
    template_name = 'dashboard/sales_order/sales_order_detail.html'
    model = SalesOrder

    def get_queryset(self):
        qs = super(SalesOrderDetailView, self).get_queryset()

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(self.request.user))

        qs = qs.annotate(
            small_code=Substr(
                'code', (Length('code') - 5), 6, output_field=CharField()
            ),
        )
        qs = qs.select_related(
            'outlet',
            'transaction_type',
        ).prefetch_related(
            'items',
            'items__product',
            'items__addon_detail',
            'payment_detail',
            'payment_detail__payment_method',
        )

        return qs

    def get_context_data(self, **kwargs):
        context = super(SalesOrderDetailView, self).get_context_data(**kwargs)
        if self.object.status == SalesOrder.STATUS.unsettled:
            try:
                cashier_activity = CashierActivity.objects.get(
                    activity_type=CashierActivity.TYPE.CART_VOID,
                    code=self.object.code,
                    outlet=self.object.outlet,
                )
                context.update({
                    "cashier_activity" : cashier_activity
                })
            except CashierActivity.MultipleObjectsReturned:
                cashier_activity = CashierActivity.objects.filter(
                    activity_type=CashierActivity.TYPE.CART_VOID,
                    code=self.object.code,
                    outlet=self.object.outlet,
                ).first()
                context.update({
                    "cashier_activity" : cashier_activity
                })
            except CashierActivity.DoesNotExist:
                pass
        return context
