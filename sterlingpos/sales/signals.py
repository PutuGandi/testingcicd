from django.conf import settings
from django.dispatch import Signal

invoice_settled = Signal(
    providing_args=["instance",])

invoice_paid = Signal(
    providing_args=["instance",])

def update_sales_order_handler(sender, **kwargs):
    instance = kwargs.get('instance')
    if settings.AUTO_UPDATE_COST:
        instance.sales_order.save()

