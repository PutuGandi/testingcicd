import csv
import json
import logging

from io import StringIO

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from sterlingpos.core.models import set_current_tenant
from sterlingpos.users.models import User
from sterlingpos.catalogue.models import Product, Category
from sterlingpos.sales.models import ProductOrder
from sterlingpos.sales.resources import ProductOrderResource

logger = logging.getLogger(__name__)



DEFAULT_SALES_ORDER_OPTION = [
    {
        "name": "Dine In",
    },
    {
        "name": "Take Away",
    },
    {
        "name": "Go-Food",
    },
    {
        "name": "Go-Resto",
    },
    {
        "name": "Grab Food",
    },
    {
        "name": "Delivery Order",
    },
    {
        "name": "Group Order",
    }
]

DEFAULT_PAYMENT_METHOD = [
    {
        "name": "Cash"
    },
    {
        "name": "Card",
        "payment_sub": "Credit",
        "payment_item": "Master Card",
    },
    {
        "name": "Card",
        "payment_sub": "Credit",
        "payment_item": "Visa",
    },
    {
        "name": "Card",
        "payment_sub": "Credit",
        "payment_item": "AMEX",
    },
    {
        "name": "Card",
        "payment_sub": "Credit",
        "payment_item": "JCB",
    },
    {
        "name": "Card",
        "payment_sub": "Credit",
        "payment_item": "BCA Card",
    },

    {
        "name": "Card",
        "payment_sub": "Debit",
        "payment_item": "Switching",
    },
    {
        "name": "Card",
        "payment_sub": "Debit",
        "payment_item": "Flash",
    },
    {
        "name": "Card",
        "payment_sub": "Debit",
        "payment_item": "MAESTRO"
    },
    {
        "name": "Card",
        "payment_sub": "Debit",
        "payment_item": "BCA"
    },
    {
        "name": "Card",
        "payment_sub": "Debit", "payment_item": "BRI"},
    {
        "name": "Card",
        "payment_sub": "Debit", "payment_item": "MANDIRI"},
    {"name": "Card", "payment_sub": "Debit", "payment_item": "BNI"},
    {"name": "Card", "payment_sub": "Debit", "payment_item": "NIAGA"},
    {"name": "Card", "payment_sub": "Debit", "payment_item": "PERMATA BANK"},
    {"name": "Card", "payment_sub": "Debit", "payment_item": "PANIN"},
    {
        "name": "Voucher"
    }
]

DEFAULT_TRANSACTION_TYPE_REASON_FOC = [
    "Entry error",
    "Friends and family",
    "Employee discount",
    "Manager special",
]

def sent_product_order_report(email, start_date, end_date):
    try:
        user = User.objects.get(email=email)
        set_current_tenant(user.account)
    except User.DoesNotExist:
        logger.info(f"User with email {email} does not exist.")
        return

    logger.info(f"Processing Product Order report for {start_date} - {end_date} to {email}.")
    
    product_order_qs = ProductOrder.objects.select_related(
        "sales_order", "product"
    ).filter(
        sales_order__transaction_date__gt=start_date,
        sales_order__transaction_date__lte=end_date,
    )
    product_order_resources = ProductOrderResource().export(
        queryset=product_order_qs)
    
    subject = "Product Order List {} - {}".format(
        start_date, end_date)
    filename = f"{subject}.csv"

    context = {
        "start_date": start_date,
        "end_date": end_date,
    }
    html_content = render_to_string(
        "dashboard/product_order/email/product_order_report.html",
        context
    )

    msg = EmailMultiAlternatives(
        subject=subject,
        body=subject,
        to=[email,]
    )
    msg.attach_alternative(
        html_content, "text/html")
    msg.attach(
        filename, product_order_resources.csv, 'text/csv')

    msg.send()
    set_current_tenant(None)
    logger.info(f"Finished sending Product Order report for {start_date} - {end_date} to {email}.")
