from collections import defaultdict
from decimal import Decimal, ROUND_DOWN

from django.conf import settings

from django.contrib.postgres.fields import ArrayField
from django.db import models, DataError
from django.db.models import Sum, Value
from django.db.models.fields.related import OneToOneField
from django.db.models.functions import Coalesce
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _, ugettext
from django.core.validators import RegexValidator
from django.contrib.contenttypes.fields import GenericRelation

from djmoney.money import Money
from djmoney.models.fields import MoneyField
from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.receipt.models import Receipt
from sterlingpos.surcharges.models import _SURCHARGE_TYPE
from sterlingpos.sales.signals import invoice_settled, invoice_paid

from .managers import (
    PaymentMethodSalesQuerySet,
    ProductSalesQuerySet,
    CategorySalesQuerySet,
    PaymentMethodTenantManager,
    ProductSalesTenantManager,
    CategorySalesTenantManager,
    SalesOrderOptionTenantManager,
    SalesOrderManager,
    ProductOrderManager,
)

from sterlingpos.catalogue.models import Product, Category
from sterlingpos.core.models import (
    SterlingTenantModel,
    SterlingTenantForeignKey,
    get_current_tenant,
    set_current_tenant,
)

_CUSTOMIZE_REPORT = Choices(
    ("revenue", _("Revenue")),
    ("revenue_per_section", _("Revenue Per Section")),
    ("payment", _("Payment")),
    ("receipt_type", _("Receipt Type")),
    ("category_parent", _("Category")),
    ("category_child", _("Sub-Category")),
    ("discount", _("Discount")),
    ("void_item", _("Void Item")),
    ("void_receipt_type", _("Void Receipt Type")),
    ("statistic", _("Statistic")),
    ("total_product_report", _("Total Product Report")),
    ("finance_order_report", _("Finance Order Report")),
)


class PaymentMethod(TimeStampedModel):
    """
    One PaymentMethod model for all tenant, tenant may disable payment_method
    by their preference via DisabledPaymentMethod model.
    """

    name = models.CharField(max_length=120)
    payment_sub = models.CharField(max_length=120, blank=True, null=True)
    payment_item = models.CharField(max_length=120, blank=True, null=True)

    objects = PaymentMethodTenantManager()

    class Meta:
        verbose_name = "PaymentMethod"
        verbose_name_plural = "PaymentMethods"

    def __str__(self):
        if self.payment_item and self.payment_sub:

            if self.name == "Digital":
                return "{} ".format(self.payment_item)

            return "{} {}".format(self.payment_sub, self.payment_item)
        else:
            return self.name

    @property
    def payment_name(self):
        return self.__str__()

    @property
    def is_disabled(self):
        return not self.payment_usages.exists()

    @property
    def enforce_description(self):
        if self.paymentmethodusage_set.exists():
            return self.paymentmethodusage_set.first().enforce_description
        return False


class PaymentMethodUsage(SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    payment_method = models.ForeignKey("sales.PaymentMethod", on_delete=models.CASCADE)
    enabled_payment_method = SterlingTenantForeignKey(
        "sales.EnabledPaymentMethod", related_name="usages", on_delete=models.CASCADE
    )
    enforce_description = models.BooleanField(default=False)

    class Meta:
        verbose_name = "PaymentMethodUsage"
        verbose_name_plural = "PaymentMethodUsages"
        unique_together = (("account", "id"),)


class EnabledPaymentMethod(SterlingTenantModel, TimeStampedModel):
    payment_methods = models.ManyToManyField("sales.PaymentMethod", blank=True)

    payments = models.ManyToManyField(
        "sales.PaymentMethod",
        blank=True,
        through="PaymentMethodUsage",
        related_name="payment_usages",
    )

    class Meta:
        verbose_name = "EnabledPaymentMethod"
        verbose_name_plural = "EnabledPaymentMethods"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Enabled Payment Methods for {} ({} item(s))".format(
            self.account, self.payments.count()
        )


class SalesOrderInvoice(SterlingTenantModel, TimeStampedModel):

    date = models.DateTimeField(auto_now_add=True)
    code = models.CharField(
        max_length=120, blank=True, default="", db_index=True)
    due_date = models.DateTimeField(blank=True, null=True)
    customer = models.ForeignKey(
        "customer.Customer", on_delete=models.CASCADE,
        blank=True, null=True)

    notes = models.TextField(
        max_length=225, blank=True, default="")

    class Meta:
        verbose_name = "SalesOrderInvoice"
        verbose_name_plural = "SalesOrderInvoice"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Invoice {} - {}".format(self.sales_order.code, self.date)

    @property
    def balance(self):
        total_cost = self.sales_order.total_cost.amount
        paid_sum = self.sales_order.payment_detail.aggregate(
            amount_used_sum=Coalesce(
                models.Sum("amount_used"), Value(0))
            ).get("amount_used_sum")
        balance = total_cost - paid_sum
        return balance

    @property
    def is_paid(self):
        return self.balance <= 0

    @property
    def is_payable(self):
        return not self.is_paid and not self.sales_order.status == SalesOrder.STATUS.unsettled

    def settle_invoice(self):
        if self.is_paid:
            self.sales_order.status = SalesOrder.STATUS.settled
            self.sales_order.save()
            invoice_settled.send(
                sender=SalesOrderInvoice,
                instance=self,
            )
        else:
            self.sales_order.status = SalesOrder.STATUS.paid
            self.sales_order.save()
            invoice_paid.send(
                sender=SalesOrderInvoice,
                instance=self,
            )
        return self.sales_order

    def update_sales_tendered_amount(self):
        self.sales_order.tendered_amount = self.sales_order.payment_detail.aggregate(
            paid_amount=Coalesce(
                Sum("amount_used"), Value(0))
        ).get("paid_amount")
        self.sales_order.save()


class InvoiceInstallment(SterlingTenantModel, TimeStampedModel):
    PERIOD = Choices(
        ("daily", _("harian")),
        ("weekly", _("mingguan")),
        ("monthly", _("bulanan")),
    )
    invoice = models.OneToOneField(
        "sales.SalesOrderInvoice", verbose_name=_("Invoice"), on_delete=models.CASCADE, related_name="installment")
    period = models.CharField(
        _("Payment Period"), max_length=10, choices=PERIOD, default=PERIOD.daily
    )
    tenor_count = models.PositiveSmallIntegerField(default=1)

    class Meta:
        verbose_name = "InvoiceInstallment"
        verbose_name_plural = "InvoiceInstallments"
        unique_together = (("invoice", "id"),)

    @property
    def paid(self):
        paid_sum = self.invoice.sales_order.payment_detail.aggregate(
            amount_used_sum=Coalesce(
                models.Sum("amount_used"), Value(0))
            ).get("amount_used_sum")
        return paid_sum

    @property
    def tenor_left(self):
        paid_count = self.invoice.sales_order.payment_detail.aggregate(
            amount_used_count=Coalesce(
                models.Count("amount_used"), Value(0))
            ).get("amount_used_count")

        # assumtion the first payment is a downpayment
        return (self.tenor_count - paid_count + 1)

    @property
    def balance_downpayment(self):
        down_payment = self.invoice.sales_order.payment_detail.first()
        if down_payment == None:
            return 0;
        else:
            return down_payment.amount_used.amount


class SalesOrderOption(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(max_length=120)

    objects = SalesOrderOptionTenantManager()

    class Meta:
        verbose_name = "SalesOrderOption"
        verbose_name_plural = "SalesOrderOptions"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Option {}".format(self.name)


class SalesOrder(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ("unsettled", _("ditolak")),
        ("unpaid", _("belum dibayar")),
        ("paid", _("menunggu verifikasi")),
        ("settled", _("selesai")),
    )
    MODE = Choices(
        ("none", _("None")),
        ("percentage", _("Percentage")),
        ("fixed_price", _("Fixed Price")),
        ("specified_amount", _("Specified Amount")),
    )

    invoice = models.OneToOneField(
        SalesOrderInvoice,
        related_name="sales_order",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    status = models.CharField(
        _("Transaction Status"),
        max_length=15,
        choices=STATUS,
        default=STATUS.unpaid,
        db_index=True,
    )
    mode = models.CharField(
        _("Discount Mode"), max_length=20, choices=MODE, default=MODE.none
    )

    code = models.CharField(max_length=120)

    global_discount_percentage = models.DecimalField(
        max_digits=5, decimal_places=2, default=0
    )
    global_discount_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    global_discount_specified_amount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    global_discount_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    round_digit = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    payment_method = models.ForeignKey(
        "sales.PaymentMethod", blank=True, null=True, on_delete=models.SET_NULL
    )
    transaction_type = SterlingTenantForeignKey(
        "sales.TransactionType",
        related_name="sales_order",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    sub_total = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    total_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    tendered_amount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    transaction_date = models.DateTimeField(db_index=True)
    tax = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    gratuity = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    change = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    served_by = models.CharField(max_length=60, blank=True, null=True)

    guest_number = models.PositiveSmallIntegerField(default=1)
    guest_name = models.CharField(max_length=60, blank=True, null=True)
    cashier_name = models.CharField(max_length=60, blank=True, null=True)
    order_name = models.CharField(max_length=60, blank=True, null=True)

    outlet = SterlingTenantForeignKey(
        "outlet.Outlet", null=True, on_delete=models.SET_NULL
    )
    order_option = SterlingTenantForeignKey(
        "sales.SalesOrderOption", null=True, on_delete=models.CASCADE
    )

    table = SterlingTenantForeignKey(
        "table_management.Table", blank=True, null=True, on_delete=models.SET_NULL
    )
    customer = SterlingTenantForeignKey(
        "customer.Customer", blank=True, null=True, on_delete=models.CASCADE
    )

    objects = SalesOrderManager()

    class Meta:
        verbose_name = "SalesOrder"
        verbose_name_plural = "SalesOrders"
        unique_together = (("account", "id"), ("account", "code"))
        indexes = [
            models.Index(
                fields=["account", "transaction_date"],
                name='acc_trsdate_idx'),
            models.Index(
                fields=["account", "transaction_date", "outlet"],
                name='acc_trsdate_otlt_idx'),
        ]

    def __str__(self):
        return "Sales Order {} - {}".format(self.code, self.transaction_date)

    @property
    def is_settled(self):
        return self.status == self.STATUS.settled

    def calculate_sub_total(self):
        sub_total = sum([item.calculate_total_cost() for item in self.items.all()])

        if not isinstance(sub_total, Money) and not isinstance(sub_total, Decimal):
            sub_total = Money(Decimal(sub_total), "IDR")

        return sub_total

    def calculate_service_charge(self):

        if self.sub_total.amount <= Decimal(0):
            sub_total = self.calculate_sub_total()
        else:
            sub_total = self.sub_total

        global_discount_price = self.calculate_global_discount_price()
        service_charge = (
            (sub_total - global_discount_price) * self.gratuity / Decimal(100)
        )
        service_charge = Money(
            service_charge.amount.quantize(
                Decimal('1')), 'IDR'
        )
        return service_charge

    def calculate_tax(self):
        if self.sub_total.amount <= Decimal(0):
            sub_total = self.calculate_sub_total()
        else:
            sub_total = self.sub_total

        global_discount_price = self.calculate_global_discount_price()
        service_charge = self.calculate_service_charge()

        total_cost = sub_total - global_discount_price + service_charge

        tax_price = total_cost.amount * (self.tax / Decimal(100))
        tax_price = Money(
            tax_price.quantize(
                Decimal('1')), 'IDR'
        )

        return tax_price

    def calculate_global_discount_price(self):
        global_discount_price = 0

        if self.sub_total.amount <= Decimal(0):
            sub_total = self.calculate_sub_total()
        else:
            sub_total = self.sub_total

        global_discount_perentage_price = sub_total * (
            self.global_discount_percentage / Decimal(100)
        )

        if self.global_discount_percentage and self.mode == self.MODE.percentage:
            global_discount_price = global_discount_perentage_price
        elif self.global_discount_fixed_price and self.mode == self.MODE.fixed_price:
            global_discount_price = self.global_discount_fixed_price
        elif (
            self.global_discount_specified_amount
            and self.mode == self.MODE.specified_amount
        ):
            global_discount_price = (
                self.sub_total - self.global_discount_specified_amount
            )

        return global_discount_price

    def calculate_sale_cost(self):
        sub_total = self.calculate_sub_total()
        global_discount_price = self.calculate_global_discount_price()
        service_charge = self.calculate_service_charge()
        tax_price = self.calculate_tax()

        total_cost = sub_total - global_discount_price + service_charge + tax_price

        if not isinstance(total_cost, Decimal):
            total_cost = total_cost.amount

        return total_cost

    def calculate_reverse_sub_total(self):
        if self.total_cost.amount > Decimal(0):
            total_cost = self.total_cost
        else:
            total_cost = self.calculate_sale_cost()

        if not isinstance(total_cost, Decimal):
            total_cost = total_cost.amount

        if total_cost > 0:
            before_tax = total_cost * (1 / ((Decimal(100) + self.tax) / Decimal(100)))
            sub_total = before_tax + self.calculate_global_discount_price()
        else:
            sub_total = 0
        return sub_total

    def create_invoice(self):
        self.invoice = SalesOrderInvoice.objects.create()
        self.save()
        return self.invoice

    def get_brand_logo(self):
        if hasattr(self.outlet, "receipt") and self.outlet.receipt.brand_logo:
            return self.outlet.receipt.brand_logo
        if self.account.brand_logo:
            return self.account.brand_logo

    def sent_email(
        self, recipients, template_name="dashboard/receipt/email/email_receipt.html"
    ):

        subject = "Transaction {} Receipt Detail.".format(self.code)
        text_content = "Transaction {} Receipt Detail.".format(self.code)

        brand_logo = self.get_brand_logo()
        receipt = Receipt.objects.filter(outlet__salesorder__code=self.code)
        html_content = rendered = render_to_string(
            template_name,
            {"object": self, "brand_logo": brand_logo, "receipt": receipt},
        )

        msg = EmailMultiAlternatives(subject, text_content, to=recipients)
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    def save(self, update_cost=False, *args, **kwargs):
        if update_cost or settings.AUTO_UPDATE_COST:
            self.sub_total = self.calculate_sub_total()
            self.global_discount_price = self.calculate_global_discount_price()
            self.total_cost = self.calculate_sale_cost()
        return super(SalesOrder, self).save(*args, **kwargs)


TRANSACTION_TYPE = Choices(
    ("city_ledger", "city_ledger", _("City Ledger")),
    ("foc", "foc", _("Free of Charge")),
)

class TransactionTypeReason(SterlingTenantModel, TimeStampedModel):
    TRANSACTION_TYPE = TRANSACTION_TYPE
    description = models.CharField(max_length=120)
    transaction_type = models.CharField(
        max_length=120,
        choices=TRANSACTION_TYPE,
        default=TRANSACTION_TYPE.foc
    )
    archived = models.BooleanField(default=False)

    class Meta:
        verbose_name = "TransactionTypeReason"
        verbose_name_plural = "TransactionTypeReasons"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "TransactionType Reason {}".format(self.description)


class TransactionType(SterlingTenantModel, TimeStampedModel):
    TRANSACTION_TYPE = TRANSACTION_TYPE
    transaction_type = models.CharField(
        max_length=120, choices=TRANSACTION_TYPE, default=TRANSACTION_TYPE.city_ledger
    )
    name = models.CharField(max_length=120)

    reason = SterlingTenantForeignKey(
        TransactionTypeReason,
        related_name="usages",
        null=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        verbose_name = "TransactionType"
        verbose_name_plural = "TransactionTypes"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "TransactionType {} | {}".format(self.transaction_type, self.name)


class PaymentDetail(SterlingTenantModel, TimeStampedModel):
    PAYMENT_AGGREGATOR = Choices(
        ("shopeepay", "shopeepay", _("ShopeePay")),
        ("cashlez", "cashlez", _("Cashlez")),
    )
    sales_order = SterlingTenantForeignKey(
        "sales.SalesOrder", related_name="payment_detail", on_delete=models.CASCADE
    )

    payment_method = models.ForeignKey("sales.PaymentMethod", on_delete=models.CASCADE)
    amount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    amount_used = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    description = models.CharField(_("Description"), max_length=255, blank=True)
    external_ref = models.CharField(_("External Ref"), max_length=255, blank=True)
    payment_aggregator = models.CharField(
        _("Payment Aggregator"), max_length=255,
        choices=PAYMENT_AGGREGATOR, blank=True)

    class Meta:
        verbose_name = "PaymentDetail"
        verbose_name_plural = "PaymentDetails"

    def __str__(self):
        return "Payment Detail {} - {} | {}".format(
            self.sales_order.code, self.payment_method, self.amount
        )

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if getattr(self.sales_order, "invoice", None):
            self.sales_order.invoice.settle_invoice()
            self.sales_order.invoice.update_sales_tendered_amount()
        return self


class ProductOrder(SterlingTenantModel, TimeStampedModel):
    MODE = Choices(
        ("none", _("None")),
        ("percentage", _("Percentage")),
        ("fixed_amount", _("Fixed Amount")),
    )

    BEHAVIOUR = Choices(
        ("none", _("None")),
        ("reduced_by", _("Reduced By")),
        ("overwrite", _("Overwrite")),
    )

    sales_order = SterlingTenantForeignKey(
        SalesOrder, related_name="items", on_delete=models.CASCADE
    )
    surcharges = SterlingTenantForeignKey(
        "surcharges.Surcharge", blank=True, null=True, on_delete=models.SET_NULL
    )
    product = SterlingTenantForeignKey("catalogue.Product", on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    discount_percentage = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    discount_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    surcharge_percentage = models.DecimalField(
        max_digits=5, decimal_places=2, default=0
    )
    surcharge_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    mode = models.CharField(
        _("Discount Mode"), max_length=15, choices=MODE, default=MODE.none
    )
    behaviour = models.CharField(
        _("Discount Behaviour"),
        max_length=15,
        choices=BEHAVIOUR,
        default=BEHAVIOUR.none,
    )

    surcharge_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    discount_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    base_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    sub_total = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    product_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    addon_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    total_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    last_edited = models.DateTimeField(auto_now=True)

    modifier_selected = models.CharField(max_length=120, blank=True, null=True)

    category = SterlingTenantForeignKey(
        "catalogue.Category", on_delete=models.CASCADE, blank=True, null=True
    )

    objects = ProductOrderManager()

    class Meta:
        verbose_name = "ProductOrder"
        verbose_name_plural = "ProductOrders"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Product Order {} | {} | {}".format(
            self.product.sku, self.quantity, self.total_cost
        )

    def calculate_discount_price(self):
        discount_price = 0
        base_total_cost = self.calculate_sub_total() + self.calculate_addon_cost()
        base_discount_perentage = base_total_cost * (
            self.discount_percentage / Decimal(100)
        )

        if self.discount_percentage and self.behaviour == self.BEHAVIOUR.overwrite:
            discount_price = base_total_cost - base_discount_perentage
        elif self.discount_percentage and self.behaviour == self.BEHAVIOUR.reduced_by:
            discount_price = base_discount_perentage
        elif self.discount_fixed_price and self.behaviour == self.BEHAVIOUR.overwrite:
            discount_price = base_total_cost - self.discount_fixed_price
        elif self.discount_fixed_price and self.behaviour == self.BEHAVIOUR.reduced_by:
            discount_price = self.discount_fixed_price
        return discount_price

    def calculate_total_surcharge(self):
        surcharge_price = 0
        surcharge_type = self.surcharges
        sub_total = self.calculate_sub_total()
        global_discount_price = self.calculate_discount_price()
        total_cost = sub_total - global_discount_price
        base_surcharge_percentage = total_cost * (
            self.surcharge_percentage / Decimal(100)
        )
        if not hasattr(surcharge_type, "surcharge_type"):
            surcharge_price
        elif self.surcharge_percentage:
            surcharge_price = base_surcharge_percentage
        elif self.surcharge_fixed_price:
            surcharge_price = self.surcharge_fixed_price
        elif surcharge_type.surcharge_type == _SURCHARGE_TYPE.percentage:
            surcharge_price = total_cost.amount * (
                self.surcharges.surcharge_percentage / Decimal(100)
            )
        elif surcharge_type.surcharge_type == _SURCHARGE_TYPE.fixed_price:
            surcharge_price = self.surcharges.surcharge_fixed_price

        surcharge_price = Money(Decimal(surcharge_price), "IDR")

        return surcharge_price

    def calculate_sub_total(self):
        sub_total = self.base_cost * self.quantity
        return sub_total

    def calculate_total_cost(self):
        discount_price = self.calculate_discount_price()
        sub_total = self.calculate_sub_total() + self.calculate_addon_cost()
        total_cost = sub_total - discount_price
        return total_cost

    def calculate_total_cost_with_surcharge(self):
        total_surcharge = self.calculate_total_surcharge()
        discount_price = self.calculate_discount_price()
        sub_total = self.calculate_sub_total() + self.calculate_addon_cost()
        total_cost_with_surcharge = sub_total - discount_price + total_surcharge
        return total_cost_with_surcharge

    def calculate_addon_cost(self):
        addon_cost = self.addon_detail.aggregate(
            sum_sub_total=Coalesce(Sum("sub_total"), Value(0))
        ).get("sum_sub_total")
        if not addon_cost:
            return Money(0, "IDR")
        return Money(addon_cost, "IDR")

    def save(self, update_cost=False, *args, **kwargs):
        self.category = self.product.category

        if not self.pk and self.product_cost.amount == 0:
            self.product_cost = self.product.cost

        if update_cost or settings.AUTO_UPDATE_COST:
            if not self.base_cost:
                self.base_cost = self.product.price
            self.surcharge_price = self.calculate_total_surcharge()
            self.sub_total = self.calculate_sub_total()
            self.addon_cost = self.calculate_addon_cost()
            self.discount_price = self.calculate_discount_price()
            self.total_cost = self.calculate_total_cost()
        return super(ProductOrder, self).save(*args, **kwargs)


class ProductOrderCompositeDetail(SterlingTenantModel, TimeStampedModel):
    product_order = SterlingTenantForeignKey(
        "sales.ProductOrder", related_name="composite_detail", on_delete=models.CASCADE
    )
    product = SterlingTenantForeignKey("catalogue.Product", on_delete=models.CASCADE)
    product_quantity = models.PositiveSmallIntegerField(default=1)

    class Meta:
        verbose_name = "ProductOrderCompositeDetail"
        verbose_name_plural = "ProductOrderCompositeDetails"
        unique_together = (("account", "id"),)

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string = "Composite Detail of {} | {} - {}".format(
            self.product_order.pk, self.product.sku, self.product_quantity
        )

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)

        return string


class ProductOrderModifierOptionDetail(SterlingTenantModel, TimeStampedModel):
    product_order = SterlingTenantForeignKey(
        "sales.ProductOrder", related_name="modifier_detail", on_delete=models.CASCADE
    )
    product = SterlingTenantForeignKey("catalogue.Product", on_delete=models.CASCADE)
    option = SterlingTenantForeignKey(
        "catalogue.ProductModifierOption", on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = "ProductOrderModifierOptionDetail"
        verbose_name_plural = "ProductOrderModifierOptionDetails"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Product Order Modifier {} | {} - {}".format(
            self.product_order.pk, self.product.sku, self.option.name
        )


class ProductOrderProductAddOnDetail(SterlingTenantModel, TimeStampedModel):
    product_order = SterlingTenantForeignKey(
        "sales.ProductOrder", related_name="addon_detail", on_delete=models.CASCADE
    )

    product = SterlingTenantForeignKey(
        "catalogue.Product", on_delete=models.CASCADE, null=True
    )
    quantity = models.PositiveIntegerField(default=0)

    base_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )
    sub_total = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    class Meta:
        verbose_name = "ProductOrderProductAddOnDetail"
        verbose_name_plural = "ProductOrderProductAddOnDetails"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Product Order AddOn Detail {} | {}".format(
            self.product_order.pk, self.product
        )


class SalesOrderPromoHistory(SterlingTenantModel, TimeStampedModel):

    sales_order = SterlingTenantForeignKey(
        "sales.SalesOrder", related_name="promo_list", on_delete=models.CASCADE
    )
    promo = SterlingTenantForeignKey("promo.Promo", on_delete=models.CASCADE)

    name = models.CharField(max_length=120)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)

    promo_type = models.CharField(max_length=60)
    discount_type = models.CharField(max_length=60)
    discount_percentage = models.DecimalField(max_digits=5, decimal_places=2, default=0)

    discount_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    base_value = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    affected_value = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
    )

    total_discount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        default_currency="IDR",
        default=0,
    )

    affected_product = SterlingTenantForeignKey(
        "catalogue.Product", on_delete=models.CASCADE, blank=True, null=True
    )
    product_quantity = models.PositiveIntegerField(default=0)
    affected_customer = SterlingTenantForeignKey(
        "customer.Customer", on_delete=models.CASCADE, blank=True, null=True
    )

    class Meta:
        verbose_name = "SalesOrderPromoHistory"
        verbose_name_plural = "SalesOrderPromoHistories"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "{} | {}".format(self.name, self.end_date.__str__())

    def get_total_discount(self):
        if self.affected_product:
            return self.affected_value * self.product_quantity
        return self.affected_value

    def save(self, *args, **kwargs):
        if isinstance(self.total_discount, Money) and self.total_discount.amount <= Decimal(0):
            self.total_discount = self.get_total_discount()

        return super().save(*args, **kwargs)


class CashierActivity(SterlingTenantModel, TimeStampedModel):
    TYPE = Choices(
        ("ITEM_DELETE", "ITEM_DELETE", _("Deleting Item")),
        ("ITEM_EDIT", "ITEM_EDIT", _("Editing Item")),
        ("PRINT_LOG", "PRINT_LOG", _("Printing")),
        ("CART_DELETE", "CART_DELETE", _("Deleting Cart")),
        ("CART_VOID", "CART_VOID", _("Voiding Cart")),
    )

    activity_type = models.CharField(_("Activity Type"), max_length=20, choices=TYPE)
    date = models.DateTimeField(db_index=True)
    description = models.TextField(_("Activity Description"))
    reason = models.TextField(_("Activity Reason"), blank=True, null=True)
    user = models.CharField(_("Staff Name"), max_length=120)
    code = models.CharField(_("Transaction Code"), max_length=120)

    outlet = models.ForeignKey("outlet.Outlet", on_delete=models.CASCADE)

    def __str__(self):
        return f"Cashier {self.activity_type}, {self.date} - {self.outlet}"

    def display_reason(self):
        if self.activity_type == CashierActivity.TYPE.CART_VOID:
            loc = self.description.find("Reason")
            if loc >= 0:
                return self.description[loc + 8 :].strip()
        return self.description


class CashierActivityReason(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    name = models.CharField(_("Reason Name"), max_length=80)
    description = models.TextField(_("Reason description"))

    class Meta:
        verbose_name = "CashierActivityReason"
        verbose_name_plural = "CashierActivityReasons"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Cashier Activity Reasonier {}".format(self.description)


class SalesReportSetting(SterlingTenantModel, TimeStampedModel):
    TYPE = Choices(
        ("simple", _("Simple")),
        ("comprehensive", _("Comprehensive")),
        ("custom", _("Customization")),
    )

    report_type = models.CharField(
        _("Report Type"), max_length=20, choices=TYPE, default=TYPE.simple
    )
    custom_report = ArrayField(
        models.CharField(max_length=255, choices=_CUSTOMIZE_REPORT),
        default=[
            "revenue",
            "revenue_per_section",
            "payment",
            "receipt_type",
            "category_parent",
            "category_child",
            "discount",
            "void_item",
            "void_receipt_type",
            "statistic",
            "total_product_report",
            "finance_order_report",
        ],
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = "SalesReportSetting"
        verbose_name_plural = "SalesReportSettings"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "Sales Report Settings {}".format(self.account)


class ProductSalesProxy(Product):
    objects = ProductSalesTenantManager()

    class Meta:
        proxy = True


class CategorySalesProxy(Category):
    objects = CategorySalesTenantManager()

    class Meta:
        proxy = True
