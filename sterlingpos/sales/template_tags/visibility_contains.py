from django import template
from django.utils.translation import gettext_lazy as _

register = template.Library()


@register.filter(name='receipt_visibility_contains')
def receipt_visibility_contains(value, args):
    return value.filter(visibility_receipt__contains=[args])


@register.filter(name='weekday_literal')
def weekday_literal(value):
    WEEKDAYS = {
        0: _('Sunday'), 1: _('Monday'), 2: _('Tuesday'), 3: _('Wednesday'), 4: _('Thursday'),
        5: _('Friday'), 6: _('Saturday')
    }
    return WEEKDAYS[value-1]

