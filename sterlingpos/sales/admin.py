from django.contrib import admin

from .models import (
    PaymentMethod,
    SalesOrderInvoice,
    SalesOrder,
    SalesOrderOption,
    EnabledPaymentMethod,
    ProductOrder,
    PaymentDetail,
    TransactionType,
    CashierActivity,
    CashierActivityReason,
    SalesReportSetting,

    ProductOrderProductAddOnDetail,
    ProductOrderCompositeDetail,
)


class ProductOrderCompositeDetailInline(admin.TabularInline):
    model = ProductOrderCompositeDetail
    raw_id_fields = ("product_order", "product", "account")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("product_order", "product", "account")
        return queryset


class ProductOrderProductAddOnDetailInline(admin.TabularInline):
    model = ProductOrderProductAddOnDetail
    raw_id_fields = ("product_order", "product", "account")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("product_order", "product")
        return queryset


class ProductOrderInline(admin.TabularInline):
    model = ProductOrder
    raw_id_fields = ("product", "account")
    exclude = ("category", )

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("sales_order", "product")
        return queryset


class PaymentDetailInline(admin.TabularInline):
    model = PaymentDetail
    raw_id_fields = ("sales_order", "account")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("sales_order", "payment_method")
        return queryset


class PaymentMethodAdmin(admin.ModelAdmin):
    """
        Admin View for PaymentMethod
    """
    list_display = ("name", "payment_sub", "payment_item")
    search_fields = ("name", "payment_sub", "payment_item")


class SalesOrderInvoiceAdmin(admin.ModelAdmin):
    """
        Admin View for Invoice
    """
    list_display = ("sales_order", "date", "account")


class SalesOrderAdmin(admin.ModelAdmin):
    """
        Admin View for SalesOrder
    """
    list_display = (
        "code", "invoice", "transaction_date", "status",
        "total_cost", "tendered_amount", "tax", "account",
        "global_discount_percentage", "global_discount_fixed_price",
        "global_discount_price",
        "mode",)

    raw_id_fields = ("account", "outlet")

    list_filter = ("status",)
    search_fields = ("code", "outlet__name", "account__name", "transaction_date")
    inlines = (
        ProductOrderInline,
        PaymentDetailInline,
    )

    def get_queryset(self, request):
        queryset = super(SalesOrderAdmin, self).get_queryset(request)
        queryset = queryset.select_related(
            "account",
            "outlet",
        ).prefetch_related(
            "items",
            "payment_detail",)

        return queryset


class ProductOrderAdmin(admin.ModelAdmin):
    """
        Admin View for ProductOrder
    """
    list_display = (
        "sales_order", "product", "quantity",
        "base_cost", "total_cost",
        "discount_percentage", "discount_fixed_price",
        "discount_price",
        "behaviour", "account")
    readonly_fields = ("sales_order",)
    search_fields = ("sales_order__code",)
    raw_id_fields = ("product", "account", "category")

    inlines = [
        ProductOrderProductAddOnDetailInline,
        ProductOrderCompositeDetailInline,
    ]

    def get_queryset(self, request):
        queryset = super(ProductOrderAdmin, self).get_queryset(request)
        queryset = queryset.select_related(
            "sales_order", "product", "account"
        ).prefetch_related(
            "addon_detail",
            "composite_detail",
        )
        return queryset



class PaymentDetailAdmin(admin.ModelAdmin):
    """
        Admin View for PaymentDetail
    """
    list_display = (
        "sales_order", "payment_method", "amount",
        "amount_used",)
    list_filter = ("account", "sales_order__transaction_date", "payment_method")
    search_fields = ("sales_order__code",)


class CashierActivityAdmin(admin.ModelAdmin):

    list_display = (
        "code", "activity_type", "user", "date", "outlet", "account")
    list_filter = (
        "date", "activity_type")
    search_fields = (
        "account__name", "code", "outlet__name", "user", "activity_type")


class CashierActivityReasonAdmin(admin.ModelAdmin):

    list_display = (
        "description", "account")
    list_filter = (
        "description", "account")
    search_fields = (
        "account__name", "description")


class EnabledPaymentMethodAdmin(admin.ModelAdmin):
    """
        Admin View for EnabledPaymentMethod
    """
    search_fields = ("account__name",)
    raw_id_fields = ("account",)


admin.site.register(ProductOrder, ProductOrderAdmin)
admin.site.register(PaymentMethod, PaymentMethodAdmin)
admin.site.register(SalesOrderInvoice, SalesOrderInvoiceAdmin)
admin.site.register(SalesOrder, SalesOrderAdmin)
admin.site.register(PaymentDetail, PaymentDetailAdmin)
admin.site.register(SalesOrderOption)
admin.site.register(EnabledPaymentMethod, EnabledPaymentMethodAdmin)
admin.site.register(TransactionType)
admin.site.register(CashierActivity, CashierActivityAdmin)
admin.site.register(CashierActivityReason, CashierActivityReasonAdmin)
admin.site.register(SalesReportSetting)
