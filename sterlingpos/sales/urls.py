# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "sales"
urlpatterns = [
    url(regex=r'^sales-summary/$', view=views.SalesSummaryTemplateView.as_view(),
        name='sales_summary'),
    url(regex=r'^sales-summary/outlet/(?P<outlet_pk>\d+)$', view=views.SalesSummaryTemplateView.as_view(),
        name='sales_summary'),

    url(regex=r'^gross-profit/$', view=views.GrossProfitTemplateView.as_view(),
        name='gross_profit'),
    url(regex=r'^gross-profit/outlet/(?P<outlet_pk>\d+)$', view=views.GrossProfitTemplateView.as_view(),
        name='gross_profit'),
    url(regex=r'^gross-profit/(?P<detail>month|date|days|hours)/$', view=views.GrossProfitTemplateView.as_view(),
       name='gross_profit'),
    url(regex=r'^gross-profit/outlet/(?P<outlet_pk>\d+)/(?P<detail>month|date|days|hours)/$', view=views.GrossProfitTemplateView.as_view(),
        name='gross_profit'), 

    url(regex=r'^sales-report/$', view=views.SalesReportTemplateView.as_view(),
       name='sales_report'),
    url(regex=r'^sales-report/outlet/(?P<outlet_pk>\d+)$', view=views.SalesReportTemplateView.as_view(),
        name='sales_report'), 
    url(regex=r'^sales-report/(?P<detail>month|date|days|hours)/$', view=views.SalesReportTemplateView.as_view(),
       name='sales_report'),
    url(regex=r'^sales-report/outlet/(?P<outlet_pk>\d+)/(?P<detail>month|date|days|hours)/$', view=views.SalesReportTemplateView.as_view(),
        name='sales_report'), 

    url(regex=r'^sales-by-category/$', view=views.SalesByCategoryTemplateView.as_view(),
        name='sales_by_category'),
    url(regex=r'^sales-by-category/outlet/(?P<outlet_pk>\d+)$', view=views.SalesByCategoryTemplateView.as_view(),
        name='sales_by_category'),

    url(regex=r'^sales-by-order-type/$', view=views.SalesByOrderTypeTemplateView.as_view(),
        name='sales_by_order_type'),
    url(regex=r'^sales-by-order-type/outlet/(?P<outlet_pk>\d+)/$', view=views.SalesByOrderTypeTemplateView.as_view(),
        name='sales_by_order_type'),
    url(regex=r'^sales-by-order-type/(?P<detail>month|date|days|hours)/$', view=views.SalesByOrderTypeTemplateView.as_view(),
        name='sales_by_order_type'),
    url(regex=r'^sales-by-order-type/outlet/(?P<outlet_pk>\d+)/(?P<detail>month|date|days|hours)/$', view=views.SalesByOrderTypeTemplateView.as_view(),
        name='sales_by_order_type'),

    url(regex=r'^sales-by-payment/$', view=views.SalesByPaymentTemplateView.as_view(),
        name='sales_by_payment'),
    url(regex=r'^sales-by-payment/outlet/(?P<outlet_pk>\d+)$', view=views.SalesByPaymentTemplateView.as_view(),
        name='sales_by_payment'),

    url(regex=r'^sales-by-type/$', view=views.SalesByTransactionTypeTemplateView.as_view(),
        name='sales_by_type'),
    url(regex=r'^sales-by-type/outlet/(?P<outlet_pk>\d+)$', view=views.SalesByTransactionTypeTemplateView.as_view(),
        name='sales_by_type'),
    url(regex=r'^sales-by-type/(?P<outlet_pk>[0-9]+)/data/$', view=views.SalesByTransactionTypeListJson.as_view(),
        name='sales_by_type_data'),
    url(regex=r'^sales-by-type/data/$', view=views.SalesByTransactionTypeListJson.as_view(),
        name='sales_by_type_data'),

    url(regex=r'^sales-order/$', view=views.SalesOrderListView.as_view(),
        name='sales_order_list'),
    url(regex=r'^sales-order/(?P<pk>\d+)/$', view=views.SalesOrderDetailView.as_view(),
        name='sales_order_detail'),
    url(regex=r'^sales-order/data/$', view=views.SalesOrderListJson.as_view(),
        name='sales_order_data'),

    url(regex=r'^invoice/$', view=views.InvoiceListView.as_view(),
        name='invoice_list'),
    url(regex=r'^invoice/data/$', view=views.InvoiceListJson.as_view(),
        name='invoice_data'),
    url(regex=r'^invoice/(?P<pk>\d+)/$', view=views.InvoiceDetailView.as_view(),
        name='invoice_detail'),
    url(regex=r'^invoice/(?P<invoice_pk>\d+)/payments/data/$', view=views.PaymentDetailListJson.as_view(),
        name='invoice_payment_list'),
    url(regex=r'^invoice/(?P<invoice_pk>\d+)/payments/create/$',
        view=views.PaymentDetailCreateView.as_view(),
        name='invoice_payment_create'),

    url(regex=r'^invoice_installment/$', view=views.InvoiceInstallmentListView.as_view(),
        name='invoice_installment_list'),
    url(regex=r'^invoice_installment/data/$', view=views.InvoiceInstallmentListJson.as_view(),
        name='invoice_installment_data'),
    url(regex=r'^invoice_installment/(?P<pk>\d+)/$', view=views.InvoiceInstallmentDetailView.as_view(),
        name='invoice_installment_detail'),
    url(regex=r'^invoice_installment/(?P<installment_pk>\d+)/payments/data/$', view=views.InstallmentPaymentDetailListJson.as_view(),
        name='invoice_installment_payment_list'),
    url(regex=r'^invoice_installment/(?P<installment_pk>\d+)/payments/create/$',
        view=views.InstallmentPaymentDetailCreateView.as_view(),
        name='invoice_installment_payment_create'),

    url(regex=r'^payment/(?P<pk>\d+)/$', view=views.PaymentDetailListJson.as_view(),
        name='payment_detail'),


    url(regex=r'^sales-order-option/$', view=views.SalesOrderOptionListView.as_view(),
        name='sales_order_option_list'),
    url(regex=r'^sales-order-option/create/$', view=views.SalesOrderOptionCreateView.as_view(),
        name='sales_order_option_create'),
    url(regex=r'^sales-order-option/(?P<pk>\d+)/$', view=views.SalesOrderOptionUpdateView.as_view(),
        name='sales_order_option_edit'),
    url(regex=r'^sales-order-option/delete/$', view=views.SalesOrderOptionDeleteView.as_view(),
        name='sales_order_option_delete'),
    url(regex=r'^sales-order-option/data/$', view=views.SalesOrderOptionListJson.as_view(),
        name='sales_order_option_data'),

    url(regex=r'^product-order/$', view=views.ProductOrderListView.as_view(),
        name='product_order_list'),
    url(regex=r'^product-order/data/$', view=views.ProductOrderListJson.as_view(),
        name='data_product_order'),

    url(regex=r'^product-sales/$', view=views.ProductSalesListView.as_view(),
        name='product_sales_list'),
    url(regex=r'^product-sales/data/$', view=views.ProductSalesListJson.as_view(),
        name='data_product_sales'),
    url(regex=r'^product-sales/branch/(?P<outlet_pk>[0-9]+)$', view=views.ProductSalesListJson.as_view(),
        name='data_product_sales'),

    url(regex=r'^addon-sales/$', view=views.ProductAddonSalesListView.as_view(),
        name='product_addon_sales_list'),
    url(regex=r'^addon-sales/data/$', view=views.ProductAddonSalesListJson.as_view(),
        name='data_product_addon_sales'),
    url(regex=r'^addon-sales/branch/(?P<outlet_pk>[0-9]+)$', view=views.ProductAddonSalesListJson.as_view(),
        name='data_product_addon_sales'),

    url(regex=r'^payment-method/$', view=views.EnabledPaymentMethodUpdateView.as_view(),
        name='payment_method_update'),
    url(regex=r'^payment-method/autocomplete/$', view=views.PaymentMethodAutoCompleteView.as_view(),
        name='payment_method_autocomplete'),
    url(regex=r'^payment-method/all/autocomplete/$', view=views.AllPaymentMethodAutoCompleteView.as_view(),
        name='all_payment_method_autocomplete'),
        

    url(regex=r'^settings/$', view=views.SalesReportSettingsUpdateView.as_view(),
        name='settings'),
    url(regex=r'^settings/data/$', view=views.SalesReportSettingInitialView.as_view(),
        name='settings_data'),

    url(regex=r'^cashier-activity/$', view=views.CashierActivityListView.as_view(),
        name='cashier_activity_list'),
    url(regex=r'^cashier-activity/data/$', view=views.CashierActivityListJson.as_view(),
        name='cashier_activity_data'),

    url(regex=r'^cashier-activity/reasons/$', view=views.CashierActivityReasonListView.as_view(),
        name='cashier_activity_reason_list'),
    url(regex=r'^cashier-activity/reasons/data/$', view=views.CashierActivityReasonListJson.as_view(),
        name='cashier_activity_reason_data'),
    url(regex=r'^cashier-activity/reasons/create/$', view=views.CashierActivityReasonCreateView.as_view(),
        name='cashier_activity_reason_create'),
    url(regex=r'^cashier-activity/reasons/delete/$', view=views.CashierActivityReasonDeleteView.as_view(),
        name='cashier_activity_reason_delete'),
    url(regex=r'^cashier-activity/reasons/(?P<pk>\d+)/$', view=views.CashierActivityReasonUpdateView.as_view(),
        name='cashier_activity_reason_update'),

    url(regex=r'^transaction-type/reasons/$',
        view=views.TransactionTypeReasonListView.as_view(),
        name='transaction_type_reason_list'),
    url(regex=r'^transaction-type/reasons/data/$',
        view=views.TransactionTypeReasonListJson.as_view(),
        name='transaction_type_reason_data'),
    url(regex=r'^transaction-type/reasons/create/$',
        view=views.TransactionTypeReasonCreateView.as_view(),
        name='transaction_type_reason_create'),
    url(regex=r'^transaction-type/reasons/delete/$',
        view=views.TransactionTypeReasonDeleteView.as_view(),
        name='transaction_type_reason_delete'),
    url(regex=r'^transaction-type/reasons/(?P<pk>\d+)/$',
        view=views.TransactionTypeReasonUpdateView.as_view(),
        name='transaction_type_reason_update'),

    url(r'^transaction-type/reasons/archive/$',
        view=views.TransactionTypeReasonArchiveView.as_view(),
        name='transaction_type_reason_archive'),
    url(r'^transaction-type/reasons/activate/$',
        view=views.TransactionTypeReasonActivateView.as_view(),
        name='transaction_type_reason_activate'),

]
