import csv
import logging
import random

from datetime import datetime

from openpyxl import load_workbook
from safedelete import *

from types import TracebackType
from django.db import transaction
from sterlingpos.core.models import set_current_tenant
from sterlingpos.outlet.models import Outlet
from sterlingpos.catalogue.models import Product
from sterlingpos.surcharges.models import Surcharge
from sterlingpos.sales.models import (
    SalesOrder,
    ProductOrder,
    PaymentDetail,

    SalesOrderOption,
    PaymentMethod,
)

logger = logging.getLogger(__name__)

def convert_sheet_row(row):
    headers = [
        'sales_code', 'order_name', 'transaction_date', 'guest_number',
        'outlet', 'order_option', 'payment_method', 'product', 'base_cost',
        'surcharge_price', 'quantity', 'product_order_sub_total',
        'product_order_discount_price', 'addon_cost',
        'product_order_total_cost', 'global_discount_price', 'sub_total',
        'tax', 'total_cost', 'tendered_amount', 'change'
    ]
    new_row = {
        headers[i]:x for (i, x) in enumerate(row) if i <= 20
    }
    return new_row

def get_outlet(outlet_name):
    obj = Outlet.objects.get(name=outlet_name)
    return obj

def get_product(sku):
    obj = Product.objects.get(sku=sku)
    return obj

def get_order_option(order_option):
    obj = SalesOrderOption.objects.get(name__istartswith=order_option)
    return obj

def get_surcharge(order_option):
    try:
        obj = Surcharge.objects.filter(order_option=order_option).first()
        return obj
    except:
        pass

def get_payment_method(payment_name):
    try:
        obj = PaymentMethod.objects.get(
            name__icontains=payment_name)
    except PaymentMethod.DoesNotExist:
        obj = PaymentMethod.objects.get(
            payment_item__icontains=payment_name)
    return obj

def get_random_string(length):
    # choose from all lowercase letter
    RAND_STRING = "0123456789BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz"
    result_str = ''.join(
        random.choice(RAND_STRING) for i in range(length))
    return result_str

def generate_sales_code(transaction_date, outlet):
    """
    OUTLET_PREFIX + yyyyMMddKK + [A/P] + mm + RAND_STRING(6)
    """   
    transaction_code = "{}{}{}{}{}".format(
        outlet.transaction_code_prefix,
        datetime.strftime(transaction_date, "%y%m%d%I%p"),
        "P" if datetime.strftime(transaction_date, "%p") == "PM" else "A",
        datetime.strftime(transaction_date, "%M"),
        get_random_string(6)
    )
    return transaction_code


def import_sales_from_xls(filename, account):
    workbook = load_workbook(filename, data_only=True)
    set_current_tenant(account)

    sales_count = 0
    sales_skip = 0

    for sheetname in workbook.sheetnames:
        worksheet = workbook[sheetname]
        sales_order = None
        sales_order_savepoint = None
        sales_order_err_set = []
        product_order_err_set = []

        for index, sheet_row in enumerate(worksheet.values):
            row = convert_sheet_row(sheet_row)

            if row["sales_code"] == "sales_code":
                continue

            if row["sales_code"]:
                if row["transaction_date"] and not isinstance(row["transaction_date"], datetime):
                    try:    
                        row["transaction_date"] = datetime.strptime(
                            row["transaction_date"], "%d/%m/%Y %H:%M")
                    except Exception as e:
                        msg = f"Unable to convert SalesOrder transaction_date for row number {index} {row['order_name']} {row['transaction_date']} {row['outlet']}, it raises {e}."
                        sales_order_err_set.append(msg)
                        print(msg)
                        pass

                transaction_date = row["transaction_date"]
                sales_count += 0

                #Existing SalesOrder !? Continue
                try:
                    outlet = get_outlet(row["outlet"])

                    sales_order = SalesOrder.objects.get(
                        order_name=row["order_name"],
                        transaction_date=transaction_date,
                        outlet=outlet,
                    )
                    if sales_order:
                        #Log Existing SalesOrder and continue
                        sales_order = None
                        print(
                            f"SalesOrder for row number {index} {row['order_name']} {row['transaction_date']} {row['outlet']} already exist.")
                except SalesOrder.DoesNotExist:
                    print(
                        f"SalesOrder not found, creating new SalesOrder for {row['order_name']} {row['transaction_date']} {row['outlet']}.")
                    
                    #Build New SalesOrder
                    try:
                        with transaction.atomic():
                            tax = row["tax"] if row["tax"] is not None else 0
                            sales_order = SalesOrder.objects.create(
                                status="settled",
                                cashier_name="admin",
                                served_by="admin",
                                guest_number=row["guest_number"],
                                order_name=row["order_name"],

                                code=generate_sales_code(
                                    row["transaction_date"],
                                    outlet),
                                transaction_date=transaction_date,
                                outlet=outlet,
                                order_option=get_order_option(row["order_option"]),

                                global_discount_fixed_price=row["global_discount_price"] if row["global_discount_price"] is not None else 0,
                                global_discount_price=row["global_discount_price"] if row["global_discount_price"] is not None else 0,
                                sub_total=row["sub_total"],
                                tax=tax,
                                total_cost=row["total_cost"],
                                tendered_amount=row["tendered_amount"],
                                change=row["change"],
                            )
                            PaymentDetail.objects.create(
                                sales_order=sales_order,
                                amount=row["tendered_amount"],
                                amount_used=row["total_cost"],
                                payment_method=get_payment_method(row["payment_method"]),
                            )
                        sales_order_savepoint = transaction.savepoint()
                        print(
                            f"SalesOrder for {row['order_name']} {row['transaction_date']} {row['outlet']} has been created.")
                    except Exception as e:
                        sales_order = None
                        msg = f"Unable to create SalesOrder for row number {index} {row['order_name']} {row['transaction_date']} {row['outlet']}, it raises {e}."
                        sales_order_err_set.append(msg)
                        print(msg)
                        # Log SalesOrder code and raised exception, then continue
                except Exception as e:
                    sales_order = None
                    msg = f"Unable to check for SalesOrder row number {index} {row['order_name']} {row['transaction_date']} {row['outlet']}, it raises {e}."
                    sales_order_err_set.append(msg)
                    print(msg)

            if sales_order and row["product"]:
                #Build ProductOrder
                try:
                    with transaction.atomic():
                        product_order = ProductOrder.objects.create(
                            sales_order=sales_order,
                            product=get_product(row["product"]),
                            base_cost=row["base_cost"],
                            mode="fixed_amount" if row["product_order_discount_price"] > 0 else "none",
                            behaviour="reduced_by" if row["product_order_discount_price"] > 0 else "none",
                            quantity=row["quantity"],
                            surcharge_fixed_price=row["surcharge_price"] if row["surcharge_price"] is not None else 0,
                            surcharge_price=(row["surcharge_price"] * row["quantity"]) if row["surcharge_price"] is not None else 0,
                            surcharges=get_surcharge(sales_order.order_option) if row["surcharge_price"] is not None and row["surcharge_price"] > 0 else None,
                            discount_price=row["product_order_discount_price"],
                            discount_fixed_price=row["product_order_discount_price"],
                            sub_total=row["product_order_sub_total"],
                            addon_cost=row["addon_cost"],
                            total_cost=row["product_order_total_cost"],
                            modifier_selected="",
                        )
                    print(
                        f"ProductOrder for row number {index} {sales_order} {row['product']} {row['quantity']} has been created.")
                except Exception as e:
                    msg = f"Unable to create ProductOrder for row number {index} {sales_order} {row['product']} {row['quantity']}, it raises {e}." 
                    msg_2 = f"Rolling back to {sales_order_savepoint}." 
                    product_order_err_set.append(msg)
                    product_order_err_set.append(msg_2)
                    print(msg)
                    print(msg_2)

                    if sales_order_savepoint:
                        transaction.savepoint_rollback(sales_order_savepoint)

                    try:
                        sales_order.delete(force_policy=HARD_DELETE)
                    except:
                        pass

                    # Log SalesOrder code, ProductOrder and raised exception, then continue
            elif row["product"]:
                print(
                    f"ProductOrder for {row['product']} {row['quantity']}, has been skipped.")
                #Log skipped ProductOrder

        print(f"SALES ORDER ERR : {sales_order_err_set}")
        print(f"PRODUCT ORDER ERR : {product_order_err_set}")

        with open(f"{filename}-{sheetname}.txt", "w+") as errfile:
            for err_line in sales_order_err_set:
                errfile.write(err_line)
                errfile.write("\n")
            for err_line in product_order_err_set:
                errfile.write(err_line)
                errfile.write("\n")

    set_current_tenant(None)
