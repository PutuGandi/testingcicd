# Generated by Django 2.0.4 on 2018-09-05 07:09

from django.db import migrations


def apply_missing_disabled_payment_method(apps, schema_editor):
    Account = apps.get_model("accounts", "Account")
    for account in Account.objects.all():
        if not account.disabledpaymentmethod_set.exists():
            account.disabledpaymentmethod_set.create(account=account)


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0015_auto_20180904_0917'),
    ]

    operations = [
        migrations.RunPython(
            apply_missing_disabled_payment_method, migrations.RunPython.noop),
    ]
