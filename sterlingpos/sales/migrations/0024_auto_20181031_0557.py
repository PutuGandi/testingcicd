# Generated by Django 2.0.4 on 2018-10-31 05:57

from decimal import Decimal
from django.db import migrations
import djmoney.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0023_auto_20181015_1022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymentdetail',
            name='amount',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='paymentdetail',
            name='amount_used',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='productorder',
            name='addon_cost',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='productorder',
            name='discount_fixed_price',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='productorder',
            name='discount_price',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='productorder',
            name='sub_total',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='productorder',
            name='total_cost',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='productorderproductaddondetail',
            name='sub_total',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorder',
            name='change',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorder',
            name='global_discount_fixed_price',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorder',
            name='global_discount_price',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorder',
            name='sub_total',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorder',
            name='tendered_amount',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorder',
            name='total_cost',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorderpromohistory',
            name='affected_value',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorderpromohistory',
            name='base_value',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
        migrations.AlterField(
            model_name='salesorderpromohistory',
            name='discount_fixed_price',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0.0'), default_currency='IDR', max_digits=12),
        ),
    ]
