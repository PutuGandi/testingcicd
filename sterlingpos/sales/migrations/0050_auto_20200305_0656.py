# Generated by Django 2.2.9 on 2020-03-05 06:56

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


def populate_cashier_activity_reason(apps, schema_editor):

    def _display_reason(description):
        loc = description.find("Reason")
        if loc >= 0:
            return description[loc + 8 :].strip()
        return ""

    CashierActivity = apps.get_model("sales", "CashierActivity")
    for cashier_activity in CashierActivity.objects.filter(activity_type="CART_VOID"):
        cashier_activity.reason = _display_reason(
            cashier_activity.description)
        cashier_activity.save()


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_account_registered_via'),
        ('sales', '0049_merge_20200102_0704'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cashieractivity',
            options={},
        ),
        migrations.AddField(
            model_name='cashieractivity',
            name='reason',
            field=models.TextField(blank=True, default='', verbose_name='Activity Reason'),
        ),
        migrations.CreateModel(
            name='CashierActivityReason',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(max_length=80, verbose_name='Reason Name')),
                ('description', models.TextField(verbose_name='Reason description')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
            ],
            options={
                'verbose_name': 'CashierActivityReason',
                'verbose_name_plural': 'CashierActivityReasons',
                'unique_together': {('account', 'id')},
            },
        ),
        migrations.RunPython(
            populate_cashier_activity_reason, migrations.RunPython.noop),
    ]
