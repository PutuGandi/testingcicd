# Generated by Django 2.2.16 on 2021-11-03 08:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0061_merge_20211101_1322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoiceinstallment',
            name='invoice',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='installment', to='sales.SalesOrderInvoice', verbose_name='Invoice'),
        ),
    ]
