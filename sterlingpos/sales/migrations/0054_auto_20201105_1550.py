# Generated by Django 2.2.12 on 2020-11-05 08:50

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import sterlingpos.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0012_account_business_code'),
        ('sales', '0053_auto_20200910_1325'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionTypeReason',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('description', models.CharField(max_length=120)),
                ('transaction_type', models.CharField(choices=[('city_ledger', 'City Ledger'), ('foc', 'Free of Charge')], default='foc', max_length=120)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
            ],
            options={
                'verbose_name': 'TransactionTypeReason',
                'verbose_name_plural': 'TransactionTypeReasons',
                'unique_together': {('account', 'id')},
            },
        ),
        migrations.AddField(
            model_name='transactiontype',
            name='reason',
            field=sterlingpos.core.models.SterlingTenantForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='usages', to='sales.TransactionTypeReason'),
        ),
    ]
