from datetime import timedelta
from django.utils import timezone
from django.db.models import Sum
from django.db.models.functions import Coalesce

from model_mommy import mommy
from test_plus.test import TestCase

from sterlingpos.sales.models import (
    PaymentMethod, SalesOrder,
    ProductSalesProxy, CategorySalesProxy,
)
from sterlingpos.core.models import set_current_tenant
from sterlingpos.catalogue.categories import create_from_sequence, create_from_breadcrumbs
from safedelete.config import HARD_DELETE


class PaymentTypeSalesManagerTest(TestCase):

    def prepare_sales_order(self, free_of_charge=False, **kwargs):
        """
        Prepare 2 Sales Order
            - 1 Sales Order with 1 Product Order, 3 quantity
            - 1 Sales Order with 3 hours date difference with 1 Product Order, 3 quantity
        """
        self.product = mommy.make(
            'catalogue.Product', price=20000, account=self.account)

        sales_order = mommy.make(
            'sales.SalesOrder', transaction_date=self.now, account=self.account, **kwargs
        )
        sales_order_3_hours_before = mommy.make(
            'sales.SalesOrder', transaction_date=self.now - timedelta(hours=3), account=self.account, **kwargs)

        product_order = mommy.make(
            'sales.ProductOrder',
            discount_percentage=0,
            quantity=3,
            product=self.product,
            sales_order=sales_order,
            account=self.account)
        product_order = mommy.make(
            'sales.ProductOrder',
            discount_percentage=0,
            quantity=3,
            product=self.product,
            sales_order=sales_order_3_hours_before,
            account=self.account)

        if free_of_charge:
            # FREE OF CHARGE ( NO PAYMENT DETAIL )
            transaction_type_foc = mommy.make(
                'sales.TransactionType',
                transaction_type='foc',
                account=self.account)
            transaction_type_foc.sales_order.add(sales_order)
            transaction_type_foc.sales_order.add(sales_order_3_hours_before)
        else:
            sales_order.payment_detail.create(
                payment_method=self.payment_method_1,
                amount=self.product.price.amount * 3 / 2,
                amount_used=self.product.price.amount * 3 / 2,
                account=self.account
            )

            sales_order.payment_detail.create(
                payment_method=self.payment_method_2,
                amount=self.product.price.amount * 3 / 2,
                amount_used=self.product.price.amount * 3 / 2,
                account=self.account
            )

            sales_order_3_hours_before.payment_detail.create(
                payment_method=self.payment_method_1,
                amount=self.product.price.amount * 3 / 2,
                amount_used=self.product.price.amount * 3 / 2,
                account=self.account
            )

            sales_order_3_hours_before.payment_detail.create(
                payment_method=self.payment_method_2,
                amount=self.product.price.amount * 3 / 2,
                amount_used=self.product.price.amount * 3 / 2,
                account=self.account
            )

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        self.payment_method_1 = mommy.make(
            'sales.PaymentMethod', name='cash')
        self.payment_method_2 = mommy.make(
            'sales.PaymentMethod', name='gold')

        self.now = timezone.now()

        self.prepare_sales_order()
        self.prepare_sales_order()
        self.prepare_sales_order(status=SalesOrder.STATUS.unsettled)
        self.prepare_sales_order(status=SalesOrder.STATUS.unsettled)
        self.prepare_sales_order(free_of_charge=True)

    def test_get_payment_method_sales(self):
        total_payment_usage = PaymentMethod.objects.payment_sales().aggregate(
            total_payment_usage=Coalesce(Sum('payment_usage'), 0)).get('total_payment_usage')
        self.assertEqual(total_payment_usage, 8)

        annotated_payment_sales = PaymentMethod.objects.payment_sales()
        payment_sales_for_payment_method_1 = annotated_payment_sales.get(pk=self.payment_method_1.pk).payment_usage
        payment_sales_for_payment_method_2 = annotated_payment_sales.get(pk=self.payment_method_2.pk).payment_usage

        self.assertEqual(payment_sales_for_payment_method_1, 4)
        self.assertEqual(payment_sales_for_payment_method_2, 4)

    def test_get_payment_method_sales_based_on_date(self):
        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        total_payment_usages = PaymentMethod.objects.payment_sales(**kwargs).aggregate(
            total_payment_usages=Coalesce(Sum('payment_usage'), 0)).get('total_payment_usages')
        self.assertEqual(total_payment_usages, 4)

        annotated_payment_sales = PaymentMethod.objects.payment_sales(**kwargs)
        payment_sales_for_payment_method_1 = annotated_payment_sales.get(pk=self.payment_method_1.pk).payment_usage
        payment_sales_for_payment_method_2 = annotated_payment_sales.get(pk=self.payment_method_2.pk).payment_usage

        self.assertEqual(payment_sales_for_payment_method_1, 2)
        self.assertEqual(payment_sales_for_payment_method_2, 2)

    def test_get_payment_method_sales_return_zero_if_no_sales_found(self):
        PaymentMethod.objects.all().delete()
        total_payment_usages = PaymentMethod.objects.payment_sales().aggregate(
            total_payment_usages=Coalesce(Sum('payment_usage'), 0)).get('total_payment_usages')
        self.assertEqual(total_payment_usages, 0)

    def test_get_payment_method_sales_net(self):

        total_payment_net = PaymentMethod.objects.payment_sales().aggregate(
            total_payment_net=Coalesce(Sum('payment_net'), 0)).get('total_payment_net')
        self.assertEqual(total_payment_net, 8 * (self.product.price.amount * 3 / 2))

        annotated_payment_method_net = PaymentMethod.objects.payment_sales()
        payment_method_net_for_payment_method_1 = annotated_payment_method_net.get(pk=self.payment_method_1.pk).payment_net
        payment_method_net_for_payment_method_2 = annotated_payment_method_net.get(pk=self.payment_method_2.pk).payment_net

        self.assertEqual(payment_method_net_for_payment_method_1, 4 * (self.product.price.amount * 3 / 2))
        self.assertEqual(payment_method_net_for_payment_method_2, 4 * (self.product.price.amount * 3 / 2))

    def test_get_payment_method_sales_net_based_on_date(self):
        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        total_payment_net = PaymentMethod.objects.payment_sales(**kwargs).aggregate(
            total_payment_net=Coalesce(Sum('payment_net'), 0)).get('total_payment_net')
        self.assertEqual(total_payment_net, 4 * (self.product.price.amount * 3 / 2))

        annotated_payment_method_net = PaymentMethod.objects.payment_sales(**kwargs)
        payment_method_net_for_payment_method_1 = annotated_payment_method_net.get(pk=self.payment_method_1.pk).payment_net
        payment_method_net_for_payment_method_2 = annotated_payment_method_net.get(pk=self.payment_method_2.pk).payment_net

        self.assertEqual(payment_method_net_for_payment_method_1, 2 * (self.product.price.amount * 3 / 2))
        self.assertEqual(payment_method_net_for_payment_method_2, 2 * ((self.product.price.amount * 3 / 2)))

    def test_get_category_sales_net_return_zero_if_no_sales_found(self):
        PaymentMethod.objects.all().delete()
        total_payment_net = PaymentMethod.objects.payment_sales().aggregate(
            total_payment_net=Coalesce(Sum('payment_net'), 0)).get('total_payment_net')
        self.assertEqual(total_payment_net, 0)

    def test_payment_method_sales_with_outlet(self):
        branch_a = mommy.make('outlet.Outlet', name="A", branch_id='A', account=self.account)

        self.prepare_sales_order(outlet=branch_a)

        total_payment_usage = PaymentMethod.objects.payment_sales(outlet=branch_a).aggregate(
            total_payment_usage=Coalesce(Sum('payment_usage'), 0)).get('total_payment_usage')
        self.assertEqual(total_payment_usage, 4)

        total_payment_net = PaymentMethod.objects.payment_sales(outlet=branch_a).aggregate(
            total_payment_net=Coalesce(Sum('payment_net'), 0)).get('total_payment_net')
        self.assertEqual(total_payment_net, 4 * (self.product.price.amount * 3 / 2))

    def test_payment_method_sales_with_exclude_zero_sales(self):
        self.payment_method_1.salesorder_set.all().delete(force_policy=HARD_DELETE)
        payment_method_sales_list = PaymentMethod.objects.payment_sales(exclude_zero_sales=True)
        self.assertEqual(payment_method_sales_list.count(), 2)


class ProductSalesManagerTest(TestCase):

    def prepare_product_sales(self, product, free_of_charge=False, **kwargs):
        """
        Prepare 2 Sales Order
            - 1 Sales Order with 1 Product Order, 3 quantity
            - 1 Sales Order with 3 hours date difference with 1 Product Order, 3 quantity
        """
        sales_order = mommy.make(
            'sales.SalesOrder',
            transaction_date=self.now, account=self.account, **kwargs)
        sales_order_3_hours_before = mommy.make(
            'sales.SalesOrder',
            transaction_date=self.now - timedelta(hours=3), account=self.account, **kwargs)

        product_order = mommy.make(
            'sales.ProductOrder',
            discount_percentage=0,
            quantity=3,
            product=product,
            sales_order=sales_order,
            account=self.account)
        product_order = mommy.make(
            'sales.ProductOrder',
            discount_percentage=0,
            quantity=3,
            product=product,
            sales_order=sales_order_3_hours_before,
            account=self.account)

        if free_of_charge:
            transaction_type_foc = mommy.make(
                'sales.TransactionType', transaction_type='foc', account=self.account)
            transaction_type_foc.sales_order.add(sales_order)
            transaction_type_foc.sales_order.add(sales_order_3_hours_before)

    def setUp(self):
        self.now = timezone.now()
        self.account = mommy.make('accounts.Account', name='account-1')

        self.product = mommy.make('catalogue.Product', price=15000, account=self.account)
        self.product_2 = mommy.make('catalogue.Product', price=15000, account=self.account)
        self.product_3 = mommy.make('catalogue.Product', price=15000, account=self.account)

        Product = self.product.__class__

        self.prepare_product_sales(self.product)
        self.prepare_product_sales(self.product_2)
        self.prepare_product_sales(self.product_2, status=SalesOrder.STATUS.unsettled)
        self.prepare_product_sales(self.product_3, free_of_charge=True, status=SalesOrder.STATUS.unsettled)
        self.prepare_product_sales(self.product_3, free_of_charge=True)

    def test_get_item_sales(self):
        total_product_sales = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_sales=Coalesce(Sum('product_sales_count'), 0)).get('total_product_sales')
        self.assertEqual(total_product_sales, 18)

        annotated_product_sales = ProductSalesProxy.objects.product_sales()
        product_sales_count_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_sales_count
        product_sales_count_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_sales_count
        product_sales_count_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_sales_count

        self.assertEqual(product_sales_count_for_product_1, 6)
        self.assertEqual(product_sales_count_for_product_2, 6)
        self.assertEqual(product_sales_count_for_product_3, 6)

    def test_get_item_sales_based_on_date(self):
        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        product_sales = ProductSalesProxy.objects.product_sales(**kwargs).aggregate(
            total_product_sales=Coalesce(Sum('product_sales_count'), 0)).get('total_product_sales')
        self.assertEqual(product_sales, 9)

        annotated_product_sales = ProductSalesProxy.objects.product_sales(**kwargs)
        product_sales_count_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_sales_count
        product_sales_count_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_sales_count
        product_sales_count_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_sales_count

        self.assertEqual(product_sales_count_for_product_1, 3)
        self.assertEqual(product_sales_count_for_product_2, 3)
        self.assertEqual(product_sales_count_for_product_3, 3)

    def test_get_item_sales_return_zero_if_no_sales_found(self):
        ProductSalesProxy.objects.all().delete(force_policy=HARD_DELETE)
        product_sales = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_sales=Coalesce(Sum('product_sales_count'), 0)).get('total_product_sales')
        self.assertEqual(product_sales, 0)

    def test_get_item_sales_net(self):
        product_sales_net = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_sales_net=Coalesce(Sum('product_sales_net'), 0)).get('total_product_sales_net')
        self.assertEqual(product_sales_net, 12 * self.product.price.amount)

        annotated_product_sales = ProductSalesProxy.objects.product_sales()
        product_sales_net_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_sales_net
        product_sales_net_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_sales_net
        product_sales_net_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_sales_net

        self.assertEqual(product_sales_net_for_product_1, 6 * self.product.price.amount)
        self.assertEqual(product_sales_net_for_product_2, 6 * self.product_2.price.amount)
        self.assertEqual(product_sales_net_for_product_3, 0)

    def test_get_item_sales_net_based_on_date(self):
        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        product_sales_net = ProductSalesProxy.objects.product_sales(**kwargs).aggregate(
            total_product_sales_net=Coalesce(Sum('product_sales_net'), 0)).get('total_product_sales_net')
        self.assertEqual(product_sales_net, 6 * self.product.price.amount)

        annotated_product_sales = ProductSalesProxy.objects.product_sales(**kwargs)
        product_sales_net_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_sales_net
        product_sales_net_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_sales_net
        product_sales_net_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_sales_net

        self.assertEqual(product_sales_net_for_product_1, 3 * self.product.price.amount)
        self.assertEqual(product_sales_net_for_product_2, 3 * self.product_2.price.amount)
        self.assertEqual(product_sales_net_for_product_3, 0)

    def test_get_item_sales_net_return_zero_if_no_sales_found(self):
        ProductSalesProxy.objects.all().delete(force_policy=HARD_DELETE)
        product_sales_net = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_sales_net=Coalesce(Sum('product_sales_net'), 0)).get('total_product_sales_net')
        self.assertEqual(product_sales_net, 0)

    def test_get_item_total_sales(self):
        product_total_sales = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('total_product_total_sales')
        self.assertEqual(product_total_sales, 18 * self.product.price.amount)

        annotated_product_sales = ProductSalesProxy.objects.product_sales()
        product_total_sales_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_total_sales
        product_total_sales_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_total_sales
        product_total_sales_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_total_sales

        self.assertEqual(product_total_sales_for_product_1, 6 * self.product.price.amount)
        self.assertEqual(product_total_sales_for_product_2, 6 * self.product_2.price.amount)
        self.assertEqual(product_total_sales_for_product_3, 6 * self.product_2.price.amount)

    def test_get_item_total_sales_based_on_date(self):

        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        product_total_sales = ProductSalesProxy.objects.product_sales(**kwargs).aggregate(
            total_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('total_product_total_sales')
        self.assertEqual(product_total_sales, 9 * self.product.price.amount)

        annotated_product_sales = ProductSalesProxy.objects.product_sales(**kwargs)
        product_total_sales_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_total_sales
        product_total_sales_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_total_sales
        product_total_sales_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_total_sales

        self.assertEqual(product_total_sales_for_product_1, 3 * self.product.price.amount)
        self.assertEqual(product_total_sales_for_product_2, 3 * self.product_2.price.amount)
        self.assertEqual(product_total_sales_for_product_3, 3 * self.product_3.price.amount)

    def test_get_item_total_sales_return_zero_if_no_sales_found(self):
        ProductSalesProxy.objects.all().delete(force_policy=HARD_DELETE)
        product_total_sales = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('total_product_total_sales')
        self.assertEqual(product_total_sales, 0)

    def test_get_item_foc_sales(self):
        product_sales_foc = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_sales_foc=Coalesce(Sum('product_sales_foc'), 0)).get('total_product_sales_foc')
        self.assertEqual(product_sales_foc, 6 * self.product.price.amount)

        annotated_product_sales = ProductSalesProxy.objects.product_sales()
        product_sales_foc_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_sales_foc
        product_sales_foc_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_sales_foc
        product_sales_foc_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_sales_foc

        self.assertEqual(product_sales_foc_for_product_1, 0)
        self.assertEqual(product_sales_foc_for_product_2, 0)
        self.assertEqual(product_sales_foc_for_product_3, 6 * self.product.price.amount)

    def test_get_item_foc_sales_based_on_date(self):

        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        product_sales_foc = ProductSalesProxy.objects.product_sales(**kwargs).aggregate(
            total_product_sales_foc=Coalesce(Sum('product_sales_foc'), 0)).get('total_product_sales_foc')
        self.assertEqual(product_sales_foc, 3 * self.product.price.amount)

        annotated_product_sales = ProductSalesProxy.objects.product_sales(**kwargs)
        product_sales_foc_for_product_1 = annotated_product_sales.get(pk=self.product.pk).product_sales_foc
        product_sales_foc_for_product_2 = annotated_product_sales.get(pk=self.product_2.pk).product_sales_foc
        product_sales_foc_for_product_3 = annotated_product_sales.get(pk=self.product_3.pk).product_sales_foc

        self.assertEqual(product_sales_foc_for_product_1, 0)
        self.assertEqual(product_sales_foc_for_product_2, 0)
        self.assertEqual(product_sales_foc_for_product_3, 3 * self.product_3.price.amount)

    def test_get_item_foc_sales_return_zero_if_no_sales_found(self):
        ProductSalesProxy.objects.all().delete(force_policy=HARD_DELETE)
        product_sales_foc = ProductSalesProxy.objects.product_sales().aggregate(
            total_product_sales_foc=Coalesce(Sum('product_sales_foc'), 0)).get('total_product_sales_foc')
        self.assertEqual(product_sales_foc, 0)

    def test_product_sales_with_outlet(self):
        branch_a = mommy.make('outlet.Outlet', name="A", branch_id='A', account=self.account)

        product = mommy.make('catalogue.Product', price=15000, account=self.account)
        self.prepare_product_sales(product, outlet=branch_a)

        total_product_sales = ProductSalesProxy.objects.product_sales(outlet=branch_a).aggregate(
            total_product_sales=Coalesce(Sum('product_sales_count'), 0)).get('total_product_sales')
        self.assertEqual(total_product_sales, 6)

        product_sales_net = ProductSalesProxy.objects.product_sales(outlet=branch_a).aggregate(
            total_product_sales_net=Coalesce(Sum('product_sales_net'), 0)).get('total_product_sales_net')
        self.assertEqual(product_sales_net, 6 * self.product.price.amount)

        product_total_sales = ProductSalesProxy.objects.product_sales(outlet=branch_a).aggregate(
            total_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('total_product_total_sales')
        self.assertEqual(product_total_sales, 6 * self.product.price.amount)

    def test_product_sales_with_exclude_zero_sales(self):
        self.product.productorder_set.all().delete(force_policy=HARD_DELETE)
        product_sales_list = ProductSalesProxy.objects.product_sales(exclude_zero_sales=True)
        self.assertEqual(product_sales_list.count(), 2)


class CategorySalesManagerTest(TestCase):

    def prepare_category_sales(self, category, product, free_of_charge=False, **kwargs):
        """
        Prepare 2 Sales Order
            - 1 Sales Order with 1 Product Order, 3 quantity
            - 1 Sales Order with 3 hours date difference with 1 Product Order, 3 quantity
        """
        category.product_set.add(product)

        sales_order = mommy.make(
            'sales.SalesOrder',
            transaction_date=self.now, account=self.account, **kwargs)
        sales_order_3_hours_before = mommy.make(
            'sales.SalesOrder',
            transaction_date=self.now - timedelta(hours=3), account=self.account, **kwargs)

        product_order = mommy.make('sales.ProductOrder',
                                   discount_percentage=0,
                                   quantity=3,
                                   product=product,
                                   sales_order=sales_order,
                                   account=self.account)
        product_order = mommy.make('sales.ProductOrder',
                                   discount_percentage=0,
                                   quantity=3,
                                   product=product,
                                   sales_order=sales_order_3_hours_before,
                                   account=self.account)
        if free_of_charge:
            transaction_type_foc = mommy.make('sales.TransactionType', transaction_type='foc', account=self.account)
            transaction_type_foc.sales_order.add(sales_order)
            transaction_type_foc.sales_order.add(sales_order_3_hours_before)

    def setUp(self):
        self.now = timezone.now()
        self.account = mommy.make('accounts.Account', name='account-1')

        self.product = mommy.make('catalogue.Product', price=15000, account=self.account)
        self.product_2 = mommy.make('catalogue.Product', price=15000, account=self.account)
        self.product_3 = mommy.make('catalogue.Product', price=15000, account=self.account)

        self.category_1 = create_from_breadcrumbs('category-1', account=self.account)
        self.category_2 = create_from_breadcrumbs('category-2', account=self.account)
        self.category_3 = create_from_breadcrumbs('category-3', account=self.account)

        self.prepare_category_sales(self.category_1, self.product)
        self.prepare_category_sales(self.category_2, self.product_2)
        self.prepare_category_sales(self.category_2, self.product_2, status=SalesOrder.STATUS.unsettled)
        self.prepare_category_sales(self.category_3, self.product_3, free_of_charge=True)

    def test_get_category_sales(self):
        total_category_sales_count = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_sales_count=Coalesce(Sum('category_sales_count'), 0)).get('total_category_sales_count')
        self.assertEqual(total_category_sales_count, 18)

        annotated_category_sales = CategorySalesProxy.objects.category_sales()
        category_sales_count_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_sales_count
        category_sales_count_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_sales_count
        category_sales_count_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_sales_count

        self.assertEqual(category_sales_count_for_category_1, 6)
        self.assertEqual(category_sales_count_for_category_2, 6)
        self.assertEqual(category_sales_count_for_category_3, 6)

    def test_get_category_sales_based_on_date(self):
        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        total_category_sales_count = CategorySalesProxy.objects.category_sales(**kwargs).aggregate(
            total_category_sales_count=Coalesce(Sum('category_sales_count'), 0)).get('total_category_sales_count')
        self.assertEqual(total_category_sales_count, 9)

        annotated_category_sales = CategorySalesProxy.objects.category_sales(**kwargs)
        category_sales_count_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_sales_count
        category_sales_count_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_sales_count
        category_sales_count_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_sales_count

        self.assertEqual(category_sales_count_for_category_1, 3)
        self.assertEqual(category_sales_count_for_category_2, 3)
        self.assertEqual(category_sales_count_for_category_3, 3)

    def test_get_category_sales_return_zero_if_no_sales_found(self):
        SalesOrder.objects.all().delete(force_policy=HARD_DELETE)
        total_category_sales_count = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_sales_count=Coalesce(Sum('category_sales_count'), 0)).get('total_category_sales_count')
        self.assertEqual(total_category_sales_count, 0)

    def test_get_category_sales_net(self):
        category_sales_net = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_sales_net=Coalesce(Sum('category_sales_net'), 0)).get('total_category_sales_net')
        self.assertEqual(category_sales_net, 12 * self.product.price.amount)

        annotated_category_sales = CategorySalesProxy.objects.category_sales()
        category_sales_net_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_sales_net
        category_sales_net_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_sales_net
        category_sales_net_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_sales_net

        self.assertEqual(category_sales_net_for_category_1, 6 * self.product.price.amount)
        self.assertEqual(category_sales_net_for_category_2, 6 * self.product_2.price.amount)
        self.assertEqual(category_sales_net_for_category_3, 0)

    def test_get_category_sales_net_based_on_date(self):
        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        category_sales_net = CategorySalesProxy.objects.category_sales(**kwargs).aggregate(
            total_category_sales_net=Coalesce(Sum('category_sales_net'), 0)).get('total_category_sales_net')
        self.assertEqual(category_sales_net, 6 * self.product.price.amount)

        annotated_category_sales = CategorySalesProxy.objects.category_sales(**kwargs)
        category_sales_net_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_sales_net
        category_sales_net_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_sales_net
        category_sales_net_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_sales_net

        self.assertEqual(category_sales_net_for_category_1, 3 * self.product.price.amount)
        self.assertEqual(category_sales_net_for_category_2, 3 * self.product_2.price.amount)
        self.assertEqual(category_sales_net_for_category_3, 0)

    def test_get_category_sales_net_return_zero_if_no_sales_found(self):
        SalesOrder.objects.all().delete(force_policy=HARD_DELETE)
        category_sales_net = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_sales_net=Coalesce(Sum('category_sales_net'), 0)).get('total_category_sales_net')
        self.assertEqual(category_sales_net, 0)

    def test_get_category_total_sales(self):
        category_total_sales = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_total_sales=Coalesce(Sum('category_total_sales'), 0)).get('total_category_total_sales')
        self.assertEqual(category_total_sales, 18 * self.product.price.amount)

        product_sales = ProductSalesProxy.objects.product_sales()

        annotated_category_sales = CategorySalesProxy.objects.category_sales()
        category_total_sales_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_total_sales
        category_total_sales_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_total_sales
        category_total_sales_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_total_sales

        product_sales_for_category_1 = product_sales.filter(category=self.category_1).aggregate(
            sum_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('sum_product_total_sales')
        product_sales_for_category_2 = product_sales.filter(category=self.category_2).aggregate(
            sum_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('sum_product_total_sales')
        product_sales_for_category_3 = product_sales.filter(category=self.category_3).aggregate(
            sum_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('sum_product_total_sales')

        self.assertEqual(category_total_sales_for_category_1, 6 * self.product.price.amount)
        self.assertEqual(category_total_sales_for_category_2, 6 * self.product_2.price.amount)
        self.assertEqual(category_total_sales_for_category_3, 6 * self.product_3.price.amount)

        self.assertEqual(category_total_sales_for_category_1, product_sales_for_category_1)
        self.assertEqual(category_total_sales_for_category_2, product_sales_for_category_2)
        self.assertEqual(category_total_sales_for_category_3, product_sales_for_category_3)

    def test_get_category_total_sales_based_on_date(self):

        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        category_total_sales = CategorySalesProxy.objects.category_sales(**kwargs).aggregate(
            total_category_total_sales=Coalesce(Sum('category_total_sales'), 0)).get('total_category_total_sales')
        self.assertEqual(category_total_sales, 9 * self.product.price.amount)

        annotated_category_sales = CategorySalesProxy.objects.category_sales(**kwargs)
        category_total_sales_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_total_sales
        category_total_sales_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_total_sales
        category_total_sales_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_total_sales

        self.assertEqual(category_total_sales_for_category_1, 3 * self.product.price.amount)
        self.assertEqual(category_total_sales_for_category_2, 3 * self.product_2.price.amount)
        self.assertEqual(category_total_sales_for_category_3, 3 * self.product_3.price.amount)

        product_sales = ProductSalesProxy.objects.product_sales(**kwargs)

        product_sales_for_category_1 = product_sales.filter(category=self.category_1).aggregate(
            sum_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('sum_product_total_sales')
        product_sales_for_category_2 = product_sales.filter(category=self.category_2).aggregate(
            sum_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('sum_product_total_sales')
        product_sales_for_category_3 = product_sales.filter(category=self.category_3).aggregate(
            sum_product_total_sales=Coalesce(Sum('product_total_sales'), 0)).get('sum_product_total_sales')

        self.assertEqual(category_total_sales_for_category_1, product_sales_for_category_1)
        self.assertEqual(category_total_sales_for_category_2, product_sales_for_category_2)
        self.assertEqual(category_total_sales_for_category_3, product_sales_for_category_3)

    def test_get_category_total_sales_return_zero_if_no_sales_found(self):
        SalesOrder.objects.all().delete(force_policy=HARD_DELETE)
        category_total_sales = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_total_sales=Coalesce(Sum('category_total_sales'), 0)).get('total_category_total_sales')
        self.assertEqual(category_total_sales, 0)

    def test_get_category_foc_sales(self):
        category_sales_foc = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_sales_foc=Coalesce(Sum('category_sales_foc'), 0)).get('total_category_sales_foc')
        self.assertEqual(category_sales_foc, 6 * self.product.price.amount)

        annotated_category_sales = CategorySalesProxy.objects.category_sales()
        category_sales_foc_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_sales_foc
        category_sales_foc_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_sales_foc
        category_sales_foc_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_sales_foc

        self.assertEqual(category_sales_foc_for_category_1, 0)
        self.assertEqual(category_sales_foc_for_category_2, 0)
        self.assertEqual(category_sales_foc_for_category_3, 6 * self.product.price.amount)

    def test_get_category_foc_sales_based_on_date(self):

        kwargs = {
            'start_date': self.now,
            'end_date': self.now
        }
        category_sales_foc = CategorySalesProxy.objects.category_sales(**kwargs).aggregate(
            total_category_sales_foc=Coalesce(Sum('category_sales_foc'), 0)).get('total_category_sales_foc')
        self.assertEqual(category_sales_foc, 3 * self.product.price.amount)

        annotated_category_sales = CategorySalesProxy.objects.category_sales(**kwargs)
        category_sales_foc_for_category_1 = annotated_category_sales.get(pk=self.category_1.pk).category_sales_foc
        category_sales_foc_for_category_2 = annotated_category_sales.get(pk=self.category_2.pk).category_sales_foc
        category_sales_foc_for_category_3 = annotated_category_sales.get(pk=self.category_3.pk).category_sales_foc

        self.assertEqual(category_sales_foc_for_category_1, 0)
        self.assertEqual(category_sales_foc_for_category_2, 0)
        self.assertEqual(category_sales_foc_for_category_3, 3 * self.product_3.price.amount)

    def test_get_item_foc_sales_return_zero_if_no_sales_found(self):
        SalesOrder.objects.all().delete(force_policy=HARD_DELETE)
        category_sales_foc = CategorySalesProxy.objects.category_sales().aggregate(
            total_category_sales_foc=Coalesce(Sum('category_sales_foc'), 0)).get('total_category_sales_foc')
        self.assertEqual(category_sales_foc, 0)

    def test_category_sales_with_outlet(self):
        branch_a = mommy.make('outlet.Outlet', name="A", branch_id='A', account=self.account)

        product = mommy.make('catalogue.Product', price=15000, account=self.account)
        category = create_from_breadcrumbs('category-5', account=self.account)
        self.prepare_category_sales(category, product, outlet=branch_a)

        total_category_sales_count = CategorySalesProxy.objects.category_sales(outlet=branch_a).aggregate(
            total_category_sales_count=Coalesce(Sum('category_sales_count'), 0)).get('total_category_sales_count')
        self.assertEqual(total_category_sales_count, 6)

        category_sales_net = CategorySalesProxy.objects.category_sales(outlet=branch_a).aggregate(
            total_category_sales_net=Coalesce(Sum('category_sales_net'), 0)).get('total_category_sales_net')
        self.assertEqual(category_sales_net, 6 * product.price.amount)

        category_total_sales = CategorySalesProxy.objects.category_sales(outlet=branch_a).aggregate(
            total_category_total_sales=Coalesce(Sum('category_total_sales'), 0)).get('total_category_total_sales')
        self.assertEqual(category_total_sales, 6 * self.product.price.amount)

    def test_category_sales_with_exclude_zero_sales(self):
        self.product.productorder_set.all().delete(force_policy=HARD_DELETE)
        category_sales_list = CategorySalesProxy.objects.category_sales(exclude_zero_sales=True)
        self.assertEqual(category_sales_list.count(), 2)
