from datetime import datetime

from test_plus.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.sales.models import (
    PaymentMethod
)
from sterlingpos.sales.forms import (
    EnabledPaymentMethodForm,
    SalesOrderOptionForm,
    PaymentDetailForm,
)


class SalesOrderOptionFormTest(TestCase):

    def setUp(self):
        account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(account)
        self.form_class = SalesOrderOptionForm

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):

        data = {
            "name": "New Order Option",
        }
        form = self.form_class(data=data)
        self.assertTrue(
            form.is_valid(), (form.data, form.errors))
        obj = form.save()
        
        self.assertIsNotNone(obj.pk)
        self.assertIsNotNone(obj.account, get_current_tenant())
        self.assertTrue(obj.surcharge_set.exists())


class EnabledPaymentMethodFormTest(TestCase):

    def setUp(self):
        account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(account)

        self.form_class = EnabledPaymentMethodForm
        self.payment_method = mommy.make(
            "sales.PaymentMethod", name="Voucher")

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):

        data = {
            "payments": [
                self.payment_method.pk,
            ],
            "enforce_voucher": True,
            "enable_description": True,
        }

        form = self.form_class(data=data)
        self.assertTrue(
            form.is_valid(), (form.data, form.errors))
        obj = form.save()
        
        self.assertIsNotNone(obj.pk)
        self.assertIsNotNone(obj.account, get_current_tenant())
        self.assertTrue(
            obj.payments.filter(pk=self.payment_method.pk).exists())
        self.assertTrue(
            self.payment_method.enforce_description)


class PaymentDetailFormTest(TestCase):

    def setUp(self):
        account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(account)
        self.form_class = PaymentDetailForm
        self.payment_method = mommy.make(
            "sales.PaymentMethod", name="Cash")

    def tearDown(self):
        set_current_tenant(None)

    def test_added_payment(self):
        product = mommy.make('catalogue.Product', cost=500, price=1000)
        sales_order = mommy.make(
            "sales.SalesOrder",
            code='SalesOrderTest', transaction_date=datetime.now(),
            total_cost=1000,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            product=product,
            quantity=1,
            sales_order=sales_order
        )
        invoice = mommy.make(
            "sales.SalesOrderInvoice",
            sales_order=sales_order
        )

        data = {
            "payment_method": self.payment_method.pk,
            "amount_0": 1000,
            "amount_1": "IDR",
            "amount_used_0": 1000,
            "amount_used_1": "IDR",
        }
        form = self.form_class(data=data, sales_order=sales_order)
        self.assertTrue(
            form.is_valid(), (form.data, form.errors))
        obj = form.save()

        self.assertIsNotNone(obj.pk)
        self.assertIsNotNone(obj.account, get_current_tenant())
        self.assertTrue(invoice.is_paid)
        self.assertEqual(invoice.sales_order.status, "settled")

    def test_added_payment_overbalance(self):
        product = mommy.make('catalogue.Product', cost=500, price=1000)
        sales_order = mommy.make(
            "sales.SalesOrder",
            code='SalesOrderTest', transaction_date=datetime.now(),
            total_cost=1000,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            product=product,
            quantity=1,
            sales_order=sales_order
        )
        invoice = mommy.make(
            "sales.SalesOrderInvoice",
            sales_order=sales_order
        )

        data = {
            "payment_method": self.payment_method.pk,
            "amount_0": 1500,
            "amount_1": "IDR",
            "amount_used_0": 1500,
            "amount_used_1": "IDR",
        }
        form = self.form_class(data=data, sales_order=sales_order)
        self.assertFalse(
            form.is_valid(), (form.data, form.errors))

    def test_added_multiple_payment_balance(self):
        product = mommy.make(
            'catalogue.Product', cost=500, price=1000)
        sales_order = mommy.make(
            "sales.SalesOrder",
            code='SalesOrderTest', transaction_date=datetime.now(),
            total_cost=1000,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            product=product,
            quantity=1,
            sales_order=sales_order
        )
        invoice = mommy.make(
            "sales.SalesOrderInvoice",
            sales_order=sales_order
        )

        data = {
            "payment_method": self.payment_method.pk,
            "amount_0": 500,
            "amount_1": "IDR",
            "amount_used_0": 500,
            "amount_used_1": "IDR",
        }
        form = self.form_class(data=data, sales_order=sales_order)
        self.assertTrue(
            form.is_valid(), (form.data, form.errors))
        obj = form.save()

        self.assertIsNotNone(obj.pk)
        self.assertIsNotNone(obj.account, get_current_tenant())
        self.assertFalse(invoice.is_paid)

        data = {
            "payment_method": self.payment_method.pk,
            "amount_0": 500,
            "amount_1": "IDR",
            "amount_used_0": 500,
            "amount_used_1": "IDR",
        }
        form = self.form_class(data=data, sales_order=sales_order)
        self.assertTrue(
            form.is_valid(), (form.data, form.errors))
        obj_2 = form.save()

        self.assertIsNotNone(obj_2.pk)
        self.assertIsNotNone(obj_2.account, get_current_tenant())
        self.assertTrue(invoice.is_paid)
        self.assertEqual(invoice.sales_order.status, "settled")
