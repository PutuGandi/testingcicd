import math
from decimal import Decimal
from django.test import override_settings
from django.core import mail
from test_plus.test import TestCase
from datetime import datetime

from sterlingpos.core.models import set_current_tenant, get_current_tenant

from sterlingpos.sales.models import (
    SalesOrder, SalesOrderOption, ProductOrder, ProductOrderCompositeDetail,
    ProductOrderProductAddOnDetail,
    CashierActivity,
    PaymentMethod, EnabledPaymentMethod, PaymentDetail, SalesOrderInvoice, TransactionType,
    PaymentMethodUsage, InvoiceInstallment,
)
from model_mommy import mommy


class TestSalesOrder(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)
        self.product = mommy.make('catalogue.Product', cost=500, price=1000)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        transaction_date = datetime.now()
        sales_order = SalesOrder.objects.create(
            code='SalesOrderTest',
            transaction_date=transaction_date,
            global_discount_percentage=0)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            discount_percentage=0,
            sales_order=sales_order
        )
        self.assertEqual(
            sales_order.__str__(),
            "Sales Order SalesOrderTest - {}".format(transaction_date))

    def test_calculate_sale_cost(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_percentage=0)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            discount_percentage=0,
            sales_order=sales_order
        )
        self.assertEqual(
            sales_order.calculate_sale_cost(),
            self.product.price.amount * 1
        )

    def test_calculate_sale_cost_with_service_charge(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_percentage=0,
            gratuity=5)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            discount_percentage=0,
            sales_order=sales_order
        )
        self.assertEqual(
            sales_order.calculate_sale_cost(),
            self.product.price.amount * 1 + (
                self.product.price.amount * (5 / Decimal(100)))
        )

    def test_calculate_reverse_sub_total_if_tax_is_zero(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_percentage=0, tax=0)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            discount_percentage=0,
            sales_order=sales_order
        )
        self.assertEqual(
            sales_order.calculate_reverse_sub_total(),
            self.product.price.amount
        )

    def test_calculate_reverse_sub_total_if_tax_not_zero(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_percentage=0, tax=10)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            discount_percentage=0,
            sales_order=sales_order)
        self.assertEqual(
            sales_order.calculate_reverse_sub_total(),
            self.product.price.amount
        )

    def test_create_invoice(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        sales_order.create_invoice()
        self.assertTrue(
            sales_order.invoice is not None
        )

    def test_calculate_global_discount_price_with_percentage(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_percentage=50, mode=SalesOrder.MODE.percentage)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            sales_order=sales_order)
        self.assertEqual(
            sales_order.calculate_global_discount_price().amount,
            self.product.price.amount * 1 / Decimal(2)
        )

    def test_calculate_global_discount_price_with_fixed_price(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_fixed_price=900, mode=SalesOrder.MODE.fixed_price)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            sales_order=sales_order)
        self.assertEqual(
            sales_order.calculate_global_discount_price().amount,
            900
        )

    def test_sent_email(self):
        sales_order = SalesOrder.objects.create(
            code="TRX-0001",
            transaction_date=datetime.now(),
            global_discount_fixed_price=900, mode=SalesOrder.MODE.fixed_price)
        product_order = ProductOrder.objects.create(
            product=self.product,
            quantity=1,
            sales_order=sales_order)
        recipients = ['test@mail.ru']
        sales_order.sent_email(recipients=recipients)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject,
            'Transaction {} Receipt Detail.'.format(sales_order.code))

    @override_settings(AUTO_UPDATE_COST=False)
    def test_calculate_service_charge(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_percentage=0,
            gratuity=5,
            sub_total=Decimal(100000),
            tax=10)

        self.assertEqual(
            sales_order.calculate_service_charge().amount,
            100000 * (5 / Decimal(100))
        )

    @override_settings(AUTO_UPDATE_COST=False)
    def test_calculate_tax_with_service_charge(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            global_discount_percentage=0,
            gratuity=5,
            sub_total=Decimal(100000),
            tax=10)

        gratuity = 100000 * (5 / Decimal(100))
        expected_tax = (100000 + gratuity) * (10 / Decimal(100))

        self.assertEqual(
            sales_order.calculate_tax().amount,
            expected_tax
        )


class TestProductOrder(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)
        self.product = mommy.make(
            'catalogue.Product', account=self.account,
            sku='PRODUCT-TEST', cost=500, price=1000)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_percentage=0,
            quantity=2,
            sales_order=sales_order
        )
        self.assertEqual(
            product_order.__str__(),
            "Product Order PRODUCT-TEST | 2 | Rp2,000.00")

    def test_calculate_total_cost(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_percentage=0,
            quantity=2,
            sales_order=sales_order
        )
        self.assertEqual(
            product_order.calculate_total_cost().amount,
            self.product.price.amount * 2
        )

    def test_calculate_addon_cost(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_percentage=0,
            quantity=2,
            sales_order=sales_order
        )
        addon_detail = ProductOrderProductAddOnDetail.objects.create(
            product_order=product_order,
            product=self.product,
            base_cost=self.product.price,
            quantity=4,
            sub_total=self.product.price * 4,
        )
        self.assertEqual(
            product_order.calculate_addon_cost().amount,
            self.product.price.amount * 4
        )

    def test_calculate_discount_price_percentage_overwrite(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_percentage=25,
            behaviour=ProductOrder.BEHAVIOUR.overwrite,
            quantity=2,
            sales_order=sales_order
        )

        # DiscountPrice = (100 - 25) % of (2 * 1000) = 1500
        self.assertEqual(
            product_order.calculate_discount_price().amount,
            1500
        )

    def test_calculate_discount_price_percentage_reduce_by(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_percentage=25,
            behaviour=ProductOrder.BEHAVIOUR.reduced_by,
            quantity=2,
            sales_order=sales_order
        )

        # DiscountPrice = 25 % of (2 * 1000) = 500
        self.assertEqual(
            product_order.calculate_discount_price().amount,
            500
        )

    def test_calculate_discount_price_fixed_price_overwrite(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_fixed_price=250,
            behaviour=ProductOrder.BEHAVIOUR.overwrite,
            quantity=2,
            sales_order=sales_order
        )

        # DiscountPrice = (2 * 1000) - 250
        self.assertEqual(
            product_order.calculate_discount_price().amount,
            2000 - 250
        )

    def test_calculate_discount_price_fixed_price_reduce_by(self):
        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_fixed_price=250,
            behaviour=ProductOrder.BEHAVIOUR.reduced_by,
            quantity=2,
            sales_order=sales_order
        )

        # DiscountPrice = 250
        self.assertEqual(
            product_order.calculate_discount_price().amount,
            250
        )

    def test_save_method_update_sales_total_cost(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(), global_discount_percentage=0)
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_percentage=0,
            quantity=1,
            sales_order=sales_order
        )

        self.assertEqual(
            sales_order.total_cost.amount,
            self.product.price.amount * 1
        )

        # Add new ProductOrder Instance
        product_order.quantity = 2
        product_order.save()

        self.assertEqual(
            sales_order.total_cost.amount,
            self.product.price.amount * 2
        )

    def test_save_method_on_soft_delete_product(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(), global_discount_percentage=0)
        product_order = ProductOrder(
            product=self.product,
            discount_percentage=0,
            quantity=1,
            sales_order=sales_order
        )
        product_price = self.product.price.amount
        self.product.delete()
        product_order.save()

        self.assertEqual(
            sales_order.total_cost.amount,
            product_price * 1
        )

        # Add new ProductOrder Instance
        product_order.quantity = 2
        product_order.save()

        self.assertEqual(
            sales_order.total_cost.amount,
            product_price * 2
        )
        self.assertIsNotNone(self.product.deleted)

    def test_save_method_overide_product_cost_based_on_product(self):
        self.product.cost = 999
        self.product.save()

        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(), global_discount_percentage=0)
        product_order = ProductOrder(
            product=self.product,
            discount_percentage=0,
            product_cost=0,
            quantity=1,
            sales_order=sales_order
        )
        product_order.save()

        self.assertEqual(
            product_order.product_cost,
            self.product.cost
        )

    @override_settings(AUTO_UPDATE_COST=False)
    def test_settings_bypass_sales_total_update(self):
        sales_order = SalesOrder.objects.create(
            transaction_date=datetime.now(),
            total_cost=5000)
        product_order = ProductOrder.objects.create(
            product=self.product,
            discount_percentage=0,
            quantity=2,
            sales_order=sales_order
        )
        self.assertEqual(
            sales_order.total_cost.amount,
            5000
        )


class TestProductOrderCompositeDetail(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        product = mommy.make(
            'catalogue.Product', sku='PRODUCT-TEST', cost=500, price=1000)

        sales_order = SalesOrder.objects.create(transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=product,
            discount_percentage=0,
            quantity=2,
            sales_order=sales_order
        )
        product_order_composite_detail = ProductOrderCompositeDetail.objects.create(
            product_order=product_order,
            product=product,
            product_quantity=2)

        self.assertEqual(
            product_order_composite_detail.__str__(),
            "Composite Detail of {} | PRODUCT-TEST - 2".format(product_order.pk)
        )


class TestPaymentMethod(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        payment_method_cash = PaymentMethod(name='Cash')
        payment_method_card = PaymentMethod(name='Card', payment_sub='Debit', payment_item='BCA')
        self.assertEqual(
            payment_method_cash.__str__(), 'Cash')
        self.assertEqual(
            payment_method_card.__str__(), 'Debit BCA')

    def test_payment_name(self):
        payment_method_cash = PaymentMethod(name='Cash')
        payment_method_card = PaymentMethod(name='Card', payment_sub='Debit', payment_item='BCA')
        self.assertEqual(
            payment_method_cash.payment_name, 'Cash')
        self.assertEqual(
            payment_method_card.payment_name, 'Debit BCA')


class TestEnabledPaymentMethod(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        payment_method_cash = PaymentMethod.objects.create(name='Cash')
        payment_method_card = PaymentMethod.objects.create(name='Card', payment_sub='Debit', payment_item='BCA')
        enabled_payment_method = EnabledPaymentMethod.objects.create(account=self.account)
        PaymentMethodUsage.objects.create(
            enabled_payment_method=enabled_payment_method,
            payment_method=payment_method_card,
            account=self.account,
        )
        self.assertEqual(
            enabled_payment_method.__str__(), 'Enabled Payment Methods for Account-1 (1 item(s))')

    def test_payment_method_is_disabled(self):
        payment_method_cash = PaymentMethod.objects.create(name='Cash')
        payment_method_card = PaymentMethod.objects.create(name='Card', payment_sub='Debit', payment_item='BANK')

        set_current_tenant(None)
        account_1 = mommy.make('accounts.Account', name='Account-1')
        account_2 = mommy.make('accounts.Account', name='Account-2')

        set_current_tenant(account_1)
        enabled_payment_method = account_1.enabledpaymentmethod_set.first()
        PaymentMethodUsage.objects.create(
            enabled_payment_method=enabled_payment_method,
            payment_method=payment_method_card,
            account=account_1,
        )
        self.assertFalse(payment_method_card.is_disabled)

        set_current_tenant(account_2)
        self.assertTrue(payment_method_card.is_disabled)
        set_current_tenant(None)


class TestPaymentDetail(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        sales_order = SalesOrder.objects.create(
            code='SalesOrderTest', transaction_date=datetime.now())
        payment_method = PaymentMethod.objects.create(name='Cash')
        payment_detail = PaymentDetail(
            sales_order=sales_order,
            payment_method=payment_method,
            amount=10000,
            amount_used=10000)
        self.assertEqual(
            payment_detail.__str__(),
            "Payment Detail SalesOrderTest - Cash | Rp10,000.00")


class TestSalesOrderOption(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        sales_order_option = SalesOrderOption(name='take-out')
        self.assertEqual(sales_order_option.__str__(), 'Option take-out')


class TestSalesOrderInvoice(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        product = mommy.make('catalogue.Product', cost=500, price=1000)
        sales_order = SalesOrder.objects.create(
            code='SalesOrderTest', transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=product,
            quantity=1,
            sales_order=sales_order
        )
        invoice = SalesOrderInvoice.objects.create(sales_order=sales_order)

        self.assertEqual(invoice.__str__(), 'Invoice SalesOrderTest - {}'.format(invoice.date))


class TestInvoiceInstallments(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_balance(self):
        product = mommy.make('catalogue.Product', cost=500, price=1000)
        sales_order = SalesOrder.objects.create(
            code='SalesOrderTest', transaction_date=datetime.now())
        product_order = ProductOrder.objects.create(
            product=product,
            quantity=1,
            sales_order=sales_order
        )
        invoice = SalesOrderInvoice.objects.create(
            sales_order=sales_order, due_date=datetime(2022, 5, 17), date=datetime.now())

        period_text = "mothly"
        tenor = ((invoice.due_date.year - invoice.date.year) * 12) + (invoice.due_date.month - invoice.date.month)

        installment = InvoiceInstallment.objects.create(invoice=invoice, period=period_text, tenor_count=tenor)

        self.assertEqual(installment.tenor_count, 7)


class TestTransactionType(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        transaction_type = TransactionType.objects.create(
            transaction_type=TransactionType.TRANSACTION_TYPE.foc,
            name='FREE OF CHARGE')

        self.assertEqual(
            transaction_type.__str__(),
            "TransactionType foc | FREE OF CHARGE")


class TestCashierActivity(TestCase):

    def setUp(self):
        self.account = mommy.make(
            'accounts.Account', name='account-1')
        self.outlet = mommy.make(
            'outlet.Outlet',
            name="A", branch_id='A', account=self.account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        transaction_date = datetime.now()
        cashier_activity = CashierActivity.objects.create(
            activity_type='CART_VOID',
            date=transaction_date,
            description="VOID CART, STATUS FROM SUCCESS TO VOID, TOTAL : 122500.0, Reason : salah input",
            user="Device User",
            code="sales-code",
            outlet=self.outlet,
        )
        self.assertEqual(
            cashier_activity.__str__(),
            "Cashier CART_VOID, {} - {}".format(
                transaction_date, self.outlet))

    def test_display_reason_void(self):
        transaction_date = datetime.now()
        cashier_activity = CashierActivity.objects.create(
            activity_type='CART_VOID',
            date=transaction_date,
            description="VOID CART, STATUS FROM SUCCESS TO VOID, TOTAL : 122500.0, Reason : salah input",
            user="Device User",
            code="sales-code",
            outlet=self.outlet,
        )
        self.assertEqual(
            cashier_activity.display_reason(),
            "salah input")

    def test_display_reason_void_old_transaction(self):
        transaction_date = datetime.now()
        cashier_activity = CashierActivity.objects.create(
            activity_type='CART_VOID',
            date=transaction_date,
            description="VOID CART, STATUS FROM SUCCESS TO VOID, TOTAL : 122500.0",
            user="Device User",
            code="sales-code",
            outlet=self.outlet,
        )
        self.assertEqual(
            cashier_activity.display_reason(),
            "VOID CART, STATUS FROM SUCCESS TO VOID, TOTAL : 122500.0")

    def test_display_reason_non_void(self):
        transaction_date = datetime.now()
        cashier_activity = CashierActivity.objects.create(
            activity_type='ITEM_DELETE',
            date=transaction_date,
            description="DELETE ITEM, SWEET TEA -ICE-, QTY : 1, PRICE : 8182.0, DISC : 0.0, TOTAL : 8182.0",
            user="Device User",
            code="sales-code",
            outlet=self.outlet,
        )
        self.assertEqual(
            cashier_activity.display_reason(),
            "DELETE ITEM, SWEET TEA -ICE-, QTY : 1, PRICE : 8182.0, DISC : 0.0, TOTAL : 8182.0")
