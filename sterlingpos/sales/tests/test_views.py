import json
from datetime import datetime, timedelta

from django.test import override_settings, RequestFactory
from django.urls import reverse_lazy, reverse

from test_plus.test import TestCase

from allauth.account.adapter import get_adapter
from model_mommy import mommy
from sterlingpos.surcharges.models import _SURCHARGE_TYPE
from sterlingpos.catalogue.models import Category
from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.sales.models import SalesOrder, ProductOrder
from sterlingpos.sales.views import (
    SalesOrderListJson, SalesOrderListView,
)


class TestSalesOrderListJson(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

        account = mommy.make('accounts.Account', name='account-1')
        mommy.make('outlet.Outlet', account=account, name="A", branch_id='A')

        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        set_current_tenant(account)

        product = mommy.make('catalogue.Product', price=10000, account=account)
        payment_method = mommy.make('sales.PaymentMethod', name='CASH')

        self.sales_orders = mommy.make('sales.SalesOrder',
                                       tax=10,
                                       account=account,
                                       _quantity=5)
        for sales_order in self.sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        self.url = self.reverse('sales:sales_order_data')

    def tearDown(self):
        set_current_tenant(None)

    def test_view_is_not_accessible_by_anon(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.reverse('account_login'), response.url)

    def test_good_view(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)

    def test_get_initial_queryset(self):
        request = self.factory.get(self.url)
        request.user = self.user

        view = SalesOrderListJson()
        view.request = request

        queryset = view.get_initial_queryset()
        self.assertEqual(queryset.count(), 5)

    def test_get_initial_queryset_with_date(self):
        ten_days_before = datetime.now() - timedelta(days=10)
        sales_orders = mommy.make(
            'sales.SalesOrder',
            tax=10,
            transaction_date=ten_days_before,
            account=get_current_tenant(),
            _quantity=10)

        site_url = "{}?start_date={}&end_date={}".format(
            self.url,
            datetime.strftime(ten_days_before, "%Y-%m-%d %H:%M:%S"),
            datetime.strftime(ten_days_before + timedelta(days=1), "%Y-%m-%d %H:%M:%S"))

        request = self.factory.get(site_url)
        request.user = self.user

        view = SalesOrderListJson()
        view.request = request

        queryset = view.get_initial_queryset()
        self.assertEqual(queryset.count(), 10)

    def test_render_column(self):
        request = self.factory.get(self.url)
        request.user = self.user

        view = SalesOrderListJson()
        view.request = request

        payment_detail = view.render_column(self.sales_orders[0], 'payment_detail')
        sales_order_pk = view.render_column(self.sales_orders[0], 'pk')

        self.assertEqual(int(sales_order_pk), self.sales_orders[0].pk)
        self.assertEqual(payment_detail, ['CASH'])


class TestSalesOrderListView(TestCase):

    def setUp(self):
        payment_method_cash = mommy.make('sales.PaymentMethod', name='cash')
        payment_method_card = mommy.make('sales.PaymentMethod', name='card')

        account = mommy.make('accounts.Account', name='account-1')
        mommy.make('outlet.Outlet', account=account, name="A", branch_id='A')
        enabled_payment_method = account.enabledpaymentmethod_set.create(account=account)
        account.enabledpaymentmethod_set.first().payment_methods.add(payment_method_cash)
        account.enabledpaymentmethod_set.first().payment_methods.add(payment_method_card)
        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        set_current_tenant(account)

        self.url = self.reverse('sales:sales_order_list')

    def tearDown(self):
        set_current_tenant(None)

    def test_view_is_not_accessible_by_anon(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.reverse('account_login'), response.url)

    def test_good_view(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)

    def test_get_context_data(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)

        self.assertEqual(response.context['payment_types'].count(), 2)
        self.assertEqual(response.context['order_types'].count(), 7)


@override_settings(EXCLUDE_ZERO_SALES=False)
class TestSalesReportTemplateView(TestCase):

    def setUp(self):
        super(TestSalesReportTemplateView, self).setUp()
        payment_method_a = mommy.make('sales.PaymentMethod', name='CASH')
        payment_method_b = mommy.make('sales.PaymentMethod', name='CARD')

        account = mommy.make('accounts.Account', name='account-1')
        enabled_payment_method = account.enabledpaymentmethod_set.first()
        enabled_payment_method.payment_methods.add(payment_method_a)
        enabled_payment_method.payment_methods.add(payment_method_b)
        set_current_tenant(account)

        self.branch_a = mommy.make('outlet.Outlet', account=account, name="A", branch_id='A')
        self.branch_b = mommy.make('outlet.Outlet', account=account, name="B", branch_id='B')

        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        self.user.account = account
        account.owners.get_or_create(account=account, user=self.user)

        cat_a = Category.add_root(name="CATEGORY A", account=account)
        cat_b = Category.add_root(name="CATEGORY B", account=account)

        product_a = mommy.make('catalogue.Product', price=10000, account=account)
        product_b = mommy.make('catalogue.Product', price=10000, account=account)
        cat_a.product_set.add(product_a)
        cat_b.product_set.add(product_b)

        """
        15 Sales Orders
        = account Branch "A"
        + CASH PAYMENT
         - 5 w/o discount
         - 5 w/o discount /w (unsettled)

        + NO PAYMENT (FOC)
         - 5 w/o discount /w (FOC)

        = account Branch "B"
        + CARD PAYMENT
         - 2 w/ global_discount(50%)
         - 2 w/ product_discount(50%)
         - 1 w/ product_discount(50%) + global_discount(50%)
        """
        # - 5 w/o discount
        sales_orders = mommy.make('sales.SalesOrder',
                                  tax=10,
                                  outlet=self.branch_a,
                                  account=account,
                                  _quantity=5)
        for sales_order in sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_a,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_a,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 5 w/o discount w/ unsettled status
        sales_orders = mommy.make('sales.SalesOrder',
                                  status=SalesOrder.STATUS.unsettled,
                                  outlet=self.branch_a,
                                  tax=10,
                                  account=account,
                                  _quantity=5,)
        for sales_order in sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_a,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_a,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 5 w/o discount w/ FOC
        sales_orders = mommy.make('sales.SalesOrder',
                                  outlet=self.branch_a,
                                  tax=10,
                                  account=account,
                                  _quantity=5,)
        transaction_type = mommy.make('sales.TransactionType',
                                      transaction_type='foc',
                                      name='test-customer',
                                      account=account
                                      )
        for sales_order in sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_a,
                                       sales_order=sales_order,
                                       account=account
                                       )
            transaction_type.sales_order.add(sales_order)

        # - 2 w/ global_discount(50%)
        discounted_sales_orders = mommy.make('sales.SalesOrder',
                                             global_discount_percentage=50,
                                             mode=SalesOrder.MODE.percentage,
                                             tax=10,
                                             outlet=self.branch_b,
                                             account=account,
                                             _quantity=2)
        for sales_order in discounted_sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_b,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_b,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 2 w/ product_discount(50%)
        product_discount_sales_orders = mommy.make('sales.SalesOrder',
                                                   tax=10,
                                                   outlet=self.branch_b,
                                                   account=account,
                                                   _quantity=2)
        for sales_order in product_discount_sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       discount_percentage=50,
                                       mode=ProductOrder.MODE.percentage,
                                       behaviour=ProductOrder.BEHAVIOUR.reduced_by,
                                       product=product_b,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_b,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 1 w/ product_discount(50%) + global_discount(50%)
        product_and_global_discounted_sales_orders = mommy.make('sales.SalesOrder',
                                                                global_discount_percentage=50,
                                                                mode=SalesOrder.MODE.percentage,
                                                                tax=10,
                                                                outlet=self.branch_b,
                                                                account=account,
                                                                _quantity=1)
        for sales_order in product_and_global_discounted_sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       discount_percentage=50,
                                       mode=ProductOrder.MODE.percentage,
                                       behaviour=ProductOrder.BEHAVIOUR.reduced_by,
                                       product=product_b,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_b,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        sales_orders[0].__class__.objects.update(payment_method=payment_method_a)

        self.url = self.reverse('sales:sales_report')
        self.report_summary_url = self.reverse('sales:sales_summary')
        self.report_by_category_url = self.reverse('sales:sales_by_category')
        self.report_by_order_type_url = self.reverse('sales:sales_by_order_type')
        self.report_by_payment_url = self.reverse('sales:sales_by_payment')

    def tearDown(self):
        set_current_tenant(None)

    def test_view_is_not_accessible_by_anon(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.reverse('account_login'), response.url)

    def test_view_is_accessible_by_registered_user(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)

    def test_view_context_total_sales_before_tax(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 5 w/o discount            - sub_total = 50000
         - 5 w/o discount (FOC)      - sub_total = 50000
         - 2 w/ global_discount(50%) - sub_total = 20000 ( 2 * (10000))
         - 2 w/ product_discount(50%)- sub_total = 10000 ( 2 * (10000 * 0.5))
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - sub_total =  5000 ( 1 * (10000 * 0.5))
                                                 = 135000
        """
        self.assertEqual(response.context['total_sales_before_tax'], 150000)

    def test_view_context_total_sales_order_disc(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 5 w/o discount            - disc = 0     (50000 * 0)
         - 5 w/o discount (FOC)      - disc = 0     (50000 * 0)
         - 2 w/ global_discount(50%) - disc = 10000 (2 * (10000 * 0.5))
         - 2 w/ product_discount(50%)- disc = 0     (0 * (2 * (10000 * 0.5))
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - disc = 2500  (0.5 * (10000 * 0.5))
                                            = 12500
        """
        self.assertEqual(response.context['total_sales_order_disc'], 12500)

    def test_view_context_total_sales_order_disc_with_mode(self):
        self.login(email=self.user.email, password='test')
        # - 1 w/ global_discount(10000)
        account = self.user.account
        product = mommy.make('catalogue.Product', price=10000, account=account)
        payment_method = mommy.make('sales.PaymentMethod', name='CASH')
        fixed_discounted_sales_orders = mommy.make('sales.SalesOrder',
                                                   global_discount_fixed_price=10000,
                                                   mode=SalesOrder.MODE.fixed_price,
                                                   tax=10,
                                                   account=account,
                                                   _quantity=1)
        for sales_order in fixed_discounted_sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )
        response = self.assertGoodView(self.url)

        """
         - 5 w/o discount            - disc = 0     (50000 * 0)
         - 5 w/o discount (FOC)      - disc = 0     (50000 * 0)
         - 2 w/ global_discount(50%) - disc = 10000 (2 * (10000 * 0.5))
         - 1 w/ global_discount(10000) - disc = 10000

         - 2 w/ product_discount(50%)- disc = 0     (0 * (2 * (10000 * 0.5))
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - disc = 2500  (0.5 * (10000 * 0.5))
                                            = 22500
        """
        self.assertEqual(response.context['total_sales_order_disc'], 22500)

    def test_view_context_total_product_disc(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 5 w/o discount            - disc = 0     (50000 * 0)
         - 5 w/o discount (FOC)      - disc = 0     (50000 * 0)
         - 2 w/ global_discount(50%) - disc = 0     (2 * (10000 * 0.0))
         - 2 w/ product_discount(50%)- disc = 10000 (2 * (10000 * 0.5))
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - disc =  5000 (1 * (10000 * 0.5))
                                            = 15000
        """
        self.assertEqual(response.context['total_product_disc'], 15000)

    def test_view_context_total_tax(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 5 w/o discount            - tax = 5000 (1 * 50000) * 0.1
         - 5 w/o discount (FOC)      - tax = 5000 (1 * 50000) * 0.1
         - 2 w/ global_discount(50%) - tax = 1000 (2 * (10000 * 0.5)) * 0.1
         - 2 w/ product_discount(50%)- tax = 1000 (2 * (10000 * 0.5)) * 0.1
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - tax =  250 (1 * (10000 * 0.5) * 0.5 ) * 0.1
                                           = 12250
        """
        self.assertEqual(response.context['total_tax'], 12250)

    def test_view_context_total_sales_order_foc(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 5 w/o discount (FOC)      + tax = 55000 (50000 + (1 * 50000) * 0.1)
        """
        self.assertEqual(
            response.context['sales_orders'][0]["sales_order_foc"],
            55000)

    def test_view_context_total_product_disc_with_mode_and_behavior(self):
        self.login(email=self.user.email, password='test')
        """
         - 5 w/o discount            - disc = 0     (50000 * 0)
         - 5 w/o discount (FOC)      - disc = 0     (50000 * 0)
         - 2 w/ global_discount(50%) - disc = 0     (2 * (10000 * 0.0))
         - 2 w/ product_discount(50%)- disc = 10000 (2 * (10000 * 0.5))
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - disc =  5000 (1 * (10000 * 0.5))
                                            = 15000
         ADDITIONAL PRODUCT
         - 1 w/product_discount(2500)- disc =  2500
           mode = FIXED, behavior = REDUCED_BY
         - 1 w/product_discount(2500)- disc =  7500 (1 * (10000) - 2500)
           mode = FIXED, behavior = OVERWRITE
                                            = 25000
        """
        account = self.user.account
        product_a = mommy.make('catalogue.Product', price=10000, account=account)

        # - 1 w/ product_discount(2500) FIXED - REDUCED_BY
        sales_orders_with_fixed_reduced_product_discount = mommy.make('sales.SalesOrder',
                                                                      global_discount_percentage=0,
                                                                      tax=10,
                                                                      account=account)
        product_order_with_fixed_reduced_product_discount = mommy.make('sales.ProductOrder',
                                                                       quantity=1,
                                                                       discount_fixed_price=2500,
                                                                       mode=ProductOrder.MODE.fixed_amount,
                                                                       behaviour=ProductOrder.BEHAVIOUR.reduced_by,
                                                                       product=product_a,
                                                                       sales_order=sales_orders_with_fixed_reduced_product_discount,
                                                                       account=account
                                                                       )

        # - 1 w/ product_discount(2500) FIXED - OVERWRITE
        sales_orders_with_fixed_overwrite_product_discount = mommy.make('sales.SalesOrder',
                                                                        global_discount_percentage=0,
                                                                        tax=10,
                                                                        account=account)
        product_order_with_fixed_overwrite_product_discount = mommy.make('sales.ProductOrder',
                                                                         quantity=1,
                                                                         discount_fixed_price=2500,
                                                                         mode=ProductOrder.MODE.fixed_amount,
                                                                         behaviour=ProductOrder.BEHAVIOUR.overwrite,
                                                                         total_cost=2500,
                                                                         product=product_a,
                                                                         sales_order=sales_orders_with_fixed_overwrite_product_discount,
                                                                         account=account
                                                                         )

        response = self.assertGoodView(self.url)
        self.assertEqual(response.context['total_product_disc'], 25000)

    def test_view_context_total_sales(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 5 w/o discount            - total= 55000 (50000 + 50000 * 0.1)
         - 2 w/ global_discount(50%) - total= 11000 (2 * 10000 * 0.5) + tax
         - 2 w/ product_discount(50%)- total= 11000 (2 * 10000 * 0.5) + tax
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - total=  2750 (1 * (10000 * 0.5) * 0.5) + tax
                                            = 79750
        79750 = 135000(sbt) - (12500(sd) + 15000(pd) + 55000(foc)) + (12250(tt))
        """
        self.assertEqual(
            response.context['sales_orders'][0]["sales_total_sales"],
            79750)

    def test_view_context_sales_summary(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.report_summary_url)
        self.assertEqual(response.context['total_category_sales'], 150000)
        self.assertEqual(response.context['total_payment_type_net'], 79750)

        response = self.assertGoodView(
            self.reverse('sales:sales_summary', outlet_pk=self.branch_a.pk))
        self.assertEqual(response.context['total_category_sales'], 100000)

        response = self.assertGoodView(
            self.reverse('sales:sales_summary', outlet_pk=self.branch_b.pk))
        self.assertEqual(response.context['total_category_sales'], 50000)

        response = self.assertGoodView(
            self.reverse('sales:sales_summary', outlet_pk=self.branch_a.pk))
        self.assertEqual(response.context['total_payment_type_net'], 55000)
        self.assertEqual(response.context['total_payment_type_net'], response.context['total_sales_after_tax'])

        response = self.assertGoodView(
            self.reverse('sales:sales_summary', outlet_pk=self.branch_b.pk))
        self.assertEqual(response.context['total_payment_type_net'], 24750)
        self.assertEqual(response.context['total_payment_type_net'], response.context['total_sales_after_tax'])

    def test_view_context_category_sales(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.report_by_category_url)
        """
        CAT : CATEGORY_A
         - 5 w/o discount            - total= 50000
         - 5 w/o discount (FOC)      - total= 50000
                                            =100000
        CAT : CATEGORY_B
         - 2 w/ global_discount(50%) - total= 20000
         - 2 w/ product_discount(50%)- total= 20000
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - total= 10000
                                            = 50000
        """
        self.assertEqual(response.context['category_sales'].first().category_total_sales, 100000)
        self.assertEqual(response.context['category_sales'].last().category_total_sales, 50000)

    def test_view_context_payment_method_sales(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.report_by_payment_url)

        """
        payment_type : CASH
         - 5 w/o discount            - total= 55000 (50000 + 50000 * 0.1)
                                            = 55000
        payment_type : CARD
         - 2 w/ global_discount(50%) - total= 11000 (2 * 10000 * 0.5) + tax
         - 2 w/ product_discount(50%)- total= 11000 (2 * 10000 * 0.5) + tax
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - total=  2750 (1 * (10000 * 0.5) * 0.5) + tax
                                            = 24750
                                            = 79750
        """
        self.assertEqual(response.context['payment_type_net'].first().payment_net, 55000)
        self.assertEqual(response.context['payment_type_net'].last().payment_net, 24750)

    def test_view_context_total_sales_with_branch_filter(self):
        self.login(email=self.user.email, password='test')
        """
        TOTAL SALES
         = BRANCH "A"
         - 5 w/o discount            - total= 55000 (50000 + 50000 * 0.1)

         = BRANCH "B"
         - 2 w/ global_discount(50%) - total= 11000 (2 * 10000 * 0.5) + tax
         - 2 w/ product_discount(50%)- total= 11000 (2 * 10000 * 0.5) + tax
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - total=  2750 (1 * (10000 * 0.5) * 0.5) + tax
                                            = 24750
        """
        response = self.assertGoodView(
            self.reverse('sales:sales_summary', outlet_pk=self.branch_a.pk))
        self.assertEqual(response.context['total_sales_after_tax'], 55000)

        response = self.assertGoodView(
            self.reverse('sales:sales_summary', outlet_pk=self.branch_b.pk))
        self.assertEqual(response.context['total_sales_after_tax'], 24750)

        """
        CATEGORY_SALES
        = BRANCH "A"
        CAT : CATEGORY_A
         - 5 w/o discount            - total= 50000
         - 5 w/o discount (FOC)      - total= 50000
                                            =100000
        = BRANCH "B"
        CAT : CATEGORY_B
         - 2 w/ global_discount(50%) - total= 20000
         - 2 w/ product_discount(50%)- total= 20000
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - total= 10000
                                            = 50000
        """
        response = self.assertGoodView(
            self.reverse('sales:sales_by_category', outlet_pk=self.branch_a.pk))
        self.assertEqual(response.context['category_sales'].first().category_total_sales, 100000)
        self.assertEqual(response.context['category_sales'].last().category_total_sales, 0)
        # self.assertEqual(response.context['total_category_sales'], 100000)
        # self.assertEqual(response.context['total_category_sales'], response.context['total_sales_before_tax'])

        response = self.assertGoodView(
            self.reverse('sales:sales_by_category', outlet_pk=self.branch_b.pk))
        self.assertEqual(response.context['category_sales'].first().category_total_sales, 0)
        self.assertEqual(response.context['category_sales'].last().category_total_sales, 50000)
        # self.assertEqual(response.context['total_category_sales'], 50000)
        # self.assertEqual(response.context['total_category_sales'], response.context['total_sales_before_tax'])

        """
        PAYMENT_METHOD_COLLECTION
        = BRANCH "A"
        payment_type : CASH
         - 5 w/o discount            - total= 55000 (50000 + 50000 * 0.1)
                                            = 55000
        = BRANCH "B"
        payment_type : CARD
         - 2 w/ global_discount(50%) - total= 11000 (2 * 10000 * 0.5) + tax
         - 2 w/ product_discount(50%)- total= 11000 (2 * 10000 * 0.5) + tax
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - total=  2750 (1 * (10000 * 0.5) * 0.5) + tax
                                            = 24750
                                            = 79750
        """
        response = self.assertGoodView(
            self.reverse('sales:sales_by_payment', outlet_pk=self.branch_a.pk))
        self.assertEqual(response.context['payment_type_net'].first().payment_net, 55000)
        self.assertEqual(response.context['payment_type_net'].last().payment_net, 0)
        # self.assertEqual(response.context['total_payment_type_net'], 55000)
        # self.assertEqual(response.context['total_payment_type_net'], response.context['total_sales_after_tax'])

        response = self.assertGoodView(
            self.reverse('sales:sales_by_payment', outlet_pk=self.branch_b.pk))
        self.assertEqual(response.context['payment_type_net'].first().payment_net, 0)
        self.assertEqual(response.context['payment_type_net'].last().payment_net, 24750)
        # self.assertEqual(response.context['total_payment_type_net'], 24750)
        # self.assertEqual(response.context['total_payment_type_net'], response.context['total_sales_after_tax'])

    def test_view_context_total_sales_with_date_filter(self):
        self.login(email=self.user.email, password='test')
        ten_days_before = datetime.now() - timedelta(days=10)

        site_url = "{}?startdate={}&enddate={}".format(
            self.reverse('sales:sales_report', outlet_pk=self.branch_a.pk),
            datetime.strftime(ten_days_before, "%Y-%m-%d %H:%M:%S"),
            datetime.strftime(ten_days_before + timedelta(days=1), "%Y-%m-%d %H:%M:%S"))

        response = self.assertGoodView(site_url)
        self.assertEqual(response.context['total_sales'], 0)

        site_url = "{}?date={}".format(
            self.reverse('sales:sales_report', outlet_pk=self.branch_a.pk),
            'today')
        response = self.assertGoodView(site_url)
        self.assertNotEqual(response.context['total_sales'], 0)

        site_url = "{}?date={}".format(
            self.reverse('sales:sales_report', outlet_pk=self.branch_a.pk),
            'yesterday')
        response = self.assertGoodView(site_url)

        site_url = "{}?date={}".format(
            self.reverse('sales:sales_report', outlet_pk=self.branch_a.pk),
            'last_week')
        response = self.assertGoodView(site_url)

        site_url = "{}?date={}".format(
            self.reverse('sales:sales_report', outlet_pk=self.branch_a.pk),
            'last_month')
        response = self.assertGoodView(site_url)


@override_settings(EXCLUDE_ZERO_SALES=False)
class TestGrossReportTemplateView(TestCase):

    def setUp(self):
        super(TestGrossReportTemplateView, self).setUp()
        payment_method_a = mommy.make('sales.PaymentMethod', name='CASH')
        payment_method_b = mommy.make('sales.PaymentMethod', name='CARD')

        account = mommy.make('accounts.Account', name='account-1')
        enabled_payment_method = account.enabledpaymentmethod_set.first()
        enabled_payment_method.payment_methods.add(payment_method_a)
        enabled_payment_method.payment_methods.add(payment_method_b)
        set_current_tenant(account)

        self.branch_a = mommy.make('outlet.Outlet', account=account, name="A", branch_id='A')
        self.branch_b = mommy.make('outlet.Outlet', account=account, name="B", branch_id='B')

        self.user = mommy.make(
            'users.User', email='test@email.com', account=account)
        self.user.set_password('test')
        self.user.save()
        self.user.account = account
        account.owners.get_or_create(account=account, user=self.user)

        cat_a = Category.add_root(name="CATEGORY A", account=account)
        cat_b = Category.add_root(name="CATEGORY B", account=account)

        product_a = mommy.make('catalogue.Product', price=10000, cost=5000, account=account)
        product_b = mommy.make('catalogue.Product', price=10000, cost=5000, account=account)
        cat_a.product_set.add(product_a)
        cat_b.product_set.add(product_b)

        """
        8 Sales Orders
        = account Branch "A"
        + CASH PAYMENT
         - 2 w/o discount
         - 2 w/o discount /w (unsettled)

        + NO PAYMENT (FOC)
         - 1 w/o discount /w (FOC)

        = account Branch "B"
        + CARD PAYMENT
         - 2 w/ global_discount(50%)
         - 2 w/ product_discount(50%)
         - 1 w/ product_discount(50%) + global_discount(50%)
        """
        # - 2 w/o discount
        sales_orders = mommy.make('sales.SalesOrder',
                                  tax=10,
                                  outlet=self.branch_a,
                                  account=account,
                                  _quantity=2)
        for sales_order in sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_a,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_a,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 2 w/o discount w/ unsettled status
        sales_orders = mommy.make('sales.SalesOrder',
                                  status=SalesOrder.STATUS.unsettled,
                                  outlet=self.branch_a,
                                  tax=10,
                                  account=account,
                                  _quantity=2,)
        for sales_order in sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_a,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_a,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 1 w/o discount w/ FOC
        sales_orders = mommy.make('sales.SalesOrder',
                                  outlet=self.branch_a,
                                  tax=10,
                                  account=account,
                                  _quantity=1,)
        transaction_type = mommy.make('sales.TransactionType',
                                      transaction_type='foc',
                                      name='test-customer',
                                      account=account
                                      )
        for sales_order in sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_a,
                                       sales_order=sales_order,
                                       account=account
                                       )
            transaction_type.sales_order.add(sales_order)

        # - 2 w/ global_discount(50%)
        discounted_sales_orders = mommy.make('sales.SalesOrder',
                                             global_discount_percentage=50,
                                             mode=SalesOrder.MODE.percentage,
                                             tax=10,
                                             outlet=self.branch_b,
                                             account=account,
                                             _quantity=2)
        for sales_order in discounted_sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       product=product_b,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_b,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 2 w/ product_discount(50%)
        product_discount_sales_orders = mommy.make('sales.SalesOrder',
                                                   tax=10,
                                                   outlet=self.branch_b,
                                                   account=account,
                                                   _quantity=2)
        for sales_order in product_discount_sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       discount_percentage=50,
                                       mode=ProductOrder.MODE.percentage,
                                       behaviour=ProductOrder.BEHAVIOUR.reduced_by,
                                       product=product_b,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_b,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        # - 1 w/ product_discount(50%) + global_discount(50%)
        product_and_global_discounted_sales_orders = mommy.make('sales.SalesOrder',
                                                                global_discount_percentage=50,
                                                                mode=SalesOrder.MODE.percentage,
                                                                tax=10,
                                                                outlet=self.branch_b,
                                                                account=account,
                                                                _quantity=1)
        for sales_order in product_and_global_discounted_sales_orders:
            product_order = mommy.make('sales.ProductOrder',
                                       quantity=1,
                                       discount_percentage=50,
                                       mode=ProductOrder.MODE.percentage,
                                       behaviour=ProductOrder.BEHAVIOUR.reduced_by,
                                       product=product_b,
                                       sales_order=sales_order,
                                       account=account
                                       )
            payment_detail = sales_order.payment_detail.create(
                payment_method=payment_method_b,
                amount=sales_order.total_cost,
                amount_used=sales_order.total_cost,
                account=account
            )

        sales_orders[0].__class__.objects.update(payment_method=payment_method_a)

        self.url = self.reverse('sales:gross_profit')
        self.report_summary_url = self.reverse('sales:sales_summary')
        self.report_by_category_url = self.reverse('sales:sales_by_category')
        self.report_by_order_type_url = self.reverse('sales:sales_by_order_type')
        self.report_by_payment_url = self.reverse('sales:sales_by_payment')

    def tearDown(self):
        set_current_tenant(None)

    def test_view_is_not_accessible_by_anon(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.reverse('account_login'), response.url)

    def test_view_is_accessible_by_registered_user(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)

    def test_view_context_total_sales_before_tax(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 2 w/o discount            - sub_total = 20000
         - 1 w/o discount (FOC)      - sub_total = 10000
         - 2 w/ global_discount(50%) - sub_total = 20000 
         - 2 w/ product_discount(50%)- sub_total = 20000 
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - sub_total =  10000 
                                                 = 80000
        """
        self.assertEqual(response.context['total_sales_before_tax'], 80000)

    def test_view_context_total_sales_order_disc(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 2 w/o discount            - disc = 0     (20000 * 0)
         - 1 w/o discount (FOC)      - disc = 0     (10000 * 0)
         - 2 w/ global_discount(50%) - disc = 10000 (2 * (10000 * 0.5))
         - 2 w/ product_discount(50%)- disc = 0     (0 * (2 * (10000 * 0.5))
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - disc = 2500  (0.5 * (10000 * 0.5))
                                            = 12500
        """
        self.assertEqual(response.context['total_sales_order_disc'], 12500)

    def test_view_context_total_product_order_disc(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 2 w/o discount            - disc = 0     (20000 * 0)
         - 1 w/o discount (FOC)      - disc = 0     (10000 * 0)
         - 2 w/ global_discount(50%) - disc = 0     (2 * (10000 * 0.0))
         - 2 w/ product_discount(50%)- disc = 10000 (2 * (10000 * 0.5))
         - 1 w/ product_discount(50%) + global_discount(50%)
                                     - disc =  5000 (1 * (10000 * 0.5))
                                            = 15000
        """
        self.assertEqual(response.context['total_product_order_disc'], 15000)

    def test_view_context_total_sales_order_foc(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
         - 1 w/o discount (FOC)      + tax = 10000 ((1 * 10000) * 0.1)
        """
        self.assertEqual(response.context['total_sales_order_foc'], 11000)

    def test_view_context_total_sales_non_tax(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
            total_sales_before_tax          = 80000
            total_product_order_disc        = 15000
            total_sales_order_disc          = 12500
            total_sales_order_foc           = 11000
            --------------------------------------------
            total_sales_non_tax             = 41500
        """
        self.assertEqual(response.context['total_sales_non_tax'], 41500)

    def test_view_context_gross_profit(self):
        self.login(email=self.user.email, password='test')
        response = self.assertGoodView(self.url)
        """
            total_sales_non_tax             = 41500
            cost_of_goods_sold              = 40000 (8 * 5000)
            --------------------------------------------
            gross_profit                    = 1500
        """
        self.assertEqual(response.context['gross_profit'], 1500)


