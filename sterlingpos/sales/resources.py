import datetime
import pytz

from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import localtime
from django.core.exceptions import MultipleObjectsReturned

from import_export import resources
from import_export.fields import Field
from decimal import Decimal, InvalidOperation

from sterlingpos.core.import_export import (
    RequiredCharWidget,
    CustomDecimalWidget,
    LastSeenIterMixin,
)
from sterlingpos.core.models import get_current_tenant
from sterlingpos.sales.models import (
    SalesOrder,
    ProductOrder,
)
from sterlingpos.outlet.models import Outlet

 
class SalesOrderResource(
        resources.ModelResource, LastSeenIterMixin):

    payment_detail = Field(
        column_name="payment_detail",
    )

    class Meta:
        model = SalesOrder
        import_id_fields = ("code",)
        fields = [
            'code', 
            'transaction_date', 
            'status', 
            'outlet', 
            'payment_detail',
            'order_option',
            'sub_total', 
            'global_discount_price', 
            'total_cost',
            'guest_name'
        ]
        export_order = [
            'code', 
            'transaction_date', 
            'status', 
            'outlet', 
            'payment_detail',
            'order_option',
            'sub_total', 
            'global_discount_price', 
            'total_cost',
            'guest_name'
        ]
        exclude = ("account",)
        clean_model_instance = True

    def get_export_headers(self):
        csv_header = [
            "Code", 
            "Date", 
            "Status", 
            "Outlet", 
            "Payment Detail", 
            "Order Type",
            "Sub Total", 
            "Global Discount", 
            "Total Cost", 
            "Guest Name"]
        return csv_header

    def dehydrate_sub_total(self, obj):
        return obj.sub_total.amount

    def dehydrate_global_discount_price(self, obj):
        return obj.global_discount_price.amount

    def dehydrate_total_cost(self, obj):
        return obj.total_cost.amount

    def dehydrate_transaction_date(self, obj):
        local_time = localtime(obj.transaction_date)
        return local_time.strftime('%Y-%m-%d %H:%M:%S')

    def dehydrate_outlet(self, obj):
        return obj.outlet.name

    def dehydrate_order_option(self, obj):
        return obj.order_option.name

    def dehydrate_payment_detail(self, obj):
        list_payment_detail = [
            pd.payment_method.payment_name 
            for pd in obj.payment_detail.all()
        ]
        render_list = ', '.join(list_payment_detail)
        return render_list


class ProductOrderResource(
        resources.ModelResource, LastSeenIterMixin):

    code = Field(
        column_name="code",
    )
    date = Field(
        column_name="date",
    )
    outlet = Field(
        column_name="outlet",
    )
    product = Field(
        column_name="product",
    )
    variant = Field(
        column_name="variant",
    )

    addon_detail = Field(
        column_name="addon_detail",
    )

    modifier_detail = Field(
        column_name="modifier_detail",
    )

    class Meta:
        model = ProductOrder
        import_id_fields = ("code",)
        fields = (
            "code",
            "date",
            "outlet",
            "product",
            "variant",

            "quantity",
            "base_cost",
            "sub_total",
            "addon_cost",
            "discount_price",
            "total_cost",

            "addon_detail",
            "modifier_detail",
        )
        export_order = [
            "code",
            "date",
            "outlet",
            "product",
            "variant",

            "quantity",
            "base_cost",
            "sub_total",
            "addon_cost",
            "discount_price",
            "total_cost",

            "addon_detail",
            "modifier_detail",
        ]
        exclude = ("account",)
        clean_model_instance = True

    def get_export_headers(self):
        csv_header = [
            "Code",
            "Date",
            "Outlet",
            "Product",
            "Variant",

            "Quantity",
            "Base Cost",
            "Sub Total",
            "Addon Cost",
            "Discount",
            "Total Cost",

            "Addons",
            "Modifiers",
        ]
        return csv_header

    def dehydrate_code(self, obj):
        return obj.sales_order.code

    def dehydrate_date(self, obj):
        local_time = localtime(obj.sales_order.transaction_date)
        return local_time.strftime('%Y-%m-%d %H:%M:%S')

    def dehydrate_outlet(self, obj):
        return obj.sales_order.outlet.name

    def dehydrate_product(self, obj):
        return obj.product.catalogue.name

    def dehydrate_variant(self, obj):
        return obj.product.name

    def dehydrate_addon_detail(self, obj):
        list_instances = [
            "{} x{}".format(
                addon_detail.product.name,
                addon_detail.quantity)
            for addon_detail in obj.addon_detail.all()
        ]
        render_list = ', '.join(list_instances)
        return render_list

    def dehydrate_modifier_detail(self, obj):
        list_instances = [
            modifier.option.value
            for modifier in obj.modifier_detail.all()
        ]
        render_list = ', '.join(list_instances)
        return render_list

    def dehydrate_base_cost(self, obj):
        return obj.base_cost.amount

    def dehydrate_sub_total(self, obj):
        return obj.sub_total.amount

    def dehydrate_addon_cost(self, obj):
        return obj.addon_cost.amount

    def dehydrate_discount_price(self, obj):
        return obj.discount_price.amount

    def dehydrate_total_cost(self, obj):
        return obj.total_cost.amount
