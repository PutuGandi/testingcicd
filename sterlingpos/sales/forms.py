from django import forms

from django.utils.translation import ugettext_lazy as _, ugettext

from django.db.models import Q
from django.forms import inlineformset_factory
from django.core.exceptions import ValidationError
from django.template.loader import render_to_string

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, Fieldset, HTML
from dal import autocomplete
from djmoney.forms.fields import MoneyField
from moneyed.classes import Money
from sterlingpos.core.models import get_current_tenant

from sterlingpos.core.widgets import MoneyInputField
from sterlingpos.core.bootstrap import KawnFieldSet, KawnField, KawnFieldSetWithHelpText
from sterlingpos.surcharges.models import Surcharge
from sterlingpos.sales.models import (
    EnabledPaymentMethod,
    PaymentMethodUsage,
    SalesOrderOption,
    SalesReportSetting,
    _CUSTOMIZE_REPORT,
    CashierActivityReason,
    TransactionTypeReason,
    SalesOrder,
    PaymentDetail,
    SalesOrderInvoice,
)


class SalesOrderOptionForm(forms.ModelForm):
    class Meta:
        model = SalesOrderOption
        exclude = ("account",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText("", KawnField("name")),
        )

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)

        if not obj.surcharge_set.exists():
            _ = Surcharge.objects.get_or_create(order_option=obj, account=obj.account)

        return obj


class EnabledPaymentMethodForm(forms.ModelForm):

    enable_description = forms.BooleanField(required=False)

    enforce_voucher = forms.BooleanField(required=False)
    enforce_credit = forms.BooleanField(required=False)
    enforce_debit = forms.BooleanField(required=False)

    class Meta:
        model = EnabledPaymentMethod
        exclude = ("account",)
        widgets = {
            "payments": autocomplete.ModelSelect2Multiple(
                url="sales:all_payment_method_autocomplete",
                attrs={
                    "data-minimum-input-length": 0,
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super(EnabledPaymentMethodForm, self).__init__(*args, **kwargs)
        self.fields["payments"].label = _("Payment Methods")

        self.fields["enable_description"].label = _("Activate Payment Description")
        self.fields["enable_description"].help_text = _(
            "Enforce additional information from payment method. eg. card number, voucher number"
        )
        self.fields["enforce_debit"].label = _("Debit Card")
        self.fields["enforce_credit"].label = _("Credit Card")
        self.fields["enforce_voucher"].label = _("Voucher")

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-3"
        self.helper.field_class = "mb-3"
        self.helper.form_tag = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText("", KawnField("payments")),
            KawnFieldSetWithHelpText(
                "",
                KawnField(
                    "enable_description",
                    template="dashboard/table_management/switch_input/float_right_switch.html",
                ),
                Div(
                    HTML(
                        "<h5>{}</h5><hr/>".format(
                            _(
                                "Choose Payment Methods that requires additional information"
                            )
                        )
                    ),
                    css_class="col-lg-12 mt-3 p-0",
                    css_id="payment_method_header",
                ),
                Div(
                    KawnField(
                        "enforce_debit",
                        template="dashboard/table_management/switch_input/float_right_switch.html",
                    ),
                    KawnField(
                        "enforce_credit",
                        template="dashboard/table_management/switch_input/float_right_switch.html",
                    ),
                    KawnField(
                        "enforce_voucher",
                        template="dashboard/table_management/switch_input/float_right_switch.html",
                    ),
                    css_id="payment_method_header",
                ),
            ),
        )
        instance = kwargs.get("instance")

        if instance:
            self.fields["enable_description"].initial = instance.usages.filter(
                enforce_description=True
            ).exists()

            self.fields["enforce_voucher"].initial = instance.usages.filter(
                payment_method__name="Voucher", enforce_description=True
            ).exists()
            self.fields["enforce_credit"].initial = instance.usages.filter(
                payment_method__payment_sub="Credit", enforce_description=True
            ).exists()
            self.fields["enforce_debit"].initial = instance.usages.filter(
                payment_method__payment_sub="Debit", enforce_description=True
            ).exists()

    def update_m2m(
        self,
        instance,
        object_list,
        rel_field,
        intermediate_model,
        intermediate_model_ref,
    ):
        if len(object_list) != 0:
            obj_ids = set(object_list.values_list("id", flat=True))
            current_ids = set(rel_field.values_list("id", flat=True))
            add_ids = obj_ids - current_ids
            delete_ids = current_ids - obj_ids
            model_objects = [
                intermediate_model(
                    **{
                        "{}_id".format(rel_field.target_field_name): obj_id,
                        "{}".format(intermediate_model_ref.field.name): instance,
                        "account": instance.account,
                    }
                )
                for obj_id in add_ids
            ]
            intermediate_model.objects.bulk_create(model_objects)
            intermediate_model.objects.complex_filter(
                {
                    "{}__pk__in".format(rel_field.target_field_name): delete_ids,
                    "{}".format(intermediate_model_ref.field.name): instance,
                }
            ).delete()
        else:
            rel_field.clear()
        rel_field.invalidated_update()

    def save(self, commit=False):
        payments = self.cleaned_data.pop("payments")

        enable_description = self.cleaned_data.pop("enable_description")
        enforce_voucher = self.cleaned_data.pop("enforce_voucher")
        enforce_credit = self.cleaned_data.pop("enforce_credit")
        enforce_debit = self.cleaned_data.pop("enforce_debit")

        self.cleaned_data["payment_methods"] = payments

        instance = super().save()

        self.update_m2m(
            instance,
            payments,
            instance.payments,
            PaymentMethodUsage,
            PaymentMethodUsage.enabled_payment_method,
        )

        instance.usages.invalidated_update(enforce_description=False)

        if enable_description:
            query = Q()
            if enforce_voucher:
                query |= Q(payment_method__name="Voucher")
            if enforce_credit:
                query |= Q(payment_method__payment_sub="Credit")
            if enforce_debit:
                query |= Q(payment_method__payment_sub="Debit")

            if enforce_credit or enforce_debit or enforce_voucher:
                instance.usages.filter(query).invalidated_update(
                    enforce_description=True
                )
        return instance


class SalesReportSettingForm(forms.ModelForm):
    custom_report = forms.MultipleChoiceField(
        choices=_CUSTOMIZE_REPORT, widget=forms.CheckboxSelectMultiple, label=""
    )

    class Meta:
        model = SalesReportSetting
        fields = ("report_type", "custom_report")
        exclude = ("account",)

    def __init__(self, *args, **kwargs):
        super(SalesReportSettingForm, self).__init__(*args, **kwargs)
        self.fields["report_type"].label = ""
        self.fields["custom_report"].required = False
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSet(
                _("Tipe Resi Laporan"),
                KawnField(
                    "report_type",
                    css_class="custom-select",
                    template="dashboard/form_fields/report_setting_radio_button.html",
                ),
                css_class="col-12 p-0",
            ),
            KawnFieldSet(
                _("Custom Report Setting"),
                KawnField(
                    "custom_report",
                    template="dashboard/receipt/switch_input/multiple_visibility_switch.html",
                ),
                css_class="col-6 pl-0",
            ),
        )

    def save(self, *args, **kwargs):
        report_settings = super(SalesReportSettingForm, self).save(*args, **kwargs)
        return report_settings


class CashierActivityReasonForm(forms.ModelForm):
    class Meta:
        model = CashierActivityReason
        exclude = ("account",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText("", KawnField("name"), KawnField("description")),
        )


class TransactionTypeReasonForm(forms.ModelForm):
    class Meta:
        model = TransactionTypeReason
        exclude = ("account", "transaction_type")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText("", KawnField("description")),
        )


class PaymentDetailForm(forms.ModelForm):
    class Meta:
        model = PaymentDetail
        exclude = (
            "account",
            "payment_aggregator",
            "external_ref",
            "sales_order",
            "amount_used",
        )
        widgets = {
            "payment_method": autocomplete.ModelSelect2(
                url="sales:payment_method_autocomplete",
                attrs={
                    "data-minimum-input-length": 0,
                },
            ),
            "amount": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "amount_used": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):
        self.sales_order = kwargs.pop("sales_order")
        sales_order_context = {
            "sales_order": self.sales_order,
        }
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Payment Detail"),
                KawnField("payment_method", css_class="custom-select"),
                KawnField("amount"),
                KawnField("description"),
                help_text=render_to_string(
                    "help_text/invoice_detail.html", context=sales_order_context
                ),
            ),
        )

    def clean_amount(self):
        """
        Check wheter value is greater that balance
        """
        paid_amount = self.cleaned_data.get("amount")
        if self.sales_order.invoice.balance < paid_amount.amount:
            raise ValidationError(
                _("Paid amount cannot be greater than invoice balance."),
                code="invalid_paid_amount",
            )
        return paid_amount

    def save(self):
        self.instance.sales_order = self.sales_order
        self.instance.amount_used = self.cleaned_data.get("amount")
        return super().save()


class InstallmentPaymentDetailForm(forms.ModelForm):

    installment = forms.MultipleChoiceField(
        choices=(), widget=forms.CheckboxSelectMultiple
    )

    class Meta:
        model = PaymentDetail
        exclude = (
            "account",
            "payment_aggregator",
            "external_ref",
            "sales_order",
            "amount_used",
            "amount",
        )
        widgets = {
            "payment_method": autocomplete.ModelSelect2(
                url="sales:payment_method_autocomplete",
                attrs={
                    "data-minimum-input-length": 0,
                },
            ),
            "amount": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
            "amount_used": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):

        self.sales_order = kwargs.pop("sales_order")

        super().__init__(*args, **kwargs)
        # calculate installment balance left
        balance = (
            self.sales_order.total_cost.amount
            - self.sales_order.invoice.installment.balance_downpayment
        )
        tenor = (
            self.sales_order.invoice.installment.tenor_count
            - self.sales_order.invoice.installment.tenor_left
            + 1
        )
        balance_per_tenor = balance / self.sales_order.invoice.installment.tenor_count
        left_balance = round(balance_per_tenor)

        self.INSTALLMENT = ()
        installment_payment = 0
        for x in range(
            (
                self.sales_order.invoice.installment.tenor_count
                - self.sales_order.invoice.installment.tenor_left
                + 1
            ),
            (self.sales_order.invoice.installment.tenor_count + 1),
        ):
            if x == self.sales_order.invoice.installment.tenor_count:
                left_balance = self.sales_order.invoice.balance - installment_payment

            installment_payment += left_balance
            self.INSTALLMENT += (
                (
                    "%s-%s" % (x, round(left_balance)),
                    "Installment %s Rp. %s" % (x, round(left_balance)),
                ),
            )
        # end calculate
        # = forms.ChoiceField(choices=self.INSTALLMENT, widget=forms.CheckboxSelectMultiple)
        self.fields["installment"].choices = self.INSTALLMENT

        sales_order_context = {"sales_order": self.sales_order, "tenor": tenor}

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Payment Detail"),
                KawnField("installment"),
                KawnField("payment_method", css_class="custom-select"),
                KawnField("description"),
                help_text=render_to_string(
                    "help_text/invoice_installment_detail.html",
                    context=sales_order_context,
                ),
            ),
        )

    def clean_installment(self):
        installment = self.cleaned_data["installment"]
        return installment

    def clean_amount(self):
        """
        Check wheter value is greater that balance
        """
        paid_amount = self.cleaned_data.get("amount")
        if self.sales_order.invoice.balance < paid_amount.amount:
            raise ValidationError(
                _("Paid amount cannot be greater than invoice balance."),
                code="invalid_paid_amount",
            )
        return paid_amount

    def save(self):
        payment = super().save(commit=False)
        objs = []
        payment.sales_order = self.sales_order
        for item in self.cleaned_data.get("installment"):
            arr_item = item.split("-")
            amount = arr_item[1]
            objs.append(self.Meta.model(
                sales_order=self.sales_order,
                amount=Money(amount, "IDR"),
                amount_used=Money(amount, "IDR"),
                payment_method=self.cleaned_data.get("payment_method"),
                description=self.cleaned_data.get("description"),
                account=get_current_tenant()
            ))
        payment_detail = self.Meta.model.objects.bulk_create(objs)
        self.sales_order.invoice.settle_invoice()
        self.sales_order.invoice.update_sales_tendered_amount()

        return payment_detail


PaymentDetailFormSet = inlineformset_factory(
    SalesOrder,
    PaymentDetail,
    fk_name="sales_order",
    form=PaymentDetailForm,
    min_num=1,
    extra=0,
    can_delete=True,
)

InstallmentPaymentDetailFormSet = inlineformset_factory(
    SalesOrder,
    PaymentDetail,
    form=InstallmentPaymentDetailForm,
    min_num=1,
    extra=0,
    can_delete=True,
)
