from django.apps import AppConfig
from django.db.models.signals import post_save


class SalesConfig(AppConfig):
    name = "sterlingpos.sales"
    verbose_name = "Sales"

    def ready(self):
        from .signals import (
            update_sales_order_handler
        )
        post_save.connect(
            update_sales_order_handler, sender="sales.ProductOrder")
