from django.db import models
from django.db.models import (
    FilteredRelation,
    Subquery,
    Sum,
    Count,
    Q,
    F,
    IntegerField,
    DecimalField,
    CharField,
    Case,
    Value,
    When,
    Avg,
    Func,
    BooleanField,
    TimeField,
)
from django.db.models.sql.datastructures import Join
from django.db.models.fields.related import RelatedField
from django.db.models.functions import (
    Coalesce,
    Concat,
    TruncMonth,
    TruncDate,
    TruncDay,
    TruncHour,
    TruncMinute,
    ExtractWeekDay,
)
from django.db.models.sql.where import AND


from sterlingpos.core.models import (
    SterlingTenantQuerySet,
    SterlingSafeDeleteAllManager,
    SterlingTenantManager,
    get_current_tenant,
)

from django.apps import apps


class PaymentMethodSalesQuerySet(models.QuerySet):
    def payment_sales(
        self,
        start_date=None,
        end_date=None,
        outlet=None,
        exclude_zero_sales=False,
        **kwargs
    ):

        basic_filter = Q(pk__isnull=False)
        basic_filter &= ~Q(status="unsettled")
        outlet_qs = kwargs.get("outlet_qs", None)

        if outlet_qs:
            basic_filter &= Q(outlet__in=outlet_qs)

        if start_date:
            basic_filter &= Q(created__gte=start_date)
            basic_filter &= Q(transaction_date__gte=start_date)
        if end_date:
            basic_filter &= Q(transaction_date__lte=end_date)
        if outlet:
            basic_filter &= Q(outlet=outlet.pk)

        foc_filter = ~Q(
            payment_detail__sales_order__transaction_type__transaction_type="foc"
        )

        PaymentDetail = apps.get_model("sales.PaymentDetail")
        pd = (
            PaymentDetail.objects.select_related(
                "sales_order", "sales_order__transaction_type"
            )
            .annotate(
                transaction_type=F("sales_order__transaction_type__transaction_type"),
                transaction_date=F("sales_order__transaction_date"),
                outlet=F("sales_order__outlet"),
                status=F("sales_order__status"),
            )
            .filter(basic_filter)
        )

        self = self.annotate(
            payment_detail=FilteredRelation(
                "paymentdetail",
                condition=Q(paymentdetail__in=Subquery(pd.values_list("pk"))),
            ),
            payment_usage=Coalesce(
                Count("payment_detail", filter=foc_filter, output_field=IntegerField()),
                Value(0),
            ),
            payment_net=Coalesce(
                Sum(
                    "payment_detail__amount_used",
                    filter=foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
        )

        if exclude_zero_sales:
            self = self.exclude(payment_usage=0)

        return self


class ProductSalesQuerySet(SterlingTenantQuerySet):

    def product_sales(
        self,
        start_date=None,
        end_date=None,
        outlet=None,
        exclude_zero_sales=False,
        **kwargs
    ):

        basic_filter = Q(pk__isnull=False)
        basic_filter &= ~Q(status="unsettled")
        outlet_qs = kwargs.get("outlet_qs", None)

        if outlet_qs:
            basic_filter &= Q(outlet__in=outlet_qs)

        if start_date:
            basic_filter &= Q(transaction_date__gte=start_date)
            basic_filter &= Q(created__gte=start_date)
        if end_date:
            basic_filter &= Q(transaction_date__lte=end_date)
        if outlet:
            basic_filter &= Q(outlet=outlet.pk)

        foc_filter = ~Q(
            product_order__sales_order__transaction_type__transaction_type="foc"
        )

        ProductOrder = apps.get_model("sales.ProductOrder")
        po = (
            ProductOrder.objects.select_related(
                "sales_order", "sales_order__transaction_type"
            )
            .annotate(
                transaction_type=F("sales_order__transaction_type__transaction_type"),
                transaction_date=F("sales_order__transaction_date"),
                outlet=F("sales_order__outlet"),
                status=F("sales_order__status"),
            )
            .filter(basic_filter)
        )

        self = self.annotate(
            product_order=FilteredRelation(
                "productorder",
                condition=Q(productorder__in=Subquery(po.values_list("pk"))),
            ),
            product_sales_count=Coalesce(
                Sum("product_order__quantity", output_field=IntegerField()), Value(0)
            ),
            product_sales_non_foc_count=Coalesce(
                Sum(
                    "product_order__quantity",
                    filter=foc_filter,
                    output_field=IntegerField(),
                ),
                Value(0),
            ),
            product_sales_net=Coalesce(
                Sum(
                    "product_order__total_cost",
                    filter=foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            product_sales_discount=Coalesce(
                Sum(
                    (F("product_order__quantity") * F("product_order__base_cost"))
                    + F("product_order__surcharge_price")
                    + F("product_order__addon_cost")
                    - F("product_order__total_cost"),
                    filter=foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            product_sales_surcharge=Coalesce(
                Sum(
                    "product_order__surcharge_price",
                    filter=foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            product_sales_addon_cost=Coalesce(
                Sum(
                    "product_order__addon_cost",
                    filter=foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            product_total_sales=Coalesce(
                Sum(
                    F("product_order__quantity") * F("product_order__base_cost"),
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            product_sales_foc_count=Coalesce(
                Sum(
                    "product_order__quantity",
                    filter=~foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            product_sales_foc=Coalesce(
                Sum(
                    "product_order__total_cost",
                    filter=~foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
        )
        
        self = self.annotate(
            is_excluded=Case(
                When(
                    Q(deleted__isnull=False) & Q(product_sales_count=0),
                    then=True
                ),
                default=False,
                output_field=BooleanField(),
            ),
        ).exclude(is_excluded=True)

        if exclude_zero_sales:
            self = self.exclude(product_sales_count=0)
        self.query.group_by = ["name"]

        return self

    def addon_sales(
        self,
        start_date=None,
        end_date=None,
        outlet=None,
        exclude_zero_sales=False,
        **kwargs
    ):

        self = self.filter(Q(classification="Complementary"))
        basic_filter = Q(pk__isnull=False)
        outlet_qs = kwargs.get("outlet_qs", None)

        if outlet_qs:
            basic_filter &= Q(
                outlet__in=outlet_qs
            )

        if start_date:
            basic_filter &= Q(
                transaction_date__gte=start_date
            )
        if end_date:
            basic_filter &= Q(
                transaction_date__lte=end_date
            )
        if outlet:
            basic_filter &= Q(
                outlet=outlet.pk
            )

        canceled_bill_filter = ~Q(
            status="unsettled"
        )

        ProductOrderProductAddonDetail = apps.get_model("sales.ProductOrderProductAddonDetail")
        po = (
            ProductOrderProductAddonDetail.objects.select_related(
                "product_order__sales_order",
            )
            .annotate(
                transaction_date=F("product_order__sales_order__transaction_date"),
                outlet=F("product_order__sales_order__outlet"),
                status=F("product_order__sales_order__status"),
            )
            .filter(
                basic_filter & canceled_bill_filter
            )
        )

        self = self.annotate(
            po_addon_detail=FilteredRelation(
                "productorderproductaddondetail",
                condition=Q(
                    productorderproductaddondetail__in=Subquery(
                        po.values_list("pk"))),
            ),
            addon_usage_count=Coalesce(
                Sum(
                    "po_addon_detail__quantity",
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            addon_usage_sales=Coalesce(
                Sum(
                    "po_addon_detail__sub_total",
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
        )

        self = self.annotate(
            is_excluded=Case(
                When(
                    Q(deleted__isnull=False) & Q(addon_usage_count=0),
                    then=True
                ),
                default=False,
                output_field=BooleanField(),
            ),
        ).exclude(is_excluded=True)

        if exclude_zero_sales:
            self = self.exclude(addon_usage_count=0)
        self.query.group_by = ["name"]

        return self


class CategorySalesQuerySet(SterlingTenantQuerySet):
    def category_sales(
        self,
        start_date=None,
        end_date=None,
        outlet=None,
        exclude_zero_sales=False,
        **kwargs
    ):

        basic_filter = Q(pk__isnull=False)
        basic_filter &= ~Q(status="unsettled")
        outlet_qs = kwargs.get("outlet_qs", None)

        if outlet_qs:
            basic_filter &= Q(outlet__in=outlet_qs)

        if start_date:
            basic_filter &= Q(transaction_date__gte=start_date)
            basic_filter &= Q(created__gte=start_date)
        if end_date:
            basic_filter &= Q(transaction_date__lte=end_date)
        if outlet:
            basic_filter &= Q(outlet=outlet.pk)

        foc_filter = ~Q(
            product_order__sales_order__transaction_type__transaction_type="foc"
        )

        ProductOrder = apps.get_model("sales.ProductOrder")
        po = (
            ProductOrder.objects.select_related(
                "sales_order", "sales_order__transaction_type"
            )
            .annotate(
                transaction_type=F("sales_order__transaction_type__transaction_type"),
                transaction_date=F("sales_order__transaction_date"),
                outlet=F("sales_order__outlet"),
                status=F("sales_order__status"),
            )
            .filter(basic_filter)
        )

        self = self.annotate(
            product_order=FilteredRelation(
                "productorder",
                condition=Q(productorder__in=Subquery(po.values_list("pk"))),
            ),
            category_sales_count=Coalesce(
                Sum("product_order__quantity", output_field=IntegerField()), Value(0)
            ),
            category_total_sales=Coalesce(
                Sum(
                    F("product_order__quantity") * F("product_order__base_cost") + F("product_order__addon_cost"),
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            category_sales_net=Coalesce(
                Sum(
                    "product_order__total_cost",
                    filter=foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            category_sales_discount=Coalesce(
                Sum(
                    "product_order__discount_price",
                    filter=foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
            category_sales_foc=Coalesce(
                Sum(
                    "product_order__total_cost",
                    filter=~foc_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
        )

        self = self.annotate(
            is_excluded=Case(
                When(
                    Q(deleted__isnull=False) & Q(category_sales_count=0),
                    then=True
                ),
                default=False,
                output_field=BooleanField(),
            ),
        ).exclude(is_excluded=True)

        if exclude_zero_sales:
            self = self.exclude(category_sales_count=0)

        self.query.group_by = ["name"]
        return self


class SalesOrderOptionQuerySet(SterlingTenantQuerySet):
    def order_type_sales(
        self,
        start_date=None,
        end_date=None,
        outlet=None,
        exclude_zero_sales=False,
        **kwargs
    ):

        basic_filter = Q(pk__isnull=False)
        outlet_qs = kwargs.get("outlet_qs", None)

        if outlet_qs:
            basic_filter &= Q(salesorder__outlet__in=outlet_qs)

        if start_date:
            basic_filter &= Q(salesorder__transaction_date__gte=start_date)
        if end_date:
            basic_filter &= Q(salesorder__transaction_date__lte=end_date)
        if outlet:
            basic_filter &= Q(salesorder__outlet=outlet)

        foc_filter = ~Q(sales_order__transaction_type__transaction_type="foc")
        canceled_bill_filter = ~Q(sales_order__status="unsettled")

        self = self.annotate(
            sales_order=FilteredRelation(
                "salesorder",
                condition=basic_filter,
            ),
            order_type_sales_count=Coalesce(
                Count(
                    "sales_order",
                    filter=canceled_bill_filter,
                    output_field=IntegerField(),
                ),
                Value(0),
            ),
            order_type_sales_net=Coalesce(
                Sum(
                    "sales_order__total_cost",
                    filter=foc_filter & canceled_bill_filter,
                    output_field=DecimalField(),
                ),
                Value(0),
            ),
        )

        self.query.group_by = ["name"]
        return self


class SalesOrderOptionTenantManager(
    SalesOrderOptionQuerySet.as_manager().__class__, SterlingSafeDeleteAllManager
):
    pass


class PaymentMethodTenantManager(PaymentMethodSalesQuerySet.as_manager().__class__):
    pass


class ProductSalesTenantManager(
    ProductSalesQuerySet.as_manager().__class__, SterlingSafeDeleteAllManager
):
    pass


class CategorySalesTenantManager(
    CategorySalesQuerySet.as_manager().__class__, SterlingSafeDeleteAllManager
):
    pass


class Round(Func):
    function = "ROUND"
    arity = 2


class SalesOrderManager(SterlingTenantManager, models.Manager):

    # TODO: Remove this once we update django-multitenancy
    def get_queryset(self):
        queryset = super(SalesOrderManager, self).get_queryset()
        current_tenant = get_current_tenant()
        if current_tenant:
            return queryset.filter(account=current_tenant)
        return queryset

    def get_summary_qs(self, start_date, end_date, outlets=None):
        qs = self.get_queryset()
        qs = qs.filter(
            ~Q(status="unsettled"),
            transaction_date__gte=start_date,
            transaction_date__lte=end_date,
        )

        if outlets:
            qs = qs.filter(outlet__in=outlets)

        return qs

    def get_sales_summary(self, start_date, end_date, outlets=None):
        qs = self.get_summary_qs(start_date, end_date, outlets)
        qs = qs.aggregate(
            count=Count("*"),
            amount=Coalesce(Sum("total_cost", output_field=DecimalField()), 0),
            average=Round(
                Coalesce(Avg("total_cost", output_field=DecimalField()), 0), 2
            ),
        )

        return qs

    def get_sales_summary_grouped_by_time(self, start_date, end_date, trunc_by, outlets=None):
        qs = self.get_summary_qs(start_date, end_date, outlets)

        time_field = "transaction_date"
        if trunc_by == "month":
            qs = qs.annotate(time_unit=TruncMonth(time_field))
        elif trunc_by == "date":
            qs = qs.annotate(time_unit=TruncDate(time_field))
        elif trunc_by == "hour":
            qs = qs.annotate(time_unit=TruncHour(time_field))
        elif trunc_by == "minute":
            qs = qs.annotate(time_unit=TruncMinute(time_field))
        else:
            raise ValueError("Unsupported trunc segment")

        qs = (
            qs.values("time_unit")
            .annotate(
                count=Coalesce(Count("transaction_date"), 0),
                amount=Coalesce(Sum("total_cost", output_field=DecimalField()), 0),
            )
            .order_by("time_unit")
        )

        return qs

    def _generate_truncate_query(self, field_name, trunc_by):
        truncate = {
            "month": TruncMonth,
            "date": TruncDate,
            "days": TruncDay,
            "hours": TruncHour,
        }
        truncate_class = truncate.get(trunc_by, TruncDate)

        if trunc_by == "hours":
            truncate_kwargs = {
                "trunc_hour": truncate_class(field_name),
                "time_unit": TruncHour(
                    "trunc_hour", output_field=TimeField()),
            }
        elif trunc_by == "days":
            truncate_kwargs = {
                "trunc_day": truncate_class(field_name),
                "time_unit": ExtractWeekDay("trunc_day")
            }
        else:
            truncate_kwargs = {
                "time_unit": truncate_class(field_name)
            }

        return truncate_kwargs

    def get_summary_grouped_by(self, start_date, end_date, trunc_by="date", outlets=None):
        qs = self.get_summary_qs(start_date, end_date, outlets)
        time_field = "transaction_date"

        truncate_kwargs = self._generate_truncate_query(
            time_field, trunc_by)
        qs = qs.annotate(**truncate_kwargs)
        
        return qs


class ProductOrderManager(SterlingTenantManager, models.Manager):

    # TODO: Remove this once we update django-multitenancy
    def get_queryset(self):
        queryset = super(ProductOrderManager, self).get_queryset()
        current_tenant = get_current_tenant()
        if current_tenant:
            return queryset.filter(account=current_tenant)
        return queryset

    def get_summary_qs(self, start_date, end_date, outlets=None):
        qs = self.get_queryset()
        qs = qs.prefetch_related(
            "sales_order", "sales_order__transaction_type", "product", "category"
        ).filter(
            ~Q(sales_order__status="unsettled"),
            sales_order__transaction_date__gte=start_date,
            sales_order__transaction_date__lte=end_date,
        )

        if outlets:
            qs = qs.filter(sales_order__outlet__in=outlets)

        return qs

    def get_sales_summary(self, start_date, end_date, outlets=None):
        qs = self.get_summary_qs(start_date, end_date, outlets)

        # Special case where we ignore FOC
        qs = qs.exclude(sales_order__transaction_type__transaction_type="foc")

        qs = qs.values("product").annotate(
            name=Case(
                When(
                    ~Q(product__catalogue__name__in=F("product__name")),
                    then=Concat(
                        'product__catalogue__name',
                        Value(' - '),
                        'product__name',
                    ),
                ),
                default=F("product__name"),
                output_field=CharField(),
            ),
            count=Sum("quantity"),
            amount=Coalesce(
                Sum(F("quantity") * F("base_cost"), output_field=DecimalField()), 0
            ),
        )

        return qs

    def get_sales_summary_by_category(self, start_date, end_date, outlets=None):
        qs = self.get_summary_qs(start_date, end_date, outlets)

        # Special case where we ignore FOC
        qs = qs.exclude(sales_order__transaction_type__transaction_type="foc")

        qs = qs.values("category").annotate(
            name=F("category__name"),
            count=Sum("quantity"),
            amount=Coalesce(
                Sum(F("quantity") * F("base_cost"), output_field=DecimalField()), 0
            ),
        )

        return qs
