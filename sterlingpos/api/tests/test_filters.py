from datetime import datetime

from django.urls import reverse

from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.outlet.models import Outlet


class OutletModifiedFilter(ModifiedFilter):

    class Meta:
        model = Outlet
        exclude = ["outlet_image",]


class ModifiedFilterTests(TestCase):

    def test_filter_deleted(self):
        account = mommy.make('accounts.Account', name='Test Account')
        outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1', account=account)
        
        outlet_2 = mommy.make(
            'outlet.Outlet', branch_id='branch-2', account=account)
        outlet_2.delete()

        queryset = account.outlet_set

        modified_filter = OutletModifiedFilter()

        filter_deleted_qs = modified_filter.filter_deleted(
            queryset, "deleted", outlet_2.deleted
        )
        self.assertTrue(
            filter_deleted_qs.filter(pk=outlet_2.pk).exists())
        self.assertFalse(
            filter_deleted_qs.filter(pk=outlet.pk).exists())
