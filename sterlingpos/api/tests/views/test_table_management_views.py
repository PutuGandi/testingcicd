import time

from datetime import timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy
from django.test import override_settings

from model_mommy import mommy
from rest_framework.test import APILiveServerTestCase

from sterlingpos.core.models import (
    get_current_tenant, set_current_tenant
)

class TestTableBulkCreateApi(APILiveServerTestCase):

    url_string = "v2:table_bulk_create"

    def setUp(self):
        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.table_area = mommy.make(
            "table_management.TableArea", name="Area 2")

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(self.url_string)

        self.obj_dummy = [
            {
                "name": "table-8",
                "pax": 1,
                "shape": "square",
                "suffix": "TBL",
                "table_area": self.table_area.pk,
            },
            {
                "name": "table-9",
                "pax": 5,
                "shape": "rectangle",
                "suffix": "TBL",
                "table_area": self.table_area.pk,
            }]

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_update_object(self):
        res = self.client.post(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_create_object(self):
        self.client.login(email=self.user_2.email, password="password")

        res = self.client.post(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 201,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

        self.assertTrue(isinstance(res.data, list), res.data)
        self.assertEqual(len(res.data), 2)
        self.assertEqual(res.data[0].get('name'), 'table-8', res.data)
