import datetime

from datetime import timedelta

from django.urls import reverse, reverse_lazy
from django.test import override_settings

from model_mommy import mommy
from rest_framework.test import APILiveServerTestCase

from sterlingpos.core.models import (
    get_current_tenant, set_current_tenant
)


class TestPromoListAPI(APILiveServerTestCase):
    url = reverse('v2:promo_list')

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.obj = mommy.make(
            'promo.Promo',
            start_date=datetime.date.today(),
            end_date=datetime.date.today() + timedelta(days=10),
            account=self.account_1
        )
        self.promoreward = mommy.make('promo.PromoReward', promo=self.obj, account=self.account_1)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = mommy.make(
            'promo.Promo',
            start_date=datetime.date.today(),
            end_date=datetime.date.today() + timedelta(days=10),
            account=self.account_2
        )
        self.promoreward1 = mommy.make('promo.PromoReward', promo=self.obj2, account=self.account_2)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_uom_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_category_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        names = map(lambda obj: obj.get('name'), res.data.get('results'))
        self.assertIn(self.obj.name, names, res.data)
        self.assertNotIn(self.obj2.name, names, res.data)


class TestPromoCreateAPI(APILiveServerTestCase):
    url = reverse('v2:promo_create')

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()
        self.outlet_1 = mommy.make(
            'outlet.Outlet', name='outlet-1', branch_id=1,
            account=self.account_1)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', name='outlet-2', branch_id=2,
            account=self.account_1)

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.data = {
            "promo_reward": {
                "discount_type": "percentage",
                "discount_percentage": "20.00",
            },
            "promo_type": "discount",
            "name": "Promo Test",
            "description": "",
            "is_active": "true",
            "start_date": "2018-10-23",
            "end_date": "2018-10-23",
            "start_hours": "00:00:00",
            "end_hours": "23:00:00",
            "allow_days": [
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7"
            ],
            "allow_multiple": "false",
            "outlet": [
                self.outlet_1.pk,
                self.outlet_2.pk
            ]
        }

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.post(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_uom_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_category_of_other_company(self):
        self.client.login(email=self.user3.email, password='password')
        res = self.client.post(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 400,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestPromoUpdateAPI(APILiveServerTestCase):

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.obj = mommy.make(
            'promo.Promo',
            start_date=datetime.date.today(),
            end_date=datetime.date.today() + timedelta(days=10),
            account=self.account_1
        )
        self.promoreward = mommy.make('promo.PromoReward', promo=self.obj, account=self.account_1)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = mommy.make(
            'promo.Promo',
            start_date=datetime.date.today(),
            end_date=datetime.date.today() + timedelta(days=10),
            account=self.account_2
        )
        self.promoreward1 = mommy.make('promo.PromoReward', promo=self.obj2, account=self.account_2)

        set_current_tenant(None)
        self.data = {
            "promo_reward": {
                "discount_type": "percentage",
                "discount_percentage": "50.00",
            },
            "promo_type": "discount",
            "name": "Promo Test (UPDATED)",
            "allow_days": [
                "5",
                "6",
                "7"
            ],
        }
        self.url = reverse(
            "v2:promo_update", kwargs={"pk": self.obj.pk})

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.patch(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_api(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.patch(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_object_of_other_company(self):
        self.client.login(email=self.user3.email, password='password')
        res = self.client.patch(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestPromoDeleteAPI(APILiveServerTestCase):

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.obj = mommy.make(
            'promo.Promo',
            start_date=datetime.date.today(),
            end_date=datetime.date.today() + timedelta(days=10),
            account=self.account_1
        )
        self.promoreward = mommy.make('promo.PromoReward', promo=self.obj, account=self.account_1)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = mommy.make(
            'promo.Promo',
            start_date=datetime.date.today(),
            end_date=datetime.date.today() + timedelta(days=10),
            account=self.account_2
        )
        self.promoreward1 = mommy.make('promo.PromoReward', promo=self.obj2, account=self.account_2)
        self.url = reverse(
            "v2:promo_delete", kwargs={"pk": self.obj.pk})
        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.delete(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_api(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.delete(self.url, format='json')
        self.assertEqual(
            res.status_code, 204,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_object_of_other_company(self):
        self.client.login(email=self.user3.email, password='password')
        res = self.client.delete(self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
