import factory
import time

from datetime import date, datetime, timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy
from django.test.utils import override_settings

from model_mommy import mommy

from rest_framework.test import APILiveServerTestCase

from sterlingpos.core.models import (
    get_current_tenant, set_current_tenant
)


class TestShopeePayAccountDetailAPI(APILiveServerTestCase):
    url = reverse('v2.1:shopeepay_detail')

    def setUp(self):
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.emailaddress_set.create(
            email=self.user.email, verified=True, primary=True)
        self.user.set_password('password')
        self.user.save()

        self.account_1.owners.create(user=self.user, account=self.account_1)

        digital_payment_settings = mommy.make(
            "digital_payment.DigitalPaymentSettings",
            status="completed",
            is_active=True,
            active_payment=["shopeepay"],
            account=self.account_1,
        )

        shopeepay_settings = mommy.make(
            "shopeepay.ShopeePaySettings",
            merchant_ext_id="MERCHANT_EXT_ID",
            account=self.account_1,
        )

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.emailaddress_set.create(
            email=self.user3.email, verified=True, primary=True)
        self.user3.set_password('password')
        self.user3.save()

        self.account_2.owners.create(user=self.user3, account=self.account_2)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_view_with_active_digital_payment_and_registered_shopeepay_account(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')

        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('merchant_ext_id', res.data, 'Bad data (%s)' % (res.data))
        self.assertIn('outlets', res.data, 'Bad data (%s)' % (res.data))

    def test_view_with_inactive_digital_payment(self):
        self.client.login(email=self.user3.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 403,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_view_with_active_digital_payment_but_unregistered_shopeepay_payment(self):
        digital_payment_settings = mommy.make(
            "digital_payment.DigitalPaymentSettings",
            status="completed",
            is_active=True,
            active_payment=["shopeepay"],
            account=self.account_2,
        )

        self.client.login(email=self.user3.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
