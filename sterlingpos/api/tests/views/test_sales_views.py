import pytz
import json
import urllib
import copy

from datetime import datetime, timedelta
from django.conf import settings
from django.core import mail
from django.utils import timezone
from django.urls import reverse, reverse_lazy
from django.test import override_settings

from model_mommy import mommy
from rest_framework.test import APITestCase, APILiveServerTestCase

from sterlingpos.core.models import get_current_tenant, set_current_tenant
from sterlingpos.catalogue.models import Product, Category
from sterlingpos.sales.models import (
    SalesOrder,
    SalesOrderOption,
    ProductOrder,
    TransactionType,
    PaymentDetail,
    PaymentMethodUsage,
)

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


@override_settings(BYPASS_AUTO_COST_UPDATE=False)
class TestSalesOrderListApi(APILiveServerTestCase):
    url = reverse('v2:sales_order_list')

    def setUp(self):
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user_2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user_2.set_password('password')
        self.user_2.save()

        self.outlet_1a = mommy.make('outlet.Outlet', branch_id='outlet-account-1a')
        self.outlet_1b = mommy.make('outlet.Outlet', branch_id='outlet-account-1b')

        self.obj = mommy.make('sales.SalesOrder', outlet=self.outlet_1a, __fill_optional=True)
        self.obj_branch = mommy.make('sales.SalesOrder', outlet=self.outlet_1b, __fill_optional=True)
        mommy.make('sales.ProductOrder', sales_order=self.obj, __fill_optional=True)
        mommy.make('sales.ProductOrder', sales_order=self.obj_branch, __fill_optional=True)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_3.set_password('password')
        self.user_3.save()

        self.outlet_2 = mommy.make('outlet.Outlet', branch_id='outlet-account-2')

        self.obj_2 = mommy.make('sales.SalesOrder', outlet=self.outlet_2, __fill_optional=True)
        mommy.make('sales.ProductOrder', sales_order=self.obj_2, __fill_optional=True)

        set_current_tenant(None)

        super(TestSalesOrderListApi, self).setUp()

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_sales_order(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_sales_order(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        results = res.data.get('results')
        self.assertEqual(len(results), 2)
        self.assertIn('items', results[0])
        self.assertEqual(len(results[0].get('items')), 1)

    def test_user_can_not_access_sales_order_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        codes = [code for code in map(lambda obj: obj.get('code'), res.data.get('results'))]
        self.assertIn(self.obj.code, codes)
        self.assertNotIn(self.obj_2.code, codes)

    def test_user_can_filter_sales_order_of_per_company_branch(self):
        url_with_branch_code = reverse(
            'v2:sales_order_list', kwargs={'pk': self.outlet_1b.pk})
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(url_with_branch_code, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        codes = [code for code in map(lambda obj: obj.get('code'), res.data.get('results'))]
        self.assertNotIn(self.obj.code, codes)
        self.assertIn(self.obj_branch.code, codes)


@override_settings(BYPASS_AUTO_COST_UPDATE=False)
class TestTransactionOrderCreateApi(APILiveServerTestCase):
    url = reverse('v2:transaction_order_create')

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user_2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user_2.set_password('password')
        self.user_2.save()

        self.outlet_1a = mommy.make('outlet.Outlet', branch_id='outlet-account-1a')
        self.outlet_1b = mommy.make('outlet.Outlet', branch_id='outlet-account-1b')

        self.product = mommy.make('catalogue.Product', sku='PROD-0001')
        self.product_2 = mommy.make('catalogue.Product', sku='PROD-0002')

        self.payment_method = mommy.make('sales.PaymentMethod', name='CASH')
        self.payment_method_2 = mommy.make('sales.PaymentMethod', name='CARD')

        self.order_option = mommy.make('sales.SalesOrderOption', name='Dine In')

        self.promo = mommy.make(
            'promo.Promo',
            name='Promo Attack', promoreward__discount_type='percentage',
            promoreward__discount_percentage=20, promoreward__discount_fixed_price=0)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_3.set_password('password')
        self.user_3.save()

        self.outlet_2 = mommy.make('outlet.Outlet', branch_id='outlet-account-2')

        set_current_tenant(None)
        self.transaction_order_dummy = {
            'payment_detail': [{
                'amount': 30000,
                'amount_used': 30000,
                'payment_method': self.payment_method.pk,
            }, {
                'amount': 30000,
                'amount_used': 30000,
                'payment_method': self.payment_method_2.pk,
            }],
            'product_order': [{
                "base_cost": "25000",
                "discount": 0,
                "modifier_selected": "",
                "product": self.product.pk,
                "product_name": self.product.name,
                "product_price": 25000,
                "quantity": 1,
                "sub_total": "25000.0",
                "total_cost": "25000.0"
            }, {
                "base_cost": "35000",
                "discount": 0,
                "modifier_selected": "",
                "product": self.product_2.pk,
                "product_name": self.product_2.name,
                "product_price": 35000,
                "quantity": 1,
                "sub_total": "35000.0",
                "total_cost": "35000.0"
            }],
            'promo_list': [{
                "affected_value": 43378.0,
                "base_value": 54222.0,
                "discount_fixed_price": 0.0,
                "discount_percentage": 20.0,
                "discount_type": "percentage",
                "name": "Promo Attack",
                "promo": self.promo.pk,
                "promo_type": "Discount"
            }],
            'transaction_date': "2016-10-25T07:00:00+07:00",
            'order_option': self.order_option.pk,
            'tendered_amount': 60000,
            'sub_total': 60000,
            'total_cost': 60000,
            'status': 'settled',
            'change': 0,
            'code': 'SALES-001',
        }

        super(TestTransactionOrderCreateApi, self).setUp()

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_create_transaction_order(self):
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_create_transaction_order(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_created_transaction_order_is_accessible_for_owner_group(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

        set_current_tenant(self.account_1)
        self.assertTrue(
            SalesOrder.objects.filter(code=self.transaction_order_dummy['code']).exists())

        set_current_tenant(self.account_2)
        self.assertFalse(
            SalesOrder.objects.filter(code=self.transaction_order_dummy['code']).exists())

    def test_created_transaction_order_does_not_create_existing_object(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

        self.assertEqual(SalesOrder.objects.count(), 1)

    def test_created_transaction_order_update_existing_object_if_attribute_is_allowed(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

        now = timezone.make_aware(datetime.now()) - timedelta(hours=7)
        self.transaction_order_dummy['transaction_date'] = datetime.strftime(now, '%Y-%m-%d %H:%M:%S.%f')
        self.transaction_order_dummy['global_discount_percentage'] = 100
        self.transaction_order_dummy['global_discount_price'] = 50000
        self.transaction_order_dummy['tendered_amount'] = 100000
        self.transaction_order_dummy['sub_total'] = 50000
        self.transaction_order_dummy['change'] = 100000
        self.transaction_order_dummy['total_cost'] = 0
        self.transaction_order_dummy['status'] = 'unsettled'

        self.transaction_order_dummy['payment_detail'].append({
            'amount': 20000,
            'amount_used': 0,
            'payment_method': self.payment_method.pk,
        })

        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        obj = SalesOrder.objects.get(code=self.transaction_order_dummy.get('code'))
        self.assertNotEqual(obj.transaction_date, now)
        self.assertNotEqual(obj.tendered_amount, 100000)
        self.assertNotEqual(obj.sub_total, 50000)
        self.assertNotEqual(obj.total_cost, 0)
        self.assertNotEqual(obj.change, 100000)
        self.assertEqual(obj.status, 'unsettled')

    def test_created_transaction_date_will_not_change_if_given(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(res.data['transaction_date'], "2016-10-25T07:00:00+07:00")

        self.transaction_order_dummy['transaction_date'] = "2016-10-25T08:00:00+07:00"
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(res.data['transaction_date'], "2016-10-25T07:00:00+07:00")

    def test_transaction_order_with_transaction_type(self):
        self.client.login(email=self.user.email, password='password')
        reason = self.user.account.transactiontypereason_set.first()
        self.transaction_order_dummy['transaction_type'] = {
            'transaction_type': 'foc',
            'reason': reason.pk,
            'name': 'foc-user',
        }
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        obj = TransactionType.objects.filter(name='foc-user')
        self.assertTrue(obj.exists())
        self.assertEqual(obj.first().transaction_type, 'foc')

    def test_transaction_order_with_company_branch(self):
        self.client.login(email=self.user.email, password='password')
        self.transaction_order_dummy['outlet'] = self.outlet_1a.pk
        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        obj = SalesOrder.objects.filter(outlet__branch_id=self.outlet_1a.branch_id)
        self.assertTrue(obj.exists())

    def test_transaction_order_without_product_on_product_order(self):
        self.client.login(email=self.user.email, password='password')

        # remove `product` from product_order
        self.transaction_order_dummy['product_order'] = []

        res = self.client.post(
            self.url, self.transaction_order_dummy, format='json')
        self.assertEqual(
            res.status_code, 400,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(len(mail.outbox), 1)


class TestPaymentMethodListApi(APILiveServerTestCase):
    url = reverse('v2:payment_method_list')

    def setUp(self):

        self.obj = mommy.make('sales.PaymentMethod', __fill_optional=True)
        self.obj2 = mommy.make('sales.PaymentMethod', __fill_optional=True)

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()
        enabled_payment_method = mommy.make('sales.EnabledPaymentMethod', __fill_optional=True)
        PaymentMethodUsage.objects.create(
            payment_method=self.obj,
            enabled_payment_method=enabled_payment_method,
            account=self.account_1,
        )
        PaymentMethodUsage.objects.create(
            payment_method=self.obj2,
            enabled_payment_method=enabled_payment_method,
            account=self.account_1,
        )

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        enabled_payment_method = mommy.make('sales.EnabledPaymentMethod', __fill_optional=True)
        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_payment_method_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_list_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.client.logout()

        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        result_list = [payment_method_status for payment_method_status in map(lambda obj: obj.get('is_disabled'), res.data.get('results'))]
        self.assertNotIn(True, result_list, res.data)

        self.client.login(email=self.user3.email, password='password')
        res = self.client.get(self.url, format='json')
        self.client.logout()

        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        result_list = [payment_method_status for payment_method_status in map(lambda obj: obj.get('is_disabled'), res.data.get('results'))]
        self.assertIn(True, result_list, res.data)


class TestSalesOrderOptionListApi(APILiveServerTestCase):
    url = reverse('v2:sales_order_option_list')

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.obj = mommy.make('sales.SalesOrderOption', __fill_optional=True)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = mommy.make('sales.SalesOrderOption', __fill_optional=True)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_sales_order_option_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_list_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        names = [name for name in map(lambda obj: obj.get('name'), res.data.get('results'))]
        self.assertIn(self.obj.name, names, res.data)
        self.assertNotIn(self.obj2.name, names, res.data)


@override_settings(BYPASS_AUTO_COST_UPDATE=False)
class TestSalesOrderSentReceiptViewApi(APILiveServerTestCase):

    def setUp(self):
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.outlet_1a = mommy.make('outlet.Outlet', branch_id='outlet-account-1a')
        self.outlet_1b = mommy.make('outlet.Outlet', branch_id='outlet-account-1b')

        self.obj = mommy.make('sales.SalesOrder', outlet=self.outlet_1a, __fill_optional=True)
        mommy.make('sales.ProductOrder', sales_order=self.obj, __fill_optional=True)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_2 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_2.set_password('password')
        self.user_2.save()
        set_current_tenant(None)

        self.url = reverse(
            'v2:sales_order_sent_receipt',
            kwargs={"pk": self.obj.pk})
        self.data = {
            "email": "email@mail.ru",
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_sales_order(self):
        res = self.client.post(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_sales_order(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_sales_order_of_other_company(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.post(self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
