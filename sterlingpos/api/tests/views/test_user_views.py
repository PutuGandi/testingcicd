import factory
import time

from datetime import date, datetime, timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy
from django.test.utils import override_settings

from model_mommy import mommy

from rest_framework.test import APILiveServerTestCase

from sterlingpos.core.models import (
    get_current_tenant, set_current_tenant
)


class TestSignUpAPI(APILiveServerTestCase):
    url = reverse('v2:user_signup')

    def test_signup_new_user(self):
        province = mommy.make('province.Province', name='Province')
        city = mommy.make(
            'province.City', name='City', province=province)

        data = {
            "account_name": "Account Name",
            "email": "account@mail.ru",
            "password1": "test-password",

            "name": "Outlet Mobile 1",
            "phone": "082124512322",
            "address": "Sesame Street 2nd",
            "province": province.pk,
            "city": city.pk
        }
        res = self.client.post(
            self.url, data, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestListEmailAPI(APILiveServerTestCase):
    url = reverse('v2:emails')

    def setUp(self):
        account = mommy.make('accounts.Account', name='Account')
        set_current_tenant(account)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()
        self.email = self.user.emailaddress_set.create(
            email='admin@test.com')
            
        account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(account_2)
        self.user_2 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_2.set_password('password')
        self.user_2.save()
        self.email = self.user_2.emailaddress_set.create(
            email='shop2-user@test.com')

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_list(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        emails = [obj.get('email') for obj in res.data.get('results')]
        self.assertIn(self.user_2.email, emails, res.data)

    def test_user_can_not_access_list_of_other_company(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        emails = [obj.get('email') for obj in res.data.get('results')]
        self.assertNotIn(self.user.email, emails, res.data)


class TestCreateAdditionalEmailAPI(APILiveServerTestCase):
    url = reverse('v2:emails')

    def setUp(self):
        account = mommy.make('accounts.Account', name='Account')
        set_current_tenant(account)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()
        self.email = self.user.emailaddress_set.create(
            email='admin@test.com')
        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_additional_email(self):
        data = {
            "email": "account@mail.ru",
        }
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(self.url, data, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(
            self.user.emailaddress_set.count(), 2
        )


class TestRemoveEmailAPI(APILiveServerTestCase):
    url_string = 'v2:emails'

    def setUp(self):
        account = mommy.make('accounts.Account', name='Account')
        set_current_tenant(account)
        self.user = mommy.make(
            'users.User', email='admin@test.com')
        self.user.set_password('password')
        self.user.save()
        self.email = self.user.emailaddress_set.create(
            email='toberemoved@test.com')

        account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(account_2)
        self.user_2 = mommy.make(
            'users.User', email='shop2-user@test.com')
        self.user_2.set_password('password')
        self.user_2.save()
        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.email.pk})

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.delete(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_remove_existing_email(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.delete(self.url, format='json')
        self.assertEqual(
            res.status_code, 204,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_remove_email_of_other_company(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.delete(self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestEmailSendVerificationAPI(APILiveServerTestCase):
    url_string = 'v2:emails_send_verification'

    def setUp(self):
        account = mommy.make('accounts.Account', name='Account')
        set_current_tenant(account)
        self.user = mommy.make(
            'users.User', email='admin@test.com')
        self.user.set_password('password')
        self.user.save()
        self.email = self.user.emailaddress_set.create(
            email='toberemoved@test.com')

        account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(account_2)
        self.user_2 = mommy.make(
            'users.User', email='shop2-user@test.com')
        self.user_2.set_password('password')
        self.user_2.save()
        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.email.pk})

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.post(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_remove_existing_email(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_remove_email_of_other_company(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.post(self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestEmailSetPrimaryAPI(APILiveServerTestCase):
    url_string = 'v2:emails_set_primary'

    def setUp(self):
        account = mommy.make('accounts.Account', name='Account')
        set_current_tenant(account)
        self.user = mommy.make(
            'users.User', email='admin@test.com')
        self.user.set_password('password')
        self.user.save()
        self.email = self.user.emailaddress_set.create(
            email='toberemoved@test.com')

        account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(account_2)
        self.user_2 = mommy.make(
            'users.User', email='shop2-user@test.com')
        self.user_2.set_password('password')
        self.user_2.save()
        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.email.pk})

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.post(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_remove_existing_email(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_remove_email_of_other_company(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.post(self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

