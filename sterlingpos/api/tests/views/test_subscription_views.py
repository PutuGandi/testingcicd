import time

from datetime import date, datetime, timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy

from model_mommy import mommy
from rest_framework.test import APILiveServerTestCase

from sterlingpos.push_notification.models import RegisteredFCMDevice
from sterlingpos.core.models import get_current_tenant, set_current_tenant
from sterlingpos.device.models import DeviceUser, DeviceUserPermission


class TestSubscriptionPlanListApi(APILiveServerTestCase):
    url = reverse('v2:subscription_plan_list')

    def setUp(self):
        self.obj = mommy.make('subscription.SubscriptionPlan', __fill_optional=True)

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user_2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user_2.set_password('password')
        self.user_2.save()

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_3.set_password('password')
        self.user_3.save()

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_object_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(len(res.data.get('results')), 1)

    def test_all_user_can_access_object_list(self):
        self.client.login(email=self.user_3.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        self.assertEqual(len(res.data.get('results')), 1)


class TestBillingListApi(APILiveServerTestCase):

    url = reverse('v2:billing_list_create')

    def setUp(self):
        self.plan = mommy.make(
            "subscription.SubscriptionPlan",
            slug="device-user-lite",
            plan_type="device_user_subscription",
            recurrence_period=1,
            price=100000,
            recurrence_unit='M')
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user_2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user_2.set_password('password')
        self.user_2.save()

        self.device_user = mommy.make(
            'device.DeviceUser',
            name='Device User',
            username="device_user",
            account=self.account_1,)
        self.subscription = mommy.make(
            'subscription.DeviceUserLiteSubscription',
            account=self.account_1,
            device_user=self.device_user,
            subscription_plan=self.plan,)
        self.billing = mommy.make(
            "subscription.Billing",
            price=self.plan.price,
            due_date=date.today() + timedelta(days=7),
            account=self.account_1,)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_3.set_password('password')
        self.user_3.save()

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_get_object_list(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_get_object_list(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(len(res.data.get('results')), 1)

    def test_authenticated_user_cannot_get_object_of_other_company(self):
        self.client.login(email=self.user_3.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(len(res.data.get('results')), 0)


class TestBillingCreateAPI(APILiveServerTestCase):
    url = reverse('v2:billing_list_create')

    def setUp(self):
        self.plan = mommy.make(
            "subscription.SubscriptionPlan",
            slug="device-user-lite",
            plan_type="device_user_subscription",
            recurrence_period=1,
            price=100000,
            recurrence_unit='M')
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user_2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user_2.set_password('password')
        self.user_2.save()

        self.device_user = mommy.make(
            'device.DeviceUser',
            name='Device User',
            username="device_user",
            account=self.account_1,)
        self.subscription = mommy.make(
            'subscription.DeviceUserLiteSubscription',
            account=self.account_1,
            device_user=self.device_user,
            subscription_plan=self.plan,)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_3.set_password('password')
        self.user_3.save()

        set_current_tenant(None)
        
        self.object_dummy = {
            "subscription_detail": [{
                "device_user": self.device_user.pk,
                "choosen_plan": self.plan.pk,
            }]
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_create_object(self):
        res = self.client.post(
            self.url, self.object_dummy, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_create_object(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.object_dummy, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_created_object_is_only_accessible_for_owner_group(self):
        self.client.login(email=self.user_3.email, password='password')
        res = self.client.post(
            self.url, self.object_dummy, format='json')
        self.assertEqual(
            res.status_code, 400,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestBillingCancelApi(APILiveServerTestCase):

    url_string = 'v2:billing_cancel'

    def setUp(self):
        self.plan = mommy.make(
            "subscription.SubscriptionPlan",
            slug="device-user-lite",
            plan_type="device_user_subscription",
            recurrence_period=1,
            price=100000,
            recurrence_unit='M')
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user_2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user_2.set_password('password')
        self.user_2.save()

        self.device_user = mommy.make(
            'device.DeviceUser',
            name='Device User',
            username="device_user",
            account=self.account_1,)
        self.subscription = mommy.make(
            'subscription.DeviceUserLiteSubscription',
            account=self.account_1,
            device_user=self.device_user,
            subscription_plan=self.plan,)
        self.billing = mommy.make(
            "subscription.Billing",
            price=self.plan.price,
            due_date=date.today() + timedelta(days=7),
            account=self.account_1,)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_3.set_password('password')
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={'pk': self.billing.pk})

        super(TestBillingCancelApi, self).setUp()

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_delete_object(self):
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_delete_object(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 204,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_cannot_delete_object_of_other_company(self):
        self.client.login(email=self.user_3.email, password='password')
        res = self.client.delete(self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
