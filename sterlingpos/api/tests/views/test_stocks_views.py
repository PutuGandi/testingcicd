from django.urls import reverse

from rest_framework.test import APILiveServerTestCase, APIRequestFactory, force_authenticate
from model_mommy import mommy
from django_multitenant.models import set_current_tenant

from sterlingpos.stocks.models import Stock
from sterlingpos.api.views import stocks_views
from sterlingpos.api.serializers.stocks_serializers import PurchaseOrderSerializer


class BasePurchaseOrderTestApi(APILiveServerTestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.inventorysetting, _ = self.account.inventorysetting_set.get_or_create(
            account=self.account
        )

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="test",
            sku="test-01",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.detail_purchased = self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )
        set_current_tenant(None)

        self.account_2 = mommy.make("accounts.Account", name="account-2")
        set_current_tenant(self.account_2)
        self.inventorysetting_2, _ = self.account_2.inventorysetting_set.get_or_create(
            account=self.account_2
        )

        self.user_2 = mommy.make(
            "users.User", email="test_2@email.com", account=self.account_2
        )
        self.user_2.set_password("test")
        self.user_2.save()
        self.account_2.owners.get_or_create(account=self.account_2, user=self.user)

        self.unit_2 = self.account_2.uom_set.first()
        self.outlet_2 = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account_2
        )
        self.supplier_2 = mommy.make(
            "stocks.Supplier", account=self.account_2, name="Supplier"
        )

        self.product_2 = mommy.make(
            "catalogue.product",
            account=self.account_2,
            is_manage_stock=True,
            uom=self.unit_2,
            name="test",
            sku="test-01",
        )
        self.stock_2, _ = Stock.objects.get_or_create(
            product=self.product_2, outlet=self.outlet_2, account=self.account_2
        )
        self.stock_2.set_balance(10, unit=self.unit_2)

        self.purchase_order_2 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account_2,
            outlet=self.outlet_2,
            supplier=self.supplier_2,
            code="PO-001",
        )
        self.detail_purchased_2 = self.purchase_order_2.purchase_product_stock(
            self.product_2, 10, self.unit_2, 1000, "percentage", 5
        )
        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)


class TestSupplierListView(BasePurchaseOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse("v2.1:suppliers_list")
        set_current_tenant(self.user.account)

    def test_anonymous_user(self):
        request = self.factory.get(self.url, format="json")
        response = stocks_views.SupplierListView.as_view()(request)
        assert response.status_code == 401

    def test_authenticated_user(self):
        request = self.factory.get(self.url, format="json")
        force_authenticate(request, user=self.user)
        response = stocks_views.SupplierListView.as_view()(request)
        assert response.status_code == 200
        assert "results" in response.data
        pks = [
            pk for pk in map(lambda obj: obj.get("id"), response.data.get("results"))
        ]
        assert self.supplier.pk in pks


class TestPurchaseOrderListApi(BasePurchaseOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse("v2.1:purchase_orders_list")
        set_current_tenant(self.user.account)

    def test_anonymous_user(self):
        request = self.factory.get(self.url, format="json")
        response = stocks_views.PurchaseOrderCreateAndListView.as_view()(request)
        assert response.status_code == 401

    def test_authenticated_user(self):
        request = self.factory.get(self.url, format="json")
        force_authenticate(request, user=self.user)
        response = stocks_views.PurchaseOrderCreateAndListView.as_view()(request)
        assert response.status_code == 200
        assert "results" in response.data
        pks = [
            pk for pk in map(lambda obj: obj.get("id"), response.data.get("results"))
        ]
        assert self.purchase_order.pk in pks
        assert self.purchase_order_2.pk not in pks


class TestPurchaseOrderCreateApi(BasePurchaseOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse("v2.1:purchase_orders_create")
        self.params = {
            "purchased_stock": [
                {
                    "purchase_cost": "0",
                    "is_received": False,
                    "received_date": None,
                    "quantity": "10.000",
                    "discount_type": "fixed",
                    "discount": "0",
                    "stock": {
                        "product": self.stock.product.pk,
                    },
                    "unit": self.unit.pk,
                }
            ],
            "status": "active",
            "code": "PO-001",
            "tax": "0",
            "discount_type": "fixed",
            "discount": "0",
            "date_of_purchase": "2021-01-29T07:34:01.281Z",
            "shipping_date": "2021-01-31T07:34:01.281Z",
            "date_of_arrival": None,
            "note": "string",
            "supplier": self.supplier.pk,
            "outlet": self.outlet.pk,
        }

    def test_anonymous_user(self):
        response = self.client.post(self.url, data=self.params, format="json")
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.post(self.url, data=self.params, format="json")
        assert response.status_code == 201


class TestPurchaseOrderUpdateApi(BasePurchaseOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "v2.1:purchase_orders_update", kwargs={"pk": self.purchase_order.pk}
        )
        self.params = {
            "purchased_stock": [
                {
                    "id": self.detail_purchased.pk,
                    "purchase_cost": self.detail_purchased.purchase_cost.amount,
                    "is_received": self.detail_purchased.is_received,
                    "received_date": self.detail_purchased.received_date,
                    "quantity": "100.000",
                    "discount_type": self.detail_purchased.discount_type,
                    "discount": self.detail_purchased.discount,
                    "stock": {
                        "product": self.detail_purchased.stock.product.pk,
                    },
                    "unit": self.detail_purchased.unit.pk,
                }
            ],
            "id": self.purchase_order.pk,
            "status": self.purchase_order.status,
            "code": self.purchase_order.code,
            "tax": self.purchase_order.tax,
            "discount_type": self.purchase_order.discount_type,
            "discount": self.purchase_order.discount,
            "date_of_purchase": self.purchase_order.date_of_purchase,
            "shipping_date": self.purchase_order.shipping_date,
            "date_of_arrival": None,
            "note": "Test Notes",
            "supplier": self.purchase_order.supplier.pk,
            "outlet": self.purchase_order.outlet.pk,
        }

    def test_anonymous_user(self):
        response = self.client.put(self.url, data=self.params, format="json")
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.put(self.url, data=self.params, format="json")
        assert response.status_code == 200


class TestPurchaseOrderCancelRemainingItemsApi(BasePurchaseOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "v2.1:purchase_orders_cancel", kwargs={"pk": self.purchase_order.pk}
        )

    def test_anonymous_user(self):
        response = self.client.put(
            self.url, data={"id": self.purchase_order.pk}, format="json"
        )
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.put(
            self.url, data={"id": self.purchase_order.pk}, format="json"
        )
        assert response.status_code == 200


class BaseReceivedOrderTestApi(BasePurchaseOrderTestApi):
    def setUp(self):
        super().setUp()
        self.purchase_order_3 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-002",
        )
        self.detail_purchased_3 = self.purchase_order_3.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

        self.received_order = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order_3,
            receipt_number="12345",
        )
        self.detail_received = self.received_order.receive_purchase(
            self.stock, 10, self.unit
        )


class TestReceivedOrderListApi(BaseReceivedOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse("v2.1:received_orders_list")

    def test_anonymous_user(self):
        response = self.client.get(self.url, format="json")
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.get(self.url, format="json")
        assert response.status_code == 200


class TestReceivedOrderCreateApi(BaseReceivedOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse("v2.1:received_orders_create")
        purchase_order = PurchaseOrderSerializer(instance=self.purchase_order)
        self.params = {
            "purchase_order": purchase_order.data,
            "receipt_number": "1231515",
            "code": f"RO-{self.purchase_order.code}-1231515",
            "received_detail": [
                {
                    "stock": {"product": self.stock.product.pk},
                    "quantity": 10,
                    "purchase_cost": 10000,
                    "purchase_cost_currency": "IDR",
                    "discount_type": "percentage",
                    "discount": 5,
                    "unit": self.unit.pk,
                }
            ],
        }

    def test_anonymous_user(self):
        response = self.client.post(self.url, data=self.params, format="json")
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.post(self.url, data=self.params, format="json")
        assert response.status_code == 201


class TestReceivedOrderUpdateApi(BaseReceivedOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "v2.1:received_orders_update", kwargs={"pk": self.received_order.pk}
        )

        purchase_order = PurchaseOrderSerializer(instance=self.purchase_order)
        self.params = {
            "purchase_order": purchase_order.data,
            "receipt_number": "1241",
            "code": f"RO-{self.purchase_order.code}-1241",
            "received_detail": [
                {
                    "stock": {"product": self.stock.product.pk},
                    "quantity": 10,
                    "purchase_cost": 10000,
                    "purchase_cost_currency": "IDR",
                    "discount_type": "percentage",
                    "discount": 5,
                    "unit": self.unit.pk,
                }
            ],
        }

    def test_anonymous_user(self):
        response = self.client.put(self.url, data=self.params, format="json")
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.put(self.url, data=self.params, format="json")
        assert response.status_code == 200


class TestReceivedOrderCompleteApi(BaseReceivedOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "v2.1:received_orders_complete", kwargs={"pk": self.received_order.pk}
        )

    def test_anonymous_user(self):
        response = self.client.put(
            self.url, data={"id": self.received_order.pk}, format="json"
        )
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.put(
            self.url, data={"id": self.received_order.pk}, format="json"
        )
        assert response.status_code == 200


class TestReceivedOrderDetailUpdateApi(BaseReceivedOrderTestApi):
    def setUp(self):
        super().setUp()
        self.url = reverse(
            "v2.1:received_orders_detail_update",
            kwargs={
                "received_order_pk": self.received_order.pk,
                "pk": self.detail_received.pk,
            },
        )
        self.params = {
            "id": self.detail_received.pk,
            "purchase_cost": "1500",
            "quantity": "666",
            "discount_type": "fixed",
            "discount": "300",
            "received": self.detail_received.received.pk,
            "stock": {"product": self.detail_received.stock.product.pk},
            "unit": self.detail_received.unit.pk,
        }

    def test_anonymous_user(self):
        response = self.client.put(self.url, data=self.params, format="json")
        assert response.status_code == 401

    def test_authenticated_user(self):
        self.client.login(email=self.user.email, password="test")
        response = self.client.put(self.url, data=self.params, format="json")
        assert response.status_code == 200
