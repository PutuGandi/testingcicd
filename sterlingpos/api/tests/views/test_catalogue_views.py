import time

from datetime import timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy
from django.test import override_settings

from model_mommy import mommy
from rest_framework.test import APILiveServerTestCase

from sterlingpos.catalogue.models import (
    Product, ProductType,
    UOM, Category
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs
from sterlingpos.core.models import get_current_tenant, set_current_tenant


class TestProductListApi(APILiveServerTestCase):
    url = reverse('v2:product_list')

    def setUp(self):
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.obj = mommy.make('catalogue.Product', __fill_optional=True)

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = mommy.make('catalogue.Product', __fill_optional=True)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_product_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_product_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        skus = [sku for sku in map(lambda obj: obj.get('sku'), res.data.get('results'))]
        self.assertIn(self.obj.sku, skus)
        self.assertNotIn(self.obj2.sku, skus)


class TestProductCreateApi(APILiveServerTestCase):
    url = reverse('v2:product_create')

    def setUp(self):
        self.data = {
            'sku': 'product-test',
            'name': 'Product Test',
            'cost': 5000,
            'price': 10000,
        }

        self.account_1 = mommy.make(
            'accounts.Account', name='Account 1')
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True,
            account=self.account_1)
        self.user.set_password('password')
        self.user.save()

        self.account_2 = mommy.make(
            'accounts.Account', name='Account 2')
        self.user2 = mommy.make(
            'users.User', email='shop2-user@test.com',
            account=self.account_2)
        self.user2.set_password('password')
        self.user2.save()

    def test_anon_user_cannot_access_api(self):
        res = self.client.post(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_create_product(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

        self.client.login(email=self.user2.email, password='password')
        res = self.client.post(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestProductUpdateApi(APILiveServerTestCase):

    def setUp(self):
        self.data = {
            'name': 'Product Test UPDATED',
        }

        self.account_1 = mommy.make(
            'accounts.Account', name='Account 1')
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True,
            account=self.account_1)
        self.user.set_password('password')
        self.user.save()
        self.product = mommy.make(
            'catalogue.Product', account=self.account_1,
            __fill_optional=True)
        self.url = reverse(
            'v2:product_update', kwargs={'pk': self.product.pk})

        self.account_2 = mommy.make(
            'accounts.Account', name='Account 2')
        self.user2 = mommy.make(
            'users.User', email='shop2-user@test.com',
            account=self.account_2)
        self.user2.set_password('password')
        self.user2.save()

    def test_anon_user_cannot_access_api(self):
        res = self.client.patch(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_update_product(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.patch(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_cannot_update_product_of_other_account(self):
        self.client.login(email=self.user2.email, password='password')
        res = self.client.patch(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestProductDeleteApi(APILiveServerTestCase):

    def setUp(self):
        self.account_1 = mommy.make(
            'accounts.Account', name='Account 1')
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True,
            account=self.account_1)
        self.user.set_password('password')
        self.user.save()
        self.product = mommy.make(
            'catalogue.Product', account=self.account_1,
            __fill_optional=True)
        self.url = reverse(
            'v2:product_delete', kwargs={'pk': self.product.pk})

        self.account_2 = mommy.make(
            'accounts.Account', name='Account 2')
        self.user2 = mommy.make(
            'users.User', email='shop2-user@test.com',
            account=self.account_2)
        self.user2.set_password('password')
        self.user2.save()

    def test_anon_user_cannot_access_api(self):
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_delete_product(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 204,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_cannot_delete_product_of_other_account(self):
        self.client.login(email=self.user2.email, password='password')
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestCategoryCreateApi(APILiveServerTestCase):
    url = reverse('v2:category_create')

    def setUp(self):
        self.data = {
            'path': 'lorem ipsum',
        }

        self.account_1 = mommy.make(
            'accounts.Account', name='Account 1')
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True,
            account=self.account_1)
        self.user.set_password('password')
        self.user.save()

        self.account_2 = mommy.make(
            'accounts.Account', name='Account 2')
        self.user2 = mommy.make(
            'users.User', email='shop2-user@test.com',
            account=self.account_2)
        self.user2.set_password('password')
        self.user2.save()

    def test_anon_user_cannot_access_api(self):
        res = self.client.post(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_create_category(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

        self.client.login(email=self.user2.email, password='password')
        res = self.client.post(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 201,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestCategoryUpdateApi(APILiveServerTestCase):

    def setUp(self):
        self.data = {
            'path': 'lorem ipsum',
        }

        self.account_1 = mommy.make(
            'accounts.Account', name='Account 1')
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True,
            account=self.account_1)
        self.user.set_password('password')
        self.user.save()
        self.category = create_from_breadcrumbs(
            "lorem", account=self.account_1)
        
        self.url = reverse(
            'v2:category_update', kwargs={'pk': self.category.pk})

        self.account_2 = mommy.make(
            'accounts.Account', name='Account 2')
        self.user2 = mommy.make(
            'users.User', email='shop2-user@test.com',
            account=self.account_2)
        self.user2.set_password('password')
        self.user2.save()

    def test_anon_user_cannot_access_api(self):
        res = self.client.patch(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_update_category(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.patch(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_cannot_update_category_of_other_account(self):
        self.client.login(email=self.user2.email, password='password')
        res = self.client.patch(
            self.url, self.data, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestCategoryDeleteApi(APILiveServerTestCase):

    def setUp(self):
        self.account_1 = mommy.make(
            'accounts.Account', name='Account 1')
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True,
            account=self.account_1)
        self.user.set_password('password')
        self.user.save()
        self.category = create_from_breadcrumbs(
            "lorem", account=self.account_1)
        
        self.url = reverse(
            'v2:category_delete', kwargs={'pk': self.category.pk})

        self.account_2 = mommy.make(
            'accounts.Account', name='Account 2')
        self.user2 = mommy.make(
            'users.User', email='shop2-user@test.com',
            account=self.account_2)
        self.user2.set_password('password')
        self.user2.save()

    def test_anon_user_cannot_access_api(self):
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_delete_category(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 204,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_cannot_delete_category_of_other_account(self):
        self.client.login(email=self.user2.email, password='password')
        res = self.client.delete(
            self.url, format='json')
        self.assertEqual(
            res.status_code, 404,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestCategoryListApi(APILiveServerTestCase):
    url = reverse('v2:category_list')

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.obj = create_from_breadcrumbs('test-category')

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = create_from_breadcrumbs('test-category-2')

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_category_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_category_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        names = [name for name in map(lambda obj: obj.get('name'), res.data.get('results'))]
        self.assertIn(self.obj.name, names, res.data)
        self.assertNotIn(self.obj2.name, names, res.data)


class TestProductTypeListApi(APILiveServerTestCase):
    url = reverse('v2:product_type_list')

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.obj = mommy.make('catalogue.ProductType', __fill_optional=True)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = mommy.make('catalogue.ProductType', __fill_optional=True)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_category_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_category_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        names = [name for name in map(lambda obj: obj.get('name'), res.data.get('results'))]
        self.assertIn(self.obj.name, names, res.data)
        self.assertNotIn(self.obj2.name, names, res.data)


class TestUOMListApi(APILiveServerTestCase):
    url = reverse('v2:uom_list')

    def setUp(self):

        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.obj = mommy.make(
            'catalogue.UOM',
            unit="unit-1",
            __fill_optional=True)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.obj2 = mommy.make(
            'catalogue.UOM',
            unit="unit-2",
            __fill_optional=True)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_uom_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_category_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        units = [unit for unit in map(lambda obj: obj.get('unit'), res.data.get('results'))]
        self.assertIn(self.obj.unit, units, res.data)
        self.assertNotIn(self.obj2.unit, units, res.data)
