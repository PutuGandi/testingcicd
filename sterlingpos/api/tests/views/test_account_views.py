import factory
import time

from datetime import date, datetime, timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy
from django.test.utils import override_settings

from model_mommy import mommy

from rest_framework.test import APILiveServerTestCase

from sterlingpos.core.models import (
    get_current_tenant, set_current_tenant
)


class TestAccountLoginAPI(APILiveServerTestCase):
    url = reverse('v2:rf_auth_login')

    def setUp(self):
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.emailaddress_set.create(
            email=self.user.email, verified=True, primary=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.emailaddress_set.create(
            email=self.user2.email, verified=True, primary=True)
        self.user2.set_password('password')
        self.user2.save()

        self.account_1.owners.create(user=self.user, account=self.account_1)
        self.account_1.owners.create(user=self.user2, account=self.account_1)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.emailaddress_set.create(
            email=self.user3.email, verified=True, primary=True)
        self.user3.set_password('password')
        self.user3.save()

        self.account_2.owners.create(user=self.user3, account=self.account_2)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_authenticated_user_can_login_subscription(self):
        res = self.client.post(
            self.url,
            {'email': self.user2.email, 'password': 'password'},
            format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('key', res.data, 'Bad data (%s)' % (res.data))
        self.assertIn('email', res.data, 'Bad data (%s)' % (res.data))
        self.assertIn('company_name', res.data, 'Bad data (%s)' % (res.data))

    def test_login_with_bad_auth(self):
        res = self.client.post(
            self.url,
            {'email': self.user2.email, 'password': 'bad-password'},
            format='json')
        self.assertEqual(
            res.status_code, 400,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_login_under_other_authenticated_session(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.post(
            self.url,
            {'email': self.user3.email, 'password': 'password'},
            format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('key', res.data, 'Bad data (%s)' % (res.data))
        self.assertIn('email', res.data, 'Bad data (%s)' % (res.data))
        self.assertEqual(self.user3.email, res.data.get('email'), 'Bad data (%s)' % (res.data))
        self.assertIn('company_name', res.data, 'Bad data (%s)' % (res.data))

    def test_login_disregard_bad_key_or_expired_key(self):
        self.client.credentials(HTTP_AUTHORIZATION='JWT BAD-JWT-KEY')
        res = self.client.post(
            self.url,
            {'email': self.user3.email, 'password': 'password'},
            format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('key', res.data, 'Bad data (%s)' % (res.data))
        self.assertIn('email', res.data, 'Bad data (%s)' % (res.data))
        self.assertEqual(self.user3.email, res.data.get('email'), 'Bad data (%s)' % (res.data))
        self.assertIn('company_name', res.data, 'Bad data (%s)' % (res.data))
        res = self.client.get(reverse('v2:account_detail'), format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestOutletAuthenticationAPI(APILiveServerTestCase):
    url = reverse('v2:rf_outlet_auth')

    def setUp(self):
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.emailaddress_set.create(
            email=self.user.email, verified=True, primary=True)
        self.user.set_password('password')
        self.user.save()
        self.account_1.owners.create(user=self.user, account=self.account_1)

        self.subscription_1 = mommy.make(
            'subscription.Subscription',
            outlet__name='Outlet-1',
            outlet__branch_id='outlet-1',
            subscription_plan__name='Trial',
            expires=date.today() + timedelta(days=30),
            __fill_optional=True)
        self.outlet_1 = self.subscription_1.outlet

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user_2 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_2.emailaddress_set.create(
            email=self.user_2.email, verified=True, primary=True)
        self.user_2.set_password('password')
        self.user_2.save()
        self.account_2.owners.create(user=self.user_2, account=self.account_2)

        self.subscription_2 = mommy.make(
            'subscription.Subscription',
            outlet__name='Outlet-2',
            outlet__branch_id='outlet-2',
            subscription_plan__name='Trial',
            expires=date.today() - timedelta(days=30),
            __fill_optional=True)
        self.outlet_2 = self.subscription_2.outlet
        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_authenticated_active_outlet(self):
        res = self.client.post(
            reverse_lazy('v2:rf_auth_login'),
            {'email': self.user.email, 'password': 'password'},
            format='json')
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + res.data.get('key'))
        res = self.client.post(self.url, {
            "branch_id": self.outlet_1.branch_id,
        }, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('key', res.data, 'Bad data (%s)' % (res.data))
        self.assertIn('outlet', res.data, 'Bad data (%s)' % (res.data))
        self.assertEqual(res.data['outlet'], self.outlet_1.branch_id, 'Bad data (%s)' % (res.data))

    def test_authenticated_nonexistant_outlet(self):
        res = self.client.post(
            reverse_lazy('v2:rf_auth_login'),
            {'email': self.user.email, 'password': 'password'},
            format='json')
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + res.data.get('key'))
        res = self.client.post(self.url, {
            "branch_id": 'not-exists',
        }, format='json')
        self.assertEqual(
            res.status_code, 400,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_inactive_outlet(self):
        res = self.client.post(
            reverse_lazy('v2:rf_auth_login'),
            {'email': self.user_2.email, 'password': 'password'},
            format='json')
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + res.data.get('key'))
        res = self.client.post(self.url, {
            "branch_id": self.outlet_2.branch_id,
        }, format='json')
        self.assertEqual(
            res.status_code, 400,
            'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestAccountDetailAPI(APILiveServerTestCase):
    url = reverse('v2:account_detail')

    def setUp(self):
        self.plan_1 = mommy.make(
            'subscription.SubscriptionPlan',
            plan_type="others", slug='lite-table-management')
        self.plan_2 = mommy.make(
            'subscription.SubscriptionPlan',
            plan_type="others", slug='lite-split-bill')

        account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.emailaddress_set.create(
            email=self.user.email, verified=True, primary=True)
        self.user.set_password('password')
        self.user.save()
        account_1.owners.create(user=self.user, account=account_1)

        account_1.othersubscription_set.create(
            subscription_plan=self.plan_1)
        account_1.othersubscription_set.create(
            subscription_plan=self.plan_2)
        account_1.othersubscription_set.update(active=True)

        account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(account_2)
        self.user_2 = mommy.make('users.User', email='shop2-user@test.com')
        self.user_2.emailaddress_set.create(
            email=self.user_2.email, verified=True, primary=True)
        self.user_2.set_password('password')
        self.user_2.save()
        account_2.owners.create(user=self.user_2, account=account_2)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_account_detail_with_lite_subscription_credential(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(len(res.data.get('other_active_subscriptions')), 2, 'Bad data (%s)' % (res.data))
        self.assertTrue(res.data.get('lite_table_management'), 'Bad data (%s)' % (res.data))
        self.assertTrue(res.data.get('lite_split_bill'), 'Bad data (%s)' % (res.data))

    def test_account_detail_with_no_lite_subscription_credential(self):
        self.client.login(email=self.user_2.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertEqual(len(res.data.get('other_active_subscriptions')), 0, 'Bad data (%s)' % (res.data))
        self.assertFalse(res.data.get('lite_table_management'), 'Bad data (%s)' % (res.data))
        self.assertFalse(res.data.get('lite_split_bill'), 'Bad data (%s)' % (res.data))
