import time

from datetime import timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy
from django.test import override_settings

from model_mommy import mommy
from rest_framework.test import APILiveServerTestCase

from sterlingpos.core.models import (
    get_current_tenant, set_current_tenant
)


class TestOutletListAPI(APILiveServerTestCase):
    url = reverse('v2:outlet_list')

    def setUp(self):
        # ADD ACCOUNT OWNER
        self.account_1 = mommy.make('accounts.Account', name='Account 1')
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

        self.user2 = mommy.make(
            'users.User', email='shop-user@test.com', account=self.user.account)
        self.user2.set_password('password')
        self.user2.save()

        self.account_1.owners.create(user=self.user, account=self.account_1)
        self.account_1.owners.create(user=self.user2, account=self.account_1)

        self.obj = mommy.make(
            'outlet.Outlet',
            name="Outlet-1", branch_id="outlet-1", __fill_optional=True)

        self.account_2 = mommy.make('accounts.Account', name='Account 2')
        set_current_tenant(self.account_2)
        self.user3 = mommy.make('users.User', email='shop2-user@test.com')
        self.user3.set_password('password')
        self.user3.save()

        self.account_2.owners.create(user=self.user3, account=self.account_2)

        self.obj2 = mommy.make(
            'outlet.Outlet',
            name="Outlet-2", branch_id="outlet-2", __fill_optional=True)

        set_current_tenant(None)

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 401,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_authenticated_user_can_access_uom_list(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))

    def test_user_can_not_access_category_of_other_company(self):
        self.client.login(email=self.user.email, password='password')
        res = self.client.get(self.url, format='json')
        self.assertEqual(
            res.status_code, 200,
            'Bad response (%i) (%s)' % (res.status_code, res.data))
        self.assertIn('results', res.data)
        names = map(lambda obj: obj.get('name'), res.data.get('results'))
        self.assertIn(self.obj.name, names, res.data)
        self.assertNotIn(self.obj2.name, names, res.data)

    # def test_user_with_expired_subscription_can_not_access_category_list(self):
    #     self.client.login(email=self.user3.email, password='password')
    #     res = self.client.get(self.url, format='json')
    #     self.assertEqual(
    #         res.status_code, 403,
    #         'Bad response (%i) (%s)' % (res.status_code, res.data))


class TestOutletUpdateApi(APILiveServerTestCase):

    url_string = "v2:outlet_update"

    def setUp(self):
        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.outlet = mommy.make("outlet.Outlet",
            name='outlet-1',
            branch_id="1",
            account=self.account_1,
            __fill_optional=True)

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.outlet.pk})

        self.obj_dummy = {
            "name": "outlet-1-updated",
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_update_object(self):
        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_update_object(self):
        self.client.login(email=self.user.email, password="password")

        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 200,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

        self.assertEqual(res.data.get('name'), 'outlet-1-updated', res.data)

    def test_authenticated_user_cannot_update_object_of_other_company(self):
        self.client.login(email=self.user_3.email, password="password")
        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 404,
            "Bad response (%i) (%s)" % (res.status_code, res.data))


class TestOutletCreateApi(APILiveServerTestCase):

    url_string = "v2:outlet_create"

    def setUp(self):
        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(self.url_string)

        self.obj_dummy = {
            "name": "outlet-1",
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_update_object(self):
        res = self.client.post(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_create_object(self):
        self.client.login(email=self.user_2.email, password="password")

        res = self.client.post(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 201,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

        self.assertEqual(res.data.get('name'), 'outlet-1', res.data)
        self.assertTrue(len(res.data.get('device_users')) > 0, res.data)
