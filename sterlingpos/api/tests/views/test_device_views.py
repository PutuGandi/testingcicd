import time

from datetime import timedelta

from django.utils import timezone
from django.urls import reverse, reverse_lazy

from model_mommy import mommy
from rest_framework.test import APILiveServerTestCase

from sterlingpos.push_notification.models import RegisteredFCMDevice
from sterlingpos.core.models import get_current_tenant, set_current_tenant
from sterlingpos.device.models import DeviceUser, DeviceUserPermission


class TestDeviceUserListApi(APILiveServerTestCase):
    url = reverse("v2:device_user_list")

    def setUp(self):

        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.obj = mommy.make("device.DeviceUser", __fill_optional=True)

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        self.obj_2 = mommy.make("device.DeviceUser", __fill_optional=True)

        set_current_tenant(None)

        super(TestDeviceUserListApi, self).setUp()

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_access_api(self):
        res = self.client.get(self.url, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_access_object_list(self):
        self.client.login(email=self.user.email, password="password")
        res = self.client.get(self.url, format="json")
        self.assertEqual(
            res.status_code, 200,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_user_can_not_access_object_of_other_company(self):
        self.client.login(email=self.user.email, password="password")
        res = self.client.get(self.url, format="json")
        self.assertEqual(
            res.status_code, 200,
            "Bad response (%i) (%s)" % (res.status_code, res.data))
        self.assertIn("results", res.data)
        device_user_pks = [pk for pk in map(lambda obj: obj.get("id"), res.data.get("results"))]
        self.assertIn(self.obj.pk, device_user_pks)
        self.assertNotIn(self.obj_2.pk, device_user_pks)


class TestDeviceUserCreateApi(APILiveServerTestCase):

    url = reverse("v2:device_user_create")

    def setUp(self):
        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.outlet = mommy.make("outlet.Outlet", branch_id="1", __fill_optional=True)
        self.outlet_2 = mommy.make("outlet.Outlet", branch_id="2", __fill_optional=True)

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.obj_dummy = {
            "name": "created",
            "email": "created@mail.ru",
            "username": "created",
            "pin": 123456,
            "outlet": [
                self.outlet.pk,
            ],
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_update_object(self):
        res = self.client.post(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_create_object(self):
        self.client.login(email=self.user_2.email, password="password")

        self.obj_dummy["outlet"].append(self.outlet_2.pk)
        res = self.client.post(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 201,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

        device_user = DeviceUser.objects.get(username="created")
        self.assertEqual(device_user.outlet.count(), 2)

    def test_authenticated_user_cannot_create_object_using_outlet_from_other_company(self):
        self.client.login(email=self.user_3.email, password="password")
        res = self.client.post(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 400,
            "Bad response (%i) (%s)" % (res.status_code, res.data))


class TestDeviceUserDeleteApi(APILiveServerTestCase):

    url_string = "v2:device_user_delete"

    def setUp(self):
        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.outlet = mommy.make("outlet.Outlet", branch_id="1", __fill_optional=True)
        self.outlet_2 = mommy.make("outlet.Outlet", branch_id="2", __fill_optional=True)
        self.obj = mommy.make("device.DeviceUser", __fill_optional=True)
        self.obj.outletdeviceuser_set.create(
            outlet=self.outlet,
            device_user=self.obj,
            account=self.obj.account)

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.obj.pk})


    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_delete_object(self):
        res = self.client.delete(self.url, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_delete_object(self):
        self.client.login(email=self.user_2.email, password="password")

        res = self.client.delete(self.url, format="json")
        self.assertEqual(
            res.status_code, 204,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_cannot_delete_object_of_other_company(self):
        self.client.login(email=self.user_3.email, password="password")
        res = self.client.delete(self.url, format="json")
        self.assertEqual(
            res.status_code, 404,
            "Bad response (%i) (%s)" % (res.status_code, res.data))


class TestDeviceUserUpdateApi(APILiveServerTestCase):

    url_string = "v2:device_user_update"

    def setUp(self):
        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.outlet = mommy.make("outlet.Outlet", branch_id="1", __fill_optional=True)
        self.outlet_2 = mommy.make("outlet.Outlet", branch_id="2", __fill_optional=True)
        self.obj = mommy.make("device.DeviceUser", __fill_optional=True)
        self.obj.outletdeviceuser_set.create(
            outlet=self.outlet,
            device_user=self.obj,
            account=self.obj.account)

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.obj.pk})

        self.obj_dummy = {
            "username": "updated",
            "outlet": [],
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_update_object(self):
        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_update_object(self):
        self.client.login(email=self.user_2.email, password="password")

        self.obj_dummy["outlet"] = [
            self.outlet.pk,
            self.outlet_2.pk
        ]
        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 200,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

        device_user = DeviceUser.objects.get(username="updated")
        self.assertEqual(device_user.outlet.count(), 2)

    def test_authenticated_user_cannot_update_object_of_other_company(self):
        self.client.login(email=self.user_3.email, password="password")
        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 404,
            "Bad response (%i) (%s)" % (res.status_code, res.data))


class TestFCMDeviceCreateAPI(APILiveServerTestCase):
    url = reverse("v2:fcm_device_create")

    def setUp(self):

        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.object_dummy = {
            "registered_id": "REGID-001",
            "name": "Device 001",
            "last_user_login": self.account_1.deviceuser_set.first().pk
        }

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_create_object(self):
        res = self.client.post(
            self.url, self.object_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_create_object(self):
        self.client.login(email=self.user.email, password="password")
        res = self.client.post(
            self.url, self.object_dummy, format="json")
        self.assertEqual(
            res.status_code, 201,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_created_object_is_accessible_for_owner_group(self):
        self.client.login(email=self.user.email, password="password")
        res = self.client.post(
            self.url, self.object_dummy, format="json")
        self.assertEqual(
            res.status_code, 201,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

        set_current_tenant(self.user.account)
        self.assertTrue(RegisteredFCMDevice.objects.filter(
            name=self.object_dummy.get("name")).exists())
        set_current_tenant(self.user_2.account)
        self.assertTrue(RegisteredFCMDevice.objects.filter(
            name=self.object_dummy.get("name")).exists())
        set_current_tenant(self.user_3.account)
        self.assertFalse(RegisteredFCMDevice.objects.filter(
            name=self.object_dummy.get("name")).exists())
        set_current_tenant(None)


class TestFCMDeviceUpdateteApi(APILiveServerTestCase):

    url_string = "v2:fcm_device_update"

    def setUp(self):

        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        device_user = mommy.make("device.DeviceUser", __fill_optional=True)
        self.obj = mommy.make("push_notification.RegisteredFCMDevice", __fill_optional=True)

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.obj.pk})

        self.obj_dummy = {"last_user_login": device_user.pk}

        super(TestFCMDeviceUpdateteApi, self).setUp()

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_update_object(self):
        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_update_object(self):
        self.client.login(email=self.user_2.email, password="password")
        res = self.client.patch(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 200,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_cannot_update_object_of_other_company(self):
        self.client.login(email=self.user_3.email, password="password")
        res = self.client.patch(self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 404,
            "Bad response (%i) (%s)" % (res.status_code, res.data))


class TestFCMDeviceDeleteApi(APILiveServerTestCase):

    url_string = "v2:fcm_device_delete"

    def setUp(self):

        self.account_1 = mommy.make("accounts.Account", name="Account 1")
        set_current_tenant(self.account_1)
        self.user = mommy.make(
            "users.User", email="admin@test.com", is_staff=True)
        self.user.set_password("password")
        self.user.save()

        self.user_2 = mommy.make(
            "users.User", email="shop-user@test.com", account=self.user.account)
        self.user_2.set_password("password")
        self.user_2.save()

        self.obj = mommy.make("push_notification.RegisteredFCMDevice", __fill_optional=True)

        self.account_2 = mommy.make("accounts.Account", name="Account 2")
        set_current_tenant(self.account_2)
        self.user_3 = mommy.make("users.User", email="shop2-user@test.com")
        self.user_3.set_password("password")
        self.user_3.save()

        set_current_tenant(None)

        self.url = reverse(
            self.url_string, kwargs={"pk": self.obj.pk})
        self.obj_dummy = {}

        super(TestFCMDeviceDeleteApi, self).setUp()

    def tearDown(self):
        set_current_tenant(None)

    def test_anon_user_cannot_delete_object(self):
        res = self.client.delete(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 401,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_can_delete_object(self):
        self.client.login(email=self.user_2.email, password="password")
        res = self.client.delete(
            self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 204,
            "Bad response (%i) (%s)" % (res.status_code, res.data))

    def test_authenticated_user_cannot_delete_object_of_other_company(self):
        self.client.login(email=self.user_3.email, password="password")
        res = self.client.delete(self.url, self.obj_dummy, format="json")
        self.assertEqual(
            res.status_code, 404,
            "Bad response (%i) (%s)" % (res.status_code, res.data))
