from model_mommy import mommy

from django.test import override_settings
from django.contrib.messages.storage import default_storage

from test_plus.test import TestCase
from rest_framework.test import APIRequestFactory
from rest_framework.request import Request
from sterlingpos.api.serializers.users_serializers import (
    UserPasswordFormSerializer,
    LostPasswordFormSerializer,
)

from rest_framework.authtoken.models import Token
from sterlingpos.users.tests.factories import UserFactory


class UserPasswordFormSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = UserPasswordFormSerializer
        self.user = mommy.make(
            'users.User',
            email='test@test.com',
            account__name='Account-1')
        self.user.set_password('password')
        self.user.save()
        self.request_factory = APIRequestFactory()

        self.request = Request(self.request_factory.get(
            '/api/v1/change_password/'))
        self.request.user = self.user

    def test_change_password(self):
        data = {
            'oldpassword': 'password',
            'password1': 'new-password',
            'password2': 'new-password',
        }

        serializer = self.serializer_class(data=data, context={
            'request': self.request,
        })

        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()
        self.assertTrue(self.user.check_password('new-password'))

    def test_change_password_with_false_current_password(self):

        data = {
            'oldpassword': 'false-password',
            'password1': 'new-password',
            'password2': 'new-password',
        }
        serializer = self.serializer_class(data=data, context={
            'request': self.request,
        })
        self.assertFalse(serializer.is_valid())

    def test_change_password_with_different_password_confirmation(self):
        data = {
            'oldpassword': 'password',
            'password1': 'new-password',
            'password2': 'new-password-2',
        }

        serializer = self.serializer_class(data=data, context={
            'request': self.request,
        })

        self.assertFalse(serializer.is_valid())


class LostPasswordFormSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = LostPasswordFormSerializer
        self.user = mommy.make(
            'users.User',
            email='test@test.com',
            account__name='Account-1')
        self.user.set_password('password')
        self.user.save()
        self.request_factory = APIRequestFactory()

        self.request = Request(self.request_factory.get('/api/v1/reset_password/'))

    def test_reset_password(self):
        data = {
            'email': self.user.email,
        }

        serializer = self.serializer_class(data=data, context={
            'request': self.request,
        })

        self.assertTrue(serializer.is_valid())
        self.assertTrue(serializer.save())

    def test_reset_password_with_false_email(self):

        data = {
            'email': 'false-email@mail.com',
        }
        serializer = self.serializer_class(data=data, context={
            'request': self.request,
        })
        self.assertFalse(serializer.is_valid())
