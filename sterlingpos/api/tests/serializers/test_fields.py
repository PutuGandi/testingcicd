import factory
from datetime import datetime
from django.utils import timezone
from test_plus.test import TestCase

from sterlingpos.api.serializers.fields import DateTimeTZField


class DateTimeTZFieldTest(TestCase):

    def setUp(self):
        self.field = DateTimeTZField()

    def test_to_representation_return_current_active_TZ_when_given_UTC_datetime(self):
        date_time = timezone.now()  # UTC
        representation = self.field.to_representation(date_time)
        self.assertIn('+07:00', representation)

    def test_to_represen_return_current_active_TZ_when_given_unaware_datetime(self):
        date_time = datetime.now()  # UTC
        representation = self.field.to_representation(date_time)
        self.assertIn('+07:00', representation)
