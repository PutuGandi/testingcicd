from model_mommy import mommy

from datetime import timedelta
from django.utils import timezone

from test_plus.test import TestCase
from sterlingpos.core.models import set_current_tenant

from sterlingpos.api.serializers.kitchen_display_serializers import (
    KitchenCardSerializer,
)

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


class KitchenCardTest(TestCase):

    def setUp(self):
        self.serializer_class = KitchenCardSerializer
        self.account = mommy.make(
            'accounts.Account', name='Account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_is_available(self):
        outlet = mommy.make(
            'outlet.Outlet', branch_id='outlet-1')
        product = mommy.make(
            'catalogue.Product', sku='prod-1')
        data = {
            "outlet": outlet.pk,
            "code": "TRX202007308P55xdnert",

            "start_date": "2020-07-28T18:32:15+0700",
            "end_date": "2020-07-28T18:32:15+0700",
            "duration": 10000,

            "card_items": [
                {
                "product": product.pk,
                "quantity": 1,
                "created_at": "2020-07-28T18:32:15+0700",
                "cooked_at": "2020-07-28T18:32:15+0700",
                "finished_at": "2020-07-28T18:32:29+0700",
                "voided_time": None,
                }
            ]
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(
            serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.card_items.count(), 1)