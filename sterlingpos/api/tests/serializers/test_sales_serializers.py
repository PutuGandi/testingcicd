import pytz

from django.utils import timezone
from django.core import mail

from test_plus.test import TestCase
from datetime import datetime, timedelta
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.api.serializers.sales_serializers import (
    SalesOrderInvoiceSerializer,
    SalesOrderPromoHistorySerializer,
    PaymentMethodSerializer,
    SalesOrderSerializer,
    SalesOrderSentReceiptSerializer,
    ProductOrderSerializer,
    PaymentDetailSerializer,
    TransactionOrderSerializer,
    TransactionTypeSerializer,
    ProductOrderCompositeDetailSerializer,
)
from sterlingpos.sales.models import (
    SalesOrder,
    SalesOrderOption,
    ProductOrder,
    TransactionType,
    PaymentMethodUsage,
)
from sterlingpos.catalogue.models import (
    UOM,
    ProductType,
    Product,
)


class InvoiceSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = SalesOrderInvoiceSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        customer = mommy.make(
            'customer.Customer', name='CUSTOMER-001', account=get_current_tenant())

        sales_invoice_data = {
            "code": "INSTALLMENT-001",
            "sales_order": sales_order.pk,
            "customer": customer.pk,
        }
        serializer = self.serializer_class(data=sales_invoice_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.account.pk, get_current_tenant().pk)


class InvoiceInstallmentSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = SalesOrderInvoiceSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object_with_installment(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        customer = mommy.make(
            'customer.Customer', name='CUSTOMER-001', account=get_current_tenant())

        sales_invoice_data = {
            "sales_order": sales_order.pk,
            "due_date": datetime(2022, 5, 17),
            "date": datetime.now(),
            "customer": customer.pk,
            "installment": {
                "period": "monthly",
                "tenor_count": 7
            }
        }
        serializer = self.serializer_class(data=sales_invoice_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertEqual(obj.pk, obj.installment.invoice_id)

    def test_create_object_without_installment(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        customer = mommy.make(
            'customer.Customer', name='CUSTOMER-001', account=get_current_tenant())

        sales_invoice_data = {
            "sales_order": sales_order.pk,
            "due_date": datetime(2022, 5, 17),
            "date": datetime.now(),
            "customer": customer.pk,
        }
        serializer = self.serializer_class(data=sales_invoice_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertFalse(hasattr(obj, 'installment'))


class TransactionTypeSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = TransactionTypeSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        reason = sales_order.account.transactiontypereason_set.first()
        transaction_type_data = {
            'name': 'FOC Test',
            'transaction_type': TransactionType.TRANSACTION_TYPE.foc,
            'reason': reason.pk,
            'sales_order': sales_order.pk,
        }
        serializer = self.serializer_class(data=transaction_type_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.account.pk, get_current_tenant().pk)


class PaymentDetailSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = PaymentDetailSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        payment_method = mommy.make('sales.PaymentMethod', name='Cash')

        payment_detail_data = {
            'amount': 10000,
            'amount_used': 10000,
            'payment_method': payment_method.pk,
        }
        serializer = self.serializer_class(
            data=payment_detail_data,
            context={'sales_order': sales_order})

        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.payment_method, payment_method)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)


class PaymentMethodSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = PaymentMethodSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        payment_method_data = {
            'name': "Cash",
        }
        serializer = self.serializer_class(data=payment_method_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.name, "Cash")

    def test_is_disabled(self):
        payment_method_1 = mommy.make('sales.PaymentMethod', name='payment-1')
        payment_method_2 = mommy.make('sales.PaymentMethod', name='payment-2')
        enabled_payment_method = mommy.make('sales.EnabledPaymentMethod')
        PaymentMethodUsage.objects.create(
            enabled_payment_method=enabled_payment_method,
            payment_method=payment_method_2,
        )
        serializer_1 = self.serializer_class(instance=payment_method_1)
        serializer_2 = self.serializer_class(instance=payment_method_2)
        self.assertTrue(serializer_1.data.get('is_disabled'), serializer_1.data)
        self.assertFalse(serializer_2.data.get('is_disabled'), serializer_2.data)


class SalesOrderSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = SalesOrderSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        sales_order_data = {
            'code': "SALES-001",
            'transaction_date': timezone.now(),
            'tendered_amount': 3000,
            'sub_total': 2000,
            'total_cost': 2000,
            'change': 1000,
            'order_name': "".join([str(x) for x in range(1, 40)])
        }
        serializer = self.serializer_class(data=sales_order_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertEqual(len(obj.order_name), 60)


class ProductOrderSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = ProductOrderSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        product = mommy.make('catalogue.Product', sku='PRODUCT-001')

        product_order_data = {
            'product': product.pk,
            'quantity': 1,
            'base_cost': 3000,
            'sub_total': 3000,
            'total_cost': 3000,
        }
        serializer = self.serializer_class(
            data=product_order_data,
            context={'sales_order': sales_order})
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.product, product)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)

    def test_ordering_composite_product(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        product = mommy.make('catalogue.Product', sku='PRODUCT-001')
        composite_product = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-COMP-001',
            classification=Product.PRODUCT_CLASSIFICATION.composite)

        composite_product.composite_material.create(
            product=product, product_quantity=2)

        product_order_data = {
            'product': composite_product.pk,
            'quantity': 1,
            'composite_detail': [{
                "product": product.pk,
                "product_quantity": 2,
            }],
            'base_cost': 3000,
            'sub_total': 3000,
            'total_cost': 3000,
        }

        serializer = self.serializer_class(
            data=product_order_data,
            context={'sales_order': sales_order})
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.composite_detail.count(), 1)

    def test_ordering_product_with_modifier(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        product = mommy.make('catalogue.Product', sku='PRODUCT-001')
        product_modifier = mommy.make(
            'catalogue.ProductModifier',
            product=product, name='PRODUCT-001-MOD')
        product_modifier_option = mommy.make(
            'catalogue.ProductModifierOption',
            product_modifier=product_modifier, value='opt-1')

        product_order_data = {
            'product': product.pk,
            'quantity': 1,
            'modifier_detail': [{
                "product": product.pk,
                "option": product_modifier_option.pk,
            }],
            'base_cost': 3000,
            'sub_total': 3000,
            'total_cost': 3000,
        }

        serializer = self.serializer_class(
            data=product_order_data,
            context={'sales_order': sales_order})
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.modifier_detail.count(), 1)

    def test_ordering_product_with_addon(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        product = mommy.make('catalogue.Product', sku='PRODUCT-001')
        product_addon = mommy.make(
            'catalogue.Product',
            sku='PRODUCT-001-ADDON',
            classification=Product.PRODUCT_CLASSIFICATION.complementary)
        mommy.make(
            'catalogue.ProductAddOn',
            product=product_addon, product_usage=product)

        product_order_data = {
            'product': product.pk,
            'quantity': 1,
            'addon_detail': [{
                "product": product_addon.pk,
                "quantity": 1,
                "base_cost": 3000,
                "sub_total": 3000,
            }],
            'base_cost': 3000,
            'sub_total': 3000,
            'addon_cost': 3000,
            'total_cost': 6000,
        }

        serializer = self.serializer_class(
            data=product_order_data,
            context={'sales_order': sales_order})
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.addon_detail.count(), 1)


class ProductOrderCompositeDetailSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = ProductOrderCompositeDetailSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        product = mommy.make('catalogue.Product', sku='PRODUCT-001')
        product_order = mommy.make(
            'sales.ProductOrder', product=product, sales_order=sales_order)

        product_order_composite_detail_data = {
            'product': product.pk,
            'product_quantity': 2,
        }
        serializer = self.serializer_class(
            data=product_order_composite_detail_data,
            context={'product_order': product_order})
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.product, product)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)


class TransactionOrderSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = TransactionOrderSerializer
        product = mommy.make('catalogue.Product', sku='PRODUCT-001', account=account)
        product_2 = mommy.make('catalogue.Product', sku='PRODUCT-002', account=account)
        payment_type = mommy.make('sales.PaymentMethod', name='Cash')
        payment_type_2 = mommy.make('sales.PaymentMethod', name='Card')
        promo = mommy.make('promo.Promo', name='Promo Attack', account=account)
        customer = mommy.make('customer.Customer', name='CUSTOMER-001', account=account)

        product_orders = [{
            'product': product.pk,
            'quantity': 1,
            'base_cost': product.cost.amount,
            'sub_total': product.cost.amount,
            'total_cost': product.cost.amount,
        }, {
            'product': product_2.pk,
            'quantity': 1,
            'base_cost': product_2.cost.amount,
            'sub_total': product_2.cost.amount,
            'total_cost': product_2.cost.amount,
        }]
        payment_details = [{
            'payment_method': payment_type.pk,
            'amount': product.cost.amount,
            'amount_used': product.cost.amount,
        }, {
            'payment_method': payment_type_2.pk,
            'amount': product_2.cost.amount,
            'amount_used': product_2.cost.amount,
        }]
        promo_list = []
        promo_list = [{
            "affected_value": 43378.0,
            "base_value": 54222.0,
            "discount_fixed_price": 0.0,
            "discount_percentage": 20.0,
            "discount_type": "percentage",
            "name": "Promo Attack",
            "promo": promo.pk,
            "promo_type": "Discount"
        }]

        self.transaction_order_dummy = {
            'code': 'SALES-001',
            'product_order': product_orders,
            'payment_detail': payment_details,
            'promo_list': promo_list,
            'transaction_date': timezone.now(),
            'sub_total': product.cost.amount + product_2.cost.amount,
            'total_cost': product.cost.amount + product_2.cost.amount,
            'tendered_amount': product.cost.amount + product_2.cost.amount,
            'change': 0,
            'customer': customer.pk,
        }

    def tearDown(self):
        set_current_tenant(None)

    def get_dummy_object(self):

        # voucher_event = (amount=0)
        vouchers = voucher_event.generate_voucher(count=2)

        vouchers_dict = [{
            'name': vouchers[0].voucher_event.name,
            'amount': vouchers[0].voucher_event.amount.amount,
            'type': vouchers[0].voucher_event.voucher_type,
            'base_value': 60000,
            'affected_value': 60000,
            'code': vouchers[0].code,
        }, {
            'name': vouchers[1].voucher_event.name,
            'amount': vouchers[1].voucher_event.amount.amount,
            'type': vouchers[1].voucher_event.voucher_type,
            'base_value': 60000,
            'affected_value': 60000,
            'code': vouchers[1].code,
        }]
        promo_list = [{
            'name': "promo-1",
            'amount': 0,
            'type': vouchers[1].voucher_event.voucher_type,
            'base_value': 60000,
            'affected_value': 60000,
        }]

        self.product_order_dummy.update({
            'product': {
                "name": product.name,
                "code": product.code,
                "cost": product.cost,
                "price": product.price,
                "uom": {
                    'label': 'uom-product'
                },
                "product_type": {
                    'name': 'product_type-product'
                },
            },
        })
        self.product_order_dummy2.update({
            'product': {
                "name": product2.name,
                "code": product2.code,
                "cost": product2.cost,
                "price": product2.price,
                "uom": {
                    'label': 'uom-product'
                },
                "product_type": {
                    'name': 'product_type-product'
                },
            },
        })

        self.payment_detail_dummy.update({
            'payment_type': {
                "name": payment_type.name,
            }
        })
        self.payment_detail_dummy2.update({
            'payment_type': {
                "name": payment_type2.name,
            }
        })

        product_order = [
            self.product_order_dummy,
            self.product_order_dummy2,
        ]
        payment_detail = [
            self.payment_detail_dummy,
            self.payment_detail_dummy2,
        ]

        self.transaction_order_dummy.update({
            'product_order': product_order,
            'payment_detail': payment_detail,
            'vouchers': vouchers_dict,
            'promo_list': promo_list,
        })

        return self.transaction_order_dummy

    def test_create_object(self):
        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.items.count(), 2)
        self.assertEqual(obj.payment_detail.count(), 2)
        self.assertEqual(obj.promo_list.count(), 1)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)

    def test_cancel_transaction(self):
        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.items.count(), 2)
        self.assertEqual(obj.payment_detail.count(), 2)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)

        self.transaction_order_dummy.update({
            'status': 'unsettled',
        })
        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj_2 = serializer.save()
        self.assertEqual(obj.items.count(), 2)
        self.assertEqual(obj.payment_detail.count(), 2)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertEqual(obj.pk, obj_2.pk)
        self.assertEqual(obj.status, 'unpaid')
        self.assertEqual(obj_2.status, 'unsettled')

    def test_create_object_with_some_deleted_products(self):
        product = Product.objects.get(sku='PRODUCT-001')
        product.delete()
        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.items.count(), 2)
        self.assertEqual(obj.payment_detail.count(), 2)
        self.assertEqual(obj.promo_list.count(), 1)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)

    def test_promo_history_validate(self):
        product = Product.objects.get(sku='PRODUCT-001')
        promo_2 = mommy.make(
            'promo.Promo', name='Promo Attack 2', account=get_current_tenant())
        promo_2.productpromo_set.create(
            product=product,
            account=promo_2.account)

        self.transaction_order_dummy['promo_list'].append({
            "affected_value": 43378.0,
            "base_value": 54222.0,
            "discount_fixed_price": 0.0,
            "discount_percentage": 20.0,
            "discount_type": "percentage",
            "name": "Promo Attack 2",
            "promo": promo_2.pk,
            "promo_type": "Discount",
            "affected_product": product.pk,
        })
        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertFalse(serializer.is_valid(), serializer.errors)

    def test_promo_history_with_affected_product(self):
        product = Product.objects.get(sku='PRODUCT-001')
        promo_2 = mommy.make(
            'promo.Promo', name='Promo Attack 2', account=get_current_tenant())
        promo_2.productpromo_set.create(
            product=product,
            account=promo_2.account)

        self.transaction_order_dummy['promo_list'].append({
            "affected_value": 43378.0,
            "base_value": 54222.0,
            "discount_fixed_price": 0.0,
            "discount_percentage": 20.0,
            "discount_type": "percentage",
            "name": "Promo Attack 2",
            "promo": promo_2.pk,
            "promo_type": "Discount",
            "affected_product": product.pk,
            "product_quantity": 2,
        })
        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.promo_list.count(), 2)

    def test_saving_object_twice_does_not_increment_m2m(self):
        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.items.count(), 2)
        self.assertEqual(obj.payment_detail.count(), 2)
        self.assertEqual(obj.promo_list.count(), 1)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        obj = serializer.save()
        self.assertEqual(obj.items.count(), 2)
        self.assertEqual(obj.payment_detail.count(), 2)
        self.assertEqual(obj.promo_list.count(), 1)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)

    def test_negative_guest_number_converted_to_zero(self):
        self.transaction_order_dummy.update({
            "guest_number": -1,
        })
        serializer = self.serializer_class(
            data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.guest_number, 1)

    def test_transaction_foc_with_reason(self):
        current_account = get_current_tenant()
        reason = current_account.transactiontypereason_set.first()
        transaction_type_data = {
            'name': 'FOC Test',
            'transaction_type': TransactionType.TRANSACTION_TYPE.foc,
            'reason': reason.pk,
        }
        self.transaction_order_dummy.update({
            'transaction_type': transaction_type_data,
        })

        serializer = self.serializer_class(data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.transaction_type.transaction_type, 'foc')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)

    def test_transaction_with_invoice(self):
        invoice_data = {
            "code": "INV2021021703p45V9lrrv", 
            "date": timezone.now(), 
            "due_date": timezone.now(),
            "customer": self.transaction_order_dummy.get("customer"),
            "notes": "transaction w/ invoice"
        }

        self.transaction_order_dummy.update({
            "invoice": invoice_data,
            "status": "unpaid",
            "payment_detail": [],
            "tendered_amount": 0,
        })
        serializer = self.serializer_class(
            data=self.transaction_order_dummy)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(
            obj.invoice.code, invoice_data.get("code"))


class SalesOrderSentReceiptSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        self.sales_order = mommy.make(
            'sales.SalesOrder',
            code="TRX-001",
            tendered_amount=3000,
            sub_total=2000,
            total_cost=2000,
            change=1000,
            account=account
        )
        self.serializer_class = SalesOrderSentReceiptSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        data = {
            'email': "email@mail.ru",
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid())
        serializer.save(sales_order=self.sales_order)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject,
            'Transaction {} Receipt Detail.'.format(
                self.sales_order.code))


class SalesOrderPromoHistorySerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = SalesOrderPromoHistorySerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        promo = mommy.make(
            'promo.Promo',
            name="Promo Test",
            promo_type="discount",
            promoreward__discount_type="percentage",
            promoreward__discount_percentage=50, promoreward__discount_amount_limit=100000)
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001')
        payment_method = mommy.make('sales.PaymentMethod', name='Cash')

        promo_detail_data = {
            "promo": promo.pk,
            "name": "Promo Test",
            "promo_type": "discount",
            "discount_type": "percentage",
            "discount_fixed_price": 0,
            "base_value": 300000,
            "affected_value": 150000,
         }
        serializer = self.serializer_class(
            data=promo_detail_data,
            context={'sales_order': sales_order})

        self.assertTrue(serializer.is_valid(), serializer.errors)
