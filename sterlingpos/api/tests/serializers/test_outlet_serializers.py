from model_mommy import mommy

from datetime import date, timedelta
from django.utils import timezone
from django.test import override_settings

from test_plus.test import TestCase

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.api.serializers.outlet_serializers import (
    OutletSerializer,
    OutletAuthTokenSerializer,
)


@override_settings(BYPASS_SUBSCRIPTION=False)
class OutletSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = OutletSerializer
        account = mommy.make('accounts.Account', name='Test')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test_serializers_is_expired(self):
        subscription = mommy.make(
            'subscription.Subscription',
            account__name='Test Account',
            outlet__name='Outlet-1',
            subscription_plan__name='Trial',
            expires=date.today() + timedelta(days=30),
            __fill_optional=True)
        serializer = self.serializer_class(instance=subscription.outlet)
        self.assertFalse(serializer.data.get('expired'))

    def test_create_outlet(self):
        data = {
            "branch_id": "0001",
            "name": "branch 1",
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        outlet = serializer.save()
        self.assertEqual(
            outlet.subscription.subscription_plan.slug,
            "mobile-lite")
        self.assertEqual(
            outlet.device_user.count(),
            1
        )

    def test_create_outlet_generate_branch(self):
        data = {
            "name": "branch 1",
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        outlet = serializer.save()
        self.assertEqual(
            len(outlet.branch_id), 8)
        self.assertEqual(
            outlet.subscription.subscription_plan.slug,
            "mobile-lite")
        self.assertEqual(
            outlet.device_user.count(),
            1
        )


class OutletAuthTokenSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = OutletAuthTokenSerializer

    def test_serializers_validation_active_outlet(self):
        subscription = mommy.make(
            'subscription.Subscription',
            account__name='Test Account',
            outlet__name='Outlet-1',
            outlet__branch_id='outlet-1',
            subscription_plan__name='Trial',
            expires=date.today() + timedelta(days=30),
            __fill_optional=True)
        serializer = self.serializer_class(data={'branch_id': 'outlet-1'})
        self.assertTrue(serializer.is_valid())

    def test_serializers_validation_inactive_outlet(self):
        subscription = mommy.make(
            'subscription.Subscription',
            account__name='Test Account',
            outlet__name='Outlet-1',
            outlet__branch_id='outlet-1',
            subscription_plan__name='Trial',
            expires=date.today() - timedelta(days=30),
            __fill_optional=True)
        serializer = self.serializer_class(data={'branch_id': 'outlet-1'})
        self.assertFalse(serializer.is_valid())
        self.assertIn('branch_id', serializer.errors)

    def test_serializers_validation_nonexistant_outlet(self):
        serializer = self.serializer_class(data={'branch_id': 'not-exists'})
        self.assertFalse(serializer.is_valid())
        self.assertIn('branch_id', serializer.errors)
