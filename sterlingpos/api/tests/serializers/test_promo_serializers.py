from model_mommy import mommy

from test_plus.test import TestCase

from sterlingpos.accounts.models import Account
from sterlingpos.outlet.models import Outlet
from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.promo.models import (
    Promo,
    PromoReward,
    OutletPromo,
    ProductPromo,
    CustomerPromo,
)

from sterlingpos.api.serializers.promo_serializers import (
    PromoRewardSerializer,
    PromoSerializer,
    PromoLimitCheckSerializer,
)


class PromoSerializerTest(TestCase):

    def setUp(self):
        self.payment_method = mommy.make('sales.PaymentMethod', name='Cash')
        account = mommy.make('accounts.Account', name='account-1')

        self.serializer_class = PromoSerializer
        self.outlet_1 = mommy.make(
            'outlet.Outlet', name='outlet-1', branch_id=1, account=account)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', name='outlet-2', branch_id=2, account=account)
        self.outlet_3 = mommy.make(
            'outlet.Outlet', name='outlet-3', branch_id=3, account=account)
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        data = {
            "promo_reward": {
                "discount_type": "percentage",
                "discount_percentage": "20.00",
            },
            "promo_type": "discount",
            "name": "Promo Test",
            "description": "",
            "is_active": "true",
            "start_date": "2018-10-23",
            "end_date": "2018-10-23",
            "start_hours": "00:00:00",
            "end_hours": "23:00:00",
            "allow_days": [
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7"
            ],
            "allow_multiple": "false",
            "outlet": [
                self.outlet_1.pk,
                self.outlet_2.pk
            ]
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.name, "Promo Test")
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertEqual(obj.outlet.count(), 2)
        self.assertEqual(obj.promoreward.discount_percentage, 20)

    def test_update_object(self):
        product = mommy.make(
            'catalogue.Product',
            sku='product-1', name='product-test',
            account=get_current_tenant(),
        )
        product_2 = mommy.make(
            'catalogue.Product',
            sku='product-2', name='product-test-2',
            account=get_current_tenant(),
        )
        product_3 = mommy.make(
            'catalogue.Product',
            sku='product-2', name='product-test-2',
            account=get_current_tenant(),
        )

        promo = mommy.make(
            'promo.Promo',
            name='Promo Test',
            promo_type="discount",
            allow_days=["1", "2", "3"],
            account=get_current_tenant(),
        )
        promo_reward = mommy.make(
            'promo.PromoReward',
            discount_type="percentage",
            discount_percentage="10.00",
            promo=promo,
            account=get_current_tenant(),
        )

        promo.productpromo_set.create(
            product=product_3,
            account=promo.account)

        data = {
            "promo_reward": {
                "discount_type": "percentage",
                "discount_percentage": "50.00",
            },
            "promo_type": "discount",
            "name": "Promo Test (UPDATED)",
            "affected_products": [
                product.pk,
                product_2.pk,
            ],
            "affected_payment_method": [
                self.payment_method.pk,
            ],
            "allow_days": [
                "5",
                "6",
                "7"
            ],
            "outlet": [
                self.outlet_1.pk,
            ]
        }
        serializer = self.serializer_class(
            data=data, instance=promo)

        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.name, "Promo Test (UPDATED)")
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertEqual(obj.outlet.count(), 1)
        self.assertEqual(obj.promoreward.discount_percentage, 50)
        self.assertEqual(obj.allow_days, ["5", "6", "7"])
        self.assertEqual(obj.affected_product.count(), 2)
        self.assertFalse(obj.affected_product.filter(pk=product_3.pk))
        self.assertEqual(obj.affected_payment_method.count(), 1)

    def test_clear_affected_product(self):
        product = mommy.make(
            'catalogue.Product',
            sku='product-2', name='product-test-2',
            account=get_current_tenant(),
        )

        promo = mommy.make(
            'promo.Promo',
            name='Promo Test',
            promo_type="discount",
            allow_days=["1", "2", "3"],
            account=get_current_tenant(),
        )
        promo_reward = mommy.make(
            'promo.PromoReward',
            discount_type="percentage",
            discount_percentage="10.00",
            promo=promo,
            account=get_current_tenant(),
        )

        promo.productpromo_set.create(
            product=product,
            account=promo.account)

        data = {
            "promo_reward": {
                "discount_type": "percentage",
                "discount_percentage": "50.00",
            },
            "promo_type": "discount",
            "name": "Promo Test (UPDATED)",
            "affected_products": [],
            "allow_days": [
                "5",
                "6",
                "7"
            ],
            "outlet": [
                self.outlet_1.pk,
            ]
        }
        serializer = self.serializer_class(
            data=data, instance=promo)

        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.affected_product.count(), 0)



class TestPromoLimitCheckSerializer(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.product = mommy.make(
            "catalogue.Product", sku='promo-product', account=self.account)
        self.outlet_1 = mommy.make(
            Outlet, branch_id='O1', account=self.account)
        self.outlet_2 = mommy.make(
            Outlet, branch_id='O2', account=self.account)
        self.customer = mommy.make(
            "customer.Customer", name="customer", account=self.account)
        
        self.serializer_class = PromoLimitCheckSerializer

    def tearDown(self):
        set_current_tenant(None)
    
    def test_validate_non_existing_promo(self):
        serializer = self.serializer_class(data={
            "promo": 0,
            "total_cost": 5000,
            "outlet": self.outlet_1.pk,
        })
        self.assertFalse(serializer.is_valid())
        self.assertIn("promo", serializer.errors)

    def test_promo_get_limitation_for_usages_limit(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="use_limit",
            promo_limit=10,
            account=self.account
        )
    
        serializer = self.serializer_class(data={
            "promo": self.promo.pk,
            "total_cost": 5000,
            "outlet": self.outlet_1.pk,
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        self.assertEqual(
            serializer.save(),
            {
                "count_usages": 0,
                "count_left": 10,
                "count_limit": 10,
                "is_usable": True,
            }
        )

    def test_promo_get_limitation_for_amount_limit(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="amount_limit",
            amount_limit=5000,
            account=self.account
        )

        serializer = self.serializer_class(data={
            "promo": self.promo.pk,
            "total_cost": 5000,
            "outlet": self.outlet_1.pk,
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        self.assertEqual(
            serializer.save(),
            {
                "amount_usages": 0,
                "amount_left": 5000,
                "amount_limit": 5000,
                "is_usable": True,
            }
        )

    def test_promo_get_limitation_for_product_amount_limit(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="amount_limit",
            amount_limit=5000,
            affected_product_type='product',
            account=self.account
        )
        ProductPromo.objects.create(
            promo=self.promo,
            product=self.product,
            account=self.account
        )

        serializer = self.serializer_class(data={
            "promo": self.promo.pk,
            "total_cost": 5000,
            "outlet": self.outlet_1.pk,
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        self.assertEqual(
            serializer.save(),
            {
                "amount_usages": 0,
                "amount_left": 5000,
                "amount_limit": 5000,
                "is_usable": True,
            }
        )

    def test_promo_get_limitation_for_amount_limit_with_limit_per_customer(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="amount_limit",
            amount_limit=5000,
            limit_per_customer=True,
            account=self.account
        )
        CustomerPromo.objects.create(
            promo=self.promo,
            customer=self.customer,
            account=self.account
        )
        sales_order = mommy.make(
            'sales.SalesOrder',
            tax=10,
            outlet=self.outlet_1,
            status="settled",
            account=self.account,
        )
        mommy.make(
            "sales.SalesOrderPromoHistory",
            sales_order=sales_order,
            promo=self.promo,
            name=self.promo.name,
            promo_type=self.promo.promo_type,
            discount_type="fixed_price",
            discount_fixed_price=2000,
            affected_value=2000,
            affected_customer=self.customer,
            account=self.account
        )

        serializer = self.serializer_class(data={
            "promo": self.promo.pk,
            "total_cost": 5000,
            "outlet": self.outlet_1.pk,
            "affected_customer": self.customer.pk,

        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        self.assertEqual(
            serializer.save(),
            {
                "amount_usages": 2000,
                "amount_left": 3000,
                "amount_limit": 5000,
                "is_usable": True,
            }
        )

        serializer = self.serializer_class(data={
            "promo": self.promo.pk,
            "total_cost": 5000,
            "outlet": self.outlet_1.pk,
            "affected_customer": None,

        })
        self.assertFalse(serializer.is_valid())
