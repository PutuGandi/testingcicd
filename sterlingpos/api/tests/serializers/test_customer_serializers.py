from model_mommy import mommy

from datetime import timedelta
from django.utils import timezone

from test_plus.test import TestCase
from sterlingpos.core.models import set_current_tenant

from sterlingpos.api.serializers.customers_serializer import (
    CustomerSerializer, CustomerCreateSerializer,
)

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


class CustomerSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = CustomerSerializer

        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_customer(self):
        data = {
            'name': "Customer 1",
        }

        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.validated_data)
        customer = serializer.save()
        self.assertEqual(customer.name, "Customer 1")
        self.assertIsNone(customer.group)
        self.assertIsNone(customer.organization)
        self.assertIsNone(customer.birth_date)
        self.assertIsNone(customer.handphone)

    def test_update_customer(self):
        customer = mommy.make(
            'customer.Customer',
            name='Customer 1', account=self.account)

        data = {
            'name': "Customer 1 (UPDATED)",
        }

        serializer = self.serializer_class(data=data, instance=customer)
        self.assertTrue(serializer.is_valid(), serializer.validated_data)
        customer = serializer.save()
        self.assertEqual(customer.name, "Customer 1 (UPDATED)")
        self.assertIsNone(customer.group)
        self.assertIsNone(customer.organization)
        self.assertIsNone(customer.birth_date)
        self.assertIsNone(customer.handphone)


class CustomerCreateSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = CustomerCreateSerializer
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_customer(self):
        data = {
            'name': "Customer 1",
        }

        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)

        customer = serializer.save()
        self.assertEqual(customer.name, "Customer 1")
        self.assertIsNone(customer.group)
        self.assertIsNone(customer.organization)
        self.assertIsNone(customer.birth_date)
        self.assertIsNone(customer.handphone)

    def test_handphone_is_exists(self):
        customer = mommy.make('customer.Customer', handphone='08123456789')

        data = {
            'name': "Customer 1",
            "email": "customer@mail.com",
            "handphone": "08123456789"
        }
        serializer = self.serializer_class(data=data)
        self.assertFalse(serializer.is_valid(), serializer.validated_data)

