from model_mommy import mommy

from datetime import timedelta
from django.utils import timezone

from test_plus.test import TestCase
from sterlingpos.core.models import set_current_tenant

from sterlingpos.api.serializers.table_management_serializers import (
    TableSerializer,
    TableAreaSerializer,
    TableAreaCreateSerializer,
    TableActivityLogSerializer,
)

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


class TableSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = TableSerializer
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_is_available(self):
        table = mommy.make(
            'table_management.Table',
            name='Table 1',
            account=self.account,
            outlet__name='Outlet-1',
            __fill_optional=True)
        serializer = self.serializer_class(instance=table)
        self.assertTrue(serializer.data.get('is_available'))
        self.assertIsNone(serializer.data.get('last_timer_expiration'))
        self.assertIsNone(serializer.data.get('last_timer_run'))

        mommy.make(
            'table_management.TableActivityLog',
            table=table,
            status='unavailable',
            activity_date=timezone.now() - timedelta(hours=1),
            account=self.account,
            __fill_optional=True)
        serializer = self.serializer_class(instance=table)
        self.assertFalse(serializer.data.get('is_available'))
        self.assertIsNotNone(serializer.data.get('last_timer_expiration'))
        self.assertIsNotNone(serializer.data.get('last_timer_run'))

        mommy.make(
            'table_management.TableActivityLog',
            table=table,
            status='available',
            account=self.account,
            __fill_optional=True)
        serializer = self.serializer_class(instance=table)
        self.assertTrue(serializer.data.get('is_available'))
        self.assertIsNone(serializer.data.get('last_timer_expiration'))
        self.assertIsNone(serializer.data.get('last_timer_run'))

        mommy.make(
            'table_management.TableActivityLog',
            table=table,
            status='unavailable',
            activity_date=timezone.now() - timedelta(hours=1),
            account=self.account,
            __fill_optional=True)
        serializer = self.serializer_class(instance=table)
        self.assertFalse(serializer.data.get('is_available'))
        self.assertIsNotNone(serializer.data.get('last_timer_expiration'))
        self.assertIsNotNone(serializer.data.get('last_timer_run'))

        mommy.make(
            'table_management.TableActivityLog',
            table=table,
            status='available',
            account=self.account,
            __fill_optional=True)
        serializer = self.serializer_class(instance=table)
        self.assertTrue(serializer.data.get('is_available'))
        self.assertIsNone(serializer.data.get('last_timer_expiration'))
        self.assertIsNone(serializer.data.get('last_timer_run'))


class TableAreaSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = TableAreaCreateSerializer
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.outlet = mommy.make('outlet.Outlet', name='Outlet 1', branch_id='branch-1')

    def tearDown(self):
        set_current_tenant(None)

    def test_create(self):
        data = {
            "name": "Lantai 3",
            "outlet_id": self.outlet.pk,
            "active": True,
            "table_list": [
                {"name": "Table 1", "pax": 1, "shape": 'square'},
                {"name": "Table 2", "pax": 1, "shape": 'square'},
                {"name": "Table 3", "pax": 1, "shape": 'square'}
            ]
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertTrue(obj.account, self.account)
        self.assertTrue(obj.table_set.count(), 3)


class TableActivityLogSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = TableActivityLogSerializer
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_validate(self):
        table = mommy.make(
            'table_management.Table',
            name='Table 1',
            account=self.account,
            outlet__name='Outlet-1',
            __fill_optional=True)

        data = {
            'table': table.pk,
            'table_name': table.name,
            'outlet_name': 'Outlet-1',
            'status': 'available',
            'activity_date': timezone.now(),
            'customer_count': 5,
        }

        serializer = self.serializer_class(data=data)
        self.assertFalse(serializer.is_valid(), serializer.data)
        self.assertIn('non_field_errors', serializer.errors, serializer.errors)

        data = {
            'table': table.pk,
            'table_name': table.name,
            'outlet_name': 'Outlet-1',
            'status': 'unavailable',
            'activity_date': timezone.now(),
            'customer_count': 5,
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()
        
        data = {
            'table': table.pk,
            'table_name': table.name,
            'outlet_name': 'Outlet-1',
            'status': 'available',
            'activity_date': timezone.now(),
            'customer_count': 5,
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()

        data = {
            'table': table.pk,
            'table_name': table.name,
            'outlet_name': 'Outlet-1',
            'status': 'available',
            'activity_date': timezone.now(),
            'customer_count': 5,
        }
        serializer = self.serializer_class(data=data)
        self.assertFalse(serializer.is_valid(), serializer.data)
        self.assertIn('non_field_errors', serializer.errors, serializer.errors)

        data = {
            'table': table.pk,
            'table_name': table.name,
            'outlet_name': 'Outlet-1',
            'status': 'unavailable',
            'activity_date': timezone.now(),
            'customer_count': 5,
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()

    def test_validate_for_deleted_table(self):
        table = mommy.make(
            'table_management.Table',
            name='Table 1',
            account=self.account,
            outlet__name='Outlet-1',
            __fill_optional=True)
        table.delete()

        data = {
            'table': table.pk,
            'table_name': table.name,
            'outlet_name': 'Outlet-1',
            'status': 'unavailable',
            'activity_date': timezone.now(),
            'customer_count': 5,
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()

        data = {
            'table': table.pk,
            'table_name': table.name,
            'outlet_name': 'Outlet-1',
            'status': 'available',
            'activity_date': timezone.now(),
            'customer_count': 5,
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()
