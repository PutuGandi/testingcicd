from model_mommy import mommy

from datetime import timedelta
from django.utils import timezone
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware

from test_plus.test import TestCase

from rest_framework.test import APIRequestFactory

from sterlingpos.core.models import set_current_tenant
from sterlingpos.users.models import User
from sterlingpos.api.serializers.users_serializers import (
    SignUpSerializer,
    EmailSerializer,
)

mommy.generators.add(
    "sterlingpos.core.models.SterlingTenantForeignKey",
    mommy.random_gen.gen_related)


class SignUpSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = SignUpSerializer

    def test_new_user_signup(self):
        province = mommy.make('province.Province', name='Province')
        city = mommy.make(
            'province.City', name='City', province=province)

        request = APIRequestFactory().post('/')
        SessionMiddleware().process_request(request)
        MessageMiddleware().process_request(request)

        data = {
            "account_name": "Account Name",
            "email": "account@mail.ru",
            "password1": "test-password",
 
            "name": "Outlet Mobile 1",
            "phone": "082124512322",
            "address": "Sesame Street 2nd",
            "province": province.pk,
            "city": city.pk,
            "taxes": 25,
            "gratuity": 10,
        }
        
        serializer = self.serializer_class(
            data=data, context={'request':request})
        self.assertTrue(serializer.is_valid())
        user = serializer.save()
        self.assertEqual(user.email, 'account@mail.ru')
        self.assertEqual(user.account.name, 'Account Name')
        self.assertEqual(user.account.registered_via, 'mobile_lite')
        self.assertEqual(user.account.outlet_set.count(), 1)
        self.assertEqual(
            user.account.outlet_set.first().name,'Outlet Mobile 1')
        self.assertEqual(
            user.account.outlet_set.first().taxes, 25)
        self.assertEqual(
            user.account.outlet_set.first().gratuity, 10)
        self.assertEqual(
            user.account.outlet_set.first().device_user.count(), 1)

        self.assertEqual(
            user.account.outlet_set.first().subscription.subscription_plan.slug,
            'mobile-lite')

    def test_signup_using_existing_email(self):
        account = mommy.make(
            'accounts.Account', name='Account Old')
        user = mommy.make(
            'users.User', email='account@mail.ru', account=account)

        province = mommy.make('province.Province', name='Province')
        city = mommy.make(
            'province.City', name='City', province=province)

        request = APIRequestFactory().post('/')
        SessionMiddleware().process_request(request)
        MessageMiddleware().process_request(request)

        data = {
            "account_name": "Account Name",
            "email": user.email,
            "password1": "test-password",

            "name": "Outlet Mobile 1",
            "phone": "082124512322",
            "address": "Sesame Street 2nd",
            "province": province.pk,
            "city": city.pk
        }
        
        serializer = self.serializer_class(
            data=data, context={'request':request})
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            'email', serializer.errors)


class EmailSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = EmailSerializer
        self.account = mommy.make("accounts.Account", name="Account")
        set_current_tenant(self.account)
        self.user = mommy.make(
            'users.User',
            email='admin@test.com',account=self.account)
        self.user_2 = mommy.make(
            'users.User',
            email='admin2@test.com',account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        request = APIRequestFactory().post('/')
        request.user = self.user
        SessionMiddleware().process_request(request)
        MessageMiddleware().process_request(request)

        data = {
            "email": "account@mail.ru",
        }
        
        serializer = self.serializer_class(
            data=data, context={'request':request})
        self.assertTrue(serializer.is_valid())
        
        email = serializer.save()
        self.assertTrue(
            self.user.emailaddress_set.filter(
                email="account@mail.ru").exists())

    def test_validate(self):
        request = APIRequestFactory().post('/')
        request.user = self.user
        SessionMiddleware().process_request(request)
        MessageMiddleware().process_request(request)

        data = {
            "email": "admin2@test.com",
        }

        serializer = self.serializer_class(
            data=data, context={'request':request})
        self.assertFalse(serializer.is_valid())
