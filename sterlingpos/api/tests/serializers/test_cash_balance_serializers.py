from model_mommy import mommy

from datetime import timedelta
from django.utils import timezone

from test_plus.test import TestCase
from sterlingpos.core.models import set_current_tenant

from sterlingpos.api.serializers.cash_balance_serializers import (
    CashBalanceLogSerializer,
)

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


class CashBalanceLogSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = CashBalanceLogSerializer
        self.start_date = timezone.now()
        self.end_date = self.start_date + timedelta(hours=4)

        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=self.account)
        self.device_user = self.account.deviceuser_set.first()

    def tearDown(self):
        set_current_tenant(None)

    def test_validate(self):

        data = {
            'device_user': self.device_user.pk,
            'outlet': self.outlet.pk,
            'transaction_date': self.start_date,
            'status': 'cash_in',
            'balance': 500,
            'description': "Cash In",
        }

        serializer = self.serializer_class(data=data)
        self.assertFalse(serializer.is_valid(), serializer.data)
        self.assertIn('non_field_errors', serializer.errors, serializer.errors)

        data = {
            'device_user': self.device_user.pk,
            'outlet': self.outlet.pk,
            'transaction_date': self.start_date,
            'status': 'shift_start',
            'balance': 500,
            'description': "Open Shift",
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()

        data = {
            'device_user': self.device_user.pk,
            'outlet': self.outlet.pk,
            'transaction_date': self.start_date + timedelta(hours=1),
            'status': 'cash_in',
            'balance': 500,
            'description': "Cash In",
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()

        data = {
            'device_user': self.device_user.pk,
            'outlet': self.outlet.pk,
            'transaction_date': self.start_date + timedelta(hours=1),
            'status': 'shift_start',
            'balance': 500,
            'description': "Open New Shift w/o closing existing one.",
        }
        serializer = self.serializer_class(data=data)
        self.assertFalse(serializer.is_valid(), serializer.data)
        self.assertIn('non_field_errors', serializer.errors, serializer.errors)

        data = {
            'device_user': self.device_user.pk,
            'outlet': self.outlet.pk,
            'transaction_date': self.end_date,
            'status': 'shift_end',
            'balance': 500,
            'description': "Closing Shift.",
            'balance_list': [
                {
                    "balance": 100,
                    "payment_name": "cash",
                },
                {
                    "balance": 400,
                    "payment_name": "ovo",
                }
            ]
        }
        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.balance_list.count(), 2)
