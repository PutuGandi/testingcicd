import datetime

from model_mommy import mommy

from datetime import timedelta
from django.utils import timezone
from django.test import override_settings

from test_plus.test import TestCase

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.subscription.models import Subscription
from sterlingpos.api.serializers.subscription_serializers import (
    SubscriptionSerializer,
    GooglePlaySubscriptionLogSerializer,

    BillingSerializer,
)


@override_settings(BYPASS_SUBSCRIPTION=False)
class SubscriptionSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = SubscriptionSerializer

    def test_subscription_plan_field(self):
        subscription = mommy.make(
            'subscription.Subscription',
            account__name='Test Account',
            outlet__name='Outlet-1',
            subscription_plan__name='Trial',
            __fill_optional=True)
        serializer = self.serializer_class(instance=subscription)
        self.assertEqual(serializer.data.get('subscription_plan'), 'Trial')


@override_settings(BYPASS_SUBSCRIPTION=False)
class BillingSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = BillingSerializer
        self.plan = mommy.make(
            "subscription.SubscriptionPlan",
            slug="device-user-lite",
            plan_type="device_user_subscription",
            recurrence_period=1,
            price=100000,
            recurrence_unit='M')
        self.account = mommy.make('accounts.Account', name='Account')
        set_current_tenant(self.account)
        self.device_user = mommy.make(
            'device.DeviceUser',
            name='Device User',
            username="device_user",)
        self.user = mommy.make(
            'users.User', email='admin@test.com', is_staff=True)
        self.user.set_password('password')
        self.user.save()

    def tearDown(self):
        set_current_tenant(None)

    def test_create_billing(self):
        data = {
            "subscription_detail":[{
                "device_user": self.device_user.pk,
                "choosen_plan": self.plan.pk,
            },]
        }

        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)

        billing = serializer.save()
        self.assertEqual(
            billing.due_date,
            datetime.date.today() + datetime.timedelta(days=7)) 
        self.assertEqual(billing.price, 100000)
        self.assertEqual(
            billing.subscriptionjournal_set.first().price,
            100000)

    def test_create_billing_of_other_subscription(self):
        other_plan = mommy.make(
            "subscription.SubscriptionPlan",
            slug="lite-test-other-plan",
            plan_type="others",
            recurrence_period=0,
            price=10000,)

        data = {
            "subscription_detail":[{
                "choosen_plan": other_plan.pk,
            },]
        }

        serializer = self.serializer_class(data=data)
        self.assertTrue(serializer.is_valid(), serializer.errors)

        billing = serializer.save()
        self.assertEqual(
            billing.due_date,
            datetime.date.today() + datetime.timedelta(days=7)) 
        self.assertEqual(billing.price, 10000)
        self.assertEqual(
            billing.subscriptionjournal_set.first().price,
            10000)
        self.assertEqual(
            self.account.othersubscription_set.filter(
                subscription_plan__slug="lite-test-other-plan").count(), 1
        )
        self.assertFalse(
            self.account.othersubscription_set.filter(
                subscription_plan__slug="lite-test-other-plan", active=True).exists()
        )

        billing.status = "settled"
        billing.save()
        self.assertTrue(
            self.account.othersubscription_set.filter(
                subscription_plan__slug="lite-test-other-plan", active=True).exists()
        )
