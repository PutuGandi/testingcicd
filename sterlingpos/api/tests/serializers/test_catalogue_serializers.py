import io
import unittest

from PIL import Image
from model_mommy import mommy
from django.core.files.uploadedfile import UploadedFile
from test_plus.test import TestCase

from sterlingpos.api.serializers.catalogue_serializers import (
    ProductSerializer,
    CompositeProductSerializer,
    CategorySerializer,
    ProductTypeSerializer,
    UOMSerializer,
)

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.catalogue.categories import create_from_sequence, create_from_breadcrumbs

from sterlingpos.catalogue.models import Category, Product


class ProductSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.category = create_from_breadcrumbs(
            "lorem", account=account)
        self.serializer_class = ProductSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        image_file = io.BytesIO()
        image = Image.new(
            'RGBA', size=(50, 50), color=(256, 0, 0))
        image.save(image_file, 'png')
        image_file.seek(0)
        uploaded_image = UploadedFile(
            image_file,
            "product-image.png",
            "image/png", len(image_file.getvalue()))

        product_data = {
            'sku': 'product-test',
            'name': 'Product Test',
            'cost': 5000,
            'price': 10000,
            'image_location': uploaded_image,
        }

        serializer = self.serializer_class(data=product_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.sku, 'product-test')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertIn("products", obj.image_location.url)
        image_file.close()

    def test_update_object(self):
        product = mommy.make(
            "catalogue.Product",
            sku="product-test",
            name="Product Test",
        )
        image_file = io.BytesIO()
        image = Image.new(
            'RGBA', size=(50, 50), color=(256, 0, 0))
        image.save(image_file, 'png')
        image_file.seek(0)
        uploaded_image = UploadedFile(
            image_file,
            "product-image.png",
            "image/png", len(image_file.getvalue()))

        product_data = {
            'sku': 'product-test-update',
            'name': 'Product Test (UPDATED)',
            'cost': 5000,
            'price': 10000,
            'image_location': uploaded_image,
        }

        serializer = self.serializer_class(
            data=product_data, instance=product)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.sku, 'product-test-update')
        self.assertEqual(obj.name, 'Product Test (UPDATED)')
        self.assertIn("products", obj.image_location.url)
        image_file.close()

    def test_passing_category_path(self):

        product_data = {
            'sku': 'product-test',
            'name': 'Product Test',
            'cost': 5000,
            'price': 10000,
            'category': self.category.pk,
        }

        serializer = self.serializer_class(data=product_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.category.full_name, 'lorem')

    def test_having_product_type_dict_passed(self):
        product_data = {
            'sku': 'product-test',
            'name': 'Product Test',
            'cost': 5000,
            'price': 10000,
            "product_type": {
                'name': 'product_type-product'
            },
        }
        serializer = self.serializer_class(data=product_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual('product_type-product', obj.product_type.name)

    @unittest.skip("product.parent is deprecated.")
    def test_having_product_parent_dict_passed(self):
        product_parent = mommy.make(
            'catalogue.Product',
            sku='parent-product-code', name='parent-product-test')
        product_data = {
            'sku': 'product-test',
            'name': 'Product Test',
            'cost': 5000,
            'price': 10000,
            'parent': product_parent.pk,
        }
        serializer = self.serializer_class(data=product_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.parent.name, 'parent-product-test')

    @unittest.skip("product.parent is deprecated.")
    def test_having_parent_while_owning_variant_will_raise_error(self):

        product_parent = mommy.make(
            'catalogue.Product',
            sku='product-parent', parent=None, account=get_current_tenant())
        product_parent_2 = mommy.make(
            'catalogue.Product',
            sku='product-parent-2', parent=None, account=get_current_tenant())
        product_var1 = mommy.make(
            'catalogue.Product',
            sku='product-var', parent=product_parent, account=get_current_tenant())

        product_data = {
            'parent': product_parent_2.pk,
        }

        serializer = self.serializer_class(
            data=product_data,
            instance=product_parent)

        self.assertFalse(serializer.is_valid(), serializer.errors)
        self.assertIn('parent', serializer.errors)

    def test_having_composite_product_list_passed(self):
        product_dummy_2 = mommy.make(
            'catalogue.Product',
            sku='dummy-2', parent=None)
        product_dummy_3 = mommy.make(
            'catalogue.Product',
            sku='dummy-3', parent=None)
        composed_product = mommy.make(
            'catalogue.Product',
            sku='composite-1',
            parent=None,
            classification=Product.PRODUCT_CLASSIFICATION.composite)

        composite_product_list = [{
            'product': product_dummy_2.sku,
        }, {
            'product': product_dummy_3.sku,
        }]
        product_data = {
            'sku': composed_product.sku,
            'name': composed_product.name,
            'cost': 5000,
            'price': 10000,
            'composite_material': composite_product_list,
        }
        serializer = self.serializer_class(
            data=product_data, instance=composed_product)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.composite_material.count(), 2)
        self.assertEqual(len(serializer.data['composite_material']), 2)


class CompositeProductSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = CompositeProductSerializer
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        product_dummy = mommy.make(
            'catalogue.Product',
            sku='dummy-2', parent=None)
        composite_product = mommy.make(
            'catalogue.Product',
            sku='composite-1',
            parent=None,
            classification=Product.PRODUCT_CLASSIFICATION.composite)
        composed_product_data = {
            'product': product_dummy.sku,
            'composite_product': composite_product.sku,
            'product_quantity': 2,
        }

        serializer = self.serializer_class(data=composed_product_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.product.sku, product_dummy.sku)
        self.assertEqual(obj.composite_product.sku, composite_product.sku)
        self.assertEqual(obj.product_quantity, 2)
        self.assertEqual(obj.account.pk, get_current_tenant().pk)

    def test_create_composite_product_from_missing_product(self):
        composite_product = mommy.make(
            'catalogue.Product',
            sku='composite-1',
            parent=None,
            classification=Product.PRODUCT_CLASSIFICATION.composite)

        composite_product_dict = {
            'composite_product': composite_product.sku,
            'product': "bad-product-code",
        }
        serializer = self.serializer_class(data=composite_product_dict)
        self.assertFalse(serializer.is_valid())

    def test_update_existing_composite_product(self):
        product_dummy = mommy.make(
            'catalogue.Product',
            sku='dummy', parent=None)
        product_dummy_2 = mommy.make(
            'catalogue.Product',
            sku='dummy-2', parent=None)
        composite_product = mommy.make(
            'catalogue.Product',
            sku='composite-1',
            parent=None,
            classification=Product.PRODUCT_CLASSIFICATION.composite)

        composite_product_dict = {
            'composite_product': composite_product.sku,
            'product': product_dummy.sku,
        }
        serializer = self.serializer_class(data=composite_product_dict)
        self.assertTrue(serializer.is_valid())
        obj = serializer.save()

        composite_product_dict_2 = {
            'composite_product': composite_product.sku,
            'product': product_dummy_2.sku,
        }
        serializer = self.serializer_class(
            data=composite_product_dict_2, instance=composite_product)
        self.assertTrue(serializer.is_valid())
        obj = serializer.save()
        self.assertEqual(
            obj.product.sku, product_dummy_2.sku)

    def test_create_composite_product_with_composite_product_as_composite_material(self):
        product_dummy = mommy.make(
            'catalogue.Product',
            sku='composite-1',
            parent=None, classification=Product.PRODUCT_CLASSIFICATION.composite)
        product_dummy_2 = mommy.make(
            'catalogue.Product',
            sku='composite-2',
            parent=None, classification=Product.PRODUCT_CLASSIFICATION.composite)

        composite_product_dict = {
            'composite_product': product_dummy.sku,
            'product': product_dummy_2.sku,
        }
        serializer = self.serializer_class(data=composite_product_dict)
        self.assertFalse(serializer.is_valid())

    def test_create_composite_product_with_non_composite_product_as_main_product(self):
        product_dummy = mommy.make(
            'catalogue.Product',
            sku='non-composite-1', parent=None)
        product_dummy_2 = mommy.make(
            'catalogue.Product',
            sku='non-composite-2', parent=None)

        composite_product_dict = {
            'composite_product': product_dummy.sku,
            'product': product_dummy_2.sku,
        }
        serializer = self.serializer_class(data=composite_product_dict)
        self.assertFalse(serializer.is_valid())


class CategorySerializerTest(TestCase):

    def setUp(self):
        self.category_serializer = CategorySerializer
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test_can_create_category_from_path(self):
        data = {
            "path": u"lorem > ipsum > dolor",
        }
        category_create = self.category_serializer(data=data)
        self.assertTrue(category_create.is_valid(), category_create.errors)
        obj = category_create.save()
        self.assertEqual(obj.name, 'dolor')
        self.assertTrue(
            Category.objects.filter(
                name__in=data['path'].split(' > ')).exists())

    def test_can_update_existing_category(self):
        category = create_from_breadcrumbs("lorem")
        data = {
            "path": "lorem ipsum",
        }
        category_create = self.category_serializer(
            data=data, instance=category)
        self.assertTrue(
            category_create.is_valid(), category_create.errors)
        obj = category_create.save()
        self.assertEqual(obj.name, 'lorem ipsum')

    def test_can_create_archived_category_from_path(self):
        data = {
            "path": u"lorem > ipsum > dolor",
            "archived": "True"
        }
        category_create = self.category_serializer(data=data)
        self.assertTrue(category_create.is_valid(), category_create.errors)
        obj = category_create.save()
        self.assertEqual(obj.name, 'dolor')
        self.assertTrue(
            Category.objects.filter(
                archived=True,
                name__in=data['path'].split(' > ')).exists())


class ProductTypeSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = ProductTypeSerializer
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        product_type_data = {
            'name': 'Appetizer'
        }
        serializer = self.serializer_class(data=product_type_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.name, 'Appetizer')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)


class UOMSerializerTest(TestCase):

    def setUp(self):
        self.serializer_class = UOMSerializer
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        uom_data = {
            'unit': 'kg',
            'ratio': 1.0
        }
        serializer = self.serializer_class(data=uom_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.unit, 'kg')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
