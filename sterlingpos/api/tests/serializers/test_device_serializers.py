from model_mommy import mommy

from test_plus.test import TestCase

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.api.serializers.device_serializers import (
    DeviceUserPermissionSerializer,
    DeviceUserSerializer,
    FCMDeviceSerializer,
)


class FCMDeviceSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        self.device_user = mommy.make(
            'device.DeviceUser', name='account-1', account=account)
        set_current_tenant(account)
        self.serializer_class = FCMDeviceSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        device_data = {
            'registered_id': 'REGISTERED-ID-001',
            'name': 'device-outlet-a',
            'last_user_login': self.device_user.pk,
        }
        serializer = self.serializer_class(data=device_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.name, 'device-outlet-a')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)


class DeviceUserPermissionSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = DeviceUserPermissionSerializer

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        device_user_permission = {
            'name': 'administrator',
        }
        serializer = self.serializer_class(data=device_user_permission)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.name, 'administrator')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)


class DeviceUserSerializerTest(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='account-1')
        set_current_tenant(account)
        self.serializer_class = DeviceUserSerializer
        self.outlet_1 = mommy.make('outlet.Outlet', name='outlet-1', branch_id=1)
        self.outlet_2 = mommy.make('outlet.Outlet', name='outlet-2', branch_id=2)
        self.outlet_3 = mommy.make('outlet.Outlet', name='outlet-3', branch_id=3)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_object(self):
        device_user_data = {
            'username': 'device.user',
            'name': 'Device User',
            'email': 'admin@administrator.com',
            'pin': 12345,
            'outlet': [
                self.outlet_1.pk,
                self.outlet_2.pk,
                self.outlet_3.pk,
            ]
        }
        serializer = self.serializer_class(data=device_user_data, context={
            'account': get_current_tenant()
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.username, 'device.user')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertEqual(obj.permission, 'staff')

    def test_revoke_outlet_object(self):
        device_user_data = {
            'username': 'device.user',
            'name': 'Device User',
            'email': 'admin@administrator.com',
            'pin': 12345,
            'outlet': [
                self.outlet_1.pk,
                self.outlet_2.pk,
                self.outlet_3.pk,
            ]
        }
        serializer = self.serializer_class(data=device_user_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.username, 'device.user')
        self.assertEqual(obj.account.pk, get_current_tenant().pk)
        self.assertEqual(obj.outlet.count(), 3)

        device_user_data = {
            'username': 'device.user',
            'name': 'Device User',
            'email': 'admin@administrator.com',
            'pin': 12345,
            'outlet': [
                self.outlet_1.pk,
            ]
        }
        serializer = self.serializer_class(data=device_user_data, instance=obj)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        obj = serializer.save()
        self.assertEqual(obj.outlet.count(), 1)
