from django.test import TestCase

from model_mommy import mommy
from django_multitenant.models import set_current_tenant, get_current_tenant

from sterlingpos.stocks.models import Stock, PurchaseOrder
from sterlingpos.api.serializers.stocks_serializers import (
    PurchaseOrderSerializer,
    PurchaseOrderCancelRemainingItemSerializers,
    ReceivedOrderSerializer,
    ReceivedOrderCompleteSerializer,
    ReceivedOrderDetailSerializer,
)


class BasePurchaseOrderTest(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.inventorysetting, _ = self.account.inventorysetting_set.get_or_create(
            account=self.account
        )

        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make(
            "outlet.Outlet", name="A", branch_id="A", account=self.account
        )
        self.supplier = mommy.make(
            "stocks.Supplier", account=self.account, name="Supplier"
        )

        self.product = mommy.make(
            "catalogue.product",
            account=self.account,
            is_manage_stock=True,
            uom=self.unit,
            name="test",
            sku="test-01",
        )
        self.stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )
        self.stock.set_balance(10, unit=self.unit)

        self.purchase_order = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-001",
        )
        self.detail_purchased = self.purchase_order.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

    def tearDown(self):
        set_current_tenant(None)


class TestPurchaseOrderSerializer(BasePurchaseOrderTest):
    def setUp(self):
        super().setUp()
        self.serializer_class = PurchaseOrderSerializer

    def test_create_object(self):
        params = {
            "purchased_stock": [
                {
                    "purchase_cost": "0",
                    "is_received": False,
                    "received_date": None,
                    "quantity": "10.000",
                    "discount_type": "fixed",
                    "discount": "0",
                    "stock": {"product": self.stock.product.pk},
                    "unit": self.unit.pk,
                }
            ],
            "status": "active",
            "code": "PO-001",
            "tax": "0",
            "discount_type": "fixed",
            "discount": "0",
            "date_of_purchase": "2021-01-29T07:34:01.281Z",
            "shipping_date": "2021-01-31T07:34:01.281Z",
            "date_of_arrival": None,
            "note": "string",
            "supplier": self.supplier.pk,
            "outlet": self.outlet.pk,
        }

        serializer = self.serializer_class(data=params)
        assert serializer.is_valid()
        assert not serializer.errors
        instance = serializer.save()
        assert instance.code == params["code"]
        assert instance.purchased_stock.count() == 1
        assert instance.account.pk == get_current_tenant().pk

    def test_update_object(self):
        params = {
            "purchased_stock": [
                {
                    "id": self.detail_purchased.pk,
                    "purchase_cost": self.detail_purchased.purchase_cost,
                    "is_received": self.detail_purchased.is_received,
                    "received_date": self.detail_purchased.received_date,
                    "quantity": "100.000",
                    "discount_type": self.detail_purchased.discount_type,
                    "discount": self.detail_purchased.discount,
                    "stock": {"product": self.detail_purchased.stock.product.pk},
                    "unit": self.detail_purchased.unit.pk,
                }
            ],
            "id": self.purchase_order.pk,
            "status": self.purchase_order.status,
            "code": self.purchase_order.code,
            "tax": self.purchase_order.tax,
            "discount_type": self.purchase_order.discount_type,
            "discount": self.purchase_order.discount,
            "date_of_purchase": self.purchase_order.date_of_purchase,
            "shipping_date": self.purchase_order.shipping_date,
            "date_of_arrival": None,
            "note": "Test Notes",
            "supplier": self.purchase_order.supplier.pk,
            "outlet": self.purchase_order.outlet.pk,
        }

        serializer = self.serializer_class(data=params, instance=self.purchase_order)
        assert serializer.is_valid()
        assert not serializer.errors
        instance = serializer.save()
        assert instance.modified
        assert instance.note == params["note"]
        assert instance.purchased_stock.first().quantity == 100


class TestPurchaseOrderCancelRemainingItemSerializers(BasePurchaseOrderTest):
    def setUp(self):
        super().setUp()
        self.serializer_class = PurchaseOrderCancelRemainingItemSerializers

    def test_cancel(self):
        params = {"id": self.purchase_order.pk}
        serializer = self.serializer_class(data=params, instance=self.purchase_order)
        assert serializer.is_valid()
        instance = serializer.save()
        assert instance.status == PurchaseOrder.STATUS.closed


class BaseReceivedOrderTest(BasePurchaseOrderTest):
    def setUp(self):
        super().setUp()
        self.purchase_order_2 = mommy.make(
            "stocks.PurchaseOrder",
            account=self.account,
            outlet=self.outlet,
            supplier=self.supplier,
            code="PO-002",
        )
        self.detail_purchased_2 = self.purchase_order_2.purchase_product_stock(
            self.product, 10, self.unit, 1000, "percentage", 5
        )

        self.received_order = mommy.make(
            "stocks.PurchaseOrderReceive",
            account=self.account,
            purchase_order=self.purchase_order_2,
            receipt_number="12345",
        )
        self.detail_received = self.received_order.receive_purchase(
            self.stock, 10, self.unit
        )


class TestReceivedOrderSerializer(BaseReceivedOrderTest):
    def setUp(self):
        super().setUp()
        self.serializer_class = ReceivedOrderSerializer

    def test_create_object(self):
        purchase_order = PurchaseOrderSerializer(instance=self.purchase_order)
        params = {
            "purchase_order": purchase_order.data,
            "receipt_number": "1231515",
            "code": f"RO-{self.purchase_order.code}-1231515",
            "received_detail": [
                {
                    "stock": {"product": self.stock.product.pk},
                    "quantity": 10,
                    "purchase_cost": 10000,
                    "purchase_cost_currency": "IDR",
                    "discount_type": "percentage",
                    "discount": 5,
                    "unit": self.unit.pk,
                }
            ],
        }

        serializer = self.serializer_class(data=params)
        assert serializer.is_valid()
        assert not serializer.errors
        instance = serializer.save()
        assert instance.purchase_order.code == self.purchase_order.code
        assert instance.receipt_number == params["receipt_number"]

    def test_update_object(self):
        purchase_order = PurchaseOrderSerializer(instance=self.purchase_order_2)
        params = {
            "purchase_order": purchase_order.data,
            "receipt_number": "666",
            "code": f"RO-{self.purchase_order_2.code}-666",
            "received_detail": [
                {
                    "stock": {"product": self.stock.product.pk},
                    "quantity": 10,
                    "purchase_cost": 10000,
                    "purchase_cost_currency": "IDR",
                    "discount_type": "percentage",
                    "discount": 5,
                    "unit": self.unit.pk,
                }
            ],
        }
        serializer = self.serializer_class(data=params, instance=self.received_order)
        assert serializer.is_valid()
        assert not serializer.errors
        instance = serializer.save()
        assert instance.purchase_order.code == self.purchase_order_2.code
        assert instance.receipt_number == params["receipt_number"]
        assert instance.code == params["code"]


class TestReceivedOrderCompleteSerializer(BaseReceivedOrderTest):
    def setUp(self):
        super().setUp()
        self.serializer_class = ReceivedOrderCompleteSerializer

    def test_complete(self):
        params = {"id": self.received_order.pk}

        serializer = self.serializer_class(data=params, instance=self.received_order)
        assert serializer.is_valid()
        instance = serializer.save()
        assert instance.received


class TestReceivedOrderDetailSerializer(BaseReceivedOrderTest):
    def setUp(self):
        super().setUp()
        self.serializer_class = ReceivedOrderDetailSerializer

    def test_update_object(self):
        params = {
            "id": self.detail_received.pk,
            "purchase_cost": "1500",
            "quantity": "666",
            "discount_type": "fixed",
            "discount": "300",
            "received": self.detail_received.received.pk,
            "stock": {"product": self.detail_received.stock.product.pk},
            "unit": self.detail_received.unit.pk,
        }

        serializer = self.serializer_class(data=params, instance=self.detail_received)
        assert serializer.is_valid()
        instance = serializer.save()
        assert instance.received == self.received_order
