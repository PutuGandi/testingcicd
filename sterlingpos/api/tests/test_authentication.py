from django.urls import reverse

from test_plus.test import TestCase
from model_mommy import mommy

from rest_framework_jwt import utils
from rest_framework_jwt.settings import api_settings

from rest_framework.test import APIClient
from rest_framework import status


class UtilsTests(TestCase):

    def setUp(self):
        account = mommy.make('accounts.Account', name='Test Account')
        self.outlet = mommy.make('outlet.Outlet', branch_id='branch-1', account=account)
        self.user = mommy.make('users.User', email='test@testament.com', account=account)
        self.user.set_password('password')
        self.user.save()
        self.jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER

    def test_jwt_payload_handler_no_outlet(self):
        payload = self.jwt_payload_handler(self.user)
        self.assertTrue(isinstance(payload, dict))
        self.assertNotIn('outlet', payload)
        self.assertEqual(payload['user_id'], self.user.pk)
        self.assertEqual(payload['email'], self.user.email)
        self.assertTrue('exp' in payload)

    def test_jwt_payload_handler_with_outlet(self):
        payload = self.jwt_payload_handler(self.user, self.outlet)
        self.assertTrue(isinstance(payload, dict))
        self.assertIn('outlet', payload)
        self.assertEqual(payload['user_id'], self.user.pk)
        self.assertEqual(payload['outlet'], self.outlet.branch_id)
        self.assertEqual(payload['email'], self.user.email)
        self.assertTrue('exp' in payload)


class MultiTenantJSONWebTokenAuthenticationTests(TestCase):
    """JSON Web Token Authentication"""

    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)
        account = mommy.make('accounts.Account', name='Test Account')
        self.outlet = mommy.make('outlet.Outlet', branch_id='branch-1', account=account)
        self.user = mommy.make('users.User', email='test@testament.com', account=account)
        self.user.set_password('password')
        self.user.save()

    def test_post_jwt_auth_non_outlet(self):
        payload = api_settings.JWT_PAYLOAD_HANDLER(self.user)
        token = api_settings.JWT_ENCODE_HANDLER(payload)
        auth = 'JWT {0}'.format(token)
        url = reverse('v2:outlet_list')
        response = self.csrf_client.get(url, HTTP_AUTHORIZATION=auth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Ensure `authenticate` returned the decoded payload.
        self.assertEqual(response.wsgi_request.user, self.user)
        payload = response.wsgi_request.auth
        self.assertIsInstance(payload, dict)
        self.assertEqual(set(payload.keys()), {
            'user_id', 'username', 'exp', 'email'})

    def test_jwt_payload_handler_with_outlet(self):
        payload = api_settings.JWT_PAYLOAD_HANDLER(self.user, self.outlet)
        token = api_settings.JWT_ENCODE_HANDLER(payload)
        auth = 'JWT {0}'.format(token)
        url = reverse('v2:outlet_list')
        response = self.csrf_client.get(url, HTTP_AUTHORIZATION=auth)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Ensure `authenticate` returned the decoded payload.
        self.assertEqual(response.wsgi_request.user, self.user)
        payload = response.wsgi_request.auth
        self.assertIsInstance(payload, dict)
        self.assertEqual(set(payload.keys()), {
            'user_id', 'username', 'exp', 'email', 'outlet'})


class MultiTenantPrivateAppTokenAuthenticationTests(TestCase):

    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)
        account = mommy.make('accounts.Account', name='Test Account')
        self.user = mommy.make('users.User', email='test@testament.com', account=account)
        self.user_2 = mommy.make('users.User', email='test2@testament.com', account=account)
        self.outlet_1 = mommy.make('outlet.Outlet', branch_id='branch-account-1', account=account)
        self.outlet_2 = mommy.make('outlet.Outlet', branch_id='branch-account-2', account=account)

        account.owners.create(user=self.user, account=account)
        account.owners.create(user=self.user_2, account=account)

        account_2 = mommy.make('accounts.Account', name='Test Account')
        self.outlet_3 = mommy.make('outlet.Outlet', branch_id='branch-account-3', account=account_2)

        self.private_app = mommy.make(
            'private_apps.PrivateApp',
            name="Test App",
            description="Test App",
            key='testappkey',
            credential='testappkeycredential',
            account=account)

    def test_post_private_key_auth(self):
        auth = 'TOKEN {}:{}'.format(self.private_app.key, self.private_app.credential)
        url = reverse('v2:outlet_list')

        response = self.csrf_client.get(url, HTTP_AUTHORIZATION=auth)
        results = response.data.get('results')
        ids = map(lambda x: x['id'], results)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.wsgi_request.user, self.user)
        token = response.wsgi_request.auth
        self.assertEqual(
            token, '{}:{}'.format(self.private_app.key, self.private_app.credential))
        self.assertEqual(response.data['count'], 2)
        self.assertNotIn(self.outlet_3.pk, ids)
