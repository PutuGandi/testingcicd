from django.apps import AppConfig


class APIConfig(AppConfig):
    name = "sterlingpos.api"
    verbose_name = "APIs"
