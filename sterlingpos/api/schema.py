from drf_yasg.generators import OpenAPISchemaGenerator


class SterlingposOpenAPISchemaGenerator(OpenAPISchemaGenerator):

    def get_operation(self, view, path, prefix, method, components, request):
        view_class = view.__class__
        parameters = self.get_path_parameters(path, view_class)
        operation = super(SterlingposOpenAPISchemaGenerator, self).get_operation(
            view, path, prefix, method, components, request)
        operation.parameters += parameters
        return operation
