from django_filters import rest_framework as filters


class ModifiedFilter(filters.FilterSet):
    modified = filters.DateTimeFilter(
        field_name="modified", lookup_expr='gte')
    deleted = filters.DateTimeFilter(
        field_name="deleted", lookup_expr='gte', method='filter_deleted')

    def filter_deleted(self, queryset, name, value):
        queryset = queryset.all(force_visibility=12)
        lookup = '__'.join([name, 'gte'])
        queryset = queryset.filter(**{lookup: value})
        return queryset


class IsSellableFilter(ModifiedFilter):
    is_sellable = filters.BooleanFilter(field_name="is_sellable")
