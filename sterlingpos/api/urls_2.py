from django.conf import settings
from django.conf.urls import url

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny

from . import views

schema_view_v2 = get_schema_view(
    openapi.Info(
        title="KAWN API",
        default_version='v2',
        description="API for KAWN Pos",
        terms_of_service="https://kawn.co.id/privacy-policy/",
        contact=openapi.Contact(email="admin@kawn.co.id"),
    ),
    public=True,
    permission_classes=(AllowAny,),
)

app_name = 'v2'
urlpatterns = [
    # URL pattern for the Catalogue API

    url(
        r'^swagger(?P<format>\.json|\.yaml)$',
        schema_view_v2.without_ui(cache_timeout=None), name='schema-json'),

    url(
        r'^$',
        schema_view_v2.without_ui(cache_timeout=12 * 60 * 60), name='schema-json'),
    url(
        regex=r'^variant/$',
        view=views.CreateVariantProductView.as_view(),
        name='variant_create'
    ),

    url(
        regex=r'^product/$',
        view=views.ProductCreateAndListView.as_view(),
        name='product_create'
    ),
    url(
        regex=r'^product/$',
        view=views.ProductCreateAndListView.as_view(),
        name='product_list'
    ),
    url(
        regex=r'^product/(?P<pk>\d+)/$',
        view=views.ProductRetrieveUpdateDestroyView.as_view(),
        name='product_update'
    ),
    url(
        regex=r'^product/(?P<pk>\d+)/$',
        view=views.ProductRetrieveUpdateDestroyView.as_view(),
        name='product_delete'
    ),
    url(
        regex=r'^product/(?P<pk>\d+)/$',
        view=views.ProductRetrieveUpdateDestroyView.as_view(),
        name='product_detail'
    ),

    url(
        regex=r'^product/meta/(?P<pk>\d+)/$',
        view=views.CatalogueRetrieveUpdateDestroyView.as_view(),
        name='catalogue_update'
    ),

    url(
        regex=r'^product/composite/$',
        view=views.ProductCompositeListView.as_view(),
        name='product_composite_list'
    ),
    url(
        regex=r'^product/complementer/$',
        view=views.ProductComplementerListView.as_view(),
        name='product_complementer_list'
    ),
    url(
        regex=r'^product/modifier/$',
        view=views.ProductModifierListView.as_view(),
        name='product_modifier_list'
    ),

    url(
        regex=r'^catalogue/(?P<pk>\d+)/$',
        view=views.CatalogueRetrieveUpdateDestroyView.as_view(),
        name='catalogue_detail'
    ),

    url(
        regex=r'^category/$',
        view=views.CategoryCreateAndListView.as_view(),
        name='category_list'
    ),
    url(
        regex=r'^category/$',
        view=views.CategoryCreateAndListView.as_view(),
        name='category_create'
    ),
    url(
        regex=r'^category/(?P<pk>\d+)/$',
        view=views.CategoryRetrieveUpdateDestroyView.as_view(),
        name='category_update'
    ),
    url(
        regex=r'^category/(?P<pk>\d+)/$',
        view=views.CategoryRetrieveUpdateDestroyView.as_view(),
        name='category_delete'
    ),


    url(
        regex=r'^product-type/$',
        view=views.ProductTypeListView.as_view(),
        name='product_type_list'
    ),

    url(
        regex=r'^uom/$',
        view=views.UOMListView.as_view(),
        name='uom_list'
    ),

    # URL patterns for customer API
    url(
        regex=r'^outlet/$',
        view=views.OutletListCreateView.as_view(),
        name='outlet_list'
    ),
    url(
        regex=r'^outlet/$',
        view=views.OutletListCreateView.as_view(),
        name='outlet_create'
    ),
    url(
        regex=r'^outlet/(?P<pk>\d+)/$',
        view=views.OutletRetrieveUpdateView.as_view(),
        name='outlet_detail'
    ),
    url(
        regex=r'^outlet/(?P<pk>\d+)/$',
        view=views.OutletRetrieveUpdateView.as_view(),
        name='outlet_update'
    ),
    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/products/$',
        view=views.OutletProductListView.as_view(),
        name='outlet_product'
    ),
    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/products/composite/$',
        view=views.OutletProductCompositeListView.as_view(),
        name='outlet_product_composite'
    ),
    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/products/complementer/$',
        view=views.OutletProductComplementerListView.as_view(),
        name='outlet_product_complementer'
    ),
    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/products/(?P<product_pk>\d+)/$',
        view=views.OutletProductStockView.as_view(),
        name='outlet_product_detail'
    ),
    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/products/(?P<product_pk>\d+)/detail/$',
        view=views.OutletProductDetailView.as_view(),
        name='outlet_product_item_detail'
    ),

    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/stocks/$',
        view=views.OutletProductStockListView.as_view(),
        name='outlet_product_stock_list'
    ),
    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/stocks/(?P<product_pk>\d+)/$',
        view=views.OutletProductStockView.as_view(),
        name='outlet_product_stock_detail'
    ),

    # URL pattern for sales API
    url(
        regex=r'^payment-method/$',
        view=views.PaymentMethodListView.as_view(),
        name='payment_method_list'
    ),
    url(
        regex=r'^sales-order-option/$',
        view=views.SalesOrderOptionListView.as_view(),
        name='sales_order_option_list'
    ),
    url(
        regex=r'^sales-order/$',
        view=views.SalesOrderListView.as_view(),
        name='sales_order_list'
    ),
    url(
        regex=r'^sales-order/(?P<code>[\w-]+)$',
        view=views.SalesOrderDetailView.as_view(),
        name='sales_order_detail'
    ),
    url(
        regex=r'^sales-order/(?P<pk>[\d-]+)/sent-receipt/$',
        view=views.SalesOrderSentReceiptView.as_view({
            "post": "sent_receipt"}),
        name='sales_order_sent_receipt'
    ),
    url(
        regex=r'^sales-order/branch/(?P<pk>\d+)/$',
        view=views.SalesOrderListView.as_view(),
        name='sales_order_list'
    ),
    url(
        regex=r'^transaction-order/create/$',
        view=views.TransactionOrderCreateView.as_view(),
        name='transaction_order_create'
    ),
    url(
        regex=r'^cash-balances/settings/$',
        view=views.CashBalanceSettingView.as_view(),
        name='cash_balance_setting'
    ),
    url(
        regex=r'^cash-balance/create/$',
        view=views.CashBalanceLogCreateView.as_view(),
        name='cash_balance_create'
    ),
    url(
        regex=r'^cash-balance/(?P<device_user_id>\d+)/$',
        view=views.CashBalanceDetailView.as_view(),
        name='cash_balance_detail'
    ),

    url(
        regex=r'^cashier-activity/create/$',
        view=views.CashierActivityCreateView.as_view(),
        name='cashier_activity_create'
    ),
    url(
        regex=r'^report-settings/$',
        view=views.SalesReportSettingView.as_view(),
        name='report_settings'
    ),

    # # FCM DEVICE VIEWS
    url(
        regex=r'^device/user/$',
        view=views.DeviceUserCreateListView.as_view(),
        name='device_user_list'
    ),
    url(
        regex=r'^device/user/$',
        view=views.DeviceUserCreateListView.as_view(),
        name='device_user_create'
    ),
    url(
        regex=r'^device/user/update/(?P<pk>\d+)/$',
        view=views.DeviceUserRetrieveUpdateDeleteView.as_view(),
        name='device_user_update'
    ),
    url(
        regex=r'^device/user/delete/(?P<pk>\d+)/$',
        view=views.DeviceUserRetrieveUpdateDeleteView.as_view(),
        name='device_user_delete'
    ),
    url(
        regex=r'^device/fcm/register/$',
        view=views.FCMDeviceCreateView.as_view(),
        name='fcm_device_create'
    ),
    url(
        regex=r'^device/fcm/(?P<pk>\d+)/$',
        view=views.FCMDeviceUpdateView.as_view(),
        name='fcm_device_update'
    ),
    url(
        regex=r'^device/fcm/unregister/(?P<pk>\d+)/$',
        view=views.FCMDeviceDeleteView.as_view(),
        name='fcm_device_delete'
    ),
    url(
        regex=r'^promo/$',
        view=views.PromoCreateListView.as_view(),
        name='promo_list'
    ),
    url(
        regex=r'^promo/$',
        view=views.PromoCreateListView.as_view(),
        name='promo_create'
    ),
    url(
        regex=r'^promo/(?P<pk>\d+)/$',
        view=views.PromoRetrieveUpdateDestroyView.as_view(),
        name='promo_update'
    ),
    url(
        regex=r'^promo/(?P<pk>\d+)/$',
        view=views.PromoRetrieveUpdateDestroyView.as_view(),
        name='promo_delete'
    ),

    url(
        regex=r'^customers/$',
        view=views.CustomerListView.as_view(),
        name='customer_list'
    ),
    url(
        regex=r'^customers/create/$',
        view=views.CustomerCreateView.as_view(),
        name='customer_create'
    ),
    url(
        regex=r'^customers/(?P<code>[\w-]+)/$',
        view=views.CustomerRetrieveUpdateDestroyView.as_view(),
        name='customer_detail'
    ),

    url(
        regex=r'^customers-group/$',
        view=views.CustomerGroupCreateListView.as_view(),
        name='customer_group_list'
    ),

    url(
        regex=r'^customers-group/$',
        view=views.CustomerGroupCreateListView.as_view(),
        name='customer_group_create'
    ),
    url(
        regex=r'^customers-organization/$',
        view=views.CustomerOrganizationCreateListView.as_view(),
        name='customer_organization_list'
    ),
    url(
        regex=r'^customers-organization/$',
        view=views.CustomerOrganizationCreateListView.as_view(),
        name='customer_organization_create'
    ),

    url(
        regex=r'^promo-tag/$',
        view=views.PromoTagCreateAndListView.as_view(),
        name='promo_tag_list'
    ),
    url(
        regex=r'^promo-tag/$',
        view=views.PromoTagCreateAndListView.as_view(),
        name='promo_tag_create'
    ),

    # TABLE MANAGEMENT VIEWS
    url(
        regex=r'^table-management/color-statuses/$',
        view=views.ColorStatusListView.as_view(),
        name='color_status_list'
    ),
    url(
        regex=r'^outlet/(?P<pk>\d+)/table-area/$',
        view=views.TableAreaListView.as_view(),
        name='table_area_list'
    ),
    url(
        regex=r'^table-area/(?P<pk>\d+)/table/$',
        view=views.TableListView.as_view(),
        name='table_list'
    ),
    url(
        regex=r'^table-area/(?P<pk>\d+)/layout/$',
        view=views.TableAreaDetailView.as_view(),
        name='table_area_detail'
    ),
    url(
        regex=r'^table-area/create/$',
        view=views.TableAreaCreateView.as_view(),
        name='table_area_create'
    ),
    url(
        regex=r'^table-area/(?P<pk>\d+)/update/$',
        view=views.TableAreaUpdateView.as_view(),
        name='table_area_update'
    ),
    url(
        regex=r'^table/create/$',
        view=views.TableCreateView.as_view(),
        name='table_create'
    ),
    url(
        regex=r'^table/bulk-create/$',
        view=views.TableBulkCreateView.as_view(),
        name='table_bulk_create'
    ),
    url(
        regex=r'^table/(?P<pk>\d+)/update/$',
        view=views.TableUpdateView.as_view(),
        name='table_update'
    ),
    url(
        regex=r'^table-area/(?P<pk>\d+)/delete/$',
        view=views.TableAreaDeleteView.as_view(),
        name='table_area_delete'
    ),
    url(
        regex=r'^table/(?P<pk>\d+)/delete/$',
        view=views.TableDeleteView.as_view(),
        name='table_delete'
    ),
    url(
        regex=r'^table-activity/create/$',
        view=views.TableActivityLogCreateView.as_view(),
        name='table_activity_log_create'
    ),
    url(
        regex=r'^table-managements/$',
        view=views.TableManagementView.as_view(),
        name='table_management'
    ),
    url(
        regex=r'^receipt/$',
        view=views.ReceiptListView.as_view(),
        name='receipt_list'
    ),
    url(
        regex=r'^outlet/(?P<pk>\d+)/receipt/$',
        view=views.ReceiptDetailView.as_view(),
        name='receipt_detail'
    ),
    url(
        regex=r'^outlet/(?P<outlet_pk>\d+)/device/user/$',
        view=views.DeviceUserCreateListView.as_view(),
        name='device_user_list'
    ),

    url(
        regex=r'^provinces/$',
        view=views.ProvinceListView.as_view(),
        name='province_list'
    ),
    url(
        regex=r'^provinces/(?P<pk>\d+)/cities/$',
        view=views.CityListView.as_view(),
        name='city_list'
    ),
    url(
        regex=r'^cities/$',
        view=views.CityListView.as_view(),
        name='city_list'
    ),

    url(
        regex=r'^billings/$',
        view=views.BillingListCreateView.as_view(),
        name='billing_list_create'
    ),
    url(
        regex=r'^billings/(?P<pk>\d+)/$',
        view=views.BillingRetrieveView.as_view(),
        name='billing_detail'
    ),
    url(
        regex=r'^billings/(?P<pk>\d+)/cancel/$',
        view=views.BillingCancelView.as_view(),
        name='billing_cancel'
    ),
    url(
        regex=r'^billings/(?P<pk>\d+)/token/$',
        view=views.MidtransTokenView.as_view(),
        name='billing_token'
    ),

    url(
        regex=r'^roles/$',
        view=views.RoleListView.as_view(),
        name='role_list'
    ),
    url(
        regex=r'^roles/settings/$',
        view=views.RoleSettingsView.as_view(),
        name='role_settings'
    ),

    url(
        regex=r'^subscription-plans/$',
        view=views.SubscriptionPlanListView.as_view(),
        name='subscription_plan_list'
    ),
    url(
        regex=r'^subscription/mobile-lite/$',
        view=views.GooglePlaySubscriptionLogCreateView.as_view(),
        name='google_play_subscription_log_create'
    ),

    url(
        regex=r'^surcharges/$',
        view=views.SurchargesListView.as_view(),
        name='surcharge_list'
    ),

    url(
        regex=r'^provinces/$',
        view=views.ProvinceListView.as_view(),
        name='province_list'
    ),
    url(
        regex=r'^provinces/(?P<pk>\d+)/cities/$',
        view=views.CityListView.as_view(),
        name='city_list'
    ),
    url(
        regex=r'^cities/$',
        view=views.CityListView.as_view(),
        name='city_list'
    ),
    url(
        regex=r'^emails/$',
        view=views.EmailListCreateView.as_view(),
        name='emails'
    ),
    url(
        regex=r'^emails/(?P<pk>\d+)/$',
        view=views.EmailRetrieveDeleteView.as_view(),
        name='emails'
    ),
    url(
        regex=r'^emails/(?P<pk>\d+)/set-primary/$',
        view=views.EmailViewSet.as_view({'post': 'set_primary'}),
        name='emails_set_primary'
    ),
    url(
        regex=r'^emails/(?P<pk>\d+)/send-verification/$',
        view=views.EmailViewSet.as_view({'post': 'send_verification'}),
        name='emails_send_verification'
    ),
    url(
        regex=r'^promo-limit/use/$',
        view=views.PromoUseCheckView.as_view(),
        name="promo_limit"
    ),

    url(
        regex=r'^promo-customer/(?P<pk>\d+)/limit/(?P<outlet_pk>\d+)/$',
        view=views.PromoByCustomerListView.as_view(),
        name="promo_customer"
    ),

    url(r'^login/$', views.LoginAccount.as_view(), name='rf_auth_login'),
    url(r'^signup/$', views.SignUpView.as_view(), name='user_signup'),

    url(
        r'^outlet/auth/$', views.OutletAuthentication.as_view(),
        name='rf_outlet_auth'),
    url(r'^password/change/$', views.ChangePassword.as_view(), name='password_change'),
    url(r'^password/reset/$', views.LostPassword.as_view(), name='password_reset'),

    url(r'^account/$', views.AccountDetail.as_view(), name='account_detail'),
]

if settings.DEBUG or settings.STAGING:
    urlpatterns += [
        url(
            r'^swagger/$',
            schema_view_v2.with_ui('swagger', cache_timeout=None),
            name='schema-swagger-ui'),
    ]
