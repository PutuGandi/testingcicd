import logging

from datetime import datetime

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, UpdateAPIView, ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from allauth.account.models import EmailAddress
from django_multitenant.utils import set_current_tenant

from sterlingpos.api.serializers.account_serializers import AccountSerializer
from sterlingpos.api.serializers.users_serializers import (
    AuthTokenSerializer,
    UserPasswordFormSerializer, LostPasswordFormSerializer,
    UserSerializer,
)
from sterlingpos.api.serializers.outlet_serializers import OutletAuthTokenSerializer

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

logger = logging.getLogger(__name__)


class LoginAccount(generics.GenericAPIView):
    """
    Login account
    """
    authentication_classes = ()
    permission_classes = (AllowAny, )
    serializer_class = AuthTokenSerializer

    def get_success_response(self):
        response = {
            'key': self.token,
            'email': self.user.email,
            'company_name': self.user.account.name,
            'company_id': self.user.account.pk
        }
        return response

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={
                'request': request,
            })

        set_current_tenant(None)

        if serializer.is_valid():
            self.user = serializer.validated_data.get('user')

            if not self.user.is_owner and "can_access_application" not in self.user.device_user.role.mobile_app_permission:
                return Response(
                    {"message": _("This account does not have permission to access POS application")},
                    status=status.HTTP_403_FORBIDDEN
                )

            payload = jwt_payload_handler(self.user)
            self.token = jwt_encode_handler(payload)

            # Update last login
            self.user.last_login = timezone.now()
            self.user.save()

            return Response(self.get_success_response())

        log_data = "Processing Login Failed for credential={} reason={}".format(
            serializer.data.get('email'),
            serializer.errors
        )
        logger.warning(log_data)
        serializer.is_valid(raise_exception=True)



class OutletAuthentication(generics.GenericAPIView):
    """
    Generate auth token used for certain outlet after login
    """
    serializer_class = OutletAuthTokenSerializer

    def get_success_response(self):
        response = {
            'key': self.token,
            'outlet': self.outlet.branch_id,
        }
        return response

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={
                'request': request,
            })
        if serializer.is_valid(raise_exception=True):
            self.outlet = serializer.validated_data.get('branch_id')

            payload = jwt_payload_handler(request.user, self.outlet)
            self.token = jwt_encode_handler(payload)

            return Response(self.get_success_response())


class LogoutAccount(APIView):
    """
    Logout account
    """
    permission_classes = (IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        token = request.auth
        token.delete()
        return Response({'status': 'success'})


class AccountDetail(generics.RetrieveAPIView):
    """
    Get Current Account Information based on token
    """
    serializer_class = AccountSerializer

    def get(self, request, *args, **kwargs):
        user = self.serializer_class(
            request.user.account, context={'request': request})
        other_active_subscriptions = request.user.account.othersubscription_set.filter(
                subscription_plan__plan_type="others",
                active=True).values_list("subscription_plan__slug", flat=True)

        data = {
            "account": user.data,
            "other_active_subscriptions": other_active_subscriptions,
            "lite_table_management": request.user.account.othersubscription_set.filter(
                subscription_plan__slug="lite-table-management", active=True).exists(),
            "lite_split_bill": request.user.account.othersubscription_set.filter(
                subscription_plan__slug="lite-split-bill", active=True).exists(),
        }

        return Response(data)


class ChangePassword(generics.GenericAPIView):
    serializer_class = UserPasswordFormSerializer

    def get_success_response(self):
        response = {'success': 'password has been changed'}
        return response

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(self.get_success_response())

        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LostPassword(generics.GenericAPIView):
    serializer_class = LostPasswordFormSerializer
    permission_classes = (AllowAny, )

    def get_success_response(self):
        response = {'success': _('An email confirmation regarding password reset has been sent, please check your email for confirmation')}
        return response

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(self.get_success_response())

        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)
