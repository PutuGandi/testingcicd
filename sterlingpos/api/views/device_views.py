from django.conf import settings
from django.core.mail import send_mail

from celery import shared_task

from rest_framework.generics import (
    ListAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from sterlingpos.accounts.models import Account
from sterlingpos.core.models import set_current_tenant
from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.api.serializers.device_serializers import (
    FCMDeviceSerializer,
    DeviceUserSerializer,
)


@shared_task(bind=True, max_retries=5)
def delayed_serializer_update(
        self, serializer_data, instance_pk, partial, account_pk):
    try:
        account = Account.objects.get(pk=account_pk)
        set_current_tenant(account)
        instance = FCMDeviceSerializer.Meta.model.objects.get(
            pk=instance_pk)
        serializer = FCMDeviceSerializer(
            instance, data=serializer_data, partial=partial)
        serializer.is_valid()
        serializer.save()
        set_current_tenant(None)
    except:
        self.retry(countdown= 2 ** self.request.retries)


class FCMDeviceCreateView(CreateAPIView):

    serializer_class = FCMDeviceSerializer

    def send_serializer_data(self, serializer, request):
        if not settings.STAGING:
            subject = 'KAWN-Prod FCMDeviceCreate 400 Bad Request data'
        else:
            subject = 'KAWN-Staging FCMDeviceCreate 400 Bad Request data'

        send_mail(
            subject,
            '{}\n\n{}'.format(serializer.errors, request.__dict__),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=['adhitya.nugroho@tech.indosterling.com'],
            fail_silently=True,
        )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            self.send_serializer_data(serializer, request)
        return super(FCMDeviceCreateView, self).create(
            request, *args, **kwargs)


class FCMDeviceDeleteView(DestroyAPIView):

    serializer_class = FCMDeviceSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class FCMDeviceUpdateView(UpdateAPIView):

    serializer_class = FCMDeviceSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs
    
    def perform_update(self, serializer):
        delayed_serializer_update.delay(
            serializer.data, serializer.instance.pk,
            serializer.partial, self.request.user.account.pk
        )


class DeviceUserCreateListView(ListCreateAPIView):
    """
    List DeviceUser based on company of authenticated user
    """
    serializer_class = DeviceUserSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_serializer(self, *args, **kwargs):
        serializer = super(
            DeviceUserCreateListView, self).get_serializer(*args, **kwargs)
        serializer.context['table_management_setting'] = self.request.user.account.tablemanagementsettings_set.first()
        return serializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        if self.kwargs.get('outlet_pk'):
            qs = qs.filter(outlet__pk=self.kwargs.get('outlet_pk'))
        qs = qs.select_related(
        ).prefetch_related(
            'deviceuserlitesubscription',
            'outlet',
            'outlet__province',
            'outlet__city',
            'outlet__table_management_setting',
            'outlet__waiter_setting',
            'outlet__subscription',
            'outlet__subscription__subscription_plan',
            'outlet__device_user',
            'outlet__account',
            'outlet__account__tablemanagementsettings_set',
            'outlet__kitchendisplayoutlet',
        ).select_related('account')
        return qs


class DeviceUserRetrieveUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    """
    Update existing DeviceUser
    """
    serializer_class = DeviceUserSerializer

    def get_serializer(self, *args, **kwargs):
        serializer = super(
            DeviceUserRetrieveUpdateDeleteView, self).get_serializer(*args, **kwargs)
        serializer.context['table_management_setting'] = self.request.user.account.tablemanagementsettings_set.first()
        return serializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.prefetch_related(
            'deviceuserlitesubscription',            
            'outlet',
            'outlet__province',
            'outlet__city',
            'outlet__table_management_setting',
            'outlet__waiter_setting',
            'outlet__subscription',
            'outlet__subscription__subscription_plan',
            'outlet__device_user',
            'outlet__account',
            'outlet__account__tablemanagementsettings_set',
            'outlet__kitchendisplayoutlet',
        ).select_related('account')
        return qs
