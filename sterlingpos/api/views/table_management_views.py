from django.db.models import CharField, IntegerField, Value
from django.db.models.functions import Concat, Cast
from rest_framework.generics import (
    ListAPIView, RetrieveAPIView,
    CreateAPIView, UpdateAPIView, DestroyAPIView
)
from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.api.views.mixins import LoggingMixins
from sterlingpos.api.serializers.table_management_serializers import (
    TableSerializer,
    TableAreaSerializer,
    TableAreaCreateSerializer,
    TableAreaLayoutSerializer,
    TableActivityLogSerializer,
    ColorStatusSerializer,

    TableManagementSettingsSerializer,
)


class TableManagementView(RetrieveAPIView):
    serializer_class = TableManagementSettingsSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs

    def get_object(self, **kwargs):
        obj = self.request.user.account.tablemanagementsettings_set.first()
        return obj


class TableAreaListView(ListAPIView):
    """
    List Table Area based on Outlet
    """
    serializer_class = TableAreaSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.filter(
            outlet__pk=self.kwargs.get('pk'))
        return qs


class TableListView(ListAPIView):
    """
    List Table based on given Table Area
    """
    serializer_class = TableSerializer
    filterset_class = ModifiedFilter
    ordering = ('ord_name', )

    def get_queryset(self):
        table_prefix = self.request.user.account.tablemanagementsettings_set.first().prefix
        qs = self.serializer_class.Meta.model.objects.filter(
            table_area__pk=self.kwargs.get('pk')
        ).annotate(
            display_name=Concat(
                Value(table_prefix), 'name', 'suffix',
                output_field=CharField()),
            ord_name=Cast('name', IntegerField()),
        ).exclude(archived=True)

        name = self.request.query_params.get('name', None)
        shape = self.request.query_params.get('shape', None)
        pax = self.request.query_params.get('pax', None)

        if name:
            qs = qs.filter(display_name__icontains=name)
        if shape:
            qs = qs.filter(shape__icontains=shape)
        if pax:
            qs = qs.filter(pax=pax)

        return qs


class TableAreaDetailView(RetrieveAPIView):
    serializer_class = TableAreaLayoutSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.exclude(archived=True)
        return qs


class TableAreaCreateView(LoggingMixins, CreateAPIView):
    """
    Create New Table Area
    """
    serializer_class = TableAreaCreateSerializer


class TableAreaUpdateView(UpdateAPIView):
    """
    Update Existing Table Area
    """
    serializer_class = TableAreaSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class TableAreaDeleteView(DestroyAPIView):
    """
    Archieve Existing Table Area
    """
    serializer_class = TableAreaSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.exclude(archived=True)
        return qs

    def perform_destroy(self, instance):
        if not instance.table_set.exists():
            instance.delete()
        else:
            instance.archived = True
            instance.active = False
            instance.save()


class TableCreateView(LoggingMixins, CreateAPIView):
    """
    Create New Table
    """
    serializer_class = TableSerializer


class TableBulkCreateView(LoggingMixins, CreateAPIView):

    serializer_class = TableSerializer

    def get_serializer(self, *args, **kwargs):
        kwargs["many"] = True
        return super().get_serializer(*args, **kwargs)


class TableUpdateView(UpdateAPIView):
    """
    Update Existing Table
    """
    serializer_class = TableSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class TableDeleteView(DestroyAPIView):
    """
    Archieve Existing Table
    """
    serializer_class = TableSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs

    def perform_destroy(self, instance):
        if not instance.tableactivity_set.exists():
            instance.delete()
        else:
            instance.archived = True
            instance.active = False
            instance.save()


class TableActivityLogCreateView(LoggingMixins, CreateAPIView):
    """
    Create New Table Activity Log
    """
    serializer_class = TableActivityLogSerializer


class ColorStatusListView(ListAPIView):
    """
    List Table Area based on Outlet
    """
    serializer_class = ColorStatusSerializer
    ordering = ('-pk', )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs
