from rest_framework.generics import ListAPIView
from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.digital_payment.models import DigitalPaymentSettings
from sterlingpos.api.serializers.digital_payment_serializers import (
    DigitalPaymentSettingsSerializer,
)


class DigitalPaymentSettingsListView(ListAPIView):

    serializer_class = DigitalPaymentSettingsSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.filter(status=DigitalPaymentSettings.STATUS.completed)
        return qs
