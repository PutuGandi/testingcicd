import logging

from django.db import DataError, IntegrityError


from rest_framework import status
from rest_framework.response import Response

logger = logging.getLogger(__name__)


class LoggingMixins:

    def fetch_log_data(self):
        """
        Fetch needed data to be put on logger.
        """
        return f"{self.request.data}"

    @property
    def log_data(self):
        return self.fetch_log_data()

    def create(self, request, *args, **kwargs):
        log_data = f"Processing {self.__class__} Started {self.log_data}"
        
        logger.info(log_data)

        try:
            response = super().create(request, args, kwargs)

            log_data = f"Processing {self.__class__} Completed({response.status_code}) {self.log_data}"
            logger.info(log_data)
            return response
        except (DataError, IntegrityError) as e:
            log_data = f"Processing {self.__class__} Failed {self.log_data}, CAUSE : {e}"
            logger.warning(log_data)

            return Response(
                {
                    'success': False,
                    'description': _("Something went wrong, please try again.")
                },
                status=status.HTTP_400_BAD_REQUEST)
