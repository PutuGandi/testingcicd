from django.db.models import CharField, IntegerField, Value
from django.db.models.functions import Concat, Cast
from rest_framework.generics import (
    ListAPIView, RetrieveAPIView,
    CreateAPIView, UpdateAPIView, DestroyAPIView
)
from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.api.views.mixins import LoggingMixins
from sterlingpos.api.serializers.table_order_serializers import (
    TableOrderSettingsSerializer,
)


class TableOrderView(RetrieveAPIView):
    serializer_class = TableOrderSettingsSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs

    def get_object(self, **kwargs):
        obj = self.request.user.account.tableordersettings_set.first()
        return obj

