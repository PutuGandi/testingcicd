from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, ListAPIView

from sterlingpos.api.serializers.stocks_serializers import (
    PurchaseOrderSerializer,
    PurchaseOrderCancelRemainingItemSerializers,
    ReceivedOrderSerializer,
    ReceivedOrderCompleteSerializer,
    ReceivedOrderDetailSerializer,
    SupplierSerializer,
)


class SupplierListView(ListAPIView):
    serializer_class = SupplierSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.prefetch_related(
            "supplier_detail",
            "supplier_detail__unit",
            "supplier_detail__product",
            "supplier_detail__product__catalogue",
            "supplier_detail__product__category",
        )
        return qs


class PurchaseOrderCreateAndListView(ListCreateAPIView):
    serializer_class = PurchaseOrderSerializer
    ordering = "-id"

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.prefetch_related(
            "purchased_stock",
            "purchased_stock__stock",
            "purchased_stock__unit",
        )
        return qs


class PurchaseOrderUpdateView(RetrieveUpdateAPIView):
    serializer_class = PurchaseOrderSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.prefetch_related(
            "purchased_stock",
            "purchased_stock__stock",
            "purchased_stock__unit",
        )
        return qs


class PurchaseOrderCancelRemainingItemsView(PurchaseOrderUpdateView):
    serializer_class = PurchaseOrderCancelRemainingItemSerializers


class ReceivedOrderCreateAndListView(ListCreateAPIView):
    serializer_class = ReceivedOrderSerializer
    ordering = "-received_date"

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.select_related("purchase_order",).prefetch_related(
            "received_detail",
            "received_detail__unit",
            "received_detail__stock",
        )
        return qs


class ReceivedOrderUpdateView(RetrieveUpdateAPIView):
    serializer_class = ReceivedOrderSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.select_related("purchase_order",).prefetch_related(
            "received_detail",
            "received_detail__unit",
            "received_detail__stock",
        )
        return qs


class ReceivedOrderCompleteView(ReceivedOrderUpdateView):
    serializer_class = ReceivedOrderCompleteSerializer


class ReceivedOrderDetailUpdateView(RetrieveUpdateAPIView):
    serializer_class = ReceivedOrderDetailSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.filter(
            received=self.kwargs["received_order_pk"]
        )
        return qs
