from rest_framework.generics import (
    ListCreateAPIView, RetrieveUpdateAPIView,
)
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.api.serializers.outlet_serializers import (
    OutletSerializer, OutletReadOnlySerializer
)


class OutletListCreateView(ListCreateAPIView):
    """
    List Outlet based on account of authenticated user
    """
    serializer_class = OutletSerializer
    read_only_serializer_class = OutletReadOnlySerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_serializer_class(self):
        if self.request.method == "GET":
            return self.read_only_serializer_class
        return super().get_serializer_class()

    def get_serializer(self, *args, **kwargs):
        serializer = super(OutletListCreateView, self).get_serializer(*args, **kwargs)
        serializer.context['table_management_setting'] = self.request.user.account.tablemanagementsettings_set.first()
        return serializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.prefetch_related(
            'table_management_setting',
            'waiter_setting',
            'device_user',
            'account__tablemanagementsettings_set',
        ).select_related(
            'kitchendisplayoutlet',
            'subscription',
            'subscription__subscription_plan',
            'province', 'city', 'account')
        if not self.request.user.is_owner:
            qs = qs.filter(
                pk__in=get_outlet_of_user(
                    self.request.user))
        return qs


class OutletRetrieveUpdateView(RetrieveUpdateAPIView):
    """
    Detail Outlet based on given branch id
    """
    serializer_class = OutletSerializer
    read_only_serializer_class = OutletReadOnlySerializer
    lookup_field = 'pk'

    def get_serializer_class(self):
        if self.request.method == "GET":
            return self.read_only_serializer_class
        return super().get_serializer_class()

    def get_object(self):
        if self.request.version == 'v2.1':
            self.lookup_field = 'pk'
            self.lookup_url_kwarg = 'pk'
        return super(OutletRetrieveUpdateView, self).get_object()

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.prefetch_related(
            'table_management_setting',
            'device_user',
            'account__tablemanagementsettings_set',
        ).select_related(
            'subscription',
            'subscription__subscription_plan',
            'province', 'city', 'account')
        return qs
