from rest_framework.permissions import AllowAny
from rest_framework.generics import (
    ListAPIView,
)

from sterlingpos.api.serializers.province_serializers import (
    ProvinceSerializer,
    CitySerializer,
)


class ProvinceListView(ListAPIView):

    serializer_class = ProvinceSerializer
    permission_classes = (AllowAny, )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class CityListView(ListAPIView):

    serializer_class = CitySerializer
    permission_classes = (AllowAny, )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        province_pk = self.kwargs.get('pk')

        if province_pk:
            qs = qs.filter(province__pk=province_pk)

        return qs
