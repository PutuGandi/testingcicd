from rest_framework.generics import ListAPIView

from sterlingpos.api.serializers.surcharges_serializer import SurchargeSerializer
from sterlingpos.api.filters import ModifiedFilter

class SurchargesListView(ListAPIView):
    """
    List Surcharges based on Account
    """
    serializer_class = SurchargeSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs
