from rest_framework.generics import (
    ListAPIView, ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
    GenericAPIView,
)
from rest_framework import status
from rest_framework.response import Response


from sterlingpos.api.serializers.promo_serializers import (
    PromoSerializer,
    PromoTagSerializer,
    PromoLimitCheckSerializer,
)
from sterlingpos.api.filters import ModifiedFilter


class PromoCreateListView(ListCreateAPIView):

    serializer_class = PromoSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.select_related(
            'promoreward',
            'itempromoreward'
        ).prefetch_related(
            'outlet',
            'affected_product',
            'affected_customer',
            'affected_group',
            'affected_payment_method',
            'affected_category',
 
            'tags',
        )

        return qs


class PromoRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):

    serializer_class = PromoSerializer
    filterset_class = ModifiedFilter

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.select_related(
            'promoreward',
        ).prefetch_related(
            'outlet',
            'affected_product',
        )

        return qs


class PromoTagCreateAndListView(ListCreateAPIView):

    serializer_class = PromoTagSerializer
    filterset_class = ModifiedFilter

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class PromoLimitCheckView(GenericAPIView):
    serializer_class = PromoLimitCheckSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data,
            context={'request': request})
        if serializer.is_valid():
            response = serializer.save()
            return Response(response, status=status.HTTP_200_OK)
        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)
