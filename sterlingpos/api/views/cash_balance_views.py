from django.db.models import Prefetch, CharField, Value
from django.db.models.functions import Concat

from rest_framework.response import Response
from rest_framework.generics import (
    CreateAPIView, RetrieveAPIView,
)

from sterlingpos.api.serializers.cash_balance_serializers import (
    CashBalanceLogSerializer,
    CashBalanceSerializer,
    CashBalanceSettingSerializer,
)


class CashBalanceLogCreateView(CreateAPIView):
    """
    Create New Cash Balance Log
    """
    serializer_class = CashBalanceLogSerializer


class CashBalanceDetailView(RetrieveAPIView):
    """
    Detail of CashBalance
    """
    serializer_class = CashBalanceSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        return qs

    def get_object(self):
        qs = self.get_queryset()
        device_user_id = self.kwargs.get('device_user_id')
        obj = qs.filter(device_user__pk=device_user_id).last()
        return obj

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance:
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        else:
            return Response({'status': 'closed', 'detail': []})


class CashBalanceSettingView(RetrieveAPIView):
    """
    Detail of CashBalanceSetting
    """
    serializer_class = CashBalanceSettingSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs

    def get_object(self):
        qs = self.get_queryset()
        obj = qs.get(account=self.request.user.account)
        return obj

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance:
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        else:
            return Response({'status': 'closed', 'detail': []})
