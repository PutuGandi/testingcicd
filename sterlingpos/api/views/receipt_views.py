from rest_framework.generics import ListAPIView, RetrieveAPIView
from sterlingpos.api.serializers.receipt_serializers import ReceiptSerializer
from sterlingpos.api.filters import ModifiedFilter


class ReceiptListView(ListAPIView):

    serializer_class = ReceiptSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class ReceiptDetailView(RetrieveAPIView):

    serializer_class = ReceiptSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs

    def get_object(self):
        self.lookup_field = 'outlet__pk'
        self.lookup_url_kwarg = 'pk'
        return super(ReceiptDetailView, self).get_object()
