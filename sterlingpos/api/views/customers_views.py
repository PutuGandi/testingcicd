from rest_framework.generics import (
    ListAPIView, ListCreateAPIView,
    RetrieveUpdateDestroyAPIView, CreateAPIView
)

from rest_framework.response import Response
from sterlingpos.api.serializers.customers_serializer import (
    CustomerSerializer, CustomerGroupSerializer,
    CustomerOrganizationSerializer, CustomerCreateSerializer
)
from sterlingpos.api.filters import ModifiedFilter
from rest_framework import serializers


class CustomerListView(ListAPIView):
    serializer_class = CustomerSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class CustomerRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    serializer_class = CustomerSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs

    def get_object(self):
        qs = self.get_queryset()
        code = self.kwargs.get('code')
        obj = qs.filter(code=code).last()
        return obj

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance:
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        else:
            return Response({'status': 'closed', 'detail': []})


class CustomerGroupCreateListView(ListCreateAPIView):
    serializer_class = CustomerGroupSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class CustomerOrganizationCreateListView(ListCreateAPIView):
    serializer_class = CustomerOrganizationSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class CustomerCreateView(CreateAPIView):
    serializer_class = CustomerCreateSerializer
