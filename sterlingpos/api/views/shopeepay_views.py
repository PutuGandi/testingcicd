import logging
import rules

from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.shortcuts import get_object_or_404
from django.conf import settings

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.permissions import BasePermission, IsAuthenticated

from shopeepay import ShopeePay
from shopeepay.exceptions import ShopeePayAPIError

from sterlingpos.integration.shopeepay.models import ShopeePaySettings
from sterlingpos.api.serializers.shopeepay_serializers import (
    GetQRSerializer,
    InvalidateQRSerializer,
    PaymentCheckSerializer,
    PaymentRefundSerializer,
)

logger = logging.getLogger(__name__)


class HasShopeePayDigitalPayment(BasePermission):
    def has_permission(self, request, view):
        has_permission = rules.test_rule(
            'can_use_digital_payment', request.user.account, "shopeepay")
        return has_permission


class ShopeePayMixins:
    permission_classes = [
        IsAuthenticated & HasShopeePayDigitalPayment]


class ShopeePayGetQRCode(ShopeePayMixins, GenericAPIView):

    serializer_class = GetQRSerializer

    def post(self, request, format=None):
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        result = serializer.save()

        return Response(result)


class ShopeePayInvalidateQRCode(ShopeePayMixins, GenericAPIView):

    serializer_class = InvalidateQRSerializer

    def post(self, request, format=None):
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        result = serializer.save()

        return Response(result)


class ShopeePayCheckPayment(ShopeePayMixins, GenericAPIView):

    serializer_class = PaymentCheckSerializer

    def post(self, request, format=None):
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        result = serializer.save()

        return Response(result)


class ShopeePayRefundPayment(ShopeePayMixins, GenericAPIView):

    serializer_class = PaymentRefundSerializer

    def post(self, request, format=None):
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        result = serializer.save()
        
        return Response(result)


class ShopeePayAccountDetail(ShopeePayMixins, APIView):

    def get(self, request, format=None):
        instance = get_object_or_404(
            ShopeePaySettings,
            account=self.request.user.account
        )
        result = {
            "payment_detail": "shopeepay",
            "merchant_ext_id": instance.merchant_ext_id,
            "outlets": instance.store_ext_ids,
        }
        return Response(result)
