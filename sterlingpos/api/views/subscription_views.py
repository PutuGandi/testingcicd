from rest_framework.generics import (
    CreateAPIView,
    ListCreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    ListAPIView,
    DestroyAPIView,
)

from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.subscription.models import (
    SubscriptionPlan,
    Billing
)
from sterlingpos.api.serializers.subscription_serializers import (
    GooglePlaySubscriptionLogSerializer,
    SubscriptionPlanSerializer,
    MidtransBillingSerializer,
    BillingSerializer,
)


class GooglePlaySubscriptionLogCreateView(CreateAPIView):

    serializer_class = GooglePlaySubscriptionLogSerializer


class SubscriptionPlanListView(ListAPIView):

    serializer_class = SubscriptionPlanSerializer
    queryset = SubscriptionPlan.objects.filter(is_public=True)


class BillingListCreateView(ListCreateAPIView):

    serializer_class = BillingSerializer
    filterset_class = ModifiedFilter
    ordering = ("-pk", )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.prefetch_related(
            "subscriptionjournal_set")
        return qs


class BillingRetrieveView(RetrieveAPIView):

    serializer_class = BillingSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.prefetch_related(
            "subscriptionjournal_set")
        return qs


class BillingCancelView(DestroyAPIView):
    
    serializer_class = BillingSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.exclude(
            status__in=["canceled", "settled"]
        ).prefetch_related(
            "subscriptionjournal_set")
        return qs

    def perform_destroy(self, instance):
        instance.status = Billing.STATUS.canceled
        instance.save()


class MidtransTokenView(RetrieveAPIView):

    serializer_class = MidtransBillingSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.prefetch_related(
            "subscriptionjournal_set").filter(status="unpaid")
        return qs
