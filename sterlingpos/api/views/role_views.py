from rest_framework.generics import (
    ListAPIView, RetrieveAPIView
)

from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.api.serializers.role_serializers import (
    RoleSerializer,
    RoleSettingsSerializer,
)


class RoleListView(ListAPIView):

    serializer_class = RoleSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class RoleSettingsView(ListAPIView):

    serializer_class = RoleSettingsSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk', )

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs

