from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import viewsets
from rest_framework.generics import (
    ListCreateAPIView, RetrieveDestroyAPIView
)

from allauth.account import signals
from allauth.account.models import EmailAddress
from allauth.account.adapter import get_adapter

from sterlingpos.api.serializers.users_serializers import (
    SignUpSerializer,
    EmailSerializer,
)


class SignUpView(generics.GenericAPIView):
    serializer_class = SignUpSerializer
    permission_classes = (AllowAny, )

    def get_success_response(self):
        response = {'success': _('An email confirmation regarding user registration has been sent, please check your email for confirmation')}
        return response

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(self.get_success_response(), status=status.HTTP_201_CREATED)

        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EmailListCreateView(ListCreateAPIView):
    serializer_class = EmailSerializer

    def get_queryset(self):
        qs = self.request.user.emailaddress_set.all()
        return qs


class EmailRetrieveDeleteView(RetrieveDestroyAPIView):
    serializer_class = EmailSerializer

    def get_queryset(self):
        qs = self.request.user.emailaddress_set.all()
        return qs

    def perform_create(self):
        self.request.user.email

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.primary:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return super(
            EmailRetrieveDeleteView, self).destroy(
                request, *args, **kwargs   
            )


class EmailViewSet(viewsets.ModelViewSet):

    def get_queryset(self, *args, **kwargs):
        return self.request.user.emailaddress_set.all()

    @action(detail=False, methods=['post'])
    def set_primary(self, request, pk=None):
        email = self.get_object()
        if not email.verified and \
                EmailAddress.objects.filter(
                    user=request.user, verified=True).exists():
            return Response(
                {"status": _("Email must be verified.")},
                status=status.HTTP_400_BAD_REQUEST)
        try:
            from_email_address = EmailAddress.objects.get(
                user=request.user, primary=True)
        except EmailAddress.DoesNotExist:
            from_email_address = None

        email.set_as_primary()
        signals.email_changed.send(
            sender=request.user.__class__,
            request=request,
            user=request.user,
            from_email_address=from_email_address,
            to_email_address=email)
        return Response({"status": _("Email has been set as primary.")})

    @action(detail=False, methods=['post'])
    def send_verification(self, request, pk=None):
        email = self.get_object()
        email.send_confirmation(request)
        return Response({"status": _("Email verification has been sent.")})
