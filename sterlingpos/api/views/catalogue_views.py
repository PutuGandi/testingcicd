from django.http import Http404

from django.db.models import (
    Sum, OuterRef, Subquery, Prefetch, F, Q, Value,
    FilteredRelation,
)
from django.db.models.functions import Coalesce

from rest_framework.generics import (
    RetrieveAPIView, ListAPIView, ListCreateAPIView,
    CreateAPIView, UpdateAPIView, RetrieveUpdateDestroyAPIView,
)

from rest_framework.permissions import AllowAny
from rest_framework.mixins import (
    CreateModelMixin, UpdateModelMixin,
    DestroyModelMixin,
)

from sterlingpos.core.models import get_current_tenant, set_current_tenant
from sterlingpos.catalogue.models import (
    ProductPricing, ProductAvailability,
    ProductModifierOption,
)
from sterlingpos.stocks.models import Stock, StockTransaction

from sterlingpos.api.filters import ModifiedFilter, IsSellableFilter
from sterlingpos.api.pagination import SterlingPOSLimitOffsetPagination
from sterlingpos.api.serializers.catalogue_serializers import (
    CatalogueSerializer,
    ProductSerializer,
    OutletProductSerializer,
    OutletProductStockSerializer,
    ProductModifierSerializer,
    CategorySerializer,
    ProductTypeSerializer,
    UOMSerializer,
    VariantCreateSerializer,
    CatalogueUpdateSerializer,
    OutletProductListSerializer,
)
from sterlingpos.table_management.models import Table


class CatalogueRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    serializer_class = CatalogueUpdateSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class ProductRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.prefetch_related(
            'addon_usage',
            'addon',
            'composite_usage',
            'composite_material',
        ).select_related(
            'category',
            'parent',
            'parent__category',
        )
        return qs


class ProductCreateAndListView(ListCreateAPIView):
    """
    List product based on company of authenticated user
    """
    serializer_class = ProductSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_serializer(self, *args, **kwargs):
        serializer = super(
            ProductCreateAndListView, self).get_serializer(*args, **kwargs)
        qs = CategorySerializer.Meta.model.objects.all()
        cat_dict = {}
        for cat in qs:
            cat_dict.update({cat.path: cat})
        serializer.context['cached_categories'] = cat_dict
        return serializer

    def get_base_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.select_related(
            "category",
            "parent",
            "parent__category",
            "parent__catalogue",
            "parent__catalogue__category",

            "catalogue",
            "catalogue__category",
        ).prefetch_related(
            "parent__catalogue__variants",

            "catalogue__variants",

            "composite_material",
            "composite_material__product",
            "composite_usage",
            "composite_usage__product",

            "addon_usage",
            "addon_usage__product",
            "addon",
            "addon__product",
        )
        return qs

    def get_queryset(self):
        qs = self.get_base_queryset().exclude(
            classification__in=['Composite', 'Complementary']
        )
        return qs


class OutletProductListView(ListAPIView):
    """
    List product based on company of authenticated user
    """
    serializer_class = OutletProductListSerializer
    filterset_class = IsSellableFilter
    ordering = ('-pk',)
    pagination_class = SterlingPOSLimitOffsetPagination   

    def get_serializer(self, *args, **kwargs):
        serializer = super().get_serializer(*args, **kwargs)
        qs = CategorySerializer.Meta.model.objects.all()
        cat_dict = {}
        for cat in qs:
            cat_dict.update({cat.path: cat})
        serializer.context['cached_categories'] = cat_dict
        return serializer

    def get_base_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.select_related(
            "category",

            "catalogue",
            "catalogue__category",
            
        ).prefetch_related(
            "catalogue__variants",

            "composite_material",
            "composite_material__product",
            "composite_material__product_list",

            "composite_usage",
            "composite_usage__product",

            "addon_usage",
            "addon_usage__product",
            "addon",
            "addon__product",
        )
        if self.kwargs.get("outlet_pk"):
            qs = qs.prefetch_related(
                "availability",
                "product_pricing",
            ).annotate(
                outlet_price=Subquery(
                    ProductPricing.objects.filter(
                        product=OuterRef('pk'),
                        outlet__pk=self.kwargs.get("outlet_pk"),
                        is_active=True,
                    ).values('price')[:1]),
                is_available=Subquery(
                    ProductAvailability.objects.filter(
                        product=OuterRef('pk'),
                        outlet__pk=self.kwargs.get("outlet_pk")
                    ).values('is_active')[:1]),
            )
        return qs

    def get_queryset(self, base_queryset=None):

        if base_queryset is None:
            base_queryset = self.get_base_queryset()

        qs = base_queryset.exclude(
            classification__in=['Composite', 'Complementary']
        )
        return qs


class OutletProductCompositeListView(OutletProductListView):
    """
    List product composite based on company of authenticated user
    """

    def get_queryset(self, base_queryset=None):
    
        if base_queryset is None:
            base_queryset = self.get_base_queryset()

        qs = base_queryset.filter(
            classification='Composite'
        )
        return qs


class OutletProductDynamicCompositeListView(OutletProductListView):
    """
    List product composite based on company of authenticated user
    """

    def get_queryset(self, base_queryset=None):
        if base_queryset is None:
            base_queryset = self.get_base_queryset()

        qs = base_queryset.filter(
            classification='Dynamic-Composite'
        )
        return qs


class OutletProductComplementerListView(OutletProductListView):
    """
    List product composite based on company of authenticated user
    """

    def get_queryset(self, base_queryset=None):
        if base_queryset is None:
            base_queryset = self.get_base_queryset()

        qs = base_queryset.filter(
            classification='Complementary'
        )
        return qs


class OutletProductDetailView(RetrieveAPIView):
    serializer_class = OutletProductSerializer
    lookup_url_kwarg = "product_pk"

    def get_serializer(self, *args, **kwargs):
        serializer = super().get_serializer(*args, **kwargs)
        qs = CategorySerializer.Meta.model.all_objects.all()
        cat_dict = {}
        for cat in qs:
            cat_dict.update({cat.path: cat})
        serializer.context['cached_categories'] = cat_dict
        return serializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        qs = qs.prefetch_related(
            'catalogue__variants',
            'addon',
            'composite_usage',
            'composite_material',
            'availability',
            'product_pricing',
        ).select_related(
            'category',
            'catalogue',
        ).annotate(
            outlet_price=Subquery(
                ProductPricing.objects.filter(
                    product=OuterRef('pk'),
                    outlet__pk=self.kwargs.get("outlet_pk"),
                    is_active=True,
                ).values('price')[:1]),
            is_available=Subquery(
                ProductAvailability.objects.filter(
                    product=OuterRef('pk'),
                    outlet__pk=self.kwargs.get("outlet_pk")
                ).values('is_active')[:1])
        )
        return qs


class OutletProductStockQueryMixins:
   
    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.filter(
            is_manage_stock=True)
        qs = qs.annotate(
            filtered_stocks=FilteredRelation(
                "stocks",
                condition=Q(
                    stocks__outlet=self.kwargs.get("outlet_pk"))
            ),
            stock=Coalesce(
                Sum(
                    "filtered_stocks__transactions__quantity",
                ),
                Value(0)
            ),
        )
        return qs


class OutletProductStockListView(
        OutletProductStockQueryMixins, ListAPIView):
    serializer_class = OutletProductStockSerializer
    pagination_class = SterlingPOSLimitOffsetPagination
    ordering = ('-pk',)

    def paginate_queryset(self, queryset):

        counting_queryset = self.serializer_class.Meta.model.all_objects.filter(
            is_manage_stock=True)

        paginator = self.paginator
        paginator._counting_queryset = counting_queryset

        return super().paginate_queryset(queryset)


class OutletProductStockView(
        OutletProductStockQueryMixins, RetrieveAPIView):
    serializer_class = OutletProductStockSerializer
    lookup_url_kwarg = "product_pk"


class PublicOutletMenuMixin:
    """
    List product based on company of authenticated user
    """
    serializer_class = OutletProductSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)
    permission_classes = (AllowAny, )

    def dispatch(self, request, *args, **kwargs):
        if get_current_tenant():
            set_current_tenant(None)
        table = Table.get_from_base64string(
            kwargs.get("outlet_base64"))
        if table:
            set_current_tenant(table.account)
            kwargs["outlet_pk"] = table.table_area.outlet.pk
            response = super().dispatch(request, *args, **kwargs)
            set_current_tenant(None)
            return response
        else:
            raise Http404()


class PublicOutletProductListView(
        PublicOutletMenuMixin, OutletProductListView):
    pass
        

class PublicOutletProductCompositeListView(
        PublicOutletMenuMixin, OutletProductCompositeListView):
    pass


class PublicOutletProductDynamicCompositeListView(
        PublicOutletMenuMixin, OutletProductDynamicCompositeListView):
    pass


class PublicOutletProductComplementerListView(
        PublicOutletMenuMixin, OutletProductComplementerListView):
    pass


class ProductByClassListView(ProductCreateAndListView):
    """
    List product based on company of authenticated user
    """

    def get_queryset(self):
        classification = self.kwargs.get('classification', None)
        qs = self.get_base_queryset()
        if classification == 'standart':
            qs = qs.exclude(
                classification__in=['Composite', 'Complementary']
            )
        elif classification != 'all':
            qs = qs.filter(
                classification__icontains=classification
            )
        return qs


class ProductCompositeListView(ProductCreateAndListView):
    """
    List product composite based on company of authenticated user
    """

    def get_queryset(self):
        qs = self.get_base_queryset().filter(
            classification='Composite'
        ).select_related(
            "catalogue",
            "catalogue__category",
            "category",
        ).prefetch_related(
            "catalogue__variants",
            "composite_material",
            "composite_material__product",
            "composite_usage",
            "composite_usage__product",
        )
        return qs


class ProductDynamicCompositeListView(ProductCreateAndListView):
    """
    List product composite based on company of authenticated user
    """

    def get_queryset(self):
        qs = self.get_base_queryset().filter(
            classification='Dynamic-Composite'
        ).select_related(
            "catalogue",
            "catalogue__category",
            "category",
        ).prefetch_related(
            "catalogue__variants",
            "composite_material",
            "composite_material__product_list",
            "composite_usage",
            "composite_usage__product",
        )
        return qs


class ProductComplementerListView(ProductCreateAndListView):
    """
    List product complementer based on company of authenticated user
    """

    def get_queryset(self):
        qs = self.get_base_queryset().filter(
            classification='Complementary'
        ).select_related(
            "catalogue",
            "catalogue__category",
            "category",
        ).prefetch_related(
            "catalogue__variants",
            "addon_usage",
            "addon_usage__product",
            "addon",
            "addon__product",
        )
        return qs


class ProductModifierListView(ListAPIView):
    """
    List product based on company of authenticated user
    """
    serializer_class = ProductModifierSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.select_related(
            "product",
        ).prefetch_related(
            "products",
            Prefetch(
                "options",
                queryset=ProductModifierOption.objects.order_by("pk")
            ),
        )
        return qs


class CategoryRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    serializer_class = CategorySerializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class CategoryCreateAndListView(ListCreateAPIView):
    serializer_class = CategorySerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_serializer(self, *args, **kwargs):
        serializer = super(
            CategoryCreateAndListView, self).get_serializer(*args, **kwargs)
        qs = self.get_queryset()
        cat_dict = {}
        for cat in qs:
            cat_dict.update({cat.path: cat})
        serializer.context['cached_categories'] = cat_dict
        return serializer

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.all_objects.all()
        return qs


class ProductTypeListView(ListAPIView):
    """
    Create new ProductType
    """
    serializer_class = ProductTypeSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class UOMListView(ListAPIView):
    """
    Create new UOM
    """
    serializer_class = UOMSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class CreateVariantProductView(CreateAPIView):
    serializer_class = VariantCreateSerializer

