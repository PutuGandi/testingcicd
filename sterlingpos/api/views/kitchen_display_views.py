from rest_framework.generics import (
    ListAPIView, RetrieveAPIView,
    CreateAPIView, UpdateAPIView, DestroyAPIView
)
from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.api.views.mixins import LoggingMixins
from sterlingpos.api.serializers.kitchen_display_serializers import (
    KitchenCardSerializer,
)


class KitchenCardCreateView(LoggingMixins, CreateAPIView):
    serializer_class = KitchenCardSerializer

