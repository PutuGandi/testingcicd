import logging

from datetime import datetime
from decimal import Decimal

from django.conf import settings
from django.db import transaction, IntegrityError
from django.db.models import Prefetch, Sum, Value, DecimalField, Q
from django.db.models.functions import Coalesce
from django.shortcuts import get_object_or_404
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _

from celery import shared_task

from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, GenericAPIView

from sterlingpos.accounts.models import Account
from sterlingpos.core.models import set_current_tenant
from sterlingpos.customer.models import Customer
from sterlingpos.promo.models import Promo, CustomerPromo
from sterlingpos.sales.models import (
    ProductOrder,
    ProductOrderProductAddOnDetail,
    ProductOrderModifierOptionDetail,
    ProductOrderCompositeDetail,
    SalesOrderInvoice,
    SalesOrderPromoHistory
)

from sterlingpos.api.filters import ModifiedFilter
from sterlingpos.api.serializers.sales_serializers import (
    CashierActivitySerializer,
    CashierActivityReasonSerializer,
    TransactionTypeReasonSerializer,
    PaymentMethodSerializer,
    PaymentDetailSerializer,
    SalesOrderOptionSerializer,
    TransactionOrderSerializer,
    SalesOrderSentReceiptSerializer,
    SalesReportSettingSerializer,
    PromoUseCheckSerializer,
    PromoByCustomerSerializer
)
from sterlingpos.sales.models import SalesOrder

from sterlingpos.private_apps.signals import feed_webhook_signal


logger = logging.getLogger(__name__)


class PaymentMethodListView(ListAPIView):
    """
    List of PaymentMethod
    """
    serializer_class = PaymentMethodSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class SalesOrderOptionListView(ListAPIView):
    """
    List of SalesOrderOption
    """
    serializer_class = SalesOrderOptionSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class SalesOrderListView(ListAPIView):
    """
    List of SalesOrder
    """
    serializer_class = TransactionOrderSerializer
    filterset_class = ModifiedFilter
    ordering = ('-transaction_date',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.prefetch_related(

            Prefetch('items', queryset=ProductOrder.all_objects.all()),
            Prefetch('items__addon_detail', queryset=ProductOrderProductAddOnDetail.all_objects.all()),
            Prefetch('items__composite_detail', queryset=ProductOrderCompositeDetail.all_objects.all()),
            Prefetch('items__modifier_detail', queryset=ProductOrderModifierOptionDetail.all_objects.all()),

            'items__modifier_detail__product',
            'items__modifier_detail__option',

            'payment_detail',
            'payment_detail__payment_method',
            'payment_detail__payment_method__enabledpaymentmethod_set',

            'promo_list',
            'promo_list__promo',
        ).select_related(
            'order_option',
            'payment_method',
            'transaction_type',
            'outlet',
        )

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = None

        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = None

        if start_date:
            qs = qs.filter(transaction_date__gte=start_date)
        if end_date:
            qs = qs.filter(transaction_date__lte=end_date)

        if 'pk' in self.kwargs:
            qs = qs.filter(outlet__pk=self.kwargs.get('pk'))

        if 'branch_id' in self.kwargs:
            qs = qs.filter(outlet__branch_id=self.kwargs.get('branch_id'))

        return qs


class SalesOrderDetailView(RetrieveAPIView):
    """
    Detail of SalesOrder
    """
    serializer_class = TransactionOrderSerializer
    lookup_field = 'code'
    lookup_url_kwarg = 'code'

    def get_object(self):
        if self.kwargs.get('pk'):
            self.lookup_field = 'pk'
            self.lookup_url_kwarg = 'pk'

        return super(SalesOrderDetailView, self).get_object()

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.prefetch_related(

            Prefetch('items', queryset=ProductOrder.all_objects.all()),
            Prefetch('items__addon_detail', queryset=ProductOrderProductAddOnDetail.all_objects.all()),
            Prefetch('items__composite_detail', queryset=ProductOrderCompositeDetail.all_objects.all()),
            Prefetch('items__modifier_detail', queryset=ProductOrderModifierOptionDetail.all_objects.all()),

            'items__modifier_detail__product',
            'items__modifier_detail__option',

            'payment_detail',
            'payment_detail__payment_method',
            'payment_detail__payment_method__enabledpaymentmethod_set',

            'promo_list',
            'promo_list__promo',
        ).select_related(
            'order_option',
            'payment_method',
            'transaction_type',
            'outlet',
        ).all(force_visibility=True).order_by('-transaction_date')

        return qs


class SalesOrderSentReceiptView(viewsets.ModelViewSet):
    serializer_class = SalesOrderSentReceiptSerializer

    def get_queryset(self):
        qs = SalesOrder.all_objects.all()
        qs = qs.prefetch_related(
            'items',
            'items__addon_detail',
            'items__composite_detail',
            'items__modifier_detail',
            'items__modifier_detail__product',
            'items__modifier_detail__option',

            'payment_detail',
            'payment_detail__payment_method',
            'payment_detail__payment_method__enabledpaymentmethod_set',

            'promo_list',
            'promo_list__promo',
        ).select_related(
            'order_option',
            'payment_method',
            'transaction_type',
            'outlet',
        ).order_by('-transaction_date')

        return qs

    def get_object(self, pk):
        instance = get_object_or_404(
            self.get_queryset(), pk=pk)
        return instance

    @action(detail=True, methods=['post'])
    def sent_receipt(self, request, pk=None):
        sales_order = self.get_object(pk)
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save(sales_order=sales_order)
            return Response({'status': 'receipt sent'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class TransactionOrderCreateView(CreateAPIView):
    """
    Create new SalesOrder along with ProductOrder
    """
    serializer_class = TransactionOrderSerializer

    @transaction.atomic()
    def dispatch(self, *args, **kwargs):
        return super(TransactionOrderCreateView, self).dispatch(*args, **kwargs)

    def send_serializer_data(self, serializer, request):
        if not settings.STAGING:
            subject = 'KAWN-Prod TransactionOrder 400 Bad Request data'
        else:
            subject = 'KAWN-Staging TransactionOrder 400 Bad Request data'

        send_mail(
            subject,
            '{}\n\n{}'.format(serializer.errors, request.__dict__),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=['adhitya.nugroho@tech.indosterling.com'],
            fail_silently=True,
        )

    def send_aborted_request_data(self, log_info, request):
        if not settings.STAGING:
            subject = 'KAWN-Prod TransactionOrder Aborted 400 Bad Request data'
        else:
            subject = 'KAWN-Staging TransactionOrder Aborted 400 Bad Request data'

        send_mail(
            subject,
            '{}\n\n{}'.format(log_info, request.__dict__),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=['adhitya.nugroho@tech.indosterling.com'],
            fail_silently=True,
        )

    def create(self, request, *args, **kwargs):
        log_data = "Processing Transaction Started code={}, \
            outlet={}, transaction_date={}".format(
            self.request.data.get('code'),
            self.request.data.get('outlet'),
            self.request.data.get('transaction_date'),
        )
        logger.info(log_data)

        try:
            with transaction.atomic():
                serializer = self.get_serializer(data=request.data)
                if not serializer.is_valid():
                    self.send_serializer_data(serializer, request)

                    log_data = "Processing Transaction Failed code={}, \
                        outlet={}, transaction_date={}, reason={}".format(
                        self.request.data.get('code'),
                        self.request.data.get('outlet'),
                        self.request.data.get('transaction_date'),
                        serializer.errors
                    )
                    logger.warning(log_data)

                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer)
                headers = self.get_success_headers(serializer.data)

                log_data = "Processing Transaction Completed code={}, \
                    outlet={}, transaction_date={}".format(
                    self.request.data.get('code'),
                    self.request.data.get('outlet'),
                    self.request.data.get('transaction_date'),
                )
                logger.info(log_data)

                feed_webhook_signal.send(
                    sender=self.__class__,
                    instance=serializer.instance,
                    created=True)

                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED, headers=headers)
        except IntegrityError as e:

            log_data = "Processing Transaction Aborted code={}, \
                outlet={}, transaction_date={} err={}".format(
                self.request.data.get('code'),
                self.request.data.get('outlet'),
                self.request.data.get('transaction_date'),
                e,
            )
            logger.error(log_data)

            self.send_aborted_request_data(log_data, request)

            return Response(
                {
                    'success': False,
                    'description': _("Something went wrong, please try again.")
                },
                status=status.HTTP_400_BAD_REQUEST)


@shared_task(bind=True, max_retries=5)
def delayed_serializer_create(
    self, serializer_data, account_pk):
    try:
        account = Account.objects.get(pk=account_pk)
        set_current_tenant(account)
        serializer = CashierActivitySerializer(
            data=serializer_data)
        serializer.is_valid()
        serializer.save()
        set_current_tenant(None)
    except:
        self.retry(countdown=2 ** self.request.retries)


class CashierActivityCreateView(CreateAPIView):
    """
    Create new CashierActivity
    """
    serializer_class = CashierActivitySerializer

    def perform_create(self, serializer):
        delayed_serializer_create.delay(
            serializer.data, self.request.user.account.pk,
        )

    def create(self, request, *args, **kwargs):
        log_data = "Processing Cashier Activity Started sales_code={}, \
            outlet={}, activity_type={}, transaction_date={}".format(
            self.request.data.get('code'),
            self.request.data.get('outlet'),
            self.request.data.get('activity_type'),
            self.request.data.get('transaction_date'),
        )
        logger.info(log_data)

        response = super(CashierActivityCreateView, self).create(request, args, kwargs)

        log_data = "Processing Cashier Activity Completed({}) sales_code={}, \
            outlet={}, activity_type={}, transaction_date={}".format(
            response.status_code,
            self.request.data.get('code'),
            self.request.data.get('outlet'),
            self.request.data.get('activity_type'),
            self.request.data.get('date'),
        )
        logger.info(log_data)
        return response


class CashierActivityReasonListView(ListAPIView):
    serializer_class = CashierActivityReasonSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class TransactionTypeReasonListView(ListAPIView):
    serializer_class = TransactionTypeReasonSerializer
    filterset_class = ModifiedFilter
    ordering = ('-pk',)

    def get_queryset(self):
        qs = self.serializer_class.Meta.model.objects.all()
        return qs


class SalesReportSettingView(RetrieveAPIView):
    serializer_class = SalesReportSettingSerializer

    def get_object(self):
        obj = self.request.user.account.salesreportsetting_set.first()
        return obj


class PromoUseCheckView(GenericAPIView):
    serializer_class = PromoUseCheckSerializer

    def get_success_response(self):
        response = {'success': _('Promo limit is available')}
        return response

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            response = serializer.save()
            return Response(response, status=status.HTTP_200_OK)
        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PromoByCustomerListView(ListAPIView):
    serializer_class = PromoByCustomerSerializer
    lookup_field = 'id'
    ordering = ('-pk',)

    def get_queryset(self):
        customer_id = self.kwargs.get('pk')
        outlet_id = self.kwargs.get('outlet_pk')
        customer = get_object_or_404(Customer, pk=customer_id)

        group_id = getattr(customer.group, "id", None)
        organizational = customer.organization is not None
        day=datetime.today().isoweekday()

        qs = Promo.objects.exclude(
            end_date__lte=datetime.now(),
            start_date__gte=datetime.now(),
            start_hours__lte=datetime.now().time(),
            end_hours__gte=datetime.now().time(),
        ).filter(
            allow_days__icontains=day,
            outlet__id=outlet_id,
            is_active=True,
        ).filter(
            Q(affected_customer__id=customer.id) |
            Q(affected_group__id=group_id) |
            Q(customer_eligibility='everyone') |
            Q(organizational=organizational)
        ).distinct()

        return qs


class InvoicePaymentDetailCreateView(CreateAPIView):
    """
    Create new PaymentDetail for SalesOrder with invoice
    """
    serializer_class = PaymentDetailSerializer

    @transaction.atomic()
    def dispatch(self, *args, **kwargs):
        self.invoice = get_object_or_404(
            SalesOrderInvoice,
            code=self.kwargs.get("code"),
            sales_order__status__in=[
                SalesOrder.STATUS.unpaid, SalesOrder.STATUS.paid],
        )
        return super().dispatch(*args, **kwargs)

    def get_serializer(self, *args, **kwargs):
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True
        return super().get_serializer(*args, **kwargs)

    def get_serializer_context(self, *args, **kwargs):
        context = super().get_serializer_context(*args, **kwargs)
        context["sales_order"] = self.invoice.sales_order
        return context

    def send_serializer_data(self, serializer, request):
        if not settings.STAGING:
            subject = 'KAWN-Prod PaymentDetail 400 Bad Request data'
        else:
            subject = 'KAWN-Staging PaymentDetail 400 Bad Request data'

        send_mail(
            subject,
            '{}\n\n{}'.format(serializer.errors, request.__dict__),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=['adhitya.nugroho@tech.indosterling.com'],
            fail_silently=True,
        )

    def send_aborted_request_data(self, log_info, request):
        if not settings.STAGING:
            subject = 'KAWN-Prod PaymentDetail Aborted 400 Bad Request data'
        else:
            subject = 'KAWN-Staging PaymentDetail Aborted 400 Bad Request data'

        send_mail(
            subject,
            '{}\n\n{}'.format(log_info, request.__dict__),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=['adhitya.nugroho@tech.indosterling.com'],
            fail_silently=True,
        )

    def create(self, request, *args, **kwargs):
        log_data = "Processing Payment Detail Started invoice_code={}, \
            outlet={}, transaction_date={}".format(
            self.invoice.code,
            self.invoice.sales_order.outlet.branch_id,
            self.invoice.sales_order.transaction_date,
        )
        logger.info(log_data)

        try:
            with transaction.atomic():
                serializer = self.get_serializer(data=request.data)
                if not serializer.is_valid():
                    self.send_serializer_data(serializer, request)

                    log_data = "Processing Payment Detail Failed invoice_code={}, \
                        outlet={}, transaction_date={}, reason={}".format(
                        self.invoice.code,
                        self.invoice.sales_order.outlet.branch_id,
                        self.invoice.sales_order.transaction_date,
                        serializer.errors
                    )
                    logger.warning(log_data)

                serializer.is_valid(raise_exception=True)
                # Clean up unused context for deepcopy
                serializer.context.pop("request")
                serializer.context.pop("view")
                self.perform_create(serializer)
                headers = self.get_success_headers(serializer.data)

                log_data = "Processing Payment Detail Completed invoice_code={}, \
                    outlet={}, transaction_date={}".format(
                    self.invoice.code,
                    self.invoice.sales_order.outlet.branch_id,
                    self.invoice.sales_order.transaction_date,
                )
                logger.info(log_data)

                return Response(
                    serializer.data,
                    status=status.HTTP_201_CREATED, headers=headers)
        except IntegrityError as e:

            log_data = "Processing Payment Detail Aborted code={}, \
                outlet={}, transaction_date={} err={}".format(
                self.invoice.code,
                self.invoice.sales_order.outlet.branch_id,
                self.invoice.sales_order.transaction_date,
                e,
            )
            logger.error(log_data)

            self.send_aborted_request_data(log_data, request)

            return Response(
                {
                    'success': False,
                    'description': _("Something went wrong, please try again.")
                },
                status=status.HTTP_400_BAD_REQUEST)
