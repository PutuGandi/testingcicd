import logging
import rules

from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.shortcuts import get_object_or_404
from django.conf import settings

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.permissions import BasePermission, IsAuthenticated

from sterlingpos.digital_payment.models import DigitalPaymentConfiguration
from sterlingpos.integration.cashlez.models import CashlezSettings

logger = logging.getLogger(__name__)


class HasCashlezDigitalPayment(BasePermission):
    def has_permission(self, request, view):
        def _has_permission(perm):
            return rules.test_rule(
                'can_use_digital_payment',
                request.user.account, perm)
        has_permission = (
            _has_permission("go_pay") or
            _has_permission("ovo") or 
            _has_permission("cashlezz_edz") 
        )
        return has_permission


class CashlezMixins:
    permission_classes = [
        IsAuthenticated & HasCashlezDigitalPayment]


class CashlezAccountDetail(CashlezMixins, APIView):

    def get(self, request, format=None):
        instance = get_object_or_404(
            CashlezSettings,
            account=self.request.user.account
        )
        result = {
            "payment_detail": "cashlez",
            "merchant_ext_id": instance.merchant_ext_id,
            "outlets": instance.store_ext_ids,
        }
        return Response(result)


class CashlezAuthKeyView(CashlezMixins, APIView):

    def get(self, request, format=None):
        try:
            cashlez_conf = DigitalPaymentConfiguration.objects.get(slug="cashlez")
            result = cashlez_conf.settings
        except DigitalPaymentConfiguration.DoesNotExist:
            result = {
                "detail": "configuration not found"
            }
        return Response(result)
