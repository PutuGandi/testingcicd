import copy

from rest_framework import serializers

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from sterlingpos.table_order.models import (
    TableOrderSettings,
)
from sterlingpos.api.serializers.mixins import ExcludeArchiveListSerializer


class TableOrderSettingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TableOrderSettings
        exclude = (
            "account",
        )

