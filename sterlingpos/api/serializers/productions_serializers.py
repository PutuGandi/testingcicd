from rest_framework import serializers

from sterlingpos.productions.models import BOM, BOMItemDetail
from sterlingpos.catalogue.models import Product
from sterlingpos.stocks.models import Stock


class BOMProductsDetail(serializers.ModelSerializer):
    cost_read = serializers.CharField(source="cost.amount", read_only=True)
    price_read = serializers.CharField(source="price.amount", read_only=True)

    stock = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = (
            "id",
            "name",
            "sku",
            "classification",
            "uom",
            "price_read",
            "cost_read",
            "stock",
        )

    def get_stock(self, obj):
        if "outlet_pk" in self.context["request"].parser_context["kwargs"]:
            outlet_pk = self.context["request"].parser_context["kwargs"]["outlet_pk"]
            stock = None

            if obj.is_manage_stock:
                try:
                    stock = obj.stocks.get(outlet__pk=outlet_pk).balance
                except Stock.DoesNotExist:
                    pass

            return stock
        else:
            return [
                {"balance": stock.balance, "outlet_pk": stock.outlet.pk}
                for stock in obj.stocks.all()
            ]


class BOMDetailSerializers(serializers.ModelSerializer):
    product = BOMProductsDetail(read_only=True)

    class Meta:
        model = BOMItemDetail
        exclude = ("account",)


class BOMSerializer(serializers.ModelSerializer):
    bom_item = BOMDetailSerializers(many=True, read_only=True)

    class Meta:
        model = BOM
        exclude = ("account",)
