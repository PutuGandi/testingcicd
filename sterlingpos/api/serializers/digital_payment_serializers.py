from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from sterlingpos.digital_payment.models import DigitalPaymentSettings


class DigitalPaymentSettingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = DigitalPaymentSettings
        exclude = ('account', )
