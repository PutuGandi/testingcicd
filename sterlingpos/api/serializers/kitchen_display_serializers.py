import datetime

from rest_framework import serializers

from sterlingpos.kitchen_display.models import KitchenCard, KitchenCardItem
from sterlingpos.api.serializers.mixins import (
    AllObjectsListSerializer,
    ListSerializer,
)

class KitchenCardItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = KitchenCardItem
        exclude = ('account', 'kitchen_card')

    def create_or_update(self, instance, validated_data):

        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            instance = self.Meta.model.objects.get(pk=instance.pk)
        else:
            validated_data['kitchen_card'] = self.context.get('kitchen_card')
            instance = self.Meta.model.objects.create(**validated_data)

        return instance

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class KitchenCardSerializer(serializers.ModelSerializer):

    card_items = KitchenCardItemSerializer(many=True)

    class Meta:
        model = KitchenCard
        exclude = ('account',)
        list_serializer_class = AllObjectsListSerializer
        m2m_fields = {
            'card_items': 'kitchen_card',
        }
        m2m_fields_rel = {
            'card_items': 'card_items',
        }

    def prepare_m2m_field(self, validated_data):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.initial_data = self.initial_data.get(m2m_field_name, None)
            validated_data.pop(m2m_field_name, None)
            if self.instance:
                rel = getattr(self.instance, self.Meta.m2m_fields_rel[m2m_field_name])
                m2m_field.instance = rel.all()
        return validated_data

    def save_m2m_field(self, instance):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.context[self.Meta.m2m_fields.get(m2m_field_name)] = instance

            if m2m_field.is_valid():
                m2m_field.save()

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def create_or_update(self, instance, validated_data):
        validated_data = self.prepare_m2m_field(
            validated_data)

        if not instance:
            instance = self.Meta.model.objects.create(
                **validated_data)
        else:
            for key in validated_data:
                if key in self.Meta.allowed_update_field:
                    setattr(instance, key, validated_data[key])
            instance.save()

        self.save_m2m_field(instance)
        return instance
