from django.utils import timezone
from rest_framework import serializers


class DateTimeTZField(serializers.DateTimeField):

    def to_representation(self, value):
        tz = timezone.get_default_timezone()
        # timezone.localtime() defaults to the current tz, you only
        # need the `tz` arg if the current tz != default tz
        if not timezone.is_aware(value):
            value = timezone.make_aware(value)

        value = timezone.localtime(value, timezone=tz)
        return super(DateTimeTZField, self).to_representation(value)
