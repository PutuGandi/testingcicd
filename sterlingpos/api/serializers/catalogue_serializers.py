from distutils.util import strtobool

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from djmoney.contrib.django_rest_framework.fields import MoneyField
from sterlingpos.catalogue.models import (
    Product, Catalogue, CompositeProduct, Category, ProductType, UOM,
    ProductAddOn, ProductModifier, ProductModifierOption,
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs
from sterlingpos.stocks.models import Stock
from sterlingpos.api.serializers.productions_serializers import BOMSerializer

from .fields import DateTimeTZField
from .mixins import ListSerializer


class UOMSerializer(serializers.ModelSerializer):
    class Meta:
        model = UOM
        exclude = ('account',)


class CategorySerializer(serializers.ModelSerializer):
    path = serializers.CharField(write_only=True)
    archived = serializers.CharField(required=False, write_only=True)
    is_archived = serializers.BooleanField(source="archived", read_only=True)
    parent = serializers.SerializerMethodField()

    class Meta:
        model = Category
        read_only_fields = ('name',)
        exclude = ("depth", "numchild", "description", "slug", "account")
        extra_kwargs = {
            'path': {'write_only': True},
            'archived': {'write_only': True}
        }

    def create(self, validated_data):
        instance = create_from_breadcrumbs(validated_data['path'])
        archived = validated_data.get('archived')
        if archived:
            instance.archived = strtobool(archived)
            instance.save()
        return instance

    def update(self, instance, validated_data):
        path = validated_data.pop('path').split(" > ")
        archived = validated_data.pop('archived', None)
        if archived:
            archived_data = archived.split(" > ")
        else:
            archived_data = []

        for i, cat in enumerate(instance.get_ancestors_and_self()):
            if archived_data:
                cat.archived = strtobool(archived_data[i])
            cat.name = "{}".format(path[-1])
            cat.save()
        return instance

    def get_parent(self, obj):
        possible_parent = obj.path[:-4]
        if len(possible_parent) >= 4:
            if 'cached_categories' in self.context:
                parent = self.context.get('cached_categories').get(possible_parent)
            else:
                parent = obj.get_ancestors().first()

            if parent:
                return self.__class__(instance=parent).data


class ProductTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductType
        exclude = ('account',)


class ProductAddOnSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductAddOn
        fields = ('pk', 'product_usage',)


class ProductModifierOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductModifierOption
        exclude = ('account',)


class ProductModifierSerializer(serializers.ModelSerializer):
    options = ProductModifierOptionSerializer(read_only=True, required=False, many=True)

    class Meta:
        model = ProductModifier
        exclude = ('account',)


class CatalogueSerializer(serializers.ModelSerializer):
    variants = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Product.objects.all())

    class Meta:
        model = Catalogue
        list_serializer_class = ListSerializer
        exclude = ('account',)

    def to_representation(self, obj):
        self.fields['category'] = CategorySerializer(required=False)

        return super(CatalogueSerializer, self).to_representation(obj)


class BaseProductSerializer(serializers.ModelSerializer):
    cost_read = serializers.CharField(source="cost.amount", read_only=True)
    price_read = serializers.CharField(source="price.amount", read_only=True)
    moving_average_cost_read = serializers.CharField(source="moving_average_cost.amount", read_only=True)

    cost = MoneyField(
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)
    price = MoneyField(
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)
    moving_average_cost = MoneyField(
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        read_only=True)

    product_type = ProductTypeSerializer(required=False, write_only=True)
    released = DateTimeTZField(required=False)

    catalogue = CatalogueSerializer(read_only=True)

    class Meta:
        model = Product
        exclude = ("account", "is_batch_tracked", "parent")

    def validate_parent(self, value):
        if self.instance and \
            self.instance.variants.exists() and \
            value is not None:
            raise serializers.ValidationError(
                _("Product with variants cannot own a parent product and Variants cannot be a parent product")
            )
        return value

    def to_representation(self, obj):
        self.fields['category'] = CategorySerializer(required=False)

        return super(BaseProductSerializer, self).to_representation(obj)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def create_or_update(self, instance, validated_data):
        product_type_f = self.fields.get('product_type')
        product_type_f.initial_data = validated_data.get('product_type', None)

        if product_type_f.is_valid():
            validated_data['product_type'] = product_type_f.save()

        if instance:
            for attr, value in validated_data.items():
                setattr(instance, attr, value)
                instance.save()
            product = instance
        else:
            product = self.Meta.model.objects.create(**validated_data)
            catalogue = Catalogue.prepare_catalogue(product)
            product.catalogue = catalogue
        return product


class CompositeProductSerializer(serializers.ModelSerializer):
    product_read = serializers.PrimaryKeyRelatedField(
        read_only=True, source="product")
    product = serializers.CharField(write_only=True)
    composite_product = serializers.CharField(required=False, write_only=True)

    class Meta:
        model = CompositeProduct
        list_serializer_class = ListSerializer
        exclude = ('account',)

    def get_product(self, sku):
        try:
            product = Product.objects.get(sku=sku)
            return product
        except Product.DoesNotExist as e:
            raise serializers.ValidationError(
                _("Product does not exists.")
            )

    def validate_product(self, value):
        self.product = self.get_product(value)
        if self.product.classification == Product.PRODUCT_CLASSIFICATION.composite:
            raise serializers.ValidationError(
                _("Product with composite_product cannot be added into another composite_product.")
            )
        return value

    def validate_composite_product(self, value):
        self.composite_product = self.get_product(value)
        if self.composite_product.classification != Product.PRODUCT_CLASSIFICATION.composite:
            raise serializers.ValidationError(
                _("Only Product with composite classification can have composite_product.")
            )
        return value

    def update(self, instance, validated_data):
        validated_data['product'] = self.product
        validated_data['composite_product'] = self.composite_product
        return super(CompositeProductSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        validated_data['product'] = self.product
        validated_data['composite_product'] = self.composite_product
        return super(CompositeProductSerializer, self).create(validated_data)


class ProductSerializer(BaseProductSerializer):
    addon_usage = ProductAddOnSerializer(read_only=True, many=True)
    composite_material = CompositeProductSerializer(required=False, many=True)

    class Meta(BaseProductSerializer.Meta):
        exclude = ('account', 'parent')

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def create_or_update(self, instance, validated_data):

        composite_material_f = self.fields.get('composite_material')
        composite_material_f.initial_data = validated_data.pop('composite_material', None)

        product = super(ProductSerializer, self).create_or_update(
            instance, validated_data)

        if composite_material_f.initial_data:
            for data in composite_material_f.initial_data:
                data.update({'composite_product': product.sku})

            composite_material_f.instance = product.composite_material.all()
            if composite_material_f.is_valid():
                composite_material = composite_material_f.save()
        return product


    def to_representation(self, obj):
        response = super().to_representation(obj)
        response["composite_material"] = sorted(
            response["composite_material"], key=lambda x: x["id"]
        )
        return response


class OutletProductListSerializer(ProductSerializer):
    price_read = serializers.SerializerMethodField()
    is_available = serializers.CharField(read_only=True)

    def get_price_read(self, obj):
        if obj.outlet_price:
            return str(obj.outlet_price)
        return str(obj.price.amount)


class OutletProductSerializer(ProductSerializer):
    price_read = serializers.SerializerMethodField()
    is_available = serializers.CharField(read_only=True)

    def get_price_read(self, obj):
        if obj.outlet_price:
            return str(obj.outlet_price)
        return str(obj.price.amount)


class OutletProductStockSerializer(serializers.ModelSerializer):
    stock = serializers.DecimalField(max_digits=19, decimal_places=3)

    class Meta:
        model = Product
        fields = ('id', 'stock',)


class VariantCreateSerializer(serializers.ModelSerializer):
    cost_read = serializers.CharField(source="cost.amount", read_only=True)
    price_read = serializers.CharField(source="price.amount", read_only=True)
    moving_average_cost_read = serializers.CharField(source="moving_average_cost.amount", read_only=True)

    cost = MoneyField(max_digits=10, decimal_places=2, write_only=True)
    price = MoneyField(max_digits=10, decimal_places=2, write_only=True)
    moving_average_cost = MoneyField(
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        read_only=True)

    addon_usage = ProductAddOnSerializer(read_only=True, many=True)
    composite_material = CompositeProductSerializer(required=False, read_only=True, many=True)

    class Meta:
        model = Product
        exclude = ('account',)

    def to_representation(self, obj):
        self.fields['catalogue'] = CatalogueSerializer(required=False)
        self.fields['parent'] = BaseProductSerializer(required=False)
        self.fields['category'] = CategorySerializer(required=False)
        return super().to_representation(obj)


class CatalogueUpdateSerializer(serializers.ModelSerializer):
    variants = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Product.objects.all(), required=False)

    class Meta:
        model = Catalogue
        exclude = ('account',)

    def to_representation(self, obj):
        self.fields['category'] = CategorySerializer(required=False)

        return super(CatalogueUpdateSerializer, self).to_representation(obj)
