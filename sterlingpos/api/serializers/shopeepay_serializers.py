import logging
import uuid

from decimal import Decimal
from requests.exceptions import (
    ConnectionError,
    HTTPError,
)

try:
    from urllib.parse import urljoin, urlencode
except ImportError:
    from urlparse import urljoin
    from urllib import urlencode

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from shopeepay import ShopeePayV3 as ShopeePay
from shopeepay.exceptions import ShopeePayAPIError


logger = logging.getLogger(__name__)


class ShopeePayMixin(object):

    def get_client(self, merchant, outlet):
        client = ShopeePay(
            settings.PAYMENT["SHOPEEPAY"]["HOST"],
            settings.PAYMENT["SHOPEEPAY"]["KEY"],
            settings.PAYMENT["SHOPEEPAY"]["SECRET"],
            merchant,
            outlet,
        )
        return client

    def handle_exception(self, action_name, error, data):
        if isinstance(error, ShopeePayAPIError):
            log_data = "Shopeepay: {} failed, error_code={} message={} request_id={} data={}".format(
                action_name,
                error.code,
                error.message,
                error.request_id if hasattr(error, 'request_id') else "NO_REQUEST_ID",
                data,
            )
            result = {
                "request_id": error.request_id if hasattr(error, 'request_id') else "NO_REQUEST_ID",
                "errcode": error.code,
                "debug_msg": error.message,
            }
        else:
            log_data = "Shopeepay: {} failed, error={} data={}".format(
                action_name,
                error,
                data
            )
            result = {
                "request_id": None,
                "errcode": 500,
                "debug_msg": "Unable to make connection to server.",
            }
        logger.critical(log_data)
        return result


class GetQRSerializer(ShopeePayMixin, serializers.Serializer):
    merchant_ext_id = serializers.CharField(
        label=_("Merchant ID"))
    store_ext_id = serializers.CharField(
        label=_("Store ID"))
    reference_id = serializers.CharField(
        label=_("Reference ID"))

    terminal_id = serializers.CharField(
        label=_("Terminal ID"), required=False)

    amount = serializers.DecimalField(
        max_digits=19, decimal_places=2, min_value=0)

    def save(self):
        data = self.data

        client = self.get_client(
            data.get("merchant_ext_id"),
            data.get("store_ext_id"))

        try:
            result = client.get_qr(
                int(Decimal(data.get("amount"))),
                data.get("reference_id"),
                data.get("terminal_id"))
            log_data = "Shopeepay: GetQRCode ref_id={} amount={} result={} ".format(
                data.get("reference_id"),
                data.get("amount"),
                result
            )
            logger.info(log_data)
        except (ShopeePayAPIError, ConnectionError, HTTPError) as error:
            result = self.handle_exception("GetQRCode", error, data)

        return result


class InvalidateQRSerializer(ShopeePayMixin, serializers.Serializer):
    merchant_ext_id = serializers.CharField(
        label=_("Merchant ID"))
    store_ext_id = serializers.CharField(
        label=_("Store ID"))
    reference_id = serializers.CharField(
        label=_("Reference ID"))

    def save(self):
        data = self.data

        client = self.get_client(
            data.get("merchant_ext_id"),
            data.get("store_ext_id"))

        try:
            result = client.invalidate_qr(
                data.get("reference_id"))
            log_data = "Shopeepay: InvalidateQR ref_id={} result={} ".format(
                data.get("reference_id"),
                result
            )
            logger.info(log_data)
        except (ShopeePayAPIError, ConnectionError, HTTPError) as error:
            result = self.handle_exception("InvalidateQR", error, data)

        return result


class PaymentSerializer(ShopeePayMixin, serializers.Serializer):
    merchant_ext_id = serializers.CharField(
        label=_("Merchant ID"))
    store_ext_id = serializers.CharField(
        label=_("Store ID"))
    reference_id = serializers.CharField(
        label=_("Reference ID"))

    def save(self):
        return self.data


class PaymentCheckSerializer(PaymentSerializer):

    def save(self):
        data = self.data
        client = self.get_client(
            data.get("merchant_ext_id"),
            data.get("store_ext_id"))

        try:
            result = client.check_payment(
                data.get("reference_id"))
            log_data = "Shopeepay: CheckPayment ref_id={} result={} ".format(
                data.get("reference_id"),
                result
            )
            logger.info(log_data)
        except (ShopeePayAPIError, ConnectionError, HTTPError) as error:
            result = self.handle_exception("CheckPayment", error, data)

        return result


class PaymentRefundSerializer(PaymentSerializer):

    def save(self):
        data = self.data

        client = self.get_client(
            data.get("merchant_ext_id"),
            data.get("store_ext_id"))

        try:
            result = client.refund_payment(
                data.get("reference_id"))
            log_data = "Shopeepay: RefundPayment ref_id={} result={} ".format(
                data.get("reference_id"),
                result
            )
        except (ShopeePayAPIError, ConnectionError, HTTPError) as error:
            result = self.handle_exception("RefundPayment", error, data)

        return result
