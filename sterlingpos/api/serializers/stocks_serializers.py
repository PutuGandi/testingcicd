from django.conf import settings
from django.utils import timezone

from rest_framework import serializers
from djmoney.contrib.django_rest_framework.fields import MoneyField

from sterlingpos.stocks.models import (
    PurchaseOrder,
    PurchaseOrderDetail,
    PurchaseOrderReceive,
    PurchaseOrderReceiveDetail,
    Supplier,
    SupplierProductDetail,
    Stock,
)


class SupplierProductDetailSerializer(serializers.ModelSerializer):
    cost = MoneyField(
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )

    class Meta:
        model = SupplierProductDetail
        exclude = ("account",)


class SupplierSerializer(serializers.ModelSerializer):
    supplier_detail = SupplierProductDetailSerializer(many=True, read_only=True)

    class Meta:
        model = Supplier
        exclude = ("account",)


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = ("product",)


class PurchaseOrderDetailSerializer(serializers.ModelSerializer):
    purchase_cost = MoneyField(
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )
    stock = StockSerializer(many=False)

    class Meta:
        model = PurchaseOrderDetail
        exclude = (
            "account",
            "purchase_order",
        )


class PurchaseOrderSerializer(serializers.ModelSerializer):
    purchased_stock = PurchaseOrderDetailSerializer(many=True)

    class Meta:
        model = PurchaseOrder
        exclude = ("account",)

    def create(self, validated_data):
        purchased_stock = validated_data.pop("purchased_stock")
        instance = PurchaseOrder.objects.create(**validated_data)
        for purchased in purchased_stock:
            product = purchased.pop("stock")["product"]
            instance.purchase_product_stock(
                product,
                purchased["quantity"],
                purchased["unit"],
                purchased["purchase_cost"],
                purchased["discount_type"],
                purchased["discount"],
            )
        return instance

    def update(self, instance, validated_data):
        purchased_stock = validated_data.pop("purchased_stock")
        instance = super().update(instance, validated_data)
        for purchased in purchased_stock:
            product = purchased.pop("stock")["product"]
            try:
                stock_exist = instance.purchased_stock.get(stock__product=product)
                for attr, value in purchased.items():
                    setattr(stock_exist, attr, value)
                stock_exist.save()
            except Stock.DoesNotExist:
                pass
            except PurchaseOrderDetail.DoesNotExist:
                instance.purchase_product_stock(
                    product,
                    purchased["quantity"],
                    purchased["unit"],
                    purchased["purchase_cost"],
                    purchased["discount_type"],
                    purchased["discount"],
                )
        return instance


class PurchaseOrderCancelRemainingItemSerializers(serializers.ModelSerializer):
    class Meta:
        model = PurchaseOrder
        fields = ("id",)

    def update(self, instance, validated_data):
        instance.status = PurchaseOrder.STATUS.closed
        instance.save()
        return instance


class ReceivedOrderDetailSerializer(serializers.ModelSerializer):
    purchase_cost = MoneyField(
        max_digits=settings.LOW_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
    )

    stock = StockSerializer(many=False)

    class Meta:
        model = PurchaseOrderReceiveDetail
        exclude = ("account", "received")

    def update(self, instance, validated_data):
        _ = validated_data.pop("stock")
        instance = super().update(instance, validated_data)
        return instance


class ReceivedOrderCompleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchaseOrderReceive
        fields = ("id",)

    def update(self, instance, validated_data):
        instance.received = True
        instance.received_date = timezone.now()
        instance.save()
        for received in instance.received_detail.filter(
            stock__product__is_manage_stock=True, stock__product__uom__isnull=False
        ).select_related(
            "stock",
            "stock__product",
            "stock__product__account",
            "stock__product__uom",
            "unit",
            "stock__outlet",
            "stock__account",
        ):
            received.received_product(user=None)
        instance.purchase_order.received()

        return instance


class ReceivedOrderSerializer(serializers.ModelSerializer):
    received_detail = ReceivedOrderDetailSerializer(many=True)
    purchase_order = PurchaseOrderSerializer(many=False)

    class Meta:
        model = PurchaseOrderReceive
        exclude = ("account",)

    def create(self, validated_data):
        received_detail = validated_data.pop("received_detail")
        instance = PurchaseOrderReceive.objects.create(**validated_data)
        outlet = instance.purchase_order.outlet
        for received in received_detail:
            product = received.pop("stock")["product"]
            try:
                stock = Stock.objects.get(product=product, outlet=outlet)
                instance.receive_purchase(
                    stock,
                    received["quantity"],
                    received["unit"],
                )
            except Stock.DoesNotExist:
                pass
    
        return instance

    def update(self, instance, validated_data):
        received_detail = validated_data.pop("received_detail")
        _ = validated_data.pop("purchase_order")

        instance = super().update(instance, validated_data)
        outlet = instance.purchase_order.outlet
        for received in received_detail:
            product = received.pop("stock")["product"]
            try:
                stock = Stock.objects.get(product=product, outlet=outlet)
                stock_exist = instance.received_detail.get(stock=stock)
                for attr, value in received.items():
                    setattr(stock_exist, attr, value)
                stock_exist.save()
            except PurchaseOrderReceiveDetail.DoesNotExist:
                pass
            except Stock.DoesNotExist:
                pass
        return instance
