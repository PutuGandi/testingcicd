import datetime

from rest_framework import serializers

from sterlingpos.role.models import Role, RoleSettings


class RoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        exclude = ('account', )


class RoleSettingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoleSettings
        exclude = ('account', )
