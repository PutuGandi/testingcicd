import uuid

try:
    from urllib.parse import urljoin, urlencode
except ImportError:
    from urlparse import urljoin
    from urllib import urlencode

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework_jwt.serializers import JSONWebTokenSerializer

from allauth.account import adapter as allauth_adapter
from allauth.account import signals as account_signals
from allauth.account import app_settings
from allauth.account.forms import (
    AddEmailForm,
    default_token_generator
)
from allauth.account.models import EmailAddress
from allauth.account.utils import (
    send_email_confirmation,
    get_next_redirect_url,
    complete_signup
)

from allauth.account.forms import (
    ResetPasswordForm,
)

from sterlingpos.accounts.models import Account
from sterlingpos.outlet.forms import BaseOutletForm, OutletForm
from sterlingpos.users.forms import UserSignupForm
from sterlingpos.subscription.models import SubscriptionPlan, Subscription

User = get_user_model()


class AuthTokenSerializer(JSONWebTokenSerializer):

    def validate(self, attrs):
        attrs = super(AuthTokenSerializer, self).validate(attrs)
        request = self.context['request']._request
        user = attrs.get('user')
        has_verified_email = EmailAddress.objects.filter(
            user=user, verified=True).exists()
        email_verification = app_settings.EMAIL_VERIFICATION
        if email_verification == app_settings.EmailVerificationMethod.NONE:
            pass
        elif email_verification == app_settings.EmailVerificationMethod.OPTIONAL:
            # In case of OPTIONAL verification: send on signup.
            if not has_verified_email:
                send_email_confirmation(
                    request, user, signup=True)
        elif email_verification == app_settings.EmailVerificationMethod.MANDATORY:
            if not has_verified_email:
                send_email_confirmation(request, user, signup=True)
                raise serializers.ValidationError(
                    _('We have sent an e-mail to for verification. '
                      'Follow the link provided to finalize the signup process. '
                      'Please contact us if you do not receive it within a few minutes.'))
        return attrs


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        exclude = (
            'account',
            'password',
            'is_active', 'email', 'groups',
            'user_permissions',
        )


class UserPasswordFormSerializer(serializers.ModelSerializer):
    oldpassword = serializers.CharField(
        label=_("Current Password"),
        write_only=True,
        style={'input_type': 'password'})
    password1 = serializers.CharField(
        label=_("New Password"),
        write_only=True,
        style={'input_type': 'password'},)
    password2 = serializers.CharField(
        label=_("New Password (again)"),
        write_only=True,
        style={'input_type': 'password'},)

    class Meta:
        model = get_user_model()
        fields = [
            'oldpassword',
            'password1', 'password2',
        ]

    def validate_oldpassword(self, oldpassword):
        if not self.context['request'].user.check_password(oldpassword):
            raise serializers.ValidationError(
                _("Please type your current"
                  " password."))
        return oldpassword

    def validate_password2(self, password2):
        if ("password1" in self.initial_data
                and "password2" in self.initial_data):
            if (self.initial_data["password1"]
                    != password2):
                raise serializers.ValidationError(
                    _("You must type the same password"
                      " each time."))
        return password2

    def save(self, **kwargs):
        allauth_adapter.get_adapter().set_password(
            self.context['request'].user, self.validated_data["password1"])
        account_signals.password_changed.send(
            sender=self.context['request'].user.__class__,
            request=self.context['request'],
            user=self.context['request'].user)


class LostPasswordFormSerializer(serializers.Serializer):
    email = serializers.EmailField(write_only=True)

    def validate(self, attrs):
        self.reset_form = ResetPasswordForm(attrs)
        if self.reset_form.is_valid():
            return attrs
        raise serializers.ValidationError(self.reset_form.errors['email'][0])

    def save(self):
        kwargs = {
            'use_https': self.context.get('request').is_secure(),
            'token_generator': default_token_generator,
            'request': self.context.get('request'),
        }
        self.reset_form.save(**kwargs)
        return True


class SignUpSerializer(serializers.Serializer):
    account_name = serializers.CharField(
        label=_("Account Name"),
        write_only=True)
    email = serializers.EmailField(
        label=_("Email"), write_only=True)
    password1 = serializers.CharField(
        label=_("Password"),
        write_only=True,
        style={'input_type': 'password'},)

    name = serializers.CharField(
        label=_("Outlet Name"),
        write_only=True)
    phone = serializers.CharField(
        label=_("Outlet Phone"),
        write_only=True)
    address = serializers.CharField(
        label=_("Outlet Address"),
        write_only=True)

    province = serializers.IntegerField(
        label=_("Province"),
        min_value=1)
    city = serializers.IntegerField(
        label=_("City"),
        min_value=1)

    taxes = serializers.DecimalField(
        max_digits=5, decimal_places=2, default=10)

    gratuity = serializers.DecimalField(
        max_digits=5, decimal_places=2, default=10)

    def validate(self, attrs):
        self.user_form = UserSignupForm(attrs)
        self.outlet_form = OutletForm(attrs)
        if self.user_form.is_valid() and self.outlet_form.is_valid():
            return attrs

        errors = {}
        errors.update(self.user_form.errors)
        errors.update(self.outlet_form.errors)
        raise serializers.ValidationError(errors)

    def process_outlet(self, form, account):
        outlet = form.save(commit=False)
        branch_id = uuid.uuid4().hex[:8]
        outlet.account = account
        outlet.branch_id = branch_id
        outlet.save()
        return outlet

    def get_mobile_subscription_plan(self):
        try:
            subscription_plan = SubscriptionPlan.objects.get(slug="mobile-lite")
        except SubscriptionPlan.DoesNotExist:
            subscription_plan = SubscriptionPlan.objects.create(
                name='Mobile Subscription',
                slug='mobile-lite',
                price=85000,
                trial_period=0,
                trial_unit=None,
                recurrence_period=0,
                recurrence_unit=None,
                is_public=False,
            )
        return subscription_plan

    def save(self):
        user = self.user_form.save(self.context.get('request'))
        user.account.registered_via = Account.REGISTRATION_ACCESS.mobile_lite
        device_user = user.account.deviceuser_set.first()
        device_user.user = user
        device_user.email = user.email
        device_user.save()

        outlet = self.process_outlet(self.outlet_form, user.account)
        subscription = Subscription.objects.create(
            subscription_plan=self.get_mobile_subscription_plan(),
            outlet=outlet,
            account=user.account,
            is_trial=False,
        )

        complete_signup(
            self.context.get('request'),
            user, app_settings.EMAIL_VERIFICATION, '/')

        return user


class EmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = EmailAddress
        exclude = ["user", ]
        read_only = [
            "verified", "primary",
        ]

    def validate(self, attrs):
        self.form = AddEmailForm(
            user=self.context.get("request").user,
            data=attrs)

        if self.form.is_valid():
            return attrs
        raise serializers.ValidationError(
            self.form.errors["email"][0])

    def save(self, **kwargs):
        email = self.form.save(
            self.context.get("request"))
        return email
