from rest_framework import serializers
from sterlingpos.surcharges.models import Surcharge


class SurchargeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Surcharge
        exclude = ('account', 'start_date', 'end_date', 'start_hours', 'end_hours')
