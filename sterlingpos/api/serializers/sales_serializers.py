import logging
from decimal import Decimal
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta, MO, SU

from django.conf import settings
from django.db.models import Sum, Value, DecimalField, F
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from djmoney.contrib.django_rest_framework.fields import MoneyField

from sterlingpos.customer.models import Customer
from .fields import DateTimeTZField

from sterlingpos.catalogue.models import Product, ProductModifierOption
from sterlingpos.table_management.models import Table
from sterlingpos.promo.models import Promo, CustomerPromo, _TIME_LIMIT_CHOICES

from sterlingpos.sales.models import (
    SalesOrder, ProductOrder,

    SalesOrderOption,
    SalesOrderInvoice,
    InvoiceInstallment,
    SalesOrderPromoHistory,

    ProductOrderCompositeDetail,
    ProductOrderModifierOptionDetail,
    ProductOrderProductAddOnDetail,

    PaymentMethod, PaymentDetail,
    TransactionType,
    TransactionTypeReason,

    CashierActivity,
    CashierActivityReason,

    SalesReportSetting,
)
from sterlingpos.api.serializers.mixins import (
    AllObjectsListSerializer,
    ListSerializer,
)
from sterlingpos.api.serializers.promo_serializers import PromoSerializer
from sterlingpos.api.serializers.catalogue_serializers import (
    ProductSerializer,
    ProductModifierOptionSerializer,
)

# Get an instance of a logger
logger = logging.getLogger(__name__)


class InvoiceInstallmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = InvoiceInstallment
        exclude = ('account', 'invoice')
        list_serializer_class = AllObjectsListSerializer


class SalesOrderInvoiceSerializer(serializers.ModelSerializer):
    installment = InvoiceInstallmentSerializer(required=False)

    class Meta:
        model = SalesOrderInvoice
        exclude = ('account',)
        list_serializer_class = AllObjectsListSerializer

    def prepare_related_field(self, instance):
        try:
            installment_data = instance.pop('installment', )
        except:
            installment_data = False

        return installment_data

    def create_or_update(self, instance, validated_data):
        related_field = self.prepare_related_field(validated_data)

        if not instance:
            invoice = self.Meta.model.objects.create(**validated_data)

            if related_field:
                installment = InvoiceInstallmentSerializer.Meta.model.objects.create(invoice=invoice, **related_field)

        return invoice

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class SalesOrderSerializer(serializers.ModelSerializer):
    global_discount_fixed_price_read = serializers.CharField(
        source="global_discount_fixed_price.amount", read_only=True)
    global_discount_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)

    global_discount_specified_amount_read = serializers.CharField(
        source='global_discount_specified_amount.amount', read_only=True)
    global_discount_specified_amount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)

    global_discount_price_read = serializers.CharField(
        source="global_discount_price.amount", read_only=True)
    global_discount_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)

    sub_total_read = serializers.CharField(
        source="sub_total.amount", read_only=True)
    sub_total = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    tendered_amount_read = serializers.CharField(
        source="tendered_amount.amount", read_only=True)
    tendered_amount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    total_cost_read = serializers.CharField(
        source="total_cost.amount", read_only=True)
    total_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    change_read = serializers.CharField(
        source="change.amount", read_only=True)
    change = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)
    date = DateTimeTZField(required=False)

    company_branch_read = serializers.CharField(
        source="company_branch.branch_id", read_only=True)

    branch_id = serializers.CharField(required=False, write_only=True)

    order_name = serializers.CharField(
        required=False, allow_blank=True, max_length=80)

    payment_type = serializers.SerializerMethodField()

    guest_number = serializers.IntegerField(default=1)

    class Meta:
        model = SalesOrder
        exclude = ('account',)
        allowed_update_field = ['status', ]

    def validate_order_name(self, value):
        if len(value) >= 60:
            return value[:60]
        return value

    def validate_guest_number(self, value):
        if value <= 0:
            return 1
        return value

    def validate(self, data):
        has_extra_changes = data.get('change') > data.get('tendered_amount') - data.get('total_cost')
        is_settled = data.get("status") == "settled"
        if is_settled and has_extra_changes:
            raise serializers.ValidationError(
                'Change value cannot be higher than tendered amount minus total cost value.')
        return data

    def get_payment_type(self, obj):
        return None


class SalesOrderOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesOrderOption
        exclude = ('account',)


class SalesOrderPromoHistorySerializer(serializers.ModelSerializer):
    promo = PromoSerializer()

    discount_fixed_price_read = serializers.CharField(
        source="discount_fixed_price.amount", read_only=True)
    discount_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    base_value_read = serializers.CharField(
        source="base_value.amount", read_only=True)
    base_value = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS, 
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    affected_value_read = serializers.CharField(
        source="affected_value.amount", read_only=True)
    affected_value = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    total_discount_read = serializers.CharField(
        source="total_discount.amount", read_only=True)
    total_discount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True,
        required=False)

    class Meta:
        model = SalesOrderPromoHistory
        list_serializer_class = AllObjectsListSerializer
        exclude = ('sales_order', 'account')

    def to_internal_value(self, data):
        self.fields['promo'] = serializers.PrimaryKeyRelatedField(
            queryset=Promo.all_objects.all())
        self.fields['affected_product'] = serializers.PrimaryKeyRelatedField(
            queryset=Product.all_objects.all(), required=False)
        return super(SalesOrderPromoHistorySerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['promo'] = serializers.PrimaryKeyRelatedField(
            queryset=Promo.all_objects.all())
        return super(SalesOrderPromoHistorySerializer, self).to_representation(obj)

    def validate(self, data):

        if ('affected_product' in data
            and ('product_quantity' not in data or data['product_quantity'] == 0)):
            raise serializers.ValidationError(
                _("product_quantity must be higher than 0, if affected_product is used."))
        return data

    def create_or_update(self, instance, validated_data):
        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            modifier_detail = self.Meta.model.objects.get(pk=instance.pk)
        else:
            validated_data['sales_order'] = self.context.get('sales_order')
            modifier_detail = self.Meta.model.objects.create(**validated_data)
        return modifier_detail

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class PaymentMethodSerializer(serializers.ModelSerializer):
    is_disabled = serializers.SerializerMethodField()
    enforce_description = serializers.SerializerMethodField()

    class Meta:
        model = PaymentMethod
        fields = '__all__'

    def get_is_disabled(self, obj):
        return obj.is_disabled

    def get_enforce_description(self, obj):
        return obj.enforce_description


class ProductOrderProductAddOnDetailSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    base_cost_read = serializers.CharField(
        source="base_cost.amount", read_only=True)
    base_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    sub_total_read = serializers.CharField(
        source="sub_total.amount", read_only=True)
    sub_total = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    class Meta:
        model = ProductOrderProductAddOnDetail
        exclude = ('product_order', 'account')
        list_serializer_class = AllObjectsListSerializer

    def to_internal_value(self, data):
        self.fields['product'] = serializers.PrimaryKeyRelatedField(
            queryset=Product.all_objects.all())
        return super(ProductOrderProductAddOnDetailSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['product'] = serializers.PrimaryKeyRelatedField(
            queryset=Product.all_objects.all())
        return super(ProductOrderProductAddOnDetailSerializer, self).to_representation(obj)

    def create_or_update(self, instance, validated_data):
        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            modifier_detail = self.Meta.model.objects.get(pk=instance.pk)
        else:
            validated_data['product_order'] = self.context.get('product_order')
            modifier_detail = self.Meta.model.objects.create(**validated_data)
        return modifier_detail

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class ProductOrderModifierOptionDetailSerializer(serializers.ModelSerializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.all_objects.all())
    option = ProductModifierOptionSerializer()

    class Meta:
        model = ProductOrderModifierOptionDetail
        list_serializer_class = AllObjectsListSerializer
        exclude = ('product_order', 'account')

    def to_internal_value(self, data):
        self.fields['option'] = serializers.PrimaryKeyRelatedField(
            queryset=ProductModifierOption.all_objects.all())
        return super(ProductOrderModifierOptionDetailSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['option'] = ProductModifierOptionSerializer()
        return super(ProductOrderModifierOptionDetailSerializer, self).to_representation(obj)

    def create_or_update(self, instance, validated_data):
        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            modifier_detail = self.Meta.model.objects.get(pk=instance.pk)
        else:
            validated_data['product_order'] = self.context.get('product_order')
            modifier_detail = self.Meta.model.objects.create(**validated_data)
        return modifier_detail

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class ProductOrderCompositeDetailSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = ProductOrderCompositeDetail
        list_serializer_class = AllObjectsListSerializer
        exclude = ('product_order', 'account')

    def to_internal_value(self, data):
        self.fields['product'] = serializers.PrimaryKeyRelatedField(
            queryset=Product.all_objects.all())
        return super(ProductOrderCompositeDetailSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['product'] = serializers.PrimaryKeyRelatedField(
            queryset=Product.all_objects.all())
        return super(ProductOrderCompositeDetailSerializer, self).to_representation(obj)

    def create_or_update(self, instance, validated_data):
        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            composite_detail = self.Meta.model.objects.get(pk=instance.pk)
        else:
            validated_data['product_order'] = self.context.get('product_order')
            composite_detail = self.Meta.model.objects.create(**validated_data)
        return composite_detail

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class ProductOrderSerializer(serializers.ModelSerializer):
    discount_fixed_price_read = serializers.CharField(
        source="discount_fixed_price.amount", read_only=True)
    discount_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)

    discount_price_read = serializers.CharField(
        source="discount_price.amount", read_only=True)
    discount_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)

    base_cost_read = serializers.CharField(
        source="base_cost.amount", read_only=True)
    base_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    sub_total_read = serializers.CharField(
        source="sub_total.amount", read_only=True)
    sub_total = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    addon_cost_read = serializers.CharField(
        source="addon_cost.amount", read_only=True)
    addon_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)

    total_cost_read = serializers.CharField(
        source="total_cost.amount", read_only=True)
    total_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    product_cost_read = serializers.CharField(
        source="product_cost.amount", read_only=True, required=False
    )
    product_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)

    product = ProductSerializer(required=True)
    product_sku = serializers.SerializerMethodField()

    addon_detail = ProductOrderProductAddOnDetailSerializer(many=True, required=False)
    composite_detail = ProductOrderCompositeDetailSerializer(many=True, required=False)
    modifier_detail = ProductOrderModifierOptionDetailSerializer(many=True, required=False)

    surcharge_price_read = serializers.CharField(
        source="surcharge_price.amount", read_only=True
    )
    surcharge_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False)
    surcharge_fixed_price_read = serializers.CharField(
        source="surcharge_fixed_price.amount", read_only=True
    )
    surcharge_fixed_price = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False
    )

    class Meta:
        model = ProductOrder
        exclude = ('sales_order', 'account')
        list_serializer_class = AllObjectsListSerializer
        m2m_fields = {
            'composite_detail': 'product_order',
            'modifier_detail': 'product_order',
            'addon_detail': 'product_order',
        }
        m2m_fields_rel = {
            'addon_detail': 'addon_detail',
            'composite_detail': 'composite_detail',
            'modifier_detail': 'modifier_detail',
        }

    def to_internal_value(self, data):
        self.fields['product'] = serializers.PrimaryKeyRelatedField(
            queryset=Product.all_objects.all())
        return super(ProductOrderSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['product'] = serializers.PrimaryKeyRelatedField(
            queryset=Product.all_objects.all())
        return super(ProductOrderSerializer, self).to_representation(obj)

    def prepare_m2m_field(self, validated_data):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.initial_data = self.initial_data.get(m2m_field_name, None)
            validated_data.pop(m2m_field_name, None)
            if self.instance:
                rel = getattr(self.instance, self.Meta.m2m_fields_rel[m2m_field_name])
                m2m_field.instance = rel.all()
        return validated_data

    def save_m2m_field(self, instance):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.context[self.Meta.m2m_fields.get(m2m_field_name)] = instance

            if m2m_field.is_valid():
                m2m_field.save()

    def create_or_update(self, instance, validated_data):
        validated_data = self.prepare_m2m_field(validated_data)

        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            product_order = self.Meta.model.objects.get(pk=instance.pk)
        else:
            validated_data['sales_order'] = self.context.get('sales_order')
            product_order = self.Meta.model.objects.create(**validated_data)

        self.save_m2m_field(product_order)
        return product_order

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def get_product_sku(self, obj):
        return obj.product.sku


class PaymentDetailSerializer(serializers.ModelSerializer):
    amount_read = serializers.CharField(
        source="amount.amount", read_only=True)
    amount = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)
    amount_used_read = serializers.CharField(
        source="amount_used.amount", read_only=True)
    amount_used = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)

    payment_method = PaymentMethodSerializer(required=True)

    class Meta:
        model = PaymentDetail
        exclude = ('sales_order', 'account')
        list_serializer_class = AllObjectsListSerializer

    def to_internal_value(self, data):
        self.fields['payment_method'] = serializers.PrimaryKeyRelatedField(
            queryset=PaymentMethod.objects.all())
        return super(PaymentDetailSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['payment_method'] = PaymentMethodSerializer()
        return super(PaymentDetailSerializer, self).to_representation(obj)

    def create_or_update(self, instance, validated_data):
        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            payment_detail = self.Meta.model.objects.get(pk=instance.pk)
        else:
            validated_data['sales_order'] = self.context.get('sales_order')
            payment_detail = self.Meta.model.objects.create(**validated_data)
        return payment_detail

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class TransactionTypeReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionTypeReason
        exclude = ('account',)


class TransactionTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = TransactionType
        exclude = ('account',)

    def to_internal_value(self, data):
        self.fields['reason'] = serializers.PrimaryKeyRelatedField(
            queryset=TransactionTypeReason.all_objects.all(), required=False)
        return super().to_internal_value(data)

    def to_representation(self, obj):
        self.fields['reason'] = serializers.PrimaryKeyRelatedField(
            queryset=TransactionTypeReason.all_objects.all(), required=False)
        return super().to_representation(obj)


class TransactionOrderSerializer(SalesOrderSerializer):
    product_order = ProductOrderSerializer(many=True, write_only=True, allow_empty=False)
    items = ProductOrderSerializer(many=True, read_only=True)

    payment_detail = PaymentDetailSerializer(many=True, required=False)
    promo_list = SalesOrderPromoHistorySerializer(many=True, required=False)
    transaction_type = TransactionTypeSerializer(required=False)
    order_option = SalesOrderOptionSerializer(required=False)
    invoice = SalesOrderInvoiceSerializer(required=False)

    class Meta(SalesOrderSerializer.Meta):
        m2m_fields = {
            'product_order': 'sales_order',
            'payment_detail': 'sales_order',
            'promo_list': 'sales_order',
        }
        m2m_fields_rel = {
            'product_order': 'items',
            'payment_detail': 'payment_detail',
            'promo_list': 'promo_list',
        }

    def to_internal_value(self, data):
        self.fields['customer'] = serializers.PrimaryKeyRelatedField(
            queryset=Customer.all_objects.all(), required=False)
        self.fields['order_option'] = serializers.PrimaryKeyRelatedField(
            queryset=SalesOrderOption.all_objects.all(), required=False)
        self.fields['table'] = serializers.PrimaryKeyRelatedField(
            queryset=Table.all_objects.all(), required=False)
        return super(TransactionOrderSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['order_option'] = SalesOrderOptionSerializer(required=False)
        response = super(TransactionOrderSerializer, self).to_representation(obj)
        response["items"] = sorted(response["items"], key=lambda x: x["id"])
        return response

    def prepare_m2m_field(self, validated_data):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.initial_data = self.initial_data.get(m2m_field_name, None)
            validated_data.pop(m2m_field_name, None)
            if self.instance:
                rel = getattr(self.instance, self.Meta.m2m_fields_rel[m2m_field_name])
                m2m_field.instance = rel.all()
        return validated_data

    def save_m2m_field(self, instance):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.context[self.Meta.m2m_fields.get(m2m_field_name)] = instance

            if m2m_field.is_valid():
                m2m_field.save()

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs.get("status") == SalesOrder.STATUS.settled and len(attrs.get("payment_detail"))<=0:
            raise serializers.ValidationError(
                {
                    "payment_detail": ["Payment Detail cannot be empty for settled transaction."]
                }
            )

        if 'invoice' in attrs:
            if isinstance(attrs['invoice']['customer'], Customer):
                attrs['invoice']['customer'] = attrs['invoice']['customer'].pk

        return attrs

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def create_or_update(self, instance, validated_data):
        validated_data = self.prepare_m2m_field(validated_data)

        transaction_type_f = self.fields.get('transaction_type')
        transaction_type_f.initial_data = validated_data.get('transaction_type', None)

        if transaction_type_f.initial_data and 'reason' in transaction_type_f.initial_data:
            transaction_type_f.initial_data['reason'] = transaction_type_f.initial_data['reason'].pk

        if transaction_type_f.is_valid():
            validated_data['transaction_type'] = transaction_type_f.save()

        invoice_f = self.fields.get('invoice')
        invoice_f.initial_data = validated_data.get('invoice', None)
        if invoice_f.is_valid():
            validated_data['invoice'] = invoice_f.save()

        if not instance:
            instance = self.Meta.model.objects.create(**validated_data)
        else:
            for key in validated_data:
                if key in self.Meta.allowed_update_field:
                    setattr(instance, key, validated_data[key])
            instance.save()

        self.save_m2m_field(instance)
        return instance

    def save(self, **kwargs):
        qs = self.Meta.model.objects.filter(
            code=self.initial_data.get('code'))
        if qs.exists():
            self.instance = qs.first()
        return super(
            TransactionOrderSerializer, self).save(**kwargs)


class CashierActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = CashierActivity
        exclude = ('account',)


class CashierActivityReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = CashierActivityReason
        exclude = ('account',)


class SalesOrderSentReceiptSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def save(self, sales_order):
        sales_order.sent_email(
            [self.validated_data.get('email')])
        return sales_order


class SalesReportSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesReportSetting
        exclude = ('account',)


class PromoUseCheckSerializer(serializers.Serializer):
    promo = serializers.IntegerField(write_only=True)
    outlet = serializers.IntegerField(write_only=True)
    affected_customer = serializers.IntegerField(write_only=True, required=False, allow_null=True)
    total_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False, allow_null=True)

    def validate(self, data):
        promo_id = data['promo']
        promo = Promo.objects.get(id=promo_id)
        promo_date = promo.start_date
        amount_limit = promo.amount_limit
        start_date = end_date = None
        if promo.usages_limit == Promo._USAGES_LIMIT.use_limit:
            if not promo.limit_per_customer:
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    if promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                               sales_order__transaction_date__lte=end_date,
                                                               sales_order__outlet=data['outlet'],
                                                               sales_order__status='settled').count() >= promo.promo_limit:
                        raise serializers.ValidationError(
                            {"promo_limit": [_("Promo can't be use anymore because limitation is "
                                               "over")]})
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    if promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                               sales_order__transaction_date__lte=end_date,
                                                               sales_order__outlet=data['outlet'],
                                                               sales_order__status='settled').count() >= promo.promo_limit:
                        raise serializers.ValidationError(
                            {"promo_limit": [_("Promo can't be use anymore because limitation is "
                                               "over")]})
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    if promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                               sales_order__transaction_date__lte=end_date,
                                                               sales_order__outlet=data['outlet'],
                                                               sales_order__status='settled').count() >= promo.promo_limit:
                        raise serializers.ValidationError(
                            {"promo_limit": [_("Promo can't be use anymore because limitation is "
                                               "over")]})
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                    if promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=promo_date,
                                                               sales_order__outlet=data['outlet'],
                                                               sales_order__status='settled').count() >= promo.promo_limit:
                        raise serializers.ValidationError(
                            {"promo_limit": [_("Promo can't be use anymore because limitation is "
                                               "over")]})
            else:
                if 'affected_customer' not in data:
                    raise serializers.ValidationError({"require fields": [_("This Promo type need affected_customer "
                                                                            "field to tracking by total sales")]})
                else:
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                        start_date = datetime.today().replace(
                            hour=0, minute=0, second=0, microsecond=0)
                        end_date = start_date + relativedelta(hour=23, minute=59)
                        if promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                   sales_order__transaction_date__gte=start_date,
                                                                   sales_order__transaction_date__lte=end_date,
                                                                   sales_order__status='settled').count() >= promo.promo_limit:
                            raise serializers.ValidationError({"promo_limit": [_("Promo can't be use anymore because limit per "
                                                                                 "customer is over")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                        start_date = datetime.today().replace(
                            hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                        end_date = datetime.today().replace(hour=23, minute=59)
                        if promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                   sales_order__transaction_date__gte=start_date,
                                                                   sales_order__transaction_date__lte=end_date,
                                                                   sales_order__status='settled').count() >= promo.promo_limit:
                            raise serializers.ValidationError({"promo_limit": [_("Promo can't be use anymore because limit per "
                                                                                 "customer is over")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                        start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                        end_date = datetime.today().replace(hour=23, minute=59)
                        if promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                   sales_order__transaction_date__gte=start_date,
                                                                   sales_order__transaction_date__lte=end_date,
                                                                   sales_order__status='settled').count() >= promo.promo_limit:
                            raise serializers.ValidationError({"promo_limit": [_("Promo can't be use anymore because limit per "
                                                                                 "customer is over")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                        if promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                   sales_order__transaction_date__gte=promo_date,
                                                                   sales_order__status='settled').count() >= promo.promo_limit:
                            raise serializers.ValidationError({"promo_limit": [_("Promo can't be use anymore because limit per "
                                                                                 "customer is over")]})

        elif promo.usages_limit == Promo._USAGES_LIMIT.amount_limit:
            if promo.affected_product.all().count() > 0 or promo.affected_category.all().count() > 0:
                if 'affected_customer' not in data:
                    raise serializers.ValidationError({"require fields": [_("This Promo type need affected_customer "
                                                                            "field to tracking by total sales")]})
                else:
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                        start_date = datetime.today().replace(
                            hour=0, minute=0, second=0, microsecond=0)
                        end_date = start_date + relativedelta(hour=23, minute=59)
                        current_amount = promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                                 sales_order__transaction_date__gte=start_date,
                                                                                 sales_order__transaction_date__lte=end_date,
                                                                                 sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError({"promo_limit": [_("Total cost isn't enough to use this promo")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                        start_date = datetime.today().replace(
                            hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                        end_date = datetime.today().replace(hour=23, minute=59)
                        current_amount = promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                                 sales_order__transaction_date__gte=start_date,
                                                                                 sales_order__transaction_date__lte=end_date,
                                                                                 sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError({"promo_limit": [_("Total cost isn't enough to use this promo")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                        start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                        end_date = datetime.today().replace(hour=23, minute=59)
                        current_amount = promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                                 sales_order__transaction_date__gte=start_date,
                                                                                 sales_order__transaction_date__lte=end_date,
                                                                                 sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError({"promo_limit": [_("Total cost isn't enough to use this promo")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                        current_amount = promo.salesorderpromohistory_set.filter(
                            affected_customer=data['affected_customer'],
                            sales_order__transaction_date__gte=promo_date,
                            sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError(
                                {"promo_limit": [_("Total cost isn't enough to use this promo")]})
            else:
                if 'affected_customer' not in data:
                    raise serializers.ValidationError({"require fields": [_("This Promo type need affected_customer "
                                                                            "field to tracking by total sales")]})
                else:
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                        start_date = datetime.today().replace(
                            hour=0, minute=0, second=0, microsecond=0)
                        end_date = start_date + relativedelta(hour=23, minute=59)
                        current_amount = promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                                 sales_order__transaction_date__gte=start_date,
                                                                                 sales_order__transaction_date__lte=end_date,
                                                                                 sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum('affected_value'), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError({"promo_limit": [_("Total cost isn't enough to use this promo")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                        start_date = datetime.today().replace(
                            hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                        end_date = datetime.today().replace(hour=23, minute=59)
                        current_amount = promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                                 sales_order__transaction_date__gte=start_date,
                                                                                 sales_order__transaction_date__lte=end_date,
                                                                                 sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum('affected_value'), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError({"promo_limit": [_("Total cost isn't enough to use this promo")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                        start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                        end_date = datetime.today().replace(hour=23, minute=59)
                        current_amount = promo.salesorderpromohistory_set.filter(affected_customer=data['affected_customer'],
                                                                                 sales_order__transaction_date__gte=start_date,
                                                                                 sales_order__transaction_date__lte=end_date,
                                                                                 sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum('affected_value'), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError({"promo_limit": [_("Total cost isn't enough to use this promo")]})
                    if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                        current_amount = promo.salesorderpromohistory_set.filter(
                            affected_customer=data['affected_customer'],
                            sales_order__transaction_date__gte=promo_date,
                            sales_order__status='settled').aggregate(
                            total_amount=Coalesce(Sum('affected_value'), Value(0),
                                                  output_field=DecimalField())).get(
                            'total_amount')
                        sisa = amount_limit - current_amount
                        if data['total_cost'] > sisa:
                            raise serializers.ValidationError(
                                {"promo_limit": [_("Total cost isn't enough to use this promo")]})

        return data

    def save(self, **kwargs):
        promo_id = self.validated_data.get('promo')
        outlet_id = self.validated_data.get('outlet')
        promo = Promo.objects.get(id=promo_id)
        promo_date = promo.start_date
        amount_limit = promo.amount_limit
        if promo.usages_limit == Promo._USAGES_LIMIT.use_limit:
            if not promo.limit_per_customer:
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          sales_order__outlet=outlet_id,
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(current_use,
                                                                                                            promo_limit)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          sales_order__outlet=outlet_id,
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(current_use,
                                                                                                            promo_limit)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          sales_order__outlet=outlet_id,
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(current_use,
                                                                                                            promo_limit)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=promo_date,
                                                                          sales_order__outlet=outlet_id,
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(current_use,
                                                                                                            promo_limit)}
                    return response

            else:
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          affected_customer=self.validated_data.get(
                                                                              'affected_customer'),
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(
                        current_use,
                        promo_limit)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          affected_customer=self.validated_data.get(
                                                                              'affected_customer'),
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(
                        current_use,
                        promo_limit)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          affected_customer=self.validated_data.get(
                                                                              'affected_customer'),
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(
                        current_use,
                        promo_limit)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                    current_use = promo.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=promo_date,
                                                                          affected_customer=self.validated_data.get(
                                                                              'affected_customer'),
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = promo.promo_limit
                    response = {'success': _('You have used this promo {} times. Promo limit is {}').format(
                        current_use,
                        promo_limit)}
                    return response

        elif promo.usages_limit == Promo._USAGES_LIMIT.amount_limit:
            if promo.affected_product.all().count() > 0 or promo.affected_category.all().count() > 0:
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(

                        ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(

                        ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(

                        ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=promo_date, sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(

                        ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response
            else:
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(

                                              ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {
                        'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(

                                              ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {
                        'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(

                                              ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {
                        'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response
                if promo.recurrence_limit == _TIME_LIMIT_CHOICES.no_recurrence:
                    current_amount = promo.salesorderpromohistory_set.filter(
                        affected_customer=self.validated_data.get('affected_customer'),
                        sales_order__transaction_date__gte=promo_date, sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(

                                              ))).get(
                        'total_amount')
                    sisa = amount_limit - current_amount
                    current_total = self.validated_data.get('total_cost')
                    total = sisa - Decimal(current_total)
                    response = {
                        'success': _('You have used this promo. Your amount limit is IDR {} more').format(total)}
                    return response


class PromoByCustomerSerializer(serializers.ModelSerializer):
    uses_limit = serializers.SerializerMethodField()
    amount_limit = serializers.SerializerMethodField()

    class Meta:
        model = Promo
        fields = ['id', 'name', 'uses_limit', 'amount_limit']

    def get_uses_limit(self, obj, *args, **kwargs):
        customer_id = self.context.get('view').kwargs.get('pk')
        outlet_id = self.context.get('view').kwargs.get('outlet_pk')
        start_date = end_date = None
        if obj.usages_limit == Promo._USAGES_LIMIT.use_limit:
            if not obj.limit_per_customer:
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                        sales_order__transaction_date__lte=end_date,
                                                                        sales_order__status='settled').count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                        sales_order__transaction_date__lte=end_date,
                                                                        sales_order__status='settled').count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                        sales_order__transaction_date__lte=end_date,
                                                                        sales_order__status='settled').count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs
                else:
                    start_date = obj.start_date
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                        sales_order__status='settled').count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs
            else:
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          affected_customer=int(customer_id),
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          affected_customer=int(customer_id),
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                          sales_order__transaction_date__lte=end_date,
                                                                          affected_customer=int(customer_id),
                                                                          sales_order__status='settled'
                                                                          ).count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs
                else:
                    start_date = obj.start_date
                    current_use = obj.salesorderpromohistory_set.filter(sales_order__transaction_date__gte=start_date,
                                                                        affected_customer=int(customer_id),
                                                                        sales_order__status='settled'
                                                                        ).count()
                    promo_limit = obj.promo_limit
                    qs = promo_limit - current_use
                    return qs

    def get_amount_limit(self, obj):
        customer_id = self.context.get('view').kwargs.get('pk')
        start_date = end_date = None
        if obj.usages_limit == Promo._USAGES_LIMIT.amount_limit:
            if obj.affected_product.all().count() > 0 or obj.affected_category.all().count() > 0:
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(

                        ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(

                        ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(
                        ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
                else:
                    start_date = obj.start_date
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum(F('affected_value')*F('product_quantity')), Value(0),
                                              output_field=DecimalField(
                        ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
            else:
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0)
                    end_date = start_date + relativedelta(hour=23, minute=59)
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(

                                              ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
                    start_date = datetime.today().replace(
                        hour=0, minute=0, second=0, microsecond=0) + relativedelta(weekday=MO(-1))
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(

                                              ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
                if obj.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
                    start_date = datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    end_date = datetime.today().replace(hour=23, minute=59)
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date, sales_order__transaction_date__lte=end_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(
                                              ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
                else:
                    start_date = obj.start_date
                    current_amount = obj.salesorderpromohistory_set.filter(
                        affected_customer=customer_id,
                        sales_order__transaction_date__gte=start_date,
                        sales_order__status='settled').aggregate(
                        total_amount=Coalesce(Sum('affected_value'), Value(0),
                                              output_field=DecimalField(
                                              ))).get(
                        'total_amount')
                    qs = obj.amount_limit - current_amount
                    return qs
