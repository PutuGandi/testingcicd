import datetime

from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _

from sterlingpos.core.midtrans.adapter import midtrans_adapter
from sterlingpos.outlet.models import Outlet
from sterlingpos.device.models import DeviceUser
from sterlingpos.subscription.models import (
    BaseSubscription,
    Subscription,
    SubscriptionPlan,
    
    Billing,
    SubscriptionJournal,
    OrderPayment,

    GooglePlaySubscriptionLog,
    DeviceUserLiteSubscription,
    OtherSubscription,
)


class SubscriptionSerializer(serializers.ModelSerializer):
    subscription_plan = serializers.CharField(
        source='subscription_plan.name', read_only=True)

    class Meta:
        model = Subscription
        fields = (
            'subscription_plan', 'expires', 'active', 'cancelled', 'outlet')


class GooglePlaySubscriptionLogSerializer(serializers.ModelSerializer):
    device_user = serializers.PrimaryKeyRelatedField(
        queryset=DeviceUser.objects.all(),
        source='device_user_lite_subscription.device_user',
        allow_null=True, required=False)
    outlet = serializers.PrimaryKeyRelatedField(
        queryset=Outlet.objects.all(),
        source='outlet_lite_subscription.subscription.outlet',
        allow_null=True, required=False)

    class Meta:
        model = GooglePlaySubscriptionLog
        exclude = ('account', )
        extra_kwargs = {
            'order_id': {'required': True},
            'product_id': {'required': True},
            'purchase_token': {'required': True},
        }

    def validate_subscription_type(self, value):
        expiracy_required_subscription = ["device_user_subscription", "outlet_subscription"]
        if value in expiracy_required_subscription and "expiracy" not in self.initial_data:
            raise serializers.ValidationError(
                _("device_user and outlet subcription requires expiracy to be set"))
        return value

    def validate_device_user(self, value):
        if self.initial_data.get("subscription_type") != "device_user_subscription":
            raise serializers.ValidationError(
                _("device_user subcription requires subscription_type to be device_user_subscription"))

        if (self.initial_data.get("subscription_type") == "device_user_subscription"
                and not value):
            raise serializers.ValidationError(
                _("device_user field must not be Null"))
        return value

    def validate_outlet(self, value):
        if self.initial_data.get("subscription_type") != "outlet_subscription":
            raise serializers.ValidationError(
                _("outlet subscription requires subscription_type to be outlet_subscription"))

        if (self.initial_data.get("subscription_type") == "outlet_subscription"
                and not value):
            raise serializers.ValidationError(
                _("outlet field must not be Null"))
        return value

    def save(self):
        if ("outlet_lite_subscription" in self.validated_data and 
                self.validated_data['subscription_type'] == "outlet_subscription"):
            self.validated_data["outlet_lite_subscription"] = self.validated_data.get(
                "outlet_lite_subscription"
            ).get('subscription').get("outlet").subscription.outletlitesubscription

        if ("device_user_lite_subscription" in self.validated_data and
                self.validated_data['subscription_type'] == "device_user_subscription"):
            device_user = self.validated_data.get("device_user_lite_subscription").get("device_user")
            deviceuserlitesubscription, _ = DeviceUserLiteSubscription.objects.get_or_create(
                device_user=device_user,
            )
            self.validated_data["device_user_lite_subscription"] = deviceuserlitesubscription

        return super(GooglePlaySubscriptionLogSerializer, self).save()


class SubscriptionPlanSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubscriptionPlan
        fields = "__all__"


class SubscriptionJournalSerializer(serializers.ModelSerializer):

    device_user = serializers.PrimaryKeyRelatedField(
        queryset=DeviceUser.objects.all(),
        source='device_user_subscription.device_user',
        allow_null=True, required=False)
    outlet = serializers.PrimaryKeyRelatedField(
        queryset=Outlet.objects.all(),
        source='subscription.outlet',
        allow_null=True, required=False)

    class Meta:
        model = SubscriptionJournal
        exclude = ("account", "billing")
        read_only = (
            "price", "current_plan",
            "device_user_subscription", "subscription",
            "other_subscription")

    def __init__(self, *args, **kwargs):
        super(SubscriptionJournalSerializer, self).__init__(*args, **kwargs)
        self.fields["device_user"].queryset = DeviceUser.objects.all()
        self.fields["outlet"].queryset = Outlet.objects.all()
    
    def create(self, validated_data):
        validated_data["billing"] = self.context.get("billing")
        subscription_plan_type = validated_data['choosen_plan'].plan_type

        if subscription_plan_type == "others":
            other_subscription, _ = OtherSubscription.objects.get_or_create(
                subscription_plan=validated_data['choosen_plan'],
            )
            validated_data["other_subscription"] = other_subscription

        if (subscription_plan_type == "outlet_subscription" and
                "subscription" in validated_data):
            validated_data["subscription"] = validated_data.get(
                "subscription").get("outlet").subscription

        if (subscription_plan_type == "device_user_subscription" and
                "device_user_subscription" in validated_data):
            device_user = validated_data.get("device_user_subscription").get("device_user")
            deviceuserlitesubscription, _ = DeviceUserLiteSubscription.objects.get_or_create(
                device_user=device_user,
            )
            validated_data["device_user_subscription"] = deviceuserlitesubscription

        return super(
            SubscriptionJournalSerializer, self).create(validated_data)


class OrderPaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderPayment
        exclude = ("account",)


class BillingSerializer(serializers.ModelSerializer):

    subscription_detail = SubscriptionJournalSerializer(
        write_only=True, many=True)
    subscription_detail_read = SubscriptionJournalSerializer(
        read_only=True, many=True, source="subscriptionjournal_set")
    order_payment = OrderPaymentSerializer(
        many=True, read_only=True, source="orderpayment_set")

    class Meta:
        model = Billing
        exclude = ("account", "subscriptions")
        extra_kwargs = {
            "status": {
                "read_only": True,
            },
            "subscriptions": {
                "required": False, "allow_null": True},
        }
        m2m_fields = {
            'subscription_detail': 'billing',
        }
        m2m_fields_rel = {
            'subscription_detail': 'billing_detail',
        }

    def prepare_m2m_field(self, validated_data):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.initial_data = self.initial_data.get(m2m_field_name, None)
            validated_data.pop(m2m_field_name, None)
            if self.instance:
                rel = getattr(self.instance, self.Meta.m2m_fields_rel[m2m_field_name])
                m2m_field.instance = rel.all()
        return validated_data

    def save_m2m_field(self, instance):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.context[self.Meta.m2m_fields.get(m2m_field_name)] = instance

            if m2m_field.is_valid():
                m2m_field.save()

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        validated_data['due_date'] = (
            datetime.date.today() + datetime.timedelta(days=7))
        return self.create_or_update(None, validated_data)

    def create_or_update(self, instance, validated_data):
        validated_data = self.prepare_m2m_field(validated_data)

        if not instance:
            instance = self.Meta.model.objects.create(**validated_data)
        else:
            for key in validated_data:
                if key in self.Meta.allowed_update_field:
                    setattr(instance, key, validated_data[key])
            instance.save()

        self.save_m2m_field(instance)

        instance.save(recalculate=True, notify_invoice=True)

        return instance


class MidtransBillingSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()

    class Meta:
        model = Billing
        fields = ("token", )

    def get_token(self, obj):
        payment_attempt_code = obj.generate_payment_attempt_code()
        snap_token = midtrans_adapter.get_snap_charge_token(
            obj, order_id=payment_attempt_code,
            view_request=self.context.get("request"))
        return snap_token
