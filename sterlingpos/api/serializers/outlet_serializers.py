import uuid
import logging

from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from sterlingpos.outlet.models import Outlet
from sterlingpos.api.serializers.mixins import ListSerializer
from sterlingpos.subscription.models import SubscriptionPlan, Subscription

logger = logging.getLogger(__name__)


class BasicOutletSerializer(serializers.ModelSerializer):

    class Meta:
        model = Outlet
        list_serializer_class = ListSerializer
        exclude = ('account', )


class OutletSerializer(BasicOutletSerializer):
    province_read = serializers.CharField(
        source="province.name", read_only=True)
    city_read = serializers.CharField(
        source="city.name", read_only=True)
    subscription_plan_read = serializers.CharField(
        source="subscription.subscription_plan.slug",
        read_only=True)

    table_management = serializers.SerializerMethodField(read_only=True)
    waiter_management = serializers.SerializerMethodField(read_only=True)
    kitchen_display = serializers.SerializerMethodField(read_only=True)

    is_expired = serializers.SerializerMethodField(read_only=True)

    def __init__(self, *args, **kwargs):
        super(OutletSerializer, self).__init__(*args, **kwargs)
        self.fields["device_user"].read_only = True

    def to_representation(self, obj):
        self.fields['device_users'] = self.fields['device_user']
        return super(OutletSerializer, self).to_representation(obj)

    def to_internal_value(self, data):
        self.fields['device_users'] = self.fields['device_user']
        return super(OutletSerializer, self).to_internal_value(data)

    def get_table_management(self, obj):
        if 'table_management_setting' in self.context:
            main_table_management_setting = self.context.get('table_management_setting')
        else:
            main_table_management_setting = obj.account.tablemanagementsettings_set.first()

        table_management_data = {
            'display_color_status': main_table_management_setting.display_color_status,
            'seat_management': main_table_management_setting.seat_management,
            'color_display': main_table_management_setting.color_display,
            'is_enable': obj.table_management_setting.exists(),
        }
        return table_management_data

    def get_kitchen_display(self, obj):
        kitchen_display_data = {
            'is_enable': hasattr(obj, 'kitchendisplayoutlet'),
        }
        if hasattr(obj, 'kitchendisplayoutlet'):
            kitchen_display_data.update({
                'urgent_time': obj.kitchendisplayoutlet.urgent_time,
            })
        return kitchen_display_data

    def get_waiter_management(self, obj):
        waiters_settings_data = {
            'is_enable': obj.waiter_setting.exists(),
        }
        return waiters_settings_data

    def get_is_expired(self, obj):
        if hasattr(obj, 'subscription'):
            return obj.subscription.expired
        return False

    def get_mobile_subscription_plan(self):
        try:
            subscription_plan = SubscriptionPlan.objects.get(slug="mobile-lite")
        except SubscriptionPlan.DoesNotExist:
            subscription_plan = SubscriptionPlan.objects.create(
                name='Mobile Subscription',
                slug='mobile-lite',
                price=85000,
                trial_period=0,
                trial_unit=None,
                recurrence_period=0,
                recurrence_unit=None,
                is_public=False,
            )
        return subscription_plan

    def create(self, validated_data):
        if 'branch_id' not in validated_data:
            validated_data['branch_id'] = uuid.uuid4().hex[:8]

        instance = super(OutletSerializer, self).create(validated_data)
        subscription = Subscription.objects.create(
            subscription_plan=self.get_mobile_subscription_plan(),
            outlet=instance,
            account=instance.account,
            is_trial=False,
        )
        return instance


class OutletReadOnlySerializer(OutletSerializer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name in self.fields:
            self.fields[field_name].read_only = True


class OutletAuthTokenSerializer(serializers.Serializer):
    branch_id = serializers.CharField()

    def validate_branch_id(self, value):
        if Outlet.objects.filter(branch_id=value).exists():
            outlet = Outlet.objects.filter(branch_id=value).first()
            if hasattr(outlet, 'subscription') and outlet.subscription.expired:
                logger.info(f"Unable to authenticate, outlet {outlet} is expired.")
                raise serializers.ValidationError(
                    _("Outlet Subscription is expired"))
        else:
            raise serializers.ValidationError(
                _("Outlet does not Exists"))
        return outlet
