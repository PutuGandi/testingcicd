from django.conf import settings
from rest_framework import serializers
from django.utils.translation import ugettext as _

from djmoney.contrib.django_rest_framework.fields import MoneyField

from sterlingpos.promo.models import (
    Promo, PromoReward,
    OutletPromo, ProductPromo, PaymentMethodPromo,
    CustomerPromo, CustomerGroupPromo, ItemPromoReward, CategoryPromo, PromoTag, TagPromo
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.catalogue.models import Product, Category
from sterlingpos.sales.models import PaymentMethod
from sterlingpos.customer.models import Customer, CustomerGroup


class PromoRewardSerializer(serializers.ModelSerializer):
    class Meta:
        model = PromoReward
        exclude = ('promo', 'account')


class ItemPromoRewardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemPromoReward
        exclude = ('promo', 'account')


class PromoTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = PromoTag
        exclude = ('account',)


class PromoSerializer(serializers.ModelSerializer):
    promo_reward = PromoRewardSerializer(required=True, source='promoreward')
    item_promo_reward = ItemPromoRewardSerializer(required=False, source='itempromoreward')

    class Meta:
        model = Promo
        exclude = ('account', )

    def __init__(self, *args, **kwargs):
        super(PromoSerializer, self).__init__(*args, **kwargs)
        self.fields["affected_product"] = serializers.PrimaryKeyRelatedField(
            many=True,
            required=False,
            allow_empty=True,
            queryset=Product.objects.exclude(classification=Product.PRODUCT_CLASSIFICATION.complementary))
        self.fields["affected_payment_method"] = serializers.PrimaryKeyRelatedField(
            many=True,
            required=False,
            allow_empty=True,
            queryset=PaymentMethod.objects.all())
        self.fields["outlet"] = serializers.PrimaryKeyRelatedField(
            many=True, queryset=Outlet.objects.all())
        self.fields["affected_customer"] = serializers.PrimaryKeyRelatedField(
            many=True,
            required=False,
            allow_empty=True,
            queryset=Customer.objects.all()
        )
        self.fields['affected_group'] = serializers.PrimaryKeyRelatedField(
            many=True,
            required=False,
            allow_empty=True,
            queryset=CustomerGroup.objects.all()
        )
        self.fields['affected_category'] = serializers.PrimaryKeyRelatedField(
            many=True,
            required=False,
            allow_empty=True,
            queryset=Category.objects.all()
        )
        self.fields['tags'] = serializers.PrimaryKeyRelatedField(
            many=True,
            required=False,
            allow_empty=True,
            queryset=PromoTag.objects.all()
        )

    def to_representation(self, obj):
        self.fields['affected_products'] = self.fields['affected_product']
        return super(PromoSerializer, self).to_representation(obj)

    def to_internal_value(self, data):
        self.fields['affected_products'] = self.fields['affected_product']
        return super(PromoSerializer, self).to_internal_value(data)

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def update_m2m(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        if len(object_list) != 0:
            if isinstance(object_list, list):
                obj_ids = set(map(lambda x: x.id, object_list))
            else:
                obj_ids = set(
                    object_list.values_list("id", flat=True))
            current_ids = set(
                rel_field.values_list("id", flat=True)
            )
            add_ids = obj_ids - current_ids
            delete_ids = current_ids - obj_ids
            model_objects = [
                intermediate_model(
                    **{
                        "{}_id".format(rel_field.target_field_name): obj_id,
                        "{}".format(intermediate_model_ref.field.name): instance,
                        "account": instance.account
                    }
                )
                for obj_id in add_ids
            ]
            intermediate_model.objects.bulk_create(model_objects)
            intermediate_model.objects.complex_filter({
                "{}__pk__in".format(rel_field.target_field_name): delete_ids,
                "{}".format(intermediate_model_ref.field.name): instance,
            }).delete()
        else:
            rel_field.clear()
        rel_field.invalidated_update()

    def create_or_update(self, instance, validated_data):
        outlet_data = validated_data.pop('outlet', None)
        affected_product = validated_data.pop(
            'affected_product', [])
        affected_payment_method = validated_data.pop(
            'affected_payment_method', [])
        affected_customer = validated_data.pop(
            'affected_customer', []
        )
        affected_group = validated_data.pop(
            'affected_group', []
        )
        affected_category = validated_data.pop(
            'affected_category', []
        )
        tags = validated_data.pop(
            'tags', []
        )

        promo_reward_f = self.fields.get('promo_reward')
        item_promo_reward_f = self.fields.get('item_promo_reward')
        if instance:
            promo_reward_f.instance = getattr(
                instance, "promoreward", None)
            item_promo_reward_f.instance = getattr(
                instance, "itempromoreward", None)

        promo_reward_f.initial_data = validated_data.pop(
            'promoreward', None)
        item_promo_reward_f.initial_data = validated_data.pop(
            'itempromoreward', None
        )

        if not instance:
            instance = self.Meta.model.objects.create(
                **validated_data)
        else:
            for key in validated_data:
                setattr(instance, key, validated_data[key])
            instance.save()

        if promo_reward_f.is_valid():
            promo_reward_f.validated_data["promo"] = instance
            promo_reward_f.save()

        if item_promo_reward_f.is_valid():
            item_promo_reward_f.validated_data['promo'] = instance
            item_promo_reward_f.save()

        if outlet_data:
            instance.outlet.clear()
            for outlet in outlet_data:
                OutletPromo.objects.create(
                    promo=instance,
                    outlet=outlet
                )
            instance.outlet.invalidated_update()

        self.update_m2m(
            instance, affected_product,
            instance.affected_product,
            ProductPromo, ProductPromo.promo)

        self.update_m2m(
            instance, affected_payment_method,
            instance.affected_payment_method,
            PaymentMethodPromo, PaymentMethodPromo.promo)

        self.update_m2m(instance, affected_customer,
                        instance.affected_customer,
                        CustomerPromo, CustomerPromo.promo
                        )
        self.update_m2m(instance, affected_group,
                        instance.affected_group,
                        CustomerGroupPromo, CustomerGroupPromo.promo
                        )
        self.update_m2m(instance, affected_category,
                        instance.affected_category,
                        CategoryPromo, CategoryPromo.promo
        )
        self.update_m2m(instance, tags,
                        instance.tags,
                        TagPromo, TagPromo.promo)
        return instance


class PromoLimitCheckSerializer(serializers.Serializer):
    promo = serializers.IntegerField(write_only=True)
    outlet = serializers.IntegerField(write_only=True)
    affected_customer = serializers.IntegerField(
        write_only=True, required=False, allow_null=True)
    total_cost = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True, required=False, allow_null=True)

    def validate_promo(self, value):
        try:
            promo = Promo.objects.get(id=value)
        except Promo.DoesNotExist:
            raise serializers.ValidationError(_("Promo does not exist"))
        return promo

    def validate(self, data):
        promo = data.get("promo")
        customer_id = data.get("affected_customer")
        if promo.limit_per_customer and not customer_id:
            raise serializers.ValidationError(
                {"require fields": [_("This Promo type need affected_customer "
                                      "field to tracking by total sales")]})

        if not promo.outlet.filter(id=data.get("outlet")).exists():
            raise serializers.ValidationError(
                {"unauthorized_outlet": [
                    _("This Promo is not available for this Outlet.")]})

        return data

    def save(self, **kwargs):
        promo = self.validated_data.get('promo')
        outlet = self.validated_data.get('outlet')
        affected_customer = self.validated_data.get('affected_customer')
        total_cost = self.validated_data.get('total_cost')
        promo_date = promo.start_date
        amount_limit = promo.amount_limit
        limit_data = promo.get_limitation_for(
            total_cost=total_cost,
            outlet=outlet,
            affected_customer=affected_customer,
        )
        return limit_data
