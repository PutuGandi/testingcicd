import itertools
import copy

from rest_framework import serializers


class ListSerializer(serializers.ListSerializer):

    def copy_serializer(self, child_serializer):
        return copy.deepcopy(child_serializer)

    def save(self, **kwargs):
        ret = []
        if self.instance:
            for instance, initial_data in itertools.zip_longest(self.instance, self.initial_data):
                child_serializer = copy.deepcopy(self.child)
                child_serializer.root._context = self.context
                child_serializer.initial_data = initial_data
                child_serializer.instance = instance

                if instance and not initial_data:
                    instance.delete()
                else:
                    child_serializer.is_valid()
                    ret.append(child_serializer.save())
        else:
            for initial_data in self.initial_data:
                
                child_serializer = self.copy_serializer(self.child)

                child_serializer.root._context = self.context
                child_serializer.initial_data = initial_data
                child_serializer.is_valid()
                ret.append(child_serializer.save())
        self.instance = ret
        return self.instance


class AllObjectsListSerializer(ListSerializer):

    def to_representation(self, data):
        if type(data) != list:
            data = data.all(force_visibility=True)
        return super(AllObjectsListSerializer, self).to_representation(data)


class ExcludeArchiveListSerializer(ListSerializer):

    def to_representation(self, data):
        if type(data) != list:
            data = data.exclude(archived=True)
        return super(ExcludeArchiveListSerializer, self).to_representation(data)
