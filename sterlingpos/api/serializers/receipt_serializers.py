from rest_framework import serializers

from sterlingpos.receipt.models import Receipt


class ReceiptSerializer(serializers.ModelSerializer):

    class Meta:
        model = Receipt
        exclude = ('account', )
