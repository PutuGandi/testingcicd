from rest_framework import serializers

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from djmoney.contrib.django_rest_framework.fields import MoneyField

from sterlingpos.api.serializers.mixins import (
    ListSerializer,
)
from sterlingpos.device.models import DeviceUser
from sterlingpos.cash_balance.models import (
    CashBalanceLog, CashBalance, CashBalanceSetting, BalanceDetail
)


class BalanceDetailSerializer(serializers.ModelSerializer):
    balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)
    balance_read = serializers.CharField(
        source="balance.amount", read_only=True)

    class Meta:
        model = BalanceDetail
        exclude = (
            'account',
            'cash_balance', 'cash_balance_log')
        list_serializer_class = ListSerializer

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def create_or_update(self, instance, validated_data):
        if not instance:
            validated_data['cash_balance_log'] = self.context.get(
                'cash_balance_log')
            instance = self.Meta.model.objects.create(**validated_data)
        else:
            for key in validated_data:
                setattr(instance, key, validated_data[key])
            instance.save()

        return instance

class CashBalanceLogSerializer(serializers.ModelSerializer):

    balance_read = serializers.CharField(
        source="balance.amount", read_only=True)
    balance = MoneyField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        write_only=True)
    balance_read = serializers.CharField(
        source="balance.amount", read_only=True)

    balance_list = BalanceDetailSerializer(many=True, required=False)

    class Meta:
        model = CashBalanceLog
        exclude = ('account', )
        m2m_fields = {
            'balance_list': 'cash_balance_log',
        }

    def to_internal_value(self, data):
        self.fields['device_user'] = serializers.PrimaryKeyRelatedField(
            queryset=DeviceUser.all_objects.all())

        return super(CashBalanceLogSerializer, self).to_internal_value(data)


    def validate(self, data):
        """
        Check that the start is before the stop.
        """

        if not self.instance and data['status'] == self.Meta.model.STATUS.shift_start and \
                CashBalance.all_objects.filter(
                    device_user=data['device_user'], status=CashBalance.STATUS.open).exists():
            raise serializers.ValidationError(
                _("Cannot open new shift for this device user, please close existing one."))

        if not self.instance and data['status'] == self.Meta.model.STATUS.shift_end and \
                not CashBalance.all_objects.filter(
                    device_user=data['device_user'], status=CashBalance.STATUS.open).exists():
            raise serializers.ValidationError(
                _("Cannot close shift for this device user, please open a new one."))

        if not self.instance and data['status'] in [self.Meta.model.STATUS.cash_in, self.Meta.model.STATUS.cash_out] and \
                not CashBalance.all_objects.filter(
                    device_user=data['device_user'], status=CashBalance.STATUS.open).exists():
            raise serializers.ValidationError(
                _("Cannot process transaction. No open shift is found."))

        return data

    def prepare_m2m_field(self, validated_data):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.initial_data = self.initial_data.get(m2m_field_name, None)
            validated_data.pop(m2m_field_name, None)
        return validated_data

    def save_m2m_field(self, instance):
        for m2m_field_name in self.Meta.m2m_fields:
            m2m_field = self.fields.get(m2m_field_name)
            m2m_field.context[self.Meta.m2m_fields.get(m2m_field_name)] = instance

            if m2m_field.is_valid():
                m2m_field.save()

    def create_or_update(self, instance, validated_data):
        validated_data = self.prepare_m2m_field(validated_data)

        if instance:
            self.Meta.model.objects.filter(pk=instance.pk).update(
                **validated_data)
            instance = self.Meta.model.objects.get(pk=instance.pk)
        else:
            instance = self.Meta.model.objects.create(**validated_data)

        self.save_m2m_field(instance)
        return instance

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)


class CashBalanceSerializer(serializers.ModelSerializer):

    status = serializers.SerializerMethodField()
    detail = serializers.SerializerMethodField()

    class Meta:
        model = CashBalance
        fields = ('status', 'outlet', 'detail',)

    def get_status(self, obj):
        if obj:
            return obj.status
        return CashBalance.STATUS.closed

    def get_detail(self, obj):
        if obj:
            qs = CashBalanceLog.objects.filter(
                device_user=obj.device_user,
                transaction_date__gte=obj.start_date,
            )

            if obj.end_date:
                qs = qs.filter(transaction_date__lte=obj.end_date)

            serializer = CashBalanceLogSerializer(instance=qs, many=True)
            return serializer.data
        else:
            return []


class CashBalanceSettingSerializer(serializers.ModelSerializer):

    class Meta:
        model = CashBalanceSetting
        exclude = ('account', )
