from rest_framework import serializers
from sterlingpos.customer.models import Customer, CustomerGroup, CustomerOrganization
from django.utils.translation import ugettext_lazy as _


class CustomerGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerGroup
        exclude = ('account',)


class CustomerOrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerOrganization
        exclude = ('account',)


class CustomerSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=False, allow_blank=True, allow_null=True)
    handphone = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    birth_date = serializers.DateField(required=False, allow_null=True)

    class Meta:
        model = Customer
        exclude = ('account',)
        read_only_fields = ('organization', 'group',)

    def validate_handphone(self, value):
        code = self.context.get(
            'view').kwargs.get('code')
        qs = Customer.objects.filter(
            handphone__iexact=value)

        if code:
            qs = qs.exclude(code=code)

        if qs.exists() and value is not None:
            raise serializers.ValidationError(
                _("This Phone Number has already been registered")
            )
        return value

    def to_internal_value(self, data):
        self.fields['group'] = serializers.PrimaryKeyRelatedField(
            queryset=CustomerGroup.all_objects.all(), required=False, allow_null=True)
        self.fields['organization'] = serializers.PrimaryKeyRelatedField(
            queryset=CustomerOrganization.all_objects.all(), required=False, allow_null=True)
        return super(CustomerSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['group'] = CustomerGroupSerializer(required=False)
        self.fields['organization'] = CustomerOrganizationSerializer(required=False)
        return super().to_representation(obj)


class CustomerCreateSerializer(serializers.ModelSerializer):
    code = serializers.SlugField(read_only=True)
    qrcode = serializers.ImageField(read_only=True)
    email = serializers.EmailField(
        required=False, allow_blank=True, allow_null=True)
    birth_date = serializers.DateField(required=False, allow_null=True)
    handphone = serializers.CharField(
        required=False, allow_null=True, allow_blank=True)

    class Meta:
        model = Customer
        exclude = ('account',)
        read_only_fields = ('organization', 'group',)

    def validate(self, data):
        if "handphone" in data and data.get("handphone"):
            if Customer.objects.filter(
                    handphone=data["handphone"]).exists():
                raise serializers.ValidationError(
                    _("Phone Number has been used."))
        return data

    def to_representation(self, obj):
        self.fields['group'] = CustomerGroupSerializer(required=False)
        self.fields['organization'] = CustomerOrganizationSerializer(required=False)
        return super().to_representation(obj)
