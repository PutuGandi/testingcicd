import datetime

from rest_framework import serializers

from sterlingpos.api.serializers.outlet_serializers import OutletSerializer

from sterlingpos.push_notification.models import RegisteredFCMDevice
from sterlingpos.device.models import DeviceUser, DeviceUserPermission
from sterlingpos.outlet.models import Outlet, OutletDeviceUser


class FCMDeviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = RegisteredFCMDevice
        exclude = ('account', )

    def to_internal_value(self, data):
        self.fields['last_user_login'] = serializers.PrimaryKeyRelatedField(
            queryset=DeviceUser.all_objects.all())
        return super(FCMDeviceSerializer, self).to_internal_value(data)


class DeviceUserPermissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceUserPermission
        exclude = ('account', )


class DeviceUserSerializer(serializers.ModelSerializer):
    revoke_outlet = serializers.PrimaryKeyRelatedField(
            many=True, required=False, write_only=True,
            queryset=Outlet.objects.none())

    lite_is_expired = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = DeviceUser
        exclude = ('account', )
        extra_kwargs = {
            "email": {"required": False}
        }

    def update_m2m(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        if len(object_list) != 0:
            if isinstance(object_list, list):
                obj_ids = set(map(lambda x: x.id, object_list))
            else:
                obj_ids = set(
                    object_list.values_list("id", flat=True))
            current_ids = set(
                rel_field.values_list("id", flat=True)
            )
            add_ids = obj_ids - current_ids
            delete_ids = current_ids - obj_ids
            model_objects = [
                intermediate_model(
                    **{
                        "{}_id".format(rel_field.target_field_name): obj_id,
                        "{}".format(intermediate_model_ref.field.name): instance,
                        "account": instance.account
                    }
                )
                for obj_id in add_ids
            ]
            intermediate_model.objects.bulk_create(model_objects)
            intermediate_model.objects.complex_filter({
                "{}__pk__in".format(rel_field.target_field_name): delete_ids,
                "{}".format(intermediate_model_ref.field.name): instance,
            }).delete()
        else:
            rel_field.clear()
        rel_field.invalidated_update()

    def get_lite_is_expired(self, obj):
        if hasattr(obj, 'deviceuserlitesubscription'):
            return obj.deviceuserlitesubscription.expired
        return True

    def to_internal_value(self, data):
        self.fields['outlet'] = serializers.PrimaryKeyRelatedField(
            many=True,
            queryset=Outlet.objects.all())
        self.fields['revoke_outlet'] = serializers.PrimaryKeyRelatedField(
            many=True, required=False, write_only=True,
            queryset=Outlet.objects.all())
        return super(DeviceUserSerializer, self).to_internal_value(data)

    def to_representation(self, obj):
        self.fields['outlet'] = OutletSerializer(many=True)
        return super(DeviceUserSerializer, self).to_representation(obj)

    def update(self, instance, validated_data):
        return self.create_or_update(instance, validated_data)

    def create(self, validated_data):
        return self.create_or_update(None, validated_data)

    def create_or_update(self, instance, validated_data):
        outlets = validated_data.pop('outlet', [])
        revoked_outlets = validated_data.pop('revoke_outlet', [])

        if not instance:
            instance = self.Meta.model.objects.create(**validated_data)
        else:
            for key in validated_data:
                setattr(instance, key, validated_data[key])
            instance.save()

        self.update_m2m(
            instance, outlets, instance.outlet, 
            OutletDeviceUser, OutletDeviceUser.device_user)

        if revoked_outlets:
            instance.outletdeviceuser_set.filter(
                outlet__in=revoked_outlets).delete()

        return instance
