import copy

from rest_framework import serializers

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from sterlingpos.table_management.models import (
    TableArea, Table,
    TableActivityLog,
    ColorStatus,
    TableManagementSettings,
)
from sterlingpos.api.serializers.mixins import ExcludeArchiveListSerializer


class TableExcludeArchiveListSerializer(ExcludeArchiveListSerializer):

    def copy_serializer(self, child_serializer):
        return copy.copy(child_serializer)


class TableManagementSettingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TableManagementSettings
        exclude = (
            "account",
        )


class TableSerializer(serializers.ModelSerializer):

    is_available = serializers.SerializerMethodField()
    last_timer_expiration = serializers.SerializerMethodField()
    last_timer_run = serializers.SerializerMethodField()

    class Meta:
        model = Table
        list_serializer_class = TableExcludeArchiveListSerializer
        exclude = ('account', )

    def get_is_available(self, obj):
        return obj.is_available

    def get_last_timer_expiration(self, obj):
        if not obj.is_available:
            table_activity_log = obj.tableactivitylog_set.last()
            last_timer_expiration = (timezone.now() - table_activity_log.activity_date).seconds
            return last_timer_expiration

    def get_last_timer_run(self, obj):
        if not obj.is_available:
            table_activity_log = obj.tableactivitylog_set.last()
            return table_activity_log.activity_date


class TableCreateSerializer(TableSerializer):

    class Meta:
        model = Table
        exclude = ('table_area', 'account')

    def create(self, validated_data):
        validated_data['table_area'] = self.context.get('table_area')
        return super(TableSerializer, self).create(validated_data)


class TableAreaSerializer(serializers.ModelSerializer):
    table_url = serializers.HyperlinkedIdentityField(
        view_name='v2:table_list',
    )

    class Meta:
        model = TableArea
        exclude = ('account', )


class TableAreaCreateSerializer(TableAreaSerializer):

    table_list = TableCreateSerializer(write_only=True, many=True,)

    def create(self, validated_data):
        table_list = validated_data.pop('table_list')
        table_list_f = self.fields.get('table_list')
        table_list_f.initial_data = self.initial_data.get('table_list')

        instance = self.Meta.model.objects.create(**validated_data)

        table_list_f.context['table_area'] = instance

        if table_list_f.is_valid():
            table_list_f.save()

        return instance


class TableAreaLayoutSerializer(serializers.ModelSerializer):
    table_set = TableSerializer(read_only=True, many=True,)
    outlet_name = serializers.SerializerMethodField()

    class Meta:
        model = TableArea
        exclude = ('account', )

    def get_outlet_name(self, obj):
        if obj.outlet:
            return obj.outlet.name


class TableActivityLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = TableActivityLog
        exclude = ('account', )

    def to_internal_value(self, data):
        self.fields['table'] = serializers.PrimaryKeyRelatedField(
            queryset=Table.all_objects.all())
        return super(TableActivityLogSerializer, self).to_internal_value(data)

    def validate(self, data):
        """
        Check that the start is before the stop.
        """
        if not self.instance and data['status'] == self.Meta.model.STATUS.available and \
                (
                    not self.Meta.model.objects.filter(table=data['table'], status=self.Meta.model.STATUS.unavailable).exists() or
                    self.Meta.model.objects.filter(table=data['table']).last() and
                    self.Meta.model.objects.filter(table=data['table']).last().status != self.Meta.model.STATUS.unavailable):
            raise serializers.ValidationError(
                _("Table is already available. No Lastest Table Unavailable Activity Found."))

        if not self.instance and data['status'] == self.Meta.model.STATUS.unavailable and \
                self.Meta.model.objects.filter(table=data['table']).last() and \
                self.Meta.model.objects.filter(table=data['table']).last().status != self.Meta.model.STATUS.available:
            raise serializers.ValidationError(
                _("Table is already unavailable. No Lastest Table Available Activity Found."))

        return data


class ColorStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = ColorStatus
        exclude = ("table_management_setting", "account",)
