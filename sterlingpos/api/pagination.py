from rest_framework.pagination import LimitOffsetPagination 


class SterlingPOSLimitOffsetPagination(LimitOffsetPagination):

    def get_count(self, queryset):
        _counting_queryset = getattr(
            self, "_counting_queryset", queryset)
        return super().get_count(_counting_queryset)
