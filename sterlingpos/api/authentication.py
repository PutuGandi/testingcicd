import logging

from django.core.cache import cache
from django.utils.translation import ugettext as _

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.utils import jwt_payload_handler
from rest_framework_jwt.settings import api_settings
from rest_framework.authentication import BaseAuthentication, get_authorization_header
from rest_framework import exceptions

from django_multitenant.utils import set_current_tenant, get_current_tenant

from sterlingpos.outlet.models import Outlet
from sterlingpos.private_apps.models import PrivateApp

logger = logging.getLogger(__name__)


def sterling_jwt_payload_handler(user, outlet=None):
    payload = jwt_payload_handler(user)
    if outlet:
        payload['outlet'] = outlet.branch_id
    return payload


class MultiTenantJSONWebTokenAuthentication(JSONWebTokenAuthentication):

    def check_outlet_expiracy(self, branch_id=None):
        if branch_id:
            try:
                outlet = Outlet.objects.get(branch_id=branch_id)
            except Exception as identifier:
                logger.info(f"MultiTenantJSONWebTokenAuthentication {identifier}")
                raise exceptions.AuthenticationFailed("Authentication Failed, bad outlet code.")

            if hasattr(outlet, 'subscription') and outlet.subscription.expired:
                msg = _('Your Outlet Subscription has Expired.')
                raise exceptions.AuthenticationFailed(msg)

    def authenticate(self, request):
        """
        Call set_current_tenant if user is authenticated, return the rest as is
        """

        try:
            user_and_payload = super(
                MultiTenantJSONWebTokenAuthentication, self).authenticate(request)
        except Exception as identifier:
            if request.parser_context['view'].get_view_name() == "Transaction Order Create":
                log_data = "Processing Transaction Failed code={}, \
                    outlet={}, transaction_date={}, reason={}".format(
                    request.data.get('code'),
                    request.data.get('outlet'),
                    request.data.get('transaction_date'),
                    identifier
                )
                logger.warning(log_data)
            msg = _('Error decoding signature, payload may have been corrupted')
            raise exceptions.AuthenticationFailed(msg)

        if user_and_payload:
           
            user = user_and_payload[0]
            payload = api_settings.JWT_DECODE_HANDLER(user_and_payload[1])
            branch_id = payload.get('outlet')
            account = user.account
            set_current_tenant(account)
            self.check_outlet_expiracy(branch_id)
            user_and_payload = (user, payload)

        return user_and_payload


class MultiTenantPrivateAppTokenAuthentication(BaseAuthentication):

    keyword = 'Token'
    model = PrivateApp

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, token):
        key, credential = token.split(':')
        app = self.model.authenticate(key, credential)

        if not app:
            raise exceptions.AuthenticationFailed(_('Invalid token.'))

        user = app.account.owners.first().user

        if not user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        account = user.account
        set_current_tenant(account)

        return (user, token)

    def authenticate_header(self, request):
        return self.keyword
