from django.conf import settings
from django.conf.urls import url

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny
from . import views


schema_view_v2_1 = get_schema_view(
    openapi.Info(
        title="KAWN API",
        default_version='v2.1',
        description="API for KAWN Pos",
        terms_of_service="https://kawn.co.id/privacy-policy/",
        contact=openapi.Contact(email="admin@kawn.co.id"),
    ),
    public=True,
    permission_classes=(AllowAny,),
)

app_name = 'v2.1'
urlpatterns = [
    # URL pattern for the Catalogue API

    url(
        r'^$',
        schema_view_v2_1.without_ui(cache_timeout=12 * 60 * 60), name='schema-json'),

    url(
        r'^swagger(?P<format>\.json|\.yaml)$',
        schema_view_v2_1.without_ui(cache_timeout=None), name='schema-json'),

    url(
        regex=r'^variant/$',
        view=views.CreateVariantProductView.as_view(),
        name='variant_create'
    ),

    url(
        regex=r'^public/products/composite/(?P<outlet_base64>.+)/$',
        view=views.PublicOutletProductCompositeListView.as_view(),
        name='public_outlet_product_composite_list'
    ),
    url(
        regex=r'^public/products/dynamic-composite/(?P<outlet_base64>.+)/$',
        view=views.PublicOutletProductDynamicCompositeListView.as_view(),
        name='public_outlet_product_dynamic_composite_list'
    ),
    url(
        regex=r'^public/products/complementer/(?P<outlet_base64>.+)/$',
        view=views.PublicOutletProductComplementerListView.as_view(),
        name='public_outlet_product_complementer_list'
    ),
    url(
        regex=r'^public/products/(?P<outlet_base64>.+)/$',
        view=views.PublicOutletProductListView.as_view(),
        name='public_outlet_product_list'
    ),

    url(
        regex=r'^products/$',
        view=views.ProductCreateAndListView.as_view(),
        name='product_list'
    ),
    url(
        regex=r'^products/$',
        view=views.ProductCreateAndListView.as_view(),
        name='product_create'
    ),
    url(
        regex=r'^products/(?P<pk>\d+)/$',
        view=views.ProductRetrieveUpdateDestroyView.as_view(),
        name='product_update'
    ),
    url(
        regex=r'^products/(?P<pk>\d+)/$',
        view=views.ProductRetrieveUpdateDestroyView.as_view(),
        name='product_delete'
    ),
    url(
        regex=r'^products/(?P<pk>\d+)/$',
        view=views.ProductRetrieveUpdateDestroyView.as_view(),
        name='product_detail'
    ),

    url(
        regex=r'^products/by-classification/(?P<classification>all|standart|composite|complementary|dynamic-composite)/$',
        view=views.ProductByClassListView.as_view(),
        name='product_list'
    ),
    url(
        regex=r'^products/composite/$',
        view=views.ProductCompositeListView.as_view(),
        name='product_composite_list'
    ),
    url(
        regex=r'^products/dynamic-composite/$',
        view=views.ProductDynamicCompositeListView.as_view(),
        name='dynamic_composite_list'
    ),
    url(
        regex=r'^products/complementer/$',
        view=views.ProductComplementerListView.as_view(),
        name='product_complementer_list'
    ),
    url(
        regex=r'^products/modifier/$',
        view=views.ProductModifierListView.as_view(),
        name='product_modifier_list'
    ),

    url(
        regex=r'^categories/$',
        view=views.CategoryCreateAndListView.as_view(),
        name='category_list'
    ),
    url(
        regex=r'^categories/$',
        view=views.CategoryCreateAndListView.as_view(),
        name='category_create'
    ),
    url(
        regex=r'^categories/(?P<pk>\d+)/$',
        view=views.CategoryRetrieveUpdateDestroyView.as_view(),
        name='category_update'
    ),
    url(
        regex=r'^categories/(?P<pk>\d+)/$',
        view=views.CategoryRetrieveUpdateDestroyView.as_view(),
        name='category_delete'
    ),

    url(
        regex=r'^product-types/$',
        view=views.ProductTypeListView.as_view(),
        name='product_type_list'
    ),

    url(
        regex=r'^uoms/$',
        view=views.UOMListView.as_view(),
        name='uom_list'
    ),

    # URL patterns for customer API
    url(
        regex=r'^outlets/$',
        view=views.OutletListCreateView.as_view(),
        name='outlet_list'
    ),
    url(
        regex=r'^outlets/$',
        view=views.OutletListCreateView.as_view(),
        name='outlet_create'
    ),
    url(
        regex=r'^outlets/(?P<pk>\d+)/$',
        view=views.OutletRetrieveUpdateView.as_view(),
        name='outlet-detail'
    ),
    url(
        regex=r'^outlets/(?P<pk>\d+)/$',
        view=views.OutletRetrieveUpdateView.as_view(),
        name='outlet-update'
    ),

    url(
        regex=r'^outlets/(?P<pk>\d+)/receipt/$',
        view=views.ReceiptDetailView.as_view(),
        name='receipt_detail'
    ),
    url(
        regex=r'^outlets/(?P<outlet_pk>\d+)/device/user/$',
        view=views.DeviceUserCreateListView.as_view(),
        name='device_user_list'
    ),
    url(
        regex=r'^outlets/(?P<outlet_pk>\d+)/products/$',
        view=views.OutletProductListView.as_view(),
        name='outlet_product'
    ),
    url(
        regex=r'^outlets/(?P<outlet_pk>\d+)/products/composite/$',
        view=views.OutletProductCompositeListView.as_view(),
        name='outlet_product_composite'
    ),
    url(
        regex=r'^outlets/(?P<outlet_pk>\d+)/products/dynamic-composite/$',
        view=views.OutletProductDynamicCompositeListView.as_view(),
        name='outlet_dynamic_composite'
    ),
    url(
        regex=r'^outlets/(?P<outlet_pk>\d+)/products/complementer/$',
        view=views.OutletProductComplementerListView.as_view(),
        name='outlet_product_complementer'
    ),

    # URL pattern for sales API
    url(
        regex=r'^payment-methods/$',
        view=views.PaymentMethodListView.as_view(),
        name='payment_method_list'
    ),
    url(
        regex=r'^sales-order-options/$',
        view=views.SalesOrderOptionListView.as_view(),
        name='sales_order_option_list'
    ),
    url(
        regex=r'^sales/$',
        view=views.SalesOrderListView.as_view(),
        name='sales_order_list'
    ),
    url(
        regex=r'^sales/(?P<pk>\d+)/$',
        view=views.SalesOrderDetailView.as_view(),
        name='sales_order_detail'
    ),
    url(
        regex=r'^sales/code/(?P<code>\w+)/$',
        view=views.SalesOrderDetailView.as_view(),
        name='sales_order_detail'
    ),
    url(
        regex=r'^outlets/(?P<pk>\d+)/sales/$',
        view=views.SalesOrderListView.as_view(),
        name='sales_order_list'
    ),
    url(
        regex=r'^sales/(?P<pk>[\d-]+)/sent-receipt/$',
        view=views.SalesOrderSentReceiptView.as_view({
            "post": "sent_receipt"}),
        name='sales_order_sent_receipt'
    ),
    url(
        regex=r'^invoice/(?P<code>\w+)/payment/$',
        view=views.InvoicePaymentDetailCreateView.as_view(),
        name='invoice_payment_create'
    ),

    url(
        regex=r'^transaction-orders/$',
        view=views.TransactionOrderCreateView.as_view(),
        name='transaction_order_create'
    ),
    url(
        regex=r'^cash-balances/settings/$',
        view=views.CashBalanceSettingView.as_view(),
        name='cash_balance_setting'
    ),
    url(
        regex=r'^cash-balances/$',
        view=views.CashBalanceLogCreateView.as_view(),
        name='cash_balance_create'
    ),
    url(
        regex=r'^cash-balances/(?P<device_user_id>\d+)/$',
        view=views.CashBalanceDetailView.as_view(),
        name='cash_balance_detail'
    ),

    url(
        regex=r'^cashier-activities/$',
        view=views.CashierActivityCreateView.as_view(),
        name='cashier_activity_create'
    ),
    url(
        regex=r'^cashier-activities/reasons/$',
        view=views.CashierActivityReasonListView.as_view(),
        name='cashier_activity_reason_list'
    ),
    url(
        regex=r'^transaction-type/reasons/$',
        view=views.TransactionTypeReasonListView.as_view(),
        name='transaction_type_reason_list'
    ),
    url(
        regex=r'^report-settings/$',
        view=views.SalesReportSettingView.as_view(),
        name='report_settings'
    ),

    # # FCM DEVICE VIEWS
    url(
        regex=r'^device-users/$',
        view=views.DeviceUserCreateListView.as_view(),
        name='device_user_list'
    ),
    url(
        regex=r'^device-users/$',
        view=views.DeviceUserCreateListView.as_view(),
        name='device_user_create'
    ),
    url(
        regex=r'^device-users/(?P<pk>\d+)/$',
        view=views.DeviceUserRetrieveUpdateDeleteView.as_view(),
        name='device_user_update'
    ),
    url(
        regex=r'^device-users/(?P<pk>\d+)/$',
        view=views.DeviceUserRetrieveUpdateDeleteView.as_view(),
        name='device_user_delete'
    ),
    url(
        regex=r'^fcm/$',
        view=views.FCMDeviceCreateView.as_view(),
        name='fcm_device_create'
    ),
    url(
        regex=r'^fcm/(?P<pk>\d+)/$',
        view=views.FCMDeviceDeleteView.as_view(),
        name='fcm_device_delete'
    ),
    url(
        regex=r'^promos/$',
        view=views.PromoCreateListView.as_view(),
        name='promo_list'
    ),
    url(
        regex=r'^promos/$',
        view=views.PromoCreateListView.as_view(),
        name='promo_create'
    ),
    url(
        regex=r'^promos/(?P<pk>\d+)/$',
        view=views.PromoRetrieveUpdateDestroyView.as_view(),
        name='promo_update'
    ),
    url(
        regex=r'^promos/(?P<pk>\d+)/$',
        view=views.PromoRetrieveUpdateDestroyView.as_view(),
        name='promo_delete'
    ),

    url(
        regex=r'^customers/$',
        view=views.CustomerListView.as_view(),
        name='customer_list'
    ),
    url(
        regex=r'^customers/create/$',
        view=views.CustomerCreateView.as_view(),
        name='customer_create'
    ),
    url(
        regex=r'^customers/(?P<code>[\w-]+)/$',
        view=views.CustomerRetrieveUpdateDestroyView.as_view(),
        name='customer_detail'
    ),

    url(
        regex=r'^customers-group/$',
        view=views.CustomerGroupCreateListView.as_view(),
        name='customer_group_list'
    ),

    url(
        regex=r'^customers-group/$',
        view=views.CustomerGroupCreateListView.as_view(),
        name='customer_group_create'
    ),
    url(
        regex=r'^customers-organization/$',
        view=views.CustomerOrganizationCreateListView.as_view(),
        name='customer_organization_list'
    ),
    url(
        regex=r'^customers-organization/$',
        view=views.CustomerOrganizationCreateListView.as_view(),
        name='customer_organization_create'
    ),

    url(
        regex=r'^promos/limit/$',
        view=views.PromoLimitCheckView.as_view(),
        name='promo_limit_view'
    ),

    url(
        regex=r'^promo-tag/$',
        view=views.PromoTagCreateAndListView.as_view(),
        name='promo_tag_list'
    ),
    url(
        regex=r'^promo-tag/$',
        view=views.PromoTagCreateAndListView.as_view(),
        name='promo_tag_create'
    ),

    # TABLE MANAGEMENT VIEWS
    url(
        regex=r'^table-management/color-status/$',
        view=views.ColorStatusListView.as_view(),
        name='color_status_list'
    ),
    url(
        regex=r'^outlets/(?P<pk>\d+)/table-area/$',
        view=views.TableAreaListView.as_view(),
        name='table_area_list'
    ),

    url(
        regex=r'^table-areas/$',
        view=views.TableAreaCreateView.as_view(),
        name='table_area_create'
    ),
    url(
        regex=r'^table-areas/(?P<pk>\d+)/$',
        view=views.TableAreaUpdateView.as_view(),
        name='table_area_update'
    ),
    url(
        regex=r'^table-areas/(?P<pk>\d+)/$',
        view=views.TableAreaDeleteView.as_view(),
        name='table_area_delete'
    ),
    url(
        regex=r'^table-areas/(?P<pk>\d+)/table/$',
        view=views.TableListView.as_view(),
        name='table_list'
    ),
    url(
        regex=r'^table-areas/(?P<pk>\d+)/layout/$',
        view=views.TableAreaDetailView.as_view(),
        name='table_area_detail'
    ),

    url(
        regex=r'^tables/$',
        view=views.TableCreateView.as_view(),
        name='table_create'
    ),
    url(
        regex=r'^table/bulk-create/$',
        view=views.TableBulkCreateView.as_view(),
        name='table_bulk_create'
    ),
    url(
        regex=r'^tables/(?P<pk>\d+)/$',
        view=views.TableUpdateView.as_view(),
        name='table_update'
    ),
    url(
        regex=r'^tables/(?P<pk>\d+)/$',
        view=views.TableDeleteView.as_view(),
        name='table_delete'
    ),
    url(
        regex=r'^table-managements/$',
        view=views.TableManagementView.as_view(),
        name='table_management'
    ),
    url(
        regex=r'^table-activities/$',
        view=views.TableActivityLogCreateView.as_view(),
        name='table_activity_log_create'
    ),
    url(
        regex=r'^table-order/$',
        view=views.TableOrderView.as_view(),
        name='table_order'
    ),
    url(
        regex=r'^receipts/$',
        view=views.ReceiptListView.as_view(),
        name='receipt_list'
    ),

    url(
        regex=r'^provinces/$',
        view=views.ProvinceListView.as_view(),
        name='province_list'
    ),
    url(
        regex=r'^provinces/(?P<pk>\d+)/cities/$',
        view=views.CityListView.as_view(),
        name='city_list'
    ),
    url(
        regex=r'^cities/$',
        view=views.CityListView.as_view(),
        name='city_list'
    ),

    url(
        regex=r'^billings/$',
        view=views.BillingListCreateView.as_view(),
        name='billing_list_create'
    ),
    url(
        regex=r'^billings/(?P<pk>\d+)/$',
        view=views.BillingRetrieveView.as_view(),
        name='billing_detail'
    ),
    url(
        regex=r'^billings/(?P<pk>\d+)/token/$',
        view=views.MidtransTokenView.as_view(),
        name='billing_token'
    ),
    url(
        regex=r'^billings/(?P<pk>\d+)/cancel/$',
        view=views.BillingCancelView.as_view(),
        name='billing_cancel'
    ),

    url(
        regex=r'^roles/$',
        view=views.RoleListView.as_view(),
        name='role_list'
    ),
    url(
        regex=r'^roles/settings/$',
        view=views.RoleSettingsView.as_view(),
        name='role_settings'
    ),

    url(
        regex=r'^subscription-plans/$',
        view=views.SubscriptionPlanListView.as_view(),
        name='subscription_plan_list'
    ),
    url(
        regex=r'^subscriptions/mobile-lite/$',
        view=views.GooglePlaySubscriptionLogCreateView.as_view(),
        name='google_play_subscription_log_create'
    ),
    url(
        regex=r'^emails/$',
        view=views.EmailListCreateView.as_view(),
        name='emails'
    ),
    url(
        regex=r'^emails/(?P<pk>\d+)/$',
        view=views.EmailRetrieveDeleteView.as_view(),
        name='emails'
    ),
    url(
        regex=r'^emails/(?P<pk>\d+)/set-primary/$',
        view=views.EmailViewSet.as_view({'post': 'set_primary'}),
        name='emails_set_primary'
    ),
    url(
        regex=r'^emails/(?P<pk>\d+)/send-verification/$',
        view=views.EmailViewSet.as_view({'post': 'send_verification'}),
        name='emails_send_verification'
    ),

    url(
        regex=r'^surcharges/$',
        view=views.SurchargesListView.as_view(),
        name='surcharge_list'
    ),
    url(
        regex=r'^promo-limit/use/$',
        view=views.PromoUseCheckView.as_view(),
        name="promo_limit"
    ),
    url(
        regex=r'^promo-customer/(?P<pk>\d+)/limit/(?P<outlet_pk>\d+)/$',
        view=views.PromoByCustomerListView.as_view(),
        name="promo_customer"
    ),

    url(
        regex=r'^digital-payment/$',
        view=views.DigitalPaymentSettingsListView.as_view(),
        name="digital_payment"
    ),
    url(
        regex=r'^cashlez/$',
        view=views.CashlezAccountDetail.as_view(),
        name="cashlez_detail"
    ),
    url(
        regex=r'^cashlez/key/$',
        view=views.CashlezAuthKeyView.as_view(),
        name="cashlez_key"
    ),
    url(
        regex=r'^shopeepay/$',
        view=views.ShopeePayAccountDetail.as_view(),
        name="shopeepay_detail"
    ),
    url(
        regex=r'^shopeepay/qr/$',
        view=views.ShopeePayGetQRCode.as_view(),
        name="shopeepay_get_qr"
    ),
    url(
        regex=r'^shopeepay/qr/invalidate/$',
        view=views.ShopeePayInvalidateQRCode.as_view(),
        name="shopeepay_invalidate_qr"
    ),
    url(
        regex=r'^shopeepay/payment/check/$',
        view=views.ShopeePayCheckPayment.as_view(),
        name="shopeepay_payment_check"
    ),
    url(
        regex=r'^shopeepay/payment/refund/$',
        view=views.ShopeePayRefundPayment.as_view(),
        name="shopeepay_refund_payment"
    ),

    url(
        regex=r'^kitchen/carts/$',
        view=views.KitchenCardCreateView.as_view(),
        name="kitchen_cart_create"
    ),

    url(
        regex=r'^suppliers/$',
        view=views.SupplierListView.as_view(),
        name="suppliers_list"
    ),

    url(
        regex=r'^purchase-orders/$',
        view=views.PurchaseOrderCreateAndListView.as_view(),
        name="purchase_orders_list"
    ),
    url(
        regex=r'^purchase-orders/$',
        view=views.PurchaseOrderCreateAndListView.as_view(),
        name="purchase_orders_create"
    ),
    url(
        regex=r'^purchase-orders/(?P<pk>\d+)/$',
        view=views.PurchaseOrderUpdateView.as_view(),
        name='purchase_orders_update'
    ),
    url(
        regex=r'^purchase-orders/(?P<pk>\d+)/cancel-remaining-items/$',
        view=views.PurchaseOrderCancelRemainingItemsView.as_view(),
        name='purchase_orders_cancel'
    ),

    url(
        regex=r'^received-orders/$',
        view=views.ReceivedOrderCreateAndListView.as_view(),
        name="received_orders_list"
    ),
    url(
        regex=r'^received-orders/$',
        view=views.ReceivedOrderCreateAndListView.as_view(),
        name="received_orders_create"
    ),
    url(
        regex=r'^received-orders/(?P<pk>\d+)/$',
        view=views.ReceivedOrderUpdateView.as_view(),
        name='received_orders_update'
    ),
    url(
        regex=r'^received-orders/complete/(?P<pk>\d+)/$',
        view=views.ReceivedOrderCompleteView.as_view(),
        name='received_orders_complete'
    ),
    url(
        regex=r'^received-orders/(?P<received_order_pk>\d+)/detail/(?P<pk>\d+)/$',
        view=views.ReceivedOrderDetailUpdateView.as_view(),
        name='received_orders_detail_update'
    ),


    url(r'^login/$', views.LoginAccount.as_view(), name='rf_auth_login'),
    url(r'^password/change/$', views.ChangePassword.as_view(), name='password_change'),
    url(r'^password/reset/$', views.LostPassword.as_view(), name='password_reset'),

    url(r'^outlets/auth/$', views.OutletAuthentication.as_view(), name='rf_outlet_auth'),
    url(r'^account/$', views.AccountDetail.as_view(), name='account_detail'),
]

if settings.DEBUG or settings.STAGING:
    urlpatterns += [
        url(r'^swagger/$', schema_view_v2_1.with_ui('swagger', cache_timeout=None), name='schema-swagger-ui'),
    ]
