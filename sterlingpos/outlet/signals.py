from django.conf import settings
from django.dispatch import Signal

from sterlingpos.catalogue.models import (
    ProductPricing,
    ProductAvailability,
)


def apply_existing_device_user_manager(instance):
    account = instance.account
    device_users = account.deviceuser_set.filter(
        permission="manager", all_outlet_access=True
    )
    for device_user in device_users:
        instance.outletdeviceuser_set.create(
            device_user=device_user,
            outlet=instance,
            account=account,
        )


def apply_existing_product_availability(instance):
    product_availabilities = [
        ProductAvailability(
            product=product,
            outlet=instance,
            account=instance.account,
        )
        for product in instance.account.product_set.all()
    ]
    ProductAvailability.objects.bulk_create(product_availabilities)


def apply_existing_product_pricing(instance):
    product_pricings = [
        ProductPricing(
            product=product,
            outlet=instance,
            account=instance.account,
            price=product.price,
        )
        for product in instance.account.product_set.all()
    ]
    ProductPricing.objects.bulk_create(product_pricings)


def create_initial_data_for_device_user(sender, **kwargs):
    instance = kwargs.get("instance")
    created = kwargs.get("created")
    if created:
        apply_existing_device_user_manager(instance)
        apply_existing_product_availability(instance)
        apply_existing_product_pricing(instance)
