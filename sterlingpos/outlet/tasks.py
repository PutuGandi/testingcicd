import logging
import uuid

from django.db import IntegrityError, transaction

from celery import shared_task
from sterlingpos.accounts.models import Account
from sterlingpos.outlet.utils import acronym
from sterlingpos.core.models import set_current_tenant

logger = logging.getLogger(__name__)


@shared_task(bind=True)
def set_outlet_id(self):
    account_counts = Account.objects.count()

    for i, account in enumerate(Account.objects.all().iterator()):
        logger.info(f"Processing Account {account}.{account.pk} {i}|{account_counts}")
        for outlet in account.outlet_set.all():
            set_current_tenant(account)
            if outlet.outlet_code:
                try:
                    with transaction.atomic():
                        outlet.branch_id = outlet.outlet_code
                        outlet.save()
                except Exception:
                    with transaction.atomic():
                        outlet.branch_id = "{}-{}".format(
                            acronym(outlet.name), uuid.uuid4().hex[:4]
                        )
                        outlet.save()
            set_current_tenant(None)
    logger.info("Finished set outlet id for all Accounts")
