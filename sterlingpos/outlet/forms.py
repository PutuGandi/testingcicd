import uuid

from django import forms
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, ugettext

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Submit
from sterlingpos.core.bootstrap import (
    KawnAppendedText,
    KawnField,
    KawnFieldSetWithHelpText,
)
from dal import autocomplete

from sterlingpos.outlet.models import Outlet
from sterlingpos.outlet.utils import acronym


MAX_UPLOAD_SIZE = 4194304  # 4MB


class BaseOutletForm(forms.ModelForm):
    class Meta:
        model = Outlet
        fields = [
            "name",
            "phone",
            "address",
            "province",
            "city",
        ]
        labels = {
            "name": "Nama Outlet",
            "phone": "Nomor Telepon",
            "address": "Alamat Outlet",
            "province": "Provinsi",
            "city": "Kota",
        }
        widgets = {
            "name": forms.TextInput(
                attrs={"placeholder": "ex: Kawn POS", "required": True}
            ),
            "phone": forms.TextInput(
                attrs={"placeholder": "ex: 021 - 238XXXX", "required": True}
            ),
            "address": forms.Textarea(attrs={"rows": 3}),
            "province": autocomplete.ModelSelect2(
                url="outlet:province_autocomplete",
                attrs={
                    "data-minimum-input-length": 3,
                },
            ),
            "city": autocomplete.ModelSelect2(
                url="outlet:cities_autocomplete",
                forward=["province"],
                attrs={
                    "data-minimum-input-length": 3,
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super(BaseOutletForm, self).__init__(*args, **kwargs)
        self.fields["name"].required = True
        self.fields["phone"].required = True
        self.fields["address"].required = True
        self.fields["province"].required = True
        self.fields["city"].required = True
        self.helper = FormHelper()
        self.helper.include_media = False
        self.helper.form_tag = False


class OutletForm(BaseOutletForm):
    class Meta(BaseOutletForm.Meta):
        fields = BaseOutletForm.Meta.fields + [
            "postal_code",
            "taxes",
            "gratuity",
            "enable_dashboard",
            "transaction_code_prefix",
            "outlet_image",
            "outlet_code",
        ]
        widgets = BaseOutletForm.Meta.widgets.copy()

    def clean_outlet_image(self):
        image = self.cleaned_data["outlet_image"]
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")
        return image

    def __init__(self, *args, **kwargs):
        super(OutletForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields["outlet_image"].label = "{}".format(_("Add Image"))
        self.fields["taxes"].label = "{}".format(_("Taxes"))
        self.fields["gratuity"].label = "{}".format(_("Gratuity"))

        self.fields["enable_dashboard"].label = "{}".format(
            _("Aktifkan Dashboard di KAWN App")
        )
        self.fields[
            "outlet_code"
        ].help_text = "Kosongkan untuk otomatis generate ID Outlet."

        self.fields["outlet_image"].help_text = "{}".format(
            _(
                "Drag an image here or browse for an image to upload (Maximum size: 4 MB)."
            )
        )
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Outlet Detail"),
                Div(
                    Div(
                        Div(
                            Div(KawnField("name"), css_class="col"),
                            Div(KawnField("outlet_code"), css_class="col"),
                            css_class="form-row",
                        ),
                        css_class="col-lg-12 px-0 mb-0",
                    ),
                    KawnField("address"),
                    Div(
                        Div(
                            Div(KawnField("province"), css_class="col"),
                            Div(KawnField("city"), css_class="col"),
                            css_class="form-row",
                        ),
                        css_class="col-lg-12 px-0 mb-0",
                    ),
                    Div(
                        Div(
                            Div(KawnField("postal_code"), css_class="col"),
                            Div(KawnField("phone"), css_class="col"),
                            css_class="form-row",
                        ),
                        css_class="col-lg-12 px-0 mb-0",
                    ),
                    Div(
                        Div(
                            Div(KawnAppendedText("taxes", "%"), css_class="col"),
                            Div(KawnAppendedText("gratuity", "%"), css_class="col"),
                            css_class="form-row",
                        ),
                        css_class="col-lg-12 px-0 mb-0",
                    ),
                    KawnField("transaction_code_prefix"),
                    KawnField("enable_dashboard"),
                    css_class="col-lg-9",
                ),
                Div(
                    KawnField(
                        "outlet_image",
                        template="dashboard/image_upload/image_fields.html",
                    ),
                    css_class="col-lg-3",
                ),
                form_grid="col-lg-12",
            )
        )

    def clean_outlet_code(self):
        outlet_code = self.cleaned_data.get("outlet_code", "")
        if outlet_code and self.instance.outlet_code != outlet_code:
            if Outlet.objects.filter(outlet_code=outlet_code).exists():
                raise forms.ValidationError(_("Kode Outlet sudah digunakan."))
        return outlet_code


class OutletUpdateForm(OutletForm):
    def __init__(self, *args, **kwargs):
        super(OutletUpdateForm, self).__init__(*args, **kwargs)
        self.helper.layout.append(
            Div(
                HTML(
                    '<a href="{}"  class="btn btn-secondary">{}</a>'.format(
                        reverse("outlet:outlet_list"), _("Back")
                    )
                ),
                Submit("save", _("Save"), css_class="ml-1"),
                css_class="d-flex justify-content-end",
            )
        )

    def save(self, commit=False):
        name = self.cleaned_data.get("name")
        outlet_code = self.cleaned_data.get("outlet_code")
        outlet_code = (
            outlet_code
            if outlet_code
            else "{}-{}".format(acronym(name), uuid.uuid4().hex[:4])
        )
        instance = super().save(commit=False)
        instance.outlet_code = outlet_code
        instance.save()

        return instance


class OutletFormReceipt(BaseOutletForm):
    class Meta(BaseOutletForm.Meta):
        fields = BaseOutletForm.Meta.fields + [
            "postal_code",
        ]
        widgets = BaseOutletForm.Meta.widgets.copy()

    def __init__(self, *args, **kwargs):
        super(OutletFormReceipt, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            Field("name", type="hidden", css_class="name-id"),
            "address",
            Div(
                Div(Field("province", css_class="custom-select"), css_class="col-4"),
                Div(Field("city", css_class="custom-select"), css_class="col-4"),
                Div("postal_code", css_class="col-4"),
                css_class="form-row",
            ),
            "phone",
        )
