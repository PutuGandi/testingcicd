from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views


app_name = "outlet"
urlpatterns = [
    url(regex=r"^outlets/$", view=views.OutletListView.as_view(), name="outlet_list"),
    url(
        regex=r"^outlets/create/$",
        view=views.OutletCreateView.as_view(),
        name="outlet_create",
    ),
    url(
        regex=r"^outlets/(?P<pk>[0-9]+)/update/$",
        view=views.OutletUpdateView.as_view(),
        name="outlet_update",
    ),
    url(
        regex=r"^outlets/data$", view=views.OutletListJson.as_view(), name="data_outlet"
    ),
    url(
        r"^outlets/autocomplete/$",
        view=views.OutletAutocompleteView.as_view(),
        name="outlet_autocomplete",
    ),
    url(
        r"^outlets/province/autocomplete/$",
        view=views.ProvinceAutocompleteView.as_view(),
        name="province_autocomplete",
    ),
    url(
        r"^outlets/cities/autocomplete/$",
        view=views.CitiesAutocompleteView.as_view(),
        name="cities_autocomplete",
    ),
]
