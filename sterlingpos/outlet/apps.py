from django.apps import AppConfig
from django.db.models.signals import post_save


class OutletConfig(AppConfig):
    name = 'sterlingpos.outlet'
    verbose_name = "Outlets"

    def ready(self):
        from .models import Outlet
        from .signals import create_initial_data_for_device_user

        post_save.connect(create_initial_data_for_device_user, sender=Outlet)
