import datetime
import os
import uuid

from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.views.generic import (
    ListView,
    DetailView,
    View,
    TemplateView,
    CreateView,
    UpdateView,
    DeleteView,
    View,
)
from braces.views import LoginRequiredMixin
from formtools.wizard.views import SessionWizardView
from dal import autocomplete

from .models import Outlet
from .forms import OutletUpdateForm, OutletForm
from sterlingpos.province.models import Province, City
from sterlingpos.subscription.forms import SubscriptionForm
from sterlingpos.subscription.models import Subscription, Billing
from sterlingpos.core.models import get_current_tenant
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.outlet.utils import acronym


class OutletListJson(SterlingRoleMixin, BaseDatatableView):

    columns = [
        "name",
        "outlet_code",
        "province.name",
        "city.name",
        "phone",
        "taxes",
        "gratuity",
        "action",
    ]
    order_columns = [
        "name",
        "outlet_code",
        "province.name",
        "city.name",
        "phone",
        "taxes",
        "gratuity",
        "action",
    ]

    model = Outlet

    csv_filename = "outlet_list"
    csv_header = [
        "name",
        "phone",
        "province",
        "city",
        "postal_code",
        "taxes",
        "gratuity",
    ]

    def filter_queryset(self, qs):
        if not self.pre_camel_case_notation:
            search = self._querydict.get("search[value]", None)
            col_data = self.extract_datatables_column_data()
            q = Q()
            for col_no, col in enumerate(col_data):
                column_name = self.order_columns[col_no].replace(".", "__")
                if col["search.value"]:
                    qs = qs.filter(
                        **{"{0}__istartswith".format(column_name): col["search.value"]}
                    )
            qs = qs.filter(q)
        return qs

    def render_column(self, row, column):
        if column == "action":
            action_data = {
                "id": row.id,
                "edit": reverse_lazy("outlet:outlet_update", kwargs={"pk": row.pk}),
            }
            return action_data
        elif column == "name":
            return '<a href="{}">{}</a>'.format(
                reverse_lazy("outlet:outlet_update", kwargs={"pk": row.pk}),
                escape(row.name),
            )
        else:
            return super(OutletListJson, self).render_column(row, column)


class ProvinceAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Province.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs

    def get_result_label(self, item):
        return item.name


class CitiesAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        province = self.forwarded.get("province", None)

        qs = City.objects.all()
        if province:
            qs = qs.filter(province=province)

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs

    def get_result_label(self, item):
        return item.name


class OutletListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/outlet/outlet_list.html"


class OutletCreateView(SterlingRoleMixin, SessionWizardView):
    form_list = [
        ("subscription_form", SubscriptionForm),
        ("outlet_form", OutletForm),
    ]
    template_name = "dashboard/outlet/outlet_create.html"
    success_url = None
    redirect_field_name = "next"
    success_url = None
    file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT))

    def process_outlet(self, form, subscription):
        outlet_code = form.cleaned_data.get("outlet_code")
        outlet_code = outlet_code = (
            outlet_code
            if outlet_code
            else "{}-{}".format(
                acronym(form.cleaned_data["name"]), uuid.uuid4().hex[:4]
            )
        )
        outlet = subscription.outlet
        outlet.name = form.cleaned_data["name"]
        branch_id = form.cleaned_data.get("branch_id")
        branch_id = (
            branch_id
            if branch_id
            else "{}-{}".format(
                acronym(form.cleaned_data["name"]), uuid.uuid4().hex[:4]
            )
        )
        outlet.branch_id = branch_id
        outlet.outlet_code = outlet_code
        outlet.phone = form.cleaned_data["phone"]
        outlet.address = form.cleaned_data["address"]
        outlet.province = form.cleaned_data["province"]
        outlet.city = form.cleaned_data["city"]
        outlet.postal_code = form.cleaned_data["postal_code"]
        outlet.taxes = form.cleaned_data["taxes"]
        outlet.gratuity = form.cleaned_data["gratuity"]
        outlet.transaction_code_prefix = form.cleaned_data["transaction_code_prefix"]
        outlet.save()

        return outlet

    def process_subscription(self, form):
        subscription = form.save(commit=False)
        subscriptions = Subscription.objects.filter(
            account=get_current_tenant(),
            expires__gte=datetime.date.today(),
            is_trial=True,
        )
        if subscriptions:
            subscription.is_trial = True
            existing_subscription = subscriptions.first()
            subscription.expires = existing_subscription.expires
            subscription.save()
        else:
            subscription.active = False
            subscription.is_trial = False
            subscription.expires = datetime.date.today() - datetime.timedelta(1)
            subscription.save()
            # subscription.issue_invoice()

        return subscription

    def done(self, form_list, form_dict, **kwargs):
        subscription = self.process_subscription(form_dict["subscription_form"])
        outlet = self.process_outlet(form_dict["outlet_form"], subscription)

        messages.success(self.request, "{}".format(_("Outlet has been created.")))

        return HttpResponseRedirect(reverse_lazy("outlet:outlet_list"))


class OutletUpdateView(SterlingRoleMixin, UpdateView):
    template_name = "dashboard/outlet/outlet_update.html"
    success_url = reverse_lazy("outlet:outlet_list")
    model = Outlet
    form_class = OutletUpdateForm

    def form_valid(self, form):
        form.save(commit=True)
        messages.success(self.request, "{}".format(_("Outlet has been updated.")))

        return HttpResponseRedirect(self.get_success_url())


class OutletAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Outlet.objects.all()
        qs = qs.filter(archieved=False)

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

    def get_result_label(self, item):
        return item.name

    def has_add_permission(self, request):
        if request.user.is_authenticated:
            return True
        return False
