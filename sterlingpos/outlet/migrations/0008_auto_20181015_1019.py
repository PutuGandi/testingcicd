# Generated by Django 2.0.4 on 2018-10-15 10:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20180926_0352'),
        ('outlet', '0007_auto_20181011_0750'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='outlet',
            unique_together={('branch_id', 'account'), ('account', 'id')},
        ),
    ]
