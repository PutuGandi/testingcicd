from django.test import TestCase
from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser
from django.contrib.sessions.middleware import SessionMiddleware
from sterlingpos.core.models import set_current_tenant, get_current_tenant

from model_mommy import mommy
from sterlingpos.accounts.models import Account
from ..forms import BaseOutletForm, OutletForm


class TestOutletForm(TestCase):

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        account = mommy.make(Account, name='Test Account')
        set_current_tenant(account)
        province = mommy.make('province.Province', name='Province')
        city = mommy.make('province.City', name='City', province=province)
        self.factory = RequestFactory()

        form_data = {
            'name': 'kawn',
            'phone': '021 - 12012931',
            'address': 'Jl. Jalan 21',
            'province': province.pk,
            'city': city.pk,
            'taxes': 10.00,
            'gratuity': 5.00,
            'transaction_code_prefix': 'kawn-store'
        }

        request = self.factory.post(reverse('account_signup'), form_data)
        request.user = AnonymousUser()

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        form = OutletForm(data=form_data)
        self.assertTrue(form.is_valid(), form.errors)
        outlet = form.save(request)

        self.assertEqual(outlet.name, form_data['name'])
        self.assertEqual(outlet.phone, form_data['phone'])
        self.assertEqual(outlet.address, form_data['address'])
        self.assertEqual(outlet.province.pk, form_data['province'])
        self.assertEqual(outlet.city.pk, form_data['city'])
