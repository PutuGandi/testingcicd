import json
from test_plus.test import TestCase
from model_mommy import mommy
from django.urls import reverse

from django.test.client import Client

from sterlingpos.accounts.models import Account
from sterlingpos.users.models import User
from sterlingpos.province.models import Province, City
from ..models import Outlet

class BaseOutletTest(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        self.user = User.objects.create_user(email='example@test.com', password='1sampai6', account=self.account)
        self.account.owners.get_or_create(account=self.account, user=self.user)
        
        self.province_1 = Province.objects.create(name='DKI Jakarta')
        self.province_2 = Province.objects.create(name='Jawa Barat')

        self.city_1 = City.objects.create(name='Jakarta Pusat', province=self.province_1)
        self.city_2 = City.objects.create(name='Jakarta Selatan', province=self.province_1)
        self.city_3 = City.objects.create(name='Bandung', province=self.province_2)

        self.outlet_1 =  Outlet.objects.create(
            branch_id='0001',
            name='branch 1',
            account=self.account,
            province=self.province_1,
            city=self.city_1
        )

        self.outlet_2 =  Outlet.objects.create(
            branch_id='0021',
            name='branch 2',
            account=self.account,
            province=self.province_2,
            city=self.city_3
        )

        self.outlet_3 =  Outlet.objects.create(
            branch_id='0011',
            name='branch 3',
            account=self.account,
            province=self.province_1,
            city=self.city_2
        )
        
        self.client = Client()
        self.client.login(email='example@test.com', password='1sampai6')


class OutletListJsonTest(BaseOutletTest):

    def setUp(self):
        super(OutletListJsonTest, self).setUp()
        self.url = reverse('outlet:data_outlet')

    def test_outlet_list_json(self):
        response = self.client.get(self.url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        data = content['data'][0]
        expected_result_valid = {
            'recordsTotal': 3,
            'recordsFiltered': 3,
            'columnsTotal': 8
        }
        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])
        self.assertEqual(len(data), expected_result_valid['columnsTotal'])

    def test_outlet_list_json_filter(self):
        data = {
            'draw': 5,
            'columns[0][data]': 0,
            'columns[0][name]': '',
            'columns[0][searchable]': True,
            'columns[0][orderable]': True,
            'columns[0][search][value]': 'branch 2',
            'columns[0][search][regex]': False,
        }

        response = self.client.get(self.url, data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            'recordsTotal': 3,
            'recordsFiltered': 1,
            'data_columns_name': '<a href="{}">{}</a>'.format(reverse('outlet:outlet_update', kwargs={'pk':self.outlet_2.pk}), self.outlet_2.name)
        }
        data = content['data'][0]

        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])
        self.assertEqual(data[0], expected_result_valid['data_columns_name'])


class ProvinceAutocompleteTest(BaseOutletTest):

    def test_province_autocomplete_success_request(self):
        expected_result = {
            'autocomplete': '{}'.format(self.province_2.name)
        }

        data = {
            'q': "Jawa"
        }

        request = self.client.get(reverse('catalogue:product_usage_autocomplete'), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(request.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(request.content)

        province_list = response_data['results']

        for province in province_list:
            self.assertEqual(province['text'], expected_result['autocomplete'])


class CityAutocompleteTest(BaseOutletTest):

    def test_city_autocomplete_success_request(self):
        expected_result = {
            'autocomplete': '{}'.format(self.city_3.name)
        }

        data = {
            'q': "Ban"
        }

        request = self.client.get(reverse('outlet:cities_autocomplete'), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(request.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(request.content)

        city_list = response_data['results']

        for city in city_list:
            self.assertEqual(city['text'], expected_result['autocomplete'])