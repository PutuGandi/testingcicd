from django.test import TestCase
from django.db import IntegrityError

from model_mommy import mommy

from sterlingpos.outlet.models import Outlet, OutletOptions

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


class TestOutlet(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Test')

    def test__str__(self):
        outlet = Outlet.objects.create(
            branch_id='0001',
            name='branch 1',
            account=self.account,
        )
        self.assertEqual(
            outlet.__str__(),
            f"{outlet.name} - {self.account.name}"
        )

    def test_signal(self):
        outlet = Outlet.objects.create(
            branch_id='0001',
            name='branch 1',
            account=self.account,
        )
        self.assertEqual(outlet.device_user.filter(permission='manager').count(), 1)
        self.assertFalse(outlet.table_management_setting.exists())

class TestOutletOptions(TestCase):
    def setUp(self):
        account = mommy.make('accounts.Account', name='Test')
        outlet = Outlet.objects.create(
            branch_id='0001',
            name='branch 1',
            account=account,
        )
        self.outlet_opt = mommy.make(
            OutletOptions, outlet=outlet, account=account, __fill_optional=True)

    def test__str__(self):
        self.assertEqual(
            self.outlet_opt.__str__(),
            'Outlet Options : {}'.format(self.outlet_opt.outlet.name)
        )
