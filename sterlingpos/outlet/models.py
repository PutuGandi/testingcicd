from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify

from model_utils.models import TimeStampedModel
from safedelete.models import HARD_DELETE

from sterlingpos.core.models import SterlingTenantModel, SterlingTenantForeignKey

from .storage import OverwriteStorage


def get_upload_path(instance, filename):
    ext = filename.split(".")[-1]
    new_filename = "{}_{}.{}".format(instance.branch_id, slugify(instance.name), ext)
    return "{}/outlet/{}".format(instance.account.pk, new_filename)


class Outlet(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_("Outlet Name"), blank=True, max_length=255)
    display_name = models.CharField(
        _("Displayed Outlet Name"), blank=True, max_length=255
    )
    branch_id = models.CharField(_("Outlet ID"), blank=True, max_length=255)
    transaction_code_prefix = models.CharField(
        _("Transaction Code Prefix"), blank=True, max_length=255
    )
    phone = models.CharField(_("Branch Phone"), blank=True, max_length=255)
    address = models.TextField(_("Address"), blank=True)
    province = models.ForeignKey(
        "province.Province",
        null=True,
        blank=True,
        verbose_name=_("province"),
        on_delete=models.PROTECT,
    )
    city = models.ForeignKey(
        "province.City",
        null=True,
        blank=True,
        verbose_name=_("city"),
        on_delete=models.PROTECT,
    )
    postal_code = models.CharField(_("Postal Code"), blank=True, max_length=255)
    outlet_code = models.CharField(_("Outlet Code"), max_length=50, blank=True)

    taxes = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    gratuity = models.DecimalField(
        max_digits=5, decimal_places=2, blank=True, null=True
    )

    device_user = models.ManyToManyField(
        "device.DeviceUser",
        blank=True,
        through="OutletDeviceUser",
        related_name="outlet",
    )

    archieved = models.BooleanField(default=False)
    enable_dashboard = models.BooleanField(default=True)

    outlet_image = models.ImageField(
        upload_to=get_upload_path,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        max_length=255,
    )

    def __str__(self):
        return "{} - {}".format(self.name, self.account)

    class Meta:
        unique_together = (
            ("branch_id", "account"),
            ("account", "id"),
        )
        ordering = ("name",)


class OutletDeviceUser(SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    outlet = models.ForeignKey(Outlet, on_delete=models.CASCADE)
    device_user = models.ForeignKey("device.DeviceUser", on_delete=models.CASCADE)


class OutletOptions(SterlingTenantModel):
    tax = models.PositiveIntegerField(_("Tax"), default=10)
    price_incl_tax = models.BooleanField(_("Price Include Tax"), default=True)
    outlet = models.OneToOneField(
        Outlet, on_delete=models.CASCADE, related_name="outlet"
    )

    def __str__(self):
        return "Outlet Options : {}".format(self.outlet.name)
