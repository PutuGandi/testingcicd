from django.contrib import admin

from .models import (
    Outlet,
)


class OutletAdmin(admin.ModelAdmin):
    #     """
    #         Admin View for Outlet
    #     """
    list_display = (
        "branch_id",
        "name",
        "transaction_code_prefix",
        "phone",
        "address",
        "province",
        "city",
        "postal_code",
        "taxes",
        "gratuity",
        "account",
    )

    search_fields = (
        "branch_id",
        "name",
        "account__name",
        "address",
        "province__name",
        "city__name",
    )


admin.site.register(Outlet, OutletAdmin)
