from django.db import models
from django.utils.translation import ugettext_lazy as _


class Province(models.Model):
    name = models.CharField(max_length=35)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return "%s" % self.name


class City(models.Model):
    name = models.CharField(max_length=50)
    province = models.ForeignKey(Province, verbose_name=_('province'), on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = _('cities')
        ordering = ['name']

    def __str__(self):
        return self.name