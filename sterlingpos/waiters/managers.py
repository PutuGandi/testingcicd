from sterlingpos.core.models import (
    SterlingTenantManager,
)


class WaiterManager(SterlingTenantManager):

    def get_queryset(self):
        return super(WaiterManager, self).get_queryset().filter(permission='waiter')

    def create(self, **kwargs):
        kwargs.update({'permission': 'waiter'})
        return super(WaiterManager, self).create(**kwargs)
