from django.apps import AppConfig
from django.db.models.signals import post_save


class WaitressConfig(AppConfig):
    name = 'sterlingpos.waiters'
    verbose_name = "Waiters Management"
    permission_name = "application"

    def ready(self):
        pass
