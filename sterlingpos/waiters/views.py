import csv
from datetime import timedelta

from django.conf import settings
from django.http import HttpResponseRedirect
from django.db.models import Q, Case, Value, When, BooleanField

from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.contrib import messages
from django.views.generic import (
    ListView, CreateView, UpdateView,
    DeleteView, TemplateView, View
)

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import JSONResponseMixin, AjaxResponseMixin

from sterlingpos.outlet.models import Outlet
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user

from sterlingpos.push_notification.models import RegisteredFCMDevice
from sterlingpos.waiters.models import (
    Waiter,
    WaiterSettings,
)
from sterlingpos.waiters.forms import (
    WaiterSettingsForm,
    WaiterForm,
    EnableWaiterManagementSettingsForm
)


class WaiterListJsonView(SterlingRoleMixin, BaseDatatableView):
    columns = ['username', 'name', 'archived', 'outlet', 'action']
    order_columns = ['username', 'name', 'archived', 'outlet', 'action']
    model = Waiter

    def get_initial_queryset(self):
        qs = super(WaiterListJsonView, self).get_initial_queryset()
        qs = qs.filter(
            permission=Waiter.PERMISSION.waiter).exclude(archived=True)

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(
                    self.request.user)).distinct()

        return qs

    def render_column(self, row, column):
        if column == 'username':
            if hasattr(row, 'device_user_image') and row.device_user_image._file:
                image_url = row.device_user_image.url,
            else:
                image_url = None
            action_data = {
                'username': escape(row.username),
                'image': image_url,
                'url': reverse_lazy("waiters:update", kwargs={'pk': row.pk}),
            }
            return action_data
        if column == 'action':
            action_data = {
                'id': row.id,
                'username': escape(row.username),
                'edit': reverse_lazy("waiters:update", kwargs={'pk': row.pk}),
                'archived': row.archived
            }
            return action_data
        if column == 'outlet':
            return escape("{}".format(", ".join(
                getattr(row, column).values_list('name', flat=True))))
        elif column == 'archived':
            if row.archived:
                return '{}'.format(_('Archived'))
            else:
                return '{}'.format(_('Active'))
        elif column == 'username':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy("waiters:update", kwargs={'pk': row.pk}), escape(row.username))
        else:
            return super(WaiterListJsonView, self).render_column(row, column)


class WaiterListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/waiters/list.html'


class WaiterStepView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/waiters/steps.html'


class WaiterCreateView(SterlingRoleMixin, CreateView):
    template_name = 'dashboard/waiters/create.html'
    model = Waiter
    success_url = reverse_lazy("waiters:list")
    form_class = WaiterForm

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def form_valid(self, form):
        response = super(WaiterCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Success add new waiter.')))
        return response


class WaiterUpdateView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/waiters/update.html'
    success_url = reverse_lazy("waiters:list")
    model = Waiter
    form_class = WaiterForm

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Success update waiter.')))
        return response


class WaiterDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            waiter_id = request.POST.get('id', '')
            waiter = Waiter.objects.get(pk=waiter_id)
            waiter.delete()
            messages.error(self.request, '{}'.format(
                _('Waiter has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class WaiterSettingsUpdateView(SterlingRoleMixin, UpdateView):
    model = WaiterSettings
    template_name = "dashboard/waiters/settings.html"
    form_class = WaiterSettingsForm

    def get_success_url(self):
        success_url = reverse("waiters:settings")
        return success_url

    def get_object(self, queryset=None):
        obj = self.request.user.account.waitersettings_set.first()
        return obj

    def get_initial(self):
        outlet = self.object.outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(outlet)
        initial = {
            'all_outlet': disabled_outlet.count() == 0,
        }
        return initial

    def form_valid(self, form):
        form.save()
        messages.success(self.request, '{}'.format(
            _('Waiter Setting has been updated.')))
        return HttpResponseRedirect(self.get_success_url())


class WaiterDeviceListJsonView(SterlingRoleMixin, BaseDatatableView):
    columns = ['name', 'last_user_login.name', 'last_login', 'is_active']
    order_columns = ['name', 'last_user_login__name', 'last_login', 'is_active']
    model = RegisteredFCMDevice

    def get_initial_queryset(self):
        qs = super(WaiterDeviceListJsonView, self).get_initial_queryset()
        qs = qs.filter(
            device_type=RegisteredFCMDevice.DEVICE_TYPE.waiter)
        inactivity_treshold = timezone.now() - timedelta(minutes=30)
        qs = qs.annotate(
            is_active=Case(
                When(modified__gte=inactivity_treshold, then=True),
                default=False,
                output_field=BooleanField(),
            )
        )
        return qs

    def render_column(self, row, column):
        if column == 'is_active':
            data = {
                'is_active': row.is_active,
                'since': row.modified,
            }
            return data
        else:
            return super(WaiterDeviceListJsonView, self).render_column(row, column)


class WaiterDeviceListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/waiters/device.html'


class WaiterManagementSettingsEnableUpdateViewJSON(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_object(self):
        obj = self.request.user.account.waitersettings_set.first()
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }

        form = EnableWaiterManagementSettingsForm(
            data=request.POST,
            instance=self.get_object()
        )

        if form.is_valid():
            waiter_settings = form.save()
            status_code = 200
            response['status'] = 'success'
            response['data'] = waiter_settings.is_enabled
        else:
            response['data'] = form.errors
            status_code = 400

        return self.render_json_response(response, status=status_code)


class WaiterManagementDisableView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/waiters/disable.html'
