from __future__ import unicode_literals, absolute_import

from datetime import datetime

from django.db import models, DataError
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices
from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.core.models import SterlingTenantModel, SterlingTenantForeignKey
from sterlingpos.device.models import DeviceUser
from sterlingpos.waiters.managers import WaiterManager


class WaiterSettings(SterlingTenantModel, TimeStampedModel):
    is_enabled = models.BooleanField(default=True)
    outlet = models.ManyToManyField(
        "outlet.Outlet", blank=True,
        through='WaiterEnabledOutlet',
        related_name='waiter_setting')

    class Meta:
        verbose_name = "WaitersSetting"
        verbose_name_plural = "WaitersSettings"
        unique_together = (('account', 'id'),)


class WaiterEnabledOutlet(SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    waiter_setting = models.ForeignKey(WaiterSettings, on_delete=models.CASCADE)
    outlet = models.ForeignKey("outlet.Outlet", on_delete=models.CASCADE)


class Waiter(DeviceUser):

    objects = WaiterManager()

    class Meta:
        proxy = True
