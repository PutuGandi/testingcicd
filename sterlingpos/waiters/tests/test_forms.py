from django.test import TestCase
from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.waiters.forms import (
    WaiterSettingsForm,
    WaiterForm,
)


class TestWaitersSettingsForm(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account')
        self.outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1', account=self.account)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', branch_id='branch-2', account=self.account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        form_data = {
            'is_enabled': True,
            'outlet': [self.outlet.pk],
        }

        form = WaiterSettingsForm(form_data)
        self.assertTrue(form.is_valid())
        waiters_setting = form.save()

        self.assertTrue(waiters_setting.is_enabled)
        self.assertEqual(waiters_setting.outlet.count(), 1)
        self.assertFalse(
            waiters_setting.outlet.filter(pk=self.outlet_2.pk).exists())

class TestWaitersForm(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account')
        self.outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1', account=self.account)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', branch_id='branch-2', account=self.account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        form_data = {
            'username': 'testament',
            'name': 'Mr. Testament',
            'email': 'testament@mail.com',
            'pin': '123456',
            'outlet': [self.outlet.pk],
            'permission': 'waiter',
        }

        form = WaiterForm(form_data)
        self.assertTrue(form.is_valid())
        waiters = form.save()

        self.assertEqual(waiters.outlet.count(), 1)
        self.assertEqual(waiters.permission, "waiter")
        self.assertFalse(waiters.outlet.filter(pk=self.outlet_2.pk).exists())
