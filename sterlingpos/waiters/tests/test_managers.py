from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.device.models import DeviceUser
from sterlingpos.waiters.models import Waiter


class TestWaitersManager(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account')
        self.outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1', account=self.account)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', branch_id='branch-2', account=self.account)

    def test_get_queryset(self):
        staff = DeviceUser.objects.create(
            permission=DeviceUser.PERMISSION.staff,
            username='staff',
            name='staff',
            email='staff@mail.com',
            pin='123456',
            account=self.account,)
        waiter = Waiter.objects.create(
            username='waiter',
            name='waiter',
            email='waiter@mail.com',
            pin='123456',
            account=self.account,)

        deleted_waiter = Waiter.objects.create(
            username='waiter (deleted)',
            name='waiter',
            email='waiter@mail.com',
            pin='123456',
            account=self.account,)
        deleted_waiter.delete()

        self.assertFalse(Waiter.objects.filter(pk=staff.pk).exists())
        self.assertFalse(Waiter.objects.filter(pk=deleted_waiter.pk).exists())
        self.assertTrue(Waiter.objects.filter(pk=waiter.pk).exists())

    def test_create(self):
        waiter = Waiter.objects.create(
            username='waiter',
            name='waiter',
            email='waiter@mail.com',
            pin='123456',
            account=self.account)
        self.assertEqual(waiter.permission, Waiter.PERMISSION.waiter)
