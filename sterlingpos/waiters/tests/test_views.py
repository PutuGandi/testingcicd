import datetime
from django.http import Http404
from django.urls import reverse
from django.test import RequestFactory
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from model_mommy import mommy

from test_plus.test import TestCase

from sterlingpos.users.models import User
from sterlingpos.accounts.models import Account
from sterlingpos.device.models import DeviceUser
from sterlingpos.waiters.models import WaiterEnabledOutlet
from sterlingpos.waiters.views import (
    WaiterListJsonView,
    WaiterDeleteView,
    WaiterSettingsUpdateView,
    WaiterDeviceListJsonView,
)

from sterlingpos.core.models import set_current_tenant, get_current_tenant


class TestWaiterListJsonView(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make(Account, name="Company A")

        self.user = User.objects.create_user(
            email="testuser@example.com", password="example", account=self.account)
        self.account.owners.create(account=self.account, user=self.user)

        self.waiter_list = mommy.make(
            DeviceUser,
            permission=DeviceUser.PERMISSION.waiter,
            account=self.account,
            _quantity=3)
        self.staff_list = mommy.make(
            DeviceUser,
            permission=DeviceUser.PERMISSION.staff,
            account=self.account,
            _quantity=3)

    def test_get_initial_queryset(self):
        view = WaiterListJsonView()
        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request

        self.assertEqual(view.get_initial_queryset().count(), 3)

    def test_render_column(self):
        view = WaiterListJsonView()
        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request

        rendered_column = view.render_column(self.waiter_list[0], 'action')
        self.assertIn('edit', rendered_column)
        self.assertIn('username', rendered_column)
        self.assertIn('id', rendered_column)


class TestWaiterSettingsUpdateView(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make(Account, name="Company A")
        self.user = mommy.make(User, email="test@mail.com", account=self.account)
        self.outlet_1 = mommy.make(
            'outlet.Outlet', account=self.account, branch_id='branch-1')
        self.outlet_2 = mommy.make(
            'outlet.Outlet', account=self.account, branch_id='branch-2')

    def test_get_initial_on_empty_settings(self):
        view = WaiterSettingsUpdateView()
        waiter_settings = self.user.account.waitersettings_set.first()
        view.object = waiter_settings

        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request

        self.assertFalse(view.get_initial().get('all_outlet'))

    def test_get_initial_on_existing_settings(self):
        waiter_settings = self.user.account.waitersettings_set.first()
        WaiterEnabledOutlet.objects.create(
            waiter_setting=waiter_settings,
            outlet=self.outlet_1,
            account=waiter_settings.account
        )
        WaiterEnabledOutlet.objects.create(
            waiter_setting=waiter_settings,
            outlet=self.outlet_2,
            account=waiter_settings.account
        )

        view = WaiterSettingsUpdateView()
        view.object = waiter_settings
        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request

        self.assertTrue(view.get_initial().get('all_outlet'))
