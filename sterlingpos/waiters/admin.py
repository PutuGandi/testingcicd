from django.contrib import admin

from sterlingpos.device.admin import DeviceUserAdmin

from .models import (
    Waiter,
    WaiterSettings,
)


class WaiterAdmin(DeviceUserAdmin):
    pass


class WaiterSettingsAdmin(admin.ModelAdmin):
    list_display = ("account",)
    search_fields = ("account__name",)


admin.site.register(WaiterSettings, WaiterSettingsAdmin)
admin.site.register(Waiter, WaiterAdmin)
