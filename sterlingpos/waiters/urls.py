# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "waiters"
urlpatterns = [
    url(regex=r'^$', view=views.WaiterListView.as_view(),
        name='list'),
    url(regex=r'^data/$', view=views.WaiterListJsonView.as_view(),
        name='data'),

    url(regex=r'^devices/$', view=views.WaiterDeviceListView.as_view(),
        name='device-list'),
    url(regex=r'^devices/data/$', view=views.WaiterDeviceListJsonView.as_view(),
        name='device-data'),

    url(regex=r'^settings/$', view=views.WaiterSettingsUpdateView.as_view(),
        name='settings'),
    url(r'^settings/enable/$', view=views.WaiterManagementSettingsEnableUpdateViewJSON.as_view(),
        name='settings_enable'),
    url(regex=r'^create/$', view=views.WaiterCreateView.as_view(),
        name='create'),
    url(regex=r'^delete/$', view=views.WaiterDeleteView.as_view(),
        name='delete'),
    url(regex=r'^(?P<pk>[0-9]+)/update/$', view=views.WaiterUpdateView.as_view(),
        name='update'),
    url(regex=r'^settings/disable/$', view=views.WaiterManagementDisableView.as_view(),
        name='settings_disable'),

    url(regex=r'^steps/$', view=views.WaiterStepView.as_view(),
        name='steps'),
]
