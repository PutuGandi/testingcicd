from django import forms
from django.template.loader import render_to_string

from sterlingpos.outlet.models import Outlet

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Submit

from .models import WaiterSettings, Waiter
from django.utils.translation import ugettext_lazy as _

from sterlingpos.device.forms import DeviceUserStaffForm
from sterlingpos.device.models import DeviceUser
from sterlingpos.waiters.models import WaiterEnabledOutlet
from sterlingpos.core.bootstrap import KawnField, KawnFieldSetWithHelpText


class EnableWaiterManagementSettingsForm(forms.ModelForm):
    class Meta:
        model = WaiterSettings
        exclude = ('account', 'outlet')

    def __init__(self, *args, **kwargs):
        super(EnableWaiterManagementSettingsForm, self).__init__(*args, **kwargs)
        self.fields['is_enabled'].label = _('Enable Table Management')
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            KawnField('is_enabled', template='dashboard/table_management/switch_input/switch.html')
        )
    
    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        if not obj.is_enabled:
            obj.outlet.clear()
        else:
            outlets = [
                WaiterEnabledOutlet(
                    outlet=outlet,
                    waiter_setting=obj,
                    account=obj.account
                )
                for outlet in obj.account.outlet_set.all()
            ]
            WaiterEnabledOutlet.objects.bulk_create(outlets)
            obj.outlet.invalidated_update()
        return obj


class WaiterSettingsForm(forms.ModelForm):

    all_outlet = forms.BooleanField(initial=True, label=_("Semua Outlet"))

    class Meta:
        model = WaiterSettings
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        super(WaiterSettingsForm, self).__init__(*args, **kwargs)
        self.fields['outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['outlet'].label = ''
        self.fields['all_outlet'].required = False
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet', template='dashboard/table_management/switch_input/float_right_switch.html'),
                KawnField('outlet', template='dashboard/table_management/switch_input/multiple_switch.html'),
                help_text=render_to_string('help_text/waiter_management_outlet.html')
            ),
        )

    def update_m2m(self, waiter_setting, object_list, rel_field, intermediate_model):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "waiter_setting": waiter_setting,
                    "account": waiter_setting.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        obj = super(WaiterSettingsForm, self).save(*args, **kwargs)
        self.update_m2m(
            obj, outlets, obj.outlet, WaiterEnabledOutlet)
        return obj


class WaiterForm(DeviceUserStaffForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        del self.fields['role']

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        obj.permission = DeviceUser.PERMISSION.waiter
        obj.save()

        return obj
