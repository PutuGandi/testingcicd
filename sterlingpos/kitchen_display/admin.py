from django.contrib import admin

from sterlingpos.kitchen_display.models import (
    KitchenDisplaySetting,
)


class KitchenDisplaySettingAdmin(admin.ModelAdmin):
    list_display = ('account',)
    search_fields = ('account',)


admin.site.register(KitchenDisplaySetting, KitchenDisplaySettingAdmin)
