# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = "kitchen_display"
urlpatterns = [
    
    url(regex=r'^$',
        view=views.KitchenCardListView.as_view(),
        name='list'),
    url(regex=r'^data/$',
        view=views.KitchenCardListJson.as_view(),
        name='data'),
    url(regex=r'^data/outlet/(?P<outlet_pk>[0-9]+)/$',
        view=views.KitchenCardListJson.as_view(),
        name='data'),

    url(regex=r'^(?P<pk>[0-9]+)/$',
        view=views.KitchenCardItemListView.as_view(),
        name='item_list'),
    url(regex=r'^(?P<pk>[0-9]+)/data/$',
        view=views.KitchenCardItemListJson.as_view(),
        name='item_data'),

    url(regex=r'^settings/$',
        view=views.KitchenDisplaySettingUpdateView.as_view(),
        name='settings'),
    url(r'^settings/enable/$', view=views.KitchenDisplayEnableUpdateViewJSON.as_view(),
        name='settings_enable'),
    url(regex=r'^settings/disable/$', view=views.KitchenDisplayDisableView.as_view(),
        name='settings_disable'),
    url(regex=r'^settings/outlet/$', view=views.KitchenDisplayOutletSettingsUpdateView.as_view(),
        name='settings_outlet'),
]
