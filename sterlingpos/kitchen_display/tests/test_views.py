from django.test import RequestFactory
from model_mommy import mommy

from test_plus.test import TestCase

from sterlingpos.users.models import User
from sterlingpos.accounts.models import Account
from sterlingpos.kitchen_display.views import (
    KitchenDisplaySettingUpdateView,
)


class TestKitchenDisplaySettingUpdateView(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make(Account, name="Company A")
        self.user = mommy.make(
            User, email="test@mail.com", account=self.account)
        self.outlet_1 = mommy.make(
            'outlet.Outlet', account=self.account, branch_id='branch-1')
        self.outlet_2 = mommy.make(
            'outlet.Outlet', account=self.account, branch_id='branch-2')

    def test_get_initial_on_empty_settings(self):
        view = KitchenDisplaySettingUpdateView()
        kitchen_display_settings = self.user.account.kitchendisplaysetting_set.first()
        view.object = kitchen_display_settings

        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request

        self.assertFalse(view.get_initial().get('all_outlet'))

    def test_get_initial_on_existing_settings(self):
        kitchen_display_settings = self.user.account.kitchendisplaysetting_set.first()
        kitchen_display_settings.kitchendisplayoutlet_set.create(
            outlet=self.outlet_1, account=self.outlet_1.account)
        kitchen_display_settings.kitchendisplayoutlet_set.create(
            outlet=self.outlet_2, account=self.outlet_2.account)

        view = KitchenDisplaySettingUpdateView()
        view.object = kitchen_display_settings
        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request

        self.assertTrue(view.get_initial().get('all_outlet'))
