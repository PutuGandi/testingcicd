from django.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.kitchen_display.forms import (
    KitchenDisplaySettingForm,
)


class TestKitchenDisplaySettingForm(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account')
        self.outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-1', account=self.account)
        self.outlet_2 = mommy.make(
            'outlet.Outlet', branch_id='branch-2', account=self.account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save(self):
        form_data = {
            'is_enabled': True,
            'enabled_outlet': [self.outlet.pk],
        }

        form = KitchenDisplaySettingForm(
            form_data, instance=self.account.kitchendisplaysetting_set.first())
        self.assertTrue(form.is_valid())
        kitchen_display_setting = form.save()

        self.assertTrue(kitchen_display_setting.is_enabled)
        self.assertEqual(kitchen_display_setting.enabled_outlet.count(), 1)
        self.assertFalse(
            kitchen_display_setting.enabled_outlet.filter(
                pk=self.outlet_2.pk).exists())
