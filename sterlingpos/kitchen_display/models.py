from __future__ import unicode_literals, absolute_import

from django.db import models

from model_utils.models import TimeStampedModel

from safedelete.models import HARD_DELETE

from sterlingpos.core.models import (
    SterlingTenantModel, SterlingTenantForeignKey
)


class KitchenDisplaySetting(SterlingTenantModel, TimeStampedModel):
    is_enabled = models.BooleanField(default=True)
    enabled_outlet = models.ManyToManyField(
        "outlet.Outlet", related_name='kitchen_settings',
        through='KitchenDisplayOutlet')

    class Meta:
        verbose_name = "KitchenDisplaySetting"
        verbose_name_plural = "KitchenDisplaySettings"
        unique_together = (('account', 'id'),)


class KitchenDisplayOutlet(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    outlet = models.OneToOneField(
        'outlet.Outlet', on_delete=models.CASCADE)
    kitchen_setting = SterlingTenantForeignKey(
        KitchenDisplaySetting, on_delete=models.CASCADE)
    urgent_time = models.PositiveIntegerField(default=60)

    class Meta:
        verbose_name = "KitchenDisplayOutlet"
        verbose_name_plural = "KitchenDisplayOutlets"
        unique_together = (('account', 'id'),)


class KitchenCard(SterlingTenantModel, TimeStampedModel):

    outlet = SterlingTenantForeignKey(
        'outlet.Outlet', on_delete=models.CASCADE)
    code = models.CharField(max_length=120)

    start_date = models.DateTimeField(db_index=True)
    end_date = models.DateTimeField(db_index=True, null=True)
    duration = models.DurationField(db_index=True)
    
    void_at = models.DateTimeField(db_index=True, null=True)

    class Meta:
        verbose_name = "KitchenCard"
        verbose_name_plural = "KitchenCards"
        unique_together = (('account', 'id'),)


class KitchenCardItem(SterlingTenantModel, TimeStampedModel):

    kitchen_card = SterlingTenantForeignKey(
        KitchenCard, on_delete=models.CASCADE,
        related_name="card_items")

    product = SterlingTenantForeignKey(
        "catalogue.Product", on_delete=models.CASCADE,
        related_name="kitchen_card_items")

    quantity = models.PositiveIntegerField()

    created_at = models.DateTimeField(db_index=True)
    cooked_at = models.DateTimeField(db_index=True, null=True)
    finished_at = models.DateTimeField(db_index=True, null=True)

    void_at = models.DateTimeField(db_index=True, null=True)

    class Meta:
        verbose_name = "KitchenCardItem"
        verbose_name_plural = "KitchenCardItems"
        unique_together = (('account', 'id'),)
