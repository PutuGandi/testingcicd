from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.utils.html import escape
from django.utils.timezone import localtime

from django.utils.translation import ugettext_lazy as _
from django.views.generic import (
    UpdateView,
    View,
    TemplateView,
)
from django_datatables_view.base_datatable_view import BaseDatatableView
from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins

from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.outlet.models import Outlet
from sterlingpos.kitchen_display.models import (
    KitchenDisplaySetting,
    KitchenCard,
    KitchenCardItem,
)
from sterlingpos.kitchen_display.forms import (
    KitchenDisplaySettingForm,
    EnableKitchenDisplaySettingForm,
    KitchenDisplayOutletSettingForm
)
from braces.views import JSONResponseMixin, AjaxResponseMixin
from sterlingpos.role.mixins import SterlingRoleMixin


class KitchenDisplaySettingUpdateView(SterlingRoleMixin, UpdateView):
    model = KitchenDisplaySetting
    template_name = "dashboard/kitchen_display/settings.html"
    form_class = KitchenDisplaySettingForm

    def get_success_url(self):
        success_url = reverse("kitchen_display:settings")
        return success_url

    def get_object(self, queryset=None):
        obj = self.request.user.account.kitchendisplaysetting_set.first()
        return obj

    def get_initial(self):
        enabled_outlet = self.object.enabled_outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(enabled_outlet)
        initial = {
            'all_outlet': disabled_outlet.count() == 0,
        }
        return initial

    def form_valid(self, form):
        form.save()
        messages.success(self.request, '{}'.format(
            _('Kitchen Display has been updated.')))
        return HttpResponseRedirect(self.get_success_url())


class KitchenDisplayOutletSettingsUpdateView(SterlingRoleMixin, UpdateView):
    model = KitchenDisplaySetting
    template_name = 'dashboard/kitchen_display/outlet_settings.html'
    form_class = KitchenDisplayOutletSettingForm

    def get_object(self, queryset=None):
        obj = self.request.user.account.kitchendisplaysetting_set.first()
        return obj

    def get_success_url(self):
        success_url = reverse('kitchen_display:settings_outlet')
        return success_url

    def get_initial(self):
        enabled_outlet = self.object.enabled_outlet.all()
        existing_outlet = Outlet.objects.all()

        disabled_outlet = existing_outlet.difference(enabled_outlet)
        initial = {
            'all_outlet': disabled_outlet.count() == 0,
        }
        return initial

    def form_valid(self, form):
        form.save()
        messages.success(self.request, '{}'.format(
            _('Kitchen Display has been updated.')))
        return HttpResponseRedirect(self.get_success_url())


class KitchenDisplayEnableUpdateViewJSON(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_object(self):
        obj = self.request.user.account.kitchendisplaysetting_set.first()
        return obj

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }

        form = EnableKitchenDisplaySettingForm(
            data=request.POST,
            instance=self.get_object()
        )

        if form.is_valid():
            kitchen_display = form.save()
            status_code = 200
            response['status'] = 'success'
            response['data'] = kitchen_display.is_enabled
        else:
            response['data'] = form.errors
            status_code = 400

        return self.render_json_response(response, status=status_code)


class KitchenDisplayDisableView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/kitchen_display/disable.html'


class KitchenCardListJson(
        SterlingRoleMixin,
        CSVExportMixin, DatatableMixins,
        BaseDatatableView):

    columns = [
        "code",
        "outlet.name",
        "start_date",
        "end_date",
        "duration",
        "void_at",
    ]
    order_columns = [
        "code",
        "outlet__name",
        "start_date",
        "end_date",
        "duration",
        "void_at",
    ]
    datetime_col = [
        'start_date',
        'end_date',
        'void_at',
    ]
    csv_header = [
        "Code",
        "Outlet",
        "Start Date",
        "End Date",
        "Duration",
        "Void at",
    ]

    model = KitchenCard

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()

        outlet_pk = self.kwargs.get('outlet_pk')
        outlet_qs = get_outlet_of_user(self.request.user)

        if outlet_pk:
            outlet = get_object_or_404(
                outlet_qs,
                pk=outlet_pk)
        else:
            outlet = None

        try:
            start_date = datetime.strptime(
                self.request.GET.get('start_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            end_date = datetime.strptime(
                self.request.GET.get('end_date', None),
                "%Y-%m-%d %H:%M:%S"
            )
        except (TypeError, ValueError) as e:
            end_date = start_date + relativedelta(
                hour=23, minute=59)

        if self.request.user.is_owner:
            qs = qs.filter(outlet__in=outlet_qs)

        if start_date:
            qs = qs.filter(start_date__gte=start_date)

        if end_date:
            qs = qs.filter(end_date__lte=end_date)

        return qs

    def get_csv_filename(self):
        csv_filename = "Kitchen_Card"
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get('outlet_pk'):
            outlet = get_object_or_404(
                outlet_qs,
                pk=self.kwargs.get('outlet_pk'))
            csv_filename = "{}_{}".format(
                csv_filename, slugify(outlet.name))

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def render_code(self, obj):
        return '<a href="{}">{}</a>'.format(
            reverse_lazy("kitchen_display:item_list", kwargs={"pk": obj.pk}),
            escape(obj.code),
        )
 
    def render_start_date(self, obj):
        local_time = localtime(obj.start_date)
        return local_time.strftime('%Y-%m-%d %H:%M:%S')

    def render_end_date(self, obj):
        if obj.end_date:
            local_time = localtime(obj.end_date)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')

    def render_void_at(self, obj):
        if obj.void_at:
            local_time = localtime(obj.void_at)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')


class KitchenCardListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/kitchen_display/kitchen_card_list.html"


class KitchenCardItemListJson(
        SterlingRoleMixin,
        CSVExportMixin, DatatableMixins,
        BaseDatatableView):

    columns = [
        "product",
        "created_at",
        "cooked_at",
        "finished_at",
        "void_at",
    ]
    order_columns = [
        "product",
        "created_at",
        "cooked_at",
        "finished_at",
        "void_at",
    ]
    datetime_col = [
        'created_at',
        'cooked_at',
        'finished_at',
        'void_at',
    ]
    csv_header = [
        "Product",
        "Created At",
        "Cooked At",
        "Finished at",
        "Void at",
    ]

    model = KitchenCardItem

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.filter(
            kitchen_card=self.kwargs.get("pk")
        )
        return qs

    def get_csv_filename(self):
        csv_filename = "Kitchen_Card_Item"
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get('outlet_pk'):
            outlet = get_object_or_404(
                outlet_qs,
                pk=self.kwargs.get('outlet_pk'))
            csv_filename = "{}_{}".format(
                csv_filename, slugify(outlet.name))

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def render_created_at(self, obj):
        if obj.created_at:
            local_time = localtime(obj.created_at)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')

    def render_cooked_at(self, obj):
        if obj.cooked_at:
            local_time = localtime(obj.cooked_at)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')

    def render_finished_at(self, obj):
        if obj.finished_at:
            local_time = localtime(obj.finished_at)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')

    def render_void_at(self, obj):
        if obj.void_at:
            local_time = localtime(obj.void_at)
            return local_time.strftime('%Y-%m-%d %H:%M:%S')


class KitchenCardItemListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/kitchen_display/kitchen_card_item_list.html"

