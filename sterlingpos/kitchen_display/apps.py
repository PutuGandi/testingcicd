from django.apps import AppConfig
from django.db.models.signals import post_save


class KitchenDisplayConfig(AppConfig):
    name = 'sterlingpos.kitchen_display'
    verbose_name = "Kitchen Display"
    permission_name = "application"

    def ready(self):
        pass
