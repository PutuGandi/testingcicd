from django import forms
from django.template.loader import render_to_string

from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout

from sterlingpos.outlet.models import Outlet
from sterlingpos.kitchen_display.models import (
    KitchenDisplaySetting,
    KitchenDisplayOutlet,
)
from sterlingpos.core.bootstrap import KawnFieldSet, KawnField, KawnFieldSetWithHelpText


class EnableKitchenDisplaySettingForm(forms.ModelForm):
    class Meta:
        model = KitchenDisplaySetting
        exclude = ('account', 'enabled_outlet')

    def __init__(self, *args, **kwargs):
        super(EnableKitchenDisplaySettingForm, self).__init__(*args, **kwargs)
        self.fields['is_enabled'].label = _('Enable Kitchen Display')
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            KawnField('is_enabled', template='dashboard/table_management/switch_input/switch.html')
        )

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        if not obj.is_enabled:
            obj.enabled_outlet.clear()
        else:
            outlets = [
                KitchenDisplayOutlet(
                    outlet=outlet,
                    kitchen_setting=obj,
                    account=obj.account
                )
                for outlet in obj.account.outlet_set.all()
            ]
            KitchenDisplayOutlet.objects.bulk_create(outlets)
        obj.account.outlet_set.all().invalidated_update()
        obj.enabled_outlet.invalidated_update()
        return obj


class KitchenDisplaySettingForm(forms.ModelForm):
    DEFAULT_URGENT_TIME = 60
    all_outlet = forms.BooleanField(initial=True, label=_("Semua Outlet"))
    urgent_time = forms.IntegerField(
        min_value=1,
        initial=DEFAULT_URGENT_TIME,
        label=_("General Urgent Time"),
        required=False)

    class Meta:
        model = KitchenDisplaySetting
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        super(KitchenDisplaySettingForm, self).__init__(*args, **kwargs)
        self.fields['enabled_outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['enabled_outlet'].label = ''
        self.fields['all_outlet'].required = False

        kitchen_outlet = KitchenDisplayOutlet.objects.first()

        if self.instance and self.instance.enabled_outlet.first():
            if hasattr(kitchen_outlet, 'urgent_time'):
                self.fields['urgent_time'].initial = KitchenDisplayOutlet.objects.first().urgent_time
            else:
                self.fields['urgent_time'].initial = 60
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "Display Settings",
                KawnField("urgent_time"),
                help_text=render_to_string('help_text/kitchen_display_urgent_time.html')
            ),
            KawnField('all_outlet', type='hidden'),
            KawnField('enabled_outlet', type='hidden')
        )

    def clean_urgent_time(self):
        return self.cleaned_data.get('urgent_time') or self.DEFAULT_URGENT_TIME

    def process_outlet(self, cleaned_data):
        self.instance.enabled_outlet.clear()
        enabled_outlet = cleaned_data.get('enabled_outlet')
        urgent_time = cleaned_data.get('urgent_time')
        KitchenDisplayOutlet.objects.bulk_create([
            KitchenDisplayOutlet(**{
                'outlet': outlet,
                'kitchen_setting': self.instance,
                'urgent_time': urgent_time,
                'account': self.instance.account,
            }) for outlet in enabled_outlet
        ])
        self.instance.enabled_outlet.invalidated_update()

    def save(self, commit=True):
        obj = super(KitchenDisplaySettingForm, self).save(commit=False)
        obj.save()
        self.process_outlet(self.cleaned_data)
        return obj


class KitchenDisplayOutletSettingForm(forms.ModelForm):
    DEFAULT_URGENT_TIME = 60
    all_outlet = forms.BooleanField(initial=True, label=_("Semua Outlet"))
    urgent_time = forms.IntegerField(
        initial=DEFAULT_URGENT_TIME, label=_("General Urgent Time"), required=False)

    class Meta:
        model = KitchenDisplaySetting
        exclude = ('account', 'is_enabled')

    def __init__(self, *args, **kwargs):
        super(KitchenDisplayOutletSettingForm, self).__init__(*args, **kwargs)
        self.fields['enabled_outlet'] = forms.ModelMultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            queryset=Outlet.objects.all())
        self.fields['enabled_outlet'].label = ''
        self.fields['all_outlet'].required = False

        kitchen_outlet = KitchenDisplayOutlet.objects.first()

        if self.instance and self.instance.enabled_outlet.first():
            if hasattr(kitchen_outlet, 'urgent_time'):
                self.fields['urgent_time'].initial = KitchenDisplayOutlet.objects.first().urgent_time
            else:
                self.fields['urgent_time'].initial = 60

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "col-lg-12 px-0"
        self.helper.label_class = "col-lg-12 px-0"
        self.helper.form_tag = False
        self.helper.layout = Layout(
            KawnField("urgent_time", type='hidden'),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet', template='dashboard/table_management/switch_input/float_right_switch.html'),
                KawnField('enabled_outlet', template='dashboard/table_management/switch_input/multiple_switch.html'),
                help_text=render_to_string('help_text/kitchen_display_outlet.html')
            ),
        )

    def clean_urgent_time(self):
        return self.cleaned_data.get('urgent_time') or self.DEFAULT_URGENT_TIME

    def process_outlet(self, cleaned_data):
        self.instance.enabled_outlet.clear()
        enabled_outlet = cleaned_data.get('enabled_outlet')
        urgent_time = cleaned_data.get('urgent_time')
        KitchenDisplayOutlet.objects.bulk_create([
            KitchenDisplayOutlet(**{
                'outlet': outlet,
                'kitchen_setting': self.instance,
                'urgent_time': urgent_time,
                'account': self.instance.account,
            }) for outlet in enabled_outlet
        ])
        self.instance.enabled_outlet.invalidated_update()
        self.instance.account.outlet_set.all().invalidated_update()


    def save(self, commit=True):
        obj = super(KitchenDisplayOutletSettingForm, self).save(commit=False)
        obj.save()
        self.process_outlet(self.cleaned_data)
        return obj
