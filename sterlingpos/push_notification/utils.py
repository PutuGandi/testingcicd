import logging

from django.conf import settings
from django.utils.translation import ugettext_lazy as _, ugettext
from django.db.models import Model

from pyfcm import FCMNotification
from rest_framework.fields import DateTimeField

logger = logging.getLogger(__name__)


class PushService(FCMNotification):

    def __init__(self, *args, **kwargs):
        super(PushService, self).__init__(
            api_key=settings.FCM_API_KEY)


class PushNotificationBasicHandler:
    serializer_class = None

    STATUS_CODE = [
        "UPDATE", "DELETE", "RESYNC",
    ]

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.get("instance")

    @property
    def is_valid(self):
        return True

    def get_serializer_class(self):
        return self.serializer_class

    def get_serialized_data(self, instance):
        serializer_class = self.get_serializer_class()
        if serializer_class:
            serializer = serializer_class(instance=instance)
            return serializer.data

    def build_data_message(self, mode_name, status_code):

        modified = getattr(self.instance, "modified", None)
        if modified:
            modified = DateTimeField().to_representation(modified)

        data_message = {
            "mode": mode_name,
            "title": "{} {}".format(
                self.instance.__class__._meta.verbose_name.upper(),
                status_code),
            "message": "Please sync your data.....",
            "object_id": self.instance.pk,
            "serialized_data": self.get_serialized_data(self.instance),
            "modified": modified,
        }
        return data_message

    def get_mode_name(self, status_code):

        mode_name = "{}_{}".format(
            status_code,
            self.instance.__class__._meta.verbose_name.upper())

        return mode_name

    def get_fcm_queryset(self):
        queryset = self.instance.account.registeredfcmdevice_set.all()
        return queryset

    def get_registration_ids(self):
        fcm_devices_qs = self.get_fcm_queryset()
        registration_ids = fcm_devices_qs.values_list(
            "registered_id", flat=True)
        return registration_ids

    def get_data_message(self, status_code):
        if status_code not in self.STATUS_CODE:
            raise ValueError(_("Not a valid status_code."))

        mode_name = self.get_mode_name(status_code)
        data_message = self.build_data_message(mode_name, status_code)

        logger.info(f"Push Notification data={data_message}")

        return data_message


class PushNotificationHandlerAdapter:
    handlers = {}

    def register_handler(self, model, handler):
        if not issubclass(model, Model):
            raise ValueError(_("Not a correct subclass of django Model class."))
 
        if not issubclass(handler, PushNotificationBasicHandler):
            raise ValueError(_("Not a correct subclass of PushNotificationBasicHandler class."))

        self.handlers[model] = handler
        return handler


push_notification_adapter = PushNotificationHandlerAdapter()

def get_handler_for_instance(instance):
    handler_class = push_notification_adapter.handlers.get(
        instance.__class__, PushNotificationBasicHandler)

    return handler_class(instance=instance)


class AppQueuePushHandler(PushNotificationBasicHandler):
    
    STATUS_CODE = [
        "PUSH",
    ]
    def get_mode_name(self, status_code):
        mode_name = super().get_mode_name(status_code)
        mode_name = "{}_QUEUE".format(mode_name)
        return mode_name

    def build_data_message(self, mode_name, status_code):
        data_message = {
            "mode": mode_name,
            "title": "Push Queue",
            "message": "Please push apps queue.....",
            "object_id": self.instance.pk,
        }
        return data_message

    def get_fcm_queryset(self):
        qs = super().get_fcm_queryset()
        qs = qs.filter(outlet=self.instance)
        return qs

