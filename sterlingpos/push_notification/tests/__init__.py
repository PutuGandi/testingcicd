import factory


class RegisteredFCMDeviceFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'customer-{0}'.format(n))
    registered_id = factory.Sequence(lambda n: 'registered_id-{0}'.format(n))

    class Meta:
        model = 'push_notification.RegisteredFCMDevice'
