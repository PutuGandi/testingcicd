from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.push_notification.models import RegisteredFCMDevice


class TestFCMDevice(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='account-1')

    def test__str__(self):
        self.registered_fcm = RegisteredFCMDevice(
            name='test-device', registered_id='registered_id',
            account=self.account)

        self.assertEqual(
            self.registered_fcm.__str__(),
            "{} | {}".format(
                self.registered_fcm.name, self.registered_fcm.registered_id
            )
        )

    def test_clean_up_unregistered_id(self):
        bad_registered_id = "eJJy0K_mWd4:APA91bFFhO6wbjhGjgGQi9CxJEbSU8dX4V" \
                            "HeeMEpznJf-AjU0G15hbs8_NGtQTtRDR1X6yS5e5XEwbLSEFN1cZyuT-" \
                            "GiqxdnQpeuFFve7eq2G0IbbL9PF_BrHuVdGiUdCjrEIFT8cve3"
        mommy.make(
            RegisteredFCMDevice,
            account=self.account, _fill_optional=['registered_id'], _quantity=500)
        mommy.make(
            RegisteredFCMDevice,
            account=self.account, registered_id=bad_registered_id)
        RegisteredFCMDevice.clean_up_unregistered_id()
        self.assertEqual(
            RegisteredFCMDevice.objects.count(),
            0
        )
