from test_plus.test import TestCase
from model_mommy import mommy


from sterlingpos.push_notification.models import RegisteredFCMDevice
from sterlingpos.push_notification.utils import PushNotificationBasicHandler, push_notification_adapter


class TestPushNotificationBasicHandler(TestCase):

    def setUp(self):
        account = mommy.make("accounts.Account", name="account-1")

        self.registered_fcm = RegisteredFCMDevice.objects.create(
            name="test-device", registered_id="registered_id", account=account
        )
        self.product = mommy.make(
            "catalogue.Product",
            sku="PRODUCT-TEST",
            cost=500,
            price=1000,
            account=account,
        )

    def test_get_registration_ids(self):
        handler = PushNotificationBasicHandler(instance=self.product)
        registered_ids = handler.get_registration_ids()
        self.assertTrue(
            registered_ids.filter(pk=self.registered_fcm.pk).exists()
        )

    def test_get_fcm_queryset(self):
        handler = PushNotificationBasicHandler(instance=self.product)
        registered_ids = handler.get_fcm_queryset()
        self.assertTrue(
            registered_ids.filter(pk=self.registered_fcm.pk).exists()
        )

    def test_get_data_message(self):
        handler = PushNotificationBasicHandler(instance=self.product)
        data_message = handler.get_data_message("UPDATE")

        self.assertEqual(data_message["mode"], "UPDATE_PRODUCT")
        self.assertEqual(data_message["title"], "PRODUCT UPDATE")
        self.assertEqual(data_message["message"], "Please sync your data.....")
        self.assertEqual(data_message["object_id"], self.product.pk)

    def test_get_mode_name(self):
        handler = PushNotificationBasicHandler(instance=self.product)

        mode_name = handler.get_mode_name("UPDATE")
        self.assertEqual(mode_name, "UPDATE_PRODUCT")

        mode_name = handler.get_mode_name("DELETE")
        self.assertEqual(mode_name, "DELETE_PRODUCT")

    def test_get_data_message_with_bad_mode(self):
        handler = PushNotificationBasicHandler(instance=self.product)

        with self.assertRaises(ValueError):
            handler.get_data_message("BAD_MODE")
