from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.push_notification.signals import _build_push_notification_message
from sterlingpos.push_notification.models import RegisteredFCMDevice
from sterlingpos.catalogue.models import Product
from sterlingpos.core.models import set_current_tenant


class TestPreparePushNotificationMessage(TestCase):
    def setUp(self):
        account = mommy.make("accounts.Account", name="account-1")

        self.registered_fcm = RegisteredFCMDevice.objects.create(
            name="test-device", registered_id="registered_id", account=account
        )
        self.product = mommy.make(
            "catalogue.Product",
            sku="PRODUCT-TEST",
            cost=500,
            price=1000,
            account=account,
        )

    def test_update_signal(self):
        registered_ids, data_message = _build_push_notification_message(
            self.product, "Updated"
        )

        self.assertIn("registered_id", registered_ids)
        self.assertEqual(data_message["mode"], "UPDATE_PRODUCT_STANDART")
        self.assertEqual(data_message["title"], "Product Updated")
        self.assertEqual(data_message["message"], "Please sync your data.....")

    def test_delete_signal(self):
        registered_ids, data_message = _build_push_notification_message(
            self.product, "Deleted"
        )

        self.assertIn("registered_id", registered_ids)
        self.assertEqual(data_message["mode"], "DELETE_PRODUCT_STANDART")
        self.assertEqual(data_message["title"], "Product Deleted")
        self.assertEqual(data_message["message"], "Please sync your data.....")


class TestPreparePushNotificationMessageStock(TestCase):
    def setUp(self):
        account = mommy.make("accounts.Account", name="account-1")
        self.unit = account.uom_set.first()
        self.registered_fcm = RegisteredFCMDevice.objects.create(
            name="test-device", registered_id="registered_id", account=account
        )
        self.product = mommy.make(
            "catalogue.Product",
            sku="PRODUCT-TEST",
            cost=500,
            price=1000,
            uom=self.unit,
            account=account,
        )
        self.outlet = mommy.make("outlet.Outlet", account=account)
        self.stock = mommy.make(
            "stocks.Stock", product=self.product, outlet=self.outlet, account=account
        )

    def test_update_signal(self):
        self.stock.add_balance(10, self.unit)
        registered_ids, data_message = _build_push_notification_message(
            self.stock.transactions.first(), "Updated"
        )

        self.assertIn("registered_id", registered_ids)
        self.assertEqual(data_message["mode"], "UPDATE_STOCK")
        self.assertEqual(data_message["title"], "Stock Updated")
        self.assertEqual(data_message["message"], "Please sync your data.....")


class TestAutogenerateProductionPushNotificationMessage(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.unit = self.account.uom_set.first()
        self.registered_fcm = RegisteredFCMDevice.objects.create(
            name="test-device", registered_id="registered_id", account=self.account
        )
        self.outlet = mommy.make("outlet.Outlet", account=self.account)

        self.manufactured_product = mommy.make(
            "catalogue.Product",
            name="Product-Manufacture",
            sku="SKU-1234",
            price=5000,
            is_manage_stock=True,
            account=self.account,
            uom=self.unit,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
        )
        self.manufactured_stock = self.manufactured_product.stocks.get(
            outlet=self.outlet
        )
        self.product_1 = mommy.make(
            "catalogue.Product",
            name="Product-1",
            sku="SKU-1",
            price=10000,
            is_manage_stock=True,
            account=self.account,
            uom=self.unit,
        )
        self.stock_1 = self.product_1.stocks.get(outlet=self.outlet)

        self.product_2 = mommy.make(
            "catalogue.Product",
            name="Product-2",
            sku="SKU-2",
            price=5000,
            is_manage_stock=True,
            account=self.account,
            uom=self.unit,
        )
        self.stock_2 = self.product_2.stocks.get(outlet=self.outlet)
        self.bom = mommy.make(
            "productions.BOM",
            yield_quantity=1,
            account=self.account,
        )
        self.bom.products.add(
            self.manufactured_product, through_defaults={"account": self.account}
        )

        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_1,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
    
    def tearDown(self):
        set_current_tenant(None)

    def test_update_signals(self):
        sales_order = mommy.make(
            "sales.SalesOrder",
            status="settled",
            outlet=self.outlet,
            account=self.account,
        )
        product_order = mommy.make(
            "sales.ProductOrder",
            account=self.account,
            sales_order=sales_order,
            product=self.manufactured_product,
            quantity=2,
        )

        assert self.manufactured_stock.balance == 0
