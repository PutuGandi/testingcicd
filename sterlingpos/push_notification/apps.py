from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_save, post_delete, m2m_changed
from safedelete.signals import post_softdelete


class PushNotificationConfig(AppConfig):
    name = "sterlingpos.push_notification"
    verbose_name = "PushNotifications"

    def ready(self):
        from sterlingpos.catalogue.models import (
            Product,
            Category,
            ProductModifier,
            CompositeProduct,
            ProductPricing,
            ProductAvailability,
        )
        from sterlingpos.device.models import DeviceUser
        from sterlingpos.promo.models import Promo
        from sterlingpos.receipt.models import Receipt
        from sterlingpos.sales.models import (
            EnabledPaymentMethod,
            SalesReportSetting,
            SalesOrderOption,
            CashierActivityReason
        )
        from sterlingpos.waiters.models import WaiterSettings, Waiter
        from sterlingpos.outlet.models import Outlet, OutletOptions
        from sterlingpos.kitchen_display.models import KitchenDisplaySetting
        from sterlingpos.cash_balance.models import CashBalanceSetting
        from sterlingpos.table_management.models import (
            TableManagementSettings,
            TableArea,
            Table,
            ColorStatus,
        )
        from sterlingpos.table_order.models import TableOrderSettings
        from sterlingpos.surcharges.models import Surcharge
        from sterlingpos.customer.models import (
            Customer,
            CustomerGroup,
            CustomerOrganization,
        )
        from sterlingpos.digital_payment.models import DigitalPaymentSettings
        from sterlingpos.stocks.models import StockTransaction
        from .signals import (
            push_update_signal,
            push_delete_signal,
            push_update_signal_m2m,
            push_resync_signal,
            resource_import,
        )
        

        if not settings.DEBUG or getattr(settings, "ENABLE_PUSH_NOTIF", False):
            post_save.connect(push_update_signal, sender=Product)
            post_softdelete.connect(push_delete_signal, sender=Product)

            m2m_changed.connect(push_update_signal_m2m, sender=ProductModifier.products.through)

            # post_save.connect(push_update_signal, sender=ProductPricing)
            # post_softdelete.connect(push_delete_signal, sender=ProductPricing)
            # post_save.connect(push_update_signal, sender=ProductAvailability)
            # post_softdelete.connect(push_delete_signal, sender=ProductAvailability)

            post_save.connect(push_update_signal, sender=Category)
            post_softdelete.connect(push_delete_signal, sender=Category)

            post_save.connect(push_update_signal, sender=ProductModifier)
            post_softdelete.connect(push_delete_signal, sender=ProductModifier)

            post_save.connect(push_update_signal, sender=CompositeProduct)
            post_softdelete.connect(push_delete_signal, sender=CompositeProduct)

            post_save.connect(push_update_signal, sender=DeviceUser)
            post_softdelete.connect(push_delete_signal, sender=DeviceUser)

            post_save.connect(push_update_signal, sender=Promo)
            post_softdelete.connect(push_delete_signal, sender=Promo)

            post_save.connect(push_update_signal, sender=Receipt)

            post_save.connect(push_update_signal, sender=EnabledPaymentMethod)

            post_save.connect(push_update_signal, sender=TableOrderSettings)

            post_save.connect(push_update_signal, sender=TableManagementSettings)
            post_save.connect(push_update_signal, sender=TableArea)
            post_softdelete.connect(push_delete_signal, sender=TableArea)
            post_save.connect(push_update_signal, sender=Table)
            post_softdelete.connect(push_delete_signal, sender=Table)

            post_save.connect(push_update_signal, sender=WaiterSettings)

            post_save.connect(push_update_signal, sender=Waiter)
            post_softdelete.connect(push_delete_signal, sender=Waiter)

            post_save.connect(push_update_signal, sender=Outlet)
            post_save.connect(push_update_signal, sender=OutletOptions)

            post_save.connect(push_update_signal, sender=SalesReportSetting)

            post_save.connect(push_update_signal, sender=SalesOrderOption)
            post_softdelete.connect(push_delete_signal, sender=SalesOrderOption)
            post_save.connect(push_update_signal, sender="sales.TransactionTypeReason")
            post_softdelete.connect(push_delete_signal, sender="sales.TransactionTypeReason")

            post_save.connect(push_update_signal, sender=CashierActivityReason)
            post_delete.connect(push_delete_signal, sender=CashierActivityReason)

            post_save.connect(push_update_signal, sender=KitchenDisplaySetting)
            post_save.connect(push_update_signal, sender=CashBalanceSetting)

            post_save.connect(push_update_signal, sender=ColorStatus)
            post_softdelete.connect(push_delete_signal, sender=ColorStatus)

            post_save.connect(push_update_signal, sender=Surcharge)
            post_softdelete.connect(push_delete_signal, sender=Surcharge)

            post_save.connect(push_update_signal, sender="role.RoleSettings")
            post_save.connect(push_update_signal, sender="role.Role")
            post_softdelete.connect(push_delete_signal, sender="role.Role")

            post_save.connect(push_update_signal, sender=Customer)
            post_softdelete.connect(push_delete_signal, sender=Customer)

            post_save.connect(push_update_signal, sender=CustomerGroup)
            post_softdelete.connect(push_delete_signal, sender=CustomerGroup)

            post_save.connect(push_update_signal, sender=CustomerOrganization)
            post_softdelete.connect(push_delete_signal, sender=CustomerOrganization)

            post_save.connect(push_update_signal, sender=DigitalPaymentSettings)

            post_save.connect(push_update_signal, sender=StockTransaction)
            
            resource_import.connect(push_resync_signal, sender=Product)
            resource_import.connect(push_resync_signal, sender=Category)
            resource_import.connect(push_resync_signal, sender=Customer)
            resource_import.connect(push_resync_signal, sender=CustomerGroup)
            resource_import.connect(push_resync_signal, sender=CustomerOrganization)
