import logging
import django.dispatch

from sterlingpos.push_notification.tasks import delayed_build_push_notif
from sterlingpos.push_notification.utils import get_handler_for_instance

from sterlingpos.catalogue.models import Product, ProductPricing, ProductAvailability
from sterlingpos.stocks.models import TRANSACTION_TYPE, StockTransaction

logger = logging.getLogger(__name__)

resource_import = django.dispatch.Signal(
    providing_args=["instance",])


def _build_mode_name(instance, status_code):
    if status_code == "Updated":
        status = "UPDATE"
    else:
        status = "DELETE"

    mode_name = "{}_{}".format(status, instance.__class__._meta.verbose_name.upper())

    if isinstance(instance, Product):
        mode_name = "{}_{}".format(mode_name, instance.classification.upper())

    if isinstance(instance, ProductPricing):
        mode_name = "{}_{}".format(mode_name, instance.product.classification.upper())

    if isinstance(instance, ProductAvailability):
        mode_name = "{}_{}".format(mode_name, instance.product.classification.upper())

    if isinstance(instance, StockTransaction):
        mode_name = "{}_{}".format(status, "STOCK")

    return mode_name


def _build_data_message(instance, status_code):
    mode_name = _build_mode_name(instance, status_code)
    data_message = {
        "mode": mode_name,
        "title": "{} {}".format(instance.__class__._meta.verbose_name, status_code),
        "message": "Please sync your data.....",
        "object_id": instance.pk,
    }

    if isinstance(instance, StockTransaction) and instance.transaction_type not in [
        TRANSACTION_TYPE.transaction
    ]:
        data_message["title"] = "Stock Updated"
        data_message["object_id"] = instance.stock.product.pk
        data_message["outlet_id"] = instance.stock.outlet.id
        data_message["transaction_type"] = instance.transaction_type
    
    return data_message


def _build_push_notification_message(instance, status_code):
    fcm_devices = instance.account.registeredfcmdevice_set.all()
    registration_ids = fcm_devices.values_list("registered_id", flat=True)
    data_message = _build_data_message(instance, status_code)

    logger.info(f"Push Notification data={data_message}")
    
    return registration_ids, data_message


def push_update_signal(sender, **kwargs):
    instance = kwargs.get("instance")

    handler = get_handler_for_instance(instance)

    if handler.is_valid:
        registration_ids = handler.get_registration_ids()
        data_message = handler.get_data_message("UPDATE")

        result = delayed_build_push_notif.delay(
            list(registration_ids), data_message)

def push_delete_signal(sender, **kwargs):
    instance = kwargs.get("instance")

    handler = get_handler_for_instance(instance)

    if handler.is_valid:
        registration_ids = handler.get_registration_ids()
        data_message = handler.get_data_message("DELETE")

        result = delayed_build_push_notif.apply_async(
            count_down=10,
            kwargs={
                "registration_ids": list(registration_ids),
                "data_message": data_message,
            }
        )

def push_update_signal_m2m(sender, **kwargs):
    action = kwargs.pop('action', None)
    instance = kwargs.get("instance")

    if action == "post_add" or action == "post_clear":
        handler = get_handler_for_instance(instance)

        if handler.is_valid:
            registration_ids = handler.get_registration_ids()
            data_message = handler.get_data_message("UPDATE")

            result = delayed_build_push_notif.apply_async(
                count_down=10,
                kwargs={
                    "registration_ids": list(registration_ids),
                    "data_message": data_message,
                }
            )

def push_resync_signal(sender, **kwargs):
    instance = kwargs.get("instance")

    handler = get_handler_for_instance(instance)

    if handler.is_valid:
        registration_ids = handler.get_registration_ids()
        data_message = handler.get_data_message("RESYNC")
        data_message.pop("object_id")

        result = delayed_build_push_notif.apply_async(
            count_down=10,
            kwargs={
                "registration_ids": list(registration_ids),
                "data_message": data_message,
            }
        )
