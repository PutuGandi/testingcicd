from .utils import PushService
from celery import shared_task


@shared_task(bind=True, max_retries=5)
def delayed_build_push_notif(self, registration_ids, data_message):
    try:
        result = PushService().multiple_devices_data_message(
            registration_ids=registration_ids, data_message=data_message)
        return result
    except:
        self.retry(countdown= 2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def cleaning_up_invalid_fcm(self):
    from .models import RegisteredFCMDevice
    try:
        RegisteredFCMDevice.clean_up_unregistered_id()
    except:
        self.retry(countdown= 2 ** self.request.retries)
