# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from model_utils import Choices

from sterlingpos.push_notification.utils import PushService
from sterlingpos.core.models import (
    SterlingTenantModel,
    SterlingTenantForeignKey,
)


@python_2_unicode_compatible
class RegisteredFCMDevice(SterlingTenantModel, TimeStampedModel):
    DEVICE_TYPE = Choices(
        ('cashier', _('Cashier App')),
        ('waiter', _('Waiter App')),
    )

    device_type = models.CharField(
        _("Device Type"), max_length=15,
        choices=DEVICE_TYPE, default=DEVICE_TYPE.cashier, db_index=True)

    registered_id = models.CharField(_("Username"), max_length=255)
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    name = models.CharField(_("Name"), max_length=255)

    last_user_login = SterlingTenantForeignKey(
        'device.DeviceUser', blank=True, null=True, on_delete=models.SET_NULL)
    last_login = models.DateTimeField(blank=True, null=True)

    outlet = SterlingTenantForeignKey(
        'outlet.Outlet', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "RegisteredFCMDevice"
        verbose_name_plural = "RegisteredFCMDevices"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "{} | {}".format(self.name, self.registered_id)

    @classmethod
    def clean_up_unregistered_id(cls):
        registration_ids = cls.objects.values_list(
            'registered_id', flat=True)
        valid_ids = PushService().clean_registration_ids(registration_ids)
        cls.objects.exclude(registered_id__in=valid_ids).delete()
