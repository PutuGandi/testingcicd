from django.contrib import admin

from .models import (
    RegisteredFCMDevice,
)


class RegisteredFCMDeviceAdmin(admin.ModelAdmin):

    list_display = (
        "device_type", "name", "last_user_login",
        "last_login", "account")
    list_filter = ("device_type", )
    search_fields = ("name", "device_type", "account__name")

admin.site.register(RegisteredFCMDevice, RegisteredFCMDeviceAdmin)
