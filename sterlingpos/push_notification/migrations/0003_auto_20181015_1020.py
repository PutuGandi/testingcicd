# Generated by Django 2.0.4 on 2018-10-15 10:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20180926_0352'),
        ('push_notification', '0002_auto_20181011_0751'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='registeredfcmdevice',
            unique_together={('account', 'id')},
        ),
    ]
