import logging

from django.db.models.signals import post_save

from celery import shared_task
from datetime import datetime
from decimal import Decimal

from sterlingpos.core.models import get_current_tenant, set_current_tenant
from sterlingpos.accounts.models import Account
from sterlingpos.productions.models import ProductionsOrder
from sterlingpos.stocks.models import StockTransaction
from sterlingpos.push_notification.signals import push_update_signal
from sterlingpos.sales.models import ProductOrder, ProductOrderCompositeDetail, ProductOrderProductAddOnDetail
from sterlingpos.outlet.models import Outlet

logger = logging.getLogger(__name__)


def process_production(production):
    post_save.disconnect(push_update_signal, sender=StockTransaction)
    production.started = datetime.now()
    production.status = ProductionsOrder.STATUS.in_production
    production.save()
    production.start_productions(user=None)

    production.finished = datetime.now()
    production.status = ProductionsOrder.STATUS.completed
    production.save()
    production.complete_productions(user=None)
    post_save.connect(push_update_signal, sender=StockTransaction)


def create_production_order(instance, outlet, quantity):
    bom = instance.product.bom.filter(deleted__isnull=True).first()
    if bom:
        data = {
            "bom": bom,
            "product": instance.product,
            "produced_quantity": abs(Decimal(quantity)),
            "outlet": outlet,
            "code": "PROD-%s" % (ProductionsOrder.all_objects.count() + 1),
            "automatic_production": True,
        }
        production = ProductionsOrder.objects.create(**data)
        for item in bom.bom_item.all():
            data_prod = {
                "product": item.product,
                "quantity": (production.produced_quantity / bom.yield_quantity)
                * item.quantity,
                "quantity_unit": item.quantity_unit,
                "wastage_quantity": item.wastage,
                "wastage_unit": item.wastage_unit,
                "unconsumed_unit": item.product.uom,
            }
            production.productions_item.create(**data_prod)

        for item in bom.bom_other_cost.all():
            data_other = {
                "name": item.name,
                "quantity": item.quantity,
                "cost_per_item": item.cost_per_item,
            }
            production.productions_other_cost.create(**data_other)
        process_production(production)


@shared_task(bind=True, max_retries=5)
def product_order_proses_production(
    self, product_order_pk, outlet_pk, quantity, account_pk, sales_order_code
):
    try:

        try:
            account = Account.objects.get(pk=account_pk)
            set_current_tenant(account)
            instance = ProductOrder.objects.get(pk=product_order_pk)
            outlet = Outlet.objects.get(pk=outlet_pk)
            create_production_order(instance, outlet, quantity)
            set_current_tenant(None)
        except ProductOrder.DoesNotExist:
            logged_data = "ProductOrder does not exist : (pk: {}, sales_code: {}, account: {})".format(
                product_order_pk, sales_order_code, get_current_tenant()
            )
            logger.warning(logged_data)
        except Outlet.DoesNotExist:
            logged_data = "Outlet does not exist : (pk: {}, account: {})".format(
                outlet_pk, get_current_tenant()
            )
            logger.warning(logged_data)

    except Exception as e:
        logged_data = "Unable to process production {} : {}".format(
            get_current_tenant(), e
        )
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def product_order_addon_process_production(
    self,
    product_addon_order_pk,
    outlet_pk,
    quantity,
    account_pk,
):
    try:
        account = Account.objects.get(pk=account_pk)
        set_current_tenant(account)
        outlet = Outlet.objects.get(pk=outlet_pk)
        instance = ProductOrderProductAddOnDetail.objects.get(pk=product_addon_order_pk)
        create_production_order(instance, outlet, quantity)
        set_current_tenant(None)
    except Exception as e:
        logged_data = "Unable to process production {} : {}".format(
            get_current_tenant(), e
        )
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


@shared_task(bind=True, max_retries=5)
def product_composite_proses_production(
    self, product_order_composite_pk, outlet_pk, quantity, account_pk, sales_order_code
):
    try:

        try:
            account = Account.objects.get(pk=account_pk)
            set_current_tenant(account)
            instance = ProductOrderCompositeDetail.objects.get(
                pk=product_order_composite_pk
            )
            outlet = Outlet.objects.get(pk=outlet_pk)
            create_production_order(instance, outlet, quantity)
            set_current_tenant(None)
        except ProductOrderCompositeDetail.DoesNotExist:
            logged_data = "ProductOrderCompositeDetail does not exist : (pk: {}, sales_code: {}, account: {})".format(
                product_order_composite_pk, sales_order_code, get_current_tenant()
            )
            logger.warning(logged_data)
        except Outlet.DoesNotExist:
            logged_data = "Outlet does not exist : (pk: {}, account: {})".format(
                outlet_pk, get_current_tenant()
            )
            logger.warning(logged_data)

    except Exception as e:
        logged_data = "Unable to process production {} : {}".format(
            get_current_tenant(), e
        )
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)
