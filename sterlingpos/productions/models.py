from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.db.models.signals import post_save

from model_utils.models import TimeStampedModel
from model_utils import Choices
from djmoney.models.fields import MoneyField

from sterlingpos.core.models import SterlingTenantModel, SterlingTenantForeignKey
from safedelete.models import HARD_DELETE
from sterlingpos.catalogue.utils import get_normalized_decimal
from sterlingpos.catalogue.utils import uom_conversion, uom_cost_conversion
from sterlingpos.stocks.models import (
    TRANSACTION_TYPE,
    Batch,
    BatchItem,
    StockTransaction,
)
from sterlingpos.push_notification.signals import push_update_signal
from sterlingpos.notification.models import NotificationSetting
from sterlingpos.notification.tasks import notify_stocks_email

from sterlingpos.productions.managers import BOMItemDetailTenantManager
from sterlingpos.catalogue.tasks import calculate_produced_average_cost


class ProductBOM(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE
    product = SterlingTenantForeignKey("catalogue.Product", on_delete=models.CASCADE)
    bom = SterlingTenantForeignKey("productions.BOM", on_delete=models.CASCADE)


class BOM(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ("inactive", "inactive", _("Inactive")), ("active", "active", _("Active"))
    )
    status = models.CharField(
        _("Status BOM"), max_length=15, choices=STATUS, default=STATUS.active
    )
    code = models.CharField(_("BOM Code"), max_length=120, db_index=True)
    yield_quantity = models.DecimalField(
        _("Yield Quantity"), max_digits=10, decimal_places=3
    )
    products = models.ManyToManyField(
        "catalogue.Product",
        through="ProductBOM",
        related_name="bom",
    )

    class Meta:
        verbose_name = "BOM"
        verbose_name_plural = "BOMs"
        unique_together = (("account", "id"),)

    def __str__(self):
        return "{} [{}]".format(self.code, self.status)

    def create_bom_item(self, product, quantity, quantity_unit, wastage, wastage_unit):
        return self.bom_item.create(
            product=product,
            quantity=quantity,
            quantity_unit=quantity_unit,
            wastage=wastage,
            wastage_unit=wastage_unit,
        )

    def create_other_cost(self, name, quantity, cost):
        return self.bom_other_cost.create(
            name=name, quantity=quantity, cost_per_item=cost
        )


class BOMItemDetail(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    bom = SterlingTenantForeignKey(
        "productions.BOM",
        related_name="bom_item",
        on_delete=models.CASCADE,
    )
    product = SterlingTenantForeignKey(
        "catalogue.Product",
        related_name="bom_item",
        on_delete=models.CASCADE,
    )

    quantity = models.DecimalField(
        _("Quantity"), max_digits=10, decimal_places=3, default=1.0
    )
    wastage = models.DecimalField(
        _("Wastage"), max_digits=10, decimal_places=3, default=0.0
    )

    quantity_unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="bom_unit_item",
        on_delete=models.CASCADE,
    )

    wastage_unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="bom_wastage_item",
        on_delete=models.CASCADE,
    )

    objects = BOMItemDetailTenantManager()

    class Meta:
        verbose_name = "BOMItemDetail"
        verbose_name_plural = "BOMItemDetails"
        unique_together = (("account", "id"),)

    @property
    def total_cost(self):
        total = self.quantity * self.product.cost.amount
        return get_normalized_decimal(total)


class BOMOtherCostDetail(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    bom = SterlingTenantForeignKey(
        "productions.BOM",
        related_name="bom_other_cost",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=100, default="")
    quantity = models.IntegerField(default=0)
    cost_per_item = MoneyField(
        _("Cost Per Item"), max_digits=10, decimal_places=2, default_currency="IDR"
    )

    class Meta:
        verbose_name = "BOMOtherCostDetail"
        verbose_name_plural = "BOMOtherCostDetails"
        unique_together = (("account", "id"),)

    @property
    def total_cost(self):
        total = self.quantity * self.cost_per_item.amount
        return get_normalized_decimal(total)


class ProductionsOrder(SterlingTenantModel, TimeStampedModel):
    STATUS = Choices(
        ("cancelled", "cancelled", _("Cancelled")),
        ("completed", "completed", _("Completed")),
        ("in_production", "in_production", _("In Production")),
        ("finalized", "finalized", _("Finalized")),
    )
    status = models.CharField(
        _("Status Production"), max_length=15, choices=STATUS, default=STATUS.finalized
    )
    code = models.CharField(_("Production Code"), max_length=120, db_index=True)
    produced_quantity = models.DecimalField(
        _("Produced Quantity"),
        max_digits=10,
        decimal_places=3,
        default=0.0,
    )
    started = models.DateTimeField(blank=True, null=True)
    finished = models.DateTimeField(blank=True, null=True)

    notes = models.TextField(_("Notes"), default="")
    product = SterlingTenantForeignKey(
        "catalogue.Product",
        null=True,
        related_name="production_order",
        on_delete=models.CASCADE,
    )
    bom = SterlingTenantForeignKey(
        "productions.BOM", related_name="production_order", on_delete=models.CASCADE
    )
    product = SterlingTenantForeignKey(
        "catalogue.Product",
        related_name="production_order",
        on_delete=models.CASCADE,
    )
    outlet = SterlingTenantForeignKey(
        "outlet.Outlet", related_name="production_order", on_delete=models.CASCADE
    )
    automatic_production = models.BooleanField(default=False)

    class Meta:
        verbose_name = "ProductionOrder"
        verbose_name_plural = "ProductionOrders"
        unique_together = (("account", "id"),)

    def __str__(self):
        string = "{} ({})".format(self.code, self.status)
        return string

    def start_productions(self, user):
        post_save.disconnect(push_update_signal, sender=StockTransaction)
        if self.status == self.STATUS.in_production:
            for item in self.productions_item.filter(product__archived=False):
                item.start_production_item(user)
            post_save.connect(push_update_signal, sender=StockTransaction)
            return self

    def complete_productions(self, user):
        post_save.disconnect(push_update_signal, sender=StockTransaction)
        if self.status == self.STATUS.completed:
            for item in self.productions_item.filter(product__archived=False):
                item.process_production_item(user)
            post_save.connect(push_update_signal, sender=StockTransaction)
            self.calculate_avarage_cost()
            self.production_add_balance(user)
            return self

    def production_add_balance(self, user):
        stock = self.product.stocks.get(outlet=self.outlet)
        stock.add_balance(
            self.produced_quantity,
            unit=self.product.uom,
            source_object=self,
            transaction_type=TRANSACTION_TYPE.productions,
            user=user,
        )

    def calculate_avarage_cost(self):
        components_cost = 0
        for component in self.productions_item.all():
            converted_qty = uom_conversion(
                component.quantity_unit, component.product.uom, component.quantity
            )
            components_cost += converted_qty * component.product.cost.amount

        others_cost = 0
        for other in self.productions_other_cost.all():
            others_cost += other.cost_per_item.amount
        calculate_produced_average_cost.apply_async(
            count_down=5,
            kwargs={
                "product_id": self.product.pk,
                "components_cost": str(components_cost),
                "others_cost": str(others_cost),
                "produced_quantity": str(self.produced_quantity),
                "account_id": self.account.pk,
            }
        )

    def generate_code(self):
        return "PROD-{}".format(self.__class__.all_objects.count() + 1)

    def create_productions_other_cost(self, name, quantity, cost):
        self.productions_other_cost.create(
            name=name, quantity=quantity, cost_per_item=cost
        )


class ProductionOrderItemDetail(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    productions = SterlingTenantForeignKey(
        "productions.ProductionsOrder",
        related_name="productions_item",
        on_delete=models.CASCADE,
    )
    product = SterlingTenantForeignKey(
        "catalogue.Product", related_name="productions_item", on_delete=models.CASCADE
    )
    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
        default=0.0,
    )
    wastage_quantity = models.DecimalField(
        _("Wastage Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
        default=0.0,
    )
    unconsumed_quantity = models.DecimalField(
        _("Unconsumed Quantity"),
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=3,
        default=0.0,
    )

    quantity_unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="productions_unit_item",
        on_delete=models.CASCADE,
    )
    wastage_unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="productions_wastage_item",
        on_delete=models.CASCADE,
    )
    unconsumed_unit = SterlingTenantForeignKey(
        "catalogue.UOM",
        related_name="productions_unconsumed_item",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "ProductionOrderItemDetail"
        verbose_name_plural = "ProductionOrderItemDetails"
        unique_together = (("account", "id"),)

    def batch_calculation(self, converted_qty):
        batches = Batch.all_objects.filter(product=self.product).order_by(
            "expiration_date"
        )
        quantity = converted_qty
        if batches:
            for batch in batches:
                try:
                    batch_outlet = batch.batch_items.get(outlet=self.productions.outlet)
                    if quantity == 0:
                        break

                    quantity_temp = batch_outlet.balance - quantity
                    qty = batch_outlet.balance - quantity_temp
                    if quantity_temp < 0 and batch_outlet.balance > 0:
                        qty = quantity - abs(quantity_temp)
                    quantity -= qty
                    batch_outlet.substract_batch_balance(
                        qty,
                        unit=self.product.uom,
                        transaction_type=TRANSACTION_TYPE.productions,
                        source_object=self.productions,
                    )
                    self.batch_used.create(batch=batch, quantity_used=qty)
                except BatchItem.DoesNotExist:
                    batch_outlet = batch.batch_items.create(
                        outlet=self.productions.outlet
                    )
                    batch_outlet.substract_batch_balance(
                        quantity,
                        unit=self.product.uom,
                        transaction_type=TRANSACTION_TYPE.productions,
                        source_object=self.productions,
                    )
        else:
            missing_name = "Missing Batch"
            missing_batch = Batch.objects.create(
                product=self.product, is_missing_batch=True, batch_id=missing_name
            )
            batch_outlet = missing_batch.batch_items.create(
                outlet=self.productions.outlet
            )
            batch_outlet.substract_batch_balance(
                quantity,
                unit=self.product.uom,
                transaction_type=TRANSACTION_TYPE.productions,
                source_object=self.productions,
            )

    def start_production_item(self, user):
        enable_notify_stocks = (
            self.account.notificationsetting_set.filter(
                notification_type=NotificationSetting.NOTIFICATION_TYPES.low_stock_notification
            )
            .first()
            .is_enabled
        )

        converted_qty = uom_conversion(
            self.quantity_unit, self.product.uom, self.quantity
        )
        stock = self.product.stocks.get(outlet=self.productions.outlet)
        if self.productions.status == ProductionsOrder.STATUS.in_production:
            stock.subtract_balance(
                converted_qty,
                unit=self.product.uom,
                source_object=self.productions,
                transaction_type=TRANSACTION_TYPE.productions,
                user=user,
            )
            if stock.balance <= stock.product.min_stock_alert and enable_notify_stocks:
                recipients = NotificationSetting.get_email_recipients_for_type(
                    NotificationSetting.NOTIFICATION_TYPES.low_stock_notification,
                    self.account,
                )

                if recipients:
                    notify_stocks_email.delay(
                        stock.product.__str__(),
                        stock.balance,
                        "{}, {}, {}".format(
                            stock.outlet.name,
                            stock.outlet.city,
                            stock.outlet.province,
                        ),
                        recipients,
                    )

            if self.product.is_batch_tracked:
                self.batch_calculation(converted_qty)
        return self

    def proces_unconsumed_batch(self, converted_unconsumed):
        batches_used = self.batch_used.all().order_by("-batch__expiration_date")
        quantity = converted_unconsumed
        for batch_used in batches_used:
            try:
                batch_outlet = batch_used.batch.batch_items.get(
                    outlet=self.productions.outlet
                )
                if quantity == 0:
                    break

                if quantity < batch_used.quantity_used:
                    batch_outlet.add_batch_balance(
                        quantity,
                        unit=self.product.uom,
                        transaction_type=TRANSACTION_TYPE.productions,
                        source_object=self.productions,
                    )
                    quantity -= quantity
                else:
                    quantity -= batch_used.quantity_used
                    batch_outlet.add_batch_balance(
                        batch_used.quantity_used,
                        unit=self.product.uom,
                        transaction_type=TRANSACTION_TYPE.productions,
                        source_object=self.productions,
                    )
            except BatchItem.DoesNotExist:
                batch_outlet = batch_used.batch_items.create(
                    outlet=self.productions.outlet
                )
                batch_outlet.add_batch_balance(
                    batch_used.quantity_used,
                    unit=self.product.uom,
                    transaction_type=TRANSACTION_TYPE.productions,
                    source_object=self.productions,
                )

    def process_production_item(self, user):
        converted_unconsumed = uom_conversion(
            self.unconsumed_unit, self.product.uom, self.unconsumed_quantity
        )
        stock = self.product.stocks.get(outlet=self.productions.outlet)
        if self.productions.status == ProductionsOrder.STATUS.completed:
            if converted_unconsumed > 0:
                stock.add_balance(
                    converted_unconsumed,
                    unit=self.product.uom,
                    source_object=self.productions,
                    transaction_type=TRANSACTION_TYPE.productions,
                    user=user,
                )
                self.proces_unconsumed_batch(converted_unconsumed)
        return self

    @property
    def converted_cost(self):
        converted_cost = uom_cost_conversion(
            self.product.uom,
            self.quantity_unit,
            1,
            self.product.cost.amount,
        )
        return converted_cost

    @property
    def total_cost(self):
        converted_cost = uom_cost_conversion(
            self.product.uom,
            self.quantity_unit,
            1,
            self.product.cost.amount,
        )
        converted_unconsumed = uom_conversion(
            self.unconsumed_unit, self.quantity_unit, self.unconsumed_quantity
        )
        total = (self.quantity - converted_unconsumed) * converted_cost
        return get_normalized_decimal(total)


class ProductionOrderItemBatchUsed(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    productions_component = SterlingTenantForeignKey(
        "productions.ProductionOrderItemDetail",
        related_name="batch_used",
        on_delete=models.CASCADE,
    )
    batch = SterlingTenantForeignKey(
        "stocks.Batch",
        related_name="batch_productions_components",
        on_delete=models.CASCADE,
    )
    quantity_used = models.DecimalField(
        _("Quantity Used"),
        max_digits=10,
        decimal_places=3,
        default=0.0,
    )

    class Meta:
        verbose_name = "ProductionOrderItemBatchUsed"
        verbose_name_plural = "ProductionOrderItemBatchUseds"
        unique_together = (("account", "id"),)


class ProductionsOrderOtherCostDetail(SterlingTenantModel, TimeStampedModel):
    _safedelete_policy = HARD_DELETE

    productions = SterlingTenantForeignKey(
        "productions.ProductionsOrder",
        related_name="productions_other_cost",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=100, default="")
    quantity = models.IntegerField(default=0)
    cost_per_item = MoneyField(
        _("Cost Per Item"), max_digits=10, decimal_places=2, default_currency="IDR"
    )

    class Meta:
        verbose_name = "ProductionOrderOtherCostDetail"
        verbose_name_plural = "ProductionOrderOtherCostDetails"
        unique_together = (("account", "id"),)

    @property
    def total_cost(self):
        total = self.quantity * self.cost_per_item.amount
        return get_normalized_decimal(total)
