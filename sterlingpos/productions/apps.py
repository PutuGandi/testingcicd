from django.apps import AppConfig


class ProductionsConfig(AppConfig):
    name = 'sterlingpos.productions'
    verbose_name = 'Productions'

    def ready(self):
        try:
            import productions.signals
        except ImportError:
            pass
