import logging

from django.core.paginator import Paginator
from celery import shared_task

from sterlingpos.accounts.models import Account
from sterlingpos.sales.models import SalesOrder
from sterlingpos.core.models import set_current_tenant

logger = logging.getLogger(__name__)


@shared_task(bind=True, max_retries=5)
def get_production_order(self, sales_pk, account_pk):
    try:
        account = Account.objects.get(pk=account_pk)
        set_current_tenant(account)
        sales_order = SalesOrder.objects.get(pk=sales_pk)
        for product_order in sales_order.items.filter(
            product__classification="Manufactured"
        ):
            for production_order in product_order.product.production_order.all():
                if production_order.started and production_order.finished:
                    if (
                        production_order.status == "completed"
                        and sales_order.created
                        <= production_order.started
                        <= production_order.finished
                    ):
                        production_order.automatic_production = True
                        production_order.save()
        set_current_tenant(None)
    except Exception as e:
        logged_data = "Unable to process migration: {}".format(e)
        logger.warning(logged_data)
        self.retry(countdown=2 ** self.request.retries)


def get_automatic_productions():
    for account in Account.objects.all():
        sales_order = SalesOrder.objects.filter(
            account=account, items__product__classification="Manufactured"
        )
        for sales in sales_order:
            get_production_order.delay(sales.pk, account.pk)
