import dateutil.parser

from django.views.generic import (
    TemplateView,
    View,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)
from django.utils.html import escape
from django.db.models import Q
from django.contrib import messages
from django.utils.text import slugify
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _
from django.urls import reverse_lazy
from django.db import transaction
from django.http import Http404, HttpResponseRedirect
from django.core.exceptions import SuspiciousOperation
from django.template.loader import render_to_string

from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    MessageMixin,
)
from django_datatables_view.base_datatable_view import BaseDatatableView
from dal import autocomplete
from decimal import Decimal
from formtools.wizard.views import SessionWizardView
from formtools.wizard.forms import ManagementForm
from datetime import datetime

from sterlingpos.core.import_export import SterlingImportMixin
from sterlingpos.core.mixins import CSVExportMixin, DatatableMixins
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.catalogue.utils import get_normalized_decimal
from sterlingpos.catalogue.models import UOM
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.productions.models import (
    BOM,
    BOMItemDetail,
    ProductionsOrder,
    ProductionOrderItemDetail,
)

from sterlingpos.productions.forms import (
    BOMForm,
    BOMItemDetailFormSet,
    BOMItemDetailFormsetHelper,
    CreateBOMOtherCostDetailFormset,
    CreateBOMOtherCostDetailFormsetHelper,
    BOMOtherCostDetailFormset,
    BOMOtherCostDetailFormsetHelper,
    ProductionsOrderForm,
    ProductionsOrderOtherCostDetailFormset,
    ProductionsOrderOtherCostDetailFormsetHelper,
    StartProductionForm,
    CompleteProductionForm,
    CompleteProductionItemDetailFormset,
    CompleteProductionItemDetailFormsetHelper,
    ProductionOrderUpdateForm,
    BOMComponentForm,
)
from sterlingpos.catalogue.utils import (
    uom_conversion,
    uom_cost_conversion,
)
from sterlingpos.productions.resources import BOMResource
from sterlingpos.catalogue.models import Product
from sterlingpos.stocks.forms import BatchFormset
from sterlingpos.stocks.models import TRANSACTION_TYPE, Batch
from sterlingpos.sales.views.mixins import ReportFilterMixins


class BOMListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "code",
        "products",
        "yield_quantity",
        "status",
        "action",
        "pk",
    ]

    order_columns = [
        "code",
        "products",
        "yield_quantity",
        "status",
        "action",
        "pk",
    ]
    model = BOM

    def render_column(self, row, column):
        if column == "action":
            action_data = {
                "id": row.id,
                "edit": reverse_lazy("productions:bom_update", kwargs={"pk": row.pk}),
                "active": row.status,
                "code": row.code,
            }
            return action_data
        if column == "code":
            return '<a href="{}">{}</a>'.format(
                reverse_lazy("productions:bom_detail", kwargs={"pk": row.pk}),
                escape(row.code),
            )
        if column == "yield_quantity":
            return get_normalized_decimal(row.yield_quantity)
        if column == "products":
            return list(row.products.values_list("name", flat=True))
        else:
            return super(BOMListJson, self).render_column(row, column)

    def filter_status(self, search_value):
        if search_value == "show":
            return Q()
        return Q(status=search_value)

    def filter_products(self, search_value):
        return Q(products__name__icontains=search_value) | Q(
            products__catalogue__name__icontains=search_value
        )

    def filter_queryset(self, qs):
        qs = super().filter_queryset(qs)
        qs = qs.distinct()
        return qs


class BOMListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/productions/bom_list.html"


class BOMItemUsageListJson(
    SterlingRoleMixin,
    ReportFilterMixins,
    CSVExportMixin,
    DatatableMixins,
    BaseDatatableView,
):

    columns = [
        "bom.code",
        "product.catalogue",
        "product",
        "stock_usage",
    ]

    order_columns = [
        "bom__code",
        "product__catalogue__name",
        "product__name",
        "stock_usage",
    ]

    csv_header = [
        "Code",
        "Produk",
        "Varian",
        "Stock Usage",
    ]
    csv_columns = [
        "bom.code",
        "product.catalogue",
        "product",
        "stock_usage",
    ]

    model = BOMItemDetail

    def get_csv_filename(self):
        csv_filename = "bom_item_usage"
        outlet_qs = get_outlet_of_user(self.request.user)
        if self.kwargs.get("outlet_pk"):
            outlet = get_object_or_404(outlet_qs, pk=self.kwargs.get("outlet_pk"))
            csv_filename = "{}_{}".format(csv_filename, slugify(outlet.name))

        if self.request.GET.get("start_date"):
            start_date = datetime.strptime(
                self.request.GET.get("start_date"), "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, start_date)

        if self.request.GET.get("end_date"):
            end_date = datetime.strptime(
                self.request.GET.get("end_date"), "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(csv_filename, end_date)

        return csv_filename

    def render_csv_column(self, row, column):
        if column == "stock_usage":
            return "{} {}".format(
                get_normalized_decimal(row.stock_usage), row.product.uom.unit
            )
        elif column == "last_stock":
            return "{} {}".format(
                get_normalized_decimal(row.last_stock), row.product.uom.unit
            )
        else:
            return super().render_csv_column(row, column)

    def get_initial_queryset(self):
        outlet_qs = get_outlet_of_user(self.request.user)
        outlet = self.get_outlet(self.kwargs.get("outlet_pk"), {})

        start_date, end_date = self.get_start_date_and_end_date(
            self.request.GET.get("date", None),
            self.request.GET.get("start_date", None),
            self.request.GET.get("end_date", None),
        )

        qs = (
            super()
            .get_initial_queryset()
            .select_related(
                "bom",
                "product",
                "product__uom",
                "product__catalogue",
            )
            .bom_usage(
                start_date,
                end_date,
                outlet,
                outlet_qs=outlet_qs,
            )
        )
        return qs

    def render_stock_usage(self, obj):
        return "{} {}".format(
            get_normalized_decimal(obj.stock_usage), obj.product.uom.unit
        )

    def render_last_stock(self, obj):
        return "{} {}".format(
            get_normalized_decimal(obj.last_stock), obj.product.uom.unit
        )


class BOMItemUsageListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/productions/bom_item_usage_list.html"


class BOMCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = "dashboard/productions/bom_form.html"
    model = BOM
    form_class = BOMForm
    success_url = reverse_lazy("productions:bom_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = BOMItemDetailFormSet
        context["formset_helper"] = BOMItemDetailFormsetHelper
        if "formset_other" not in context:
            context["formset_other"] = CreateBOMOtherCostDetailFormset
        context["formset_other_helper"] = CreateBOMOtherCostDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        formset = BOMItemDetailFormSet(self.request.POST)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            u"""There was a problemss creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = BOMItemDetailFormSet(form_params, instance=form.instance)
        other_formset = CreateBOMOtherCostDetailFormset(
            form_params, instance=form.instance
        )

        if not other_formset.is_valid() and "has_other_cost" in form_params:
            self.messages.error(
                _(
                    u"""
                There was a problem creating this {}.
                Please review the fields marked red and resubmit.
            """.format(
                        self.model._meta.verbose_name
                    )
                )
            )

            return self.render_to_response(
                self.get_context_data(
                    form=form, formset=formset, formset_other=other_formset
                )
            )

        with transaction.atomic():
            bom = form.instance
            bom.save()

            if formset.is_valid():
                for item in formset:
                    if item.is_valid() and item.has_changed():
                        deleted = item.cleaned_data.get("DELETE")
                        if not deleted:
                            item.save(commit=True)

                if other_formset.is_valid():
                    for other_item in other_formset:
                        name = other_item.cleaned_data.get("name")
                        deleted = other_item.cleaned_data.get("DELETE")
                        if not deleted and name:
                            other_item.save(commit=True)

        self.messages.success(
            _("Bill of Materials {} was successfully created.".format(bom))
        )
        return super().form_valid(form)


class BOMUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/productions/bom_form.html"
    model = BOM
    form_class = BOMForm
    success_url = reverse_lazy("productions:bom_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if "formset_other" not in context:
            context["formset_other"] = BOMOtherCostDetailFormset(
                instance=self.object, queryset=self.object.bom_other_cost.all()
            )
        context["formset_other_helper"] = BOMOtherCostDetailFormsetHelper

        context["total_other_cost"] = self.object.bom_other_cost.all().count()
        return context

    def form_invalid(self, form, formset_other):
        error_messages = _(
            u"""There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset_other=formset_other)
        )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset_other = BOMOtherCostDetailFormset(
            self.request.POST, instance=self.object
        )
        if form.is_valid() and formset_other.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset_other)

    def delete_productions_item(self, item, bom):
        productions = bom.production_order.filter(status="finalized")
        for production in productions:
            obj = production.productions_item.get(product=item.product)
            obj.delete()

    def update_productions_item(self, item, bom, products):
        productions = bom.production_order.filter(status="finalized")
        for production in productions:
            data = {
                "quantity": (production.produced_quantity / bom.yield_quantity)
                * item.quantity,
                "quantity_unit": item.quantity_unit,
                "wastage_quantity": item.wastage,
                "wastage_unit": item.wastage_unit,
                "unconsumed_unit": item.product.uom,
            }
            try:
                obj = production.productions_item.get(product=item.product)
                for key, value in data.items():
                    setattr(obj, key, value)
                obj.save()
            except ProductionOrderItemDetail.DoesNotExist:
                default = {"product": item.product, "productions": production}
                data.update(default)
                obj = ProductionOrderItemDetail(**data)
                obj.save()
            deleted_components = production.productions_item.exclude(
                product__in=products
            )
            deleted_components.delete()

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset_other = BOMOtherCostDetailFormset(form_params, instance=form.instance)

        with transaction.atomic():
            bom = form.instance
            bom.save()
            if formset_other.is_valid():
                for deleted_form2 in formset_other.deleted_forms:
                    if deleted_form2.instance and deleted_form2.instance.pk:
                        deleted_form2.instance.delete()

                for other_item in formset_other:
                    name = other_item.cleaned_data.get("name")
                    deleted = other_item.cleaned_data.get("DELETE")
                    if not deleted and name:
                        other_item.save(commit=True)

        self.messages.success(
            _("Bill of Materials {} was successfully updated.".format(bom))
        )
        return super().form_valid(form)


class BOMItemDetailListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "product",
        "quantity",
        "wastage",
        "cost_per_item",
        "total_cost",
        "action",
    ]

    order_columns = [
        "product",
        "quantity",
        "wastage",
        "cost_per_item",
        "total_cost",
        "action",
    ]
    model = BOMItemDetail

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.filter(bom__pk=self.kwargs["pk"])
        qs = qs.select_related("product")
        return qs

    def render_column(self, row, column):
        if column == "quantity":
            return "{} {}".format(
                get_normalized_decimal(row.quantity), row.quantity_unit
            )
        elif column == "wastage":
            return "{} {}".format(get_normalized_decimal(row.wastage), row.wastage_unit)
        elif column == "action":
            if row.bom.status == "active":
                html = """
                    <button class="btn btn-link dropdown-toggle" type="button" id="actionButton-row-{0}"
                        data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="actionButton-row-{0}">
                        <button class="dropdown-item btn btn-link component-update" data-toggle="modal"
                            data-target="#components-update" data-url="{1}">Ubah</button>
                        <div class="dropdown-divider"></div>
                        <button class="dropdown-item btn btn-link component-delete" data-toggle="modal"
                            data-target="#components-delete" data-url="{2}">Hapus</button>
                    </div>
                """.format(
                    row.pk,
                    reverse_lazy(
                        "productions:bom_component_update",
                        kwargs={"bom_pk": row.bom.pk, "pk": row.pk},
                    ),
                    reverse_lazy(
                        "productions:bom_component_delete",
                        kwargs={"bom_pk": row.bom.pk, "pk": row.pk},
                    ),
                )
                return html
            return ""
        elif column == "cost_per_item":
            return "Rp. {}".format(get_normalized_decimal(row.product.cost.amount))
        elif column == "total_cost":
            return "Rp. {}".format(get_normalized_decimal(row.total_cost))
        else:
            return super().render_column(row, column)

    def filter_product(self, value):
        return Q(product__name__icontains=value) | Q(
            product__catalogue__name__icontains=value
        )


class BOMDetailView(SterlingRoleMixin, DetailView):
    model = BOM
    template_name = "dashboard/productions/bom_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "modal_form": BOMComponentForm(bom_pk=self.kwargs["pk"]),
                "products": list(
                    self.object.products.all().values_list("pk", flat=True)
                ),
            }
        )
        return context


class BOMCreateComponentsView(
    SterlingRoleMixin, MessageMixin, JSONResponseMixin, CreateView
):
    model = BOMItemDetail
    form_class = BOMComponentForm
    template_name = "dashboard/productions/bom_component_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        bom = get_object_or_404(BOM, pk=self.kwargs["bom_pk"], status="active")
        products = list(bom.products.all().values_list("pk", flat=True))
        context.update(
            {
                "bom_id": self.kwargs["bom_pk"],
                "products": products,
                "table_id": "#tbl_bom_components",
                "action_url": reverse_lazy(
                    "productions:bom_component_create",
                    kwargs={"bom_pk": self.kwargs["bom_pk"]},
                ),
            }
        )
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return self.render_json_response(self.get_error_result(form))
        return super().form_invalid(form)

    def get_error_result(self, form):
        html = render_to_string(
            self.get_template_names(),
            self.get_context_data(form=form),
            request=self.request,
        )
        return {"status": "error", "message": html}

    def get_form_valid_message(self):
        return _("<strong>%s</strong> was successfully added to components!") % (
            self.object.product.name,
        )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"bom_pk": self.kwargs["bom_pk"]})
        return kwargs

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.bom = get_object_or_404(
            BOM, pk=self.kwargs["bom_pk"], status="active"
        )
        self.object.save()
        if self.request.is_ajax():
            return self.render_json_response(
                {
                    "status": "success",
                    "message": self.get_form_valid_message(),
                    "object_pk": self.object.pk,
                }
            )
        return super().form_valid(form)


class BOMUpdateComponentsView(
    SterlingRoleMixin, MessageMixin, JSONResponseMixin, UpdateView
):
    model = BOMItemDetail
    form_class = BOMComponentForm
    template_name = "dashboard/productions/bom_component_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "pk": self.kwargs["pk"],
                "bom_id": self.kwargs["bom_pk"],
            }
        )
        return context

    def form_invalid(self, form):
        if self.request.is_ajax():
            return self.render_json_response(self.get_error_result(form))
        return super().form_invalid(form)

    def get_error_result(self, form):
        html = render_to_string(
            self.get_template_names(),
            self.get_context_data(form=form),
            request=self.request,
        )
        return {"status": "error", "message": html}

    def get_form_valid_message(self):
        return _("Component <strong>%s</strong> was successfully updated!") % (
            self.object.product.name,
        )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"bom_pk": self.kwargs["bom_pk"]})
        return kwargs

    def form_valid(self, form):
        self.object = form.save(commit=True)
        if self.request.is_ajax():
            return self.render_json_response(
                {
                    "status": "success",
                    "message": self.get_form_valid_message(),
                    "object_pk": self.object.pk,
                }
            )
        return super().form_valid(form)


class BOMDeleteComponentView(
    SterlingRoleMixin, MessageMixin, JSONResponseMixin, DeleteView
):
    model = BOMItemDetail

    def get_object(self, queryset=None):
        return get_object_or_404(
            BOMItemDetail, pk=self.kwargs["pk"], bom__pk=self.kwargs["bom_pk"]
        )

    def get_form_valid_message(self):
        return _("Component <strong>%s</strong> was successfully deleted!") % (
            self.object.product.name,
        )

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        if self.request.is_ajax():
            return self.render_json_response(
                {
                    "status": "success",
                    "message": self.get_form_valid_message(),
                    "object_pk": self.object.pk,
                },
                status=200,
            )
        return super().delete(request, *args, **kwargs)


class BOMDeactivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}
        status_code = 404
        try:
            bom_id = request.POST.get("id", "")
            qs = BOM.objects.all()
            bom = qs.get(pk=bom_id)
            bom.status = BOM.STATUS.inactive
            bom.save()
            status_code = 200
            response["status"] = "success"
            messages.success(
                request, "{}".format(_("Bill of materials has been deactivated"))
            )
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class BOMActivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}
        status_code = 404
        try:
            bom_id = request.POST.get("id", "")
            qs = BOM.objects.all()
            bom = qs.get(pk=bom_id)
            bom.status = BOM.STATUS.active
            bom.save()
            status_code = 200
            response["status"] = "success"
            messages.success(
                request, "{}".format(_("Bill of materials has been activated"))
            )
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class BOMDeleteJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}
        status_code = 404
        try:
            bom_id = request.POST.get("id", "")
            qs = BOM.objects.all()
            bom = qs.get(pk=bom_id)
            can_delete = True

            for production in bom.production_order.all():
                if production.status == "in_production":
                    can_delete = False
            status_code = 200
            response["status"] = "success"
            if can_delete:
                bom.delete()
                messages.success(
                    request, "{}".format(_("Bill of materials has been deleted"))
                )
            else:
                messages.warning(
                    request,
                    "Tidak bisa hapus Resep Produk karena sedang ada produksi yang berjalan.",
                )
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class ComponentProductView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404

        try:
            bom = request.GET.get("bom", "")
            components = (
                Product.objects.filter(
                    archived=False,
                    is_manage_stock=True,
                    deleted__isnull=True,
                    classification__in=[
                        Product.PRODUCT_CLASSIFICATION.standart,
                        Product.PRODUCT_CLASSIFICATION.manufactured,
                    ],
                )
                .exclude(pk=bom)
                .values("pk", "name", "sku", "catalogue__name")
            )
            response["result"]["components"] = list(components)
            status_code = 200
            response["status"] = "success"
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class ProductionListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = [
        "code",
        "product.catalogue.name",
        "product.name",
        "produced_quantity",
        "outlet.name",
        "status",
        "pk",
    ]

    order_columns = [
        "code",
        "product__catalogue__name",
        "product__name",
        "produced_quantity",
        "outlet__pk",
        "status",
        "pk",
    ]

    model = ProductionsOrder

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        qs = qs.order_by("-created")
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(self.request.user)
            qs = qs.filter(outlet__in=outlet_qs)
        return qs

    def render_column(self, row, column):
        if column == "code":
            return '<a href="{}">{}</a>'.format(
                reverse_lazy("productions:production_detail", kwargs={"pk": row.pk}),
                escape(row.code),
            )
        elif column == "produced_quantity":
            return get_normalized_decimal(row.produced_quantity)
        else:
            return super(ProductionListJson, self).render_column(row, column)

    def filter_status(self, search_value):
        if search_value == "show":
            return Q()
        return Q(status=search_value)


class BOMAutoCompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = BOM.objects.none()
        product = self.forwarded.get("product")
        if product:
            qs = BOM.objects.filter(products__pk=product, status=BOM.STATUS.active)
        if self.q:
            qs = qs.filter(
                Q(code__istartswith=self.q)
                | Q(products__name__istartswith=self.q)
                | Q(products__catalogue__name__icontains=self.q)
            )
        return qs


class BOMQuantityView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def get_ajax(self, request, *args, **kwargs):
        item_list = []
        other_cost_list = []
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            bom_pk = request.GET.get("bom", "")
            produced_quantity = request.GET.get("produced_quantity", "")
            produced_quantity = Decimal(produced_quantity)
            bom = BOM.objects.get(pk=bom_pk)
            response["status"] = "success"
            response["result"]["quantity"] = bom.yield_quantity
            for row, item in enumerate(bom.bom_item.filter(product__archived=False)):
                converted_cost = uom_cost_conversion(
                    item.product.uom,
                    item.quantity_unit,
                    1,
                    item.product.cost.amount,
                )
                converted_cost = Decimal(converted_cost.quantize(Decimal(".01")))
                quantity = Decimal(produced_quantity) * (
                    Decimal(item.quantity) / Decimal(bom.yield_quantity)
                )
                quantity = Decimal(quantity.quantize(Decimal(".001")))
                wastage = Decimal(produced_quantity) * (
                    Decimal(item.wastage) / Decimal(bom.yield_quantity)
                )
                wastage = Decimal(wastage.quantize(Decimal(".001")))
                total_cost = Decimal(quantity) * Decimal(converted_cost)
                item_list.append(
                    [
                        "{}".format(item.product),
                        "{} {}".format(
                            get_normalized_decimal(quantity),
                            item.quantity_unit,
                        ),
                        "{} {}".format(
                            get_normalized_decimal(wastage),
                            item.wastage_unit,
                        ),
                        get_normalized_decimal(converted_cost),
                        get_normalized_decimal(total_cost),
                    ]
                )

            for row, other_cost in enumerate(bom.bom_other_cost.all()):
                other_cost_list.append(
                    {
                        "name": other_cost.name,
                        "quantity": other_cost.quantity,
                        "cost_per_item": get_normalized_decimal(
                            other_cost.cost_per_item.amount
                        ),
                        "total_cost": get_normalized_decimal(other_cost.total_cost),
                    },
                )
            response["result"]["item"] = item_list
            response["result"]["other_cost"] = other_cost_list
            status_code = 200
        except BOM.DoesNotExist:
            pass
        return self.render_json_response(response, status=status_code)


class ProductCostConversionView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            product_pk = request.GET.get("product", "")
            unit = request.GET.get("unit", "")
            product = Product.objects.get(pk=product_pk)
            unit = UOM.objects.get(pk=unit)
            converted_cost = uom_cost_conversion(
                product.uom,
                unit,
                1,
                product.cost.amount,
            )
            converted_cost = get_normalized_decimal(converted_cost)
            status_code = 200
            response["status"] = "success"
            response["result"]["cost"] = converted_cost
        except (Product.DoesNotExist, UOM.DoesNotExist):
            status_code = 404
        except ValueError:
            status_code = 400
        return self.render_json_response(response, status=status_code)


class BOMYieldQuantityJsonView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "result": {}}
        status_code = 404
        try:
            bom_pk = request.GET.get("bom", "")
            bom = BOM.objects.get(pk=bom_pk)
            response["status"] = "success"
            status_code = 200
            response["result"]["yield_quantity"] = get_normalized_decimal(
                bom.yield_quantity
            )
        except (BOM.DoesNotExist, ValueError):
            pass

        return self.render_json_response(response, status=status_code)


class ProductionListView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/productions/production_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["outlets"] = get_outlet_of_user(self.request.user)
        return context


class ProductionsCreateView(SterlingRoleMixin, MessageMixin, CreateView):
    template_name = "dashboard/productions/production_form.html"
    model = ProductionsOrder
    form_class = ProductionsOrderForm
    success_url = reverse_lazy("productions:production_list")

    def get_success_url(self):
        return reverse_lazy(
            "productions:production_detail", kwargs={"pk": self.object.pk}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = ProductionsOrderOtherCostDetailFormset
        context["formset_helper"] = ProductionsOrderOtherCostDetailFormsetHelper
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        formset = ProductionsOrderOtherCostDetailFormset(self.request.POST)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            u"""There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )
        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = ProductionsOrderOtherCostDetailFormset(
            form_params, instance=form.instance
        )

        with transaction.atomic():
            self.object = form.save(commit=True)
            bom = form.cleaned_data.get("bom")
            produced_quantity = form.cleaned_data.get("produced_quantity")
            if bom.bom_item.filter(product__archived=False).exists():
                for item in bom.bom_item.filter(product__archived=False):
                    data = {
                        "product": item.product,
                        "quantity": (produced_quantity / bom.yield_quantity)
                        * item.quantity,
                        "quantity_unit": item.quantity_unit,
                        "wastage_quantity": (produced_quantity / bom.yield_quantity)
                        * item.wastage,
                        "wastage_unit": item.wastage_unit,
                        "unconsumed_unit": item.product.uom,
                    }
                    self.object.productions_item.create(**data)
            else:
                error_messages = _(
                    "Produk Manufaktur {} tidak memiliki BOM".format(self.object)
                )
                self.messages.error(error_messages)
                return self.render_to_response(
                    self.get_context_data(form=form, formset=formset)
                )
            if formset.is_valid():
                for item in formset:
                    deleted = item.cleaned_data.get("DELETE")
                    if not deleted:
                        item.save(commit=True)

        self.messages.success(
            _("Productions {} was successfully created.".format(self.object))
        )
        return super().form_valid(form)


class ProductionDetailView(SterlingRoleMixin, DetailView):
    template_name = "dashboard/productions/production_order.html"
    model = ProductionsOrder

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        production_components = []
        stocks_available = []
        productions_components_qs = self.object.productions_item.all()
        if self.object.status == ProductionsOrder.STATUS.finalized:
            productions_components_qs = self.object.productions_item.filter(
                product__archived=False
            )
        for item in productions_components_qs:
            converted_cost = uom_cost_conversion(
                item.product.uom,
                item.quantity_unit,
                1,
                item.product.cost.amount,
            )
            current_stock = uom_conversion(
                item.product.uom,
                item.quantity_unit,
                item.product.stocks.get(outlet=self.object.outlet).balance,
            )
            data = {
                "item_name": "{} - {}".format(
                    item.product.catalogue.name, item.product.name
                ),
                "quantity": "{} {}".format(
                    get_normalized_decimal(item.quantity),
                    item.quantity_unit,
                ),
                "availability": "{} {}".format(
                    get_normalized_decimal(
                        item.product.stocks.get(outlet=self.object.outlet).balance
                    ),
                    item.product.uom,
                ),
                "wastage": "{} {}".format(
                    get_normalized_decimal(item.wastage_quantity),
                    item.wastage_unit,
                ),
                "cost_per_item": get_normalized_decimal(converted_cost),
                "total_cost": get_normalized_decimal(item.total_cost),
                "stock_available": item.quantity <= current_stock,
            }
            if self.object.status == ProductionsOrder.STATUS.completed:
                data = {
                    "item_name": "{} - {}".format(
                        item.product.catalogue.name, item.product.name
                    ),
                    "quantity": "{} {}".format(
                        get_normalized_decimal(item.quantity),
                        item.quantity_unit,
                    ),
                    "availability": "{} {}".format(
                        get_normalized_decimal(
                            item.product.stocks.get(outlet=self.object.outlet).balance
                        ),
                        item.product.uom,
                    ),
                    "wastage": "{} {}".format(
                        get_normalized_decimal(item.wastage_quantity),
                        item.wastage_unit,
                    ),
                    "unconsumed": "{} {}".format(
                        get_normalized_decimal(item.unconsumed_quantity),
                        item.unconsumed_unit,
                    ),
                    "cost_per_item": get_normalized_decimal(item.product.cost.amount),
                    "total_cost": get_normalized_decimal(item.total_cost),
                    "stock_available": item.quantity <= current_stock,
                }
            production_components.append(data)
            stocks_available.append(item.quantity <= current_stock)
        stocks_not_enough = False in stocks_available
        context["stocks_not_enough"] = stocks_not_enough
        context["production_components"] = production_components
        context["productions_other_cost"] = self.object.productions_other_cost.all()
        context["start_production_form"] = StartProductionForm
        return context


class ProductionUpdateView(SterlingRoleMixin, MessageMixin, UpdateView):
    template_name = "dashboard/productions/production_form.html"
    model = ProductionsOrder
    form_class = ProductionOrderUpdateForm

    def get_success_url(self):
        return reverse_lazy(
            "productions:production_detail", kwargs={"pk": self.object.pk}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = ProductionsOrderOtherCostDetailFormset(
                instance=self.object, queryset=self.object.productions_other_cost.all()
            )
        context["formset_helper"] = ProductionsOrderOtherCostDetailFormsetHelper
        production_components = []
        for item in self.object.productions_item.all():
            converted_cost = uom_cost_conversion(
                item.product.uom,
                item.quantity_unit,
                1,
                item.product.cost.amount,
            )
            production_components.append(
                {
                    "item_name": "{} - {}".format(
                        item.product.catalogue.name, item.product.name
                    ),
                    "quantity": "{} {}".format(
                        get_normalized_decimal(item.quantity),
                        item.quantity_unit,
                    ),
                    "availability": "{} {}".format(
                        get_normalized_decimal(
                            item.product.stocks.get(outlet=self.object.outlet).balance
                        ),
                        item.product.uom,
                    ),
                    "wastage": "{} {}".format(
                        get_normalized_decimal(item.wastage_quantity),
                        item.wastage_unit,
                    ),
                    "cost_per_item": get_normalized_decimal(converted_cost),
                    "total_cost": get_normalized_decimal(item.total_cost),
                }
            )
        context["production_components"] = production_components
        context["total_other_cost"] = self.object.productions_other_cost.all().count()
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset = ProductionsOrderOtherCostDetailFormset(
            self.request.POST, instance=self.object
        )
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset):
        error_messages = _(
            u"""There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form):
        formset = ProductionsOrderOtherCostDetailFormset(
            self.request.POST, instance=self.object
        )

        with transaction.atomic():
            self.object = form.save()
            if formset.is_valid():
                for deleted in formset.deleted_forms:
                    if deleted.instance and deleted.instance.pk:
                        deleted.instance.delete()

                for item in formset:
                    deleted = item.cleaned_data.get("DELETE")
                    if not deleted:
                        item.save(commit=True)

        self.messages.success(
            _("Productions {} was successfully update.".format(self.object))
        )
        return super().form_valid(form)


class ProductionCancelledView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}
        status_code = 404
        try:
            production_id = request.POST.get("id", "")
            qs = ProductionsOrder.objects.all()
            production = qs.get(pk=production_id)
            production.status = ProductionsOrder.STATUS.cancelled
            production.save()
            status_code = 200
            response["status"] = "success"
            messages.success(
                request, "{}".format(_("Production Order has been cancelled"))
            )
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


class ProductionStartedView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_availability(self, production):
        availability = []
        for item in production.productions_item.filter(product__archived=False):
            stock = item.product.stocks.get(outlet=production.outlet)
            converted_qty = uom_conversion(
                item.quantity_unit, item.product.uom, item.quantity
            )
            availability.append(converted_qty <= stock.balance)

        if not availability:
            return False
        if False in availability:
            return False
        return True

    def post_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}
        status_code = 404
        try:
            started = request.POST.get("started", "")
            production_id = request.POST.get("id", "")
            qs = ProductionsOrder.objects.all()
            production = qs.get(pk=production_id)

            status_code = 200
            response["status"] = "success"

            if self.get_availability(production):
                production.started = dateutil.parser.parse(started, dayfirst=True)
                production.status = ProductionsOrder.STATUS.in_production
                production.save()
                production.start_productions(request.user)

                messages.success(
                    request,
                    "{}".format(_("Production Order {} started".format(production))),
                )
            else:
                messages.error(
                    request,
                    "{}".format(_("Production Components stock not available.")),
                )
        except ValueError:
            pass
        except ProductionsOrder.DoesNotExist:
            raise Http404

        return self.render_json_response(response, status=status_code)


class ProductionCompleteView(SterlingRoleMixin, MessageMixin, UpdateView):
    model = ProductionsOrder
    form_class = CompleteProductionForm
    success_url = reverse_lazy("productions:production_list")
    template_name = "dashboard/productions/production_complete_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = CompleteProductionItemDetailFormset(
                instance=self.object, queryset=self.object.productions_item.all()
            )
        context["formset_helper"] = CompleteProductionItemDetailFormsetHelper
        if "formset_other" not in context:
            context["formset_other"] = ProductionsOrderOtherCostDetailFormset(
                instance=self.object, queryset=self.object.productions_other_cost.all()
            )
        context["formset_other_helper"] = ProductionsOrderOtherCostDetailFormsetHelper
        context["total_other_cost"] = self.object.productions_other_cost.all().count()
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset = CompleteProductionItemDetailFormset(
            self.request.POST, instance=self.object
        )
        formset_other = ProductionsOrderOtherCostDetailFormset(
            self.request.POST, instance=self.object
        )
        if form.is_valid() and formset.is_valid() and formset_other.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, formset, formset_other)

    def form_invalid(self, form, formset, formset_other):
        error_messages = _(
            u"""There was a problem creating this {}.
            Please review the fields marked red and resubmit.""".format(
                self.model._meta.verbose_name
            )
        )

        self.messages.error(error_messages)
        return self.render_to_response(
            self.get_context_data(
                form=form, formset=formset, formset_other=formset_other
            )
        )

    def form_valid(self, form):
        form_params = self.request.POST or None
        formset = CompleteProductionItemDetailFormset(
            form_params, instance=form.instance
        )
        formset_other = ProductionsOrderOtherCostDetailFormset(
            form_params, instance=form.instance
        )

        with transaction.atomic():
            production = form.save(commit=False)
            finished = form.cleaned_data.get("finished")
            production.finished = dateutil.parser.parse(finished, dayfirst=True)
            production.status = ProductionsOrder.STATUS.completed
            production.save()

            if formset.is_valid() and formset_other.is_valid():
                for deleted in formset_other.deleted_forms:
                    if deleted.instance and deleted.instance.pk:
                        deleted.instance.delete()

                for item in formset:
                    wastage_unit = item.cleaned_data.get("wastage_unit")
                    unconsumed_unit = item.cleaned_data.get("unconsumed_unit")
                    if item.instance and item.instance.pk:
                        item.instance.unconsumed_unit = unconsumed_unit
                        item.instance.wastage_unit = wastage_unit
                        item.instance.save()
                for other_item in formset_other:
                    if other_item.instance and other_item.instance.pk:
                        other_item.instance.save()
                    else:
                        deleted = other_item.cleaned_data.get("DELETE")
                        if not deleted:
                            other_item.production = production
                            other_item.save()

            production.complete_productions(self.request.user)
        self.messages.success(_("Production Order {} Completed.".format(production)))
        return super().form_valid(form)


class ProductionUnconsumedJsonView(
    SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View
):
    def get_ajax(self, request, *args, **kwargs):
        response = {"status": "fail", "data": {}}
        status_code = 404
        try:
            production_pk = request.GET.get("production_pk", "")
            production = ProductionsOrder.objects.get(pk=production_pk)

            production_detail_pk = request.GET.get("production_detail_pk", "")
            production_detail = production.productions_item.get(pk=production_detail_pk)

            unconsumed = request.GET.get("unconsumed", "")
            unconsumed_unit = request.GET.get("unconsumed_unit", "")
            unconsumed_uom = UOM.objects.get(pk=unconsumed_unit)

            status_code = 200
            response["status"] = "success"
            converted_unconsumed = uom_conversion(
                unconsumed_uom, production_detail.quantity_unit, unconsumed
            )
            converted_cost = uom_cost_conversion(
                production_detail.product.uom,
                production_detail.quantity_unit,
                1,
                production_detail.product.cost.amount,
            )
            total = production_detail.quantity - Decimal(converted_unconsumed)
            total_cost = total * converted_cost
            response["data"]["total_cost"] = get_normalized_decimal(total_cost)
        except ValueError:
            pass
        return self.render_json_response(response, status=status_code)


STEP_PRODUCTION_COMPLETE_FORM = "productions_order_complete"
STEP_BATCH_TRACKED_FORM = "batch_tracked_form"


def has_batch_tracked(wizard):
    production_order = ProductionsOrder.objects.get(pk=wizard.kwargs["pk"])
    return production_order.product.is_batch_tracked


class ProductionCompleteWizardView(SterlingRoleMixin, MessageMixin, SessionWizardView):
    template_name = {
        STEP_PRODUCTION_COMPLETE_FORM: "dashboard/productions/production_complete_form.html",
        STEP_BATCH_TRACKED_FORM: "dashboard/stocks/batch_tracked_form.html",
    }
    form_list = [
        (STEP_PRODUCTION_COMPLETE_FORM, CompleteProductionForm),
        (STEP_BATCH_TRACKED_FORM, BatchFormset),
    ]
    condition_dict = {STEP_BATCH_TRACKED_FORM: has_batch_tracked}

    def get_success_url(self):
        return reverse_lazy("productions:production_list")

    def get_template_names(self):
        return [self.template_name[self.steps.current]]

    def __init__(self, *args, **kwargs):
        super(ProductionCompleteWizardView, self).__init__(*args, **kwargs)
        self.object = []

    def get_context_data(self, form, **kwargs):
        self.object = ProductionsOrder.objects.get(pk=self.kwargs["pk"])
        context = super().get_context_data(form, **kwargs)
        context["object"] = self.object
        if self.steps.current == STEP_PRODUCTION_COMPLETE_FORM:
            if "formset" not in context:
                context["formset"] = CompleteProductionItemDetailFormset(
                    instance=self.object, queryset=self.object.productions_item.all()
                )
            context["formset_helper"] = CompleteProductionItemDetailFormsetHelper
            if "formset_other" not in context:
                context["formset_other"] = ProductionsOrderOtherCostDetailFormset(
                    instance=self.object,
                    queryset=self.object.productions_other_cost.all(),
                )
            context[
                "formset_other_helper"
            ] = ProductionsOrderOtherCostDetailFormsetHelper
            context[
                "total_other_cost"
            ] = self.object.productions_other_cost.all().count()

        if self.steps.current == STEP_BATCH_TRACKED_FORM:
            context["inventory_type"] = TRANSACTION_TYPE.productions
            context["outlet_pk"] = self.object.outlet.pk
            context["outlet"] = self.object.outlet
            context["status"] = TRANSACTION_TYPE.productions
            context["status_display"] = TRANSACTION_TYPE["productions"]
        return context

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs(step)
        obj = ProductionsOrder.objects.get(pk=self.kwargs["pk"])
        if step == STEP_PRODUCTION_COMPLETE_FORM:
            kwargs.update({"instance": obj})
        if step == STEP_BATCH_TRACKED_FORM:
            kwargs["status"] = TRANSACTION_TYPE.productions
            kwargs["outlet"] = obj.outlet
        return kwargs

    def get_form_initial(self, step):
        if step == STEP_BATCH_TRACKED_FORM:
            obj = ProductionsOrder.objects.get(pk=self.kwargs["pk"])
            initial_data = []
            initial_data.append(
                {
                    "product": obj.product,
                    "product_name": obj.product.name,
                    "quantity_to_assign": "{} {}".format(
                        get_normalized_decimal(Decimal(obj.produced_quantity)),
                        obj.product.uom,
                    ),
                    "total_quantity": obj.produced_quantity,
                    "unit": obj.product.uom.pk,
                }
            )
            return self.instance_dict.get(step, initial_data)
        return super().get_form_initial(step)

    def post(self, *args, **kwargs):
        wizard_goto_step = self.request.POST.get("wizard_goto_step", None)
        if wizard_goto_step and wizard_goto_step in self.get_form_list():
            return self.render_goto_step(wizard_goto_step)

        management_form = ManagementForm(self.request.POST, prefix=self.prefix)
        if not management_form.is_valid():
            raise SuspiciousOperation(
                _("ManagementForm data is missing or has been tampered.")
            )

        form = self.get_form(data=self.request.POST, files=self.request.FILES)
        if form.is_valid():
            self.storage.set_step_data(self.steps.current, self.process_step(form))
            self.storage.set_step_files(
                self.steps.current, self.process_step_files(form)
            )

            if self.steps.current == self.steps.last:
                return self.render_done(form, **kwargs)
            else:
                if self.steps.current == STEP_PRODUCTION_COMPLETE_FORM:
                    formset = CompleteProductionItemDetailFormset(
                        self.request.POST, instance=form.instance
                    )
                    formset_other = ProductionsOrderOtherCostDetailFormset(
                        self.request.POST, instance=form.instance
                    )

                    if formset.is_valid() and formset_other.is_valid():
                        return self.render_next_step(form)
                    else:
                        error_messages = _(
                            u"""There was a problem creating this Stock Adjustment.
                            Please review the fields marked red and resubmit."""
                        )

                        self.messages.error(error_messages)
                        return self.render_to_response(
                            self.get_context_data(
                                form=form, formset=formset, formset_other=formset_other
                            )
                        )
                else:
                    return self.render_done(form, **kwargs)
        if self.steps.current not in self.steps.all:
            return self.render_done(form, **kwargs)
        return self.render(form)

    def done(self, form_list, **kwargs):
        productions_form = list(form_list)[0]
        form_params = self.storage.get_step_data(STEP_PRODUCTION_COMPLETE_FORM)
        productions_formset = CompleteProductionItemDetailFormset(
            form_params, instance=productions_form.instance
        )
        productions_formset_other = ProductionsOrderOtherCostDetailFormset(
            form_params, instance=productions_form.instance
        )
        if len(form_list) > 1:
            batch_formset = list(form_list)[1]

        with transaction.atomic():
            production = productions_form.save(commit=False)
            finished = productions_form.cleaned_data.get("finished")
            production.finished = dateutil.parser.parse(finished, dayfirst=True)
            production.status = ProductionsOrder.STATUS.completed
            production.save()
            outlet = production.outlet

            if productions_formset.is_valid() and productions_formset_other.is_valid():
                for deleted in productions_formset_other.deleted_forms:
                    if deleted.instance and deleted.instance.pk:
                        deleted.instance.delete()

                for item in productions_formset:
                    wastage_unit = item.cleaned_data.get("wastage_unit")
                    unconsumed_unit = item.cleaned_data.get("unconsumed_unit")
                    if item.instance and item.instance.pk:
                        item.instance.unconsumed_unit = unconsumed_unit
                        item.instance.wastage_unit = wastage_unit
                        item.instance.save()
                for other_item in productions_formset_other:
                    if other_item.instance and other_item.instance.pk:
                        other_item.instance.save()
                    else:
                        deleted = other_item.cleaned_data.get("DELETE")
                        if not deleted:
                            other_item.production = production
                            other_item.save()
            production.complete_productions(self.request.user)

            if len(form_list) > 1:
                for batch_form in batch_formset:
                    if batch_form.is_valid():
                        for item in batch_form.nested:
                            if item.is_valid():
                                batch = None
                                deleted = item.cleaned_data.get("DELETE")
                                batch_id = item.cleaned_data.get("batch_id")
                                quantity = item.cleaned_data.get("quantity")
                                unit = item.cleaned_data.get("unit")
                                converted_qty = uom_conversion(
                                    unit, batch_form.instance.product.uom, quantity
                                )

                                if not deleted:
                                    batch, created = Batch.objects.get_or_create(
                                        batch_id=batch_id,
                                        product=batch_form.instance.product,
                                    )
                                    if created:
                                        expiration_date = datetime.strptime(
                                            item.cleaned_data.get("expiration_date"),
                                            "%d/%m/%Y",
                                        )
                                        batch.expiration_date = expiration_date
                                        batch.save()

                                        batch_item = item.save(commit=False)
                                        batch_item.batch = batch
                                        batch_item.outlet = outlet
                                        batch_item.save()
                                        batch_item.set_batch_balance(
                                            converted_qty,
                                            batch_form.instance.product.uom,
                                            self.object,
                                            TRANSACTION_TYPE.stock_count,
                                        )
                                    else:
                                        (
                                            batch_item,
                                            created,
                                        ) = batch.batch_items.get_or_create(
                                            outlet=outlet
                                        )
                                        batch_item.set_batch_balance(
                                            converted_qty,
                                            batch_form.instance.product.uom,
                                            self.object,
                                            TRANSACTION_TYPE.stock_count,
                                        )

        self.messages.success(_("Production Order {} Completed.".format(production)))
        return HttpResponseRedirect(self.get_success_url())


class BOMImportTemplateView(SterlingRoleMixin, TemplateView):
    template_name = "dashboard/productions/bom_import.html"


class BOMImportView(SterlingRoleMixin, SterlingImportMixin):
    template_name = "dashboard/productions/bom_import_form.html"
    resource_class = BOMResource
    file_headers = [
        "product",
        "code",
        "yield_quantity",
        "sku_components",
        "product_components",
        "quantity_to_consumed",
        "unit_to_consumed",
    ]
    success_url = reverse_lazy("productions:bom_import")


class ProductComponentsAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Product.objects.filter(
            archived=False,
            is_manage_stock=True,
            uom__isnull=False,
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.standart,
                Product.PRODUCT_CLASSIFICATION.manufactured,
                Product.PRODUCT_CLASSIFICATION.complementary,
            ],
        )
        manufactured_product = self.forwarded.get("manufactured_product")
        if manufactured_product:
            qs = qs.exclude(pk__in=manufactured_product)

        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(sku__icontains=self.q)
                | Q(catalogue__name__icontains=self.q)
            )
        return qs


class UOMComponentsAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = UOM.objects.none()
        product_component_pk = self.forwarded.get("product")
        try:
            product_component = Product.objects.get(pk=product_component_pk)
        except Product.DoesNotExist:
            product_component = None
        except ValueError:
            product_component = None
        if product_component:
            qs = UOM.objects.filter(category=product_component.uom.category)

        if self.q:
            qs = qs.filter(Q(unit__icontains=self.q))
        return qs


class ProductionProductAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Product.objects.filter(
            archived=False,
            is_manage_stock=True,
            uom__isnull=False,
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.manufactured,
                Product.PRODUCT_CLASSIFICATION.complementary,
            ],
            bom__isnull=False,
        ).distinct()

        if self.q:
            qs = qs.filter(Q(name__icontains=self.q) | Q(sku__icontains=self.q))
        return qs


class ProductionUnitAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = UOM.objects.none()
        production_pk = self.forwarded.get("productions")
        production_item_pk = self.forwarded.get("id")
        if production_pk and production_item_pk:
            production = ProductionsOrder.objects.get(pk=production_pk)
            production_item = production.productions_item.get(pk=production_item_pk)
            product = production_item.product
            qs = UOM.objects.filter(category=product.uom.category, is_active=True)

        if self.q:
            qs = qs.filter(unit__icontains=self.q)

        return qs
