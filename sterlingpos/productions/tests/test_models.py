from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.productions.models import (
    BOM,
    BOMItemDetail,
    BOMOtherCostDetail,
    ProductionsOrder,
)
from sterlingpos.catalogue.models import UOM


class TestBOM(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)

        self.uom = UOM.objects.get(
            account=self.account, unit="pcs", category="units", uom_type="base"
        )
        self.manufactured_product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
            classification="manufactured",
        )
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
        )
        self.product_2 = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        bom = BOM.objects.create(yield_quantity=1, account=self.account,)
        bom.products.add(
            self.manufactured_product, through_defaults={"account": self.account}
        )
        BOMItemDetail.objects.create(
            product=self.product,
            bom=bom,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )

        BOMItemDetail.objects.create(
            product=self.product_2,
            bom=bom,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )

        self.assertEqual(
            bom.__str__(), "{} [{}]".format(bom.code, bom.status),
        )
        self.assertEqual(bom.bom_item.all().count(), 2)
        self.assertEqual(bom.products.all().count(), 1)

    def test_multiple_products_bom(self):
        manufactured_product_2 = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
            classification="manufactured",
        )

        bom = BOM.objects.create(yield_quantity=1, account=self.account,)
        bom.products.add(
            self.manufactured_product, through_defaults={"account": self.account}
        )
        bom.products.add(
            manufactured_product_2, through_defaults={"account": self.account}
        )
        BOMItemDetail.objects.create(
            product=self.product,
            bom=bom,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )

        BOMItemDetail.objects.create(
            product=self.product_2,
            bom=bom,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )
        self.assertEqual(bom.bom_item.all().count(), 2)
        self.assertEqual(bom.products.all().count(), 2)


class TestProductionOrder(TestCase):
    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)

        self.uom = UOM.objects.get(
            account=self.account, unit="pcs", category="units", uom_type="base"
        )
        self.manufactured_product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
            classification="manufactured",
        )
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
        )

        self.bom = BOM.objects.create(yield_quantity=1, account=self.account,)
        self.bom.products.add(
            self.manufactured_product, through_defaults={"account": self.account}
        )
        self.bom_item = BOMItemDetail.objects.create(
            product=self.product,
            bom=self.bom,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )
        self.other_cost = BOMOtherCostDetail.objects.create(
            bom=self.bom, name="other", quantity=1, cost_per_item=1000,
        )
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        productions = ProductionsOrder.objects.create(
            product=self.manufactured_product, bom=self.bom, produced_quantity=1, outlet=self.outlet
        )

        productions.productions_item.create(
            product=self.product,
            quantity=1,
            quantity_unit=self.uom,
            wastage_quantity=0,
            wastage_unit=self.uom,
            unconsumed_quantity=0,
            unconsumed_unit=self.uom,
        )

        productions.productions_other_cost.create(
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )

        self.assertEqual(
            productions.__str__(),
            "{} ({})".format(productions.code, productions.status),
        )
        self.assertEqual(productions.productions_item.all().count(), 1)
        self.assertEqual(productions.productions_other_cost.all().count(), 1)
