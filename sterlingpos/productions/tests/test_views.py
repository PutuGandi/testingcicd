import json

from model_mommy import mommy
from test_plus.test import TestCase

from django.test import RequestFactory
from django.urls import reverse
from django.test.client import Client
from django.contrib.messages.storage.fallback import FallbackStorage
from datetime import datetime

from sterlingpos.core.models import set_current_tenant
from sterlingpos.device.models import DeviceUser
from sterlingpos.catalogue.models import Product
from sterlingpos.productions.models import BOM, ProductionsOrder
from sterlingpos.stocks.models import Stock
from sterlingpos.productions.views import (
    BOMCreateView,
    BOMUpdateView,
    BOMDeactivateJson,
    BOMActivateJson,
    BOMDeleteJson,
    ComponentProductView,
    BOMQuantityView,
    BOMAutoCompleteView,
    ProductCostConversionView,
    BOMYieldQuantityJsonView,
    ProductionsCreateView,
    ProductionUpdateView,
    ProductionCancelledView,
    ProductionStartedView,
    ProductionCompleteView,
    ProductionUnconsumedJsonView,
)


class TestBOMListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make("users.User", email="test@email.com", account=account)
        self.user.set_password("test")
        self.user.save()
        account.owners.get_or_create(account=account, user=self.user)

        set_current_tenant(account)
        self.product_1 = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=account,
        )
        self.product_2 = mommy.make(
            "catalogue.Product",
            name="Product-2",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=account,
        )

        self.bom_1 = mommy.make("productions.BOM", account=account)
        self.bom_1.products.add(self.product_1, through_defaults={"account": account})
        self.bom_2 = mommy.make("productions.BOM", account=account)
        self.bom_2.products.add(self.product_2, through_defaults={"account": account})
        self.url = reverse("productions:bom_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}
        self.client = Client()
        self.client.login(email="test@email.com", password="test")

    def tearDown(self):
        set_current_tenant(None)

    def test_bom_list_json(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content["data"][0]
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 6,
        }
        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])

    def test_bom_list_json_filter(self):
        self.login(email=self.user.email, password="test")
        total_column = 4
        data = {"draw": 6}
        for i in range(0, total_column):
            data.update({"columns[{}][data]".format(i): i})
            data.update({"columns[{}][name]".format(i): ""})
            data.update({"columns[{}][searchable]".format(i): True})
            if i == 2:
                data.update({"columns[{}][orderable]".format(i): False})
            else:
                data.update({"columns[{}][orderable]".format(i): True})
            data.update({"columns[{}][value]".format(i): ""})
            data.update({"columns[{}][regex]".format(i): False})

        data.update(
            {
                "columns[1][data]": 1,
                "columns[1][name]": "",
                "columns[1][searchable]": True,
                "columns[1][orderable]": True,
                "columns[1][search][value]": "Product-2",
                "columns[1][search][regex]": False,
            }
        )
        response = self.get(self.url, data=data, **self.request_header)
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 1,
        }
        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])


class TestBOMBOMCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            is_manage_stock=True,
            uom=self.unit,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.url = reverse("productions:bom_create")

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 0,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.unit.pk,
            "bom_item-0-wastage": 0,
            "bom_item-0-wastage_unit": self.unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": "",
            "bom_item-0-bom": "",
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "",
            "bom_other_cost-0-quantity": 0,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 0,
            "bom_other_cost-0-total_cost": 0,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMCreateView.as_view()(request)

        assert response.status_code == 302
        assert BOM.objects.last().bom_item.all().count() == 1
        assert BOM.objects.last().bom_other_cost.all().count() == 0

    def test_post_with_other_cost(self):
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 0,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.unit.pk,
            "bom_item-0-wastage": 0,
            "bom_item-0-wastage_unit": self.unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": "",
            "bom_item-0-bom": "",
            "has_other_cost": "on",
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "other",
            "bom_other_cost-0-quantity": 3,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 1000,
            "bom_other_cost-0-total_cost": 3000,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMCreateView.as_view()(request)

        assert response.status_code == 302
        assert BOM.objects.last().bom_item.all().count() == 1
        assert BOM.objects.last().bom_other_cost.all().count() == 1

    def test_post_fail(self):
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 0,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": "",
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.unit.pk,
            "bom_item-0-wastage": 0,
            "bom_item-0-wastage_unit": self.unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": "",
            "bom_item-0-bom": "",
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "",
            "bom_other_cost-0-quantity": 0,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 0,
            "bom_other_cost-0-total_cost": 0,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMCreateView.as_view()(request)

        assert response.status_code == 200

    def test_post_other_cost_fail(self):
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 0,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.unit.pk,
            "bom_item-0-wastage": 0,
            "bom_item-0-wastage_unit": self.unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": "",
            "bom_item-0-bom": "",
            "has_other_cost": "on",
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "",
            "bom_other_cost-0-quantity": "",
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": "",
            "bom_other_cost-0-total_cost": 0,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMCreateView.as_view()(request)

        assert response.status_code == 200


class TestBOMUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            is_manage_stock=True,
            uom=self.unit,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.url = reverse("productions:bom_update", kwargs={"pk": self.bom.pk})

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 1,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.component_1.quantity_unit.pk,
            "bom_item-0-wastage": 1,
            "bom_item-0-wastage_unit": self.component_1.wastage_unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost.amount,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": self.bom.bom_item.all().first().pk,
            "bom_item-0-bom": self.bom.pk,
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "other",
            "bom_other_cost-0-quantity": 1,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 1000,
            "bom_other_cost-0-total_cost": 1000,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": self.bom.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMUpdateView.as_view()(request, pk=self.bom.pk)

        assert response.status_code == 302
        assert BOM.objects.last().bom_item.all().count() == 1
        assert BOM.objects.last().bom_other_cost.all().count() == 1

    def test_post_fail(self):
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 1,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": "",
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.component_1.quantity_unit.pk,
            "bom_item-0-wastage": 1,
            "bom_item-0-wastage_unit": self.component_1.wastage_unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost.amount,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": self.bom.bom_item.all().first().pk,
            "bom_item-0-bom": self.bom.pk,
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "other",
            "bom_other_cost-0-quantity": "1",
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 1000,
            "bom_other_cost-0-total_cost": 1000,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": self.bom.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMUpdateView.as_view()(request, pk=self.bom.pk)

        assert response.status_code == 200

    def test_post_formset_other_fail(self):
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 1,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.component_1.quantity_unit.pk,
            "bom_item-0-wastage": 1,
            "bom_item-0-wastage_unit": self.component_1.wastage_unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost.amount,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": self.bom.bom_item.all().first().pk,
            "bom_item-0-bom": self.bom.pk,
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "",
            "bom_other_cost-0-quantity": "",
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 1000,
            "bom_other_cost-0-total_cost": 1000,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": self.bom.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMUpdateView.as_view()(request, pk=self.bom.pk)

        assert response.status_code == 200

    def test_add_new_components(self):
        product_component_2 = mommy.make(
            "catalogue.Product",
            name="Product-X2",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

        other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other_cost",
            quantity=1,
            cost_per_item=1000,
        )
        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 2,
            "bom_item-INITIAL_FORMS": 1,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.component_1.quantity_unit.pk,
            "bom_item-0-wastage": 1,
            "bom_item-0-wastage_unit": self.component_1.wastage_unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost.amount,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": self.bom.bom_item.all().first().pk,
            "bom_item-0-bom": self.bom.pk,
            "bom_item-0-DELETE": "[on]",
            "bom_item-1-product": product_component_2.pk,
            "bom_item-1-quantity": 5,
            "bom_item-1-quantity_unit": product_component_2.uom.pk,
            "bom_item-1-wastage": 1,
            "bom_item-1-wastage_unit": product_component_2.uom.pk,
            "bom_item-1-cost_per_item": product_component_2.cost.amount,
            "bom_item-1-total_cost": 5 * product_component_2.cost.amount,
            "bom_item-1-id": "",
            "bom_item-1-bom": self.bom.pk,
            "bom_other_cost-TOTAL_FORMS": 2,
            "bom_other_cost-INITIAL_FORMS": 1,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": other_cost.name,
            "bom_other_cost-0-quantity": other_cost.quantity,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": other_cost.cost_per_item.amount,
            "bom_other_cost-0-total_cost": 1000,
            "bom_other_cost-0-id": other_cost.pk,
            "bom_other_cost-0-bom": self.bom.pk,
            "bom_other_cost-0-DELETE": "[on]",
            "bom_other_cost-1-name": "name",
            "bom_other_cost-1-quantity": 1,
            "bom_other_cost-1-cost_per_item_1": "IDR",
            "bom_other_cost-1-cost_per_item_0": 1000,
            "bom_other_cost-1-total_cost": 1000,
            "bom_other_cost-1-id": "",
            "bom_other_cost-1-bom": self.bom.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMUpdateView.as_view()(request, pk=self.bom.pk)
        assert response.status_code == 302
        assert BOM.objects.last().bom_item.all().count() == 1
        assert BOM.objects.last().bom_other_cost.all().count() == 1

    def test_post_bom_has_productions(self):
        other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other_cost",
            quantity=1,
            cost_per_item=1000,
        )
        product_component_2 = mommy.make(
            "catalogue.Product",
            name="Product-X2",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )

        productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            product=self.product,
            bom=self.bom,
            produced_quantity=1,
            outlet=mommy.make("outlet.Outlet", account=self.account),
            status="finalized",
        )

        mommy.make(
            "productions.ProductionOrderItemDetail",
            productions=productions,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            productions=productions,
            name="other_cost",
            quantity=1,
            cost_per_item=1000,
        )

        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 2,
            "bom_item-INITIAL_FORMS": 1,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.component_1.quantity_unit.pk,
            "bom_item-0-wastage": 1,
            "bom_item-0-wastage_unit": self.component_1.wastage_unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost.amount,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": self.bom.bom_item.all().first().pk,
            "bom_item-0-bom": self.bom.pk,
            "bom_item-0-DELETE": "[on]",
            "bom_item-1-product": product_component_2.pk,
            "bom_item-1-quantity": 5,
            "bom_item-1-quantity_unit": product_component_2.uom.pk,
            "bom_item-1-wastage": 1,
            "bom_item-1-wastage_unit": product_component_2.uom.pk,
            "bom_item-1-cost_per_item": product_component_2.cost.amount,
            "bom_item-1-total_cost": 5 * product_component_2.cost.amount,
            "bom_item-1-id": "",
            "bom_item-1-bom": self.bom.pk,
            "bom_other_cost-TOTAL_FORMS": 2,
            "bom_other_cost-INITIAL_FORMS": 1,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": other_cost.name,
            "bom_other_cost-0-quantity": other_cost.quantity,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 1000,
            "bom_other_cost-0-total_cost": 1000,
            "bom_other_cost-0-id": self.bom.bom_other_cost.all().first().pk,
            "bom_other_cost-0-bom": self.bom.pk,
            "bom_other_cost-0-DELETE": "[on]",
            "bom_other_cost-1-name": "other name",
            "bom_other_cost-1-quantity": 1,
            "bom_other_cost-1-cost_per_item_1": "IDR",
            "bom_other_cost-1-cost_per_item_0": 1000,
            "bom_other_cost-1-total_cost": 1000,
            "bom_other_cost-1-id": "",
            "bom_other_cost-1-bom": self.bom.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMUpdateView.as_view()(request, pk=self.bom.pk)
        assert response.status_code == 302
        assert BOM.objects.last().bom_item.all().count() == 1
        assert BOM.objects.last().bom_other_cost.all().count() == 1

    def test_post_update_bom_has_productions(self):
        other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other_cost",
            quantity=1,
            cost_per_item=1000,
        )

        productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            produced_quantity=1,
            outlet=mommy.make("outlet.Outlet", account=self.account),
            status="finalized",
        )

        mommy.make(
            "productions.ProductionOrderItemDetail",
            productions=productions,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            productions=productions,
            name="other_cost",
            quantity=1,
            cost_per_item=1000,
        )

        params = {
            "products": [self.product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 1,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 2,
            "bom_item-0-quantity_unit": self.component_1.quantity_unit.pk,
            "bom_item-0-wastage": 1,
            "bom_item-0-wastage_unit": self.component_1.wastage_unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost.amount,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": self.bom.bom_item.all().first().pk,
            "bom_item-0-bom": self.bom.pk,
            "bom_other_cost-TOTAL_FORMS": 2,
            "bom_other_cost-INITIAL_FORMS": 1,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": other_cost.name,
            "bom_other_cost-0-quantity": other_cost.quantity,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 1000,
            "bom_other_cost-0-total_cost": 1000,
            "bom_other_cost-0-id": self.bom.bom_other_cost.all().first().pk,
            "bom_other_cost-0-bom": self.bom.pk,
            "bom_other_cost-0-DELETE": "[on]",
            "bom_other_cost-1-name": "other name",
            "bom_other_cost-1-quantity": 1,
            "bom_other_cost-1-cost_per_item_1": "IDR",
            "bom_other_cost-1-cost_per_item_0": 1000,
            "bom_other_cost-1-total_cost": 1000,
            "bom_other_cost-1-id": "",
            "bom_other_cost-1-bom": self.bom.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMUpdateView.as_view()(request, pk=self.bom.pk)
        assert response.status_code == 302
        assert BOM.objects.last().bom_item.all().count() == 1
        assert BOM.objects.last().bom_other_cost.all().count() == 1

    def test_add_multiple_product_bom(self):
        product = mommy.make(
            "catalogue.Product",
            name="Product-2",
            is_manage_stock=True,
            uom=self.unit,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )
        params = {
            "products": [self.product.pk, product.pk],
            "code": "BOM-1",
            "yield_quantity": 1,
            "bom_item-TOTAL_FORMS": 1,
            "bom_item-INITIAL_FORMS": 1,
            "bom_item-MIN_NUM_FORMS": 1,
            "bom_item-MAX_NUM_FORMS": 1000,
            "bom_item-0-product": self.product_component.pk,
            "bom_item-0-quantity": 5,
            "bom_item-0-quantity_unit": self.component_1.quantity_unit.pk,
            "bom_item-0-wastage": 1,
            "bom_item-0-wastage_unit": self.component_1.wastage_unit.pk,
            "bom_item-0-cost_per_item": self.product_component.cost.amount,
            "bom_item-0-total_cost": 5 * self.product_component.cost.amount,
            "bom_item-0-id": self.bom.bom_item.all().first().pk,
            "bom_item-0-bom": self.bom.pk,
            "bom_other_cost-TOTAL_FORMS": 1,
            "bom_other_cost-INITIAL_FORMS": 0,
            "bom_other_cost-MIN_NUM_FORMS": 1,
            "bom_other_cost-MAX_NUM_FORMS": 1000,
            "bom_other_cost-0-name": "other",
            "bom_other_cost-0-quantity": 1,
            "bom_other_cost-0-cost_per_item_1": "IDR",
            "bom_other_cost-0-cost_per_item_0": 1000,
            "bom_other_cost-0-total_cost": 1000,
            "bom_other_cost-0-id": "",
            "bom_other_cost-0-bom": self.bom.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMUpdateView.as_view()(request, pk=self.bom.pk)

        assert response.status_code == 302
        assert BOM.objects.last().bom_item.all().count() == 1
        assert BOM.objects.last().bom_other_cost.all().count() == 1


class TestBOMDeactivateJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.url = reverse("productions:bom_deactivate")

    def tearDown(self):
        set_current_tenant(None)

    def test_post_ajax(self):
        params = {"id": self.bom.pk}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMDeactivateJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_post_ajax_fail(self):
        params = {"id": "asd"}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMDeactivateJson.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestBOMActivateJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.url = reverse("productions:bom_active")

    def tearDown(self):
        set_current_tenant(None)

    def test_post_ajax(self):
        params = {"id": self.bom.pk}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMActivateJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_post_ajax_fail(self):
        params = {"id": "asd"}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMActivateJson.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestBOMDeleteJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.url = reverse("productions:bom_delete")

    def tearDown(self):
        set_current_tenant(None)

    def test_post_ajax(self):
        params = {"id": self.bom.pk}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMDeleteJson.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_post_ajax_fail(self):
        params = {"id": "asd"}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMDeleteJson.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestComponentProductView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.url = reverse("productions:get_components")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {"bom": self.bom.pk}
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ComponentProductView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_fail(self):
        params = {"bom": "asd"}
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ComponentProductView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestBOMQuantityView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.other_cost_1 = mommy.make(
            "productions.BOMOtherCostDetail",
            bom=self.bom,
            name="others",
            quantity=1,
            cost_per_item=1000,
        )

        self.url = reverse("productions:get_bom_quantity")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "bom": self.bom.pk,
            "produced_quantity": 2,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMQuantityView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_fail(self):
        params = {
            "bom": 10,
            "produced_quantity": 2,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMQuantityView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestBOMAutoCompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.other_cost_1 = mommy.make(
            "productions.BOMOtherCostDetail",
            bom=self.bom,
            name="others",
            quantity=1,
            cost_per_item=1000,
        )

        self.url = reverse("productions:bom_autocomplete")

    def tearDown(self):
        set_current_tenant(None)

    def test_autocomplete_success(self):
        expected_result = {
            "autocomplete": "{} [{}]".format(self.bom.code, self.bom.status)
        }
        forward_data = {"product": self.product.pk}
        params = {"q": "Product-1", "forward": json.dumps(forward_data)}
        request = self.factory.get(self.url, data=params)
        request.user = self.user

        response = BOMAutoCompleteView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        data = response_data["results"]
        self.assertEqual(data[0]["text"], expected_result["autocomplete"])


class TestProductCostConversionView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.other_unit = self.account.uom_set.filter(
            category=self.unit.category
        ).first()

        self.product_component = mommy.make(
            "catalogue.Product",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.url = reverse("productions:get_product_cost")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "product": self.product_component.pk,
            "unit": self.other_unit.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductCostConversionView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_fail(self):
        params = {
            "product": 10,
            "unit": self.other_unit.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductCostConversionView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestBOMYieldQuantityJsonView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.other_cost_1 = mommy.make(
            "productions.BOMOtherCostDetail",
            bom=self.bom,
            name="others",
            quantity=1,
            cost_per_item=1000,
        )

        self.url = reverse("productions:get_yield_quantity")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "bom": self.bom.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = BOMYieldQuantityJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestProductionListJson(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )

        self.other_cost_1 = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")

        self.productions_1 = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
        )

        self.productions_component_1 = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions_1,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost_1 = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions_1,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.productions_2 = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=2,
        )

        self.productions_component_2 = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions_1,
            product=self.product_component,
            quantity=2,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost_2 = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions_1,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.url = reverse("productions:production_data")
        self.request_header = {"extra": {"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}}

    def tearDown(self):
        set_current_tenant(None)

    def test_productions_list_json(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url, **self.request_header)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        data = content["data"][0]
        expected_result_valid = {
            "recordsTotal": 2,
            "recordsFiltered": 2,
            "columnsTotal": 7,
        }
        self.assertEqual(content["recordsTotal"], expected_result_valid["recordsTotal"])
        self.assertEqual(
            content["recordsFiltered"], expected_result_valid["recordsFiltered"]
        )
        self.assertEqual(len(data), expected_result_valid["columnsTotal"])


class TestProductionsCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.url = reverse("productions:production_create")

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "product": self.product.pk,
            "bom": self.bom.pk,
            "code": "PROD-1",
            "produced_quantity": 1,
            "outlet": self.outlet.pk,
            "notes": "",
            "productions_other_cost-TOTAL_FORMS": self.bom.bom_other_cost.all().count(),
            "productions_other_cost-INITIAL_FORMS": 0,
            "productions_other_cost-MIN_NUM_FORMS": 1,
            "productions_other_cost-MAX_NUM_FORMS": 1000,
            "productions_other_cost-0-name": self.other_cost.name,
            "productions_other_cost-0-quantity": self.other_cost.quantity,
            "productions_other_cost-0-cost_per_item_1": ["IDR"],
            "productions_other_cost-0-cost_per_item_0": self.other_cost.cost_per_item.amount,
            "productions_other_cost-0-total_cost": self.other_cost.total_cost,
            "productions_other_cost-0-id": "",
            "productions_other_cost-0-productions": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionsCreateView.as_view()(request)

        assert response.status_code == 302
        assert ProductionsOrder.objects.last().productions_item.all().count() == 1
        assert ProductionsOrder.objects.last().productions_other_cost.all().count() == 1

    def test_post_fail(self):
        params = {
            "product": self.product.pk,
            "bom": self.bom.pk,
            "code": "PROD-1",
            "produced_quantity": 1,
            "outlet": "",
            "notes": "",
            "productions_other_cost-TOTAL_FORMS": self.bom.bom_other_cost.all().count(),
            "productions_other_cost-INITIAL_FORMS": 0,
            "productions_other_cost-MIN_NUM_FORMS": 1,
            "productions_other_cost-MAX_NUM_FORMS": 1000,
            "productions_other_cost-0-name": self.other_cost.name,
            "productions_other_cost-0-quantity": self.other_cost.quantity,
            "productions_other_cost-0-cost_per_item_1": ["IDR"],
            "productions_other_cost-0-cost_per_item_0": self.other_cost.cost_per_item.amount,
            "productions_other_cost-0-total_cost": self.other_cost.total_cost,
            "productions_other_cost-0-id": "",
            "productions_other_cost-0-productions": "",
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionsCreateView.as_view()(request)

        assert response.status_code == 200


class TestProductionDetailView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.component_stock, _ = Stock.objects.get_or_create(
            product=self.product_component, outlet=self.outlet, account=self.account
        )
        self.component_stock.set_balance(10, unit=self.unit)

        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
        )

        self.productions_component = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )

        self.productions_complete = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
            status="completed",
        )

        self.productions_componen_completet = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions_complete,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost_complete = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions_complete,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )

        self.productions_not_enough_stock = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
        )

        self.productions_componen_completet = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions_not_enough_stock,
            product=self.product_component,
            quantity=1000,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost_complete = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions_not_enough_stock,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )

        self.url = reverse(
            "productions:production_detail", kwargs={"pk": self.productions.pk}
        )
        self.url_complete = reverse(
            "productions:production_detail", kwargs={"pk": self.productions_complete.pk}
        )
        self.url_not_enough_stock = reverse(
            "productions:production_detail",
            kwargs={"pk": self.productions_not_enough_stock.pk},
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_url_view_success_complete(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url_complete)
        self.assertEqual(response.status_code, 200)

    def test_url_view_success_not_enough_stock(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url_not_enough_stock)
        self.assertEqual(response.status_code, 200)


class TestProductionUpdateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.component_stock, _ = Stock.objects.get_or_create(
            product=self.product_component, outlet=self.outlet, account=self.account
        )
        self.component_stock.set_balance(10, unit=self.unit)

        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
        )

        self.productions_component = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )
        self.url = reverse(
            "productions:production_update", kwargs={"pk": self.productions.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "product": self.product.pk,
            "bom": self.bom.pk,
            "code": self.productions.code,
            "produced_quantity": self.productions.produced_quantity,
            "outlet": self.productions.outlet.pk,
            "notes": [""],
            "productions_other_cost-TOTAL_FORMS": 2,
            "productions_other_cost-INITIAL_FORMS": 1,
            "productions_other_cost-MIN_NUM_FORMS": 1,
            "productions_other_cost-MAX_NUM_FORMS": 1000,
            "productions_other_cost-0-name": self.productions_other_cost.name,
            "productions_other_cost-0-quantity": self.productions_other_cost.quantity,
            "productions_other_cost-0-cost_per_item_1": "IDR",
            "productions_other_cost-0-cost_per_item_0": self.productions_other_cost.cost_per_item.amount,
            "productions_other_cost-0-total_cost": self.productions_other_cost.total_cost,
            "productions_other_cost-0-id": self.productions_other_cost.pk,
            "productions_other_cost-0-productions": self.productions.pk,
            "productions_other_cost-0-DELETE": "on",
            "productions_other_cost-1-name": "other cost",
            "productions_other_cost-1-quantity": 1,
            "productions_other_cost-1-cost_per_item_1": "IDR",
            "productions_other_cost-1-cost_per_item_0": 300.00,
            "productions_other_cost-1-total_cost": 300.00,
            "productions_other_cost-1-id": "",
            "productions_other_cost-1-productions": self.productions.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionUpdateView.as_view()(request, pk=self.productions.pk)

        assert response.status_code == 302
        assert ProductionsOrder.objects.last().productions_item.all().count() == 1
        assert ProductionsOrder.objects.last().productions_other_cost.all().count() == 1

    def test_post_fail(self):
        params = {
            "product": self.product.pk,
            "bom": self.bom.pk,
            "code": self.productions.code,
            "produced_quantity": self.productions.produced_quantity,
            "outlet": self.productions.outlet.pk,
            "notes": [""],
            "productions_other_cost-TOTAL_FORMS": 2,
            "productions_other_cost-INITIAL_FORMS": 1,
            "productions_other_cost-MIN_NUM_FORMS": 1,
            "productions_other_cost-MAX_NUM_FORMS": 1000,
            "productions_other_cost-0-name": self.productions_other_cost.name,
            "productions_other_cost-0-quantity": self.productions_other_cost.quantity,
            "productions_other_cost-0-cost_per_item_1": "IDR",
            "productions_other_cost-0-cost_per_item_0": self.productions_other_cost.cost_per_item.amount,
            "productions_other_cost-0-total_cost": self.productions_other_cost.total_cost,
            "productions_other_cost-0-id": self.productions_other_cost.pk,
            "productions_other_cost-0-productions": self.productions.pk,
            "productions_other_cost-0-DELETE": "on",
            "productions_other_cost-1-name": "",
            "productions_other_cost-1-quantity": "",
            "productions_other_cost-1-cost_per_item_1": "IDR",
            "productions_other_cost-1-cost_per_item_0": 300.00,
            "productions_other_cost-1-total_cost": 300.00,
            "productions_other_cost-1-id": "",
            "productions_other_cost-1-productions": self.productions.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionUpdateView.as_view()(request, pk=self.productions.pk)

        assert response.status_code == 200


class TestProductionCancelledView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.component_stock, _ = Stock.objects.get_or_create(
            product=self.product_component, outlet=self.outlet, account=self.account
        )
        self.component_stock.set_balance(10, unit=self.unit)

        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
        )

        self.productions_component = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )
        self.url = reverse("productions:production_cancelled")

    def tearDown(self):
        set_current_tenant(None)

    def test_post_ajax(self):
        params = {"id": self.productions.pk}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionCancelledView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_post_ajax_fail(self):
        params = {"id": "asd"}
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionCancelledView.as_view()(request)
        self.assertEqual(response.status_code, 404)


class TestProductionStartedView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.component_stock, _ = Stock.objects.get_or_create(
            product=self.product_component, outlet=self.outlet, account=self.account
        )
        self.component_stock.set_balance(10, unit=self.unit)

        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
        )

        self.productions_component = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )

        self.productions_not_enough_stock = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
        )

        self.productions_component_not_enough_stock = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions_not_enough_stock,
            product=self.product_component,
            quantity=1000,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost_not_enough_stock = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions_not_enough_stock,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )
        self.url = reverse("productions:production_start")

    def tearDown(self):
        set_current_tenant(None)

    def test_post_ajax(self):
        params = {
            "started": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "id": self.productions.pk,
        }
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionStartedView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_post_ajax_not_enough_stock(self):
        params = {
            "started": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "id": self.productions_not_enough_stock.pk,
        }
        request = self.factory.post(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionStartedView.as_view()(request)
        self.assertEqual(response.status_code, 200)


class TestProductionCompleteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
            uom=self.unit,
        )
        self.product_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.component_stock, _ = Stock.objects.get_or_create(
            product=self.product_component, outlet=self.outlet, account=self.account
        )
        self.component_stock.set_balance(100, unit=self.unit)

        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
            status="in_production",
        )

        self.productions_component = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions,
            product=self.product_component,
            quantity=5,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )

        self.url = reverse(
            "productions:production_complete", kwargs={"pk": self.productions.pk}
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_url_view_success(self):
        self.login(email=self.user.email, password="test")
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        params = {
            "finished": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "productions_item-TOTAL_FORMS": self.productions.productions_item.all().count(),
            "productions_item-INITIAL_FORMS": self.productions.productions_item.all().count(),
            "productions_item-MIN_NUM_FORMS": 0,
            "productions_item-MAX_NUM_FORMS": 1000,
            "productions_item-0-product": "{} {}".format(
                self.productions_component.product.catalogue.name,
                self.productions_component.product.name,
            ),
            "productions_item-0-quantity": "{} {}".format(
                self.productions_component.quantity,
                self.productions_component.quantity_unit.unit,
            ),
            "productions_item-0-wastage_quantity": 0,
            "productions_item-0-wastage_unit": self.unit.pk,
            "productions_item-0-unconsumed_quantity": 1,
            "productions_item-0-unconsumed_unit": self.unit.pk,
            "productions_item-0-cost_per_item": self.productions_component.product.cost.amount,
            "productions_item-0-total_cost": self.productions_component.total_cost,
            "productions_item-0-id": self.productions_component.pk,
            "productions_item-0-productions": self.productions.pk,
            "productions_other_cost-TOTAL_FORMS": self.productions.productions_other_cost.all().count(),
            "productions_other_cost-INITIAL_FORMS": self.productions.productions_other_cost.all().count(),
            "productions_other_cost-MIN_NUM_FORMS": 1,
            "productions_other_cost-MAX_NUM_FORMS": 1000,
            "productions_other_cost-0-name": self.productions_other_cost.name,
            "productions_other_cost-0-quantity": self.productions_other_cost.quantity,
            "productions_other_cost-0-cost_per_item_1": "IDR",
            "productions_other_cost-0-cost_per_item_0": self.productions_other_cost.cost_per_item.amount,
            "productions_other_cost-0-total_cost": self.productions_other_cost.total_cost,
            "productions_other_cost-0-id": self.productions_other_cost.pk,
            "productions_other_cost-0-productions": self.productions.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionCompleteView.as_view()(request, pk=self.productions.pk)

        assert response.status_code == 302
        assert ProductionsOrder.objects.last().productions_item.all().count() == 1
        assert ProductionsOrder.objects.last().productions_other_cost.all().count() == 1
        assert self.product_stock.balance == 1

    def test_post_faild(self):
        params = {
            "finished": datetime.today().strftime("%d/%m/%Y %H:%M"),
            "productions_item-TOTAL_FORMS": self.productions.productions_item.all().count(),
            "productions_item-INITIAL_FORMS": self.productions.productions_item.all().count(),
            "productions_item-MIN_NUM_FORMS": 0,
            "productions_item-MAX_NUM_FORMS": 1000,
            "productions_item-0-product": "{} {}".format(
                self.productions_component.product.catalogue.name,
                self.productions_component.product.name,
            ),
            "productions_item-0-quantity": "{} {}".format(
                self.productions_component.quantity,
                self.productions_component.quantity_unit.unit,
            ),
            "productions_item-0-wastage_quantity": 0,
            "productions_item-0-wastage_unit": self.unit.pk,
            "productions_item-0-unconsumed_quantity": 1000,
            "productions_item-0-unconsumed_unit": self.unit.pk,
            "productions_item-0-cost_per_item": self.productions_component.product.cost.amount,
            "productions_item-0-total_cost": self.productions_component.total_cost,
            "productions_item-0-id": self.productions_component.pk,
            "productions_item-0-productions": self.productions.pk,
            "productions_other_cost-TOTAL_FORMS": self.productions.productions_other_cost.all().count(),
            "productions_other_cost-INITIAL_FORMS": self.productions.productions_other_cost.all().count(),
            "productions_other_cost-MIN_NUM_FORMS": 1,
            "productions_other_cost-MAX_NUM_FORMS": 1000,
            "productions_other_cost-0-name": self.productions_other_cost.name,
            "productions_other_cost-0-quantity": self.productions_other_cost.quantity,
            "productions_other_cost-0-cost_per_item_1": "IDR",
            "productions_other_cost-0-cost_per_item_0": self.productions_other_cost.cost_per_item.amount,
            "productions_other_cost-0-total_cost": self.productions_other_cost.total_cost,
            "productions_other_cost-0-id": self.productions_other_cost.pk,
            "productions_other_cost-0-productions": self.productions.pk,
        }

        request = self.factory.post(self.url, data=params)
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionCompleteView.as_view()(request, pk=self.productions.pk)

        assert response.status_code == 200


class TestProductionUnconsumedJsonView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.account = mommy.make("accounts.Account", name="account-1")
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.user.set_password("test")
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)
        set_current_tenant(self.account)

        self.unit = self.account.uom_set.first()
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.product = mommy.make(
            "catalogue.Product",
            name="Product-1",
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="1"),
            account=self.account,
            uom=self.unit,
        )
        self.product_stock, _ = Stock.objects.get_or_create(
            product=self.product, outlet=self.outlet, account=self.account
        )

        self.product_component = mommy.make(
            "catalogue.Product",
            name="Product-X",
            catalogue=mommy.make("catalogue.Catalogue", account=self.account, name="X"),
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.component_stock, _ = Stock.objects.get_or_create(
            product=self.product_component, outlet=self.outlet, account=self.account
        )
        self.component_stock.set_balance(100, unit=self.unit)

        self.bom = mommy.make(
            "productions.BOM", yield_quantity=1, account=self.account,
        )
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.component_1 = mommy.make(
            "productions.BOMItemDetail",
            bom=self.bom,
            account=self.account,
            product=self.product_component,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.other_cost = mommy.make(
            "productions.BOMOtherCostDetail",
            account=self.account,
            bom=self.bom,
            name="other",
            quantity=1,
            cost_per_item=1,
        )

        self.productions = mommy.make(
            "productions.ProductionsOrder",
            account=self.account,
            bom=self.bom,
            product=self.product,
            outlet=self.outlet,
            produced_quantity=1,
            status="in_production",
        )

        self.productions_component = mommy.make(
            "productions.ProductionOrderItemDetail",
            account=self.account,
            productions=self.productions,
            product=self.product_component,
            quantity=5,
            quantity_unit=self.unit,
            wastage_quantity=0,
            wastage_unit=self.unit,
            unconsumed_quantity=0,
            unconsumed_unit=self.unit,
        )

        self.productions_other_cost = mommy.make(
            "productions.ProductionsOrderOtherCostDetail",
            account=self.account,
            productions=self.productions,
            name=self.other_cost.name,
            quantity=self.other_cost.quantity,
            cost_per_item=self.other_cost.cost_per_item.amount,
        )

        self.url = reverse("productions:get_total_cost")

    def tearDown(self):
        set_current_tenant(None)

    def test_get_ajax(self):
        params = {
            "production_pk": self.productions.pk,
            "production_detail_pk": self.productions_component.pk,
            "unconsumed": 1,
            "unconsumed_unit": self.unit.pk,
        }
        request = self.factory.get(
            self.url, data=params, **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"}
        )
        request.user = self.user
        setattr(request, "session", "session")
        messages = FallbackStorage(request)
        setattr(request, "_messages", messages)

        response = ProductionUnconsumedJsonView.as_view()(request)
        self.assertEqual(response.status_code, 200)
