from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.productions.models import BOM, ProductionsOrder

from sterlingpos.productions.forms import (
    BOMForm,
    BOMItemDetailForm,
    BOMOtherCostDetailForm,
    ProductionsOrderForm,
    ProductionOrderUpdateForm,
    ProductionsOrderOtherCostDetailForm,
)
from sterlingpos.catalogue.models import Product, UOM


class BOMFormTest(TestCase):
    def setUp(self):
        self.form_class = BOMForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.uom = UOM.objects.get(
            account=self.account, unit="pcs", category="units", uom_type="base"
        )
        self.manufactured_product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
        )

        self.manufactured_product_2 = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            name="P-2",
            uom=self.uom,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_add_bom(self):
        data = {
            "code": "BOM-1",
            "products": [self.manufactured_product.pk],
            "yield_quantity": 2,
        }
        form = BOMForm(data)
        self.assertTrue(form.is_valid(), form.errors)
        bom = form.save()
        self.assertEqual(bom.yield_quantity, 2)
        self.assertEqual(bom.status, BOM.STATUS.active)

    def test_clean_yield_quantity_invalid(self):
        data = {
            "product": self.manufactured_product.pk,
            "yield_quantity": -1,
        }
        form = BOMForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn("yield_quantity", form.errors)

    def test_clean_yield_quantity_required(self):
        data = {
            "products": [self.manufactured_product.pk],
            "yield_quantity": "0",
        }
        form = BOMForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn("yield_quantity", form.errors)

    def test_clean_yield_quantity_invalid_format(self):
        data = {
            "products": [self.manufactured_product.pk],
            "yield_quantity": "ASD",
        }
        form = BOMForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn("yield_quantity", form.errors)

    def test_clean_product_invalid(self):
        data = {
            "products": [],
            "code": "BOM-123",
            "yield_quantity": 2,
        }
        form = BOMForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn("products", form.errors)


class BOMItemDetailFormTest(TestCase):
    def setUp(self):
        self.form_class = BOMItemDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.uom = UOM.objects.get(
            account=self.account, unit="pcs", category="units", uom_type="base"
        )
        self.manufactured_product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )
        self.bom = BOM.objects.create(yield_quantity=1, account=self.account)
        self.bom.products.add(
            self.manufactured_product, through_defaults={"account": self.account}
        )
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_create_bom_item_detail(self):
        data = {
            "product": self.product.pk,
            "quantity": 3,
            "quantity_unit": self.uom.pk,
            "wastage": 2,
            "wastage_unit": self.uom.pk,
            "bom_id": self.bom.pk,
        }
        form = BOMItemDetailForm(data=data)
        self.assertTrue(form.is_valid(), form.errors)

    def test_clean_quantity_invalid(self):
        data = {
            "product": self.product.pk,
            "quantity": -1,
            "wastage": 1,
            "quantity_unit": self.uom.pk,
            "wastage_unit": self.uom.pk,
            "bom": self.bom.pk,
        }
        form = BOMItemDetailForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertIn("quantity", form.errors)

    def test_clean_wastage_invalid(self):
        data = {
            "product": self.product.pk,
            "quantity": 2,
            "wastage": -1,
            "quantity_unit": self.uom.pk,
            "wastage_unit": self.uom.pk,
            "bom": self.bom.pk,
        }
        form = BOMItemDetailForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertIn("wastage", form.errors)


class BOMOtherCostDetailFormTest(TestCase):
    def setUp(self):
        self.form_class = BOMOtherCostDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )
        self.bom = BOM.objects.create(yield_quantity=1, account=self.account)
        self.bom.products.add(self.product, through_defaults={"account": self.account})

    def tearDown(self):
        set_current_tenant(None)

    def test_invalid_create_bom_other_cost_detail(self):
        prepare_bom = mommy.prepare(
            "productions.BOMOtherCostDetail",
            bom=self.bom,
            quantity=2,
            cost_per_item=1000,
        )
        bom_other_cost_data = prepare_bom.__dict__
        data = {
            "name": "Cost-1",
            "quantity": -1,
            "cost_per_item": bom_other_cost_data["cost_per_item"],
            "bom": self.bom.pk,
        }
        form = BOMOtherCostDetailForm(data=data)
        self.assertFalse(form.is_valid(), form.errors)
        self.assertIn("quantity", form.errors)


class TestProductionsOrderForm(TestCase):
    def setUp(self):
        self.form_class = ProductionsOrderForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )
        self.component_product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = BOM.objects.create(yield_quantity=1, account=self.account)
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.bom.bom_item.create(
            product=self.component_product,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")

    def tearDown(self):
        set_current_tenant(None)

    def add_productions(self):
        data = {
            "bom": self.bom.pk,
            "code": "PROD-1",
            "produced_quantity": 1,
            "outlet": self.outlet.pk,
            "notes": "",
        }
        form = ProductionsOrderForm(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        productions = form.save()
        self.assertEqual(productions.produced_quantity, 1)
        self.assertEqual(productions.status, ProductionsOrder.STATUS.finalized)

    def test_clean_produced_quantity_fail(self):
        data = {
            "bom": self.bom.pk,
            "code": "PROD-1",
            "produced_quantity": -1,
            "outlet": self.outlet.pk,
            "notes": "",
        }
        form = ProductionsOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertIn("produced_quantity", form.errors)

    def test_clean_produced_quantity_required(self):
        data = {
            "bom": self.bom.pk,
            "code": "PROD-1",
            "produced_quantity": 0,
            "outlet": self.outlet.pk,
            "notes": "",
        }
        form = ProductionsOrderForm(data=data)
        self.assertFalse(form.is_valid())
        self.assertIn("produced_quantity", form.errors)


class TestProductionsOrderOtherCostDetailForm(TestCase):
    def setUp(self):
        self.form_class = ProductionsOrderOtherCostDetailForm
        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.user = mommy.make(
            "users.User", email="test@email.com", account=self.account
        )
        self.unit = self.account.uom_set.first()

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            classification=Product.PRODUCT_CLASSIFICATION.manufactured,
            account=self.account,
        )
        self.component_product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.unit,
            account=self.account,
        )
        self.bom = BOM.objects.create(yield_quantity=1, account=self.account)
        self.bom.products.add(self.product, through_defaults={"account": self.account})
        self.bom.bom_item.create(
            product=self.component_product,
            quantity=1,
            quantity_unit=self.unit,
            wastage=0,
            wastage_unit=self.unit,
        )
        self.outlet = mommy.make("outlet.Outlet", account=self.account, name="A")
        self.productions = mommy.make(
            "productions.ProductionsOrder",
            bom=self.bom,
            product=self.product,
            produced_quantity=1,
            outlet=self.outlet,
            account=self.account,
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_add_other_cost(self):
        data = {
            "name": "Cost-1",
            "quantity": 2,
            "cost_per_item": -1,
            "productions": self.productions.pk,
        }
        form = ProductionsOrderOtherCostDetailForm(data=data)
        self.assertFalse(form.is_valid(), form.errors)
        self.assertIn("cost_per_item", form.errors)
