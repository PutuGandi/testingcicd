from test_plus.test import TestCase
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.stocks.models import StockTransaction
from sterlingpos.productions.models import (
    BOM,
    BOMItemDetail,
    BOMOtherCostDetail,
    ProductionsOrder,
)
from sterlingpos.productions.tasks import create_production_order

from sterlingpos.catalogue.models import UOM


class MockProductOrder:

    def __init__(self, product):
        self.product = product


class TestBOMItemDetailManager(TestCase):

    def setUp(self):
        self.account = mommy.make(
            "accounts.Account", name="account-1")
        set_current_tenant(self.account)

        self.outlet = mommy.make(
            "outlet.Outlet",
            name="A", account=self.account)

        self.uom = UOM.objects.get(
            account=self.account,
            unit="pcs", category="units", uom_type="base"
        )
        self.manufactured_product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
            classification="manufactured",
        )

        self.product = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
        )
        self.product_2 = mommy.make(
            "catalogue.Product",
            is_manage_stock=True,
            uom=self.uom,
            account=self.account,
        )

        bom = BOM.objects.create(
            yield_quantity=1,
            account=self.account,
        )
        bom.products.add(
            self.manufactured_product,
            through_defaults={"account": self.account}
        )

        self.bom_item_1 = BOMItemDetail.objects.create(
            product=self.product,
            bom=bom,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )

        self.bom_item_2 = BOMItemDetail.objects.create(
            product=self.product_2,
            bom=bom,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )

        mocked_product_order = MockProductOrder(
            product=self.manufactured_product,
        )
        create_production_order(
            mocked_product_order,
            self.outlet, 1
        )

    def tearDown(self):
        set_current_tenant(None)

    def test_bom_usage(self):
        self.assertEqual(
            StockTransaction.objects.filter(
                transaction_type=StockTransaction.TRANSACTION_TYPE.productions,
                stock__product=self.product,
            ).count(), 1)
        self.assertEqual(
            BOMItemDetail.objects.bom_usage().get(
                pk=self.bom_item_1.pk
            ).stock_usage, 1,
            BOMItemDetail.objects.bom_usage().query.__str__()
        )
        # self.assertEqual(
        #     BOMItemDetail.objects.bom_usage().get(
        #         pk=self.bom_item_1.pk
        #     ).last_stock, -1,
        #     BOMItemDetail.objects.bom_usage().query.__str__()
        # )

        self.assertEqual(
            BOMItemDetail.objects.bom_usage().get(
                pk=self.bom_item_2.pk
            ).stock_usage, 1
        )
        # self.assertEqual(
        #     BOMItemDetail.objects.bom_usage().get(
        #         pk=self.bom_item_2.pk
        #     ).last_stock, -1
        # )
