import csv
import os
from test_plus.test import TestCase

from model_mommy import mommy

from django.core.files.uploadedfile import SimpleUploadedFile

from sterlingpos.core.models import set_current_tenant
from sterlingpos.core.import_export import SterlingImportForm
from sterlingpos.productions.resources import BOMResource
from sterlingpos.productions.models import BOM


class BOMResourceTest(TestCase):
    def setUp(self):
        self.form_class = SterlingImportForm
        self.resource = BOMResource()
        self.headers = BOMResource.Meta.fields

        self.account = mommy.make("accounts.Account", name="account-1")
        set_current_tenant(self.account)
        self.uom = self.account.uom_set.filter(
            unit="pcs", category="units", uom_type="base"
        ).first()
        self.manufactured_product_1 = mommy.make(
            "catalogue.Product",
            sku="SKU-01",
            classification="manufactured",
            uom=self.uom,
            is_manage_stock=True,
            account=self.account,
        )
        self.manufactured_product_2 = mommy.make(
            "catalogue.Product",
            sku="SKU-02",
            classification="manufactured",
            uom=self.uom,
            is_manage_stock=True,
            account=self.account,
        )

        self.manufactured_product_3 = mommy.make(
            "catalogue.Product",
            sku="SKU-03",
            classification="manufactured",
            uom=self.uom,
            is_manage_stock=True,
            account=self.account,
        )
        self.manufactured_product_4 = mommy.make(
            "catalogue.Product",
            sku="SKU-04",
            classification="manufactured",
            uom=self.uom,
            is_manage_stock=True,
            account=self.account,
        )

        self.product_component_1 = mommy.make(
            "catalogue.Product",
            sku="SKU-COM-01",
            name="Product Component 1",
            uom=self.uom,
            account=self.account,
            is_manage_stock=True,
        )
        self.product_component_2 = mommy.make(
            "catalogue.Product",
            sku="SKU-COM-02",
            name="Product Component 2",
            uom=self.uom,
            account=self.account,
            is_manage_stock=True,
        )
        self.product_component_3 = mommy.make(
            "catalogue.Product",
            sku="SKU-COM-03",
            name="Product Component 3",
            uom=self.uom,
            account=self.account,
            is_manage_stock=True,
        )

        raw_items = [
            ["SKU-01", "BOM-01", "1", "SKU-COM-01", "Product Component 1", "1", "pcs"],
            ["SKU-01", "BOM-01", "1", "SKU-COM-02", "Product Component 2", "1", "pcs"],
            ["SKU-02", "BOM-02", "3", "SKU-COM-01", "Product Component 1", "3", "pcs"],
            ["SKU-02", "BOM-02", "3", "SKU-COM-03", "Product Component 3", "1", "pcs"],
        ]
        with open("bom_import_test.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "code",
                    "yield_quantity",
                    "sku_component",
                    "product_component",
                    "quantity_to_consumed",
                    "unit_to_consumed",
                ]
            )
            for item in raw_items:
                writer.writerow(item)

        raw_items = [
            ["SKU-03", "BOM-03", "1", "SKU-COM-01", "Product Component 1", "1", "pcs"],
            ["SKU-04", "BOM-03", "1", "SKU-COM-01", "Product Component 1", "1", "pcs"],
        ]
        with open("bom_import_multiple_test.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "code",
                    "yield_quantity",
                    "sku_component",
                    "product_component",
                    "quantity_to_consumed",
                    "unit_to_consumed",
                ]
            )
            for item in raw_items:
                writer.writerow(item)

        raw_items = [
            ["SKU-05", "BOM-04", "1", "SKU-COM-01", "Product Component 1", "1", "pcs"],
        ]
        with open("bom_import_multiple_fail_test.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "code",
                    "yield_quantity",
                    "sku_component",
                    "product_component",
                    "quantity_to_consumed",
                    "unit_to_consumed",
                ]
            )
            for item in raw_items:
                writer.writerow(item)

        raw_items_bad = [
            ["", "BOM-01", "", "SKU-COM-01", "Product Component 1", "1", ""],
        ]

        with open("bom_import_test_bad_file.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "code",
                    "yield_quantity",
                    "sku_component",
                    "product_component",
                    "quantity_to_consumed",
                    "unit_to_consumed",
                ]
            )
            for item in raw_items_bad:
                writer.writerow(item)

        raw_items_bad = [
            ["SKU-01", "BOM-01", "1", "SKU-COM-02", "Product Component 2", "1", "kg"],
        ]

        with open("bom_import_test_bad_file_2.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerow(
                [
                    "sku",
                    "code",
                    "yield_quantity",
                    "sku_component",
                    "product_component",
                    "quantity_to_consumed",
                    "unit_to_consumed",
                ]
            )
            for item in raw_items_bad:
                writer.writerow(item)

    def tearDown(self):
        set_current_tenant(None)
        os.remove("bom_import_test.csv")
        os.remove("bom_import_multiple_test.csv")
        os.remove("bom_import_multiple_fail_test.csv")
        os.remove("bom_import_test_bad_file.csv")
        os.remove("bom_import_test_bad_file_2.csv")

    def test_create_bom(self):
        with open("bom_import_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        form.save()
        self.assertEqual(BOM.objects.count(), 2)

    def test_create_multiple_bom_product(self):
        with open("bom_import_multiple_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertTrue(form.is_valid(), form.errors)
        form.save()
        self.assertEqual(BOM.objects.count(), 1)
        self.assertEqual(BOM.objects.last().products.count(), 2)

    def test_create_multiple_bom_product_fail(self):
        bom = mommy.make("productions.BOM", account=self.account, yield_quantity=1, code="BOM-1234")
        manufactured_product_5 = mommy.make(
            "catalogue.Product",
            sku="SKU-05",
            classification="manufactured",
            uom=self.uom,
            is_manage_stock=True,
            account=self.account,
        )
        bom.products.add(manufactured_product_5, through_defaults={"account": self.account})
        mommy.make(
            "productions.BOMItemDetail",
            bom=bom,
            account=self.account,
            product=self.product_component_1,
            quantity=1,
            quantity_unit=self.uom,
            wastage=0,
            wastage_unit=self.uom,
        )
        with open("bom_import_multiple_fail_test.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))

    def test_create_bom_with_bad_import_header(self):
        with open("bom_import_test_bad_file.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))

    def test_create_bom_with_bad_import_value(self):
        with open("bom_import_test_bad_file_2.csv", "r") as file:
            document = SimpleUploadedFile(
                file.name, bytes(file.read(), "utf-8"), content_type="text/csv"
            )

        form = self.form_class(
            data={},
            files={"upload_file": document},
            resource=self.resource,
            headers=self.headers,
        )
        self.assertFalse(form.is_valid(), (form.__dict__, form.result.__dict__))
