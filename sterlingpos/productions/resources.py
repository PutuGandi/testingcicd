from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import MultipleObjectsReturned

from import_export import resources
from import_export.fields import Field
from decimal import Decimal, InvalidOperation

from sterlingpos.core.import_export import (
    CustomForeignKeyWidget,
    RequiredCharWidget,
    CustomDecimalWidget,
)
from sterlingpos.core.models import get_current_tenant
from sterlingpos.productions.models import BOM, BOMItemDetail
from sterlingpos.catalogue.models import Product, UOM


class BOMResource(resources.ModelResource):
    sku = Field(
        column_name="sku",
        widget=RequiredCharWidget(),
    )
    bom_code = Field(
        column_name="bom_code", attribute="code", widget=RequiredCharWidget()
    )
    yield_quantity = Field(
        column_name="yield_quantity",
        attribute="yield_quantity",
        default=0,
        widget=CustomDecimalWidget(),
    )

    sku_component = Field(column_name="sku_component", widget=RequiredCharWidget(),)
    product_component = Field(
        column_name="product_component", widget=RequiredCharWidget(),
    )
    quantity_to_consumed = Field(
        column_name="quantity_to_consumed", default=0, widget=CustomDecimalWidget(),
    )
    unit_to_consumed = Field(column_name="unit_to_consumed",)

    class Meta:
        model = BOM
        import_id_fields = ("code",)
        fields = (
            "code",
            "yield_quantity",
        )
        exclude = ("account",)
        clean_model_instance = True

    def init_instance(self, row=None):
        instance = super().init_instance(row)
        instance.account = get_current_tenant()
        instance.code = row["code"]
        instance.yield_quantity = row["yield_quantity"]
        instance.save()

        try:
            product = Product.objects.get(sku=row["sku"])
            if product not in instance.products.all():
                if product.bom.all():
                    raise ValueError(_("Produk Manufaktur {} sudah memiliki BOM".format(row["sku"])))
                instance.products.add(product, through_defaults={"account": instance.account})
        except Product.DoesNotExist:
            raise ValueError(_("Produk Manufaktur {} tidak ada".format(row["sku"])))

        instance.save()

        data_product_components = {
            "sku": row["sku_component"],
            "name": row["product_component"],
        }
        try:
            product_comp = Product.objects.get(**data_product_components)
        except Product.DoesNotExist:
            raise ValueError(
                _(
                    "Product Component: {} - {} Does Not Exists".format(
                        row["sku_component"], row["product_component"]
                    )
                )
            )

        if not product_comp.is_manage_stock:
            raise ValueError(
                _(
                    "Produk Komponen: {} - {} tidak menggunakan manajemen stok".format(
                        row["sku_component"], row["product_component"]
                    )
                )
            )

        try:
            obj = UOM.objects.get(unit=row["unit_to_consumed"])
        except UOM.DoesNotExist:
            raise ValueError("UOM: {} does not exist".format(row["unit_to_consumed"]))

        if not product_comp.uom:
            raise ValueError(
                _(
                    "Produk Komponen: {} - {} tidak memiliki satuan".format(
                        row["sku_component"], row["product_component"]
                    )
                )
            )

        if product_comp.uom.category != obj.category:
            raise ValueError(
                _(
                    "Satuan dikonsumsi: {} ({}) dan satuan produk komponen: {} ({}) berbeda kategori".format(
                        obj.unit,
                        obj.category,
                        product_comp.uom.unit,
                        product_comp.uom.category,
                    )
                )
            )

        try:
            qty_to_consumed = Decimal(row["quantity_to_consumed"])
        except InvalidOperation:
            raise ValueError(
                _("Jumlah Dikonsumsi: {} format salah".format(row["quantity_to_consumed"]))
            )

        data_componentes = {
            "product": product_comp,
            "quantity": qty_to_consumed,
            "quantity_unit": obj,
            "wastage_unit": obj,
            "bom": instance
        }
        BOMItemDetail.objects.get_or_create(**data_componentes)
        return instance

    def get_instance(self, instance_loader, row):
        try:
            instance = super().get_instance(instance_loader, row)
            if instance:
                instance.yield_quantity = row["yield_quantity"]
                instance.save()

                try:
                    product = Product.objects.get(sku=row["sku"])
                    if product not in instance.products.all():
                        if product.bom.all():
                            raise ValueError(_("Produk Manufaktur {} sudah memiliki BOM".format(row["sku"])))
                        instance.products.add(product, through_defaults={"account": instance.account})
                except Product.DoesNotExist:
                    raise ValueError(_("Produk Manufaktur {} tidak ada".format(row["sku"])))

                data_product_components = {
                    "sku": row["sku_component"],
                    "name": row["product_component"],
                }
                try:
                    product_comp = Product.objects.get(**data_product_components)
                except Product.DoesNotExist:
                    raise ValueError(
                        _(
                            "Product Component: {} - {} Does Not Exists".format(
                                row["sku_component"], row["product_component"]
                            )
                        )
                    )
                
                if not product_comp.is_manage_stock:
                    raise ValueError(
                        _(
                            "Produk Komponen: {} - {} tidak menggunakan manajemen stok".format(
                                row["sku_component"], row["product_component"]
                            )
                        )
                    )

                try:
                    obj = UOM.objects.get(unit=row["unit_to_consumed"])
                except UOM.DoesNotExist:
                    raise ValueError("UOM: {} does not exist".format(row["unit_to_consumed"]))

                if not product_comp.uom:
                    raise ValueError(
                        _(
                            "Produk Komponen: {} - {} tidak memiliki satuan".format(
                                row["sku_component"], row["product_component"]
                            )
                        )
                    )

                if product_comp.uom.category != obj.category:
                    raise ValueError(
                        _(
                            "Satuan dikonsumsi: {} ({}) dan satuan produk komponen: {} ({}) berbeda kategori".format(
                                obj.unit,
                                obj.category,
                                product_comp.uom.unit,
                                product_comp.uom.category,
                            )
                        )
                    )
                try:
                    qty_to_consumed = Decimal(row["quantity_to_consumed"])
                except InvalidOperation:
                    raise ValueError(
                        _("Jumlah Dikonsumsi: {} format salah".format(row["quantity_to_consumed"]))
                    )

                data_componentes = {
                    "product": product_comp,
                    "quantity": qty_to_consumed,
                    "quantity_unit": obj,
                    "wastage_unit": obj,
                }
                try:
                    detail = instance.bom_item.get(product=product_comp)
                    detail.quantity = data_componentes["quantity"]
                    detail.quantity_unit = data_componentes["quantity_unit"]
                    detail.wastage_unit = data_componentes["wastage_unit"]
                    detail.save()
                except BOMItemDetail.DoesNotExist:
                    instance.bom_item.create(**data_componentes)
                return instance
        except MultipleObjectsReturned:
            raise ValueError(
                _("There are more than 2 existing BOM under this SKU.")
            )


class BOMItemDetailExportResource(resources.ModelResource):
    code = Field(column_name="code", attribute="bom")

    product_sku = Field(column_name="sku", attribute="bom")
    product_name = Field(column_name="sku", attribute="bom")

    yield_quantity = Field(column_name="yield_quantity", attribute="bom")

    sku_component = Field(column_name="sku_component", )
    product_component = Field(column_name="product_component", attribute="product")

    quantity = Field(
        column_name="quantity_to_consumed", attribute="quantity"
    )
    quantity_unit = Field(
        column_name="unit_to_consumed", attribute="quantity_unit",
    )

    class Meta:
        model = BOMItemDetail
        fields = (
            "code",

            "product_sku",
            "product_name",

            "yield_quantity",
            "sku_component",
            "product_component",

            "quantity",
            "quantity_unit",
        )
        export_order = (
            "code",
            "product_sku",
            "product_name",
            "yield_quantity",
            "sku_component",
            "product_component",
            "quantity",
            "quantity_unit",
        )
        exclude = ("account",)
        clean_model_instance = True

    def dehydrate_product_sku(self, obj):
        return obj.bom_product_sku

    def dehydrate_product_name(self, obj):
        return obj.bom_product_name

    def dehydrate_yield_quantity(self, obj):
        return obj.bom.yield_quantity

    def dehydrate_code(self, obj):
        return obj.bom.code

    def dehydrate_sku_component(self, obj):
        return obj.product.sku

    def dehydrate_product_component(self, obj):
        return obj.product.name

    def dehydrate_quantity_unit(self, obj):
        return obj.quantity_unit.unit




class ExportBOMResource(BOMResource):
    product_name = Field(
        column_name="product_name",
        widget=RequiredCharWidget(),
    )
    product_sku = Field(
        column_name="product_sku",
        widget=RequiredCharWidget(),
    )
    quantity = Field(
        column_name="quantity",
    )
    quantity_unit = Field(
        column_name="quantity_unit",
    )

    class Meta(BOMResource.Meta):
        fields = (
            "code",
            "product_sku",
            "product_name",
            "yield_quantity",
            "sku_component",
            "product_component",
            "quantity",
            "quantity_unit",
        )
        export_order = (
            "code",
            "product_sku",
            "product_name",
            "yield_quantity",
            "sku_component",
            "product_component",
            "quantity",
            "quantity_unit",
        )

    def dehydrate_product_sku(self, obj):
        return obj.bom_product_sku

    def dehydrate_product_name(self, obj):
        return obj.bom_product_name

    def dehydrate_yield_quantity(self, obj):
        return obj.bom.yield_quantity

    def dehydrate_code(self, obj):
        return obj.bom.code

    def dehydrate_sku_component(self, obj):
        return obj.product.sku

    def dehydrate_product_component(self, obj):
        return obj.product.name

    def dehydrate_quantity_unit(self, obj):
        return obj.quantity_unit.unit
