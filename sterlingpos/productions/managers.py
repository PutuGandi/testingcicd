from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.db.models import (
    FilteredRelation,
    ExpressionWrapper,
    Subquery,
    Prefetch,
    Sum,
    Count,
    Q,
    F,
    Value as V,
    IntegerField,
    DecimalField,
    Case,
    Value,
    When,
    Avg,
    Func,
    CharField,
    TimeField,
    TextField,
    OuterRef,
)
from django.db.models.sql.datastructures import Join
from django.db.models.fields.related import RelatedField
from django.db.models.functions import (
    Abs,
    Cast,
    Coalesce,
    Concat,
)
from django.db.models.sql.where import AND

from sterlingpos.core.models import (
    SterlingTenantQuerySet,
    SterlingSafeDeleteAllManager,
    SterlingTenantManager,
    get_current_tenant,
)

from django.apps import apps


class BOMItemDetailQuerySet(SterlingTenantQuerySet):

    def bom_usage(
        self,
        start_date=None,
        end_date=None,
        outlet=None,
        **kwargs
    ):

        outlet_qs = kwargs.get("outlet_qs", None)

        basic_filter = Q(pk__isnull=False)

        if outlet_qs:
            basic_filter &= Q(stock__outlet__in=outlet_qs)

        if end_date:
            basic_filter &= Q(created__lte=end_date)

        if outlet:
            basic_filter &= Q(stock__outlet=outlet.pk)

        production_filter = basic_filter & Q(
            transaction_type="productions")

        if start_date:
            production_filter &= Q(created__gte=start_date)

        StockTransaction = apps.get_model(
            "stocks.StockTransaction")
        stock_transcation_qs = StockTransaction.objects.select_related(
            "stock",
        )
        production_transaction_qs = stock_transcation_qs.filter(
            production_filter,
        ).annotate(
            ref_cast=Cast(
                'reference',
                output_field=TextField()
            ),
            bom_id=Func(
                F('ref_cast'),
                Value("bom.*?(\d+),"),
                function='regexp_match',
                template='(%(function)s(%(expressions)s))[1]::int',
                output_field=IntegerField(),
            ),
        )
        self = self.annotate(
            stock_usage=Coalesce(
                Subquery(
                    production_transaction_qs.filter(
                        bom_id=OuterRef("bom"),
                        stock__product=OuterRef("product"),
                    ).order_by(
                    ).values(
                        "quantity"
                    ).annotate(
                        sum_quantity=Abs(Sum("quantity")),
                    ).values_list('sum_quantity')[:1]
                ),
                Value(0),
                output_field=DecimalField(),
            ),
        )
        return self


class BOMItemDetailTenantManager(
    BOMItemDetailQuerySet.as_manager().__class__,
    SterlingSafeDeleteAllManager
):
    pass
