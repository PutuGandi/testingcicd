from django import forms
from django.utils.translation import ugettext as _
from django.forms import inlineformset_factory

from dal import autocomplete, forward
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from decimal import Decimal, InvalidOperation

from sterlingpos.core.widgets import MoneyInputField
from sterlingpos.productions.models import (
    BOM,
    BOMItemDetail,
    BOMOtherCostDetail,
    ProductionsOrder,
    ProductionOrderItemDetail,
    ProductionsOrderOtherCostDetail,
)
from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from sterlingpos.stocks.forms import RequiredFormSet, unique_field_formset
from sterlingpos.catalogue.models import UOM
from sterlingpos.catalogue.utils import (
    get_normalized_decimal,
    uom_conversion,
    uom_cost_conversion,
)
from sterlingpos.catalogue.models import Product


MAX_DIGITS_MANUFACTURE = 5


class BOMForm(forms.ModelForm):
    class Meta:
        model = BOM
        fields = ("products", "code", "yield_quantity")
        widgets = {
            "products": autocomplete.ModelSelect2Multiple(
                url="catalogue:product_manufactured_autocomplete",
                attrs={
                    "data-minimum-input-length": 0,
                    "data-placeholder": _("Type to search"),
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super(BOMForm, self).__init__(*args, **kwargs)
        self.fields["products"].label = _("Product")
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Product"),
                KawnField("products"),
                KawnField("code"),
                KawnField("yield_quantity"),
            )
        )
        self.fields["code"].initial = "BOM-{}".format(
            BOM.all_objects.filter(deleted__isnull=True).count() + 1
        )

    def validate_product_bom(self, product):
        if self.instance:
            qs = product.bom.exclude(pk=self.instance.pk)
        else:
            qs = product.bom.all()

        if qs.exists():
            raise forms.ValidationError(_(f"Product {product} already has BOM"))

    def clean_products(self):
        products = self.cleaned_data.get("products")
        for product in products:
            self.validate_product_bom(product)
        return products

    def clean_yield_quantity(self):
        yield_quantity = self.cleaned_data.get("yield_quantity")
        if yield_quantity:
            try:
                yield_quantity = Decimal(yield_quantity)
                if yield_quantity < 0:
                    raise forms.ValidationError(_("Yield quantity can't be negative"))
            except InvalidOperation:
                raise forms.ValidationError(_("Invalid format"))
        else:
            raise forms.ValidationError(
                _("This field is required and value must be more than 0")
            )
        return yield_quantity

    def update_m2m(self, instance, object_list, rel_field):
        if len(object_list) != 0:
            current_mod = rel_field.all()
            add_objects = object_list.exclude(pk__in=current_mod)
            deleted_objects = current_mod.exclude(pk__in=object_list)
            for obj in add_objects:
                obj.bom.add(instance, through_defaults={"account": instance.account})
            for obj in deleted_objects:
                obj.bom.remove(instance)
        else:
            rel_field.clear()

    def save(self, commit=False):
        products = self.cleaned_data.pop("products")
        instance = super(BOMForm, self).save(commit=commit)
        instance.save()

        self.update_m2m(instance, products, instance.products)
        return instance


class BOMItemDetailForm(forms.ModelForm):
    cost_per_item = forms.CharField(
        max_length=100,
        required=False,
        label=_("Cost per Item"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    total_cost = forms.CharField(
        max_length=100,
        required=False,
        label=_("Total Cost"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    quantity_unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)
    wastage_unit = forms.ModelChoiceField(queryset=UOM.objects.none(), required=True)

    class Meta:
        model = BOMItemDetail
        fields = ("product", "quantity", "quantity_unit", "wastage", "wastage_unit")
        widgets = {
            "product": autocomplete.ModelSelect2(
                url="productions:get_product_components",
                attrs={
                    "data-minimum-input-length": 0,
                    "data-placeholder": _("Type to search"),
                },
                forward=(
                    forward.JavaScript(
                        handler="get_manufactured_product", dst="manufactured_product"
                    ),
                ),
            ),
            "quantity": forms.TextInput(attrs={"maxlength": 5}),
            "wastage": forms.TextInput(attrs={"maxlength": 5}),
        }

    field_order = [
        "product",
        "quantity",
        "quantity_unit",
        "wastage",
        "wastage_unit",
        "cost_per_item",
        "total_cost",
    ]

    def __init__(self, *args, **kwargs):
        super(BOMItemDetailForm, self).__init__(*args, **kwargs)
        self.empty_permitted = False
        self.fields["product"].widget.attrs["class"] = "products-dropdown"
        self.fields["quantity"].widget.attrs["class"] = "products-quantity"
        self.fields["wastage"].widget.attrs["class"] = "products-wastage"
        self.fields["product"].label = _("Product")
        self.fields["quantity_unit"].label = _("Quantity Unit")
        self.fields["wastage_unit"].label = _("Wastage Unit")

        self.fields["quantity_unit"].widget.attrs["class"] = "products-quantity-unit"
        self.fields["wastage_unit"].widget.attrs["class"] = "products-wastage-unit"
        unit_queryset = UOM.objects.filter(is_active=True)
        self.fields["quantity_unit"].queryset = unit_queryset
        self.fields["wastage_unit"].queryset = unit_queryset
        self.fields["quantity_unit"].empty_label = None
        self.fields["wastage_unit"].empty_label = None
        self.fields["product"].empty_label = ""
        self.fields["product"].queryset = Product.objects.filter(
            archived=False,
            is_manage_stock=True,
            deleted__isnull=True,
            classification__in=[
                Product.PRODUCT_CLASSIFICATION.standart,
                Product.PRODUCT_CLASSIFICATION.manufactured,
                Product.PRODUCT_CLASSIFICATION.complementary,
            ],
        )

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["cost_per_item"].widget.attrs["style"] = styles
        self.fields["total_cost"].widget.attrs["style"] = styles

        instance = kwargs.get("instance")
        if instance and instance.pk:
            if not instance.product.archived:
                converted_cost = uom_cost_conversion(
                    instance.product.uom,
                    instance.quantity_unit,
                    1,
                    instance.product.cost.amount,
                )
                self.fields["wastage_unit"].initial = instance.wastage_unit
                self.fields["quantity_unit"].initial = instance.quantity_unit
                self.fields["cost_per_item"].initial = get_normalized_decimal(
                    converted_cost
                )
                self.fields["product"].initial = instance.product
                result = instance.quantity * self.fields["cost_per_item"].initial
                self.fields["total_cost"].initial = get_normalized_decimal(result)

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        wastage = cleaned_data.get("wastage")
        quantity_unit = cleaned_data.get("quantity_unit")
        wastage_unit = cleaned_data.get("wastage_unit")
        deleted = cleaned_data.get("DELETE")

        if not deleted:
            try:
                if quantity:
                    quantity = Decimal(quantity)
                    digits = len(quantity.as_tuple().digits) - abs(
                        quantity.as_tuple().exponent
                    )
                    if quantity < 0:
                        self._errors["quantity"] = self.error_class([""])
                    if digits > MAX_DIGITS_MANUFACTURE:
                        self._errors["quantity"] = self.error_class(
                            [
                                _(
                                    "Maksimal digit {} sebelum koma".format(
                                        MAX_DIGITS_MANUFACTURE
                                    )
                                )
                            ]
                        )
                else:
                    self._errors["quantity"] = self.error_class([""])
            except (InvalidOperation, TypeError):
                self._errors["quantity"] = self.error_class([""])

            try:
                if wastage:
                    wastage = Decimal(wastage)
                    digits = len(wastage.as_tuple().digits) - abs(
                        wastage.as_tuple().exponent
                    )
                    converted_wastage = uom_conversion(
                        wastage_unit, quantity_unit, wastage
                    )
                    if wastage < 0:
                        self._errors["wastage"] = self.error_class([""])
                    if quantity < converted_wastage:
                        self._errors["wastage"] = self.error_class([""])
                    if digits > MAX_DIGITS_MANUFACTURE:
                        self._errors["wastage"] = self.error_class(
                            [
                                _(
                                    "Maksimal digit {} sebelum koma".format(
                                        MAX_DIGITS_MANUFACTURE
                                    )
                                )
                            ]
                        )
            except (InvalidOperation, TypeError):
                self._errors["wastage"] = self.error_class([""])

        return self.cleaned_data

    def save(self, *args, **kwargs):
        product = self.cleaned_data.pop("product")
        quantity = self.cleaned_data.pop("quantity")
        wastage = self.cleaned_data.pop("wastage")
        quantity_unit = self.cleaned_data.pop("quantity_unit")
        wastage_unit = self.cleaned_data.pop("wastage_unit")
        bom = self.cleaned_data.pop("bom")

        if self.instance and self.instance.pk:
            bom_item = super().save(commit=False)
            bom_item.quantity_unit = quantity_unit
            bom_item.wastage_unit = wastage_unit
            bom_item.save()
        else:
            bom_item = bom.create_bom_item(
                product=product,
                quantity=quantity,
                quantity_unit=quantity_unit,
                wastage=wastage,
                wastage_unit=wastage_unit,
            )
        return bom_item


class BOMItemDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(BOMItemDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "bom_item_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Components Item")
        self.add_content = _("Add Component")
        self.form_grid = "col-lg-12"


BOMItemDetailFormSet = inlineformset_factory(
    BOM,
    BOMItemDetail,
    form=BOMItemDetailForm,
    formset=unique_field_formset("product"),
    min_num=1,
    extra=0,
    can_delete=True,
)


class BOMOtherCostDetailForm(forms.ModelForm):
    total_cost = forms.CharField(
        max_length=100,
        required=False,
        label=_("Total Cost"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )

    class Meta:
        model = BOMOtherCostDetail
        fields = (
            "name",
            "quantity",
            "cost_per_item",
        )
        widgets = {
            "quantity": forms.TextInput(attrs={"maxlength": 11}),
            "total_cost": forms.TextInput(),
            "cost_per_item": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs["class"] = "other-cost-name"
        self.fields["name"].label = _("Name")

        self.fields["quantity"].widget.attrs["class"] = "other-cost-quantity"
        self.fields["quantity"].label = _("Quantity")

        self.fields["cost_per_item"].widget.attrs["class"] = "other-cost-per-item"

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["total_cost"].widget.attrs["style"] = styles
        self.fields["total_cost"].initial = 0

        instance = kwargs.get("instance")
        if instance and instance.pk:
            result = instance.quantity * int(instance.cost_per_item.amount)
            self.fields["total_cost"].initial = result

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        cost_per_item = cleaned_data.get("cost_per_item")
        deleted = cleaned_data.get("DELETE")

        if not deleted:
            try:
                if quantity:
                    quantity = Decimal(quantity)
                    if quantity < 0:
                        self._errors["quantity"] = self.error_class(
                            ["Jumlah tidak bisa negatif"]
                        )
                else:
                    self._errors["quantity"] = self.error_class(
                        [_("This field is required")]
                    )

            except (InvalidOperation, TypeError):
                self._errors["quantity"] = self.error_class(["Format tidak sesuai"])

            if cost_per_item:
                if cost_per_item.amount < 0:
                    self._errors["cost_per_item"] = self.error_class(
                        ["Biaya tidak bisa negatif"]
                    )

        return self.cleaned_data

    def save(self, *args, **kwargs):
        name = self.cleaned_data.pop("name")
        quantity = self.cleaned_data.pop("quantity")
        cost = self.cleaned_data.pop("cost_per_item")
        bom = self.cleaned_data.pop("bom")

        if self.instance and self.instance.pk:
            other_cost_item = super().save(commit=True)
        else:
            other_cost_item = bom.create_other_cost(
                name=name, quantity=quantity, cost=cost
            )
        return other_cost_item


class BOMOtherCostDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(BOMOtherCostDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "bom_other_cost_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Other Cost")
        self.add_content = _("Add Other Cost")
        self.form_grid = "col-lg-12"


class CreateBOMOtherCostDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(CreateBOMOtherCostDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "bom_other_cost_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.collapsible_title = _("This bill of materials has other cost")
        self.collapsible_field_name = "has_other_cost"
        self.legend = _("Other Cost")
        self.add_content = _("Add Other Cost")
        self.form_grid = "col-lg-12"


BOMOtherCostDetailFormset = inlineformset_factory(
    BOM,
    BOMOtherCostDetail,
    form=BOMOtherCostDetailForm,
    min_num=1,
    extra=0,
    can_delete=True,
)

CreateBOMOtherCostDetailFormset = inlineformset_factory(
    BOM,
    BOMOtherCostDetail,
    form=BOMOtherCostDetailForm,
    min_num=1,
    extra=0,
    can_delete=True,
)


class BOMComponentForm(forms.ModelForm):
    class Meta:
        model = BOMItemDetail
        fields = ("product", "quantity", "quantity_unit", "wastage", "wastage_unit")
        widgets = {
            "product": autocomplete.ModelSelect2(
                url="productions:get_product_components",
                attrs={
                    "data-minimum-input-length": 0,
                    "data-placeholder": _("Type to search"),
                },
                forward=(
                    forward.JavaScript(
                        handler="get_manufactured_product", dst="manufactured_product"
                    ),
                ),
            ),
            "quantity_unit": autocomplete.ModelSelect2(
                url="productions:get_uom_components",
                attrs={
                    "data-minimum-input-length": 0,
                    "data-placeholder": _("Type to search"),
                },
                forward=["product"],
            ),
            "wastage_unit": autocomplete.ModelSelect2(
                url="productions:get_uom_components",
                attrs={
                    "data-minimum-input-length": 0,
                    "data-placeholder": _("Type to search"),
                },
                forward=["product"],
            ),
            "quantity": forms.TextInput(attrs={"maxlength": 5}),
            "wastage": forms.TextInput(attrs={"maxlength": 5}),
        }

    def __init__(self, *args, **kwargs):
        bom_pk = kwargs.pop("bom_pk")
        try:
            self.bom = BOM.objects.get(pk=bom_pk)
        except BOM.DoesNotExist:
            self.bom = None
        super(BOMComponentForm, self).__init__(*args, **kwargs)
        self.fields["product"].label = _("Product")
        self.fields["quantity_unit"].label = _("Quantity Unit")
        self.fields["wastage_unit"].label = _("Wastage Unit")

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.helper.form_tag = False
        self.helper.include_media = False

        self.helper.layout = Layout(
            KawnField("product"),
            Div(
                Div(
                    Div(KawnField("quantity"), css_class="col-lg-6"),
                    Div(KawnField("quantity_unit"), css_class="col-lg-6"),
                    css_class="form-row",
                ),
                css_class="col-lg-12 px-0 mb-0",
            ),
            Div(
                Div(
                    Div(KawnField("wastage"), css_class="col-lg-6"),
                    Div(KawnField("wastage_unit"), css_class="col-lg-6"),
                    css_class="form-row",
                ),
                css_class="col-lg-12 px-0 mb-0",
            ),
        )

    def clean_product(self):
        product = self.cleaned_data["product"]
        if self.instance and self.instance.pk:
            if self.instance.product != product:
                if self.bom:
                    if self.bom.bom_item.filter(product=product).exists():
                        raise forms.ValidationError(_("This product is already exist."))
        else:
            if self.bom:
                if self.bom.bom_item.filter(product=product).exists():
                    raise forms.ValidationError(_("This product is already exist."))

        return product

    def clean_quantity_unit(self):
        quantity_unit = self.cleaned_data["quantity_unit"]
        product = self.cleaned_data.get("product")

        if product and product.uom.category != quantity_unit.category:
            raise forms.ValidationError(_("Satuan tidak sesuai dengan satuan produk"))
        return quantity_unit

    def clean_wastage_unit(self):
        wastage_unit = self.cleaned_data["wastage_unit"]
        product = self.cleaned_data.get("product")

        if product and product.uom.category != wastage_unit.category:
            raise forms.ValidationError(_("Satuan tidak sesuai dengan satuan produk"))
        return wastage_unit


class ProductionsOrderForm(forms.ModelForm):
    class Meta:
        model = ProductionsOrder
        fields = ("product", "bom", "code", "produced_quantity", "outlet", "notes")
        widgets = {
            "product": autocomplete.ModelSelect2(
                url="productions:get_manufactured_product",
                attrs={"data-minimum-input-length": 0},
            ),
            "bom": autocomplete.ModelSelect2(
                url="productions:bom_autocomplete",
                attrs={"data-minimum-input-length": 0},
                forward=["product"],
            ),
            "outlet": autocomplete.ModelSelect2(
                url="stocks:outlet_inventory_autocomplete",
                attrs={"data-minimum-input-length": 0},
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["notes"].widget.attrs["rows"] = 3

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.fields["notes"].required = False
        self.fields["product"].widget.attrs["class"] = "product-dropdown"
        self.fields["product"].label = _("Product")
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Production Order"),
                KawnField("product"),
                KawnField("bom"),
                KawnField("code"),
                KawnField("produced_quantity"),
                KawnField("outlet"),
                KawnField("notes"),
            )
        )

        self.fields["code"].initial = "PROD-{}".format(
            ProductionsOrder.all_objects.count() + 1
        )

    def clean_produced_quantity(self):
        produced_quantity = self.cleaned_data.get("produced_quantity")
        if produced_quantity:
            try:
                produced_quantity = Decimal(produced_quantity)
                if produced_quantity < 0:
                    raise forms.ValidationError(
                        _("Produced quantity can't be negative number")
                    )
            except InvalidOperation:
                raise forms.ValidationError(_("Invalid format."))
        else:
            raise forms.ValidationError(
                _("This field is required and value must be more than 0")
            )
        return produced_quantity


class ProductionsOrderOtherCostDetailForm(forms.ModelForm):
    total_cost = forms.CharField(
        max_length=100,
        required=False,
        label=_("Total Cost"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )

    class Meta:
        model = ProductionsOrderOtherCostDetail
        fields = (
            "name",
            "quantity",
            "cost_per_item",
        )
        widgets = {
            "quantity": forms.TextInput(attrs={"maxlength": 11}),
            "total_cost": forms.TextInput(),
            "cost_per_item": MoneyInputField(
                default_currency="IDR", currency_widget=forms.HiddenInput
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs["class"] = "other-cost-name"
        self.fields["quantity"].widget.attrs["class"] = "other-cost-quantity"
        self.fields["cost_per_item"].widget.attrs["class"] = "other-cost-per-item"

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["total_cost"].widget.attrs["style"] = styles
        self.fields["total_cost"].initial = 0

        instance = kwargs.get("instance")
        if instance and instance.pk:
            result = instance.quantity * instance.cost_per_item.amount
            self.fields["total_cost"].initial = result

    def clean(self):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        cost_per_item = cleaned_data.get("cost_per_item")
        deleted = cleaned_data.get("DELETE")

        if not deleted:
            try:
                if quantity:
                    quantity = Decimal(quantity)
                    if quantity < 0:
                        self._errors["quantity"] = self.error_class(
                            ["Jumlah tidak bisa negatif"]
                        )
                else:
                    self._errors["quantity"] = self.error_class(
                        [_("This field is required")]
                    )

            except (InvalidOperation, TypeError):
                self._errors["quantity"] = self.error_class(["Format tidak sesuai"])

            if cost_per_item:
                if cost_per_item.amount < 0:
                    self._errors["cost_per_item"] = self.error_class(
                        ["Biaya tidak bisa negatif"]
                    )

        return self.cleaned_data

    def save(self, *args, **kwargs):
        name = self.cleaned_data.pop("name")
        quantity = self.cleaned_data.pop("quantity")
        cost = self.cleaned_data.pop("cost_per_item")
        productions = self.cleaned_data.pop("productions")

        if self.instance and self.instance.pk:
            other_cost_item = super().save(commit=True)
        else:
            other_cost_item = productions.create_productions_other_cost(
                name=name, quantity=quantity, cost=cost
            )
        return other_cost_item


class ProductionsOrderOtherCostDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ProductionsOrderOtherCostDetailFormsetHelper, self).__init__(
            *args, **kwargs
        )
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.form_id = "production_other_cost_detail"
        self.template = "bootstrap4/formset_with_help.html"
        self.legend = _("Other Cost")
        self.add_content = _("Add Other Cost")
        self.form_grid = "col-lg-12"


ProductionsOrderOtherCostDetailFormset = inlineformset_factory(
    ProductionsOrder,
    ProductionsOrderOtherCostDetail,
    form=ProductionsOrderOtherCostDetailForm,
    min_num=1,
    extra=0,
    can_delete=True,
)


class ProductionOrderUpdateForm(forms.ModelForm):
    product = forms.CharField(
        max_length=100,
        required=False,
        label=_("Product"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    bom = forms.CharField(
        max_length=100,
        required=False,
        label=_("Bill of Material"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    produced_quantity = forms.CharField(
        max_length=100,
        required=False,
        label=_("Produced Quantity"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    outlet = forms.CharField(
        max_length=100,
        required=False,
        label=_("Outlet"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )

    class Meta:
        model = ProductionsOrder
        fields = ("code", "notes")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["notes"].widget.attrs["rows"] = 3
        self.fields["produced_quantity"].widget.attrs["maxlength"] = 11

        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"
        self.fields["notes"].required = False
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Production Order"),
                KawnField("product"),
                KawnField("bom"),
                KawnField("code"),
                KawnField("produced_quantity"),
                KawnField("outlet"),
                KawnField("notes"),
            )
        )

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["product"].initial = instance.product
            self.fields["bom"].initial = instance.bom
            self.fields["produced_quantity"].initial = get_normalized_decimal(
                instance.produced_quantity
            )
            self.fields["outlet"].initial = instance.outlet

    def clean_produced_quantity(self):
        produced_quantity = self.cleaned_data.get("produced_quantity")
        if produced_quantity:
            try:
                produced_quantity = Decimal(produced_quantity)
                if produced_quantity > 10000000:
                    raise forms.ValidationError(
                        _("Max limit for produced quantity is 10.000.000")
                    )
                if produced_quantity < 0:
                    raise forms.ValidationError(
                        _("Produced quantity can't be negative number")
                    )
            except InvalidOperation:
                raise forms.ValidationError(_("Invalid format."))
        else:
            raise forms.ValidationError(
                _("This field is required and value must be more than 0")
            )


class StartProductionForm(forms.ModelForm):
    class Meta:
        model = ProductionsOrder
        fields = ("started",)

    def __init__(self, *args, **kwargs):
        super(StartProductionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"

        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields["started"].required = True
        self.fields["started"].label = _("Started On")


class CompleteProductionForm(forms.ModelForm):
    finished = forms.CharField()

    class Meta:
        model = ProductionsOrder
        fields = ("id",)

    def __init__(self, *args, **kwargs):
        super(CompleteProductionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-3"

        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields["finished"].label = _("Finished On")
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Complete Production Order"),
                KawnField("finished"),
            )
        )


class CompleteProductionItemDetailForm(forms.ModelForm):
    product = forms.CharField(
        max_length=100,
        required=False,
        label=_("Item Name"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    quantity = forms.CharField(
        max_length=100,
        required=False,
        label=_("Qauntity"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    cost_per_item = forms.CharField(
        max_length=100,
        required=False,
        label=_("Cost per Item"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )
    total_cost = forms.CharField(
        max_length=100,
        required=False,
        label=_("Total Cost"),
        widget=forms.TextInput(attrs={"readonly": True}),
    )

    class Meta:
        model = ProductionOrderItemDetail
        fields = (
            "wastage_quantity",
            "unconsumed_quantity",
            "unconsumed_unit",
            "wastage_unit",
        )
        widgets = {
            "wastage_quantity": forms.TextInput(attrs={"maxlength": 5}),
            "unconsumed_quantity": forms.TextInput(attrs={"maxlength": 5}),
            "wastage_unit": autocomplete.ModelSelect2(
                url="productions:get_production_unit",
                attrs={"data-minimum-input-length": 0, "data-allow-clear": False},
                forward=["productions", "id"],
            ),
            "unconsumed_unit": autocomplete.ModelSelect2(
                url="productions:get_production_unit",
                attrs={"data-minimum-input-length": 0},
                forward=["productions", "id"],
            ),
        }

    field_order = [
        "product",
        "quantity",
        "wastage_quantity",
        "wastage_unit",
        "unconsumed_quantity",
        "unconsumed_unit",
        "cost_per_item",
        "total_cost",
    ]

    def __init__(self, *args, **kwargs):
        super(CompleteProductionItemDetailForm, self).__init__(*args, **kwargs)

        styles = "background-color: #fff !important; border: 0 !important;"
        self.fields["cost_per_item"].widget.attrs["style"] = styles
        self.fields["total_cost"].widget.attrs["style"] = styles
        self.fields["product"].widget.attrs["style"] = styles
        self.fields["quantity"].widget.attrs["style"] = styles
        self.fields["unconsumed_quantity"].widget.attrs["class"] = "unconsumed-quantity"
        self.fields["wastage_quantity"].widget.attrs["class"] = "wastage-quantity"
        self.fields["unconsumed_unit"].widget.attrs["class"] = "unconsumed-unit"
        self.fields["wastage_unit"].widget.attrs["class"] = "wastage-unit"
        self.fields["unconsumed_unit"].label = _("Unconsumed Unit")
        self.fields["wastage_unit"].label = _("Wastage Unit")

        self.fields["wastage_quantity"].label = _("Wastage")
        self.fields["unconsumed_quantity"].label = _("Unconsumed")

        self.fields["unconsumed_unit"].empty_label = None
        self.fields["wastage_unit"].empty_label = None

        instance = kwargs.get("instance")
        if instance and instance.pk:
            self.fields["product"].initial = "{} - {}".format(
                instance.product.catalogue.name,
                instance.product.name,
            )
            self.fields["wastage_unit"].initial = instance.wastage_unit
            self.fields["unconsumed_unit"].initial = instance.unconsumed_unit
            self.fields["quantity"].initial = "{} {}".format(
                get_normalized_decimal(instance.quantity),
                instance.quantity_unit,
            )
            self.fields["cost_per_item"].initial = get_normalized_decimal(
                instance.converted_cost
            )
            self.fields["total_cost"].initial = get_normalized_decimal(
                instance.total_cost
            )
            self.fields["wastage_quantity"].initial = get_normalized_decimal(
                instance.wastage_quantity
            )
            self.fields["unconsumed_quantity"].initial = get_normalized_decimal(
                instance.unconsumed_quantity
            )

    def clean(self):
        wastage = self.cleaned_data.get("wastage_quantity")
        wastage_unit = self.cleaned_data.get("wastage_unit")
        unconsumed = self.cleaned_data.get("unconsumed_quantity")
        unconsumed_unit = self.cleaned_data.get("unconsumed_unit")

        converted_wastage = 0
        converted_unconsumed = 0
        if wastage:
            wastage = Decimal(wastage)
            digits = len(wastage.as_tuple().digits) - abs(wastage.as_tuple().exponent)
            if digits > MAX_DIGITS_MANUFACTURE:
                self._errors["wastage"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_MANUFACTURE))]
                )
            converted_wastage = uom_conversion(
                wastage_unit, self.instance.quantity_unit, wastage
            )
        else:
            if not wastage == 0:
                self._errors["wastage"] = self.error_class([""])

        if unconsumed:
            unconsumed = Decimal(unconsumed)
            digits = len(unconsumed.as_tuple().digits) - abs(
                unconsumed.as_tuple().exponent
            )
            if digits > MAX_DIGITS_MANUFACTURE:
                self._errors["unconsumed"] = self.error_class(
                    [_("Maksimal digit {} sebelum koma".format(MAX_DIGITS_MANUFACTURE))]
                )
            converted_unconsumed = uom_conversion(
                unconsumed_unit, self.instance.quantity_unit, unconsumed
            )
        else:
            if not unconsumed == 0:
                self._errors["unconsumed"] = self.error_class([""])

        total = converted_wastage + converted_unconsumed
        if self.instance.quantity <= total:
            self._errors["wastage_quantity"] = self.error_class(
                ["Melebihi jumlah komponen"]
            )
            self._errors["unconsumed_quantity"] = self.error_class(
                ["Melebihi jumlah komponen"]
            )


class CompleteProductionItemDetailFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(CompleteProductionItemDetailFormsetHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.add_false = True
        self.form_id = "production_components_item"
        self.template = "bootstrap4/formset_inline.html"
        self.legend = _("Components")
        self.form_grid = "col-lg-12"


CompleteProductionItemDetailFormset = inlineformset_factory(
    ProductionsOrder,
    ProductionOrderItemDetail,
    form=CompleteProductionItemDetailForm,
    formset=RequiredFormSet,
    extra=0,
    can_delete=False,
)
