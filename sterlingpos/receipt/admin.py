from django.contrib import admin
from treebeard.admin import TreeAdmin

from treebeard.forms import movenodeform_factory

from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportMixin
from import_export.fields import Field
from import_export.widgets import ForeignKeyWidget

from .models import (
    Receipt,
)


class ReceiptAdmin(admin.ModelAdmin):
    '''
        Admin View for Receipt
    '''
    list_display = ('account', 'outlet', 'social_facebook', 'social_twitter', 'social_instagram', 'website')
    search_fields = ("outlet__name", "account__name",)


admin.site.register(Receipt, ReceiptAdmin)
