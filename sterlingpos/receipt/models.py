from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField
from django.utils.text import slugify
from django.contrib.postgres.fields import ArrayField

from model_utils import Choices
from model_utils.models import TimeStampedModel
from sterlingpos.core.models import SterlingTenantModel

from imagekit.models import ProcessedImageField
from imagekit.processors import Adjust, ResizeToFit

from sterlingpos.utils.storage import OverwriteStorage

_VISIBILITY_RECEIPT = Choices(
    ('brand_logo', _('Logo Image')),
    ('outlet', _('Outlet Name')),
    ('address', _('Outlet Address')),
    ('province', _('Province')),
    ('city', _('City')),
    ('phone', _('Phone Number')),
    ('notes', _('Footnotes')),
    ('product_notes', _('Product Notes')),
    ('website', _('Website')),
    ('social_facebook', _('Facebook')),
    ('social_twitter', _('Twitter')),
    ('social_instagram', _('Instagram')),
)


def get_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = "receipt-brand_logo-{}.{}".format(
        slugify(instance.outlet.name), ext)
    return '{}/brand-logo/{}'.format(instance.account.pk, new_filename)


class Receipt(SterlingTenantModel, TimeStampedModel):
    RECEIPT_TEMPLATE = Choices(
        ('template_1', _('Template-1')),
        ('template_2', _('Template-2')),
        ('template_3', _('Template-3')),
    )

    receipt_header = models.TextField(
        _("Receipt Header"), blank=True, max_length=255)
    brand_logo = ProcessedImageField(
        upload_to=get_upload_path,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        processors=[ResizeToFit(width=240, height=240, upscale=False), Adjust(color=0)],
        format='JPEG',
        options={'quality': 60}
    )

    outlet = models.OneToOneField(
        'outlet.Outlet', blank=True, null=True, on_delete=models.CASCADE)
    social_facebook = models.CharField(
        _("Facebook"), blank=True, default="", max_length=255)
    social_twitter = models.CharField(
        _("Twitter"), blank=True, default="", max_length=255)
    social_instagram = models.CharField(
        _("Instagram"), blank=True, default="", max_length=255)
    website = models.CharField(
        _("Website"), blank=True, default="", max_length=255)

    template = models.CharField(
        _('Template'), max_length=255,
        choices=RECEIPT_TEMPLATE, default=RECEIPT_TEMPLATE.template_1)
    style = JSONField(default={})
    notes = models.TextField(_("Notes"), blank=True)
    visibility_receipt = ArrayField(
        models.CharField(
            max_length=255,
            choices=_VISIBILITY_RECEIPT,
        ),
        default=["brand_logo", "outlet", "address", "province", "city", "phone", "notes", "website", "social_facebook",
                 "social_twitter", "social_instagram"],
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = "Receipt"
        verbose_name_plural = "Receipts"
        unique_together = (('account', 'id'),)

    def apply_style(self, template_name):
        if template_name == "template_1":
            self.style = {
                'header': {
                    'alignment': 'center'
                },
                'receipt_info': {
                    'alignment': 'left'
                }
            }
        elif template_name == 'template_2':
            self.style = {
                'header': {
                    'alignment': 'left'
                },
                'receipt_info': {
                    'alignment': 'left'
                }
            }
        elif template_name == 'template_3':
            self.style = {
                'header': {
                    'alignment': 'right'
                },
                'receipt_info': {
                    'alignment': 'justify'
                }
            }

    def save(self, *args, **kwargs):
        self.apply_style(self.template)
        super(Receipt, self).save(*args, **kwargs)
