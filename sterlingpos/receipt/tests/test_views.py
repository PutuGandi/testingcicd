import json

from test_plus.test import TestCase
from django.urls import reverse, reverse_lazy
from django.http import Http404
from django.test import RequestFactory
from django.test.client import Client
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant
from sterlingpos.receipt.views import ReceiptFormView


class ReceiptFormViewTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

        self.account = mommy.make('accounts.account', name='test-1')
        set_current_tenant(self.account)
        self.user = mommy.make(
            'users.user', email='example@test.com', account=self.account)
        self.account.owners.get_or_create(account=self.account, user=self.user)

        self.outlet = mommy.make(
            'outlet.Outlet',
            branch_id='Branch-1', account=self.account,
            _fill_optional=['city', 'province'])
        self.user.set_password('1sampai6')
        self.user.save()

        self.url = reverse_lazy(
            'receipt:receipt', kwargs={'outlet_id': self.outlet.pk})

    def tearDown(self):
        set_current_tenant(None)

    def test_view_is_not_accessible_by_anon(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertIn(self.reverse('account_login'), response.url)

    def test_good_view(self):
        self.login(email=self.user.email, password='1sampai6')
        self.assertGoodView(self.url)

    def test_get_context_data(self):
        request = self.factory.get(self.url)
        request.user = self.user

        view = ReceiptFormView(**{'kwargs': {'outlet_id': self.outlet.pk}})
        view.request = request
        view.object = None

        context = view.get_context_data()
        self.assertEqual(context.get('outlet'), self.outlet)

    def test_get_form_kwargs(self):
        request = self.factory.get(self.url)
        request.user = self.user

        view = ReceiptFormView(**{'kwargs': {'outlet_id': self.outlet.pk}})
        view.request = request
        view.object = None

        form_kwargs = view.get_form_kwargs()
        self.assertNotEqual(form_kwargs.get('instance'), None)

    def test_success_url(self):
        request = self.factory.get(self.url)
        request.user = self.user

        view = ReceiptFormView(**{'kwargs': {'outlet_id': self.outlet.pk}})
        view.request = request
        view.object = None

        success_url = view.get_success_url()
        self.assertEqual(success_url, self.url)

    def test_post(self):
        data = {
            'receipt_header': 'RECEIPT HEADER',
            'name': 'OUTLET NAME',
            'address': 'Address',
            'postal_code': '99999', 'phone': '0821123456',
            'outlet': self.outlet.pk,
            'brand_logo': '',
            'city': self.outlet.city.pk,
            'province': self.outlet.province.pk,
            'notes': 'Receipt Note',
            'website': 'www.website.com',
            'social_facebook': '@facebook',
            'social_twitter': '',
            'social_instagram': '@instagram',
            'template-preview': '48',
            'template': 'template_1',
            'visibility_receipt': ['brand_logo', 'outlet', 'city', 'province']
        }
        request = self.factory.post(self.url, data)
        request.user = self.user

        SessionMiddleware().process_request(request)
        MessageMiddleware().process_request(request)

        view = ReceiptFormView(**{'kwargs': {'outlet_id': self.outlet.pk}})
        view.request = request
        response = view.post(request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.outlet.receipt.receipt_header, 'RECEIPT HEADER')
        self.assertEqual(self.outlet.receipt.notes, 'Receipt Note')

    def test_post_invalid_data(self):
        data = {
            'outlet': "",
            'notes': 'Updated Notes',
        }
        request = self.factory.post(self.url, data)
        request.user = self.user

        view = ReceiptFormView(**{'kwargs': {'outlet_id': self.outlet.pk}})
        view.request = request
        response = view.post(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.outlet.receipt.notes, '')

    def test_post_invalid_outlet(self):
        request = self.factory.post(self.url)
        request.user = self.user

        view = ReceiptFormView(**{'kwargs': {'outlet_id': 99}})
        view.request = request
        with self.assertRaises(Http404):
            view.post(request)


class ReceiptInitalViewTest(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.account', name='test-1')
        set_current_tenant(self.account)
        self.user = mommy.make('users.user', email='example@test.com', account=self.account)
        self.outlet = mommy.make('outlet.Outlet', branch_id='Branch-1', account=self.account)
        self.user.set_password('1sampai6')
        self.user.save()
        self.account.owners.get_or_create(account=self.account, user=self.user)

        self.client = Client()
        self.client.login(email='example@test.com', password='1sampai6')

        self.url = reverse_lazy('receipt:receipt', kwargs={'outlet_id': self.outlet.pk})

    def test_get_initial_receipt(self):
        data = {
            'outlet_id': self.outlet.pk
        }

        response = self.client.get(reverse('receipt:receipt_data'), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")

    def test_get_initial_receipt_failed(self):
        data = {
            'outlet_id': ''
        }

        response = self.client.get(reverse('receipt:receipt_data'), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 404)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "fail")

    def test_get_initial_already_have_receipt(self):
        receipt = mommy.make('receipt.Receipt', outlet=self.outlet)
        data = {
            'outlet_id': self.outlet.pk
        }

        response = self.client.get(reverse('receipt:receipt_data'), data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")

    def test_get_initial_visibility_receipt(self):
        receipt = mommy.make('receipt.Receipt', outlet=self.outlet)
        data = {
            'outlet_id': self.outlet.pk,
            'visibility_receipt': ["brand_logo", "outlet", "address", "province", "city", "phone", "notes", "website",
                                   "social_facebook",
                                   "social_twitter", "social_instagram"]
        }
        response = self.client.get(reverse('receipt:receipt_data'), data=data, **{'HTTP_X_REQUESTED_WITH':
                                                                                      'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], 'success')

