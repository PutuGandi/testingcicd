from django.test import TestCase

from model_mommy import mommy
from django.template.defaultfilters import slugify


from sterlingpos.core.models import set_current_tenant
from sterlingpos.receipt.models import Receipt, get_upload_path


class TestReceipt(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Test Account')
        set_current_tenant(self.account)
        self.outlet = mommy.make(
            'outlet.Outlet', branch_id='branch-01', account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_get_upload_path(self):
        receipt = Receipt.objects.create(
            outlet=self.outlet,
            receipt_header='Receipt\r\nOutlet',
            social_facebook=self.account.social_facebook,
            social_twitter=self.account.social_twitter,
            social_instagram=self.account.social_instagram,
            website=self.account.website,
            template=Receipt.RECEIPT_TEMPLATE.template_1,
            notes='Receipt Notes'
        )
        upload_path = get_upload_path(receipt, 'image.jpg')
        file_name = "receipt-brand_logo-{}".format(slugify(self.outlet.name))
        self.assertEqual(
            upload_path, '{}/brand-logo/{}.jpg'.format(
                receipt.account.pk, file_name)
        )

    def test_apply_style(self):
        receipt = Receipt.objects.create(
            outlet=self.outlet,
            receipt_header='Receipt\r\nOutlet',
            social_facebook=self.account.social_facebook,
            social_twitter=self.account.social_twitter,
            social_instagram=self.account.social_instagram,
            website=self.account.website,
            template=Receipt.RECEIPT_TEMPLATE.template_1,
            notes='Receipt Notes'
        )
        receipt.apply_style('template_2')
        self.assertEqual(
            receipt.style.get('header'), {'alignment': 'left'})
        self.assertEqual(
            receipt.style.get('receipt_info'), {'alignment': 'left'})

    def test_save_method_invoke_apply_style(self):
        receipt = Receipt(
            outlet=self.outlet,
            receipt_header='Receipt\r\nOutlet',
            social_facebook=self.account.social_facebook,
            social_twitter=self.account.social_twitter,
            social_instagram=self.account.social_instagram,
            website=self.account.website,
            template=Receipt.RECEIPT_TEMPLATE.template_3,
            notes='Receipt Notes'
        )
        receipt.save()

        self.assertEqual(
            receipt.style.get('header'), {'alignment': 'right'})
        self.assertEqual(
            receipt.style.get('receipt_info'), {'alignment': 'justify'})
