from test_plus.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.receipt.forms import ReceiptForm


class ReceiptFormTest(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Test Account')
        set_current_tenant(self.account)
        self.outlet = mommy.make('outlet.Outlet', branch_id='branch-01', account=self.account)
        self.receipt_form = ReceiptForm

    def tearDown(self):
        set_current_tenant(None)

    def test_create_receipt(self):
        prepared_receipt = mommy.prepare(
            'receipt.Receipt', outlet=self.outlet)
        receipt_data = prepared_receipt.__dict__

        receipt_form = self.receipt_form(data=receipt_data)
        self.assertTrue(receipt_form.is_valid(), receipt_form.errors)
        receipt = receipt_form.save()
        self.assertIsNotNone(receipt.pk)
        self.assertIsNotNone(receipt.account, get_current_tenant())