from django import forms
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML

from sterlingpos.core.bootstrap import KawnField
from .models import Receipt, _VISIBILITY_RECEIPT

MAX_UPLOAD_SIZE = 153600  # 4MB


class ReceiptForm(forms.ModelForm):
    receipt_id = forms.CharField(widget=forms.TextInput)

    class Meta:
        model = Receipt
        fields = (
            'receipt_header', 'brand_logo', 'outlet',
            'website', 'social_facebook', 'social_twitter',
            'social_instagram', 'notes')
        exclude = ('account', 'template', 'visibility_receipt')

    def __init__(self, *args, **kwargs):
        super(ReceiptForm, self).__init__(*args, **kwargs)
        self.fields['receipt_header'].widget.attrs['rows'] = 3
        self.fields['receipt_header'].widget.attrs['maxlength'] = 255
        self.fields['notes'].widget.attrs['rows'] = 6
        self.fields['notes'].widget.attrs['maxlength'] = 255
        self.fields['website'].widget.attrs['maxlength'] = 50
        self.fields['social_facebook'].widget.attrs['maxlength'] = 50
        self.fields['social_twitter'].widget.attrs['maxlength'] = 50
        self.fields['social_instagram'].widget.attrs['maxlength'] = 50
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields['receipt_id'].required = False

        self.helper.layout = Layout(
            Field('receipt_id', type='hidden', css_class='receipt-id'),
            Field('outlet', type='hidden', css_class='outlet-id'),
            'receipt_header',
            Div(
                Div(
                    Field('brand_logo', template='dashboard/image_upload/receipt_logo_fields.html'),
                    css_class="col-lg-5"
                ),
                Div(
                    Div(
                        HTML('<p class="receipt-image-upload">{}</p>'.format(
                            _('Drag an image here or browse for an image to upload (Maximum size: 150 KB).'))),
                        css_class="mt-4 pt-2"
                    ),
                    css_class="col-lg-7"
                ),
                css_class="row"
            ),
            'notes',
            Div(
                Div(
                    'website',
                    css_class='col-lg-6'
                ),
                Div(
                    'social_facebook',
                    css_class="col-lg-6"
                ),
                css_class='row'
            ),
            Div(

                Div(
                    'social_twitter',
                    css_class="col-lg-6"
                ),
                Div(
                    'social_instagram',
                    css_class="col-lg-6"
                ),
                css_class="row"
            ),
        )


class TemplateReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ('template',)

    def __init__(self, *args, **kwargs):
        super(TemplateReceiptForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['template'].label = ''
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.layout = Layout(
            Field('template', css_class='custom-select'),
        )


class VisibilityReceiptForm(forms.ModelForm):
    visibility_receipt = forms.MultipleChoiceField(
        choices=_VISIBILITY_RECEIPT,
        widget=forms.CheckboxSelectMultiple,
        label=''
    )

    all_outlet = forms.BooleanField(initial=False, label="Gunakan pengaturan ini untuk semua outlet")

    class Meta:
        model = Receipt
        fields = ('visibility_receipt', 'all_outlet')

    def __init__(self, *args, **kwargs):
        super(VisibilityReceiptForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['all_outlet'].required = False
        self.fields['visibility_receipt'].required = False
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.layout = Layout(
            HTML('<p>Edit tampilan yang akan ditampilkan</p>'),
            KawnField('visibility_receipt', template='dashboard/receipt/switch_input/multiple_visibility_switch.html'),
            KawnField('all_outlet'),
        )

    def save(self, *args, **kwargs):
        visibility_receipt = self.cleaned_data['visibility_receipt']
        receipt = super(VisibilityReceiptForm, self).save(*args, **kwargs)
        if self.cleaned_data['all_outlet'] == True:
            Receipt.objects.all().update(visibility_receipt=visibility_receipt)
        return receipt
