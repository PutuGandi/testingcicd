from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import FormView, View
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404

from braces.views import (
    JSONResponseMixin, AjaxResponseMixin
)

from .models import Receipt
from .forms import ReceiptForm, TemplateReceiptForm, VisibilityReceiptForm
from sterlingpos.outlet.forms import OutletFormReceipt

from sterlingpos.outlet.models import Outlet
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user


class ReceiptFormView(SterlingRoleMixin, FormView):
    form_class = ReceiptForm
    outlet_form = OutletFormReceipt
    template_form = TemplateReceiptForm
    visibility_receipt_form = VisibilityReceiptForm
    success_url = reverse_lazy('receipt:receipt_from_view')
    template_name = 'dashboard/receipt/receipt.html'

    def get_visibility_receipt_form(self):
        return self.visibility_receipt_form

    def get_visibility_receipt(self, visibility_receipt_form=None):
        if visibility_receipt_form is None:
            visibility_receipt_form = self.get_visibility_receipt_form()
        return visibility_receipt_form(**self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(ReceiptFormView, self).get_context_data(**kwargs)

        outlet = self.get_outlet()
        context['outlet'] = outlet

        if 'outlet_form' not in context:
            context['outlet_form'] = self.outlet_form(instance=outlet)

        if 'template_form' not in context:
            context['template_form'] = self.template_form()

        if 'visibility_receipt_form' not in context:
            context['visibility_receipt_form'] = self.get_visibility_receipt()

        return context

    def get_outlet(self):
        outlet = get_object_or_404(
            get_outlet_of_user(self.request.user),
            pk=self.kwargs.get('outlet_id'))
        return outlet

    def get_receipt(self):
        outlet = self.get_outlet()
        receipt, _ = Receipt.objects.get_or_create(outlet=outlet)
        return receipt

    def get_success_url(self):
        return reverse_lazy(
            'receipt:receipt', kwargs={'outlet_id': self.kwargs['outlet_id']})

    def post(self, request, *args, **kwargs):
        outlet_form = self.outlet_form(
            request.POST, request.FILES, instance=self.get_outlet())
        receipt = self.get_receipt()

        form = self.form_class(
            request.POST, request.FILES,
            instance=receipt)
        template_form = self.template_form(
            request.POST, request.FILES,
            instance=receipt)
        visibility_receipt_form = self.visibility_receipt_form(
            request.POST, request.FILES,
            instance=receipt
        )

        if (outlet_form.is_valid()
            and form.is_valid()
            and template_form.is_valid()
            and visibility_receipt_form.is_valid()):
            return self.form_valid(outlet_form, form, template_form, visibility_receipt_form)
        else:
            return self.form_invalid(outlet_form, form, template_form, visibility_receipt_form)

    def get_form_kwargs(self):
        kwargs = super(ReceiptFormView, self).get_form_kwargs()
        kwargs.update({
            'instance': self.get_receipt()
        })
        return kwargs

    def form_valid(self, outlet_form, form, template_form, visibility_receipt_form):
        outlet_form.save(commit=True)

        receipt = form.save(commit=False)
        brand_logo = form.cleaned_data.get('brand_logo')
        if brand_logo:
            receipt.brand_logo = brand_logo
        else:
            receipt.brand_logo = self.request.user.account.brand_logo.name

        template_form.save(commit=False)
        template_name = template_form.cleaned_data['template']
        visibility_receipt_form.save(commit=False)
        visibility_receipts = visibility_receipt_form.cleaned_data['visibility_receipt']

        receipt.template = template_name
        receipt.visibility_receipt = visibility_receipts
        receipt.save()

        messages.success(
            self.request, '{}'.format(
                _('Receipt has been updated.')))

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, outlet_form, form, template_form, visibility_receipt_form):
        return self.render_to_response(
            self.get_context_data(
                outlet_form=outlet_form,
                form=form,
                template_form=template_form,
                visibility_receipt_form=visibility_receipt_form
            )
        )


class ReceiptInitalView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def get_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            receipt_data = {}
            outlet_data = {}
            data = {}
            outlet_id = request.GET.get('outlet_id', '')
            outlet = Outlet.objects.get(pk=outlet_id)
            try:
                receipt = Receipt.objects.get(outlet=outlet)
                receipt_data['receipt'] = receipt.id
                receipt_data['website'] = receipt.website
                receipt_data['facebook'] = receipt.social_facebook
                receipt_data['twitter'] = receipt.social_twitter
                receipt_data['instagram'] = receipt.social_instagram
                receipt_data['notes'] = receipt.notes
                receipt_data['template'] = receipt.template
                receipt_data['receipt_header'] = receipt.receipt_header
                receipt_data['brand_logo'] = receipt.brand_logo.name
                receipt_data['visibility_receipt'] = receipt.visibility_receipt

                outlet_data['outlet'] = outlet.id
            except Receipt.DoesNotExist:
                receipt_data['template'] = Receipt.RECEIPT_TEMPLATE.template_1
                receipt_data['receipt'] = ''
                receipt_data['website'] = ''
                receipt_data['facebook'] = ''
                receipt_data['twitter'] = ''
                receipt_data['instagram'] = ''
                receipt_data['notes'] = ''
                receipt_data['receipt_header'] = ''
                receipt_data['brand_logo'] = ''
                receipt_data['visibility_receipt'] = ''

                outlet_data['outlet'] = outlet.id

            data['outlet'] = outlet_data
            data['receipt_data'] = receipt_data
            response['data'] = data
            status_code = 200
            response['status'] = 'success'
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
