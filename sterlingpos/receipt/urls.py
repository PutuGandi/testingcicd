from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views


app_name = 'receipt'
urlpatterns = [
    url(r'^branch/(?P<outlet_id>\d+)/$', views.ReceiptFormView.as_view(),
        name='receipt'),

    url(r'^data/$', views.ReceiptInitalView.as_view(),
        name='receipt_data'),

]
