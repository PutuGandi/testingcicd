from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = 'promo'
urlpatterns = [
    url(regex=r'^$', view=views.PromoListView.as_view(),
        name='promo_list'),
    url(regex=r'^data$', view=views.PromoListJson.as_view(),
        name='data_promo'),
    url(regex=r'^create/$', view=views.PromoCreateView.as_view(),
        name='promo_create'),

    url(r'^deactive/$', views.PromoDeactiveJson.as_view(),
        name='promo_deactive'),
    url(r'^activate/$', views.PromoActivateJson.as_view(),
        name='promo_activate'),
    url(r'^delete/$', views.PromoDeleteJson.as_view(),
        name='promo_delete'),
    url(regex=r'^edit/(?P<pk>\d+)/$', view=views.PromoUpdateView.as_view(),
        name='promo_update'),
    url(regex=r'^promo-tag/autocomplete/$', view=views.PromoTagAutoCompleteView.as_view(create_field='value'),
        name='promo_tag_autocomplete'),

    url(regex=r'^employee/$', view=views.EmployeePromoListView.as_view(),
        name='employee_promo_list'),
    url(regex=r'^employee/data$', view=views.EmployeePromoListJson.as_view(),
        name='employee_promo_data'),
    url(regex=r'^employee/create/$', view=views.EmployeePromoCreateView.as_view(),
        name='employee_promo_create'),
    url(regex=r'^employee/edit/(?P<pk>\d+)/$', view=views.EmployeePromoUpdateView.as_view(),
        name='employee_promo_update'),

    url(regex=r'^usages/data/$', view=views.PromoUsageReportListJson.as_view(),
        name='data_promo_usages'),
    url(regex=r'^usages/outlet/(?P<outlet_id>\d+)/data/$', view=views.PromoUsageReportListJson.as_view(),
        name='data_promo_usages'),
    url(regex=r'^usages/$', view=views.PromoUsageReportView.as_view(),
        name='promo_usages'),
    url(regex=r'^usages/(?P<promo_id>\d+)/data/$', view=views.PromoUsageListJson.as_view(),
        name='data_promo_usages_list'),
    url(regex=r'^usages/(?P<promo_id>\d+)/outlet/(?P<outlet_id>\d+)/data/$', view=views.PromoUsageListJson.as_view(),
        name='data_promo_usages_list'),
    url(regex=r'^usages/(?P<promo_id>\d+)/$', view=views.PromoUsageListView.as_view(),
        name='promo_usages_list'),
    url(regex=r'^usages/(?P<promo_id>\d+)/outlet/(?P<outlet_id>\d+)/$', view=views.PromoUsageListView.as_view(),
        name='promo_usages_list'),
]
