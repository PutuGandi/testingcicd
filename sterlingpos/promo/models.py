from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta, MO, SU

from django.conf import settings
from django.db import models
from django.db.models import Q, Count, Sum, Value, DecimalField, F
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext as _
from model_utils.models import TimeStampedModel
from django.template.defaultfilters import slugify

from django.contrib.postgres.fields import ArrayField
from safedelete.models import HARD_DELETE
from model_utils import Choices

from sterlingpos.core.models import (
    SterlingTenantModel, SterlingTenantForeignKey,
)
from sterlingpos.promo.managers import PromoManager


_DISCOUNT_TYPE = Choices(
    ('percentage', _('Percentage')),
    ('fixed_price', _('Fixed amount')),
    ('specified_amount', _('Specified amount')),

)

_ALLOW_DAYS = Choices(
    ('1', _('Senin')),
    ('2', _('Selasa')),
    ('3', _('Rabu')),
    ('4', _('Kamis')),
    ('5', _('Jumat')),
    ('6', _('Sabtu')),
    ('7', _('Minggu')),
)

_REWARD_TYPE_CHOICES = Choices(
    ('free_product', _('Free')),
    ('buy_product_in_discount_percentage', _('Percentage')),
    ('buy_product_in_discount_fixed_price', _('Fixed Price')),
    ('buy_product_in_specified_amount', _('Specified Amount')),
)


_TIME_LIMIT_CHOICES = Choices(
    ('daily', _('Daily')),
    ('weekly', _('Weekly')),
    ('monthly', _('Monthly')),
    ('no_recurrence', _('No Recurrence'))
)

def get_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = "{}_{}.{}".format(instance.promo_type, slugify(instance.name), ext)
    return '{}/promo/{}'.format(instance.account.pk, new_filename)


class Promo(SterlingTenantModel, TimeStampedModel):
    _PROMO_TYPE_CHOICES = Choices(
        ('discount', _('Discount')),
    )
    _PROMO_RULE_CHOICES = Choices(
        ('promo_global', _('Global Promo')),
        ('promo_product', _('Promo By Product')),
        ('promo_payment_method', _('Promo By Payment Method')),
        ('buy_a_get_b', _('Buy A get B')),
    )

    _ELIGIBILITY_CHOICES = Choices(
        ('everyone', _('Everyone')),
        ('selected_customer', _('Selected Customer')),
        ('selected_group', _('Selected Group'))
    )

    _PRODUCT_TYPE_CHOICE = Choices(
        ('product', _('Product')),
        ('category', _('Category'))
    )

    _USAGES_LIMIT = Choices(
        ('unlimited', _('Unlimited')),
        ('use_limit', _('Uses Limit')),
        ('amount_limit', _('Amount Limit'))
    )

    promo_type = models.CharField(
        max_length=120,
        choices=_PROMO_TYPE_CHOICES
    )
    promo_rule = models.CharField(
        max_length=120,
        choices=_PROMO_RULE_CHOICES,
        default=_PROMO_RULE_CHOICES.promo_global
    )

    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    allow_days = ArrayField(
        models.CharField(
            max_length=255,
            choices=_ALLOW_DAYS,
        ),
        blank=True,
        null=True
    )

    affected_product_type = models.CharField(
        max_length=120,
        choices=_PRODUCT_TYPE_CHOICE,
        default=_PRODUCT_TYPE_CHOICE.product
    )

    affected_category = models.ManyToManyField(
        "catalogue.Category",
        blank=True,
        through='CategoryPromo',
        related_name='category_promo'
    )

    affected_product = models.ManyToManyField(
        "catalogue.Product",
        blank=True,
        through="ProductPromo",
        related_name='product_promo')

    affected_payment_method = models.ManyToManyField(
        "sales.PaymentMethod",
        blank=True,
        through="PaymentMethodPromo",
        related_name='promo')

    affected_customer = models.ManyToManyField(
        "customer.Customer",
        blank=True,
        through="CustomerPromo",
        related_name="customer_promo"
    )

    affected_group = models.ManyToManyField(
        "customer.CustomerGroup",
        blank=True,
        through="CustomerGroupPromo",
        related_name='customer_group_promo'
    )

    customer_eligibility = models.CharField(
        max_length=120,
        choices=_ELIGIBILITY_CHOICES,
        default=_ELIGIBILITY_CHOICES.everyone
    )

    tags = models.ManyToManyField(
        "promo.PromoTag",
        blank=True,
        through="TagPromo",
        related_name="tag_promo"
    )

    # Validation Time
    start_hours = models.TimeField(blank=True, null=True)
    end_hours = models.TimeField(blank=True, null=True)
    outlet = models.ManyToManyField('outlet.Outlet', through='OutletPromo')

    allow_multiple = models.BooleanField(default=False)
    is_enforced = models.BooleanField(default=True)

    usages_limit = models.CharField(max_length=120,
                                    choices=_USAGES_LIMIT,
                                    default=_USAGES_LIMIT.unlimited)
    recurrence_limit = models.CharField(max_length=120, choices=_TIME_LIMIT_CHOICES,
    default=_TIME_LIMIT_CHOICES.no_recurrence)
    promo_limit = models.PositiveSmallIntegerField(blank=True, null=True)
    limit_per_customer = models.BooleanField(default=False)
    minimum_transaction = models.DecimalField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        blank=True, null=True)
    amount_limit = models.DecimalField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        blank=True, null=True)
    organizational = models.BooleanField(default=False)

    objects = PromoManager()

    class Meta:
        verbose_name = "Promo"
        verbose_name_plural = "Promos"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "{} - {}".format(self.promo_type, self.name)

    def _get_recurrence_filter(self):
        start_date = end_date = None

        if self.recurrence_limit == _TIME_LIMIT_CHOICES.daily:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0)
            end_date = start_date + relativedelta(hour=23, minute=59)

        if self.recurrence_limit == _TIME_LIMIT_CHOICES.weekly:
            start_date = datetime.today().replace(
                hour=0, minute=0, second=0, microsecond=0
            ) + relativedelta(weekday=MO(-1))
            end_date = datetime.today().replace(hour=23, minute=59)

        if self.recurrence_limit == _TIME_LIMIT_CHOICES.monthly:
            start_date = datetime.today().replace(
                day=1, hour=0, minute=0, second=0, microsecond=0)
            end_date = datetime.today().replace(hour=23, minute=59)

        if start_date and end_date:
            recurrence_filter = Q(
                sales_order__transaction_date__gte=start_date,
                sales_order__transaction_date__lte=end_date
            )
            return recurrence_filter

    def _get_use_limit(self, promo_history_qs):
        limit_dict = promo_history_qs.aggregate(
            count_usages=Count('pk')
        )
        limit_dict["count_left"] = self.promo_limit - limit_dict["count_usages"]
        limit_dict.update({
            "count_limit": self.promo_limit,
            "is_usable": limit_dict["count_left"] > 0
        })
        return limit_dict

    def _get_amount_limit(self, promo_history_qs):
        if self.promo_rule == Promo._PROMO_RULE_CHOICES.promo_product:
            limit_dict = promo_history_qs.aggregate(
                amount_usages=Coalesce(
                    Sum(
                        F('affected_value')*F('product_quantity')),
                    Value(0),
                    output_field=DecimalField()),
            )
        else:
            limit_dict = promo_history_qs.aggregate(
                amount_usages=Coalesce(
                    Sum('affected_value'),
                    Value(0),
                    output_field=DecimalField()),
            )
        limit_dict["amount_left"] = self.amount_limit - limit_dict["amount_usages"]
        limit_dict.update({
            "amount_limit": self.amount_limit,
            "is_usable": limit_dict["amount_left"] > 0
        })
        return limit_dict

    def _get_unlimited(self):
        return {}

    def _get_limitation_for(self, *args, **kwargs):
        limitation_data = {}
        
        promo_history = self.salesorderpromohistory_set.filter(
            sales_order__transaction_date__gte=self.start_date,
            sales_order__status='settled')

        recurence_filters = self._get_recurrence_filter()
        if recurence_filters:
            promo_history = promo_history.filter(
                recurence_filters)
        
        if self.limit_per_customer or self.usages_limit == "amount_limit":
            promo_history = promo_history.filter(
                affected_customer=kwargs.get('affected_customer')
            )

        if self.usages_limit:
            limitation_check_func = getattr(
                self, f"_get_{self.usages_limit}")
            limitation_data = limitation_check_func(promo_history)

        return limitation_data

    def get_limitation_for(self, *args, **kwargs):
        return self._get_limitation_for(*args, **kwargs)


class PaymentMethodPromo(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE

    promo = models.ForeignKey(
        'promo.Promo', on_delete=models.CASCADE)
    payment_method = models.ForeignKey(
        'sales.PaymentMethod', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "PaymentMethodPromo"
        verbose_name_plural = "PaymentMethodPromos"


class ProductPromo(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE

    promo = models.ForeignKey(
        'promo.Promo', on_delete=models.CASCADE)
    product = models.ForeignKey(
        'catalogue.Product', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "ProductPromo"
        verbose_name_plural = "ProductPromos"


class CategoryPromo(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE

    promo = models.ForeignKey(
        'promo.Promo', on_delete=models.CASCADE)
    category = models.ForeignKey(
        'catalogue.Category', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "CategoryPromo"
        verbose_name_plural = "CategoryPromos"


class OutletPromo(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE

    promo = models.ForeignKey('promo.Promo', on_delete=models.CASCADE)
    outlet = models.ForeignKey('outlet.Outlet', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "OutletPromo"
        verbose_name_plural = "OutletPromos"
        unique_together = (('account', 'id'),)


class CustomerPromo(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE

    promo = models.ForeignKey('promo.Promo', on_delete=models.CASCADE)
    customer = models.ForeignKey('customer.Customer', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'CustomerPromo'
        verbose_name_plural = 'CustomerPromos'
        unique_together = (('account', 'id'),)


class CustomerGroupPromo(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE

    promo = models.ForeignKey('promo.Promo', on_delete=models.CASCADE)
    customer_group = models.ForeignKey('customer.CustomerGroup', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'CustomerGroupPromo'
        verbose_name_plural = 'CustomerGroupPromos'
        unique_together = (('account', 'id'),)


class TagPromo(SterlingTenantModel):
    _safedelete_policy = HARD_DELETE

    promo = models.ForeignKey('promo.Promo', on_delete=models.CASCADE)
    promo_tag = models.ForeignKey('promo.PromoTag', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'TagPromo'
        verbose_name_plural = 'TagPromos'
        unique_together = (('account', 'id'),)


class PromoReward(SterlingTenantModel, TimeStampedModel):
    promo = models.OneToOneField('promo.Promo', on_delete=models.CASCADE)
    discount_type = models.CharField(
        max_length=120,
        choices=_DISCOUNT_TYPE
    )
    discount_percentage = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    discount_fixed_price = models.DecimalField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        blank=True, null=True)
    discount_amount_limit = models.DecimalField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        blank=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.promo, self.discount_type)

    class Meta:
        verbose_name = "PromoReward"
        verbose_name_plural = "PromoRewards"
        unique_together = (('account', 'id'),)

    def discount():

        def fget(self):
            if self.discount_type == _DISCOUNT_TYPE.percentage:
                return self.discount_percentage
            else:
                return self.discount_fixed_price

        def fset(self, value):
            if self.discount_type == _DISCOUNT_TYPE.percentage:
                self.discount_percentage = value
                self.discount_fixed_price = None
            else:
                self.discount_fixed_price = value
                self.discount_percentage = None

        def fdel(self):
            self.discount_percentage = None
            self.discount_fixed_price = None

        return locals()

    discount = property(**discount())


class ItemPromoReward(SterlingTenantModel, TimeStampedModel):
    _ITEM_TYPE_CHOICES = Choices(
        ('product', _('Product')),
        ('category', _('Category')),
    )
    promo = models.OneToOneField('promo.Promo', blank=True, null=True, on_delete=models.CASCADE)
    reward_buy_quantity = models.PositiveSmallIntegerField(blank=True, null=True)
    product_buy = SterlingTenantForeignKey('catalogue.Product', related_name='product_buy',
                                           on_delete=models.CASCADE, null=True, blank=True)
    category_buy = SterlingTenantForeignKey('catalogue.Category', related_name='category_buy',
                                            on_delete=models.CASCADE, null=True, blank=True)
    item_buy_type = models.CharField(
        max_length=120,
        choices=_ITEM_TYPE_CHOICES,
        default=_ITEM_TYPE_CHOICES.product
    )

    reward_type = models.CharField(
        max_length=120,
        choices=_REWARD_TYPE_CHOICES,
        default=_REWARD_TYPE_CHOICES.free_product
    )

    reward_get_quantity = models.PositiveSmallIntegerField(blank=True, null=True)
    product_get = SterlingTenantForeignKey('catalogue.Product', related_name='product_get',
                                           on_delete=models.CASCADE, null=True, blank=True)
    category_get = SterlingTenantForeignKey('catalogue.Category', related_name='category_get',
                                            on_delete=models.CASCADE, null=True, blank=True)
    item_get_type = models.CharField(
        max_length=120,
        choices=_ITEM_TYPE_CHOICES,
        default=_ITEM_TYPE_CHOICES.product
    )
    reward_percentage = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    reward_fixed_price = models.DecimalField(
        max_digits=settings.HIGH_DECIMAL_MAX_DIGITS,
        decimal_places=settings.DECIMAL_PLACES,
        blank=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.promo, self.reward_type)

    class Meta:
        verbose_name = "ProductReward"
        verbose_name_plural = "ProductRewards"
        unique_together = (('account', 'id'),)

    def reward():
        def fget(self):
            if self.reward_type == _REWARD_TYPE_CHOICES.buy_product_in_discount_percentage:
                return self.reward_percentage
            else:
                return self.reward_fixed_price

        def fset(self, value):
            if self.reward_type == _REWARD_TYPE_CHOICES.free_product:
                self.reward_percentage = None
                self.reward_fixed_price = None
            elif self.reward_type == _REWARD_TYPE_CHOICES.buy_product_in_discount_percentage:
                self.reward_percentage = value
                self.reward_fixed_price = None
            else:
                self.reward_percentage = None
                self.reward_fixed_price = value

        def fdel(self):
            self.reward_percentage = None
            self.reward_fixed_price = None

        return locals()

    reward = property(**reward())


class PromoTag(SterlingTenantModel, TimeStampedModel):
    value = models.CharField(_('Promo Tag'), max_length=50)

    class Meta:
        verbose_name = "PromoTag"
        verbose_name_plural = "PromoTags"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "{}".format(
             self.value)
