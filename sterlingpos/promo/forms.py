from django import forms
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django_multitenant.utils import get_current_tenant

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, HTML
from dal import autocomplete
from model_utils import Choices

from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField, KawnFieldSet, KawnAppendedText
from sterlingpos.promo.models import (
    Promo,
    OutletPromo,
    ProductPromo,
    PaymentMethodPromo,
    PromoReward,
    _DISCOUNT_TYPE,
    _ALLOW_DAYS,
    CustomerPromo,
    CustomerGroupPromo,
    _REWARD_TYPE_CHOICES,
    ItemPromoReward,
    CategoryPromo,
    PromoTag,
    TagPromo,
    _TIME_LIMIT_CHOICES,
)
from sterlingpos.outlet.models import Outlet
from sterlingpos.catalogue.models import Product, Category
from sterlingpos.sales.models import PaymentMethod
from sterlingpos.customer.models import Customer, CustomerGroup

current_tenant = get_current_tenant()

MAX_UPLOAD_SIZE = 4194304  # 4MB

_PROMO_RULES_CHOICES = Choices(
    ('promo_global', _('Global Promo')),
    ('promo_product', _('Promo By Product')),
    ('promo_payment_method', _('Promo By Payment Method')),
)


class PromoForm(forms.ModelForm):
    all_days = forms.BooleanField(initial=True, label="Setiap Hari")
    allow_days = forms.MultipleChoiceField(
        choices=_ALLOW_DAYS,
        widget=forms.CheckboxSelectMultiple,
        label=''
    )
    all_outlet = forms.BooleanField(initial=True, label="Semua Lokasi")
    outlet = forms.ModelMultipleChoiceField(
        queryset=None, label='', widget=forms.CheckboxSelectMultiple)

    discount_type = forms.ChoiceField(
        choices=_DISCOUNT_TYPE,
        widget=forms.RadioSelect,
        label='',
        initial=_DISCOUNT_TYPE.percentage
    )
    discount = forms.DecimalField(max_digits=19, decimal_places=2, label='')
    minimum_transaction = forms.DecimalField(
        max_digits=19, decimal_places=2, label='Requirement Value')
    discount_amount_limit = forms.DecimalField(
        max_digits=19, decimal_places=2, label='Limit Value')

    enable_minimum_transaction = forms.BooleanField(
        initial=False, label='Minimum Purchase Amount (IDR)')
    enable_discount_amount_limit = forms.BooleanField(
        initial=False, label='Discount Amount Limit (IDR)')

    affected_category = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:category_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Category.objects.none()
    )

    affected_product = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )
    affected_payment_method = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='sales:payment_method_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=PaymentMethod.objects.none()
    )

    affected_customer = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='customers:customer_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Customer.objects.none()
    )
    affected_group = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='customers:customer_group_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=CustomerGroup.objects.none()
    )

    tags = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='promo:promo_tag_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=PromoTag.objects.none()
    )

    reward_buy_quantity = forms.IntegerField(initial=0, label=_("Quantity"))
    reward_get_quantity = forms.IntegerField(initial=0, label=_("Quantity"))
    reward_type = forms.ChoiceField(
        choices=_REWARD_TYPE_CHOICES,
        widget=forms.RadioSelect,
        label=_('Discount Value'),
        initial=_REWARD_TYPE_CHOICES.free_product
    )
    reward = forms.DecimalField(max_digits=19, decimal_places=2, label='Amount')
    product_buy = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:product_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )
    product_get = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:product_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )
    category_buy = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:category_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Category.objects.none()
    )
    category_get = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:category_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Category.objects.none()
    )
    item_buy_type = forms.ChoiceField(
        choices=ItemPromoReward._ITEM_TYPE_CHOICES,
        widget=forms.Select,
        label=_('Pilih Produk Berdasarkan')
    )
    item_get_type = forms.ChoiceField(
        choices=ItemPromoReward._ITEM_TYPE_CHOICES,
        widget=forms.Select,
        label=_('Pilih Produk Berdasarkan')
    )
    customer_eligibility = forms.ChoiceField(
        choices=Promo._ELIGIBILITY_CHOICES,
        widget=forms.RadioSelect,
        initial=Promo._ELIGIBILITY_CHOICES.everyone
    )
    enable_end_date = forms.BooleanField(initial=False, label='Atur Tanggal Berakhir')
    happy_hours = forms.BooleanField(initial=False, label='Atur Waktu Promo')

    affected_product_type = forms.ChoiceField(
        choices=Promo._PRODUCT_TYPE_CHOICE,
        widget=forms.RadioSelect,
        initial=Promo._PRODUCT_TYPE_CHOICE.product
    )

    promo_rule = forms.ChoiceField(
        choices=_PROMO_RULES_CHOICES,
        widget=forms.RadioSelect,
        initial=_PROMO_RULES_CHOICES.promo_global,
        label=''
    )
    usages_limit = forms.ChoiceField(
        choices=Promo._USAGES_LIMIT,
        widget=forms.RadioSelect,
        initial=Promo._USAGES_LIMIT.unlimited,
        label=''
    )
    amount_limit = forms.DecimalField(max_digits=19, decimal_places=2, label='Amount Limit')
    recurrence_limit = forms.ChoiceField(
        choices=_TIME_LIMIT_CHOICES,
        widget=forms.RadioSelect,
        initial=_TIME_LIMIT_CHOICES.no_recurrence,
        label=''
    )
    optional_affected_products = forms.BooleanField(initial=False, label='Choose Product')

    class Meta:
        model = Promo
        fields = '__all__'
        exclude = (
            'is_active',
            'account',
        )

    def clean(self):
        cleaned_data = super(PromoForm, self).clean()
        cleaned_data['promo_type'] = Promo._PROMO_TYPE_CHOICES.discount

        if (cleaned_data.get('promo_rule') != Promo._PROMO_RULE_CHOICES.buy_a_get_b and cleaned_data.get(
            'discount_type') == _DISCOUNT_TYPE.percentage
            and cleaned_data.get('discount', 0) > 100):
            raise forms.ValidationError(
                _("Invalid Discount Percentage Value."),
                code='invalid_discount_percentage')
        if (cleaned_data.get('promo_rule') != Promo._PROMO_RULE_CHOICES.buy_a_get_b and cleaned_data.get(
            'reward_type') == _REWARD_TYPE_CHOICES.buy_product_in_discount_percentage and
            cleaned_data.get('reward', 0) > 100):
            raise forms.ValidationError(
                _("Invalid Reward Percentage Value."),
                code='invalid_reward_percentage')
        if (cleaned_data.get('promo_rule') == Promo._PROMO_RULE_CHOICES.buy_a_get_b):
            if (cleaned_data.get('reward_buy_quantity') <= 0 or cleaned_data.get('reward_get_quantity') <= 0):
                raise forms.ValidationError(
                    _("Invalid Buy or Get Item Quantity."),
                    code='invalid_reward_buy_and_get_quantity')
            if (cleaned_data.get('reward_type') == _REWARD_TYPE_CHOICES.buy_product_in_specified_amount):
                if cleaned_data.get('item_get_type') == ItemPromoReward._ITEM_TYPE_CHOICES.product:
                    product_price = cleaned_data.get('product_get').price
                    quantity = cleaned_data.get('reward_get_quantity')
                    price = product_price.amount * quantity
                    reward_price = cleaned_data.get('reward')
                    if reward_price is None:
                        raise forms.ValidationError(_("You must fill the reward value"))
                    if reward_price > price:
                        raise forms.ValidationError(_("Reward price is over base cost. Your reward price is {} but "
                                                      "your total product cost is {} "
                                                      "").format(cleaned_data.get('reward'), price),
                                                    code='invalid_reward_price')
                if cleaned_data.get('item_get_type') == ItemPromoReward._ITEM_TYPE_CHOICES.category:
                    category_id = cleaned_data.get('category_get').id
                    price = cleaned_data.get('reward')
                    if price is None:
                        raise forms.ValidationError(_("You must fill the reward value"))
                    product_list = Product.objects.filter(category=category_id, archived=False).exclude(
                        classification=Product.PRODUCT_CLASSIFICATION.complementary)
                    for product in product_list:
                        if product.price.amount < price:
                            raise forms.ValidationError(_("Reward price is over base cost. Your reward price should "
                                                          "be under {}").format(product.price.amount),
                                                        code='invalid_reward_price')

        if (cleaned_data.get('promo_rule') == Promo._PROMO_RULE_CHOICES.promo_product or
            Promo._PROMO_RULE_CHOICES.promo_payment_method):
            if (cleaned_data.get('affected_product_type') == Promo._PRODUCT_TYPE_CHOICE.product and cleaned_data.get(
                'discount_type') == _DISCOUNT_TYPE.specified_amount):
                product_list = cleaned_data.get('affected_product')
                discount_price = cleaned_data.get('discount')
                if discount_price is None:
                    raise forms.ValidationError(_("You must fill the discount value"))
                if product_list is None:
                    raise forms.ValidationError(_("You must select at least one product"))
                else:
                    for product in product_list:
                        if product.price.amount < discount_price:
                            raise forms.ValidationError(
                                _("Discount price is over base cost. Your promo price should be "
                                  "under {}").format(product.price.amount),
                                code='invalid_discount_price')
            if (cleaned_data.get('affected_product_type') == Promo._PRODUCT_TYPE_CHOICE.category and cleaned_data.get(
                'discount_type') == _DISCOUNT_TYPE.specified_amount):
                category_list = cleaned_data.get('affected_category')
                discount_price = cleaned_data.get('discount')
                if discount_price is None:
                    raise forms.ValidationError(_("You must fill the discount value"))
                if category_list is None:
                    raise forms.ValidationError(_("You must select at least one category"))
                else:
                    for category in category_list:
                        product_list = Product.objects.filter(category=category.id, archived=False).exclude(
                            classification=Product.PRODUCT_CLASSIFICATION.complementary)
                        for product in product_list:
                            if product.price.amount < discount_price:
                                raise forms.ValidationError(
                                    _("Discount price is over base cost. Your promo price should "
                                      "be under {}").format(product.price.amount),
                                    code='invalid_discount_price')
        if (cleaned_data.get('end_date') is not None):
            if (cleaned_data.get('start_date') > cleaned_data.get('end_date')):
                raise forms.ValidationError(
                    _('Start date is beyond end date'),
                    code='invalid_end_date'
                )

        if (cleaned_data.get('customer_eligibility') == Promo._ELIGIBILITY_CHOICES.selected_customer):
            if (len(cleaned_data.get('affected_customer')) == 0):
                raise forms.ValidationError(_("You must select at least one customer"))
        if (cleaned_data.get('customer_eligibility') == Promo._ELIGIBILITY_CHOICES.selected_group):
            if (len(cleaned_data.get('affected_group')) == 0):
                raise forms.ValidationError(_("You must select at least one group"))
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(PromoForm, self).__init__(*args, **kwargs)
        self.fields['affected_product'].queryset = Product.objects.exclude(
            classification=Product.PRODUCT_CLASSIFICATION.complementary)
        self.fields['tags'].label = _('Tags')
        if self.data and self.data.get('promo_rule', None) == Promo._PROMO_RULE_CHOICES.buy_a_get_b:
            self.fields['discount'].required = False
            self.fields['discount_type'].required = False
            self.fields['reward_type'].required = True
            if self.data and self.data.get('item_buy_type', None) == ItemPromoReward._ITEM_TYPE_CHOICES.product:
                self.fields['product_buy'].required = True
                self.fields['category_buy'].required = False
            else:
                self.fields['product_buy'].required = False
                self.fields['category_buy'].required = True
            if self.data and self.data.get('item_get_type', None) == ItemPromoReward._ITEM_TYPE_CHOICES.product:
                self.fields['product_get'].required = True
                self.fields['category_get'].required = False
            else:
                self.fields['product_get'].required = False
                self.fields['category_get'].required = True
            if self.data and self.data.get('reward_type', None) == _REWARD_TYPE_CHOICES.free_product:
                self.fields['reward'].required = False
            else:
                self.fields['reward'].required = True
            self.fields['reward_buy_quantity'].required = True
            self.fields['reward_get_quantity'].required = True
        else:
            self.fields['discount'].required = True
            self.fields['discount_type'].required = True
            self.fields['reward_type'].required = False
            self.fields['product_buy'].required = False
            self.fields['product_get'].required = False
            self.fields['category_buy'].required = False
            self.fields['category_get'].required = False
            self.fields['reward_buy_quantity'].required = False
            self.fields['reward_get_quantity'].required = False
            self.fields['reward'].required = False

        if self.data and self.data.get('promo_rule', None) == Promo._PROMO_RULE_CHOICES.promo_product:
            if self.data and self.data.get('affected_product_type', None) == Promo._PRODUCT_TYPE_CHOICE.product:
                self.fields['affected_product'].required = True
                self.fields['affected_category'].required = False
            else:
                self.fields['affected_product'].required = False
                self.fields['affected_category'].required = True

        if self.data and self.data.get('promo_rule', None) == Promo._PROMO_RULE_CHOICES.promo_payment_method:
            self.fields['affected_product'].required = False
            self.fields['affected_category'].required = False
            self.fields['affected_product_type'].required = False
            self.fields['affected_payment_method'].required = True

        self.fields['affected_payment_method'].queryset = self.get_payment_method_queryset()
        self.fields['affected_customer'].queryset = Customer.objects.filter(is_active=True)
        self.fields['affected_category'].queryset = Category.objects.all()
        self.fields['affected_group'].queryset = CustomerGroup.objects.filter(is_active=True)
        self.fields['outlet'].queryset = Outlet.objects.all()
        self.fields['tags'].queryset = PromoTag.objects.all()
        self.fields['product_buy'].queryset = Product.objects.exclude(
            classification=Product.PRODUCT_CLASSIFICATION.complementary)
        self.fields['product_get'].queryset = Product.objects.exclude(
            classification=Product.PRODUCT_CLASSIFICATION.complementary)
        self.fields['category_buy'].queryset = Category.objects.exclude(name="Paketan",
                                                                        classification=Category.CATEGORY_CLASSIFICATION.composite,
                                                                        depth=1)
        self.fields['category_get'].queryset = Category.objects.exclude(name="Paketan",
                                                                        classification=Category.CATEGORY_CLASSIFICATION.composite,
                                                                        depth=1)
        self.fields['allow_days'].label = ''
        self.fields['is_enforced'].label = 'Promo ini berlaku otomatis'
        self.fields['start_hours'].label = 'Jam Mulai'
        self.fields['end_hours'].label = 'Jam Berakhir'
        self.fields['discount'].label = 'Discount Value'
        self.fields['customer_eligibility'].label = _("Apply to")
        self.fields['discount_type'].label = _("Discount Type")
        self.fields['affected_product_type'].label = _("Product Type")
        self.fields['promo_limit'].label = _('Limitation')
        self.fields['start_hours'].required = False
        self.fields['end_hours'].required = False
        self.fields['all_days'].required = False
        self.fields['all_outlet'].required = False
        self.fields['enable_end_date'].required = False
        self.fields['happy_hours'].required = False
        self.fields['enable_minimum_transaction'].required = False
        self.fields['minimum_transaction'].required = False
        self.fields['discount_amount_limit'].required = False
        self.fields['enable_discount_amount_limit'].required = False
        self.fields['limit_per_customer'].required = False
        self.fields['amount_limit'].required = False
        self.fields['promo_limit'].required = False
        self.fields['organizational'].required = False
        self.fields['optional_affected_products'].required = False
        self.fields['optional_affected_products'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak berlaku pada tipe product atau category. )')
        self.fields['is_enforced'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak ditampilkan secara otomatis pada transaksi. )')
        self.fields['all_days'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak berlaku setiap hari. )'
        )
        self.fields['all_outlet'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak berlaku pada gerai yang baru. )'
        )
        self.fields['amount_limit'].help_text = _('(Tracked by registered customer)')
        self.fields['promo_type'].required = False
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-2"
        self.helper.label_class = "m-0 pt-0"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.help_text_inline = True

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "Promo Information",
                KawnField("name"),
                KawnField("tags"),
                KawnField("organizational", type="hidden"),
                help_text=render_to_string("help_text/promo_information.html"),
            ),
            KawnFieldSetWithHelpText(
                "Tipe Promo",
                KawnField("promo_rule"),
                help_text=render_to_string("help_text/promo_type.html"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('is_enforced'),
                css_id='item_enforced'
            ),
            KawnFieldSetWithHelpText(
                _("Promo Settings"),
                KawnField("discount_type"
                          ),
                KawnAppendedText("discount", "%", css_id='div_id_discount'),
                css_id='promo_settings_1',
            ),
            KawnFieldSetWithHelpText(
                "Promo Settings",
                Div(
                    HTML(
                        '<h5>{}</h5>'.format(_('Customer Buy'))
                    ),
                    css_class="col-lg-12 p-0",
                    css_id='customer_buy_header'
                ),
                KawnField('reward_buy_quantity'),
                KawnField('item_buy_type', css_class="custom-select"),
                KawnField('product_buy', css_class="custom-select"),
                KawnField('category_buy', css_class="custom-select"),
                Div(
                    HTML(
                        '<hr/><h5>{}</h5>'.format(_('Customer Get'))
                    ),
                    css_class="col-lg-12 p-0",
                    css_id='customer_get_header'
                ),
                KawnField('reward_get_quantity'),
                KawnField('item_get_type', css_class="custom-select"),
                KawnField('product_get', css_class="custom-select"),
                KawnField('category_get', css_class="custom-select"),
                KawnField('reward_type'),
                KawnAppendedText('reward', "%", css_class="div_id_reward"),
                css_id='promo_settings_2',
                help_text=render_to_string("help_text/promo_settings.html"),
            ),
            KawnFieldSetWithHelpText(
                _("Choose Product"),
                KawnField("optional_affected_products"),
                KawnField("affected_product_type"),
                KawnField('affected_product'),
                KawnField('affected_category'),
                help_text=render_to_string("help_text/affected_product.html"),
                css_id='affected_product',
            ),
            KawnFieldSetWithHelpText(
                _("Affected Payment Method"),
                KawnField('affected_payment_method'),
                optional_legend=True,
                small_desc=_(
                    "( Jika centang ini dihapus, promo ini tidak berlaku pada tipe pembayaran. )"),
                css_id='affected_payment_method',
                help_text=render_to_string("help_text/affected_payment_method.html"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField("enable_minimum_transaction"),
                KawnAppendedText("minimum_transaction", "IDR", css_id="div_id_minimum_transaction")
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField("enable_discount_amount_limit"),
                KawnAppendedText("discount_amount_limit", "IDR", css_id="div_id_discount_amount_limit"),
                css_id='discount_amount_limit',
            ),
            KawnFieldSetWithHelpText(
                _("Customer Eligibility"),
                KawnField('customer_eligibility'),
                KawnField('affected_customer'),
                KawnField('affected_group'),
                help_text=render_to_string("help_text/customer_eligibility.html"),
            ),
            KawnFieldSetWithHelpText(
                _("Usages Limit"),
                KawnField('usages_limit'),
                KawnField('promo_limit'),
                KawnField('limit_per_customer'),
                KawnAppendedText('amount_limit', "IDR"),
                KawnField('recurrence_limit')
            ),
            KawnFieldSetWithHelpText(
                "Tanggal Berlaku",
                KawnField("start_date"),
                KawnField("enable_end_date"),
                KawnField("end_date")

            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_days'),
                KawnField('allow_days'),
                KawnField('happy_hours'),
                KawnField("start_hours"),
                KawnField("end_hours"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet'),
                KawnField('outlet'),
            ),
        )

    def clean_promo_limit(self):
        promo_limit = self.cleaned_data['promo_limit']
        usages_limit = self.cleaned_data['usages_limit']
        if usages_limit == Promo._USAGES_LIMIT.use_limit:
            if promo_limit is None:
                raise forms.ValidationError(_("This field is required"))
        return promo_limit

    def clean_amount_limit(self):
        amount_limit = self.cleaned_data['amount_limit']
        usages_limit = self.cleaned_data['usages_limit']
        if usages_limit == Promo._USAGES_LIMIT.amount_limit:
            if amount_limit is None:
                raise forms.ValidationError(_("This field is required"))
        return amount_limit

    def get_payment_method_queryset(self):

        if current_tenant:
            return PaymentMethod.objects.filter(
                account=current_tenant,
                enabledpaymentmethod__isnull=False
            )
        else:
            return PaymentMethod.objects.all()

    def update_m2m(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "{}".format(intermediate_model_ref.field.name): instance,
                    "account": instance.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids,
            "{}".format(intermediate_model_ref.field.name): instance,
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def update_outlet(self, promo, outlet_list):
        outlet_list = outlet_list.only('pk')
        current_outlet = promo.outlet.filter(
            pk__in=self.fields['outlet'].queryset
        ).only('pk')
        deleted_outlet = current_outlet.difference(outlet_list)

        OutletPromo.objects.filter(
            outlet__pk__in=deleted_outlet.values_list('pk', flat=True)).delete()

        for new_outlet in outlet_list.difference(current_outlet):
            OutletPromo.objects.create(
                outlet=new_outlet, promo=promo)
        promo.outlet.invalidated_update()

    def update_promo_reward(self, promo):
        discount_type = self.cleaned_data['discount_type']
        discount = self.cleaned_data['discount']
        discount_amount_limit = self.cleaned_data['discount_amount_limit']

        if not hasattr(promo, 'promoreward'):
            promo.promoreward = PromoReward.objects.create(
                discount_type=discount_type,
                promo=promo,
                discount=discount,
                discount_amount_limit=discount_amount_limit,
            )
        else:
            promo.promoreward.discount_amount_limit = discount_amount_limit
            promo.promoreward.discount_type = discount_type
            promo.promoreward.discount = discount
            promo.promoreward.save()

    def update_item_promo_reward(self, promo):
        reward_type = self.cleaned_data['reward_type']
        reward = self.cleaned_data['reward']
        product_buy = self.cleaned_data['product_buy']
        product_get = self.cleaned_data['product_get']
        category_buy = self.cleaned_data['category_buy']
        category_get = self.cleaned_data['category_get']
        item_buy_type = self.cleaned_data['item_buy_type']
        item_get_type = self.cleaned_data['item_get_type']
        reward_buy_quantity = self.cleaned_data['reward_buy_quantity']
        reward_get_quantity = self.cleaned_data['reward_get_quantity']
        if not hasattr(promo, 'itempromoreward'):
            promo.itempromoreward = ItemPromoReward.objects.create(
                reward_type=reward_type,
                promo=promo,
                reward=reward,
                product_buy=product_buy,
                category_buy=category_buy,
                reward_buy_quantity=reward_buy_quantity,
                product_get=product_get,
                category_get=category_get,
                item_get_type=item_get_type,
                item_buy_type=item_buy_type,
                reward_get_quantity=reward_get_quantity,
            )
        else:
            promo.itempromoreward.reward_type = reward_type
            promo.itempromoreward.reward = reward
            promo.itempromoreward.product_get = product_get
            promo.itempromoreward.category_get = category_get
            promo.itempromoreward.item_get_type = item_get_type
            promo.itempromoreward.reward_get_quantity = reward_get_quantity
            promo.itempromoreward.product_buy = product_buy
            promo.itempromoreward.category_buy = category_buy
            promo.itempromoreward.item_buy_type = item_buy_type
            promo.itempromoreward.reward_buy_quantity = reward_buy_quantity
            promo.itempromoreward.save()

    def save(self, *args, **kwargs):
        outlet_list = self.cleaned_data.pop('outlet')
        payment_method_list = self.cleaned_data.pop('affected_payment_method')
        customer_list = self.cleaned_data.pop('affected_customer')
        group_list = self.cleaned_data.pop('affected_group')
        tags_list = self.cleaned_data.pop('tags')

        product_list = self.cleaned_data.pop('affected_product')
        category_list = self.cleaned_data.pop('affected_category')

        promo = super(PromoForm, self).save(*args, **kwargs)

        if promo.affected_product_type == Promo._PRODUCT_TYPE_CHOICE.category:
            product_list = product_list.none()
        else:
            category_list = category_list.none()

        self.update_m2m(
            promo, outlet_list,
            promo.outlet,
            OutletPromo, OutletPromo.promo
        )
        self.update_m2m(
            promo, product_list,
            promo.affected_product,
            ProductPromo, ProductPromo.promo)
        self.update_m2m(
            promo, payment_method_list,
            promo.affected_payment_method,
            PaymentMethodPromo, PaymentMethodPromo.promo)
        self.update_m2m(
            promo, customer_list,
            promo.affected_customer,
            CustomerPromo, CustomerPromo.promo
        )
        self.update_m2m(
            promo, group_list,
            promo.affected_group,
            CustomerGroupPromo, CustomerGroupPromo.promo
        )
        self.update_m2m(
            promo, category_list,
            promo.affected_category,
            CategoryPromo, CategoryPromo.promo
        )
        self.update_m2m(
            promo, tags_list,
            promo.tags,
            TagPromo, TagPromo.promo
        )
        self.update_promo_reward(promo)
        self.update_item_promo_reward(promo)
        return promo


class EmployeePromoForm(forms.ModelForm):
    all_days = forms.BooleanField(initial=True, label="Setiap Hari")
    allow_days = forms.MultipleChoiceField(
        choices=_ALLOW_DAYS,
        widget=forms.CheckboxSelectMultiple,
        label=''
    )
    all_outlet = forms.BooleanField(initial=True, label="Semua Lokasi")
    outlet = forms.ModelMultipleChoiceField(
        queryset=None, label='', widget=forms.CheckboxSelectMultiple)

    discount_type = forms.ChoiceField(
        choices=_DISCOUNT_TYPE,
        widget=forms.RadioSelect,
        label='',
        initial=_DISCOUNT_TYPE.percentage
    )
    discount = forms.DecimalField(max_digits=19, decimal_places=2, label='')
    minimum_transaction = forms.DecimalField(max_digits=19, decimal_places=2, label='Requirement Value')

    affected_category = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:category_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Category.objects.none()
    )

    affected_product = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='catalogue:product_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )
    affected_payment_method = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='sales:payment_method_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=PaymentMethod.objects.none()
    )

    affected_customer = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='customers:customer_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Customer.objects.none()
    )
    affected_group = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='customers:customer_group_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=CustomerGroup.objects.none()
    )

    tags = forms.ModelMultipleChoiceField(
        widget=autocomplete.ModelSelect2Multiple(
            url='promo:promo_tag_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=PromoTag.objects.none()
    )

    reward_buy_quantity = forms.IntegerField(initial=0, label=_("Quantity"))
    reward_get_quantity = forms.IntegerField(initial=0, label=_("Quantity"))
    reward_type = forms.ChoiceField(
        choices=_REWARD_TYPE_CHOICES,
        widget=forms.RadioSelect,
        label=_('Discount Value'),
        initial=_REWARD_TYPE_CHOICES.free_product
    )
    reward = forms.DecimalField(max_digits=19, decimal_places=2, label='Amount')
    product_buy = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:product_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )
    product_get = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:product_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Product.objects.none()
    )
    category_buy = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:category_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Category.objects.none()
    )
    category_get = forms.ModelChoiceField(
        widget=autocomplete.ModelSelect2(
            url='catalogue:category_promo_autocomplete',
            attrs={
                'data-minimum-input-length': 0,
            },
        ),
        required=False,
        queryset=Category.objects.none()
    )
    item_buy_type = forms.ChoiceField(
        choices=ItemPromoReward._ITEM_TYPE_CHOICES,
        widget=forms.Select,
        label=_('Pilih Produk Berdasarkan')
    )
    item_get_type = forms.ChoiceField(
        choices=ItemPromoReward._ITEM_TYPE_CHOICES,
        widget=forms.Select,
        label=_('Pilih Produk Berdasarkan')
    )
    customer_eligibility = forms.ChoiceField(
        choices=Promo._ELIGIBILITY_CHOICES,
        widget=forms.RadioSelect,
        initial=Promo._ELIGIBILITY_CHOICES.everyone
    )
    enable_end_date = forms.BooleanField(initial=False, label='Atur Tanggal Berakhir')
    happy_hours = forms.BooleanField(initial=False, label='Atur Waktu Promo')

    affected_product_type = forms.ChoiceField(
        choices=Promo._PRODUCT_TYPE_CHOICE,
        widget=forms.RadioSelect,
        initial=Promo._PRODUCT_TYPE_CHOICE.product
    )
    enable_minimum_transaction = forms.BooleanField(initial=False, label='Minimum Purchase Amount (IDR)')
    promo_rule = forms.ChoiceField(
        choices=_PROMO_RULES_CHOICES,
        widget=forms.RadioSelect,
        initial=_PROMO_RULES_CHOICES.promo_global,
        label=''
    )
    usages_limit = forms.ChoiceField(
        choices=Promo._USAGES_LIMIT,
        widget=forms.RadioSelect,
        initial=Promo._USAGES_LIMIT.unlimited,
        label=''
    )
    amount_limit = forms.DecimalField(max_digits=19, decimal_places=2, label='Amount Limit')
    recurrence_limit = forms.ChoiceField(
        choices=_TIME_LIMIT_CHOICES,
        widget=forms.RadioSelect,
        initial=_TIME_LIMIT_CHOICES.no_recurrence,
        label=''
    )
    optional_affected_products = forms.BooleanField(initial=False, label='Choose Product')

    class Meta:
        model = Promo
        fields = '__all__'
        exclude = (
            'is_active',
            'account',
        )

    def clean(self):
        cleaned_data = super(EmployeePromoForm, self).clean()
        cleaned_data['promo_type'] = Promo._PROMO_TYPE_CHOICES.discount

        if (cleaned_data.get('promo_rule') != Promo._PROMO_RULE_CHOICES.buy_a_get_b and cleaned_data.get(
            'discount_type') == _DISCOUNT_TYPE.percentage
            and cleaned_data.get('discount', 0) > 100):
            raise forms.ValidationError(
                _("Invalid Discount Percentage Value."),
                code='invalid_discount_percentage')
        if (cleaned_data.get('promo_rule') != Promo._PROMO_RULE_CHOICES.buy_a_get_b and cleaned_data.get(
            'reward_type') == _REWARD_TYPE_CHOICES.buy_product_in_discount_percentage and
            cleaned_data.get('reward', 0) > 100):
            raise forms.ValidationError(
                _("Invalid Reward Percentage Value."),
                code='invalid_reward_percentage')
        if (cleaned_data.get('promo_rule') == Promo._PROMO_RULE_CHOICES.buy_a_get_b):
            if (cleaned_data.get('reward_buy_quantity') <= 0 or cleaned_data.get('reward_get_quantity') <= 0):
                raise forms.ValidationError(
                    _("Invalid Buy or Get Item Quantity."),
                    code='invalid_reward_buy_and_get_quantity')
            if (cleaned_data.get('reward_type') == _REWARD_TYPE_CHOICES.buy_product_in_specified_amount):
                if cleaned_data.get('item_get_type') == ItemPromoReward._ITEM_TYPE_CHOICES.product:
                    product_price = cleaned_data.get('product_get').price
                    quantity = cleaned_data.get('reward_get_quantity')
                    price = product_price.amount * quantity
                    reward_price = cleaned_data.get('reward')
                    if reward_price is None:
                        raise forms.ValidationError(_("You must fill the reward value"))
                    if reward_price > price:
                        raise forms.ValidationError(_("Reward price is over base cost. Your reward price is {} but "
                                                      "your total product cost is {} "
                                                      "").format(cleaned_data.get('reward'), price),
                                                    code='invalid_reward_price')
                if cleaned_data.get('item_get_type') == ItemPromoReward._ITEM_TYPE_CHOICES.category:
                    category_id = cleaned_data.get('category_get').id
                    price = cleaned_data.get('reward')
                    if price is None:
                        raise forms.ValidationError(_("You must fill the reward value"))
                    product_list = Product.objects.filter(category=category_id, archived=False).exclude(
                        classification=Product.PRODUCT_CLASSIFICATION.complementary)
                    for product in product_list:
                        if product.price.amount < price:
                            raise forms.ValidationError(_("Reward price is over base cost. Your reward price should "
                                                          "be under {}").format(product.price.amount),
                                                        code='invalid_reward_price')

        if (cleaned_data.get('promo_rule') == Promo._PROMO_RULE_CHOICES.promo_product or
            Promo._PROMO_RULE_CHOICES.promo_payment_method):
            if (cleaned_data.get('affected_product_type') == Promo._PRODUCT_TYPE_CHOICE.product and cleaned_data.get(
                'discount_type') == _DISCOUNT_TYPE.specified_amount):
                product_list = cleaned_data.get('affected_product')
                discount_price = cleaned_data.get('discount')
                if discount_price is None:
                    raise forms.ValidationError(_("You must fill the discount value"))
                if product_list is None:
                    raise forms.ValidationError(_("You must select at least one product"))
                else:
                    for product in product_list:
                        if product.price.amount < discount_price:
                            raise forms.ValidationError(
                                _("Discount price is over base cost. Your promo price should be "
                                  "under {}").format(product.price.amount),
                                code='invalid_discount_price')
            if (cleaned_data.get('affected_product_type') == Promo._PRODUCT_TYPE_CHOICE.category and cleaned_data.get(
                'discount_type') == _DISCOUNT_TYPE.specified_amount):
                category_list = cleaned_data.get('affected_category')
                discount_price = cleaned_data.get('discount')
                if discount_price is None:
                    raise forms.ValidationError(_("You must fill the discount value"))
                if category_list is None:
                    raise forms.ValidationError(_("You must select at least one category"))
                else:
                    for category in category_list:
                        product_list = Product.objects.filter(category=category.id, archived=False).exclude(
                            classification=Product.PRODUCT_CLASSIFICATION.complementary)
                        for product in product_list:
                            if product.price.amount < discount_price:
                                raise forms.ValidationError(
                                    _("Discount price is over base cost. Your promo price should "
                                      "be under {}").format(product.price.amount),
                                    code='invalid_discount_price')
        if (cleaned_data.get('end_date') is not None):
            if (cleaned_data.get('start_date') > cleaned_data.get('end_date')):
                raise forms.ValidationError(
                    _('Start date is beyond end date'),
                    code='invalid_end_date'
                )

        if (cleaned_data.get('customer_eligibility') == Promo._ELIGIBILITY_CHOICES.selected_customer):
            if (len(cleaned_data.get('affected_customer')) == 0):
                raise forms.ValidationError(_("You must select at least one customer"))
        if (cleaned_data.get('customer_eligibility') == Promo._ELIGIBILITY_CHOICES.selected_group):
            if (len(cleaned_data.get('affected_group')) == 0):
                raise forms.ValidationError(_("You must select at least one group"))
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(EmployeePromoForm, self).__init__(*args, **kwargs)
        self.fields['affected_product'].queryset = Product.objects.exclude(
            classification=Product.PRODUCT_CLASSIFICATION.complementary)
        self.fields['tags'].label = _('Tags')
        if self.data and self.data.get('promo_rule', None) == Promo._PROMO_RULE_CHOICES.buy_a_get_b:
            self.fields['discount'].required = False
            self.fields['discount_type'].required = False
            self.fields['reward_type'].required = True
            if self.data and self.data.get('item_buy_type', None) == ItemPromoReward._ITEM_TYPE_CHOICES.product:
                self.fields['product_buy'].required = True
                self.fields['category_buy'].required = False
            else:
                self.fields['product_buy'].required = False
                self.fields['category_buy'].required = True
            if self.data and self.data.get('item_get_type', None) == ItemPromoReward._ITEM_TYPE_CHOICES.product:
                self.fields['product_get'].required = True
                self.fields['category_get'].required = False
            else:
                self.fields['product_get'].required = False
                self.fields['category_get'].required = True
            if self.data and self.data.get('reward_type', None) == _REWARD_TYPE_CHOICES.free_product:
                self.fields['reward'].required = False
            else:
                self.fields['reward'].required = True
            self.fields['reward_buy_quantity'].required = True
            self.fields['reward_get_quantity'].required = True
        else:
            self.fields['discount'].required = True
            self.fields['discount_type'].required = True
            self.fields['reward_type'].required = False
            self.fields['product_buy'].required = False
            self.fields['product_get'].required = False
            self.fields['category_buy'].required = False
            self.fields['category_get'].required = False
            self.fields['reward_buy_quantity'].required = False
            self.fields['reward_get_quantity'].required = False
            self.fields['reward'].required = False

        if self.data and self.data.get('promo_rule', None) == Promo._PROMO_RULE_CHOICES.promo_product:
            if self.data and self.data.get('affected_product_type', None) == Promo._PRODUCT_TYPE_CHOICE.product:
                self.fields['affected_product'].required = True
                self.fields['affected_category'].required = False
            else:
                self.fields['affected_product'].required = False
                self.fields['affected_category'].required = True

        if self.data and self.data.get('promo_rule', None) == Promo._PROMO_RULE_CHOICES.promo_payment_method:
            self.fields['affected_product'].required = False
            self.fields['affected_category'].required = False
            self.fields['affected_product_type'].required = False
            self.fields['affected_payment_method'].required = True

        self.fields['affected_payment_method'].queryset = self.get_payment_method_queryset()
        self.fields['affected_customer'].queryset = Customer.objects.filter(is_active=True)
        self.fields['affected_category'].queryset = Category.objects.all()
        self.fields['affected_group'].queryset = CustomerGroup.objects.filter(is_active=True)
        self.fields['outlet'].queryset = Outlet.objects.all()
        self.fields['tags'].queryset = PromoTag.objects.all()
        self.fields['product_buy'].queryset = Product.objects.exclude(
            classification=Product.PRODUCT_CLASSIFICATION.complementary)
        self.fields['product_get'].queryset = Product.objects.exclude(
            classification=Product.PRODUCT_CLASSIFICATION.complementary)
        self.fields['category_buy'].queryset = Category.objects.exclude(name="Paketan",
                                                                        classification=Category.CATEGORY_CLASSIFICATION.composite,
                                                                        depth=1)
        self.fields['category_get'].queryset = Category.objects.exclude(name="Paketan",
                                                                        classification=Category.CATEGORY_CLASSIFICATION.composite,
                                                                        depth=1)
        self.fields['allow_days'].label = ''
        self.fields['is_enforced'].label = 'Promo ini berlaku otomatis'
        self.fields['start_hours'].label = 'Jam Mulai'
        self.fields['end_hours'].label = 'Jam Berakhir'
        self.fields['discount'].label = 'Discount Value'
        self.fields['customer_eligibility'].label = _("Apply to")
        self.fields['discount_type'].label = _("Discount Type")
        self.fields['affected_product_type'].label = _("Product Type")
        self.fields['promo_limit'].label = _('Limitation')
        self.fields['start_hours'].required = False
        self.fields['end_hours'].required = False
        self.fields['all_days'].required = False
        self.fields['all_outlet'].required = False
        self.fields['enable_end_date'].required = False
        self.fields['enable_minimum_transaction'].required = False
        self.fields['happy_hours'].required = False
        self.fields['minimum_transaction'].required = False
        self.fields['limit_per_customer'].required = False
        self.fields['amount_limit'].required = False
        self.fields['promo_limit'].required = False
        self.fields['organizational'].required = False
        self.fields['organizational'].initial = True
        self.fields['optional_affected_products'].required = False
        self.fields['optional_affected_products'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak berlaku pada tipe product atau category. )')
        self.fields['is_enforced'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak ditampilkan secara otomatis pada transaksi. )')
        self.fields['all_days'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak berlaku setiap hari. )'
        )
        self.fields['organizational'].help_text = _(
            '( Jika centang ini dihapus, promo tidak akan tersedia di employee benefit. )'
        )
        self.fields['all_outlet'].help_text = _(
            '( Jika centang ini dihapus, promo ini tidak berlaku pada gerai yang baru. )'
        )
        self.fields['amount_limit'].help_text = _('(Tracked by registered customer)')
        self.fields['promo_type'].required = False
        self.helper = FormHelper()
        self.helper.form_class = "col-lg-12 px-0 mb-0"
        self.helper.field_class = "p-0 mb-2"
        self.helper.label_class = "m-0 pt-0"
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.help_text_inline = True

        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                "Promo Information",
                KawnField("name"),
                KawnField("tags"),
                help_text=render_to_string("help_text/promo_information.html"),
            ),
            KawnFieldSetWithHelpText(
                "Tipe Promo",
                KawnField("promo_rule"),
                help_text=render_to_string("help_text/promo_type.html"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('is_enforced'),
                KawnField("organizational", type="hidden"),
                css_id='item_enforced'
            ),
            KawnFieldSetWithHelpText(
                _("Promo Settings"),
                KawnField("discount_type"
                          ),
                KawnAppendedText("discount", "%", css_id='div_id_discount'),
                css_id='promo_settings_1',
            ),
            KawnFieldSetWithHelpText(
                "Promo Settings",
                Div(
                    HTML(
                        '<h5>{}</h5>'.format(_('Customer Buy'))
                    ),
                    css_class="col-lg-12 p-0",
                    css_id='customer_buy_header'
                ),
                KawnField('reward_buy_quantity'),
                KawnField('item_buy_type', css_class="custom-select"),
                KawnField('product_buy', css_class="custom-select"),
                KawnField('category_buy', css_class="custom-select"),
                Div(
                    HTML(
                        '<hr/><h5>{}</h5>'.format(_('Customer Get'))
                    ),
                    css_class="col-lg-12 p-0",
                    css_id='customer_get_header'
                ),
                KawnField('reward_get_quantity'),
                KawnField('item_get_type', css_class="custom-select"),
                KawnField('product_get', css_class="custom-select"),
                KawnField('category_get', css_class="custom-select"),
                KawnField('reward_type'),
                KawnAppendedText('reward', "%", css_class="div_id_reward"),
                css_id='promo_settings_2',
                help_text=render_to_string("help_text/promo_settings.html"),
            ),
            KawnFieldSetWithHelpText(
                _("Choose Product"),
                KawnField("optional_affected_products"),
                KawnField("affected_product_type"),
                KawnField('affected_product'),
                KawnField('affected_category'),
                help_text=render_to_string("help_text/affected_product.html"),
                css_id='affected_product',
            ),
            KawnFieldSetWithHelpText(
                _("Affected Payment Method"),
                KawnField('affected_payment_method'),
                optional_legend=True,
                small_desc=_(
                    "( Jika centang ini dihapus, promo ini tidak berlaku pada tipe pembayaran. )"),
                css_id='affected_payment_method',
                help_text=render_to_string("help_text/affected_payment_method.html"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField("enable_minimum_transaction"),
                KawnAppendedText("minimum_transaction", "IDR", css_id="div_id_minimum_transaction")
            ),
            KawnFieldSetWithHelpText(
                _("Customer Eligibility"),
                KawnField('customer_eligibility'),
                KawnField('affected_customer'),
                KawnField('affected_group'),
                help_text=render_to_string("help_text/customer_eligibility.html"),
            ),
            KawnFieldSetWithHelpText(
                _("Usages Limit"),
                KawnField('usages_limit'),
                KawnField('promo_limit'),
                KawnField('limit_per_customer'),
                KawnAppendedText('amount_limit', "IDR"),
                KawnField('recurrence_limit')
            ),
            KawnFieldSetWithHelpText(
                "Tanggal Berlaku",
                KawnField("start_date"),
                KawnField("enable_end_date"),
                KawnField("end_date")

            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_days'),
                KawnField('allow_days'),
                KawnField('happy_hours'),
                KawnField("start_hours"),
                KawnField("end_hours"),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet'),
                KawnField('outlet'),
            ),
        )

    def clean_promo_limit(self):
        promo_limit = self.cleaned_data['promo_limit']
        usages_limit = self.cleaned_data['usages_limit']
        if usages_limit == Promo._USAGES_LIMIT.use_limit:
            if promo_limit is None:
                raise forms.ValidationError(_("This field is required"))
        return promo_limit

    def clean_amount_limit(self):
        amount_limit = self.cleaned_data['amount_limit']
        usages_limit = self.cleaned_data['usages_limit']
        if usages_limit == Promo._USAGES_LIMIT.amount_limit:
            if amount_limit is None:
                raise forms.ValidationError(_("This field is required"))
        return amount_limit

    def get_payment_method_queryset(self):

        if current_tenant:
            return PaymentMethod.objects.filter(
                account=current_tenant,
                enabledpaymentmethod__isnull=False
            )
        else:
            return PaymentMethod.objects.all()

    def update_m2m(self, instance, object_list, rel_field, intermediate_model, intermediate_model_ref):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.values_list("id", flat=True)
        )
        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "{}".format(intermediate_model_ref.field.name): instance,
                    "account": instance.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids,
            "{}".format(intermediate_model_ref.field.name): instance,
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def update_outlet(self, promo, outlet_list):
        outlet_list = outlet_list.only('pk')
        current_outlet = promo.outlet.filter(
            pk__in=self.fields['outlet'].queryset
        ).only('pk')
        deleted_outlet = current_outlet.difference(outlet_list)

        OutletPromo.objects.filter(
            outlet__pk__in=deleted_outlet.values_list('pk', flat=True)).delete()

        for new_outlet in outlet_list.difference(current_outlet):
            OutletPromo.objects.create(
                outlet=new_outlet, promo=promo)
        promo.outlet.invalidated_update()

    def update_promo_reward(self, promo):
        discount_type = self.cleaned_data['discount_type']
        discount = self.cleaned_data['discount']
        if not hasattr(promo, 'promoreward'):
            promo.promoreward = PromoReward.objects.create(
                discount_type=discount_type,
                promo=promo,
                discount=discount,
            )
        else:
            promo.promoreward.discount_type = discount_type
            promo.promoreward.discount = discount
            promo.promoreward.save()

    def update_item_promo_reward(self, promo):
        reward_type = self.cleaned_data['reward_type']
        reward = self.cleaned_data['reward']
        product_buy = self.cleaned_data['product_buy']
        product_get = self.cleaned_data['product_get']
        category_buy = self.cleaned_data['category_buy']
        category_get = self.cleaned_data['category_get']
        item_buy_type = self.cleaned_data['item_buy_type']
        item_get_type = self.cleaned_data['item_get_type']
        reward_buy_quantity = self.cleaned_data['reward_buy_quantity']
        reward_get_quantity = self.cleaned_data['reward_get_quantity']
        if not hasattr(promo, 'itempromoreward'):
            promo.itempromoreward = ItemPromoReward.objects.create(
                reward_type=reward_type,
                promo=promo,
                reward=reward,
                product_buy=product_buy,
                category_buy=category_buy,
                reward_buy_quantity=reward_buy_quantity,
                product_get=product_get,
                category_get=category_get,
                item_get_type=item_get_type,
                item_buy_type=item_buy_type,
                reward_get_quantity=reward_get_quantity,
            )
        else:
            promo.itempromoreward.reward_type = reward_type
            promo.itempromoreward.reward = reward
            promo.itempromoreward.product_get = product_get
            promo.itempromoreward.category_get = category_get
            promo.itempromoreward.item_get_type = item_get_type
            promo.itempromoreward.reward_get_quantity = reward_get_quantity
            promo.itempromoreward.product_buy = product_buy
            promo.itempromoreward.category_buy = category_buy
            promo.itempromoreward.item_buy_type = item_buy_type
            promo.itempromoreward.reward_buy_quantity = reward_buy_quantity
            promo.itempromoreward.save()

    def save(self, *args, **kwargs):
        outlet_list = self.cleaned_data.pop('outlet')
        product_list = self.cleaned_data.pop('affected_product')
        payment_method_list = self.cleaned_data.pop('affected_payment_method')
        customer_list = self.cleaned_data.pop('affected_customer')
        group_list = self.cleaned_data.pop('affected_group')
        category_list = self.cleaned_data.pop('affected_category')
        tags_list = self.cleaned_data.pop('tags')
        promo = super(EmployeePromoForm, self).save(*args, **kwargs)
        self.update_m2m(
            promo, outlet_list,
            promo.outlet,
            OutletPromo, OutletPromo.promo
        )
        self.update_m2m(
            promo, product_list,
            promo.affected_product,
            ProductPromo, ProductPromo.promo)
        self.update_m2m(
            promo, payment_method_list,
            promo.affected_payment_method,
            PaymentMethodPromo, PaymentMethodPromo.promo)
        self.update_m2m(
            promo, customer_list,
            promo.affected_customer,
            CustomerPromo, CustomerPromo.promo
        )
        self.update_m2m(
            promo, group_list,
            promo.affected_group,
            CustomerGroupPromo, CustomerGroupPromo.promo
        )
        self.update_m2m(
            promo, category_list,
            promo.affected_category,
            CategoryPromo, CategoryPromo.promo
        )
        self.update_m2m(
            promo, tags_list,
            promo.tags,
            TagPromo, TagPromo.promo
        )
        self.update_promo_reward(promo)
        self.update_item_promo_reward(promo)
        return promo
