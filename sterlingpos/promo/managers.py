import types

from datetime import datetime

from django.db import models
from django.db.models import (
    FilteredRelation,
    Subquery,
    Prefetch,
    Sum,
    Count,
    Q,
    F,
    IntegerField,
    DecimalField,
    Case,
    Value,
    When,
    Avg,
    Func,
    CharField,
    BooleanField,
    TimeField,
)
from django.apps import apps

from django.db.models.sql.datastructures import Join
from django.db.models.fields.related import RelatedField
from django.db.models.functions import (
    Coalesce,
    TruncMonth,
    TruncDate,
    TruncDay,
    TruncHour,
    TruncMinute,
    ExtractWeekDay,
)
from django.db.models.sql.where import AND

from sterlingpos.core.models import (
    SterlingTenantQuerySet,
    SterlingSafeDeleteAllManager,
    SterlingTenantManager,
    get_current_tenant,
)
from sterlingpos.stocks.query import (
    names_to_path,
    add_filtered_relation, 
    join_to,
)


class Round(Func):
    function = "ROUND"
    arity = 2


class PromoQuerySet(SterlingTenantQuerySet):

    def active(self):
        qs = self.exclude(
            end_date__lte=datetime.now(),
            start_date__gte=datetime.now(),
            start_hours__lte=datetime.now().time(),
            end_hours__gte=datetime.now().time(),
        ).filter(
            is_active=True,
        )
        return qs

    def get_summary(
        self,
        start_date,
        end_date,
        outlets=None
    ):
        SalesOrderPromoHistory = apps.get_model("sales.SalesOrderPromoHistory")
        qs = self
        filters = Q(
            sales_order__transaction_date__gte=start_date,
            sales_order__transaction_date__lte=end_date,
        )
        if outlets:
            filters &= Q(
                sales_order__outlet__in=outlets,
            )
        promo_usage_ids = SalesOrderPromoHistory.objects.filter(
            filters
        ).values_list(
            "pk", flat=True
        )

        qs = qs.annotate(
            promo_usage_list=FilteredRelation(
                "salesorderpromohistory",
                condition=Q(salesorderpromohistory__in=promo_usage_ids),
            ),
            count=Count("promo_usage_list"),
            total_discount=Coalesce(
                Sum(F('promo_usage_list__total_discount')),
                Value(0),
                output_field=DecimalField()
            ),
            product_affected_value=Coalesce(
                Sum(
                    F('promo_usage_list__affected_value') * F('promo_usage_list__product_quantity'),
                    filter=Q(promo_usage_list__product_quantity__gt=0)
                ),
                Value(0),
                output_field=DecimalField()
            ),
            non_product_affected_value=Coalesce(
                Sum(
                    F('promo_usage_list__affected_value'),
                    filter=Q(promo_usage_list__product_quantity=0)
                ),
                Value(0),
                output_field=DecimalField()
            ),
            affected_value=Round(
                F('product_affected_value') + F('non_product_affected_value'),
                2
            ),
        )
        qs.query.group_by = ["pk"]

        return qs


class PromoManager(
    PromoQuerySet.as_manager().__class__, SterlingTenantManager
):
    pass
