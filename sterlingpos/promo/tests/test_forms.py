from django.test import TestCase
from model_mommy import mommy

from sterlingpos.promo.models import (
    Promo,
    _DISCOUNT_TYPE,
    ItemPromoReward, _REWARD_TYPE_CHOICES, _TIME_LIMIT_CHOICES)
from sterlingpos.promo.forms import PromoForm
from sterlingpos.accounts.models import Account
from sterlingpos.outlet.models import Outlet
from sterlingpos.core.models import set_current_tenant


class TestPromoConfigurationForm(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        self.outlet_1 = mommy.make(
            Outlet, name='branch-1', branch_id='branch-1', account=self.account)
        self.outlet_2 = mommy.make(
            Outlet, name='branch-2', branch_id='branch-2', account=self.account)
        self.product_1 = mommy.make(
            "catalogue.Product", account=self.account)
        self.category_1 = mommy.make("catalogue.Category", account=self.account)
        self.category_2 = mommy.make("catalogue.Category", account=self.account)
        self.product_2 = mommy.make(
            "catalogue.Product", account=self.account, price=10000, category=self.category_1
        )
        self.product_3 = mommy.make(
            "catalogue.Product", account=self.account, price=20000, category=self.category_1
        )
        self.product_4 = mommy.make(
            "catalogue.Product", account=self.account, price=15000, category=self.category_2
        )
        self.product_5 = mommy.make(
            "catalogue.Product", account=self.account, price=25000, category=self.category_2
        )
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_save_promo(self):
        valid_data = {
            'name': 'Test valid promo discount percentage',
            'promo_rule': 'promo_global',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product': [self.product_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertTrue(form.is_valid(), form.errors)
        promo = form.save()
        self.assertTrue(promo.is_active)
        self.assertEqual(promo.promo_type, Promo._PROMO_TYPE_CHOICES.discount)

    def test_clean_promo_configuration_discount_percentage(self):
        valid_data = {
            'name': 'Test valid promo discount percentage',
            'promo_rule': 'promo_global',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertTrue(form.is_valid(), form.errors)
        promo = form.save()
        self.assertEqual(promo.promoreward.discount_type, _DISCOUNT_TYPE.percentage)
        self.assertEqual(promo.promoreward.discount_fixed_price, None)
        self.assertEqual(promo.promoreward.discount_percentage, 15.00)
        self.assertEqual(promo.promoreward.discount, 15.00)

    def test_clean_promo_configuration_discount_fixed_price(self):
        valid_data = {
            'name': 'Test valid promo discount percentage',
            'promo_rule': 'promo_global',
            'discount_type': _DISCOUNT_TYPE.fixed_price,
            'discount': 15000.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertTrue(form.is_valid(), form.errors)
        promo = form.save()
        self.assertEqual(promo.promoreward.discount_type, _DISCOUNT_TYPE.fixed_price)
        self.assertEqual(promo.promoreward.discount_fixed_price, 15000.00)
        self.assertEqual(promo.promoreward.discount_percentage, None)
        self.assertEqual(promo.promoreward.discount, 15000.00)

    def test_clean_promo_name_invalid(self):
        valid_data = {
            'name': '',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('name', form.errors)

    def test_clean_promo_discount_invalid(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Valid',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': '',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('discount', form.errors)

    def test_clean_promo_reward_invalid(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'buy_a_get_b',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': '',
            'reward_type': _REWARD_TYPE_CHOICES.buy_product_in_discount_percentage,
            'product_buy': self.product_1.pk,
            'product_get': self.product_1.pk,
            'reward_get_quantity': 1,
            'reward_buy_quantity': 2,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('reward', form.errors)

    def test_clean_start_date_invalid(self):
        valid_data = {
            'name': 'Test',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'promo_rule': 'global_promo',
            'discount': 15.00,
            'start_date': '01/10/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_clean_promo_discount_invalid_value(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Valid',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': '1000',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_clean_promo_allow_days_invalid(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Valid',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': [],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('allow_days', form.errors)

    def test_clean_promo_outlet_invalid(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Valid',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('outlet', form.errors)

    def test_clean_discount_specified_amount_invalid_in_promo_product(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Invalid',
            'discount_type': _DISCOUNT_TYPE.specified_amount,
            'promo_rule': 'promo_product',
            'discount': 30000,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'affected_product': [self.product_3.pk, self.product_2.pk],
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_clean_discount_specified_amount_invalid_in_promo_product_by_category(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Invalid',
            'discount_type': _DISCOUNT_TYPE.specified_amount,
            'promo_rule': 'promo_product',
            'discount': 30000,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'outlet': [self.outlet_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.category,
            'affected_category': [self.category_1.pk, self.category_2.pk],
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_discount_specified_amount_valid_in_promo_product(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Invalid',
            'discount_type': _DISCOUNT_TYPE.specified_amount,
            'promo_rule': 'promo_product',
            'discount': 9000,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'affected_product': [self.product_3.pk, self.product_2.pk],
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertTrue(form.is_valid(), form.errors)
        promo = form.save()
        self.assertTrue(promo.is_active)
        self.assertEqual(promo.promo_type, Promo._PROMO_TYPE_CHOICES.discount)
    
    def test_clean_promo_reward_specified_price_over_product_price_invalid(self):
        # TODO : Enable after implementation of buy_a_get_b
        '''
        valid_data = {
            'name': 'Test',
            'promo_rule': 'buy_a_get_b',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 25000,
            'reward_type': _REWARD_TYPE_CHOICES.buy_product_in_specified_amount,
            'product_buy': self.product_1.pk,
            'product_get': self.product_2.pk,
            'reward_get_quantity': 1,
            'reward_buy_quantity': 2,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)
        '''
        pass

    def test_clean_promo_reward_specified_price_over_category_price_invalid(self):
        # TODO : Enable after implementation of buy_a_get_b
        '''
        valid_data = {
            'name': 'Test',
            'promo_rule': 'buy_a_get_b',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 20000,
            'reward_type': _REWARD_TYPE_CHOICES.buy_product_in_specified_amount,
            'product_buy': self.product_1.pk,
            'category_get': self.category_2.pk,
            'reward_get_quantity': 1,
            'reward_buy_quantity': 2,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.category,
            'recurrence_limit': 'no_recurrence',
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)
        '''
        pass

    def test_clean_promo_limit_invalid(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'promo_global',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'discount_type': _DISCOUNT_TYPE.fixed_price,
            'discount': 1000,
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'affected_product': [self.product_2.pk],
            'customer_eligibilty': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.use_limit,
            'promo_limit': None,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('promo_limit', form.errors)

    def test_clean_amount_limit(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'promo_global',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'discount_type': _DISCOUNT_TYPE.fixed_price,
            'discount': 1000,
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'affected_product': [self.product_2.pk],
            'customer_eligibilty': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.amount_limit,
            'amount_limit': None,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence'
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('amount_limit', form.errors)

    def test_clean_discount_value_before_select_product(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'promo_product',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'discount_type': _DISCOUNT_TYPE.specified_amount,
            'discount': None,
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'affected_product': [self.product_2.pk, self.product_1.pk],
            'customer_eligibilty': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.amount_limit,
            'amount_limit': 1000,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence'
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_clean_discount_value_before_select_category(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'promo_product',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'discount_type': _DISCOUNT_TYPE.specified_amount,
            'discount': None,
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.category,
            'affected_category': [self.category_1.pk, self.category_2.pk],
            'customer_eligibilty': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.amount_limit,
            'amount_limit': 1000,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence'
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_clean_product_invalid_input_affected_product(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'promo_product',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'discount_type': _DISCOUNT_TYPE.specified_amount,
            'discount': 10000,
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'affected_product': [],
            'customer_eligibilty': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.amount_limit,
            'amount_limit': 1000,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence'
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_clean_category_invalid_input_affected_category(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'promo_product',
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'discount_type': _DISCOUNT_TYPE.specified_amount,
            'discount': 10000,
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.category,
            'affected_category': [],
            'customer_eligibilty': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.amount_limit,
            'amount_limit': 1000,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence'
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_invalid_input_affected_customer(self):
        valid_data = {
            'name': 'Test valid promo discount percentage',
            'promo_rule': 'promo_global',
            'discount_type': _DISCOUNT_TYPE.fixed_price,
            'discount': 15000.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.selected_customer,
            'affected_customer': [],
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_invalid_input_affected_group(self):
        valid_data = {
            'name': 'Test valid promo discount percentage',
            'promo_rule': 'promo_global',
            'discount_type': _DISCOUNT_TYPE.fixed_price,
            'discount': 15000.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.selected_group,
            'affected_group': [],
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('__all__', form.errors)

    def test_update_outlet(self):
        valid_data = {
            'name': 'Test',
            'description': 'Test Valid',
            'promo_rule': 'promo_global',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }

        form = PromoForm(valid_data)
        self.assertTrue(form.is_valid())
        promo = form.save()
        self.assertTrue(promo.outlet.filter(pk=self.outlet_1.pk).exists())
        self.assertFalse(promo.outlet.filter(pk=self.outlet_2.pk).exists())

        valid_data['outlet'] = [self.outlet_2.pk]
        form = PromoForm(valid_data, instance=promo)
        self.assertTrue(form.is_valid())
        form.update_outlet(promo, form.cleaned_data['outlet'])
        self.assertTrue(promo.outlet.filter(pk=self.outlet_2.pk).exists())
        self.assertFalse(promo.outlet.filter(pk=self.outlet_1.pk).exists())

        valid_data['outlet'] = [self.outlet_1.pk]
        form = PromoForm(valid_data, instance=promo)
        self.assertTrue(form.is_valid())
        form.update_outlet(promo, form.cleaned_data['outlet'])
        self.assertTrue(promo.outlet.filter(pk=self.outlet_1.pk).exists())
        self.assertFalse(promo.outlet.filter(pk=self.outlet_2.pk).exists())

    def test_required_fields_in_category_buy(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'buy_a_get_b',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': 5.0,
            'reward_type': _REWARD_TYPE_CHOICES.buy_product_in_discount_percentage,
            'category_buy': '',
            'product_buy': '',
            'product_get': self.product_1.pk,
            'reward_get_quantity': 1,
            'reward_buy_quantity': 2,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.category,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
        }

        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('category_buy', form.errors)

    def test_non_required_reward_in_free_product(self):
        # TODO : Enable after implementation of buy_a_get_b
        '''
        valid_data = {
            'name': 'Test',
            'promo_rule': 'buy_a_get_b',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 15.00,
            'start_date': '01/08/2018',
            'end_date': '31/08/2018',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'reward': None,
            'reward_type': _REWARD_TYPE_CHOICES.free_product,
            'product_buy': self.product_2.pk,
            'product_get': self.product_1.pk,
            'reward_get_quantity': 1,
            'reward_buy_quantity': 2,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': _TIME_LIMIT_CHOICES.no_recurrence,
        }

        form = PromoForm(valid_data)
        self.assertTrue(form.is_valid(), form.errors)
        promo = form.save()
        self.assertEqual(promo.itempromoreward.reward_type, _REWARD_TYPE_CHOICES.free_product)
        self.assertEqual(promo.itempromoreward.reward, None)
        '''
        pass

    def test_required_fields_in_promo_payment_method(self):
        valid_data = {
            'name': 'Test',
            'promo_rule': 'promo_payment_method',
            'discount_type': _DISCOUNT_TYPE.percentage,
            'discount': 10.00,
            'start_date': '01/08/2019',
            'end_date': '20/08/2019',
            'start_hours': '',
            'end_hours': '',
            'all_days': False,
            'allow_days': ['1', '2', '3'],
            'all_outlet': False,
            'outlet': [self.outlet_1.pk, self.outlet_2.pk],
            'affected_product_type': Promo._PRODUCT_TYPE_CHOICE.product,
            'customer_eligibility': Promo._ELIGIBILITY_CHOICES.everyone,
            'usages_limit': Promo._USAGES_LIMIT.unlimited,
            'affected_payment_method': [],
            'reward': 0,
            'item_buy_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'item_get_type': ItemPromoReward._ITEM_TYPE_CHOICES.product,
            'recurrence_limit': 'no_recurrence',
        }
        form = PromoForm(valid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('affected_payment_method', form.errors)
