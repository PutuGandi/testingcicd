from django.test import TestCase
from model_mommy import mommy

from sterlingpos.promo.models import (
    Promo,
    PromoReward,
    OutletPromo,
    _DISCOUNT_TYPE,
    ItemPromoReward,
    _REWARD_TYPE_CHOICES,
    PromoTag,
    ProductPromo,
    CustomerPromo,
)
from sterlingpos.core.models import set_current_tenant
from sterlingpos.accounts.models import Account
from sterlingpos.outlet.models import Outlet


class TestPromoDiscount(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.outlet_1 = mommy.make(Outlet, branch_id='O1', account=self.account)
        self.outlet_2 = mommy.make(Outlet, branch_id='O2', account=self.account)
        self.tags = mommy.make(PromoTag, account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_promo_discount_percentage(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount',
            is_active=True,
            start_date='2018-08-01',
            end_date='2018-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            account=self.account
        )

        self.promo_reward = PromoReward.objects.create(
            promo=self.promo,
            discount_type=_DISCOUNT_TYPE.percentage,
            discount_percentage=15.00,
        )

        self.item_promo_reward = ItemPromoReward.objects.create(
            promo=self.promo,
            reward_type=_REWARD_TYPE_CHOICES.buy_product_in_discount_percentage,
            reward_percentage=10.00,
        )

        self.outlet_promo_1 = OutletPromo.objects.create(
            promo=self.promo,
            outlet=self.outlet_1
        )

        self.assertEqual(
            self.promo.__str__(),
            "{} - {}".format(self.promo.promo_type, self.promo.name)
        )

        self.assertEqual(
            self.promo_reward.__str__(),
            "{} - {}".format(self.promo_reward.promo, self.promo_reward.discount_type)
        )

        self.assertEqual(
            self.item_promo_reward.__str__(),
            "{} - {}".format(self.item_promo_reward.promo, self.item_promo_reward.reward_type)
        )

        self.assertEqual(
            self.tags.__str__(),
            "{}".format(self.tags.value)
        )

    def test_create_promo_discount_fixed_price(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount',
            is_active=True,
            start_date='2018-08-01',
            end_date='2018-08-31',
            allow_days=['1', '2', '3'],
            account=self.account
        )

        self.promo_reward = PromoReward.objects.create(
            promo=self.promo,
            discount_type=_DISCOUNT_TYPE.fixed_price,
            discount_fixed_price=15000.00,
        )

        self.item_promo_reward = ItemPromoReward.objects.create(
            promo=self.promo,
            reward_type=_REWARD_TYPE_CHOICES.buy_product_in_discount_fixed_price,
            reward_fixed_price=10000.00,
        )

        self.outlet_promo_1 = OutletPromo.objects.create(
            promo=self.promo,
            outlet=self.outlet_1
        )

        self.assertEqual(
            self.promo.__str__(),
            "{} - {}".format(self.promo.promo_type, self.promo.name)
        )

        self.assertEqual(
            self.promo_reward.__str__(),
            "{} - {}".format(self.promo_reward.promo, self.promo_reward.discount_type)
        )

        self.assertEqual(
            self.item_promo_reward.__str__(),
            "{} - {}".format(self.item_promo_reward.promo, self.item_promo_reward.reward_type)
        )

    def test_create_promo_discount_specified_amount(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount',
            is_active=True,
            start_date='2018-08-01',
            end_date='2018-08-31',
            allow_days=['1', '2', '3'],
            account=self.account
        )

        self.promo_reward = PromoReward.objects.create(
            promo=self.promo,
            discount_type=_DISCOUNT_TYPE.specified_amount,
            discount_fixed_price=15000.00,
        )

        self.item_promo_reward = ItemPromoReward.objects.create(
            promo=self.promo,
            reward_type=_REWARD_TYPE_CHOICES.buy_product_in_specified_amount,
            reward_fixed_price=10000.00,
        )

        self.outlet_promo_1 = OutletPromo.objects.create(
            promo=self.promo,
            outlet=self.outlet_1
        )

        self.assertEqual(
            self.promo.__str__(),
            "{} - {}".format(self.promo.promo_type, self.promo.name)
        )

        self.assertEqual(
            self.promo_reward.__str__(),
            "{} - {}".format(self.promo_reward.promo, self.promo_reward.discount_type)
        )

        self.assertEqual(
            self.item_promo_reward.__str__(),
            "{} - {}".format(self.item_promo_reward.promo, self.item_promo_reward.reward_type)
        )


    def test_promo_discount_multiple_outlet(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount',
            is_active=True,
            start_date='2018-08-01',
            end_date='2018-08-31',
            allow_days=['1', '2', '5'],
            account=self.account
        )

        self.promo_reward = PromoReward.objects.create(
            promo=self.promo,
            discount_type=_DISCOUNT_TYPE.percentage,
            discount=15.00,
        )

        self.outlet_promo_1 = OutletPromo.objects.create(
            promo=self.promo,
            outlet=self.outlet_1
        )

        self.outlet_promo_2 = OutletPromo.objects.create(
            promo=self.promo,
            outlet=self.outlet_2
        )

        self.assertEqual(
            self.promo.__str__(),
            "{} - {}".format(self.promo.promo_type, self.promo.name)
        )

        self.assertEqual(
            self.promo_reward.__str__(),
            "{} - {}".format(self.promo_reward.promo, self.promo_reward.discount_type)
        )


class TestPromo(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)
        self.product = mommy.make(
            "catalogue.Product", sku='promo-product', account=self.account)
        self.outlet_1 = mommy.make(
            Outlet, branch_id='O1', account=self.account)
        self.outlet_2 = mommy.make(
            Outlet, branch_id='O2', account=self.account)
        self.tags = mommy.make(
            PromoTag, account=self.account)
        self.customer = mommy.make(
            "customer.Customer", name="customer", account=self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_promo_get_limitation_for_usages_limit(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="use_limit",
            promo_limit=10,
            account=self.account
        )
        self.assertEqual(
            self.promo.get_limitation_for(
                total_cost=5000,
                outlet=self.outlet_1,
            ),
            {
                "count_usages": 0,
                "count_left": 10,
                "count_limit": 10,
                "is_usable": True,
            }
        )

    def test_promo_get_limitation_for_amount_limit(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="amount_limit",
            amount_limit=5000,
            account=self.account
        )
        self.assertEqual(
            self.promo.get_limitation_for(
                total_cost=5000,
                outlet=self.outlet_1,
            ),
            {
                "amount_usages": 0,
                "amount_left": 5000,
                "amount_limit": 5000,
                "is_usable": True,
            }
        )

    def test_promo_get_limitation_for_product_amount_limit(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="amount_limit",
            amount_limit=5000,
            affected_product_type='product',
            account=self.account
        )
        ProductPromo.objects.create(
            promo=self.promo,
            product=self.product,
            account=self.account
        )

        self.assertEqual(
            self.promo.get_limitation_for(
                total_cost=5000,
                outlet=self.outlet_1,
            ),
            {
                "amount_usages": 0,
                "amount_left": 5000,
                "amount_limit": 5000,
                "is_usable": True,
            }
        )

    def test_promo_get_limitation_for_amount_limit_with_limit_per_customer(self):
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="amount_limit",
            amount_limit=5000,
            limit_per_customer=True,
            account=self.account
        )
        CustomerPromo.objects.create(
            promo=self.promo,
            customer=self.customer,
            account=self.account
        )
        sales_order = mommy.make(
            'sales.SalesOrder',
            tax=10,
            outlet=self.outlet_1,
            status="settled",
            account=self.account,
        )
        mommy.make(
            "sales.SalesOrderPromoHistory",
            sales_order=sales_order,
            promo=self.promo,
            name=self.promo.name,
            promo_type=self.promo.promo_type,
            discount_type="fixed_price",
            discount_fixed_price=2000,
            affected_value=2000,
            affected_customer=self.customer,
            account=self.account
        )

        self.assertEqual(
            self.promo.get_limitation_for(
                total_cost=5000,
                outlet=self.outlet_1,
                affected_customer=self.customer,
            ),
            {
                "amount_usages": 2000,
                "amount_left": 3000,
                "amount_limit": 5000,
                "is_usable": True,
            }
        )

    def test_promo_active_queryset(self):
        promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="use_limit",
            promo_limit=10,
            account=self.account
        )
        promo_2 = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=False,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="use_limit",
            promo_limit=10,
            account=self.account
        )
        self.assertEqual(
            Promo.objects.active().count(), 1,
            Promo.objects.active()
        )

    def test_promo_get_summary_of_active_promo(self):
        promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=True,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="use_limit",
            promo_limit=10,
            account=self.account
        )
        promo_2 = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount w/ Usages Limit',
            is_active=False,
            start_date='2018-08-01',
            end_date='2099-08-31',
            allow_days=['1', '2', '3', '4', '5'],
            usages_limit="use_limit",
            promo_limit=10,
            account=self.account
        )
        CustomerPromo.objects.create(
            promo=promo,
            customer=self.customer,
            account=self.account
        )
        sales_order = mommy.make(
            'sales.SalesOrder',
            tax=10,
            outlet=self.outlet_1,
            status="settled",
            account=self.account,
            transaction_date='2018-08-02',
        )
        mommy.make(
            "sales.SalesOrderPromoHistory",
            sales_order=sales_order,
            promo=promo,
            name=promo.name,
            promo_type=promo.promo_type,
            discount_type="fixed_price",
            discount_fixed_price=2000,
            affected_value=2000,
            affected_customer=self.customer,
            account=self.account
        )

        self.assertEqual(
            Promo.objects.active().count(), 1,
            Promo.objects.active()
        )
        qs = Promo.objects.active().get_summary(
                start_date='2018-08-01',
                end_date='2018-08-05'
            )
        self.assertEqual(qs.count(), 1, qs)
        self.assertEqual(qs.first().count, 1, qs.first())
        self.assertEqual(qs.first().affected_value, 2000, qs.first())
