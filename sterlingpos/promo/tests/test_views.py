import json

from test_plus.test import TestCase
from django.test.client import Client, RequestFactory
from django.urls import reverse_lazy, reverse
from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant

from sterlingpos.promo.models import (
    Promo, PromoReward, OutletPromo,
    _DISCOUNT_TYPE
)

from sterlingpos.promo.views import PromoListJson
from sterlingpos.accounts.models import Account
from sterlingpos.outlet.models import Outlet
from sterlingpos.users.models import User
from sterlingpos.device.models import  DeviceUser


class TestPromoListJson(TestCase):

    def setUp(self):
        self.client = Client()
        self.account = mommy.make(Account)
        set_current_tenant(self.account)

        self.user = User.objects.create_superuser("testuser@example.com", password="example")
        device = DeviceUser.objects.first()
        device.user = self.user
        device.save()
        self.outlet = mommy.make(Outlet, account=self.account)
        self.promo = mommy.make(Promo, name="Promo Test 1", account=self.account)
        self.promo_2 = mommy.make(Promo, name="Promo Test 2", account=self.account)

        self.promo_reward = PromoReward.objects.create(
            promo=self.promo,
            discount_type=_DISCOUNT_TYPE.percentage,
            discount_percentage=15.00,
            account=self.account
        )
        self.outlet_promo = OutletPromo.objects.create(
            promo=self.promo,
            outlet=self.outlet,
            account=self.account
        )

        self.client.login(email="testuser@example.com", password="example")

        self.url = reverse_lazy('promo:data_promo')

    def tearDown(self):
        set_current_tenant(None)

    def test_promo_list_json(self):
        response = self.client.get(self.url, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)

    def test_promo_list_json_filter(self):
        self.login(email=self.user.email, password="example")
        total_column = 4
        data = {
            'draw': 3
        }
        for i in range(0, total_column):
            data.update({'columns[{}][data]'.format(i): i})
            data.update({'columns[{}][name]'.format(i): ''})
            data.update({'columns[{}][searchable]'.format(i): True})
            data.update({'columns[{}][orderable]'.format(i): True})
            data.update({'columns[{}][value]'.format(i): ''})
            data.update({'columns[{}][regex]'.format(i): False})

        data.update({
            'columns[0][data]': 0,
            'columns[0][name]': '',
            'columns[0][searhable]': True,
            'columns[0][orderable]': True,
            'columns[0][search][value]': 'Promo Test 1',
            'columns[0][search][regex]': False,
        })
        response = self.get(self.url, data=data, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)
        expected_result_valid = {
            'recordsTotal': 1,
            'recordsFiltered': 1,
        }
        self.assertEqual(content['recordsTotal'], expected_result_valid['recordsTotal'])
        self.assertEqual(content['recordsFiltered'], expected_result_valid['recordsFiltered'])


class BasePromoTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.account = mommy.make(Account)
        set_current_tenant(self.account)

        self.user = User.objects.create_user("testuser@example.com", password="example")
        device = DeviceUser.objects.first()
        device.user = self.user
        device.save()
        self.outlet = mommy.make(Outlet, account=self.account)
        self.promo = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Promo Discount',
            is_active=True,
            start_date='2018-08-01',
            end_date='2018-08-31',
            allow_days=['1', '2', '5'],
            account=self.account
        )

        self.promo_2 = Promo.objects.create(
            promo_type=Promo._PROMO_TYPE_CHOICES.discount,
            name='Second Promo',
            is_active=True,
            start_date='2018-08-01',
            end_date='2018-08-31',
            allow_days=['1', '2', '5'],
            account=self.account
        )

        self.promo_reward_2 = PromoReward.objects.create(
            promo=self.promo_2,
            discount_type=_DISCOUNT_TYPE.fixed_price,
            discount_fixed_price=5000,
        )

        self.outlet_promo_2 = OutletPromo.objects.create(
            promo=self.promo_2,
            outlet=self.outlet
        )

        self.promo_reward = PromoReward.objects.create(
            promo=self.promo,
            discount_type=_DISCOUNT_TYPE.percentage,
            discount_percentage=15.00,
        )
        self.outlet_promo = OutletPromo.objects.create(
            promo=self.promo,
            outlet=self.outlet
        )

        self.promo_tag = mommy.make('promo.PromoTag', value='Bakmitopia', account=self.account)

        self.client.login(email="testuser@example.com", password="example")

        self.url = reverse_lazy('promo:data_promo')

    def tearDown(self):
        set_current_tenant(None)


class TestPromoDeactivateView(BasePromoTest):

    def test_deactivate_promo_view(self):
        data = {
            'id': self.promo.pk,
        }
        response = self.client.post(reverse('promo:promo_deactive'), data=data, **{'HTTP_X_REQUESTED_WITH':
                                                                                       'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class PromoTagAutoComplete(BasePromoTest):

    def test_promo_autocomplete_success_request(self):
        expected_results = {
            'autocomplete': '{}'.format(self.promo_tag.value)
        }
        data = {
            'q': 'Bakmitopia'
        }
        request = self.client.get(reverse('promo:promo_tag_autocomplete'), data=data, **{'HTTP_X_REQUESTED_WITH':
                                                                                             'XMLHttp'})
        self.assertEqual(request.status_code, 200)
        self.assertTrue(self.user.is_authenticated)
        response_data = json.loads(request.content)
        promo_tag_list = response_data['results']
        self.assertEqual(promo_tag_list[0]['text'], expected_results['autocomplete'])


class TestPromoActivateView(BasePromoTest):

    def test_activate_promo_view(self):
        data = {
            'id': self.promo_2.pk
        }
        response = self.client.post(reverse('promo:promo_activate'), data=data,
                                    **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], "success")


class TestPromoDeleteView(BasePromoTest):

    def test_delete_promo_view(self):
        data = {
            'id': self.promo_2.pk
        }
        response = self.client.post(reverse('promo:promo_delete'), data=data, **{"HTTP_X_REQUESTED_WITH":
                                                                                     "XMLHttpRequest"})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertEqual(response_data['status'], 'success')
