from django.contrib import admin
from treebeard.admin import TreeAdmin

from treebeard.forms import movenodeform_factory

from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportMixin
from import_export.fields import Field
from import_export.widgets import ForeignKeyWidget

from .models import (
    Promo,
    PromoReward,
    OutletPromo,
)


class PromoRewardInline(admin.TabularInline):
    model = PromoReward


class OutletPromoInline(admin.TabularInline):
    model = OutletPromo


class PromoAdmin(admin.ModelAdmin):
    '''
        Admin View for ProductModifier
    '''
    list_display = ('name', 'is_active', 'start_date', 'end_date')

    list_filter = ('is_active', 'start_date')
    inlines = (
        PromoRewardInline,
        OutletPromoInline,
    )

admin.site.register(Promo, PromoAdmin)
