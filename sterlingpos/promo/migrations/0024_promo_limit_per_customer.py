# Generated by Django 2.0.4 on 2019-10-04 08:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promo', '0023_auto_20191003_0709'),
    ]

    operations = [
        migrations.AddField(
            model_name='promo',
            name='limit_per_customer',
            field=models.BooleanField(default=False),
        ),
    ]
