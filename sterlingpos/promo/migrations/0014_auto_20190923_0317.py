# Generated by Django 2.0.4 on 2019-09-23 03:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promo', '0013_auto_20190920_0244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='promo',
            name='promo_rule',
            field=models.CharField(choices=[('promo_global', 'Global Promo'), ('promo_product', 'Promo By Product'), ('promo_payment_method', 'Promo By Payment Method'), ('buy_a_get_b', 'Buy A get B')], default='promo_global', max_length=120),
        ),
    ]
