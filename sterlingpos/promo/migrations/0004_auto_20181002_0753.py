# Generated by Django 2.0.4 on 2018-10-02 07:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promo', '0003_auto_20180913_1300'),
    ]

    operations = [
        migrations.RenameField(
            model_name='promoreward',
            old_name='discount_persentage',
            new_name='discount_percentage',
        ),
        migrations.AlterField(
            model_name='promo',
            name='promo_type',
            field=models.CharField(choices=[('discount', 'Diskon')], max_length=120),
        ),
        migrations.AlterField(
            model_name='promoreward',
            name='discount_type',
            field=models.CharField(choices=[('percentage', '%'), ('fixed_price', 'IDR')], max_length=120),
        ),
    ]
