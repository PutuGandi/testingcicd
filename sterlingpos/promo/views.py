from datetime import datetime, date

from django.views.generic import TemplateView, View, CreateView, UpdateView
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils.html import escape
from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin
)
from dal import autocomplete

from .models import (
    Promo,
    PromoReward,
    OutletPromo,
    PromoTag,
    ItemPromoReward)
from .forms import (
    _DISCOUNT_TYPE,
    PromoForm,
    EmployeePromoForm)
from sterlingpos.outlet.models import Outlet
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.core.mixins import (
    CSVExportMixin, DatatableMixins, ReportFilterMixins
)
from sterlingpos.sales.models import SalesOrderPromoHistory


class PromoListJson(SterlingRoleMixin, BaseDatatableView):
    columns = ['name', 'promo_rule', 'is_active', 'action']
    order_columns = ['name', 'promo_rule', 'is_active', 'action']
    model = Promo

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().filter(organizational=False)
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(
                self.request.user)
            qs = qs.filter(
                outlet__in=outlet_qs
            ).distinct()
        return qs

    def filter_queryset(self, qs):
        if not self.pre_camel_case_notation:
            search = self._querydict.get('search[value]', None)
            col_data = self.extract_datatables_column_data()
            q = Q()
            for col_no, col in enumerate(col_data):
                column_name = self.order_columns[col_no].replace('.', '__')
                if column_name in ['is_active'] and col['search.value']:
                    if col['search.value'] == 'show':
                        qs = Promo.objects.all()
                    else:
                        qs = qs.filter(**{'{0}__istartswith'.format(column_name): col['search.value']})
                elif column_name in ['promo_type', 'promo_rule'] and col['search.value']:
                    if col['search.value'] == 'show':
                        qs = Promo.objects.all()
                    else:
                        qs = qs.filter(**{'{0}__istartswith'.format(column_name): col['search.value']})
                elif col['search.value']:
                    qs = qs.filter(**{'{0}__istartswith'.format(column_name): col['search.value']})
            qs = qs.filter(q)
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy("promo:promo_update", kwargs={'pk': row.pk}),
                'archived': row.is_active
            }
            return action_data
        elif column == 'is_active':
            action_data = {
                'is_active': row.is_active
            }
            return action_data
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy("promo:promo_update", kwargs={'pk': row.pk}),
                escape(row.name))
        else:
            return super(PromoListJson, self).render_column(row, column)


class PromoListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/promo/promo_list.html'


class PromoCreateView(SterlingRoleMixin, CreateView):
    template_name = 'dashboard/promo/promo_create.html'
    success_url = reverse_lazy('promo:promo_list')
    form_class = PromoForm
    model = Promo

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def form_valid(self, form):
        response = super(PromoCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Promo has been created.')))
        return response


class PromoUpdateView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/promo/promo_update.html'
    success_url = reverse_lazy('promo:promo_list')
    form_class = PromoForm
    model = Promo

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def get_queryset(self):
        qs = super().get_queryset()

        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(
                self.request.user)
            qs = qs.filter(
                outlet__in=outlet_qs
            ).distinct()

        qs = qs.select_related(
            "account",
        ).prefetch_related(
            "affected_product",
            "affected_product__catalogue",
            "outlet",
            "outlet__account",
        )

        return qs

    def get_initial(self):
        start_date = date.strftime(self.object.start_date, "%d/%m/%Y")
        if self.object.end_date:
            end_date = date.strftime(self.object.end_date, "%d/%m/%Y")
            enable_end_date = True
        else:
            enable_end_date = False
            end_date = None

        if self.object.start_hours or self.object.end_hours:
            enable_happy_hours = True
        else:
            enable_happy_hours = False

        if self.object.minimum_transaction:
            enable_minimum_transaction = True
        else:
            enable_minimum_transaction = False

        if self.object.promoreward.discount_amount_limit:
            enable_discount_amount_limit = True
        else:
            enable_discount_amount_limit = False


        try:
            reward = self.object.itempromoreward.reward
            reward_type = self.object.itempromoreward.reward_type
            product_buy = self.object.itempromoreward.product_buy
            category_buy = self.object.itempromoreward.category_buy
            reward_buy_quantity = self.object.itempromoreward.reward_buy_quantity
            reward_get_quantity = self.object.itempromoreward.reward_get_quantity
            product_get = self.object.itempromoreward.product_get
            category_get = self.object.itempromoreward.category_get
            item_buy_type = self.object.itempromoreward.item_buy_type
            item_get_type = self.object.itempromoreward.item_get_type
        except ItemPromoReward.DoesNotExist:
            reward = None
            reward_type = None
            product_buy = None
            category_buy = None
            reward_buy_quantity = None
            reward_get_quantity = None
            product_get = None
            category_get = None
            item_buy_type = None
            item_get_type = None

        outlet = self.object.outlet.all()
        outlet_account = Outlet.objects.all()
        all_outlet = outlet.count() == outlet_account.count()

        all_days = len(self.object.allow_days) == 7

        initial = {
            'discount_type': self.object.promoreward.discount_type,
            'discount': self.object.promoreward.discount,
            'start_date': start_date,
            'end_date': end_date,
            'enable_end_date': enable_end_date,
            'happy_hours': enable_happy_hours,
            'enable_minimum_transaction': enable_minimum_transaction,
            'enable_discount_amount_limit': enable_discount_amount_limit,
            'discount_amount_limit': self.object.promoreward.discount_amount_limit,
            'all_days': all_days,
            'all_outlet': all_outlet,
            'reward': reward,
            'reward_type': reward_type,
            'product_buy': product_buy,
            'category_buy': category_buy,
            'reward_buy_quantity': reward_buy_quantity,
            'reward_get_quantity': reward_get_quantity,
            'product_get': product_get,
            'category_get': category_get,
            'item_get_type': item_get_type,
            'item_buy_type': item_buy_type,
        }
        return initial

    def form_valid(self, form):
        response = super(PromoUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Promo has been updated.')))
        return response


class PromoDeactiveJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            promo_id = request.POST.get('id', '')
            qs = Promo.objects.all()

            if not request.user.is_owner:
                outlet_qs = get_outlet_of_user(
                    request.user)
                qs = qs.filter(
                    outlet__in=outlet_qs
                ).distinct()

            promo = qs.get(pk=promo_id)
            promo.is_active = False
            promo.save()
            status_code = 200
            response['status'] = 'success'
            messages.success(request, '{}'.format(
                _('Promo has been archived.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class PromoActivateJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            promo_id = request.POST.get('id', '')
            qs = Promo.objects.all()

            if not request.user.is_owner:
                outlet_qs = get_outlet_of_user(
                    request.user)
                qs = qs.filter(
                    outlet__in=outlet_qs
                ).distinct()

            promo = qs.get(pk=promo_id)
            promo.is_active = True
            promo.save()
            status_code = 200
            response['status'] = 'success'
            messages.success(self.request, '{}'.format(
                _('Promo has been re-activated.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class PromoDeleteJson(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            promo_id = request.POST.get('id', '')
            qs = Promo.objects.all()

            if not request.user.is_owner:
                outlet_qs = get_outlet_of_user(
                    request.user)
                qs = qs.filter(
                    outlet__in=outlet_qs
                ).distinct()

            promo = qs.get(pk=promo_id)
            promo.delete()
            status_code = 200
            response['status'] = 'success'
            messages.error(self.request, '{}'.format(
                _('Promo has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class PromoTagAutoCompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = PromoTag.objects.all()
        if self.q:
            qs = qs.filter(value__istartswith=self.q)
        return qs

    def create_object(self, text):
        try:
            obj = PromoTag.objects.get(value=text)
        except PromoTag.DoesNotExist:
            obj = PromoTag.objects.create(value=text)
            obj.save()

        return obj

    def has_add_permission(self, request):
        if request.user.is_authenticated:
            return True
        return False


class EmployeePromoListJson(SterlingRoleMixin, BaseDatatableView):
    columns = ['name', 'promo_rule', 'is_active', 'action']
    order_columns = ['name', 'promo_rule', 'is_active', 'action']
    model = Promo

    def get_initial_queryset(self):
        qs = super().get_initial_queryset().filter(organizational=True)
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(
                self.request.user)
            qs = qs.filter(
                outlet__in=outlet_qs
            ).distinct()
        return qs

    def filter_queryset(self, qs):
        if not self.pre_camel_case_notation:
            search = self._querydict.get('search[value]', None)
            col_data = self.extract_datatables_column_data()
            q = Q()
            for col_no, col in enumerate(col_data):
                column_name = self.order_columns[col_no].replace('.', '__')
                if column_name in ['is_active'] and col['search.value']:
                    if col['search.value'] == 'show':
                        qs = Promo.objects.all()
                    else:
                        qs = qs.filter(**{'{0}__istartswith'.format(column_name): col['search.value']})
                elif column_name in ['promo_type', 'promo_rule'] and col['search.value']:
                    if col['search.value'] == 'show':
                        qs = Promo.objects.all()
                    else:
                        qs = qs.filter(**{'{0}__istartswith'.format(column_name): col['search.value']})
                elif col['search.value']:
                    qs = qs.filter(**{'{0}__istartswith'.format(column_name): col['search.value']})
            qs = qs.filter(q)
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'name': escape(row.name),
                'edit': reverse_lazy("promo:employee_promo_update", kwargs={'pk': row.pk}),
                'archived': row.is_active
            }
            return action_data
        elif column == 'is_active':
            action_data = {
                'is_active': row.is_active
            }
            return action_data
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy("promo:employee_promo_update", kwargs={'pk': row.pk}),
                escape(row.name))
        else:
            return super(EmployeePromoListJson, self).render_column(row, column)


class EmployeePromoListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/employee/employee_promo_list.html'


class EmployeePromoCreateView(SterlingRoleMixin, CreateView):
    template_name = 'dashboard/promo/promo_create.html'
    success_url = reverse_lazy('promo:employee_promo_list')
    form_class = EmployeePromoForm
    model = Promo

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def form_valid(self, form):
        response = super(EmployeePromoCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Promo has been created.')))
        return response


class EmployeePromoUpdateView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/promo/promo_update.html'
    success_url = reverse_lazy('promo:employee_promo_list')
    form_class = EmployeePromoForm
    model = Promo

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(
                self.request.user)
            qs = qs.filter(
                outlet__in=outlet_qs
            ).distinct()
        return qs

    def get_initial(self):
        start_date = date.strftime(self.object.start_date, "%d/%m/%Y")
        if self.object.end_date:
            end_date = date.strftime(self.object.end_date, "%d/%m/%Y")
            enable_end_date = True
        else:
            enable_end_date = False
            end_date = None

        if self.object.start_hours or self.object.end_hours:
            enable_happy_hours = True
        else:
            enable_happy_hours = False

        if self.object.minimum_transaction:
            enable_minimum_transaction = True
        else:
            enable_minimum_transaction = False

        try:
            reward = self.object.itempromoreward.reward
            reward_type = self.object.itempromoreward.reward_type
            product_buy = self.object.itempromoreward.product_buy
            category_buy = self.object.itempromoreward.category_buy
            reward_buy_quantity = self.object.itempromoreward.reward_buy_quantity
            reward_get_quantity = self.object.itempromoreward.reward_get_quantity
            product_get = self.object.itempromoreward.product_get
            category_get = self.object.itempromoreward.category_get
            item_buy_type = self.object.itempromoreward.item_buy_type
            item_get_type = self.object.itempromoreward.item_get_type
        except ItemPromoReward.DoesNotExist:
            reward = None
            reward_type = None
            product_buy = None
            category_buy = None
            reward_buy_quantity = None
            reward_get_quantity = None
            product_get = None
            category_get = None
            item_buy_type = None
            item_get_type = None


        outlet = self.object.outlet.all()
        outlet_account = Outlet.objects.all()
        all_outlet = outlet.count() == outlet_account.count()

        all_days = len(self.object.allow_days) == 7

        initial = {
            'discount_type': self.object.promoreward.discount_type,
            'discount': self.object.promoreward.discount,
            'start_date': start_date,
            'end_date': end_date,
            'enable_end_date': enable_end_date,
            'happy_hours': enable_happy_hours,
            'enable_minimum_transaction': enable_minimum_transaction,
            'all_days': all_days,
            'all_outlet': all_outlet,
            'reward': reward,
            'reward_type': reward_type,
            'product_buy': product_buy,
            'category_buy': category_buy,
            'reward_buy_quantity': reward_buy_quantity,
            'reward_get_quantity': reward_get_quantity,
            'product_get': product_get,
            'category_get': category_get,
            'item_get_type': item_get_type,
            'item_buy_type': item_buy_type,
        }
        return initial

    def form_valid(self, form):
        response = super(EmployeePromoUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Promo has been updated.')))
        return response


class PromoUsageReportListJson(
        SterlingRoleMixin, CSVExportMixin,
        ReportFilterMixins, DatatableMixins, BaseDatatableView):
    columns = ['name', 'count', 'total_discount', 'detail_url']
    order_columns = ['name', 'count', 'total_discount', 'detail_url']

    csv_header = ["Nama", "Jumlah Penggunaan", "Total Diskon"]
    csv_columns = ['name', 'count', 'total_discount']

    model = Promo

    def get_csv_filename(self):
        csv_filename = "Promo_Usage"

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        outlet_id = self.kwargs.get('outlet_id', None)

        qs = super().get_initial_queryset().filter(organizational=False)
        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(
                self.request.user)
            qs = qs.filter(
                outlet__in=outlet_qs
            ).distinct()

        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )
        qs = qs.get_summary(
            start_date=start_date,
            end_date=end_date,
            outlets=[outlet_id] if outlet_id else None,
        )
        return qs

    def render_detail_url(self, row):
        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )
        query_string = ""

        if start_date and end_date:
            query_string = "?start_date={}&end_date={}".format(start_date, end_date)

        outlet_pk = self.kwargs.get("outlet_id")
        kwargs_dict = {'promo_id': row.pk}

        if outlet_pk:
            kwargs_dict.update({
                "outlet_id": outlet_pk,
            })

        base_url = reverse("promo:promo_usages_list", kwargs=kwargs_dict)
        url = f"{base_url}{query_string}"
        return url


class PromoUsageReportView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/promo/promo_usage_report.html'


class PromoUsageListJson(
        SterlingRoleMixin, CSVExportMixin,
        ReportFilterMixins, DatatableMixins, BaseDatatableView):
    columns = [
        'sales_order.code', 'sales_order.transaction_date',
        'affected_product.name', 'product_quantity',
        'total_discount.amount', 'detail_url']
    order_columns = [
        'sales_order__code', 'sales_order__transaction_date',
        'affected_product__name', 'product_quantity',
        'total_discount', 'detail_url']

    csv_header = [
        "Kode", "Tanggal Transaksi",
        "Product", "Quantity", "Total Diskon"]
    csv_columns = [
        'sales_order.code', 'sales_order.transaction_date',
        'affected_product.name', 'product_quantity', 'total_discount']

    model = SalesOrderPromoHistory

    def get_csv_filename(self):
        csv_filename = "Promo_Usage_{}".format(self.kwargs.get('promo_id', None))

        if self.request.GET.get('start_date'):
            start_date = datetime.strptime(
                self.request.GET.get('start_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            start_date = start_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, start_date)

        if self.request.GET.get('end_date'):
            end_date = datetime.strptime(
                self.request.GET.get('end_date'),
                "%Y-%m-%d %H:%M:%S"
            )
            end_date = end_date.strftime("%Y/%m/%d--%H.%M.%S")
            csv_filename = "{}_{}".format(
                csv_filename, end_date)

        return csv_filename

    def get_initial_queryset(self):
        promo_id = self.kwargs.get('promo_id', None)
        outlet_id = self.kwargs.get('outlet_id', None)

        qs = super().get_initial_queryset().filter(promo_id=promo_id).select_related(
            "sales_order", "sales_order__outlet",
        )

        if not self.request.user.is_owner:
            outlet_qs = get_outlet_of_user(
                self.request.user)
            qs = qs.filter(
                sales_order__outlet__in=outlet_qs
            ).distinct()

        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )
        
        if start_date:
            qs = qs.filter(sales_order__transaction_date__gte=start_date)
        if end_date:
            qs = qs.filter(sales_order__transaction_date__lte=end_date)
        if outlet_id:
            qs = qs.filter(sales_order__outlet_id=outlet_id)

        return qs

    def render_detail_url(self, row):
        return reverse("sales:sales_order_detail", kwargs={'pk': row.sales_order.pk})

    def filter_sales_order__code(self, search_value):
        return Q(sales_order__code__icontains=search_value)


class PromoUsageListView(SterlingRoleMixin, ReportFilterMixins, TemplateView):
    template_name = 'dashboard/promo/promo_usage_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["promo"] = get_object_or_404(
            Promo, pk=self.kwargs.get("promo_id"))
        context["promo_id"] = self.kwargs.get("promo_id")
        context["outlet_id"] = self.kwargs.get("outlet_id")
        start_date, end_date = self.get_start_date_and_end_date(
            "custom",
            self.request.GET.get('start_date', None),
            self.request.GET.get('end_date', None),
        )
        context["start_date"] = start_date
        context["end_date"] = end_date
        return context
