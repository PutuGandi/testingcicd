from django import forms
from django.utils.translation import ugettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Submit

from sterlingpos.core.bootstrap import KawnFieldSetWithHelpText, KawnField
from .models import Account

MAX_UPLOAD_SIZE = 153600 #150Kb


class AccountFormHelper(FormHelper):
    include_media = False
    form_class = "col-lg-12 px-0 m-0"
    field_class = "mb-3"
    layout = Layout(
        KawnFieldSetWithHelpText(
            _('Business Information'),
            KawnField('name'),
            KawnField('address'),
            KawnField('phone'),
            KawnField('business_code'),
            Div(
                HTML(
                    '<h5>{}</h5><hr/>'.format(_('Brand'))
                ),
                css_class="col-lg-12 mt-3 p-0"
            ),
            KawnField('brand_name'),
            KawnField('brand_logo', template='dashboard/image_upload/images_field_inline_with_text.html'),
            Div(
                HTML(
                    '<h5>{}</h5><hr/>'.format(_('Social Media'))
                ),
                css_class="col-lg-12 mt-3 p-0"
            ),
            KawnField('social_facebook'),
            KawnField('social_twitter'),
            KawnField('social_instagram'),
            KawnField('website'),
        ),
        Div(
            Submit('account_update', _('Update Account Info')),
            css_class='justify-content-end col-6 p-0 flex',
            css_id='fixed-button-content',
        )
    )
    

class AccountForm(forms.ModelForm):

    helper = AccountFormHelper()

    class Meta:
        model = Account
        exclude = ("registered_via", )
        widgets = {
            'address': forms.Textarea(attrs={'rows':3}),
        }

    def clean_brand_logo(self):
        image = self.cleaned_data['brand_logo']
        if image:
            print(image.size)
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")

        return image

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['brand_logo'].label = _("Brand Logo")
        self.fields['name'].label = _("Name")
        self.fields['address'].label = _("Address")
        self.fields['phone'].label = _("Phone Number")
        self.fields['brand_logo'].help_text = "{}".format(
            _("Drag an image here or browse for an image to upload (Maximum size: 150 KB)."))
