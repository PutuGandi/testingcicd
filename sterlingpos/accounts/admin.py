from django.contrib import admin

# from sterlingpos.company.admin import CompanyObjectAdminMixin

from .models import Account, AccountOwner
from sterlingpos.users.models import User


class UserInline(admin.TabularInline):
    model = User


class AccountAdmin(admin.ModelAdmin):
    '''
        Admin View for Account
    '''
    list_display = ('name', 'address', 'phone')
    search_fields = ('name', 'address', 'phone')

    inlines = (
        UserInline,
    )


class AccountOwnerAdmin(admin.ModelAdmin):
    '''
        Admin View for AccountOwner
    '''
    list_display = ('user', 'account', 'assigned_by')
    search_fields = ('user__email', 'account__name')
    raw_id_fields = ("account", "assigned_by", "user")


admin.site.register(Account, AccountAdmin)
admin.site.register(AccountOwner, AccountOwnerAdmin)
