from django.test import TestCase
from django.test import RequestFactory, Client

from model_mommy import mommy

from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.messages.middleware import MessageMiddleware

from sterlingpos.users.models import User
from sterlingpos.accounts.models import Account
from sterlingpos.accounts.views import (
    AccountUpdateView,
    AccountOwnerCreateView,
    AccountOwnerDeleteView,
)


class BaseAcccountTestCase(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        self.user = mommy.make(User, email="testuser@example.com", account=self.account)
        self.user_2 = mommy.make(User, email="testuser2@example.com", account=self.account)
        self.user_3 = mommy.make(User, email="testuser3@example.com", account=self.account)
        self.user_4 = mommy.make(User, email="testuser4@example.com", account=self.account)
        
        self.account.owners.get_or_create(
            user=self.user, account=self.account
        )

        self.factory = RequestFactory()


class TestAccountUpdateView(BaseAcccountTestCase):

    def test_get_object(self):
        view = AccountUpdateView()
        request = self.factory.get("/fake-url")
        request.user = self.user

        """Annotate a request object with a session"""
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware()
        middleware.process_request(request)
        request.session.save()

        view.request = request

        self.assertEqual(view.get_object(), self.user.account)


class TestAssignAccountOwnerView(BaseAcccountTestCase):

    def setUp(self):

        super(TestAssignAccountOwnerView, self).setUp()
        # Instantiate the view directly. Never do this outside a test!
        self.view = AccountOwnerCreateView()
        # Generate a fake request
        request = self.factory.post("/fake-url")
        # Attach the user to the request
        request.user = self.user

        """Annotate a request object with a session"""
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware()
        middleware.process_request(request)
        request.session.save()

        # Attach the request to the view
        self.view.request = request

    def test_post_ajax(self):
        self.view.request.POST = {'id': self.user_3.pk}
        response = self.view.post_ajax(self.view.request)
        self.assertTrue(self.account.user_set.owners().filter(pk=self.user_3.pk).exists())

    def test_post_ajax_bad_user_id(self):
        self.view.request.POST = {'id': 99}
        response = self.view.post_ajax(self.view.request)
        self.assertEqual(response.status_code, 404)

    def test_post_ajax_not_owner(self):
        self.view.request.user = self.user_4
        self.view.request.POST = {'id': self.user_3.pk}
        response = self.view.post_ajax(self.view.request)
        self.assertEqual(response.status_code, 404)


class TestDeleteAccountOwnerView(BaseAcccountTestCase):

    def setUp(self):

        super(TestDeleteAccountOwnerView, self).setUp()
        # Instantiate the view directly. Never do this outside a test!
        self.view = AccountOwnerDeleteView()
        # Generate a fake request
        request = self.factory.post("/fake-url")
        # Attach the user to the request
        request.user = self.user

        """Annotate a request object with a session"""
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware()
        middleware.process_request(request)
        request.session.save()

        # Attach the request to the view
        self.view.request = request

    def test_post_ajax(self):
        self.account.owners.get_or_create(
            user=self.user_2, account=self.account
        )
        self.view.request.POST = {'id': self.user_2.pk}
        response = self.view.post_ajax(self.view.request)
        self.assertFalse(self.account.user_set.owners().filter(pk=self.user_2.pk).exists())

    def test_post_ajax_bad_user_id(self):
        self.view.request.POST = {'id': 99}
        response = self.view.post_ajax(self.view.request)
        self.assertEqual(response.status_code, 404)

    def test_post_ajax_not_owner(self):
        self.view.request.user = self.user_4
        self.view.request.POST = {'id': self.user_3.pk}
        response = self.view.post_ajax(self.view.request)
        self.assertEqual(response.status_code, 404)
