from django.test import TestCase
from django.db import IntegrityError

from model_mommy import mommy

from sterlingpos.accounts.models import Account, AccountOwner
from sterlingpos.core.models import set_current_tenant
from sterlingpos.accounts.signals import create_initial_data_for_account
from django.db.models.signals import post_save


class TestAccount(TestCase):

    def test__str__(self):
        account = Account.objects.create(name='Account-1')
        self.assertEqual(
            account.__str__(),
            '{}'.format(account.name)
        )

    def test_signal_create_initial_data_for_account(self):
        account_1 = Account.objects.create(name='Account-1')
        account_2 = Account.objects.create(name='Account-1')
        self.assertTrue(account_1.uom_set.count(), 10)
        self.assertTrue(account_1.salesorderoption_set.count(), 5)
        self.assertTrue(account_2.uom_set.count(), 10)
        self.assertTrue(account_2.salesorderoption_set.count(), 5)

        self.assertTrue(account_1.enabledpaymentmethod_set.count(), 1)
        self.assertTrue(account_1.tablemanagementsettings_set.count(), 1)
        self.assertTrue(account_1.deviceuser_set.count(), 1)
        self.assertTrue(account_1.deviceuser_set.filter(permission='manager').count(), 1)
        self.assertTrue(account_1.surcharge_set.count(), 7)
        self.assertTrue(account_1.customergroup_set.count(), 1)


class TestAccountOwner(TestCase):

    def test__str__(self):
        account = Account.objects.create(name='Account-1')
        user = mommy.make('users.User', email='test-user@testament.com', account=account)
        account_owner = AccountOwner.objects.create(account=account, user=user)
        self.assertEqual(
            account_owner.__str__(),
            'test-user owner of Account-1'
        )
