from django.apps import AppConfig
from django.db.models.signals import post_save


class AccountsConfig(AppConfig):
    name = "sterlingpos.accounts"
    verbose_name = "Accounts"
    permission_name = "account"

    def ready(self):
        from .signals import create_initial_data_for_account

        post_save.connect(
            create_initial_data_for_account,
            sender="accounts.Account")
