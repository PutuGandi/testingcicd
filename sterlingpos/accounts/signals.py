from django.conf import settings
from django.dispatch import Signal
from django.db import IntegrityError

from sterlingpos.catalogue.utils import DEFAULT_UOM
from sterlingpos.sales.utils import (
    DEFAULT_SALES_ORDER_OPTION,
    DEFAULT_PAYMENT_METHOD,
    DEFAULT_TRANSACTION_TYPE_REASON_FOC,
)
from sterlingpos.sales.models import (
    PaymentMethod,
    PaymentMethodUsage,
    TransactionTypeReason,
)
from sterlingpos.subscription.models import DeviceUserLiteSubscription
from sterlingpos.surcharges.models import Surcharge
from sterlingpos.customer.models import CustomerGroup
from sterlingpos.notification.models import NotificationSetting
from sterlingpos.role.utils import create_initial_roles_for_account
from sterlingpos.core.models import get_current_tenant, set_current_tenant


def create_initial_data_for_account(sender, **kwargs):
    instance = kwargs.get('instance')
    created = kwargs.get('created')
    current_tenant = get_current_tenant()
    if created:

        if current_tenant and instance != current_tenant:
            set_current_tenant(instance)

        default_uom = map(
            lambda data: instance.uom_set.model(account=instance, **data),
            DEFAULT_UOM)
        default_sales_order_option = map(
            lambda data: instance.salesorderoption_set.model(account=instance, **data),
            DEFAULT_SALES_ORDER_OPTION)
        instance.uom_set.bulk_create(default_uom)
        instance.salesorderoption_set.bulk_create(default_sales_order_option)
        instance.enabledpaymentmethod_set.create(account=instance)
        enabledpaymentmethod = instance.enabledpaymentmethod_set.first()

        for sales_order in instance.salesorderoption_set.all():
            default_surcharge = [
                Surcharge(
                    order_option=sales_order,
                    account=sales_order.account)
            ]
            instance.surcharge_set.bulk_create(default_surcharge)

        for default_payment in DEFAULT_PAYMENT_METHOD:
            if PaymentMethod.objects.filter(
                name=default_payment.get('name'),
                payment_sub=default_payment.get('payment_sub'),
                    payment_item=default_payment.get('payment_item')).exists():
                payment = PaymentMethod.objects.get(
                    name=default_payment.get('name'),
                    payment_sub=default_payment.get('payment_sub'),
                    payment_item=default_payment.get('payment_item'))
                PaymentMethodUsage.objects.create(
                    payment_method=payment,
                    enabled_payment_method=enabledpaymentmethod,
                    account=instance,
                )
                enabledpaymentmethod.payment_methods.add(payment)

        foc_reason_list = [
            TransactionTypeReason(
                description=description,
                transaction_type='foc',
                archived=False,
                account=instance,
            )
            for description in DEFAULT_TRANSACTION_TYPE_REASON_FOC
        ]
        foc_reason_objs = TransactionTypeReason.objects.bulk_create(
            foc_reason_list)

        instance.waitersettings_set.create(account=instance)
        instance.kitchendisplaysetting_set.create(account=instance)
        instance.cashbalancesetting_set.create(account=instance)
        instance.inventorysetting_set.create(account=instance)

        sales_order_option = instance.salesorderoption_set.first()
        table_order = instance.tableordersettings_set.create(
            order_option=sales_order_option,
            account=instance,
        )
        table_management = instance.tablemanagementsettings_set.create(account=instance)
        table_management.colorstatus_set.get_or_create(
            name="Open",
            status_type="open",
            account=table_management.account,
            is_active=True
        )
        table_management.colorstatus_set.get_or_create(
            name="Occupied",
            status_type="occupied",
            color_code="#ff6259",
            account=table_management.account,
            is_active=True
        )

        manager, staff, cashier = create_initial_roles_for_account(
            instance)

        device_user = instance.deviceuser_set.create(
            username='admin',
            name='admin',
            email='admin@changethisemail.com',
            pin=123456,
            permission='manager',
            all_outlet_access=True,
            account=instance,
            role=manager)

        DeviceUserLiteSubscription.objects.create(
            device_user=device_user,
            is_free=True,
            account=instance)

        CustomerGroup.objects.create(
            name='Default Group',
            account=instance
        )

        for notification_type in NotificationSetting.NOTIFICATION_TYPES:
            try:
                NotificationSetting.objects.create(
                    account=instance,
                    notification_type=notification_type[0],
                )
            except IntegrityError as e:
                print(f"ERROR RAISED {e}=========\n")
                pass

        if current_tenant:
            set_current_tenant(current_tenant)
