from django.conf.urls import url

from . import views

app_name = "account"
urlpatterns = [
    url(regex=r"^update/$", view=views.AccountUpdateView.as_view(), name="update"),
    url(regex=r"^owner/create/$", view=views.AccountOwnerCreateView.as_view(), name="assign-owner"),
    url(regex=r"^owner/delete/$", view=views.AccountOwnerDeleteView.as_view(), name="remove-owner"),
]
