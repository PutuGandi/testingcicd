from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify

from imagekit.models import ProcessedImageField
from imagekit.processors import Adjust, ResizeToFit

from safedelete.models import HARD_DELETE

from model_utils import Choices
from model_utils.models import TimeStampedModel
from sterlingpos.core.models import (
    SterlingTenantModel,
    SterlingTenantForeignKey, SterlingTenantOneToOneField,
    get_current_tenant, set_current_tenant
)

from sterlingpos.utils.storage import OverwriteStorage


def get_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = "brand_{}.{}".format(slugify(instance.brand_name), ext)
    return '{}/brand-logo/{}'.format(instance.pk, new_filename)


class Account(TimeStampedModel):

    REGISTRATION_ACCESS = Choices(
        ('web', _('Website')),
        ('mobile_lite', _('Mobile Lite App')),
    )

    name = models.CharField(_("Account Name"), blank=True, max_length=255)
    address = models.TextField(_("Account Address"), blank=True, max_length=255)
    phone = models.CharField(_("Account Phone Number"), blank=True, max_length=255)
    business_code = models.CharField(_("Business Code"), blank=True, max_length=50)

    brand_name = models.CharField(_("Brand Name"), blank=True, max_length=255)
    brand_logo = ProcessedImageField(
        upload_to=get_upload_path,
        storage=OverwriteStorage(),
        blank=True,
        null=True,
        processors=[ResizeToFit(width=240, height=240, upscale=False)],
        format='JPEG',
        options={'quality': 60}
    )

    social_facebook = models.CharField(_("Facebook"), blank=True, default='', max_length=255)
    social_twitter = models.CharField(_("Twitter"), blank=True, default='', max_length=255)
    social_instagram = models.CharField(_("Instagram"), blank=True, default='', max_length=255)
    website = models.CharField(_("Website"), blank=True, default='', max_length=255)

    registered_via = models.CharField(
        _("Registered Via"), max_length=15,
        choices=REGISTRATION_ACCESS,
        default=REGISTRATION_ACCESS.web)

    class Meta:
        verbose_name = "Account"
        verbose_name_plural = "Accounts"

    def __str__(self):
        return self.name

    def __init__(self, *args, **kwargs):
        super(Account, self).__init__(*args, **kwargs)
        self._meta.get_field('name').error_messages.update({
            'unique': _("This account name has already been registered.")
        })


class AccountOwner(SterlingTenantModel, TimeStampedModel):

    _safedelete_policy = HARD_DELETE

    account = models.ForeignKey('accounts.Account', related_name='owners', on_delete=models.CASCADE)
    user = SterlingTenantOneToOneField('users.User', related_name='owner_of', on_delete=models.CASCADE)
    assigned_by = SterlingTenantForeignKey('users.User', null=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "AccountOwner"
        verbose_name_plural = "AccountOwners"
        unique_together = (('account', 'user'), )

    def __str__(self):
        current_tenant = get_current_tenant()

        if current_tenant != self.account:
            set_current_tenant(self.account)

        string ="{} owner of {}".format(
            self.user.get_name, self.account.name)

        if current_tenant:
            if current_tenant != self.account:
                set_current_tenant(current_tenant)
        else:
            set_current_tenant(None)

        return string
