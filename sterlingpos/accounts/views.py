from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import UpdateView, View
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _, ugettext

from braces.views import (
    LoginRequiredMixin,
    JSONResponseMixin,
    AjaxResponseMixin,
    FormMessagesMixin,
)

from sterlingpos.core.mixins import OwnerOnlyMixin
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.users.models import User

from .models import Account, AccountOwner
from .forms import AccountForm


class AccountUpdateView(
    LoginRequiredMixin, SterlingRoleMixin,
    FormMessagesMixin, UpdateView):

    model = Account
    form_class = AccountForm
    success_url = reverse_lazy('accounts:update')
    template_name = 'accounts/account_form.html'
    form_valid_message = _('Account has been updated.')
    form_invalid_message = _("Something went wrong, please check form inputs")

    def get_object(self):
        # Only get the User record for the user making the request
        return self.request.user.account

class AccountOwnerCreateView(LoginRequiredMixin, SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        if request.user.is_owner:
            try:
                status_code = 200
                response['status'] = 'success'
                user_id = request.POST.get('id', '')

                user = request.user.account.user_set.members().get(pk=user_id)
                account_owner, created = request.user.account.owners.get_or_create(
                    account=request.user.account, user=user, assigned_by=request.user)

                messages.success(self.request, '{}'.format(
                    _('Account has been updated as Owner.')))
            except (ValueError, User.DoesNotExist):
                status_code = 404

        return self.render_json_response(response, status=status_code)


class AccountOwnerDeleteView(LoginRequiredMixin, SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404

        if request.user.is_owner:
            try:
                status_code = 200
                response['status'] = 'success'
                user_id = request.POST.get('id', '')

                account_owner = request.user.account.owners.exclude(
                    user=request.user
                ).get(
                    account=request.user.account, user__pk=user_id)
                account_owner.delete()

                messages.success(self.request, '{}'.format(
                    _('Account has been revoked from Owner.')))
            except (ValueError, AccountOwner.DoesNotExist):
                status_code = 404

        return self.render_json_response(response, status=status_code)
