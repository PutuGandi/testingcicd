# Generated by Django 2.0.4 on 2018-11-12 07:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


def migrate_first_user_of_account_as_account_owner(apps, schema_editor):
    Account = apps.get_model("accounts", "Account")

    for account in Account.objects.all():

        user = account.user_set.order_by('pk').first()
        if user:
            account_owner, _ = account.owners.get_or_create(
                account=account, user=user)


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0006_auto_20181012_0549'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountOwner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='owners', to='accounts.Account')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='owner_of', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'AccountOwner',
                'verbose_name_plural': 'AccountOwners',
            },
        ),
        migrations.AlterUniqueTogether(
            name='accountowner',
            unique_together={('account', 'user')},
        ),
        migrations.RunPython(
            migrate_first_user_of_account_as_account_owner, migrations.RunPython.noop),
    ]
