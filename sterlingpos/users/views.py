import uuid
import datetime
import django_multitenant

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.views.generic import (
    DetailView, ListView, RedirectView, UpdateView, TemplateView,
    FormView, View
)
from django.db.models import Q
from django.db import IntegrityError, transaction
from django.http import Http404, HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.shortcuts import get_object_or_404

from allauth.account.utils import get_next_redirect_url, complete_signup
from allauth.account import app_settings
from allauth.account.views import ConfirmEmailView, LoginView, PasswordChangeView

from formtools.wizard.views import SessionWizardView

from django_datatables_view.base_datatable_view import BaseDatatableView
from invitations.views import (
    SendInvite as BaseSendInvite,
    AcceptInvite as BaseAcceptInvite
)
from braces.views import (
    AnonymousRequiredMixin, LoginRequiredMixin,
    JSONResponseMixin, AjaxResponseMixin,
)

from .models import User, UserInvitation
from .forms import (
    UserSignupForm, UserForm,
    InviteeUserSignupForm, FormChangePassword,
    UserInvitationForm
)
from sterlingpos.core.mixins import OwnerOnlyMixin
from sterlingpos.outlet.forms import BaseOutletForm
from sterlingpos.outlet.utils import acronym
from sterlingpos.accounts.models import Account
from sterlingpos.accounts.forms import AccountForm
from sterlingpos.subscription.models import SubscriptionPlan, Subscription, _recurrence_unit_days
from sterlingpos.subscription.forms import SubscriptionForm


class UserDetailView(OwnerOnlyMixin, DetailView):
    model = User

    def get_object(self):
        # Only get the User record for the user making the request
        return self.request.user


class UserRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("dashboard")


class UserUpdateView(UpdateView):

    # we already imported User in the view code above, remember?
    model = User
    form_class = UserForm
    account_form = AccountForm

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(UserUpdateView, self).get_context_data(**kwargs)

        if 'user_form' not in context:
            context['user_form'] = self.form_class(instance=self.request.user)

        if 'account_form' not in context:
            context['account_form'] = self.account_form(instance=self.request.user.account)
        return context

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_object(self):
        # Only get the User record for the user making the request
        self.object = self.request.user
        return self.object


class UserListView(OwnerOnlyMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = "email"
    slug_url_kwarg = "email"


def referred_via_pricing_page(wizard):
    """Save subscription_plan if user is reffered via pricing page"""
    # Get plan_id from url, then check validity
    plan_id = wizard.request.GET.get('plan_id', None)
    if plan_id:
        wizard.storage.data['subscription_plan'] = plan_id

    # Always return True
    return True


class WizardUserSignUpView(AnonymousRequiredMixin, SessionWizardView):
    condition_dict = {'user_form': referred_via_pricing_page}
    form_list = [
        ('user_form', UserSignupForm),
        ('outlet_form', BaseOutletForm),
    ]
    template_name = 'account/signup.html'
    redirect_field_name = 'next'
    success_url = reverse_lazy("account_email_verification_sent")
    subscription_plan = None

    def post(self, *args, **kwargs):
        self.backup_storage = self.storage
        return super(WizardUserSignUpView, self).post(*args, **kwargs)

    def get_form_initial(self, step):
        email = self.request.GET.get('email', '')
        if step == 'user_form':
            return self.initial_dict.get(step, {'email': email})
        else:
            return self.initial_dict.get(step, {})

    def process_outlet(self, form, account):
        outlet = form.save(commit=False)
        branch_id = "{}-{}".format(acronym(outlet.name), uuid.uuid4().hex[:4])
        outlet.account = account
        outlet.branch_id = branch_id
        outlet.save()

        return outlet

    def done(self, form_list, form_dict, **kwargs):
        try:
            with transaction.atomic():
                user = form_dict['user_form'].save(
                    request=self.request
                )
                device_user = user.account.deviceuser_set.first()
                device_user.user = user
                device_user.email = user.email
                device_user.save()

                outlet = self.process_outlet(form_dict['outlet_form'], user.account)

                subscription_data = {
                    'subscription_plan': self.storage.data.get('subscription_plan')
                }
                subscription_form = SubscriptionForm(subscription_data)

                if not subscription_form.is_valid():
                    subscription_data = {
                        'subscription_plan': self.get_default_subscription().slug
                    }
                    subscription_form = SubscriptionForm(subscription_data)
                    subscription_form.is_valid()

                subscription_form.outlet = outlet
                subscription_form.account = user.account

                subscription = subscription_form.save(commit=False)
                subscription.is_trial = True
                subscription.save()

                self.register_loyality_auth(form_dict['user_form'], user)

                return complete_signup(self.request, user,
                                       app_settings.EMAIL_VERIFICATION,
                                       self.get_success_url())
        except IntegrityError:
            email = form_dict['user_form'].cleaned_data.get('email')
            if User.objects.filter(email=email).exists():
                return HttpResponseRedirect(self.get_success_url())

            self.storage = self.backup_storage
            form = self.get_form(
                data=self.request.POST, files=self.request.FILES)
            return self.render_done(form, **kwargs)

    def get_default_subscription(self):
        try:
            subscription_plan = SubscriptionPlan.objects.get(
                slug="basic-subscription")
        except SubscriptionPlan.DoesNotExist:
            subscription_plan = SubscriptionPlan.objects.create(
                name='Basic Subscription',
                slug='basic-subscription',
                price=0,
                trial_period=30,
                trial_unit='D',
                recurrence_period=0,
                recurrence_unit=None,
                is_public=False,
            )
        return subscription_plan

    def register_loyality_auth(self, user_form, user):
        pass

    def create_default_trial_subscription(self, user, outlet):
        try:
            plan = SubscriptionPlan.objects.get(~Q(trial_unit=0))
            Subscription.objects.create(
                account=user.account,
                outlet=outlet,
                subscription_plan=plan,
                expires=datetime.date.today() +
                datetime.timedelta(plan.trial_period * _recurrence_unit_days[plan.trial_unit]),
                cancelled=False
            )
        except SubscriptionPlan.DoesNotExist as err:
            raise err

    def get_success_url(self):
        return self.success_url


class EmailConfirmationRedirectView(TemplateView):
    template_name = "account/email_confirmed.html"


class EmailConfirmation(ConfirmEmailView):

    def get_redirect_url(self):
        return reverse('users:confirmed')


class ChangePasswordView(PasswordChangeView):
    form_class = FormChangePassword
    success_url = reverse_lazy("users:update")


class UserListViewDashboard(OwnerOnlyMixin, TemplateView):
    template_name = "dashboard/user/user_list.html"


class UserListJson(OwnerOnlyMixin, BaseDatatableView):
    model = User
    columns = ['email', 'get_name', 'is_owner', 'action']
    order_columns = ['email', 'email', 'owner_of', 'action']

    def render_column(self, row, column):
        if column == "action":
            action_data = {
                'id': row.id,
                'email': escape(row.email),
                'is_founder': row.is_owner,
                'request_is_founder': self.request.user.is_owner,
                'is_self': self.request.user.pk == row.pk,
            }
            return action_data
        return super(UserListJson, self).render_column(row, column)


class InvitationViewDashboard(OwnerOnlyMixin, ListView):
    template_name = "dashboard/invitations/invitation_list.html"
    model = UserInvitation


class InvitationListJson(OwnerOnlyMixin, BaseDatatableView):
    model = UserInvitation
    columns = ['id', 'email', 'inviter.username', 'sent', 'accepted']
    order_columns = ['id', 'email', 'inviter__username', 'sent', 'accepted']
    datetime_col = ['sent', ]


class SendInvite(OwnerOnlyMixin, BaseSendInvite):
    form_class = UserInvitationForm
    success_url = reverse_lazy('send-invite-complete')
    template_name = "invitations/forms/invite.html"

    def form_valid(self, form):
        email = form.cleaned_data["email"]
        try:
            invitation = UserInvitation.objects.get(email=email)
            invitation.accepted = False
            invitation.save()
            invitation.send_invitation(self.request)
        except UserInvitation.DoesNotExist:
            invite = form.save(email)
            invite.inviter = self.request.user
            invite.save()
            invite.send_invitation(self.request)

        return HttpResponseRedirect(self.get_success_url())


class SendInviteComplete(TemplateView):
    template_name = "invitations/forms/success.html"


class AcceptInvite(BaseAcceptInvite):

    def get_signup_redirect(self):
        return reverse('invitee-registration')


class InviteeRegistration(FormView):
    template_name = 'invitations/forms/signup.html'
    form_class = InviteeUserSignupForm
    success_url = reverse_lazy('invitee-registration-success')

    def dispatch(self, request, *args, **kwargs):
        if hasattr(request, 'session') and request.session.get('account_verified_email'):
            self.invitation = self.get_invitation()
            return super(InviteeRegistration, self).dispatch(request, *args, **kwargs)
        raise Http404

    def get_invitation(self):
        invitation = get_object_or_404(
            UserInvitation,
            email=self.request.session.get('account_verified_email'))
        return invitation

    def get_initial(self):
        initial = super(InviteeRegistration, self).get_initial()
        email = self.request.session.get('account_verified_email')
        initial.update({
            'email': email
        })
        return initial

    def form_valid(self, form):
        """
        All user created under UserDasboard View by Company Manager,
        will always be usable right after w/o email activation.
        """
        user = form.save(self.request, self.invitation.account)
        self.invitation.device_user.user = user
        self.invitation.device_user.save()
        user.emailaddress_set.update(verified=True)

        return complete_signup(
            self.request, user,
            None,
            self.get_success_url())


class InviteeRegistrationComplete(TemplateView):
    template_name = "invitations/forms/register_success.html"


class InvitationDeleteView(OwnerOnlyMixin, View):

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('invitation:invitation_list_dashboard'))

    def post(self, request, *args, **kwargs):
        user_list = map(int, request.POST.get('user_list', '').split(','))
        request.user.invitation_set.filter(pk__in=user_list, accepted=False).delete()
        return HttpResponseRedirect(reverse('invitation:invitation_list_dashboard'))


class UserDeleteView(OwnerOnlyMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            user_id = request.POST.get('id', '')

            user = User.objects.members().get(pk=user_id)
            user.delete()

            messages.success(self.request, '{}'.format(
                _('User has been deleted.')))
        except (ValueError, User.DoesNotExist):
            pass

        return self.render_json_response(response, status=status_code)


class InvitationResendView(OwnerOnlyMixin, View):

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('invitation:invitation_list_dashboard'))

    def post(self, request, *args, **kwargs):
        user_list = map(int, request.POST.get('user_list', '').split(','))
        if not request.user.is_staff:
            qs = request.user.invitation_set.filter(pk__in=user_list)
        else:
            qs = UserInvitation.objects.filter(pk__in=user_list)

        qs.update(accepted=False)
        for invitation in qs:
            invitation.send_invitation(request)

        return HttpResponseRedirect(reverse('invitation:invitation_list_dashboard'))


class UserInvitationDeleteView(
    OwnerOnlyMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            pk = request.POST.get('id', '')

            invitation = UserInvitation.objects.get(pk=pk)
            invitation.delete()

            messages.success(self.request, '{}'.format(
                _('Invitation has been deleted.')))
        except (ValueError, User.DoesNotExist):
            pass

        return self.render_json_response(response, status=status_code)

