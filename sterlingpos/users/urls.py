from django.conf.urls import url

from . import views

app_name = "users"
urlpatterns = [
    # url(regex=r"^$", view=views.UserListView.as_view(), name="list"),
    # url(regex=r"^detail/$", view=views.UserDetailView.as_view(), name="detail"),

    url(regex=r"^~redirect/$", view=views.UserRedirectView.as_view(), name="redirect"),
    url(regex=r"^update/$", view=views.UserUpdateView.as_view(), name="update"),
    url(regex=r"^~confirmed/$", view=views.EmailConfirmationRedirectView.as_view(), name="confirmed"),
    url(regex=r"^password/change/$", view=views.ChangePasswordView.as_view(), name="password_change"),
]
