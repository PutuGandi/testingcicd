import datetime
import subprocess
import csv
import io

from celery import shared_task

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.db import models
from django.db.models import (
    Avg, Sum, Count, Q, IntegerField, Value
)
from django.db.models.functions import Coalesce

from sterlingpos.accounts.models import Account
from sterlingpos.users.models import User


def user_registration_qs(start_date, end_date):
    basic_filter = Q(pk__isnull=False)
    if start_date:
        basic_filter &= Q(date_joined__gte=start_date)
    if end_date:
        basic_filter &= Q(date_joined__lte=end_date)

    users = User.objects.filter(basic_filter)
    
    return users

def sent_user_registration_report(start_date, end_date, recipients=None):

    subject = "KAWN User Registration Report {} s/d {}".format(
        start_date, end_date
    )

    to = settings.CUSTOM_REPORT_RECIPIENTS.get(
        "user_registration_report")
    if recipients:
        to = recipients

    users = user_registration_qs(start_date, end_date)

    user_values_list = users.values_list(
        "account__name", 
        "email",
        "date_joined",
        "account__outlet__subscription__expires",
        "account__outlet__subscription__is_trial",
        "account__outlet__subscription__is_free",
        "account__outlet__phone",
    ).distinct('account__name', 'email')

    pseudo_buffer = io.StringIO()
    writer = csv.writer(pseudo_buffer)
    writer.writerow([
        "Account", "Email", "Date Joined", "Expires",
        "Is Trial", "Is Free(KAWN Lite)", "Phone"])
    for row in user_values_list:
        writer.writerow(row)

    text_content = '{}'.format(
        "".join([
            x for x in (
                "{}, {}, {}, {}, {} ,{} ,{}\n".format(*user_data)
                for user_data in user_values_list
            )
        ])
    )
    template_name = "dashboard/user/email/registration_report.html"
    html_content = render_to_string(
        template_name, {
            'object_list': users,
            'start_date': start_date,
            'end_date': end_date
        })

    msg = EmailMultiAlternatives(
        subject=subject, 
        body=text_content, to=to)
    msg.attach_alternative(html_content, "text/html")
    msg.attach(
        f"user_registration_{start_date}_{end_date}.csv",
        pseudo_buffer.getvalue(),
        "text/csv")
    msg.send()
    pseudo_buffer.close()

@shared_task(bind=True, max_retries=5)
def daily_user_registration_report(self):
    try:
        start_date = datetime.date.today() - datetime.timedelta(days=1)
        end_date = datetime.date.today()

        sent_user_registration_report(start_date, end_date)
    except:
        self.retry(countdown= 2 ** self.request.retries)


