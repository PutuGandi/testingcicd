import datetime
import re
from django.conf import settings
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.urls.resolvers import get_resolver
from django.urls.exceptions import Resolver404
from django.utils.deprecation import MiddlewareMixin

from braces.views import LoginRequiredMixin

from sterlingpos.subscription.models import Subscription


class SubscriptionUserMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if request.user.is_authenticated:
            tenant = request.user.account
            trial = False
            subscription_info = ""
            secondary_list = ""

            subscriptions = Subscription.objects.filter(
                account=tenant,
                expires__gt=datetime.date.today(),
            )

            if subscriptions.exists():
                request.session['subscription'] = True
                subscription = subscriptions.first()

                if subscriptions.filter(is_trial=True).exists():
                    subscription = subscriptions.filter(is_trial=True).first()
                    trial = True
                    subscription_info = 'subscription-info-appears'
                    secondary_list = "secondary-list-subscribtion"

                request.session['is_trial'] = trial
                request.session['subscription_info'] = subscription_info
                left_days = subscription.expires - datetime.date.today()
                request.session['left_days'] = left_days.days
                request.session['secondary_list'] = secondary_list
            else:
                expired_subscription = Subscription.objects.filter(
                    account=tenant,
                    expires__lte=datetime.date.today()
                )
                if expired_subscription.exists():
                    request.session['subscription'] = False
                    request.session['subscription_info'] = 'subscription-info-appears'
                    request.session['secondary_list'] = "secondary-list-subscribtion"

                    if expired_subscription.filter(is_trial=True).exists():
                        request.session['is_trial'] = True


class UserDashboardRedirectMiddleware(MiddlewareMixin):

    def process_request(self, request):
        user = request.user
        path = request.path
        if not user.is_anonymous and user.is_authenticated:
            if path == reverse('home'):
                return HttpResponseRedirect(reverse('dashboard'))


class IsAuthenticatedMiddleware(MiddlewareMixin):

    def __init__(self, *args, **kwargs):
        self.non_auth_url_name = getattr(settings, 'NON_AUTH_URL_NAME', [])
        super(IsAuthenticatedMiddleware, self).__init__(*args, **kwargs)

    def is_non_auth_view(self, view_name):
        matching_results = [re.search(pattern, view_name) for pattern in self.non_auth_url_name]

        return any(matching_results)

    def process_request(self, request):
        try:
            resolver_match = get_resolver().resolve(request.path)

            if (not request.user.is_authenticated
                    and not self.is_non_auth_view(resolver_match.view_name)):
                response = LoginRequiredMixin().no_permissions_fail(request)
                return response
        except Resolver404 as e:
            pass
