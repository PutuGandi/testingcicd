from django.test import TestCase
from django.test.client import Client
from django.urls import reverse

from model_mommy import mommy

from sterlingpos.users.models import User
from sterlingpos.accounts.models import Account
from sterlingpos.outlet.models import Outlet


class TestUserLogin(TestCase):

    def setUp(self):
        self.client = Client()
        self.account = mommy.make(Account)
        self.user = mommy.make(User, account=self.account)
        mommy.make(Outlet, branch_id='branch-01', account=self.account)
        self.user.set_password('test')
        self.user.save()

    def test_login_successfull(self):
        """
            Test Login Successfull.
        """
        self.client.login(email=self.user.email, password='test')
        response = self.client.get(reverse('dashboard'))
        self.assertEqual(response.status_code, 200)


class TestLoginMultipleUserSingleAccount(TestCase):

    def setUp(self):
        self.client_1 = Client()
        self.client_2 = Client()

        self.account = mommy.make(Account)
        mommy.make(Outlet, branch_id='branch-01', account=self.account)

        self.user = mommy.make(User, account=self.account)
        self.user.set_password('test')
        self.user.save()

        self.user_2 = mommy.make(User, account=self.account)
        self.user_2.set_password('test')
        self.user_2.save()

    def test_multiple_user_login_successful(self):
        """
            Test Login with different User and using same Account.
        """
        self.client_1.login(email=self.user.email, password='test')
        response_user = self.client_1.get(reverse('dashboard'))
        self.assertEqual(response_user.status_code, 200)

        self.client_2.login(email=self.user_2.email, password='test')
        response_user_2 = self.client_2.get(reverse('dashboard'))
        self.assertEqual(response_user_2.status_code, 200)
