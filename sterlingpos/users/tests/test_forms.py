from django.test import TestCase
from django.test import RequestFactory
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser
from django.contrib.sessions.middleware import SessionMiddleware
from model_mommy import mommy

from sterlingpos.accounts.models import Account
from ..forms import (
    UserSignupForm, UserForm, FormChangePassword,
    InviteeUserSignupForm
)

from ..models import User


class TestUserSignupForm(TestCase):

    def test_clean_account(self):

        valid_data = {
            'account_name': 'sterlingpos'
        }
        form = UserSignupForm(valid_data)
        form.is_valid()

        account = form.cleaned_data['account_name']
        self.assertEqual(account, valid_data['account_name'])

    def test_clean_account_invalid(self):
        """
            Account name is already exist.
        """
        exist_account = mommy.make(Account)

        invalid_data = {
            'account_name': exist_account.name
        }
        form = UserSignupForm(invalid_data)
        self.assertFalse(form.is_valid())

    def test_save(self):

        self.factory = RequestFactory()

        form_data = {
            'account_name': 'sterlingpos',
            'email': 'example@sterlingpos.com',
            'password1': 'ex4mpl3pas5w0rd',
        }

        request = self.factory.post(reverse('account_signup'), form_data)
        request.user = AnonymousUser()

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        form = UserSignupForm(data=form_data)
        self.assertTrue(form.is_valid())
        user = form.save(request)

        self.assertEqual(user.account.name, form_data['account_name'])
        self.assertEqual(user.email, form_data['email'])

        self.assertTrue(user.is_owner)


class TestUserForm(TestCase):

    def test_form_valid(self):

        valid_data = {
            'name': 'kawn'
        }
        form = UserForm(valid_data)
        form.is_valid()

        name = form.cleaned_data['name']
        self.assertEqual(name, valid_data['name'])


class TestFormChangePassword(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        self.user = mommy.make(User, account=self.account)

    def test_form_valid(self):
        valid_data = {
            'oldpassword': self.user.password,
            'password1': '1sampai6',
            'password2': '1sampai6'
        }
        form = FormChangePassword(valid_data)

    def test_from_invalid(self):
        invalid_data = {
            'oldpassword': self.user.password,
            'password1': '1sampai6',
            'password2': '123456'
        }
        form = FormChangePassword(invalid_data)
        self.assertFalse(form.is_valid())


class TestInviteeUserSignupForm(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        self.form = InviteeUserSignupForm
        self.factory = RequestFactory()

    def test_form_save(self):
        data = {
            'email': 'testmail@mail.com',
            'password1': 'test.password',
        }
        request = self.factory.post('fake-url', data)

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        form = self.form(data)
        form.account = self.account
        self.assertTrue(form.is_valid())
        user = form.save(request, self.account)
        self.assertEqual(user.account, self.account)
