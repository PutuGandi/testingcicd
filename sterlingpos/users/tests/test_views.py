import datetime
from django.test import RequestFactory, Client
from django.http import Http404
from django.urls import reverse
from model_mommy import mommy

from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from test_plus.test import TestCase

from ..views import (
    UserRedirectView,
    UserUpdateView,
    EmailConfirmation,
    UserDetailView,
    UserDeleteView,

    InviteeRegistration,
)
from sterlingpos.users.models import User, UserInvitation
from sterlingpos.accounts.models import Account
from ..forms import UserForm
from sterlingpos.accounts.forms import AccountForm
from sterlingpos.core.models import set_current_tenant, get_current_tenant


class BaseUserTestCase(TestCase):

    def setUp(self):
        self.account = Account.objects.create(name="Company A", address="Jl. Tebet Raya Timur", phone="021-182318")
        set_current_tenant(self.account)

        self.user = User.objects.create_user(email="testuser@example.com", password="example", account=self.account)
        self.user_2 = User.objects.create_user(email="testuser2@example.com", password="example", account=self.account)
        self.user_3 = User.objects.create_user(email="testuser3@example.com", password="example", account=self.account)
        self.user_4 = User.objects.create_user(email="testuser4@example.com", password="example", account=self.account)

        self.account.owners.create(account=self.account, user=self.user)
        self.account.owners.create(account=self.account, user=self.user_2)

        self.factory = RequestFactory()

    def tearDown(self):
        set_current_tenant(None)


class TestUserDetailView(BaseUserTestCase):

    def test_get_object(self):
        view = UserDetailView()
        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request

        self.assertEqual(view.get_object(), self.user)


class TestUserRedirectView(BaseUserTestCase):

    def test_get_redirect_url(self):
        # Instantiate the view directly. Never do this outside a test!
        view = UserRedirectView()
        # Generate a fake request
        request = self.factory.get("/fake-url")
        # Attach the user to the request
        request.user = self.user
        # Attach the request to the view
        view.request = request
        # Expect: '/users/testuser/', as that is the default username for
        #   self.make_user()
        self.assertEqual(view.get_redirect_url(), "/dashboard/")


class TestUserUpdateView(BaseUserTestCase):

    def setUp(self):
        # call BaseUserTestCase.setUp()
        super(TestUserUpdateView, self).setUp()
        # Instantiate the view directly. Never do this outside a test!
        self.view = UserUpdateView()
        # Generate a fake request
        request = self.factory.get("/fake-url")
        # Attach the user to the request
        request.user = self.user
        # Attach the request to the view
        self.view.request = request

    def test_get_success_url(self):
        # Expect: '/users/testuser/', as that is the default username for
        #   self.make_user()
        self.assertEqual(self.view.get_success_url(), "/dashboard/")

    def test_get_object(self):
        # Expect: self.user, as that is the request's user object
        self.assertEqual(self.view.get_object(), self.user)

    def test_get_context_data(self):
        user_form = UserForm(instance=self.view.request.user)
        account_form = AccountForm(instance=self.view.request.user.account)
        context = self.view.get_context_data()
        # self.assertEqual(context['user_form'], user_form)
        # self.assertEqual(context['account_form'], account_form)


class TestEmailConfirmationView(BaseUserTestCase):

    def test_get_redirect_url(self):
        view = EmailConfirmation()
        request = self.factory.get("/fake-url")
        request.user = self.user
        view.request = request
        self.assertEqual(view.get_redirect_url(), "/settings/users/~confirmed/")


class TestWizardUserSignUpView(TestCase):

    def setUp(self):
        super(TestWizardUserSignUpView, self).setUp()
        self.plan = mommy.make(
            'subscription.SubscriptionPlan',
            slug='test-subscription',
            recurrence_period=3, recurrence_unit='M',
            trial_period=1, trial_unit='M')
        self.province = mommy.make('province.Province', name='Province')
        self.city = mommy.make(
            'province.City', name='City', province=self.province)

        self.data_user = {
            'user_form-account_name': 'sterlingpos',
            'user_form-email': 'example@sterlingpos.com',
            'user_form-password1': 'ex4mpl3pas5w0rd',
            'wizard_user_sign_up_view-current_step': 'user_form'
        }

        self.data_outlet = {
            'outlet_form-name': 'toko sterlingpos',
            'outlet_form-phone': '021 - 1231910182',
            'outlet_form-address': 'Jl. Jalan 123',
            'outlet_form-province': self.province.pk,
            'outlet_form-city': self.city.pk,
            'wizard_user_sign_up_view-current_step': 'outlet_form'
        }

        self.url = self.reverse('account_signup')

    def test_register(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'user_form')

        response = self.client.post(self.url, data=self.data_user)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'outlet_form')

        response = self.client.post(self.url, data=self.data_outlet)
        self.assertEqual(response.status_code, 302)

        self.assertTrue(Account.objects.filter(name='sterlingpos').exists())
        account = Account.objects.get(name='sterlingpos')
        outlet = account.outlet_set.get(name='toko sterlingpos')
        self.assertEqual(
            outlet.subscription.subscription_plan.slug, 'basic-subscription')
        self.assertEqual(
            outlet.device_user.count(), 1)

    def test_register_with_choosen_plan_id_slug(self):
        url_with_plan = "{}?plan_id={}".format(self.url, self.plan.slug)
        response = self.client.get(url_with_plan)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'user_form')

        response = self.client.post(self.url, data=self.data_user)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'outlet_form')

        response = self.client.post(self.url, data=self.data_outlet)
        self.assertEqual(response.status_code, 302)

        self.assertTrue(Account.objects.filter(name='sterlingpos').exists())
        account = Account.objects.get(name='sterlingpos')
        outlet = account.outlet_set.get(name='toko sterlingpos')
        self.assertEqual(
            outlet.subscription.subscription_plan.pk, self.plan.pk)

    def test_register_with_choosen_plan_id_pk(self):
        url_with_plan = "{}?plan_id={}".format(self.url, self.plan.pk)
        response = self.client.get(url_with_plan)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'user_form')

        response = self.client.post(self.url, data=self.data_user)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'outlet_form')

        response = self.client.post(self.url, data=self.data_outlet)
        self.assertEqual(response.status_code, 302)

        self.assertTrue(Account.objects.filter(name='sterlingpos').exists())
        account = Account.objects.get(name='sterlingpos')
        outlet = account.outlet_set.get(name='toko sterlingpos')
        self.assertEqual(
            outlet.subscription.subscription_plan.pk, self.plan.pk)

    def test_register_with_bad_plan_id(self):
        url_with_plan = "{}?plan_id={}".format(self.url, 99)
        response = self.client.get(url_with_plan)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'user_form')

        response = self.client.post(self.url, data=self.data_user)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'outlet_form')

        response = self.client.post(self.url, data=self.data_outlet)
        self.assertEqual(response.status_code, 302)

        self.assertTrue(Account.objects.filter(name='sterlingpos').exists())
        account = Account.objects.get(name='sterlingpos')
        outlet = account.outlet_set.get(name='toko sterlingpos')
        self.assertEqual(
            outlet.subscription.subscription_plan.slug, 'basic-subscription')

    def test_register_with_bad_plan_id_2(self):
        url_with_plan = "{}?plan_id={}".format(self.url, None)
        response = self.client.get(url_with_plan)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'user_form')

        response = self.client.post(self.url, data=self.data_user)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'outlet_form')

        response = self.client.post(self.url, data=self.data_outlet)
        self.assertEqual(response.status_code, 302)

        self.assertTrue(Account.objects.filter(name='sterlingpos').exists())
        account = Account.objects.get(name='sterlingpos')
        outlet = account.outlet_set.get(name='toko sterlingpos')
        self.assertEqual(
            outlet.subscription.subscription_plan.slug, 'basic-subscription')

    def test_register_new_user_while_logged_in(self):
        account = mommy.make('accounts.Account', name='account-1')
        user = mommy.make(
            'users.User', email='test@email.com', account=account)
        user.set_password('test')
        user.save()

        url_with_plan = "{}".format(self.url)
        self.login(email=user.email, password='test')
        response = self.client.get(url_with_plan)
        self.assertEqual(response.status_code, 302)


class TestUserDeleteView(BaseUserTestCase):

    def setUp(self):
        # call BaseUserTestCase.setUp()
        super(TestUserDeleteView, self).setUp()
        # Instantiate the view directly. Never do this outside a test!
        self.view = UserDeleteView()
        # Generate a fake request
        request = self.factory.post("/fake-url")

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        # Apply MessageMiddleware
        message_middleware = MessageMiddleware()
        message_middleware.process_request(request)
        # Attach the user to the request
        request.user = self.user
        # Attach the request to the view
        self.view.request = request

    def test_post_ajax(self):
        self.view.request.POST = {'id': self.user_3.pk}
        response = self.view.post_ajax(self.view.request)
        self.assertFalse(self.account.user_set.members().filter(pk=self.user_3.pk).exists())

    def test_cannot_delete_owner(self):
        self.view.request.POST = {'id': self.user_2.pk}
        response = self.view.post_ajax(self.view.request)
        self.assertTrue(self.account.user_set.owners().filter(pk=self.user_2.pk).exists())


class TestInviteeRegistrationView(BaseUserTestCase):

    def setUp(self):
        self.account = Account.objects.create(
            name="Company A", address="Jl. Tebet Raya Timur", phone="021-182318")
        self.user = User.objects.create_user(
            email="testuser@example.com", password="example", account=self.account)
        self.device_user = mommy.make(
            'device.DeviceUser',
            username='administrator',
            name='Administrator Test',
            email='testuser@example.com',
            pin=12345,
            account=self.account,
        )
        self.factory = RequestFactory()

        self.invitation = mommy.make(
            UserInvitation,
            email='invited@mail.com',
            inviter=self.user,
            device_user=self.device_user,
            account=self.account)
        # Instantiate the view directly. Never do this outside a test!
        self.view = InviteeRegistration()
        # Generate a fake request
        data = {
            'email': self.invitation.email,
            'password1': 'test.password'
        }
        request = self.factory.post("/fake-url", data)
        request.session = {}
        # Attach the request to the view
        self.view.request = request

    def test_dispatch_no_invited_email_session_raise_404(self):
        with self.assertRaises(Http404):
            response = self.view.dispatch(self.view.request)

    def test_dispatch_with_invited_email_session(self):
        self.view.request.session.update({
            'account_verified_email': self.invitation.email
        })
        response = self.view.dispatch(self.view.request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('invitee-registration-success'))

    def test_get_invitation(self):
        self.view.request.session.update({
            'account_verified_email': self.invitation.email
        })
        self.assertEqual(self.view.get_invitation(), self.invitation)

    def test_get_initial(self):
        self.view.request.session.update({
            'account_verified_email': self.invitation.email
        })
        initial_data = self.view.get_initial()
        self.assertIn('email', initial_data)
        self.assertEqual(initial_data.get('email'), self.invitation.email)

    def test_form_valid_will_validate_invited_email(self):
        self.view.invitation = self.invitation
        form = self.view.get_form()
        self.assertTrue(form.is_valid())
        response = self.view.form_valid(form)

        new_user = User.objects.get(email=self.invitation.email)
        self.assertEqual(new_user.account, self.account)

        emails = new_user.emailaddress_set.values_list('verified', flat=True)
        self.assertTrue(all(emails))
