import datetime

from django.urls import reverse
from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser

from model_mommy import mommy
from test_plus.test import TestCase

from sterlingpos.users.middlewares import (
    SubscriptionUserMiddleware,
    IsAuthenticatedMiddleware,
)


class SubscriptionUserMiddlewareTests(TestCase):
    def setUp(self):
        self.request_factory = RequestFactory()
        self.request = self.request_factory.get("/fake-url")

        self.request.session = {}

    def test_requestProcessing_non_auth_user(self):
        self.request.user = AnonymousUser()
        response = SubscriptionUserMiddleware().process_request(self.request)

        self.assertFalse(self.request.session.get('subscription'))
        self.assertNotIn('is_trial', self.request.session)
        self.assertNotIn('left_days', self.request.session)

    def test_requestProcessing_auth_user(self):
        plan = mommy.make(
            'subscription.SubscriptionPlan',
            recurrence_period=3, recurrence_unit='M',
            trial_period=1, trial_unit='M')
        account = mommy.make('accounts.Account', name="Company A")
        user = mommy.make('users.User', account=account)
        outlet = mommy.make(
            'outlet.Outlet', branch_id='1', account=account)
        subscription = mommy.make(
            'subscription.Subscription',
            is_trial=True,
            subscription_plan=plan, outlet=outlet, account=account)
        subscription.extend()
        subscription.save()

        self.request.user = user

        response = SubscriptionUserMiddleware().process_request(self.request)

        self.assertTrue(self.request.session.get('subscription'))
        self.assertTrue(self.request.session.get('is_trial'))
        self.assertEqual(
            self.request.session.get('left_days'),
            (subscription.expires - datetime.date.today()).days)

    def test_requestProcessing_admin_or_staff_user(self):
        plan = mommy.make(
            'subscription.SubscriptionPlan',
            recurrence_period=3, recurrence_unit='M',
            trial_period=1, trial_unit='M')
        account = mommy.make('accounts.Account', name="Company A")
        user = mommy.make('users.User', account=account, is_staff=True)

        self.request.user = user

        response = SubscriptionUserMiddleware().process_request(self.request)

        self.assertFalse(self.request.session.get('subscription'))
        self.assertNotIn('is_trial', self.request.session)
        self.assertNotIn('left_days', self.request.session)


class TestIsAuthenticatedMiddleware(TestCase):

    def setUp(self):
        self.request_factory = RequestFactory()

    def test_is_non_auth_view_function(self):
        self.assertTrue(
            IsAuthenticatedMiddleware().is_non_auth_view("account_login"))
        self.assertTrue(
            IsAuthenticatedMiddleware().is_non_auth_view("account_signup"))
        self.assertTrue(
            IsAuthenticatedMiddleware().is_non_auth_view("v2:outlet_list"))

        self.assertFalse(
            IsAuthenticatedMiddleware().is_non_auth_view("dashboard"))
        self.assertFalse(
            IsAuthenticatedMiddleware().is_non_auth_view("users:update"))

    def test_request_with_non_auth_user_on_auth_only_view_will_block_request(self):
        request = self.request_factory.get(reverse("dashboard"))
        request.session = {}
        request.user = AnonymousUser()
        response = IsAuthenticatedMiddleware().process_request(request)
        self.assertTrue(response.status_code, 401)

    def test_request_with_auth_user_on_auth_only_view_will_not_block_request(self):
        request = self.request_factory.get(reverse("dashboard"))
        request.session = {}
        account = mommy.make('accounts.Account', name='Account')
        user = mommy.make('users.User', email='user@mail.com', account=account)
        request.user = user
        response = IsAuthenticatedMiddleware().process_request(request)
        self.assertEqual(response, None)

    def test_request_with_non_auth_user_on_non_auth_view_will_not_block_request(self):
        request = self.request_factory.get(reverse("account_login"))
        request.session = {}
        request.user = AnonymousUser()
        response = IsAuthenticatedMiddleware().process_request(request)
        self.assertEqual(response, None)

    def test_request_with_bad_url_will_not_block_request(self):
        request = self.request_factory.get("/fake-url/")
        request.session = {}
        request.user = AnonymousUser()
        response = IsAuthenticatedMiddleware().process_request(request)
        self.assertEqual(response, None)
