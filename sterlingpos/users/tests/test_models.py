from test_plus.test import TestCase
from django.db import IntegrityError
from django.db import transaction

from model_mommy import mommy
from sterlingpos.users.models import User
from sterlingpos.accounts.models import Account
from sterlingpos.core.models import set_current_tenant, get_current_tenant


class TestUser(TestCase):

    def setUp(self):
        self.account = mommy.make(Account)
        set_current_tenant(self.account)

    def tearDown(self):
        set_current_tenant(None)

    def test_create_user(self):
        user = User.objects.create_user(
            email="testuser@example.com",
            password="example",
            account=self.account)

        self.assertEqual(
            user.__str__(),
            "{} - {}".format(user.email, self.account.name)
        )

    def test_no_email_create_user(self):
        """
            Test Create User without Email,
            will raise Value Error.
        """
        with self.assertRaises(ValueError):
                self.new_user = User.objects.create_user(
                    email=None,
                    password="example",
                    account=None
                )

    def test_create_user_using_exist_email(self):
        """
           Test Create or Add user using exist email will
           raise Integrity Error.
        """
        user = User.objects.create_user(
            email="testuser@example.com",
            password="example",
            account=self.account)
        self.new_account = mommy.make(Account)
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                self.same_user = User.objects.create_user(
                    email="testuser@example.com",
                    password="example",
                    account=self.new_account
                )

    def test_create_user_in_same_account(self):
        """
            Test Create or Add new User with existing Account.
        """

        self.new_user = User.objects.create_user(
            email="testuser_1@example.com",
            password="example",
            account=self.account
        )

        self.assertEqual(
            self.new_user.__str__(),
            "{} - {}".format(self.new_user.email, self.account.name)
        )

    def test_create_superuser(self):
        self.superuser = User.objects.create_superuser(
            email="example@example.com",
            password="example",
            account=self.account
        )

        self.assertEqual(
            self.superuser.__str__(),
            "{} - {}".format(self.superuser.email, self.account.name)
        )

    def test_create_superuser_is_staff_false(self):
        """
            Test Create Superuser with field is_staff = False,
            will raise Value Error.
        """

        with self.assertRaises(ValueError):
            self.superuser = User.objects.create_superuser(
                email="example@example.com",
                password="example",
                account=self.account,
                is_staff=False
            )

    def test_create_superuser_is_superuser_false(self):
        """
            Test Create Superuser with field is_superuser = False,
            will raise Value Error.
        """

        with self.assertRaises(ValueError):
            self.superuser = User.objects.create_superuser(
                email="example@example.com",
                password="example",
                account=self.account,
                is_superuser=False
            )

    def test_user_is_owner(self):
        user = User.objects.create_user(
            email="testuser@example.com",
            password="example",
            account=self.account)
        self.account.owners.create(
            account=self.account,
            user=user)
        self.assertTrue(user.is_owner)

    def test_user_manager_members(self):
        user = User.objects.create_user(
            email="testuser@example.com",
            password="example",
            account=self.account)
        user_2 = User.objects.create_user(
            email="testuser-2@example.com",
            password="example",
            account=self.account)

        self.account.owners.create(
            account=self.account,
            user=user)

        qs = User.objects.members()
        self.assertEqual(qs.count(), 1)
        self.assertEqual(qs.first(), user_2)

    def test_user_manager_owners(self):
        user = User.objects.create_user(
            email="testuser@example.com",
            password="example",
            account=self.account)
        user_2 = User.objects.create_user(
            email="testuser-2@example.com",
            password="example",
            account=self.account)
        self.account.owners.create(
            account=self.account,
            user=user)

        qs = User.objects.owners()
        self.assertEqual(qs.count(), 1)
        self.assertEqual(qs.first(), user)
