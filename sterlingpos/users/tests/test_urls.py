from django.urls import reverse, resolve

from test_plus.test import TestCase
from sterlingpos.users.models import User
from sterlingpos.accounts.models import Account


class TestUserURLs(TestCase):
    """Test URL patterns for users app."""

    def setUp(self):
        self.account = Account.objects.create(name="Company A", address="Jl. Tebet Raya Timur", phone="021-182318")
        self.user = User.objects.create_user(email="testuser@example.com", password="example", account=self.account)

    # def test_list_reverse(self):
    #     """users:list should reverse to /users/."""
    #     self.assertEqual(reverse("users:list"), "/settings/users/")

    # def test_list_resolve(self):
    #     """/users/ should resolve to users:list."""
    #     self.assertEqual(resolve("/settings/users/").view_name, "users:list")

    def test_redirect_reverse(self):
        """users:redirect should reverse to /users/~redirect/."""
        self.assertEqual(reverse("users:redirect"), "/settings/users/~redirect/")

    def test_redirect_resolve(self):
        """/users/~redirect/ should resolve to users:redirect."""
        self.assertEqual(resolve("/settings/users/~redirect/").view_name, "users:redirect")

    # def test_detail_reverse(self):
    #     """users:detail should reverse to /users/testuser/."""
    #     self.assertEqual(
    #         reverse("users:detail"), "/settings/users/detail/"
    #     )

    # def test_detail_resolve(self):
    #     """/users/testuser/ should resolve to users:detail."""
    #     self.assertEqual(resolve("/settings/users/detail/").view_name, "users:detail")

    def test_update_reverse(self):
        """users:update should reverse to /users/~update/."""
        self.assertEqual(reverse("users:update"), "/settings/users/update/")

    def test_update_resolve(self):
        """/users/~update/ should resolve to users:update."""
        self.assertEqual(resolve("/settings/users/update/").view_name, "users:update")
