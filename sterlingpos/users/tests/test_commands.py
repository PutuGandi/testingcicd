import os
import csv

from io import StringIO
from django.core import mail
from django.test import TestCase
from django.core.management import call_command


class BlastEmailTest(TestCase):

    def setUp(self):
        self.csv_data = [{'email': 'test-{}@mail.com'.format(x)} for x in range(0, 5)]

        with open('blastemailtest.csv', mode='w') as csv_file:
            fieldnames = ['email', ]
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(self.csv_data)

    def tearDown(self):
        os.remove("blastemailtest.csv")

    def test_command_output(self):
        out = StringIO()
        call_command(
            'blast_email',
            csv_file='blastemailtest.csv',
            template_name="invitations/email/email_invite",
            stdout=out)
        self.assertEqual(len(mail.outbox), 5)
