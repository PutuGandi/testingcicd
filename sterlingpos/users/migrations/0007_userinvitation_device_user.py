# Generated by Django 2.0.4 on 2019-09-19 07:38

from django.db import migrations
import django.db.models.deletion
import sterlingpos.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('device', '0011_deviceuser_role'),
        ('users', '0006_auto_20181011_0753'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinvitation',
            name='device_user',
            field=sterlingpos.core.models.SterlingTenantOneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='invitation', to='device.DeviceUser'),
        ),
    ]
