# Generated by Django 2.0.4 on 2018-05-25 03:23

from django.db import migrations
import django.db.models.deletion
import sterlingpos.core.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='account',
            field=sterlingpos.core.models.SterlingTenantForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='accounts.Account'),
        ),
    ]
