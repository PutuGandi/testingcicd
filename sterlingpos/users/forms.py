from django import forms
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from allauth.account.forms import SignupForm as BaseSignupForm, ChangePasswordForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Submit
from invitations.forms import InviteForm
from invitations.models import Invitation as BaseInvitation
from invitations.adapters import get_invitations_adapter
from invitations.exceptions import AlreadyAccepted, UserRegisteredEmail
from invitations.utils import get_invitation_model


from .models import User
from sterlingpos.accounts.models import Account

Invitation = get_invitation_model()


class BaseUserSignupForm(BaseSignupForm):

    def __init__(self, *args, **kwargs):
        super(BaseUserSignupForm, self).__init__(*args, **kwargs)
        self.order_fields(sorted(self.fields.keys()))
        self.fields['email'] = forms.EmailField(
            label='Alamat Email',
            widget=forms.TextInput(attrs={'placeholder': 'ex: example@kawn.co.id'})
        )
        self.fields['password1'] = forms.CharField(
            label="Kata Sandi",
            widget=forms.PasswordInput(attrs={
                'data-eye': '',
                'placeholder': 'Password'
            })
        )


class UserSignupForm(BaseUserSignupForm):
    account_name = forms.CharField(label='Nama Akun', widget=forms.TextInput(attrs={'placeholder': 'ex: kawn pos'}))

    def __init__(self, *args, **kwargs):
        super(UserSignupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

    def create_account(self, account_name):
        account = Account(name=account_name)
        account.save()
        return account

    def save(self, request):
        account_name = self.cleaned_data['account_name']
        account = self.create_account(account_name)
        self.data['account'] = account

        user = super(UserSignupForm, self).save(request)

        return user


class InviteeUserSignupForm(BaseUserSignupForm):

    def save(self, request, account):
        self.data = self.data.copy()
        self.data['account'] = account
        user = super(InviteeUserSignupForm, self).save(request)
        user.save()
        return user


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        Div(
                            HTML('<h5>{}</h5>'.format(_('User Information'))),
                            css_class='col-lg-12 col-md-12 col-sm-12'
                        ),
                        css_class='row'
                    ),
                    Div(
                        Div(
                            'name',
                            css_class='col-lg-12 col-md-12 col-sm-12'
                        ),
                        css_class='row'
                    ),
                    Div(
                        Div(
                            Div(
                                Div(
                                    Div(
                                        HTML('<a href="{}" class="btn btn-block btn-primary">{}</a>'.format(reverse('account_email'), _('Email Management'))),
                                        css_class='col-lg-6 col-md-6 col-sm-6'
                                    ),
                                    Div(
                                        HTML('<a href="{}" class="btn btn-block btn-primary">{}</a>'.format(reverse('users:password_change'), _('Change Password'))),
                                        css_class='col-lg-6 col-md-6 col-sm-6'
                                    ),
                                    css_class='row'
                                ),
                            ),
                            css_class='col-lg-12 col-md-12 col-sm-12'
                        ),
                        css_class='row'
                    ),
                    css_class='card-body'
                ),
                css_class='card'
            )
        )

        self.helper.layout.append(
            Div(
                Submit('user_update', _('Update User Info'), css_class='btn btn-light'),
                css_class='d-flex justify-content-end'
            )
        )


class FormChangePassword(ChangePasswordForm):

    def __init__(self, * args, **kwargs):
        super(FormChangePassword, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['oldpassword'].widget.attrs['placeholder'] = ''
        self.fields['password1'].widget.attrs['placeholder'] = ''
        self.fields['password2'].widget.attrs['placeholder'] = ''
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        Div(
                            'oldpassword',
                            css_class='col-lg-12 col-md-12 col-sm-12'
                        ),
                        css_class='row'
                    ),
                    Div(
                        Div(
                            'password1',
                            css_class='col-lg-12 col-md-12 col-sm-12'
                        ),
                        css_class='row'
                    ),
                    Div(
                        Div(
                            'password2',
                            css_class='col-lg-12 col-md-12 col-sm-12'
                        ),
                        css_class='row'
                    ),
                    css_class='card-body'
                ),
                css_class='card'
            ),
        )

        self.helper.layout.append(
            Div(
                HTML('<a href="{}"  class="btn btn-secondary">{}</a>'.format(reverse('users:update'), _('Back'))),
                Submit('save', _('Change Password'), css_class='btn btn-primary ml-1'),
                css_class='justify-content-end col-6 p-0 flex',
                css_id='fixed-button-content',
            )
        )


class UserInvitationForm(InviteForm):

    def validate_invitation(self, email):
        if Invitation.objects.filter(
                email__iexact=email, accepted=True):
            raise AlreadyAccepted
        elif BaseInvitation.objects.filter(email__iexact=email):
            raise forms.ValidationError(
                _("This e-mail address has already been registered on another Account."))
        elif User.objects.filter(email__iexact=email):
            raise UserRegisteredEmail
        else:
            return True

    def clean_email(self):
        email = self.cleaned_data["email"]
        email = get_invitations_adapter().clean_email(email)

        errors = {

            "already_accepted": _("This e-mail address has already"
                                  " accepted an invite."),
            "email_in_use": _("An active user is using this e-mail address"),
        }
        try:
            self.validate_invitation(email)
        except(AlreadyAccepted):
            raise forms.ValidationError(errors["already_accepted"])
        except(UserRegisteredEmail):
            raise forms.ValidationError(errors["email_in_use"])
        return email
