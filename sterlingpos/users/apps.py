from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = "sterlingpos.users"
    verbose_name = "Users"

    def ready(self):
        from invitations.adapters import get_invitations_adapter
        from sterlingpos.users.signals import sterling_accept_invite_after_signup

        signed_up_signal = get_invitations_adapter().get_user_signed_up_signal()
        signed_up_signal.connect(
            sterling_accept_invite_after_signup)
