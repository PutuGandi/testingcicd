from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _

from invitations.models import Invitation
from invitations.managers import BaseInvitationManager

from safedelete.models import HARD_DELETE

from django_multitenant.models import TenantManager

from sterlingpos.core.models import SterlingTenantOneToOneField, SterlingTenantModel
from sterlingpos.accounts.models import Account


class UserManager(BaseUserManager, TenantManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

    def members(self):
        return self.filter(owner_of__isnull=True)

    def owners(self):
        return self.exclude(owner_of__isnull=True)


class User(AbstractUser, SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    # First Name and Last Name do not cover name patterns
    # around the globe.
    username = None
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(_("Name of User"), blank=True, max_length=255)

    tenant_id = 'account_id'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return "{} - {}".format(self.email, self.account.name)

    @property
    def is_owner(self):
        return hasattr(self, 'owner_of') and self.owner_of is not None

    @property
    def get_name(self):
        return self.email.split('@')[0]


class UserInvitationManager(BaseInvitationManager, TenantManager):
    pass


class UserInvitation(Invitation, SterlingTenantModel):

    _safedelete_policy = HARD_DELETE

    objects = UserInvitationManager()
    device_user = SterlingTenantOneToOneField(
        "device.DeviceUser", null=True, on_delete=models.CASCADE,
        related_name='invitation',
    )
