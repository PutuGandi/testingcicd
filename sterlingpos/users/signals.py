from sterlingpos.users.models import UserInvitation
from invitations.views import accept_invitation


def sterling_accept_invite_after_signup(sender, request, user, **kwargs):
    invitation = UserInvitation.objects.filter(
        email__iexact=user.email).first()
    if invitation:
        accept_invitation(invitation=invitation,
                          request=request,
                          signal_sender=UserInvitation)
