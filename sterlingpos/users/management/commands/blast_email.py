import csv
import argparse
from django.core.management.base import BaseCommand, CommandError

from sterlingpos.users.adapters import AccountAdapter


class Command(BaseCommand):
    help = 'Blast email to recepients based on choosen template'

    def add_arguments(self, parser):
        parser.add_argument('--csv-file',
                            help='csv file that contains data of recipients and email context '
                                 'to be sent')
        parser.add_argument('--template-name', type=str,
                            help='name of email template to render '
                                 'e.g. "account/email/email_confirmation')
        parser.add_argument('--recipients-field', type=str, required=False,
                            default='email',
                            help='name of email field on csv_file to be '
                                 'used as recipient')

    def handle(self, *args, **options):
        adapter = AccountAdapter()

        with open(options['csv_file'], 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for i, mail in enumerate(csv_reader):
                adapter.send_mail(
                    options.get('template_name'),
                    mail.get(options.get('recipients_field')),
                    mail,
                )

            self.stdout.write(
                self.style.SUCCESS(
                    "Successfully blast email to '%i' recipients" % (i + 1))
            )
