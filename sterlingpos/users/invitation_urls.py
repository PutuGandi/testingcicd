from django.conf.urls import url

from . import views

app_name = "invitation"
urlpatterns = [
    url(regex=r'^$', view=views.InvitationViewDashboard.as_view(),
        name='invitation_list_dashboard'),
    url(regex=r'^deactivate/$', view=views.InvitationDeleteView.as_view(),
        name='invitation_deactivate_dashboard'),
    url(regex=r'^resend/$', view=views.InvitationResendView.as_view(),
        name='invitation_resend_dashboard'),
    url(regex=r'^data/$', view=views.InvitationListJson.as_view(),
        name='data_invitation'),

    url(regex=r'^delete/$', view=views.UserDeleteView.as_view(),
        name='invitation_delete_dashboard'),

    url(regex=r'^users/$', view=views.UserListViewDashboard.as_view(),
        name='user_list_dashboard'),
    url(regex=r'^data/users/$', view=views.UserListJson.as_view(),
        name='data_user'),
]
