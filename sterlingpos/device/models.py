from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.template.defaultfilters import slugify

from model_utils import Choices
from model_utils.models import TimeStampedModel
from sterlingpos.core.models import (
    SterlingTenantModel, SterlingTenantForeignKey,
    SterlingTenantOneToOneField,
)
from .storage import OverwriteStorage


def get_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    new_filename = "{}_{}.{}".format(instance.username, slugify(instance.name), ext)
    return '{}/device_user/{}'.format(instance.account.pk, new_filename)


@python_2_unicode_compatible
class DeviceUserPermission(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_("Pemission Name"), max_length=255)
    tenant_id = 'account_id'

    class Meta:
        verbose_name = "DeviceUserPermission"
        verbose_name_plural = "DeviceUserPermissions"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "Device Permission : {}".format(self.name)


@python_2_unicode_compatible
class DeviceUser(SterlingTenantModel, TimeStampedModel):

    PERMISSION = Choices(
        ('manager', _('Manager')),
        ('staff', _('Staff')),
        ('cashier', _('Cashier')),
        ('waiter', _('Waiter')),
    )

    username = models.CharField(_("Username"), max_length=255)
    name = models.CharField(_("Name"), max_length=255)
    email = models.CharField(
        _("Email"), blank=True, max_length=255)
    pin = models.CharField(_("Pin"), max_length=255)
    archived = models.BooleanField(default=False)
    device_user_image = models.ImageField(upload_to=get_upload_path, storage=OverwriteStorage(), blank=True, null=True, max_length=255)

    permission = models.CharField(
        _("Device User Permission"), max_length=15, choices=PERMISSION, default=PERMISSION.staff)
    
    all_outlet_access = models.BooleanField(default=False)

    user = SterlingTenantOneToOneField(
        "users.User", null=True, on_delete=models.SET_NULL,
        related_name='device_user',
    )

    role = SterlingTenantForeignKey(
        "role.Role", null=True, on_delete=models.SET_NULL,
    )

    class Meta:
        verbose_name = "DeviceUser"
        verbose_name_plural = "DeviceUsers"
        unique_together = (('account', 'id'),)

    def __str__(self):
        return "{} | {}".format(self.username, self.name)
