from django.apps import AppConfig


class DeviceConfig(AppConfig):
    name = 'sterlingpos.device'
    verbose_name = "Devices"
