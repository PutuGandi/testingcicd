from django.contrib import admin

from sterlingpos.outlet.models import Outlet
from .models import (
    DeviceUser,
)


class OutletInline(admin.TabularInline):
    model = Outlet.device_user.through


class DeviceUserAdmin(admin.ModelAdmin):
    """
        Admin View for DeviceUser
    """
    list_display = (
        "username", "name", "email",
        "archived", "permission", "account")
    list_filter = ("permission", )

    search_fields = (
        "username", "name", "email",
        "archived", "permission", "account__name")

    inlines = (
        OutletInline,
    )

admin.site.register(DeviceUser, DeviceUserAdmin)
