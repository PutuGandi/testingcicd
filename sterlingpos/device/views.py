import csv
from datetime import datetime, timedelta

from django.conf import settings
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.html import escape
from django.contrib import messages
from django.views.generic import (
    ListView, CreateView, UpdateView, DeleteView, TemplateView, View
)

from django_datatables_view.base_datatable_view import BaseDatatableView
from braces.views import JSONResponseMixin, AjaxResponseMixin
from sterlingpos.role.mixins import SterlingRoleMixin
from sterlingpos.role.utils import get_outlet_of_user
from sterlingpos.core.mixins import OwnerOnlyMixin, DatatableMixins

from sterlingpos.users.models import UserInvitation

from .models import DeviceUser
from .forms import (
    DeviceUserForm,
    DeviceUserManagerForm,
    DeviceUserStaffForm,
)


class DeviceUserStaffListJson(SterlingRoleMixin, DatatableMixins, BaseDatatableView):
    columns = ['username', 'name', 'archived', 'role.name', 'action']
    order_columns = ['username', 'name', 'archived', 'role__pk', 'action']
    model = DeviceUser

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()

        qs = qs.exclude(
            outlet__subscription__subscription_plan__slug='mobile-lite')
        qs = qs.exclude(permission__in=[
            DeviceUser.PERMISSION.manager,
            DeviceUser.PERMISSION.waiter
        ])

        if not self.request.user.is_owner:
            qs = qs.filter(
                outlet__in=get_outlet_of_user(
                    self.request.user)).distinct()

        return qs

    def filter_archived(self, value):
        if value != "show":
            return Q(archived__istartswith=value)
        return Q()

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'username': escape(row.username),
                'edit': reverse_lazy("device:device_user_staff_update", kwargs={'pk': row.pk}),
                'archived': row.archived
            }
            return action_data
        elif column == 'archived':
            if row.archived:
                return '{}'.format(_('Archived'))
            else:
                return '{}'.format(_('Active'))
        elif column == 'username':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "device:device_user_staff_update",
                    kwargs={'pk': row.pk}),
                escape(row.username))
        else:
            return super(DeviceUserStaffListJson, self).render_column(row, column)


class DeviceUserManagerListJson(OwnerOnlyMixin, DeviceUserStaffListJson):
    columns = ['username', 'name', 'email', 'archived', 'action']
    order_columns = ['username', 'name', 'email', 'archived', 'action']

    def get_initial_queryset(self):
        qs = self.model.objects.exclude(
            outlet__subscription__subscription_plan__slug='mobile-lite',
        ).filter(
            permission=DeviceUser.PERMISSION.manager)
        return qs

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'username': escape(row.username),
                'edit': reverse_lazy("device:device_user_manager_update", kwargs={'pk': row.pk}),
                'archived': row.archived
            }
            return action_data
        elif column == 'archived':
            if row.archived:
                return '{}'.format(_('Archived'))
            else:
                return '{}'.format(_('Active'))
        elif column == 'username':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "device:device_user_manager_update",
                    kwargs={'pk': row.pk}),
                escape(row.username))
        else:
            return super(DeviceUserManagerListJson, self).render_column(row, column)


class DeviceUserManagerListView(OwnerOnlyMixin, SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/device/device_user_manager_list.html'


class DeviceUserStaffListView(SterlingRoleMixin, TemplateView):
    template_name = 'dashboard/device/device_user_staff_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['role_set'] = self.request.user.account.role_set.filter(
            is_supervisor=False,
        )
        return context


class DeviceUserManagerCreateView(OwnerOnlyMixin, SterlingRoleMixin, CreateView):
    template_name = 'dashboard/device/device_user_manager_create.html'
    model = DeviceUser
    success_url = reverse_lazy("device:device_user_manager_list")
    form_class = DeviceUserManagerForm

    def form_valid(self, form):
        res = super().form_valid(form)
        if not self.object.user:
            try:
                invitation = UserInvitation.objects.get(
                    email=self.object.email)
                invitation.accepted = False
                invitation.save()
                invitation.send_invitation(self.request)
            except UserInvitation.DoesNotExist:
                invite = UserInvitation.create(
                    email=self.object.email,
                    inviter=self.request.user,
                    device_user=self.object,
                )
                invite.save()
                invite.send_invitation(self.request)
        messages.success(self.request, '{}'.format(
            _('Success add new manager.')))
        return res


class DeviceUserManagerUpdateView(OwnerOnlyMixin, SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/device/device_user_update.html'
    success_url = reverse_lazy("device:device_user_manager_list")
    model = DeviceUser
    form_class = DeviceUserManagerForm

    def form_valid(self, form):
        res = super().form_valid(form)
        if not self.object.user:
            try:
                invitation = UserInvitation.objects.get(
                    email=self.object.email)
                invitation.accepted = False
                invitation.save()
                invitation.send_invitation(self.request)
            except UserInvitation.DoesNotExist:
                invite = UserInvitation.create(
                    email=self.object.email,
                    inviter=self.request.user,
                    device_user=self.object,
                )
                invite.save()
                invite.send_invitation(self.request)
        messages.success(self.request, '{}'.format(
            _('Success update manager.')))
        return res


class DeviceUserManagerArchiveView(OwnerOnlyMixin, SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            device_user_id = request.POST.get('id', '')
            device_user = DeviceUser.objects.get(pk=device_user_id, permission=DeviceUser.PERMISSION.manager)
            device_user.archived = True
            device_user.save()
            messages.success(self.request, '{}'.format(
                _('Manager has been archived.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class DeviceUserManagerActivateView(OwnerOnlyMixin, SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            device_user_id = request.POST.get('id', '')
            device_user = DeviceUser.objects.get(pk=device_user_id, permission=DeviceUser.PERMISSION.manager)
            device_user.archived = False
            device_user.save()
            messages.success(self.request, '{}'.format(
                _('Manager has been re-activated.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class DeviceUserManagerDeleteView(OwnerOnlyMixin, SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            device_user_id = request.POST.get('id', '')
            device_user = DeviceUser.objects.get(pk=device_user_id, permission=DeviceUser.PERMISSION.manager)
            device_user.delete()

            if hasattr(device_user, "invitation"):
                device_user.invitation.delete()

            messages.error(self.request, '{}'.format(
                _('Manager has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class DeviceUserStaffCreateView(SterlingRoleMixin, CreateView):
    template_name = 'dashboard/device/device_user_staff_create.html'
    model = DeviceUser
    success_url = reverse_lazy("device:device_user_staff_list")
    form_class = DeviceUserStaffForm

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def form_valid(self, form):
        response = super(DeviceUserStaffCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Success add new staff.')))
        return response


class DeviceUserStaffUpdateView(SterlingRoleMixin, UpdateView):
    template_name = 'dashboard/device/device_user_update.html'
    success_url = reverse_lazy("device:device_user_staff_list")
    model = DeviceUser
    form_class = DeviceUserStaffForm

    def get_form(self, **kwargs):
        form = super().get_form(**kwargs)
        if not self.request.user.is_owner:
            form.fields['outlet'].queryset = get_outlet_of_user(
                self.request.user)
        return form

    def form_valid(self, form):
        response = super(DeviceUserStaffUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Success update staff.')))
        return response


class DeviceUserStaffArchiveView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            device_user_id = request.POST.get('id', '')
            device_user = DeviceUser.objects.get(pk=device_user_id)
            device_user.archived = True
            device_user.save()
            messages.success(self.request, '{}'.format(
                _('Staff has been archived.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class DeviceUserStaffActivateView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            device_user_id = request.POST.get('id', '')
            device_user = DeviceUser.objects.get(pk=device_user_id)
            device_user.archived = False
            device_user.save()
            messages.success(self.request, '{}'.format(
                _('Staff has been re-activated.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)


class DeviceUserStaffDeleteView(SterlingRoleMixin, JSONResponseMixin, AjaxResponseMixin, View):

    def post_ajax(self, request, *args, **kwargs):
        response = {
            'status': 'fail',
            'data': {}
        }
        status_code = 404
        try:
            status_code = 200
            response['status'] = 'success'
            device_user_id = request.POST.get('id', '')
            device_user = DeviceUser.objects.get(pk=device_user_id)
            device_user.delete()
            messages.error(self.request, '{}'.format(
                _('Staff has been deleted.')))
        except ValueError:
            pass

        return self.render_json_response(response, status=status_code)
