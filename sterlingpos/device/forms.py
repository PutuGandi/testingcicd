from django import forms
from django.contrib.admin import widgets
from django.utils.translation import ugettext_lazy as _, ugettext

from dal import autocomplete

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, HTML, Fieldset
from crispy_forms.bootstrap import PrependedText

from invitations.models import Invitation as BaseInvitation
from invitations.adapters import get_invitations_adapter
from invitations.exceptions import AlreadyAccepted, UserRegisteredEmail
from invitations.utils import get_invitation_model

from sterlingpos.users.models import User
from sterlingpos.device.models import DeviceUser
from sterlingpos.outlet.models import Outlet, OutletDeviceUser
from sterlingpos.core.bootstrap import KawnField, KawnFieldSetWithHelpText

from sterlingpos.role.models import Role

MAX_UPLOAD_SIZE = 4194304 # 4MB

Invitation = get_invitation_model()


class DeviceUserForm(forms.ModelForm):

    all_outlet = forms.BooleanField(initial=True, label="Semua Lokasi")
    outlet = forms.ModelMultipleChoiceField(
        required=False, queryset=None, label='', widget=forms.CheckboxSelectMultiple)

    class Meta:
        model = DeviceUser
        exclude = ('account', 'archived', 'user')

    def update_m2m(self, device_user, object_list, rel_field, intermediate_model, intermediate_model_ref):
        obj_ids = set(
            object_list.values_list("id", flat=True))
        current_ids = set(
            rel_field.filter(
                pk__in=self.fields[rel_field.target_field.name].queryset
            ).values_list("id", flat=True)
        )

        add_ids = obj_ids - current_ids
        delete_ids = current_ids - obj_ids
        model_objects = [
            intermediate_model(
                **{
                    "{}_id".format(rel_field.target_field_name): obj_id,
                    "{}".format(intermediate_model_ref.field.name): device_user,
                    "account": device_user.account
                }
            )
            for obj_id in add_ids
        ]
        intermediate_model.objects.bulk_create(model_objects)
        intermediate_model.objects.complex_filter({
            "{}__pk__in".format(rel_field.target_field_name): delete_ids,
            "{}".format(intermediate_model_ref.field.name): device_user,
        }).delete()

        if delete_ids:
            rel_field.model.objects.filter(
                pk__in=delete_ids).invalidated_update()

        rel_field.invalidated_update()

    def clean_device_user_image(self):
        image = self.cleaned_data['device_user_image']
        if image:
            if image.size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError("Image file too large.")
        return image

    def clean_pin(self):
        pin = self.cleaned_data['pin']
        if pin:
            if len(pin) > 6:
                raise forms.ValidationError(_("Pin must be 6 characters."))
            elif len(pin) < 6:
                raise forms.ValidationError(_("Pin must be 6 characters."))

        if not pin.isdigit():
            raise forms.ValidationError(_("Pin must be Numeric."))

        return pin


class DeviceUserManagerForm(DeviceUserForm):
    email = forms.EmailField()

    class Meta(DeviceUserForm.Meta):
        exclude = DeviceUserForm.Meta.exclude + ('permission',)

    def __init__(self, *args, **kwargs):
        super(DeviceUserManagerForm, self).__init__(*args, **kwargs)
        self.fields['pin'].widget.attrs['maxlength'] = 6
        self.fields['outlet'].queryset = Outlet.objects.exclude(
            subscription__subscription_plan__slug='mobile-lite'
        )
        self.fields['all_outlet'].required = False
        self.initial['all_outlet'] = False
        self.fields['email'].required = True

        self.fields['device_user_image'].label = _('Add Image')
        self.fields['device_user_image'].help_text = _("Drag an image here or browse for an image to upload (Maximum size: 4 MB).")
        self.fields['all_outlet'].help_text = _(
            '( Jika centang ini dihapus, Karyawan ini tidak berlaku pada gerai yang baru. )'
        )

        self.fields['role'].queryset = Role.objects.filter(
            is_supervisor=True)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "px-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _("Staff Information"),
                KawnField('name'),
                KawnField('username'),
                KawnField('email'),
                KawnField('pin', css_class='numeric'),
                KawnField('role', css_class='custom-select'),
                KawnField('device_user_image', template='dashboard/image_upload/images_field_inline_with_text.html'),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet'),
                KawnField('outlet'),
            ),
        )

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            current_outlet = kwargs['instance'].outlet.all()
            self.initial['outlet'] = current_outlet
            total_outlet = self.fields['outlet'].queryset.count()
            if total_outlet > len(current_outlet):
                self.initial['all_outlet'] = False
            else:
                self.initial['all_outlet'] = True

            if getattr(self.instance, "invitation", None) or getattr(self.instance, "user", None):
                self.fields['email'].disabled = True
                if getattr(self.instance, "user", None):
                    self.initial['email'] = self.instance.user.email
                else:
                    self.initial['email'] = self.instance.invitation.email

    def validate_invitation(self, email):
        if Invitation.objects.filter(
            email__iexact=email).exists() and not User.objects.filter(
                    email__iexact=email).exists():
            return True
        elif Invitation.objects.filter(
            email__iexact=email).exists() and User.objects.filter(
                    email__iexact=email).exists():
            raise AlreadyAccepted
        elif BaseInvitation.objects.filter(email__iexact=email).exists():
            raise forms.ValidationError(
                _("This e-mail address has already been registered on another Account."))
        else:
            return True

    def clean_email(self):
        email = self.cleaned_data["email"]
        email = get_invitations_adapter().clean_email(email)

        if email and not getattr(self.instance, "user", None):
            errors = {

                "already_accepted": _("This e-mail address has already"
                                    " accepted an invite."),
                "email_in_use": _("An active user is using this e-mail address"),
            }

            try:
                self.validate_invitation(email)
            except(AlreadyAccepted):
                raise forms.ValidationError(errors["already_accepted"])
            except(UserRegisteredEmail):
                raise forms.ValidationError(errors["email_in_use"])

        return email

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        all_outlet = self.cleaned_data.pop('all_outlet')
        obj = super(DeviceUserManagerForm, self).save(*args, **kwargs)
        obj.all_outlet_access = all_outlet

        obj.permission = DeviceUser.PERMISSION.manager
        obj.save()

        self.update_m2m(
            obj, outlets, obj.outlet,
            OutletDeviceUser, OutletDeviceUser.device_user)

        return obj


class DeviceUserStaffForm(DeviceUserForm):

    class Meta(DeviceUserForm.Meta):
        exclude = DeviceUserForm.Meta.exclude + ('permission', 'email')

    def __init__(self, *args, **kwargs):
        super(DeviceUserStaffForm, self).__init__(*args, **kwargs)
        self.fields['pin'].widget.attrs['maxlength'] = 6
        self.fields['role'].queryset = Role.objects.filter(
            is_supervisor=False)

        self.fields['outlet'].queryset = Outlet.objects.exclude(
            subscription__subscription_plan__slug='mobile-lite'
        )
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.include_media = False
        self.fields['all_outlet'].required = False
        self.initial['all_outlet'] = False
        self.fields['all_outlet'].help_text = _(
            '( Jika centang ini dihapus, Karyawan ini tidak berlaku pada gerai yang baru. )'
        )
        self.fields['device_user_image'].label = _('Add Image')
        self.fields['device_user_image'].help_text = _("Drag an image here or browse for an image to upload (Maximum size: 4 MB).")
        self.helper.form_class = "col-lg-12 px-0 m-0"
        self.helper.field_class = "px-0 mb-3"
        self.helper.layout = Layout(
            KawnFieldSetWithHelpText(
                _('Staff Information'),
                KawnField('name'),
                KawnField('username'),
                KawnField('email'),
                KawnField('pin', css_class='numeric'),
                KawnField('role', css_class='custom-select'),
                KawnField('device_user_image', template='dashboard/image_upload/images_field_inline_with_text.html'),
            ),
            KawnFieldSetWithHelpText(
                "",
                KawnField('all_outlet'),
                KawnField('outlet'),
            ),
        )

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            current_outlet = kwargs['instance'].outlet.all()
            self.initial['outlet'] = current_outlet
            total_outlet = self.fields['outlet'].queryset.count()
            if total_outlet > len(current_outlet):
                self.initial['all_outlet'] = False
            else:
                self.initial['all_outlet'] = True

    def save(self, *args, **kwargs):
        outlets = self.cleaned_data.pop('outlet')
        all_outlet = self.cleaned_data.pop('all_outlet')
        obj = super(DeviceUserStaffForm, self).save(*args, **kwargs)
        obj.permission = DeviceUser.PERMISSION.staff
        obj.all_outlet_access = all_outlet

        obj.save()

        self.update_m2m(
            obj, outlets, obj.outlet,
            OutletDeviceUser, OutletDeviceUser.device_user)

        return obj
