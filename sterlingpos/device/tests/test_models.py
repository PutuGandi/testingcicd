from django.test import TestCase
from django.db import IntegrityError

from model_mommy import mommy

from sterlingpos.device.models import DeviceUserPermission, DeviceUser
from sterlingpos.core.models import set_current_tenant

mommy.generators.add(
    'sterlingpos.core.models.SterlingTenantForeignKey',
    mommy.random_gen.gen_related)


class TestDeviceUserPermission(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.device_user_permission = DeviceUserPermission.objects.create(name='admin')

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        self.assertEqual(
            self.device_user_permission.__str__(),
            'Device Permission : admin'
        )


class TestDeviceUser(TestCase):

    def setUp(self):
        self.account = mommy.make('accounts.Account', name='Account-1')
        set_current_tenant(self.account)
        self.device_user_permission = DeviceUserPermission.objects.create(name='admin')
        self.device_user = DeviceUser.objects.create(
            username='administrator',
            name='Administrator Test',
            email='admin@administrator.com',
            pin=12345)

    def tearDown(self):
        set_current_tenant(None)

    def test__str__(self):
        self.assertEqual(
            self.device_user.__str__(),
            'administrator | Administrator Test'
        )
