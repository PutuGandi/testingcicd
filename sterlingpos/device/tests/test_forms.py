from test_plus.test import TestCase

from model_mommy import mommy

from sterlingpos.core.models import set_current_tenant, get_current_tenant
from sterlingpos.device.forms import (
    DeviceUserManagerForm,
    DeviceUserStaffForm
)


class DeviceUserManagerFormTest(TestCase):

    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.form_class = DeviceUserManagerForm
        set_current_tenant(self.account)
    
    def tearDown(self):
        set_current_tenant(None)

    def test_create_manager(self):
        data = {
            "username": "username",
            "name": "name",
            "email": "username@mail.test",
            "pin": "012345",
            "role": self.account.role_set.filter(
                is_supervisor=True).first().pk,
        }
        
        form = self.form_class(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        obj = form.save()
        self.assertEqual(obj.username, data.get("username"))
        self.assertEqual(obj.account, self.account)
        self.assertEqual(obj.permission, "manager")
        self.assertEqual(obj.pin, data.get("pin"))


class DeviceUserStaffFormTest(TestCase):

    def setUp(self):
        self.account = mommy.make("accounts.Account", name="account-1")
        self.form_class = DeviceUserStaffForm
        set_current_tenant(self.account)
    
    def tearDown(self):
        set_current_tenant(None)

    def test_create_staff(self):
        data = {
            "username": "username",
            "name": "name",
            "pin": "012345",
            "permission": "staff",
            "role": self.account.role_set.filter(
                is_supervisor=False).first().pk,
        }

        form = self.form_class(data=data)
        self.assertTrue(form.is_valid(), form.errors)
        obj = form.save()
        self.assertEqual(obj.username, data.get("username"))
        self.assertEqual(obj.account, self.account)
        self.assertEqual(obj.pin, data.get("pin"))
        self.assertEqual(obj.permission, data.get("permission"))

    def test_m2m_update_does_not_affect_other_staff(self):
        outlet = mommy.make(
            "outlet.Outlet", branch_id="test", name="Test") 

        device_user_1 = mommy.make(
            "device.DeviceUser",
            name="test-1", username="test", pin="123456",
            account=self.account) 
        device_user_2 = mommy.make(
            "device.DeviceUser",
            name="test-2", username="test2", pin="123456",
            account=self.account)

        mommy.make(
            "outlet.OutletDeviceUser",
            device_user=device_user_1, outlet=outlet,
            account=self.account)
        mommy.make(
            "outlet.OutletDeviceUser",
            device_user=device_user_2, outlet=outlet,
            account=self.account)

        data = {
            "username": "username",
            "name": "name",
            "pin": "012345",
            "permission": "staff",
            "role": self.account.role_set.filter(
                is_supervisor=False).first().pk,
            "outlet": []
        }

        form = self.form_class(
            data=data, instance=device_user_1)
        self.assertTrue(form.is_valid(), form.errors)
        obj = form.save()
        self.assertEqual(obj.username, data.get("username"))
        self.assertEqual(obj.account, self.account)
        self.assertEqual(obj.pin, data.get("pin"))
        self.assertEqual(obj.permission, data.get("permission"))
        self.assertEqual(obj.outlet.count(), 0)
        self.assertEqual(device_user_1.outlet.count(), 0)
        self.assertEqual(device_user_2.outlet.count(), 1)
