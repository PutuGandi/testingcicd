from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = 'device'
urlpatterns = [
    url(regex=r'^device-user/staff$', view=views.DeviceUserStaffListView.as_view(),
        name='device_user_staff_list'),
    url(regex=r'^device-user/staff/create/$', view=views.DeviceUserStaffCreateView.as_view(),
        name='device_user_staff_create'),
    url(regex=r'^device-user/staff/(?P<pk>[0-9]+)/update/$', view=views.DeviceUserStaffUpdateView.as_view(),
        name='device_user_staff_update'),
    url(r'^device-user/staff/archive/$', view=views.DeviceUserStaffArchiveView.as_view(),
        name='device_user_staff_archive'),
    url(r'^device-user/staff/activate/$', view=views.DeviceUserStaffActivateView.as_view(),
        name='device_user_staff_activate'),
    url(regex=r'^device-user/staff/delete/$', view=views.DeviceUserStaffDeleteView.as_view(),
        name='device_user_staff_delete'),

    url(regex=r'^device-user/manager/$', view=views.DeviceUserManagerListView.as_view(),
        name='device_user_manager_list'),
    url(regex=r'^device-user/manager/create/$', view=views.DeviceUserManagerCreateView.as_view(),
        name='device_user_manager_create'),
    url(regex=r'^device-user/manager/(?P<pk>[0-9]+)/update/$', view=views.DeviceUserManagerUpdateView.as_view(),
        name='device_user_manager_update'),
    url(regex=r'^device-user/manager/archive/$', view=views.DeviceUserManagerArchiveView.as_view(),
        name='device_user_manager_archive'),
    url(regex=r'^device-user/manager/activate/$', view=views.DeviceUserManagerActivateView.as_view(),
        name='device_user_manager_activate'),
    url(regex=r'^device-user/manager/delete/$', view=views.DeviceUserManagerDeleteView.as_view(),
        name='device_user_manager_delete'),

    url(regex=r'^data/device-user/staff/$', view=views.DeviceUserStaffListJson.as_view(),
        name='data_device_user_staff'),
    url(regex=r'^data/device-user/manager/$', view=views.DeviceUserManagerListJson.as_view(),
        name='data_device_user_manager'),
]
