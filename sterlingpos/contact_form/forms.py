from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from contact_form.forms import ContactForm

class ContactUsForm(ContactForm):
    company = forms.CharField(max_length=100, label='Company')
    phone = forms.CharField(max_length=100, label='Your phone')


class TryFreeForm(forms.Form):
    email = forms.EmailField(max_length=200, label='Email')