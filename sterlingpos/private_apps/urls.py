from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

app_name = 'private_apps'
urlpatterns = [
    url(regex=r'^$', view=views.PrivateAppsListView.as_view(),
        name='list'),
    url(regex=r'^data/$', view=views.PrivateAppsListJson.as_view(),
        name='data'),
    url(regex=r'^create/$', view=views.PrivateAppsCreateView.as_view(),
        name='create'),
    url(regex=r'^(?P<pk>\d+)/$', view=views.PrivateAppsUpdateView.as_view(),
        name='update'),
    url(r'^(?P<pk>\d+)/delete/$', views.PrivateAppsDeleteView.as_view(),
        name='delete'),
]
