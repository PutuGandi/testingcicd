import datetime
import requests
import logging

from celery import shared_task

logger = logging.getLogger(__name__)


@shared_task(bind=True, max_retries=5)
def async_feed_webhook(self, webhook_url, data):
    try:
        response = requests.post(webhook_url, json=data)
        if response.status_code != 200:
            logger.error(f"Request failed for {webhook_url} - {response} [{data['topic_label']}-{data['data']['id']}]")
            raise Exception
        logger.info(f"Request success for {webhook_url} - {response} [{data['topic_label']}-{data['data']['id']}]")
        return response.status_code
    except:
        self.retry(countdown= 5 ** self.request.retries)
