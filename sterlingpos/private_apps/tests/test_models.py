import hashlib
from django.test import TestCase
from django.utils import timezone

from model_mommy import mommy

from celery.result import AsyncResult

from sterlingpos.private_apps.models import PrivateApp, Webhook
from sterlingpos.cash_balance.models import CashBalanceLog
from sterlingpos.catalogue.models import (
    Category,
    Catalogue,
    Product,
)
from sterlingpos.catalogue.categories import create_from_breadcrumbs


class TestPrivateApp(TestCase):

    def test_save_will_generate_API_KEY_and_credentials(self):
        account = mommy.make('accounts.Account', name='Test Account')
        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,)
        private_app.save()
        self.assertIsNotNone(private_app.key)
        self.assertIsNotNone(private_app.credential)
        self.assertNotEqual(private_app.key, private_app.credential)

    def test_authenticate_classmethod(self):
        account = mommy.make('accounts.Account', name='Test Account')
        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            key="APIKEYTEST",
            credential="APIKEYCREDENTIAL",
            account=account,)
        private_app.save()
        self.assertEqual(PrivateApp.authenticate(
            key='APIKEYTEST',
            credential='APIKEYCREDENTIAL'
        ), private_app)


class TestWebhook(TestCase):

    def test_get_topic_sales_order(self):
        account = mommy.make('accounts.Account', name='Test Account')

        sales_order = mommy.make('sales.SalesOrder', code='SALES-001', account=account)

        self.assertEqual(
            Webhook.get_topic(sales_order), Webhook.TOPIC_CHOICES.transaction)

    def test_get_topic_cash_balance(self):
        account = mommy.make('accounts.Account', name='Test Account')
        
        outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=account)
        device_user = account.deviceuser_set.first()
        cash_balance_log = mommy.make(
            'cash_balance.CashBalanceLog',
            account=account,
            outlet=outlet,
            device_user=device_user,
            status=CashBalanceLog.STATUS.shift_start,
            transaction_date=timezone.now(),
        )

        self.assertEqual(
            Webhook.get_topic(cash_balance_log),
            Webhook.TOPIC_CHOICES.shift_activity)

    def test_validate_topic(self):
        account = mommy.make('accounts.Account', name='Test Account')
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001', account=account)
        
        outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=account)
        device_user = account.deviceuser_set.first()
        cash_balance_log = mommy.make(
            'cash_balance.CashBalanceLog',
            account=account,
            outlet=outlet,
            device_user=device_user,
            status=CashBalanceLog.STATUS.shift_start,
            transaction_date=timezone.now(),
        )

        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction",],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        
        self.assertTrue(webhook.validate_topic(sales_order))
        self.assertFalse(webhook.validate_topic(cash_balance_log))

    def test_build_token(self):
        account = mommy.make('accounts.Account', name='Test Account')
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001', account=account)

        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction",],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        token = f"{sales_order.pk}-{private_app.credential}"
        sign_key = hashlib.sha512(
            token.encode('utf-8')).hexdigest()
        self.assertEqual(
            webhook.build_token(sales_order),
            sign_key
        )

    def test_build_json_sales_order(self):
        account = mommy.make('accounts.Account', name='Test Account')
        sales_order = mommy.make('sales.SalesOrder', code='SALES-001', account=account)

        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction",],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        
        json_response = webhook.build_json(sales_order)
        self.assertIn("signature_key", json_response)
        self.assertIn("data", json_response)
        self.assertEqual(json_response["topic_label"], "TRANSACTION")

    def test_build_json_cash_balance(self):
        account = mommy.make('accounts.Account', name='Test Account')
        
        outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=account)
        device_user = account.deviceuser_set.first()
        cash_balance_log = mommy.make(
            'cash_balance.CashBalanceLog',
            account=account,
            outlet=outlet,
            device_user=device_user,
            status=CashBalanceLog.STATUS.shift_start,
            transaction_date=timezone.now(),
        )

        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction",],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        json_response = webhook.build_json(cash_balance_log)
        self.assertIn("signature_key", json_response)
        self.assertIn("data", json_response)
        self.assertEqual(json_response["topic_label"], "SHIFT_ACTIVITY")

    def test_build_json_product(self):
        account = mommy.make(
            'accounts.Account', name='Test Account')

        product = mommy.make(
            'catalogue.Product',
            account=account,
            sku='product-test',
            parent=None,
        )

        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction",],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        json_response = webhook.build_json(product)
        self.assertIn("signature_key", json_response)
        self.assertIn("data", json_response)
        self.assertEqual(
            json_response["topic_label"], "PRODUCT")

    def test_build_json_category(self):
        account = mommy.make(
            'accounts.Account', name='Test Account')

        category = create_from_breadcrumbs(
            "category", account=account)
            
        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction",],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        json_response = webhook.build_json(category)
        self.assertIn("signature_key", json_response)
        self.assertIn("data", json_response)
        self.assertEqual(
            json_response["topic_label"], "CATEGORY")

    def test_build_json_uom(self):
        account = mommy.make(
            'accounts.Account', name='Test Account')
    
        uom = mommy.make(
            'catalogue.uom',
            account=account,
            unit='pcs',
            uom_type="base",
            category="units",
        )

        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction",],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        json_response = webhook.build_json(uom)
        self.assertIn("signature_key", json_response)
        self.assertIn("data", json_response)
        self.assertEqual(json_response["topic_label"], "UOM")

    def test_webhook_feed(self):
        account = mommy.make('accounts.Account', name='Test Account')
        
        outlet = mommy.make(
            'outlet.Outlet',
            name='Outlet 1', branch_id='Outlet-1', account=account)
        device_user = account.deviceuser_set.first()
        cash_balance_log = mommy.make(
            'cash_balance.CashBalanceLog',
            account=account,
            outlet=outlet,
            device_user=device_user,
            status=CashBalanceLog.STATUS.shift_start,
            transaction_date=timezone.now(),
        )

        private_app = PrivateApp(
            name="Private App",
            description="Private App Description",
            account=account,
        )
        private_app.save()

        webhook = Webhook(
            title="Transaction Webhook",
            address="https://localhost",
            topic_list=["transaction", "shift_activity"],     
            private_app=private_app,
            account=account,
        )
        webhook.save()
        response = webhook.feed(cash_balance_log)
        self.assertTrue(isinstance(response, AsyncResult))
