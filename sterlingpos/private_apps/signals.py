from django.conf import settings
from django.dispatch import Signal

from sterlingpos.private_apps.models import Webhook
from sterlingpos.core.models import set_current_tenant


def feed_webhook(sender, **kwargs):

    instance = kwargs.get('instance')
    created = kwargs.get('created')
    account = instance.account

    webhooks = Webhook.objects.filter(
        topic_list__contains=[Webhook.get_topic(instance)],
        account=account, is_active=True)

    for webhook in webhooks:
        webhook.feed(instance)

feed_webhook_signal = Signal(providing_args=["instance", "created"])
