from django.contrib import admin

from .models import (
    PrivateApp, Webhook
)


class PrivateAppAdmin(admin.ModelAdmin):
    raw_id_fields = ("account", )
    list_display = (
        "name", "description", "account")
    search_fields = (
        "name", "description", "account__name")


class WebhookAdmin(admin.ModelAdmin):
    raw_id_fields = ("account", "private_app", "outlet", )
    filter_horizontal = ("outlet", )

    list_display = (
        "title", "address", "topic_list", "is_active", "account")
    search_fields = (
        "title", "address", "topic_list", "is_active", "account__name")


admin.site.register(PrivateApp, PrivateAppAdmin)
admin.site.register(Webhook, WebhookAdmin)
