from django import forms

from sterlingpos.private_apps.models import (
    PrivateApp,
)


class PrivateAppUpdateForm(forms.ModelForm):

    class Meta:
        model = PrivateApp
        exclude = ('account', )

    def __init__(self, *args, **kwargs):
        super(PrivateAppUpdateForm, self).__init__(*args, **kwargs)
        self.fields['key'].disabled = True
        self.fields['credential'].disabled = True
