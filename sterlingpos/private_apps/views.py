from django.views.generic import (
    TemplateView,
    CreateView, UpdateView, DeleteView
)
from django.urls import reverse_lazy
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.utils.html import escape
from django_datatables_view.base_datatable_view import BaseDatatableView

from sterlingpos.core.mixins import OwnerOnlyMixin
from sterlingpos.private_apps.models import PrivateApp
from sterlingpos.private_apps.forms import PrivateAppUpdateForm
from sterlingpos.role.mixins import SterlingRoleMixin


class PrivateAppsListJson(SterlingRoleMixin, BaseDatatableView):
    columns = ['name', 'created', 'action']
    order_columns = ['name', 'created', 'action']
    model = PrivateApp

    def render_column(self, row, column):
        if column == 'action':
            action_data = {
                'id': row.id,
                'update': reverse_lazy(
                    "private-apps:update", kwargs={'pk': row.pk}),
                'delete': reverse_lazy(
                    "private-apps:delete", kwargs={'pk': row.pk})
            }
            return action_data
        elif column == 'name':
            return '<a href="{}">{}</a>'.format(
                reverse_lazy(
                    "private-apps:update", kwargs={'pk': row.pk}), escape(row.name))
        else:
            return super(PrivateAppsListJson, self).render_column(row, column)


class PrivateAppsListView(SterlingRoleMixin, TemplateView):
    model = PrivateApp
    template_name = "dashboard/private_apps/list.html"


class PrivateAppsCreateView(SterlingRoleMixin, CreateView):
    model = PrivateApp
    fields = ['name', 'description']
    template_name = "dashboard/private_apps/create.html"

    def form_valid(self, form):
        response = super(PrivateAppsCreateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Private App has been created.')))
        return response

    def get_success_url(self):
        return reverse_lazy(
            "private-apps:update", kwargs={'pk': self.object.pk})


class PrivateAppsUpdateView(SterlingRoleMixin, UpdateView):
    model = PrivateApp
    form_class = PrivateAppUpdateForm
    template_name = "dashboard/private_apps/update.html"
    success_url = reverse_lazy('private-apps:list')

    def form_valid(self, form):
        response = super(PrivateAppsUpdateView, self).form_valid(form)
        messages.success(self.request, '{}'.format(
            _('Private App has been updated.')))
        return response


class PrivateAppsDeleteView(SterlingRoleMixin, DeleteView):
    model = PrivateApp
    template_name = "dashboard/private_apps/delete.html"
    success_url = reverse_lazy('private-apps:list')

    def delete(self, request, *args, **kwargs):
        response = super(PrivateAppsDeleteView, self).delete(
            request, *args, **kwargs)
        messages.success(self.request, '{}'.format(
            _('Private App has been deleted.')))
        return response
