from django.apps import AppConfig
from django.db.models.signals import post_save


class PrivateAppsConfig(AppConfig):
    name = 'sterlingpos.private_apps'
    verbose_name = "Private Apps"

    def ready(self):

        from sterlingpos.cash_balance.models import CashBalanceLog
        from sterlingpos.catalogue.models import (
            UOM,
            Category,
            Product,
            ProductModifier,
            CompositeProduct,
        )
        from sterlingpos.stocks.models import (
            Supplier,
            PurchaseOrder,
            PurchaseOrderReceive,
        )

        from sterlingpos.private_apps.signals import feed_webhook, feed_webhook_signal

        feed_webhook_signal.connect(feed_webhook, dispatch_uid="sales_feed_webhook")
        post_save.connect(feed_webhook, sender=CashBalanceLog)

        post_save.connect(feed_webhook, sender=UOM)
        post_save.connect(feed_webhook, sender=Category)
        post_save.connect(feed_webhook, sender=Product)

        post_save.connect(feed_webhook, sender=Supplier)
        post_save.connect(feed_webhook, sender=PurchaseOrder)
        post_save.connect(feed_webhook, sender=PurchaseOrderReceive)
