from __future__ import unicode_literals, absolute_import

import os
import binascii
import hashlib
import logging

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify

from django.contrib.postgres.fields import ArrayField
from model_utils import Choices
from model_utils.models import TimeStampedModel
from sterlingpos.core.models import SterlingTenantModel, SterlingTenantForeignKey

logger = logging.getLogger(__name__)


class PrivateApp(SterlingTenantModel, TimeStampedModel):
    name = models.CharField(_("Name"), blank=True, default="", max_length=255)
    description = models.TextField(_("Description"), blank=True, max_length=255)

    key = models.CharField(_("Key"), max_length=40, db_index=True)
    credential = models.CharField(_("Credential"), max_length=40)

    class Meta:
        verbose_name = "PrivateApp"
        verbose_name_plural = "PrivateApps"
        unique_together = (('account', 'id'),)

    @classmethod
    def authenticate(cls, key, credential):
        try:
            obj = cls.objects.get(key=key, credential=credential)
            return obj
        except cls.DoesNotExist as e:
            pass

    def __str__(self):
        return f"{self.name}, {self.account}"

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        if not self.credential:
            self.credential = self.generate_key()
        return super(PrivateApp, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()


class Webhook(SterlingTenantModel, TimeStampedModel):

    TOPIC_CHOICES = Choices(
        ('transaction', 'transaction', _('Transaction')),
        ('shift_activity', 'shift_activity', _('Shift Activity')),
        ('uom', 'uom', _('UOM')),
        ('product', 'product', _('Product')),
        ('category', 'category', _('Category')),
        ('supplier', 'supplier', _('Supplier')),

        ('purchase_order', 'purchase_order', _('Purchase Order')),
        ('purchase_order_receive', 'purchase_order_receive', _('PurchaseOrderReceive')),
    )

    title = models.CharField(_("Title"), max_length=255)
    address = models.CharField(_("Url Address"), blank=True, default="", max_length=255)
    
    topic_list = ArrayField(
        models.CharField(
            max_length=255,
            choices=TOPIC_CHOICES,
        ),
        default=[
            "transaction", "shift_activity",
            "product", "category", "uom",
            "supplier",
        ],
        blank=True,
        null=True
    )
    
    is_active = models.BooleanField(default=True)
    
    private_app = SterlingTenantForeignKey(
        PrivateApp, blank=True, null=True,
        on_delete=models.CASCADE)

    outlet = models.ManyToManyField(
        "outlet.Outlet", blank=True,
        related_name='outlet_webhook')

    class Meta:
        verbose_name = "Webhook"
        verbose_name_plural = "Webhooks"
        unique_together = (('account', 'id'),)

    @classmethod
    def get_topic(cls, instance):
        if instance.__class__._meta.model_name == 'salesorder':
            return cls.TOPIC_CHOICES.transaction
        if instance.__class__._meta.model_name == 'cashbalancelog':
            return cls.TOPIC_CHOICES.shift_activity
        if instance.__class__._meta.model_name == 'uom':
            return cls.TOPIC_CHOICES.uom
        if instance.__class__._meta.model_name == 'category':
            return cls.TOPIC_CHOICES.category
        if instance.__class__._meta.model_name == 'product':
            return cls.TOPIC_CHOICES.product
        if instance.__class__._meta.model_name == 'supplier':
            return cls.TOPIC_CHOICES.supplier

        if instance.__class__._meta.model_name == 'purchaseorder':
            return cls.TOPIC_CHOICES.purchase_order
        if instance.__class__._meta.model_name == 'purchaseorderreceive':
            return cls.TOPIC_CHOICES.purchase_order_receive

    def __str__(self):
        return f"{self.title}, {self.address}"

    def _get_outlet_rel(self, instance):
        if instance.__class__._meta.model_name == 'salesorder':
            return instance.outlet
        if instance.__class__._meta.model_name == 'cashbalancelog':
            return instance.outlet       
        if instance.__class__._meta.model_name == 'purchaseorder':
            return instance.outlet       
        if instance.__class__._meta.model_name == 'purchaseorderreceive':
            return instance.purchase_order.outlet       

    def validate_topic(self, instance):
        topic = Webhook.get_topic(instance)
        outlet = self._get_outlet_rel(instance)

        logger.info(f"Processing webhook feed for {topic} ID {instance.pk}")

        has_topic = topic in self.topic_list

        if self.outlet.exists() and outlet:
            is_allowed = self.outlet.filter(pk=outlet.pk).exists()
        else:
            is_allowed = True

        condition = has_topic & is_allowed

        if not condition:
            logger.info(f"Validation failed for {topic} ID {instance.pk}")

        return condition

    def build_token(self, instance):
        token = f"{instance.pk}-{self.private_app.credential}"
        sign_key = hashlib.sha512(
            token.encode('utf-8')).hexdigest()
        return sign_key

    def build_json(self, instance):

        from sterlingpos.api.serializers.sales_serializers import TransactionOrderSerializer
        from sterlingpos.api.serializers.cash_balance_serializers import CashBalanceLogSerializer
        from sterlingpos.api.serializers.catalogue_serializers import (
            UOMSerializer,
            CategorySerializer,
            ProductModifierSerializer,
            ProductSerializer,
        )
        from sterlingpos.api.serializers.stocks_serializers import (
            SupplierSerializer,
            PurchaseOrderSerializer,
            ReceivedOrderSerializer,
        )
        MODEL_NAME_TO_SERIALIZER = {
            "salesorder": TransactionOrderSerializer,
            "cashbalancelog": CashBalanceLogSerializer,
            "uom": UOMSerializer,
            "category": CategorySerializer,
            "product": ProductSerializer,
            "supplier": SupplierSerializer,

            "purchaseorder": PurchaseOrderSerializer,
            "purchaseorderreceive": ReceivedOrderSerializer,
        }
        serializer_class = MODEL_NAME_TO_SERIALIZER[instance.__class__._meta.model_name]
        serializer = serializer_class(instance=instance)
        sign_key = self.build_token(instance)
        data = {
            "topic_label": Webhook.get_topic(instance).upper(),
            "signature_key": sign_key,
            "data": serializer.data,
        }
        return data

    def feed(self, instance):
        from sterlingpos.private_apps.tasks import async_feed_webhook
        if self.validate_topic(instance):
            data = self.build_json(instance)
            logger.info(f"Sending request to {self.address} for {self.__class__.get_topic(instance)} ID {instance.pk}")
            result =  async_feed_webhook.delay(self.address, data)
            return result
