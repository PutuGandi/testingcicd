var url = window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');

var ranges = {
    'Hari Ini': [moment(), moment()],
    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Minggu Berjalan': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
    'Minggu Lalu': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
    'Bulan Berjalan': [moment().startOf('month'), moment().endOf('month')],
    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
    'Tahun Berjalan': [moment().startOf('year'), moment().endOf('days')],
    'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
}

var locale_format = {
      'format': 'DD/MM/YYYY',
      "customRangeLabel": "Pilih Tanggal",
      "applyLabel": "Terapkan",
      "cancelLabel": "Batal",
      "daysOfWeek": [
        "Min",
        "Sen",
        "Sel",
        "Rab",
        "Kam",
        "Jum",
        "Sab"
      ],
      "monthNames": [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
      ],
}

$('#mobilereportrange').val(startDateMobile + " - " + endDateMobile);
$('#mobilereportrange').daterangepicker({
  'ranges': ranges ,
  'opens': 'left',
});

$('#dashboardrange').daterangepicker({
    'minDate': moment().subtract(18, 'month'),
    'maxDate': moment().add(1, 'month'),
    'opens': "right",
    'ranges': ranges,
    'locale': locale_format,
  },
  function (start, end, label) {
    var s = moment(start.toISOString());
    var e = moment(end.toISOString());
    startDate = s.format('YYYY-MM-DD HH:mm:ss');
    endDate = e.format('YYYY-MM-DD HH:mm:ss');
    labelChoice = label;
    $('#dashboardrange span').html(label);
  });

$('#daterange').daterangepicker({
    'minDate': moment().subtract(18, 'month'),
    'maxDate': moment().add(1, 'month'),
    'timePicker': true,
    'timePicker24Hour': true,
    'timePickerIncrement': 1,
    'opens': "left",
    'locale': locale_format,

  },
  function (start, end, label) {
    console.log(label);
    var s = moment(start.toISOString());
    var e = moment(end.toISOString());
    startDate = s.format('YYYY-MM-DD HH:mm:ss');
    endDate = e.format('YYYY-MM-DD HH:mm:ss');
    $('#daterange span').html(s.format('DD/MM/YYYY HH:mm:ss') + ' - ' + e.format('DD/MM/YYYY HH:mm:ss'));
  });

$('#daterange').on('cancel.daterangepicker', function (ev, picker) {
  $(this).val('');
});

$('#daterange').on('apply.daterangepicker',
  function (ev, picker) {
    console.log('date');
    startdate = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
    enddate = picker.endDate.format('YYYY-MM-DD HH:mm:ss');
    base_url = baseURL;
    new_url = base_url + "?startdate=" + startdate + "&enddate=" + enddate
    window.location = new_url;
  });

$('#dashboardrange').on('cancel.daterangepicker', function (ev, picker) {
  $(this).val('');
});

$('#dashboardrange').on('apply.daterangepicker',
  function (ev, picker) {
    label_date = picker.chosenLabel;
    console.log(label_date);
    base_url = baseURL;
    startdate = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
    enddate = picker.endDate.format('YYYY-MM-DD HH:mm:ss');
    switch (label_date) {
      case "Hari Ini":
        new_url = base_url + "?date=today"
        window.location = new_url;
        break;
      case "Kemarin":
        new_url = base_url + "?date=yesterday"
        window.location = new_url;
        break;
      case "Minggu Berjalan":
        new_url = base_url + "?date=week_to_date"
        window.location = new_url;
        break;
      case "Minggu Lalu":
        new_url = base_url + "?date=last_week"
        window.location = new_url;
        break;
      case "Bulan Berjalan":
        new_url = base_url + "?date=month_to_date"
        window.location = new_url;
        break;
      case "Bulan Lalu":
        new_url = base_url + "?date=last_month"
        window.location = new_url;
        break;
      case "Tahun Berjalan":
        new_url = base_url + "?date=year_to_date"
        window.location = new_url;
        break;
      case "Tahun Lalu":
        new_url = base_url + "?date=last_year"
        window.location = new_url;
        break;
      case "Pilih Tanggal":
        new_url = base_url + "?startdate=" + startdate + "&enddate=" + enddate
        window.location = new_url;
        break;
    }

  });

$('#mobilereportrange').on('apply.daterangepicker',
  function (ev, picker) {
    base_url = baseURL;
    startdate = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
    enddate = picker.endDate.format('YYYY-MM-DD HH:mm:ss');

    new_url = base_url + "?startdate=" + startdate + "&enddate=" + enddate
    window.location = new_url;
  });

$("#companyBranch").change(function () {
  new_url = this.value + dateUrlExtra
  window.location = new_url;
});
