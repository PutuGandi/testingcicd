function numDigits(x) {
  return Math.max(Math.floor(Math.log10(Math.abs(x))), 0) + 1;
}

function upperRoundNum(x) {
  let digitLength = numDigits(x);
  if (digitLength >= 2) {
    let divider = 10 ** (digitLength - 2);
    return Math.round(x / divider) * divider;
  } else {
    return Math.round(x / 10) * 10;
  }
}

function kFormatter(num) {
    return Math.abs(num) > 999999 ? Math.sign(num)*((Math.abs(num)/1000000).toFixed(1)) + 'jt' : Math.sign(num)*Math.abs(num)
}

function generateSalesSummaryChart(salesData){
    let xAxisOption = {
      type: 'category',
      tick: {
        fit: false,
        centered: true,
        culling: {
          max: 7
        },
        multiline: false
      }
    };

    let yAxisOption = {
      tick: {
        format: function (x) {
          return (x == Math.floor(x)) ? x : "";
        }
      }
    };

    let salesYAxisOption;

    if (salesData.length) {
      salesYAxisOption = {
        max: upperRoundNum(Math.max(...salesData.map(x => x["amount"]))),
        tick: {
          count: 6,
          fit: false,
          multiline: false,
          format: function (x) {
            return "IDR " + x.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
          }
        }
      };
    } else {
      salesYAxisOption = yAxisOption;
    }

    var salesAreaChart = c3.generate({
      bindto: '#flot-line',
      data: {
        x: 'x',
        columns: [
          ['x', ...salesData.map(x => x["date"])],
          ['Penjualan', ...salesData.map(x => x["amount"])],
        ],
        types: {
          Penjualan: 'area-spline'
        }
      },
      legend: {
        show: false
      },
      axis: {
        x: xAxisOption,
        y: salesYAxisOption,
      },
      grid: {
        y: {
          show: true
        }
      },
      onrendered: () => {
        d3.selectAll('#flot-line .c3-axis-y tspan')[0].forEach((v, i) => {
          if (v.innerHTML == "") {
            v.parentElement.parentElement.style.opacity = 0;
          }
        });
      },
    });

    var transactionAreaChart = c3.generate({
      bindto: '#flot-line-2',
      data: {
        x: 'x',
        columns: [
          ['x', ...salesData.map(x => x["date"])],
          ['Transaksi', ...salesData.map(x => x["count"])],
        ],
        types: {
          Transaksi: 'area-spline'
        }
      },
      legend: {
        show: false
      },
      axis: {
        x: xAxisOption,
        y: yAxisOption
      },
      grid: {
        y: {
          show: true
        }
      },
      onrendered: () => {
        d3.selectAll('#flot-line-2 .c3-axis-y tspan')[0].forEach((v, i) => {
          if (v.innerHTML == "") {
            v.parentElement.parentElement.style.opacity = 0;
          }
        });
      },
    });
}





