function generateDistributionChart(transactionData, element, c3Options) {
  let formattedData = transactionData.map(function(x) {
    return [x["name"], x["value"]];
  });
  let colorObject = {};

  for (let i = 0; i < transactionData.length; i++) {
    colorObject[transactionData[i]["name"]] = dataColor[i];
  }
  let defaultOptions = {
    bindto: element,
    data: {
      columns: formattedData,
      order: 'asc',
      type: 'donut',
      labels: {
        format: function (v, id, i, j) {
          return v + "%";
        },
        position: 'inner-center',
      },
      colors: colorObject,
    },
    axis: {
      rotated: true,
      x: {
        show: false,
      },
      y: {
        max: 86,
        show: false,
      },
    },
    bar: {
      width: 20 // this makes bar width 100px
    }
  };

  c3.generate(Object.assign(defaultOptions, c3Options));
}

function generateCategorySalesChart(transactionData){
  let data = transactionData.map(function(x) {
    return {"name":x["name"], "value": parseInt(x["amount"])}
  });

  generateDistributionChart(data, "#category-sales-chart", {
    tooltip: {
      format: {
          title: function (d) {
            return 'Penjualan Per Kategori';
          },
          value: function (value, ratio, id) {
            value = "IDR " + kFormatter(transactionData[transactionData.map(x => x["name"]).indexOf(id)]["amount"]);
            return value;
          }
      }
    },
  });
}

function generateCategoryTransactionChart(transactionData){
  let data = transactionData.map(function(x) {
    return {"name":x["name"], "value": x["count"]}
  });

  generateDistributionChart(data, "#category-transaction-chart", {
    tooltip: {
      format: {
          title: function (d) {
            return 'Transaksi Per Kategori';
          },
          value: function (value, ratio, id) {
            value = transactionData[transactionData.map(x => x["name"]).indexOf(id)]["count"];
            return value;
          }
      }
    },
  });
}

function generateTop5Chart(element, label, transactionData, format_function=undefined) {
  c3.generate({
      bindto: element,
      data: {
        columns: [
          [label, ...transactionData.map(x => x["value"])]
        ],
        type: 'bar',
      },
      legend: {
        show: false
      },
      axis: {
        rotated: true,
        y:{
          min: 0,
          max: upperRoundNum(Math.max(...transactionData.map(x => x["value"]))),
          tick: {
            fit: false,
            multiline: false,
            format: format_function
          },
          padding: 0
        },
        x:{
          type: 'category',
          categories: transactionData.map(x => x["name"]),
          tick: {
            fit: true,
            centered: true
          }
        },
      },
      bar: {
        width: {
          ratio: 0.9
        },
        space: 0.7,
      },
      grid: {
        y: {
          show: true
        }
      }
    });
}

function generateProductSalesChart(transactionData) {
  let data = transactionData.map(function(x) {
    return {"name":x["name"], "value": x["amount"]}
  });

  function label_formatter(x) {
    return "IDR " + kFormatter(x);
  }

  return generateTop5Chart("#product-sales-chart", "Penjualan", data, label_formatter);
}

function generateProductTransactionChart(transactionData) {
  let data = transactionData.map(function(x) {
    return {"name":x["name"], "value": x["count"]}
  });
  return generateTop5Chart("#product-transaction-chart", "Transaksi", data)
}


