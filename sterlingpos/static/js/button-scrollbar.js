 $(document).ready(function($){

if ($(document).height() > $(window).height()) {
    $('.fixed-bottom-content').show();
    $('#fixed-button-content').hide();
}
else {
    $('.fixed-bottom-content').hide();
    $('#fixed-button-content').show();

    $(document).scroll(function() {
      var y = $(this).scrollTop();
      if (y > 50) {
        $('.fixed-bottom-content').slideDown();
        $('#fixed-button-content').fadeOut();
      } else {
         $('.fixed-bottom-content').slideUp();
         $('#fixed-button-content').fadeIn();
      }
    });
}
});
