$(document).ready(function() {
  $('.clear-filter').hide();
  var url = window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
  var base_url = window.base_url || "";
  var custom_url = base_url
  var start_date = null;
  var end_date = null;


  var __fnExportFile = function (export_type_data) {
    params = table.ajax.params();
    params.length = -1;
    params.export_file = export_type_data;
    window.open(table.ajax.url() + "?" + $.param( params ),'_blank');
  }

  var defaultTableOptions = {
    dom: "lrt<'d-flex justify-content-between datatables-info'ip>",
    // dom: "rt<'row'<'col-lg-6'l><'col-lg-6'p>>",
    lengthChange: false,
    pageLength: 25,
    orderCellsTop: true,
    processing: false,
    serverSide: true,
    pagingType: "simple",
    scrollX: true,
    ajax: {
      url: base_url,
      data: function ( d ) {
        if (start_date) {
          d.start_date = start_date;
        }
        if (end_date) {
          d.end_date = end_date;
        }
        return d;
      }
    },
    order: [[ 0, "desc" ]],
    language: {
      paginate: {
        previous: "<",
        next: ">",
      },
    },
    // scrollX: true,
    language: {
      loadingRecords: '<div class="text-center margin-100"><section class="icon-menu table-not-found "></section><div style="margin-top:32px; margin-bottom:32px;"><h3 class="mb-3">Penarikan Data</h3><p class="text-muted text-center my-0">Sedang menarik data.</p></div></div>',
      emptyTable: '<div class="text-center margin-100"><section class="icon-menu table-not-found "></section><div style="margin-top:32px; margin-bottom:32px;"><h3 class="mb-3">Oops</h3><p class="text-muted text-center my-0">Data tidak tersedia.</p></div></div>',
      zeroRecords: '<div class="text-center margin-100"><section class="icon-menu table-not-found "></section><div style="margin-top:32px; margin-bottom:32px;"><h3 class="mb-3">Oops</h3><p class="text-muted text-center my-0">Data yang anda cari tidak ditemukan.</p></div></div>',
    }
  };

  var tableOptions = defaultTableOptions;
  if (window.tableOptions) {
      tableOptions = $.extend(tableOptions, window.tableOptions);
  }

  var reportRangeOnApply = window.reportRangeOnApply || function(ev, picker){}
  var reportRangeOnCancel = window.reportRangeOnCancel || function(ev, picker){}

  table = $('.selector-datatable').DataTable(tableOptions);
  table.export_file = __fnExportFile
  table.on( 'draw.dt', function () {
    var dataCount = table.page.info().recordsDisplay;
    $('.data-object-count').text(dataCount);
    if (dataCount <= table.page.len() ){
      $('.dataTables_paginate').hide();
    } else {
      $('.dataTables_paginate').show();
    }
  });

  $('.extra-table-control').each(function(index){
    $(this).on('keyup change', function(){
      var search_value = this.value;
      if (
          $(this).is('select') &&
          ["All", "Semua"].includes(
            $(this).children().filter(":selected").text())) {
        search_value = "";
      };
      table
        .column(index)
        .search(search_value)
        .draw();
      this.focus();
      if ($(this).is('input:text')) {
        $(this).attr('title', search_value);
      } else {
        $(this).attr('title', $(this).children().filter(":selected").text());
      };
      updateClearFilter();
    });
    $(this).on('apply.daterangepicker', function(ev, picker){
      var date_format = picker.startDate.format('YYYY-MM-DD HH:mm:ss') + ' - ' + picker.endDate.format('YYYY-MM-DD HH:mm:ss')
      table
        .column(index)
        .search(date_format)
        .draw();
      this.focus();
      updateClearFilter();
    });
    $(this).on('cancel.daterangepicker', function (ev, picker) {
        var date_format = ''
        table
          .column(index)
          .search(date_format)
          .draw();
        this.focus();
    });
  });

  $('.datatable-clearfilter').click(function(e){
    e.preventDefault();
    var control = $(this).parent().prev();
    control.val("");
    control.attr('title', "");
    control.trigger("keyup");
    return false;
  });

  function updateClearFilter() {
    $('.extra-table-control').each(function(index){
      var clearFilter = $(this).next();
      if (clearFilter.length > 0 && this.value && this.value !== 'All' && !$(this).is('select')) {
          // clearFilter.show();
      } else {
        clearFilter.hide();
      }
    });
  };

  $.extend( $.fn.dataTableExt.oSort, {
    "numeric-comma-pre": function ( a ) {
      var x = (a == "-") ? 0 : a.replace( /,/, "." );
      return parseFloat( x );
    },
    "numeric-comma-asc": function ( a, b ) {
      return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
    "numeric-comma-desc": function ( a, b ) {
      return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
  });

  $('.date-calendar').daterangepicker({
      'minDate': moment().subtract(6, 'month'),
      'maxDate': moment().add(1, 'month'),
      'ranges': {
          'Hari ini': [moment().format('DD/MM/YYYY 00:00:00'), moment().format('DD/MM/YYYY 23:59:59')],
          'Kemarin': [moment().subtract(1, 'days').format('DD/MM/YYYY 00:00:00'), moment().subtract(1, 'days').format('DD/MM/YYYY 23:59:59')],
          'Minggu ini': [moment().startOf('isoweek').format('DD/MM/YYYY 00:00:00'), moment().endOf('isoweek').format('DD/MM/YYYY 23:59:59')],
          'Minggu lalu': [moment().subtract(1, 'week').startOf('isoweek').format('DD/MM/YYYY 00:00:00'), moment().subtract(1, 'week').endOf('isoweek').format('DD/MM/YYYY 23:59:59')],
          'Bulan ini': [moment().startOf('month').format('DD/MM/YYYY 00:00:00'), moment().endOf('month').format('DD/MM/YYYY 23:59:59')],
          'Bulan lalu': [moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY 00:00:00'), moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY 23:59:59')]
      },
      'opens': "right",
      'locale': {
        'format': 'DD/MM/YYYY',
        "customRangeLabel": "Pilih Tanggal",
        "applyLabel": "Terapkan",
        "cancelLabel": "Batal",
        "daysOfWeek": [
          "Min",
          "Sen",
          "Sel",
          "Rab",
          "Kam",
          "Jum",
          "Sab"
        ],
        "monthNames": [
          "Januari",
          "Februari",
          "Maret",
          "April",
          "Mei",
          "Juni",
          "Juli",
          "Agustus",
          "September",
          "Oktober",
          "November",
          "Desember"
        ],
      },
    },
    function (start, end, label) {
      var s = moment(start.toISOString());
      var e = moment(end.toISOString());
      startDate = s.format('YYYY-MM-DD HH:mm:ss');
      endDate = e.format('YYYY-MM-DD HH:mm:ss');

      date_format = s.format('DD/MM/YYYY') + ' - ' + e.format('DD/MM/YYYY')
      $(this)[0].element.find("span").html(date_format)
  });

  $('.date-calendar').on('cancel.daterangepicker', function (ev, picker) {
      $(this).val('');
      $('.date-calendar span').html("Filter by Date");
      reportRangeOnCancel(ev, picker);
      start_date = null;
      end_date = null;
      table.ajax.reload();
  });

  $(':not(.export-date-calendar).date-calendar').on('apply.daterangepicker',
    function (ev, picker) {
      start_date = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
      end_date = picker.endDate.format('YYYY-MM-DD HH:mm:ss');
      table.ajax.reload();
      reportRangeOnApply(ev, picker);
  });

  function tog(v){return v?'addClass':'removeClass';} 

  $(document).on('input', '.clearable', function(){
      $(this)[tog(this.value)]('x');
  }).on('mousemove', '.x', function( e ){
      $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');
  }).on('touchstart click', '.onX', function( ev ){
      ev.preventDefault();
      $(this).removeClass('x onX').val('').change();
  });

  function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    else {
      params_arr = queryString.split("&");
      rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
  };

  $('.export-date-calendar').on('apply.daterangepicker',
    function (ev, picker) {
      params = table.ajax.params();
      params.length = -1;
      var old_url = table.ajax.url() + "?" + $.param( params );
      var alteredStartURL = removeParam("start_date", old_url);
      alteredEndURL = removeParam("end_date", alteredStartURL);
      start_date = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
      end_date = picker.endDate.format('YYYY-MM-DD HH:mm:ss');
      new_url = alteredEndURL + "&" +"start_date=" + start_date + "&end_date=" + end_date + "&" + $.param({"export_file": "csv"});
      window.open(new_url, '_blank');
  });
});
