
$('.export-date-calendar').daterangepicker({
  'minDate': moment().subtract(36, 'month'),
  'maxDate': moment().add(1, 'month'),
  'ranges': {
      'Hari ini': [moment().format('DD/MM/YYYY 00:00:00'), moment().format('DD/MM/YYYY 23:59:59')],
      'Kemarin': [moment().subtract(1, 'days').format('DD/MM/YYYY 00:00:00'), moment().subtract(1, 'days').format('DD/MM/YYYY 23:59:59')],
      'Minggu ini': [moment().startOf('isoweek').format('DD/MM/YYYY 00:00:00'), moment().endOf('isoweek').format('DD/MM/YYYY 23:59:59')],
      'Bulan ini': [moment().startOf('month').format('DD/MM/YYYY 00:00:00'), moment().endOf('month').format('DD/MM/YYYY 23:59:59')],
      'Minggu lalu': [moment().subtract(1, 'week').startOf('isoweek').format('DD/MM/YYYY 00:00:00'), moment().subtract(1, 'week').endOf('isoWeek').format('DD/MM/YYYY 23:59:59')],
      'Bulan lalu': [moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY 00:00:00'), moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY 23:59:59')],

  },
  'opens': "right",
  'locale': {
    'format': 'DD/MM/YYYY',
    "customRangeLabel": "Pilih Tanggal",
    "applyLabel": "Terapkan",
    "cancelLabel": "Batal",
    "daysOfWeek": [
      "Min",
      "Sen",
      "Sel",
      "Rab",
      "Kam",
      "Jum",
      "Sab"
    ],
    "monthNames": [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember"
    ],
  },
},
function (start, end, label) {
  var s = moment(start.toISOString());
  var e = moment(end.toISOString());
  startDate = s.format('YYYY-MM-DD HH:mm:ss');
  endDate = e.format('YYYY-MM-DD HH:mm:ss');

  date_format = s.format('DD/MM/YYYY') + ' - ' + e.format('DD/MM/YYYY')
});
  
$('.export-date-calendar').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
    start_date = null;
    end_date = null;
});
  
function removeParam(key, sourceURL) {
  var rtn = sourceURL.split("?")[0],
      param,
      params_arr = [],
      queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
  if (queryString !== "") {
      params_arr = queryString.split("&");
      for (var i = params_arr.length - 1; i >= 0; i -= 1) {
          param = params_arr[i].split("=")[0];
          if (param === key) {
              params_arr.splice(i, 1);
          }
      }
      rtn = rtn + "?" + params_arr.join("&");
  }
  else {
    params_arr = queryString.split("&");
    rtn = rtn + "?" + params_arr.join("&");
  }
  return rtn;
}

$('.export-date-calendar').on('apply.daterangepicker',
  function (ev, picker) {
    params = table.ajax.params();
    params.length = -1;
    var old_url = table.ajax.url() + "?" + $.param( params );
    var alteredStartURL = removeParam("start_date", old_url);
    alteredEndURL = removeParam("end_date", alteredStartURL);
    start_date = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
    end_date = picker.endDate.format('YYYY-MM-DD HH:mm:ss');
    new_url = alteredEndURL + "&" +"start_date=" + start_date + "&end_date=" + end_date + "&" + $.param({"export_file": "csv"});
    window.open(new_url, '_blank');
});
