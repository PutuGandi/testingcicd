/* Project specific Javascript goes here. */

/*
Formatting hack to get around crispy-forms unfortunate hardcoding
in helpers.FormHelper:

    if template_pack == 'bootstrap4':
        grid_colum_matcher = re.compile('\w*col-(xs|sm|md|lg|xl)-\d+\w*')
        using_grid_layout = (grid_colum_matcher.match(self.label_class) or
                             grid_colum_matcher.match(self.field_class))
        if using_grid_layout:
            items['using_grid_layout'] = True

Issues with the above approach:

1. Fragile: Assumes Bootstrap 4's API doesn't change (it does)
2. Unforgiving: Doesn't allow for any variation in template design
3. Really Unforgiving: No way to override this behavior
4. Undocumented: No mention in the documentation, or it's too hard for me to find
*/
$(document).ready(function() {
	$('.form-group').removeClass('row');

	$(function() {
		$("input[type='password'][data-eye]").each(function(i) {
			var $this = $(this);

			$this.after($("<div/>", {
				html: '<span class="fa fa-eye"></span>',
				id: 'passeye-toggle-'+i,
				style: 'float: right;margin-right: 15px;margin-top: -32px;position: relative;z-index: 2;'
			}));
			$this.after($("<input/>", {
				type: 'hidden',
				id: 'passeye-' + i
			}));
			$this.on("keyup paste", function() {
				$("#passeye-"+i).val($(this).val());
			});
			$("#passeye-toggle-"+i).on("click", function() {
				if($this.hasClass("show")) {
					$this.attr('type', 'password');
					$this.removeClass("show");
					$(this).html('<i class="fa fa-eye"></i>');
				}else{
					$this.attr('type', 'text');
					$this.val($("#passeye-"+i).val());				
					$this.addClass("show");
					$(this).html('<i class="fa fa-eye-slash"></i>');
				}
			});
		});
	});

	$('#subscription-box input[type="radio"]').on('click', function(){
		$('#subscription-box label').css('border','1px solid #E0E0E0');
		if ($(this).is(':checked')){
		  	$(this).parent().css('border','1px solid #0094C9');
		}
	});
});
