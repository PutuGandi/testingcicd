jQuery(document).ready(function($) {

	$('.sidebar .nav li.main-menu').on('click', function(){
		$(".secondary-sidebar").removeClass('expand');

		if($(this).hasClass('nav-item-has-children')){
			$(".secondary-sidebar").addClass('expand');
			$('.secondary-sidebar span').removeClass('show');
			$('.secondary-sidebar '+$(this).attr('data-toggle')).addClass('show');
		}

	 	$('li').removeClass('active');
		$(this).addClass('active');
	});

	$('.sidebar .nav li.nav-item .nav-link .menu-title .icon-menu').hover(function(){
		$('.secondary-sidebar span').removeClass('show');
		$(".secondary-sidebar").removeClass('expand');
	});

	$(document).on("keydown", ".numeric", function(event){
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
	});

	$(document).on("keydown", ".minus-numeric", function(event){
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 190)) {
                event.preventDefault();
            }
        }
	});


	function readFile(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('.dropzone-box').css('border', '1px solid #ebedf2', "!important");
				$('.invalid-feedback').hide();
				var wrapperZone = $(input).parent();
				$(wrapperZone).css("background-image", "url(" + e.target.result +")");
				$(wrapperZone).css("background-size", "contain");
				$(wrapperZone).css("background-repeat", "no-repeat");
				$(wrapperZone).css('border', '0');

				wrapperZone.removeClass('dragover');
				$('.dropzone-desc').hide();
			};
		reader.readAsDataURL(input.files[0]);
		}
	}

	var input_height = $('.dropzone-wrapper').width();
	$('.dropzone-wrapper').css({'height':input_height+'px'});

	var file_dropzone_height = $('input.dropzone.clearablefileinput').width();
	$("input.dropzone.clearablefileinput").css({'height':file_dropzone_height+'px'});
	$("input.dropzone.clearablefileinput").change(function () {
		readFile(this);
	});

	var dropzone_height = $('input#id_image_location').width();
	$('input#id_image_location').css({'height':dropzone_height+'px'});
	$("input#id_image_location").change(function () {
		readFile(this);
	});

	var promo_dropzone_height = $('input#id_promo_image').width();
	$('input#id_promo_image').css({'height':promo_dropzone_height+'px'});
	$('input#id_promo_image').change(function(){
		readFile(this);
	});

	var outlet_dropzone_height = $('input#id_outlet_image').width();
	$('input#id_outlet_image').css({'height':outlet_dropzone_height+'px'});
	$('input#id_outlet_image').change(function(){
		readFile(this);
	});

	var device_dropzone_height = $('input#id_device_user_image').width();
	$('input#id_device_user_image').css({'height':device_dropzone_height+'px'});
	$('input#id_device_user_image').change(function(){
		readFile(this);
	});


	$('.dropzone-wrapper').on('dragover', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).addClass('dragover');
	});
	$('.dropzone-wrapper').on('dragleave', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).removeClass('dragover');
	});

	function show_all_days(input){
		if(input.is(':checked')){
			$('#div_id_promo_configuration_form-allow_days').hide();
			$('#div_id_promo_configuration_form-allow_days input[type="checkbox"]').prop('checked', true);
		}else{
			$('#div_id_promo_configuration_form-allow_days').show();
		}
	}

	function show_all_outlet(input){
		if(input.is(':checked')){
			$('#div_id_promo_configuration_form-outlet').hide();
			$('#div_id_promo_configuration_form-outlet input[type="checkbox"]').prop('checked', true);
		}else{
			$('#div_id_promo_configuration_form-outlet').show();
		}
	}

	show_all_days($('#id_promo_configuration_form-all_days'));
	show_all_outlet($('#id_promo_configuration_form-all_outlet'));

	$('#div_id_promo_type-promo_type .form-check input[type="radio"]').click(function(){
		$('#div_id_promo_type-promo_type .form-check label').css('border','2px solid #9AA1A6');
		if ($(this).is(':checked')){
		  	$(this).parent().css('border','2px solid #4A90E2');
		}
	});

	$('#id_promo_configuration_form-all_days').on('change', function(){
		if($(this).is(':checked')){
			$('#div_id_promo_configuration_form-allow_days').hide();
			$('#div_id_promo_configuration_form-allow_days input[type="checkbox"]').prop('checked', true);
		}else{
			$('#div_id_promo_configuration_form-allow_days').show();
			$('#div_id_promo_configuration_form-allow_days input[type="checkbox"]').prop('checked', false);
		}
	});

	$('#id_promo_configuration_form-all_outlet').on('change', function(){
		if($(this).is(':checked')){
			$('#div_id_promo_configuration_form-outlet').hide();
			$('#div_id_promo_configuration_form-outlet input[type="checkbox"]').prop('checked', true);
		}else{
			$('#div_id_promo_configuration_form-outlet').show();
			$('#div_id_promo_configuration_form-outlet input[type="checkbox"]').prop('checked', false);
		}
	});

});
