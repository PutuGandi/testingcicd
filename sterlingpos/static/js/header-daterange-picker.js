var url = window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');

var ranges = {
    'Hari Ini': [moment(), moment()],
    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Minggu Berjalan': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
    'Minggu Lalu': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
    'Bulan Berjalan': [moment().startOf('month'), moment().endOf('month')],
    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
}

var locale_format = {
      'format': 'DD/MM/YYYY',
      "customRangeLabel": "Pilih Tanggal",
      "applyLabel": "Terapkan",
      "cancelLabel": "Batal",
      "daysOfWeek": [
        "Min",
        "Sen",
        "Sel",
        "Rab",
        "Kam",
        "Jum",
        "Sab"
      ],
      "monthNames": [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
      ],
}

$('#mobilereportrange').val(startDateMobile + " - " + endDateMobile);
$('#mobilereportrange').daterangepicker({
  'ranges': ranges ,
  'opens': 'left',
});
$('#mobilereportrange').on('apply.daterangepicker',
  function (ev, picker) {
    base_url = baseURL;
    startdate = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
    enddate = picker.endDate.format('YYYY-MM-DD HH:mm:ss');

    new_url = base_url + "?startdate=" + startdate + "&enddate=" + enddate
    window.location = new_url;
});


$('.header-daterange-picker').daterangepicker({
    'minDate': moment().subtract(18, 'month'),
    'maxDate': moment().add(1, 'month'),
    'opens': "right",
    'ranges': ranges,
    'locale': locale_format,
  },
  function (start, end, label) {
    var s = moment(start.toISOString());
    var e = moment(end.toISOString());
    startDate = s.format('YYYY-MM-DD HH:mm:ss');
    endDate = e.format('YYYY-MM-DD HH:mm:ss');
    labelChoice = label;
    $('#dashboardrange span').html(label);
  });
$('.header-daterange-picker').on('cancel.daterangepicker', function (ev, picker) {
  $(this).val('');
});
$('.header-daterange-picker').on('apply.daterangepicker',
  function (ev, picker) {
    label_date = picker.chosenLabel;
    console.log(label_date);
    base_url = baseURL;
    startdate = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
    enddate = picker.endDate.format('YYYY-MM-DD HH:mm:ss');
    switch (label_date) {
      case "Hari Ini":
        new_url = base_url + "?date=today"
        window.location = new_url;
        break;
      case "Kemarin":
        new_url = base_url + "?date=yesterday"
        window.location = new_url;
        break;
      case "Minggu Berjalan":
        new_url = base_url + "?date=week_to_date"
        window.location = new_url;
        break;
      case "Minggu Lalu":
        new_url = base_url + "?date=last_week"
        window.location = new_url;
        break;
      case "Bulan Berjalan":
        new_url = base_url + "?date=month_to_date"
        window.location = new_url;
        break;
      case "Bulan Lalu":
        new_url = base_url + "?date=last_month"
        window.location = new_url;
        break;
      case "Pilih Tanggal":
        new_url = base_url + "?date=custom&startdate=" + startdate + "&enddate=" + enddate
        window.location = new_url;
        break;
    }

  });

$("#companyBranch").change(function () {
  new_url = this.value + dateUrlExtra
  window.location = new_url;
});
$("option[data-pk='"+ outlet_pk +"']").prop('selected',true);
$("#companyBranch").attr(
    'title', $("option[data-pk='"+ outlet_pk +"']").text());

$("#detailView").change(function() {
    new_url = this.value + dateUrlExtra
    window.location = new_url;
});
$("option[data-pk='"+ detail +"']").prop('selected', true);
$("#detailView").attr('title', $("option[data-pk='"+ detail +"']").text());
  