var $filterCheckboxes = $('.custom-visibility .form-check input[type="checkbox"]');
var filterFunc = function() {
 var selectedFilters = {};
 $filterCheckboxes.filter(':checked').each(function() {
   if (!selectedFilters.hasOwnProperty(this.name)) {
     selectedFilters[this.name] = [];
   }
   selectedFilters[this.name].push(this.value);
 });

 var $filteredResults = $('.receipt');

 $.each($filterCheckboxes, function(name, filterValues) {
	var $filterCheckboxCategory = $filterCheckboxes.filter(':checked');
    if ( $filterCheckboxCategory.length === 0 ) {
    $filteredResults = [];
   }
  });

 $.each(selectedFilters, function(name, filterValues) {
   $filteredResults = $filteredResults.filter(function() {
     var matched = false,
       currentFilterValues = $(this).data('category').split(' ');

     $.each(currentFilterValues, function(_, currentFilterValue) {

       if ($.inArray(currentFilterValue, filterValues) != -1) {
         matched = true;
         return false;
       }
     });

     return matched;
   });
 });
 $('.receipt').hide().filter($filteredResults).show();
}
$filterCheckboxes.on('change', filterFunc);
