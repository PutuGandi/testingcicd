const path = require("path");

const columns = [
  { title: 'Name', prop: 'name', type:'text', fieldType:'string' },
  { title: 'Pax', prop: 'pax', type:'select', fieldType:'integer',
    data: [
      { value: 1, label: '1 Pax'},
      { value: 2, label: '2 Paxes'},
      { value: 3, label: '3 Paxes'},
      { value: 4, label: '4 Paxes'},
      { value: 5, label: '5 Paxes'},
      { value: 6, label: '6 Paxes'},
    ] 
  },
  { title: 'Shape', prop: 'shape', type:'select', fieldType:'integer',
    data: [
      { value: 1, label: 'Square'},
      { value: 2, label: 'Circle'},
      { value: 3, label: 'Rectangle'},
    ]
  }
];
