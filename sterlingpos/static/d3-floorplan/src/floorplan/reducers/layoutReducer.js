import * as types from '../constants/ActionTypes';

export function layoutReducer(
  state = {
    outlet: {
      id: -1,
      name: '',
    },
    table_group: {
      id: -1,
      name: '',
      status: 0,
      layout_hash: '',
      obj_parse: []
    },
    tables: [],
    UIState: {
      uploaded: 0,
      updated: -1
    }
  }, action
) {
  switch (action.type) {
    case types.GET_LAYOUT_DETAIL:
      return {
        ...state,
        outlet: {
          ...state.outlet,
          id: action.payload.outlet,
          name: action.payload.outlet_name,
        },
        table_group: {
          ...state.table_group,
          id: action.payload.id,
          name: action.payload.name,
          active: action.payload.active,
          layout_image: action.payload.layout_image,
          // obj_parse: Object.keys(action.payload.table_group.obj_parse).length === 0 ? [] : 
          //   action.payload.table_group.obj_parse
        },
        tables: action.payload.table_set.slice()
      }
      case types.UPLOAD_IMAGE:
        return {
          ...state,
          UIState: {
            ...state.UIState,
            uploaded: action.payload
          }
        }
      case types.ALERT_LAYOUT:
        return {
          ...state,
          UIState: {
            ...state.UIState,
            updated: action.payload,        
          }
        }
  }
  return state;
}