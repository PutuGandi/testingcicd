import '../constants/ActionTypes';
import { GET_TABLE, FILTER_TABLE, ADD_TABLE, DELETE_TABLE, EDIT_TABLE, CLEAR_TABLE_DETAIL, CHANGE_TABLE_DATA, ADD_TABLES  } from '../constants/ActionTypes';

export function tableReducer (
  state = {
    data: [],
    columns: []
  },
  action
) {
  
  switch(action.type) {
    case GET_TABLE:
      return {
        ...action.payload
      }
    case CHANGE_TABLE_DATA:
      return {
        ...state,
        data: action.payload.slice()
      }
    case FILTER_TABLE: {
      return {
        ...action.payload
      }
    }
    case ADD_TABLE: {
      return {
        ...state,
        data: [
          ...state.data,
          {
            id: action.payload.id,
            name: action.payload.name,
            pax: action.payload.pax,
            shape: action.payload.shape
          }
        ]
      }
    }
    case ADD_TABLES: {
      return {
        ...state,
        data: [
          ...state.data,
          ...action.payload
        ]
      }
    }
    case DELETE_TABLE: {
      return {
        ...state,
        data: state.data.filter((el, idx) => {
          let ret = true;
          action.payload.forEach(element => {
            if (element.id === el.id) {
              ret = ret & false;
            }
          });

          return ret;
        }).slice()
      }
    }
    case EDIT_TABLE: {
      return {
        ...state,
        data: state.data.map((el, idx) => {
          if (el.id === action.payload.id) {
            return {
              id: action.payload.id,
              name: action.payload.name,
              pax: action.payload.pax,
              shape: action.payload.shape
            }
          }
          return el;
        }).slice()
      }
    }
    case CLEAR_TABLE_DETAIL: {
      return {
        ...state,
        data: [],
      }
    }
    default:
      return state;
  }
}