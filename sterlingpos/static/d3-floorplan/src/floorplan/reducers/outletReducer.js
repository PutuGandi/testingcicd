import '../constants/ActionTypes';
import * as types from '../constants/ActionTypes';

export function outletReducer(
  state = [],
  action
) {
  switch (action.type) {
    case types.GET_OUTLET:
      return [
        ...action.payload
      ]
    default:
      return state
  }
}