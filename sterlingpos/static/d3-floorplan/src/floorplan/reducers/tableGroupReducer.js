import '../constants/ActionTypes';
import * as types from '../constants/ActionTypes';

export function tableGroupReducer(
  state = [],
  action
) {
  switch (action.type) {
    case types.GET_TABLE_GROUP:
      return [
        ...action.payload
      ]
    case types.UPDATE_TABLEGROUP:
      return state.map((el, idx) => {
        if (el.id === action.payload.id) {
          return {
            id: action.payload.id,
            name: action.payload.name,
            outlet: action.payload.outlet,
            active: action.payload.active,
            layout_image: action.payload.layout_image
          }
        }
        return el;
      }).slice();
    case types.ADD_TABLEGROUP:
      return [
        ...state,
        {
          id: action.payload.id,
          name: action.payload.name,
          outlet: action.payload.outlet,
          status: action.payload.active,
        }
      ]
    default:
      return state
  }
}