import * as types from '../constants/ActionTypes';

export function UIStateReducer(
  state = {
    table: {
      create_table_group: 0,
      input_data: [],
      table_group: {
        id: -1,
        name: '',
        outlet: -1,
        active: false,
        layout_id: -1,
      },
      outlet: {
        id: -1,
        name: ''
      },
      is_tg_name_error: false,
      is_tg_outlet_error: false,
      is_update_init: true,
      checkbox_list: {},
      saved: -1,
      deleted: -1
    },
    table_group: {
      edited: 1
    },
    error: null,
    prefix: ''
  },
  action
) {
  switch (action.type) {
    case types.ERROR_HANDLER:
      return {
        ...state,
        error: action.payload === null ? null : {
          code: action.payload.code,
          message: action.payload.message,
          action: action.payload.actionName
        }
      }
    case types.CREATE_TABLE_GROUP:
      return {
        ...state,
        table: {
          ...state.table,
          create_table_group: action.payload,
        }
      }
    case types.ADD_INLINE_INPUT:
      return {
        ...state,
        table: {
          ...state.table,
          input_data: action.payload.slice()
        }
      }
    case types.REMOVE_INLINE_INPUT:
      return {
        ...state,
        table: {
          ...state.table,
          input_data: state.table.input_data.filter((el, id)=>{
            return el.id !== action.payload
          }).slice()
        }
      }
    case types.EDIT_INLINE_INPUT:
      return {
        ...state,
        table: {
          ...state.table,
          input_data: state.table.input_data.map((el, idx)=>{
            if (el.id === action.payload.id) {
              return {
                ...state.table.input_data[idx],
                [action.payload.field]: action.payload.value
              }
            }
            return el;
          }).slice()
        }
      }
    case types.APPEND_TABLE_INPUT:
      return {
        ...state,
        table: {
          ...state.table,
          input_data:[
            ...state.table.input_data,
            {
              index: action.payload.index,
              name: action.payload.name,
              pax: action.payload.pax,
              shape: action.payload.shape
            }
          ]
        }
      }
    case types.CHANGE_TABLE_GROUP_VALUE:
      return {
        ...state,
        table: {
          ...state.table,
          table_group: {
            ...state.table.table_group,
            [action.payload.field]: action.payload.value
          }
        }
      }
    case types.SET_TABLE_GROUP_VALUE:
      return {
        ...state,
        table: {
          ...state.table,
          table_group: action.payload
        }
      }
    case types.TOGGLE_CHECKBOX:
      return {
        ...state,
        table: {
          ...state.table,
          checkbox_list: {
            ...state.table.checkbox_list,
            [action.payload]: state.table.checkbox_list[action.payload] === undefined ? true : !state.table.checkbox_list[action.payload]
          }
        }
      }
    case types.TOGGLE_CHECKBOX_ALL:
      return {
        ...state,
        table: {
          ...state.table,
          checkbox_list: action.payload
        }
      }
    case types.ALERT_TABLE:
      return {
        ...state,
        table: {
          ...state.table,
          saved: action.payload
        }
      }
    case types.EDIT_TABLE_GROUP:
      return {
        ...state,
        table_group: {
          ...state.table_group,
          edited: action.payload
        }
      }
    case types.ADD_CHECKBOX:
      return {
        ...state,
        table: {
          ...state.table,
          checkbox_list: {
            ...state.table.checkbox_list,
            ...action.payload
          }
        }
      }
    case types.SET_ERROR_TG_NAME:
      return {
        ...state,
        table: {
          ...state.table,
          is_tg_name_error: action.payload
        }
      }
    case types.SET_ERROR_TG_OUTLET:
      return {
        ...state,
        table: {
          ...state.table,
          is_tg_outlet_error: action.payload
        }
      }
    case types.ALERT_DELETE:
      return {
        ...state,
        table: {
          ...state.table,
          deleted: action.payload
        }
      }
    case types.CHANGE_TABLE_GROUP_LAYOUT:
      return {
        ...state,
        table: {
          ...state.table,
          outlet: action.payload
        }
      }
    case types.CHANGE_UPDATE_INIT:
      return {
        ...state,
        table: {
          ...state.table,
          is_update_init: action.payload
        }
      }
    default:
      return state;
  }
}