import axios from 'axios';
import { webConfig } from '../../../settings';
import * as types from "../constants/ActionTypes";

const columns = [
  { title: 'Name', prop: 'name', type:'text', fieldType:'string' },
  { title: 'Pax', prop: 'pax', type:'select', fieldType:'integer',
    data: [
      { value: 1, label: '1 Pax'},
      { value: 2, label: '2 Paxes'},
      { value: 3, label: '3 Paxes'},
      { value: 4, label: '4 Paxes'},
      { value: 5, label: '5 Paxes'},
      { value: 6, label: '6 Paxes'},
      { value: 7, label: '7 Paxes'},
      { value: 8, label: '8 Paxes'},
      { value: 9, label: '9 Paxes'},
      { value: 10, label: '10 Paxes'},
      { value: 11, label: '11 Paxes'},
      { value: 12, label: '12 Paxes'},
      { value: 13, label: '13 Paxes'},
      { value: 14, label: '14 Paxes'},
      { value: 15, label: '15 Paxes'},
      { value: 16, label: '16 Paxes'},
      { value: 17, label: '17 Pax'},
      { value: 18, label: '18 Paxes'},
      { value: 19, label: '19 Paxes'},
      { value: 20, label: '20 Paxes'},
      { value: 21, label: '21 Paxes'},
      { value: 22, label: '22 Paxes'},
      { value: 23, label: '23 Paxes'},
      { value: 24, label: '24 Paxes'},
      { value: 25, label: '25 Paxes'},
      { value: 26, label: '26 Paxes'},
      { value: 27, label: '27 Paxes'},
      { value: 28, label: '28 Paxes'},
      { value: 29, label: '29 Paxes'},
      { value: 30, label: '30 Paxes'},
      { value: 31, label: '31 Paxes'},
      { value: 32, label: '32 Paxes'},
    ] 
  },
  { title: 'Shape', prop: 'shape', type:'select', fieldType:'string',
    data: [
      { value: 'square', label: 'Square'},
      { value: 'circle', label: 'Circle'},
      { value: 'rectangle', label: 'Rectangle'},
    ]
  }
];

const header = {
  'X-CSRFToken': document.getElementsByName("csrfmiddlewaretoken")[0].value,
}

export function filterTable(type, val, field, tg_id) {
  return async function(dispatch) {
    const value = val === '' ? '%20' : escape(val),
          fieldObj = field === '' ? '%20' : field,
          http = webConfig.protocol;
    
    let URL = '';
    if (val === '') {
      URL = `${http}//${webConfig.http.host}/api/v2/table-area/${tg_id}/table/`;
    }else {
      URL = `${http}//${webConfig.http.host}/api/v2/table-area/${tg_id}/table/?${fieldObj}=${value}`
    }

    try {
      const response = await axios.get(URL, { headers: header });
      return Promise.all([
        dispatch({
          type: types.FILTER_TABLE,
          payload: {
            data: response.data.results,
            columns: columns
          }
        }),
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        })
      ])
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'filterTable',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    }
  }
}

export function addTable(tableData, tgID) {
  return async function(dispatch) {
    let tableItems = [],
        chkObjRet = {};
    const tableInserted = await Promise.all(JSON.parse(tableData).map(async item => {
      const http = webConfig.protocol,
            URL = `${http}//${webConfig.http.host}/api/v2/table/create/`;
      const data = {
        ...item,
        table_area: tgID
      }
      const response = await axios.post(URL, data, {headers: header});
      if (response.status === 200) {
        tableItems.push(response.data)
      }
      return response.data;
    }));

    return Promise.all(tableInserted).then(async result => {
      const tables = await getTable(tgID);
      tables.data.forEach((el, i) => {
        chkObjRet = {
          ...chkObjRet,
          [`chk${el.id}`]: false
        }
      });
      return Promise.all([
        dispatch({
          payload: [],
          type: types.ADD_INLINE_INPUT,
        }),
        dispatch({
          type: types.ADD_TABLES,
          payload: tableInserted
        }),
        dispatch({
          type: types.TOGGLE_CHECKBOX_ALL,
          payload: chkObjRet  
        }),
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        }),
        dispatch({
          type: types.CHANGE_UPDATE_INIT,
          payload: true
        })
      ])
    });
  }
}

export function saveTableGroup(tgItem, tblData, isCreated) {
  return async function(dispatch) {
    const http = webConfig.protocol;

    if (isCreated) {
      const data = {
        ...tgItem,
        table_list: tblData
      }
      try {
        const URL = `${http}//${webConfig.http.host}/api/v2/table-area/create/`,
              response = await axios.post(URL, data, { headers: header });
        if (response.status === 201) {
          const tgID = response.data.id,
                tables = await getTable(tgID);
          
          const path = window.location.pathname.replace('create', tgID),
                _url = `${window.location.origin}${path}`;

          history.pushState({id: tgID}, 'Edit', `${_url}#/edit/${tgID}`)
          
          return Promise.all([
            dispatch({
              type: types.ERROR_HANDLER,
              payload: null
            }),
            dispatch({
              type: types.ADD_TABLEGROUP,
              payload: response.data   
            }),
            dispatch({
              type: types.CREATE_TABLE_GROUP,
              payload: 0              
            }),
            dispatch({
              payload: [],
              type: types.ADD_INLINE_INPUT,
            }),
            dispatch({
              type: types.CHANGE_TABLE_DATA,
              payload: tables.data
            }),
            dispatch({
              type: types.SET_TABLE_GROUP_VALUE,
              payload: {
                id: response.data.id,
                name: response.data.name,
                outlet: response.data.outlet,
                active: response.data.active,
              }
            }),
            dispatch({
              type: types.CHANGE_UPDATE_INIT,
              payload: true
            })
          ]);
        }else {
          throw new Error('Something bad happening!');
        }
      }catch (err) {
        return dispatch({
          type: types.ERROR_HANDLER,
          payload: {
            actionName: 'saveTableGroup',
            message: err.response.statusText,
            code: err.response.status
          }
        });
      }
    }else {
      try {
        const URL = `${http}//${webConfig.http.host}/api/v2/table-area/${tgItem.id}/update/`,
              response = await axios.put(URL, tgItem, { headers: header });

        if (response.status === 200) {
          return Promise.all([
            dispatch({
              type: types.UPDATE_TABLEGROUP,
              payload: response.data 
            }),
            dispatch({
              payload: 0,
              type: types.CREATE_TABLE_GROUP
            }),
            dispatch({
              type: types.ERROR_HANDLER,
              payload: null
            }),
            dispatch({
              type: types.CHANGE_UPDATE_INIT,
              payload: true
            })
          ])
        }else {
          throw new Error('Something bad happening!');
        }
      }catch (err) {
        return dispatch({
          type: types.ERROR_HANDLER,
          payload: {
            actionName: 'saveTableGroup',
            message: err.response.statusText,
            code: err.response.status
          }
        });
      }
    }
  }
}

export function editTable(data, ID) {
  return async function(dispatch) {
    try {
      const http = webConfig.protocol,
            URL = `${http}//${webConfig.http.host}/api/v2/table/${ID}/update/`;
      const response = await axios.patch(URL, {...data, id: ID}, {headers: header});

      if (response.status === 200) {
        window.location = location.href;
        window.location.reload();
      }else {
        throw new Error('Something bad happening!');
      }
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'editTable',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    }
  }
}

export function deleteTable(strID, checkboxData) {
  return async function(dispatch) {
    let tableItems = [],
        chkObjRet = {};
    const tableDeleted = await Promise.all(JSON.parse(strID).map(async item => {
      const http = webConfig.protocol,
            URL = `${http}//${webConfig.http.host}/api/v2/table/${item.id}/delete/`;
      
      const response = await axios.delete(URL, {headers: header});
      if (response.status === 204) {
        tableItems.push(response.data)
      }
      return item;
    }));

    return Promise.all(tableDeleted).then(result => {
      window.location = location.href;
      window.location.reload();
      const lenCheckbox = Object.keys(checkboxData).length,
            itemsCheckbox = Object.keys(checkboxData);
      let chkObjRet = {};
      for (let i=0; i<lenCheckbox; i++) {
        const key = itemsCheckbox[i];
        let appended = true;
        tableDeleted.forEach((el, i) => {
          const keyDeleted = `chk${el.id}`;
          if (keyDeleted === key) {
            appended = appended & false;
          }
        });
        if (appended) {
          chkObjRet = {
            ...chkObjRet,
            [key]: false
          }
        }  
      }
      return Promise.all([
        dispatch({
          type: types.DELETE_TABLE,
          payload: tableDeleted.slice()
        }),
        dispatch({
          type: types.TOGGLE_CHECKBOX_ALL,
          payload: chkObjRet  
        }),
      ])
    });
  }
}

export function deleteTableGroup(ID) {
  return async dispatch => {
    const http = webConfig.protocol,
          URL = `${http}//${webConfig.http.host}/api/v2/table-area/${ID}/delete/`;
    try {
      const response = await axios.delete(URL, {headers: header});
      if (response.status === 204) {
        // reload data
        window.location = '/';
        //getFirstLoadData(dispatch, false);
      }
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'deleteTableGroup',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    } 
  }
}

export function getOutletData() {
  return async function(dispatch) {
    try {
      const outlet = await getAllOutlet();
      return Promise.all([
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        }),
        dispatch({
          type: types.GET_OUTLET,
          payload: outlet
        })
      ])
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'getOutletData',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    }
  }  
}

async function getAllOutlet() {
  const http = webConfig.protocol,
        URL = `${http}//${webConfig.http.host}/api/v2/outlet/`;
  
  const response = await axios.get(URL, { headers: header });
  return response.data.results;
}

async function getTableGroup(ID) {
  const http = webConfig.protocol,
        URL = `${http}//${webConfig.http.host}/api/v2/outlet/${ID}/table-area/`,
        response = await axios.get(URL, { headers: header });
        
  return response.data.results;
}

async function getTableGroupByID(ID) {
  const http = webConfig.protocol,
          URL = `${http}//${webConfig.http.host}/api/v2/table-area/${ID}/layout/`,
          response = await axios.get(URL, { headers: header });
  return [{
    id: response.data.id,
    name: response.data.name,
    active: response.data.active,
    layout_image: response.data.layout_image,
    outlet: response.data.outlet,
  }];
}

async function getTable(ID) {
  const http = webConfig.protocol,
        URL = `${http}//${webConfig.http.host}/api/v2/table-area/${ID}/table/`,
        response = await axios.get(URL, { headers: header });
  return {
    columns: columns,
    data: response.data.results,
  };
}

async function getFirstLoadData(dispatch, isShownAlert, tgID) {
  try {
    const outlet = await getAllOutlet();
    let tableGroup = [],
        table = [],
        UITableGroup = {
          id: -1,
          name: '',
          outlet: -1,
          active: true,
          layout_id: -1,
          suffix: ''
        },
        UIOutlet = {
          id: -1,
          name: ''
        },
        objData = {};
    if (outlet.length > 0) {
      if (tgID !== undefined) {
        tableGroup = await getTableGroupByID(tgID);
        if (tableGroup.length > 0) {
          table = await getTable(tgID);
          let outletID = -1;
          tableGroup.forEach(el => {
            if (el.id == tgID) {
              UITableGroup = {
                id: el.id,
                name: el.name,
                outlet: el.outlet,
                active: el.active,
                suffix: el.suffix 
              }
              outletID = el.outlet;
            }
          });
          outlet.forEach(el => {
            if (el.id === outletID) {
              UIOutlet = {
                id: el.id,
                name: el.name
              }
            }
          });
          if (table.data.length > 0) {
            table.data.forEach((el, i) =>{
              objData = {
                ...objData,
                [`chk${el.id}`]: false
              }
            });
          }
        }
        let ret = [
          dispatch({
            type: types.ERROR_HANDLER,
            payload: null
          }),
          dispatch({
            type: types.GET_OUTLET,
            payload: outlet
          }),
          dispatch({
            type: types.GET_TABLE_GROUP,
            payload: tableGroup
          }),
          dispatch({
            type: types.SET_TABLE_GROUP_VALUE,
            payload: UITableGroup
          }),
          dispatch({
            type: types.CHANGE_TABLE_GROUP_LAYOUT,
            payload: UIOutlet
          }),
          dispatch({
            type: types.GET_TABLE,
            payload: {
              data: table.data === undefined ? [] : table.data,
              columns: columns
            }
          }),
          dispatch({
            type: types.TOGGLE_CHECKBOX_ALL,
            payload: objData  
          }),
        ];
        if (isShownAlert) {
          ret = [
            ...ret,
            dispatch({
              type: types.ALERT_DELETE,
              payload: 1
            }),
          ]
        }

        return Promise.all(ret);
      }else {
        let ret = [
          dispatch({
            type: types.ERROR_HANDLER,
            payload: null
          }),
          dispatch({
            type: types.GET_OUTLET,
            payload: outlet
          }),
          dispatch({
            type: types.CREATE_TABLE_GROUP,
            payload: 1
          }),
          dispatch({
            type: types.ADD_INLINE_INPUT,
            payload: []
          }),
          dispatch({
            type: types.SET_TABLE_GROUP_VALUE,
            payload: UITableGroup
          }),
          dispatch({
            type: types.CHANGE_TABLE_GROUP_LAYOUT,
            payload: UIOutlet
          }),
          dispatch({
            type: types.GET_TABLE,
            payload: {
              data: [],
              columns: columns
            }
          }),
          dispatch({
            type: types.TOGGLE_CHECKBOX_ALL,
            payload: objData  
          }),
        ];
        if (isShownAlert) {
          ret = [
            ...ret,
            dispatch({
              type: types.ALERT_DELETE,
              payload: 1
            }),
          ]
        }
        return Promise.all(ret);
      }
    }
  }catch (err) {
    return dispatch({
      type: types.ERROR_HANDLER,
      payload: {
        actionName: 'getFirstLoadData',
        message: err.response.statusText,
        code: err.response.status
      }
    });
  }
}

export function getInitData(tgID) {
  return async function(dispatch) {
    if (tgID !== undefined) {
      getFirstLoadData(dispatch, false, tgID);
    }else {
      getFirstLoadData(dispatch, false);
    }
  }  
}

export function changeTableGroup(outletID) {
  return async function(dispatch) {
    try {
      let table = {},
          UITableGroup = {};
      const tableGroup = await getTableGroup(outletID);
      if (tableGroup.length > 0) {
        table = await getTable(tableGroup[0].id);
        UITableGroup = {
          id: tableGroup[0].id,
          name: tableGroup[0].name,
          outlet_id: tableGroup[0].outlet,
          active: tableGroup[0].active,
          suffix: tableGroup[0].suffix
        }
      }else {  
        table = await getTable(-1);
      }
      return Promise.all([
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        }),
        dispatch({
          type: types.GET_OUTLET,
          payload: outlet
        }),
        dispatch({
          type: types.GET_OUTLET,
          payload: outlet
        }),
        dispatch({
          payload: 0,
          type: types.CREATE_TABLE_GROUP
        }),
        dispatch({
          payload: [],
          type: types.ADD_INLINE_INPUT,
        }),
        dispatch({
          type: types.GET_TABLE_GROUP,
          payload: tableGroup
        }),
        dispatch({
          type: types.SET_TABLE_GROUP_VALUE,
          payload: UITableGroup
        }),
        dispatch({
          type: types.GET_TABLE,
          payload: {
            data: table.data,
            columns: table.columns
          }
        }),
      ]);
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'changeTableGroup',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    }
  }  
}

export function createNewTableGroup(outletID) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        payload: 1,
        type: types.CREATE_TABLE_GROUP
      }),
      dispatch({
        payload: [],
        type: types.ADD_INLINE_INPUT,
      }),
      dispatch({
        type: types.SET_TABLE_GROUP_VALUE,
        payload: {
          id: -1,
          name: '',
          outlet: outletID,
          active: true
        }
      }),
    ])
    
}

export function addInlineInput(tableData, tgID) {
  return dispatch => {
    return Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        payload: tableData,
        type: types.ADD_INLINE_INPUT,
      })
    ]);
  }
}

export function removeInlineInput(index) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.REMOVE_INLINE_INPUT,
        payload: index
      })
    ])
}

export function setErrorTGName(status) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.SET_ERROR_TG_NAME,
        payload: status
      })
    ])
}

export function setErrorTGOutlet(status) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.SET_ERROR_TG_OUTLET,
        payload: status
      })
    ])
}

export function changeInlineInput(id, field, value) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.SET_ERROR_TG_NAME,
        payload: status
      }),
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
    ]);
}

export function changeTableGroupValue(field, value) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.CHANGE_TABLE_GROUP_VALUE,
        payload: {
          field: field,
          value: value
        }
      })    
    ])
}

export function setTableGroupValue(objValue) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.SET_TABLE_GROUP_VALUE,
        payload: objValue
      })
    ]);
}

export function toggleCheckBox(ID) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.TOGGLE_CHECKBOX,
        payload: ID
      })
    ])
}

export function toggleCheckBoxAll(objData) {
  return dispatch => 
    Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.TOGGLE_CHECKBOX_ALL,
        payload: objData  
      })
    ])
}

/**
 * action for Layout Container
 */

export function getTableData(tgID) {
  return async function(dispatch) {
    const http = webConfig.protocol,
          URL = `${http}//${webConfig.http.host}/api/v2/table-area/${tgID}/table/`;
    try {
      const response = await axios.get(URL, {headers: header});
      return Promise.all([
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        }),
        dispatch({
          type: types.GET_TABLE,
          payload: {
            data: response.data.results,
            columns: columns
          }
        })
      ])
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'getTableData',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    }
  }
}

export function uploadImage(status, initStatus) {
  return dispatch => {
    let ret = [
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.UPLOAD_IMAGE,
        payload: status
      })
    ];

    if (initStatus) {
      ret = [
        ...ret,
        dispatch({
          type: types.CHANGE_UPDATE_INIT,
          payload: true
        })
      ]
    }
    return Promise.all(ret);
  }
}

export function getLayoutDetail(tgID) {
  return async dispatch => {
    const http = webConfig.protocol,
          URL = `${http}//${webConfig.http.host}/api/v2/table-area/${tgID}/layout/`;
    try {
      const response = await axios.get(URL, {headers: header});
      if (response.data.layout_image !== null) { 
        return Promise.all([
          dispatch({
            type: types.ERROR_HANDLER,
            payload: null
          }),
          dispatch({
            type: types.GET_LAYOUT_DETAIL,
            payload: response.data
          }),
          dispatch({
            type: types.UPLOAD_IMAGE,
            payload: 1
          }),
        ]);
      }else {
        return Promise.all([
          dispatch({
            type: types.GET_LAYOUT_DETAIL,
            payload: response.data
          }),
        ])
      }
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'getLayoutDetail',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    }      
  }
}

export function saveLayout(id, formData, objParse, objReset) {
  return async function(dispatch) {
    const http = webConfig.protocol,
          URL = `${http}//${webConfig.http.host}/api/v2/table-area/${id}/update/`;
    if (formData !== undefined || formData !== null) {
      try {
        const response = await axios.patch(URL, formData, {
          headers: {
            'content-type': 'application/json',
            ...header
          }
        });

        if (response.status === 200) {
          const tableUpdated = await Promise.all(objParse.map(async item => {
            const http = webConfig.protocol,
                  URL = `${http}//${webConfig.http.host}/api/v2/table/${item.id}/update/`;
            const data = {
              ...item,
              table_area: id
            }
            const response = await axios.put(URL, data, {headers: header});
            return response.data;
          }));

          const tableReset = await Promise.all(objReset.map(async item => {
            const http = webConfig.protocol,
                  URL = `${http}//${webConfig.http.host}/api/v2/table/${item.id}/update/`;
            const data = {
              ...item,
              id: item.id,
              location_x: -1,
              location_y: -1,
              table_area: id
            }
            const response = await axios.put(URL, data, {headers: header});
            return response.data;
          }));

          window.location = location.href;
          window.location.reload();
          return Promise.all([
            tableUpdated,
            tableReset,
            dispatch({
              type: types.ERROR_HANDLER,
              payload: null
            }),
          ]);
        }else {
          throw new Error('Something bad happening!');
        }
      }catch (err) {
        return dispatch({
          type: types.ERROR_HANDLER,
          payload: {
            actionName: 'saveLayout',
            message: err.response.statusText,
            code: err.response.status
          }
        });
      }
    }else {
      try {
        const tableUpdated = await Promise.all(objParse.map(async item => {
          const http = webConfig.protocol,
                URL = `${http}//${webConfig.http.host}/api/v2/table/${item.id}/update/`;
          const data = {
            ...item,
            table_area: id
          }
          const response = await axios.put(URL, data, {headers: header});
          return response.data;
        }));
        
        const tableReset = await Promise.all(objReset.map(async item => {
          const http = webConfig.protocol,
                URL = `${http}//${webConfig.http.host}/api/v2/table/${item.id}/update/`;
          const data = {
            ...item,
            id: item.id,
            location_x: -1,
            location_y: -1,
            table_area: id
          }
          const response = await axios.put(URL, data, {headers: header});
          return response.data;
        }));

        window.location = location.href;
        window.location.reload();
        
        return Promise.all([
          tableUpdated,
          tableReset,
          dispatch({
            type: types.ERROR_HANDLER,
            payload: null
          }),
        ]);
      }catch (err) {
        return dispatch({
          type: types.ERROR_HANDLER,
          payload: {
            actionName: 'saveLayout',
            message: err.response.statusText,
            code: err.response.status
          }
        });
      }
    }
  }
}

export function changeAlertStatus(type, value) {
  if (type === 'LAYOUT') {
    return dispatch => {
      Promise.all([
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        }),
        dispatch({
          type: types.ALERT_LAYOUT,
          payload: value
        })
      ])
    }
  }else if (type === 'TABLE') {
    return dispatch => {
      Promise.all([
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        }),
        dispatch({
          type: types.ALERT_TABLE,
          payload: value
        })
      ])
    }
  }
}

export function nextTableGroup(tgObj) {
  return async dispatch => {
    try {
      let objData = {},
          table = await getTable(tgObj.id);
      if (table.data.length > 0) {
        table.data.forEach((el, i) =>{
          objData = {
            ...objData,
            [`chk${el.id}`]: false
          }
        });
      }
      return Promise.all([
        dispatch({
          type: types.ERROR_HANDLER,
          payload: null
        }),
        dispatch({
          type: types.SET_TABLE_GROUP_VALUE,
          payload: tgObj
        }),
        dispatch({
          payload: [],
          type: types.ADD_INLINE_INPUT,
        }),
        dispatch({
          type: types.TOGGLE_CHECKBOX_ALL,
          payload: objData  
        }),
        dispatch({
          type: types.SET_ERROR_TG_NAME,
          payload: false
        })
      ])
    }catch (err) {
      return dispatch({
        type: types.ERROR_HANDLER,
        payload: {
          actionName: 'nextTableGroup',
          message: err.response.statusText,
          code: err.response.status
        }
      });
    }
  }
}

export function editTableGroup(edited) {
  return dispatch => {
    return Promise.all([
      dispatch({
        type: types.ERROR_HANDLER,
        payload: null
      }),
      dispatch({
        type: types.EDIT_TABLE_GROUP,
        payload: edited
      }),
      dispatch({
        type: types.ALERT_TABLE,
        payload: -1
      })
    ])
  }
}

export function changeTGOutlet(outlet) {
  return dispatch => {
    return dispatch({
      type: types.CHANGE_TABLE_GROUP_LAYOUT,
      payload: outlet
    })
  }
}

export function changeUpdateInit(value) {
  return dispatch => {
    return dispatch({
      type: types.CHANGE_UPDATE_INIT,
      payload: value
    })
  }
}