import React from 'react';

class TableAreaList extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <table>
        <thead>
          <tr>
            <th>
              <input type="checkbox" />
            </th>
            <th className="text-left" scope="col">Name
              <input className="form-control" placeholder="Input the Name" type="text" />
            </th>
            <th className="text-left" scope="col">Pax
              <select className="form-control">
                  <option value="">- SELECT -</option>
                  <option value="1">1 Pax</option>
                  <option value="2">2 Paxes</option>
                  <option value="3">3 Paxes</option>
                  <option value="4">4 Paxes</option>
                  <option value="5">5 Paxes</option>
                  <option value="6">6 Paxes</option>
                  <option value="7">7 Paxes</option>
                  <option value="8">8 Paxes</option>
                  <option value="9">9 Paxes</option>
                  <option value="10">10 Paxes</option>
                  <option value="11">11 Paxes</option>
                  <option value="12">12 Paxes</option>
                  <option value="13">13 Paxes</option>
                  <option value="14">14 Paxes</option>
                  <option value="15">15 Paxes</option>
                  <option value="16">16 Paxes</option>
              </select>
            </th>
            <th className="text-left" scope="col">Shape
              <select className="form-control">
                  <option value="">- SELECT -</option>
                  <option value="square">Square</option>
                  <option value="circle">Circle</option>
                  <option value="rectangle">Rectangle</option>
              </select>
            </th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <input type="checkbox" className="checkbox-table" />
            </td>
            <td>
              <input className="form-control d-none" type="text" value="#11" />
              <div className="table-label" val="#11">#11</div>
            </td>
            <td>
              <select className="form-control d-none">
                  <option value="1">1 Pax</option>
                  <option value="2">2 Paxes</option>
                  <option value="3">3 Paxes</option>
                  <option value="4" selected="">4 Paxes</option>
                  <option value="5">5 Paxes</option>
                  <option value="6">6 Paxes</option>
                  <option value="7">7 Paxes</option>
                  <option value="8">8 Paxes</option>
                  <option value="9">9 Paxes</option>
                  <option value="10">10 Paxes</option>
                  <option value="11">11 Paxes</option>
                  <option value="12">12 Paxes</option>
                  <option value="13">13 Paxes</option>
                  <option value="14">14 Paxes</option>
                  <option value="15">15 Paxes</option>
                  <option value="16">16 Paxes</option>
              </select>
              <div className="table-label" val="4">4 Paxes</div>
            </td>
            <td>
                <select className="form-control d-none">
                    <option value="square" selected="">Square</option>
                    <option value="circle">Circle</option>
                    <option value="rectangle">Rectangle</option>
                </select>
                <div className="table-label" val="square">Square</div>
            </td>
            <td className="action">
                <a className="nav-link float-right" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: rgb(152, 154, 156); cursor: pointer;">
                    <svg aria-hidden="true" data-prefix="fas" data-icon="ellipsis-v" class="svg-inline--fa fa-ellipsis-v fa-w-6 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512">
                        <path fill="currentColor" d="M96 184c39.8 0 72 32.2 72 72s-32.2 72-72 72-72-32.2-72-72 32.2-72 72-72zM24 80c0 39.8 32.2 72 72 72s72-32.2 72-72S135.8 8 96 8 24 40.2 24 80zm0 352c0 39.8 32.2 72 72 72s72-32.2 72-72-32.2-72-72-72-72 32.2-72 72z"></path>
                    </svg>
                </a>
                <div className="inline-action d-none">
                    <button type="button" className="btn btn-link">Cancel</button>
                    <button type="button" className="btn btn-primary">Save</button>
                </div>
                <div className="dropdown-menu">
                  <a className="dropdown-item" data-toggle="modal" data-tr-id="tr100" data-target="#deleteConfirmation" href="#" style="min-width: 0px;">Delete</a>
                  <a className="dropdown-item" href="#" style="min-width: 0px;">Edit</a>
                </div>
            </td>
          </tr>
        </tbody>
      </table>
    )
  }
}

export default TableAreaList;