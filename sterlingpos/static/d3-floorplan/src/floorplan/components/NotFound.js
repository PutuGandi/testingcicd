import React from 'react';
import imageNotFound from '../../public/img/error-404.svg';

class NotFound extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="not-found text-center">
        <h1>Not Found</h1>
        <img src="/static/images/error-404.svg" className="image-not-found"/>
      </div>
    )
  }
}

export default NotFound;