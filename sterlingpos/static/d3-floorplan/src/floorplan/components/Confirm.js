import React from 'react';

class Confirm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="modal fade" id={ this.props.confirmID } tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">{ this.props.title }</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>{ this.props.body }</p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">{ this.props.button.close }</button>
              <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={ this.props.handleActionButton }>{ this.props.button.action }</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Confirm;