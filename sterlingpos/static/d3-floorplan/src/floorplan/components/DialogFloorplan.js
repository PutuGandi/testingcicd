import React from 'react';

const DialogFloorplan = ({ dialogID }) => {
  return (
    <div className="modal fade" id={dialogID} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Table Properties</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <form>
              <div className="form-group">
                <label htmlFor="tableName">Table Name</label>
                <input type="input" className="form-control" id="tableName" aria-describedby="tableName" placeholder="Enter Table Name" />
                <small id="tableNameHelp" className="form-text text-muted">Enter the name of table such as Lotus</small>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div className="form-group">
                    <label htmlFor="tableWidth">Top Chairs</label>
                    <input type="input" className="form-control" id="tableTopChairs" aria-describedby="tableTopChairs" placeholder="Enter amount of top chairs" />
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <label htmlFor="tableDimension">Right Chairs</label>
                    <input type="input" className="form-control" id="tableRightChairs" aria-describedby="tableRightChairs" placeholder="Enter amount of right chairs" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div className="form-group">
                    <label htmlFor="tableWidth">Bottom Chairs</label>
                    <input type="input" className="form-control" id="tableBottomChairs" aria-describedby="tableBottomChairs" placeholder="Enter amount of bottom chairs" />
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <label htmlFor="tableDimension">Left Chairs</label>
                    <input type="input" className="form-control" id="tableLeftChairs" aria-describedby="tableLeftChairs" placeholder="Enter amount of left chairs" />
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary" id="btn-apply">Apply</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DialogFloorplan;