import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import Confirm from '../components/Confirm';
import DialogAddTable from '../components/DialogAddTable';
import { inlineInputField, makeHeader } from './TableFunc';
library.add(faEllipsisV);

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.init = false;
    this.trID = -1;    
    this.type = 'DELETE';
    this.quickButton = null;
    this.checkBoxAllVisibility = true;
    this.checkBoxAllState = false;
    this.colLen = 0;
    this.numRow = 0;
    this.selectValue = {}
    this.table = [];
    this.autoFocusID = -1;
    this.checkbox = [];
    this.dialogAddTable = "addTable";
    this.prefix = '';
    this.toggleAll = this.toggleAll.bind(this);
    this.toggleRow = this.toggleRow.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleModalShow = this.handleModalShow.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleAddTable = this.handleAddTable.bind(this);
    this.handleSaveAdd = this.handleSaveAdd.bind(this);
    this.handleChangeInputField = this.handleChangeInputField.bind(this);
    this.handleClickInputField = this.handleClickInputField.bind(this);
    this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    const comp = document.getElementById('main');
    this.prefix = comp.getAttribute('data-prefix');
  }

  remove(id) {
    this.table = this.table.filter((el, idx) => {
      return el.value.id !== id
    })
    this.props.handleNewRowInput(this.table);
  }

  handleChangeInputField(id, field, value) {
    this.table[id] = {
      ...this.table[id],
      value: {
        ...this.table[id].value,
        [field]: value.toString() === "" ? "" : !isNaN(value) ? parseInt(value) : value
      },
    };
  }

  handleClickInputField(id, error) {
    const that = this;
    if (error) {
      this.autoFocusID = id;
      this.table = this.table.map((el, i)=>{
        if (id === el.value.id) {
          that.autoFocusID = id;
          return {
            ...el,
            attributes: {
              ...el.attributes,
              name: {
                ...el.attributes.name,
                error: false,
                err_msg: ""
              }
            }
          }
        }
        return el;      
      });
      this.props.handleNewRowInput(this.table);
    }
  }

  toggleAll() {
    const checkboxList = this.props.UIState.table.checkbox_list,
          checkBoxLen = Object.keys(checkboxList).length;
    let objCheckBox = Object.keys(checkboxList),
        checkboxData = {};
    this.checkBoxAllState = !this.checkBoxAllState;
    for (let i=0; i<checkBoxLen; i++) {
      objCheckBox = Object.keys(checkboxList)[i];
      checkboxData = {
        ...checkboxData,
        [objCheckBox]: this.checkBoxAllState
      }
    }
    this.props.handleCheckboxAll(checkboxData);
  }

  toggleRow(chkIndex) {
    this.props.handleCheckBox(chkIndex);
  }

  handleDeleteData() {
    this.props.handleDeleteData(JSON.stringify([{id: this.trID}]));
  }

  handleModalShow(index, type) {  
    this.trID = index;
    this.type = type;
  }
  
  handleAddTable(objData) {
    this.numRow++;
    this.autoFocusID = -1;

    for (let i = objData.startFrom; i < (objData.startFrom + objData.amountTable); i++) {
      this.table = [
        ...this.table,
        {
          value: {
            id: i,
            name: `${i}`,
            val: i,
            suffix: objData.suffix,
            pax: 1,
            shape: 'square'
          },
          attributes: {
            name: {
              required: true,
              error: false,
              err_msg: ''
            },
            pax: {
              required: false,
              error: false,
              err_msg: ''
            },
            shape: {
              required: false,
              error: false,
              err_msg: ''
            }
          }
        }
      ];
    }
    this.props.handleNewRowInput(this.table);
  }

  handleEdit(index) {
    const len = this.props.columns.length;
    for (let i=0; i<len; i++) {
      this.refs[`tr${index}_td${this.props.columns[i].prop}_label`].className = "table-label d-none";
      this.refs[`tr${index}_td${this.props.columns[i].prop}_input`].className = "form-control";
      this.refs[`tr${index}_tdsuffix_input`].className = "form-control";
      this.refs[`tr${index}_tdprefix_input`].className = "form-control";

      const data = this.refs[`tr${index}_td${this.props.columns[i].prop}_label`];
      this.refs[`tr${index}_td${this.props.columns[i].prop}_input`].value = data.attributes['val'].value;
    }
    this.refs[`actionMore${index}`].className = "nav-link d-none";
    this.refs[`actionInline${index}`].className = "inline-action"; 
  }

  handleSaveAdd() {
    let ret = true,
        error = false,
        errMsg = "";
    this.autoFocusID = -1;
    this.table = this.table.map((el, i)=>{
      if (el.attributes.name.required) {
        if (el.value.name.toString() === "") {
          error = true;
          errMsg = "This value is required";
          ret &= false;
        }else {
          error = false;
          errMsg = "";
          ret &= true;
        }
      }
      return {
        ...el,
        attributes: {
          ...el.attributes,
          name: {
            ...el.attributes.name,
            error: error,
            err_msg: errMsg
          }
        }
      }
    });
    
    if (ret) {
      const tableData = this.table.map((el, i) => {
        return {
          id: el.value.id,
          name: el.value.val,
          pax: el.value.pax,
          shape: el.value.shape,
          suffix: el.value.suffix
        };
      });
      this.props.handleInlineSaveData(tableData, this.props.UIState.table.table_group.id, 'ADD');
    }else {
      this.props.handleNewRowInput(this.table);
    }
  }

  handleInlineSave(index){
    const that = this;
    this.props.handleInlineSaveData({
      name: that.refs[`tr${index}_tdname_input`].value,
      pax: parseInt(that.refs[`tr${index}_tdpax_input`].value),
      shape: that.refs[`tr${index}_tdshape_input`].value,
      suffix: that.refs[`tr${index}_tdsuffix_input`].value
    }, index, 'EDIT');
    
    const len = this.props.columns.length;
    for (let i=0; i<len; i++) {
      const columns = this.props.columns;
      const data = this.refs[`tr${index}_td${columns[i].prop}_input`];
      if (columns[i].type === 'select') {
        let dataLabel = '';
        columns[i].data.forEach(itm => {
          if (itm.value.toString() === data.value.toString()) {
            dataLabel = itm.label;
          }
        });
        this.refs[`tr${index}_td${columns[i].prop}_label`].textContent = dataLabel;
      }else if (columns[i].type === 'text') {
        this.refs[`tr${index}_td${columns[i].prop}_label`].textContent = data.value;
      }
    }
    this.refs[`actionMore${index}`].className = "nav-link float-right";
    this.refs[`actionInline${index}`].className = "inline-action d-none";
  }

  handleCancelEdit(index) {
    const len = this.props.columns.length;
    for (let i=0; i<len; i++) {
      this.refs[`tr${index}_td${this.props.columns[i].prop}_input`].className = "form-control d-none";
      this.refs[`tr${index}_td${this.props.columns[i].prop}_label`].className = "table-label";

      this.refs[`tr${index}_tdsuffix_input`].className = "form-control d-none";
      this.refs[`tr${index}_tdprefix_input`].className = "form-control d-none";
    }
    this.refs[`actionMore${index}`].className = "nav-link float-right";
    this.refs[`actionInline${index}`].className = "inline-action d-none";
  }

  render() {
    const that = this,
          header = makeHeader(that);

    const InlineInputRow = (props) => {
      const actionBulkAdd = (remove, save, rowAmout, props, item) => {
        const btnRemove = (
          <a className="btn"
            onClick={ () => props.remove(item.value.id) } ><i className="fa fa-close fa-1x"></i>
          </a>
        );
        const btnSave = (
          <button type="button" className="btn btn-primary btn-sm" 
            onClick={ () => that.handleSaveAdd() } >{ rowAmout >1 ? 'Save All' : 'Save' }
          </button>
        );

        return (
          <td>
            <div className="inline-action float-right">
              { save ? btnSave : '' }
              { remove ? btnRemove : '' }
            </div>
          </td>
        )
      }

      const rowInput = props.data.map((item, i) => {
          const isRemove = true,
              row_amount = props.data.length,
              isSave = i === row_amount - 1 && that.props.UIState.table.create_table_group === 0 ? true : false;
        
        return (
          <tr key={`row_${i}`}>
            <td>&nbsp;</td>
            { inlineInputField(i, item, that, that.prefix) }
            { actionBulkAdd(isRemove, isSave, row_amount, props, item) }
          </tr>  
        )
      });
      
      return rowInput;
    }

    const data = this.props.data.map(function(item, index){
      that.colLen = that.props.columns.length;
      const tdData = that.props.columns.map(function(element, idx){
        let input = null;
        if (element.type === 'text') {
          input = <div>
              <input ref={`tr${item.id}_tdprefix_input`} 
                className="form-control d-none" type="text" 
                style={{width: '60px', float: 'left'}}
                disabled={true}
                defaultValue={ that.prefix } />
              <input ref={`tr${item.id}_td${element.prop}_input`} 
                className="form-control d-none" type="text" 
                style={{width: '60px', float: 'left'}}
                disabled={true}
                defaultValue={ item[element.prop] } />
              <input ref={`tr${item.id}_tdsuffix_input`} 
                className="form-control d-none" type="text"
                style={{float: 'left', width: 'calc(100% - 120px)'}} 
                defaultValue={ item.suffix } />
            </div>
        }else if (element.type === 'select') {
          const option = element.data.map(function(itm, i){
            return <option key={`opt${i}`} value={ itm.value }>{ itm.label }</option>            
          });
          input = <select ref={`tr${item.id}_td${element.prop}_input`} 
                    defaultValue={item[element.prop]}
                    className="form-control d-none">
                    { option }
                  </select>
        }
        let strVal = '';
        if (element.type === 'select') {
          const data = element.data;
          data.forEach(itm => {
            if (itm.value == item[element.prop]) {
              strVal = itm.label;
            }
          });
        }else if (element.type === 'text') {
          strVal = `${that.prefix}${item[element.prop]}${item.suffix}`;
        }

        return (
          <td key={`tr${item.id}_td${element.prop}`}>
            { input }
            <div ref={`tr${item.id}_td${element.prop}_label`} 
              className="table-label" val={ item[element.prop] }>
              { strVal }
            </div>
          </td>
        )
      });

      if (!that.init) {
        const checkboxData = that.checkbox;
        that.checkbox = {
          ...checkboxData,
          [`chk${item.id}`]: false
        }
      }

      if (index === that.props.data.length - 1) {
        that.init = true;
      }

      return (
        <tr ref={`tr${item.id}`} key={`tr${item.id}`}>
          <td className="text-center">
            <input 
              type="checkbox"
              ref="chkAll"
              onChange={() => that.toggleRow(`chk${item.id}`)}
              className="checkbox-table"
              checked={ 
                that.props.UIState.table.checkbox_list[`chk${item.id}`] === undefined ? false : 
                that.props.UIState.table.checkbox_list[`chk${item.id}`] 
              } 
            />
          </td>
          { tdData }
          <td className="action">
            <a 
              ref={`actionMore${item.id}`}
              className="nav-link float-right" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" 
              aria-expanded="false" style={{color:'#989A9C', cursor:'pointer'}}>
              <FontAwesomeIcon icon="ellipsis-v" />
            </a>
            <div ref={`actionInline${item.id}`} className="inline-action d-none">
              <button type="button" className="btn btn-link" 
                onClick={ () => that.handleCancelEdit(item.id) } >Cancel</button>
              <button type="button" className="btn btn-primary" 
                onClick={ () => that.handleInlineSave(item.id) }>Save</button>
            </div>
            <div className="dropdown-menu">
              <a 
                className="dropdown-item" 
                data-toggle="modal" 
                data-tr-id={`tr${item.id}`} 
                onClick={ () => that.handleModalShow(item.id, 'DELETE') } 
                data-target={ `#${that.props.deleteConf}` } 
                style={{minWidth: 0}} href="#">
                Delete
              </a>
              <a 
                className="dropdown-item" 
                style={{minWidth: 0}}
                onClick={ () => that.handleEdit(item.id) }
                href="javascript:void(0)">
                Edit
              </a>
            </div>
          </td>
        </tr>
      )
    });

    if (data.length === 0) {
      that.checkBoxAllVisibility = false;
            
    }else {
      if (that.props.UIState.table.create_table_group === 1) {
        that.checkBoxAllVisibility = false;
      }
      else {
        that.checkBoxAllVisibility = true;
      }
    }

    that.quickButton = <a className="btn" 
      data-toggle="modal" 
      data-target={ `#${this.dialogAddTable}` } 
      style={{color: '#1294C5', cursor:'pointer'}} 
        >+ Add a table</a>;
    
    if (Object.keys(that.props.UIState.table.checkbox_list).length === 0) {
      that.checkBoxAllState = false;
    }

    return(
      <div className="row card">
        <div className={ this.props.disabled === 1 ? 'col-sm-12 disabled-div' : 'col-sm-12'}>
          <table className="table">
            <thead>
              <tr>
                <th>
                  <input type="checkbox" onChange={ this.toggleAll } 
                    style={ that.checkBoxAllVisibility ? {visibility: 'visible'} : {visibility: 'hidden'}}
                    checked={ that.checkBoxAllState }
                    onClick={ this.props.handleShowDeleteAllTable } 
                  />
                </th>
                { header }
                <th style={{ width: '164px'}}>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              { this.props.UIState.table.create_table_group === 0 ? data : <tr></tr> }
              { <InlineInputRow data={ this.props.UIState.table.input_data } remove={ this.remove } /> }
              {
                <tr ref={`quickAddTable`}>
                  <td className="text-center quick-add-table" colSpan="5">
                    { that.quickButton }
                  </td>
                </tr> 
              }
            </tbody>
          </table>
          <Confirm 
            title="Delete Confirmation!"
            button={{close: "Close", action: "Delete"}}
            body="Are you sure to delete this item?"
            handleActionButton={ () => this.handleDeleteData() }
            confirmID={ this.props.deleteConf }
          />
          <DialogAddTable
            dialogID={ this.dialogAddTable }
            title="Dialog Add Table"
            pushTableData={ (data) => this.handleAddTable(data) }
          />
        </div>
      </div>
    )
  }
}

export default Table;
