import React from 'react';

class Alert extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={ `alert alert-${this.props.category} alert-dismissible fade show` } role="alert">
        { this.props.message }
        <button onClick={ this.props.handleClose } type="button" className="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    )
  }
}

export default Alert;