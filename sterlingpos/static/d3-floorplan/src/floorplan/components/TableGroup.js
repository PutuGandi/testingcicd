import React from 'react';
import { Link } from 'react-router-dom';
import Confirm from '../components/Confirm';

class TableGroup extends React.Component {
  constructor(props) {
    super(props);
    this.tableGroupID = -1;
    this.tableGroupName = '';
    this.tableGroupOutlet = '';
    this.tableGroupStatus = true;
    this.tableGroupLayout = '';
    this.prevTableGroupName = '';
    this.prevTableGroupOutlet = '';
    this.prevTableGroupStatus = false;
    this.next = true;
    this.prev = false;
    this.init = false;
    this.status = 1;
    this.tgIndex = 0;
    this.tgLen = 0;
    this.changeStatus = this.changeStatus.bind(this);
    this.saveData = this.saveData.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleNextTableGroup = this.handleNextTableGroup.bind(this);
    this.handlePrevTableGroup = this.handlePrevTableGroup.bind(this);
    this.handleOutletChange = this.handleOutletChange.bind(this);
  }

  handleInputChange(event, field) {
    let value = event.target.value;
    if (value !== "") {
      if (!isNaN(value)) {
        value = parseInt(value);
      }
    }
    this.props.handleChangeTableGroupValue(field, value);
  }

  handleOutletChange(event) {
    const id = event.target.value,
          name = event.target.options[event.target.selectedIndex].text;
    if (id !== "") {
      const outlet = {
        id: parseInt(id),
        name: name
      }
      this.props.changeTGOutlet(outlet);
      this.props.handleChangeTableGroupValue('outlet', parseInt(id));
      this.tableGroupOutlet = parseInt(id);
      this.saveData();
    }
  }

  handleNextTableGroup() {
    this.tgIndex++;
    const tgObj = this.props.tableGroup[this.tgIndex];
    this.props.handleNextTableGroup(tgObj);
  }

  handlePrevTableGroup() {
    this.tgIndex--;
    const tgObj = this.props.tableGroup[this.tgIndex];
    this.props.handleNextTableGroup(tgObj);
  }

  changeStatus(status) {
    this.tableGroupStatus = status;
    this.props.handleChangeTableGroupValue('active', status);
    this.saveData();
  }

  saveData() {
    if (this.props.UIState.table.create_table_group === 1) {
      if (
        this.tableGroupName !== '' &&
        this.tableGroupOutlet !== -1
      ) {
        this.props.setErrorTGName(false);
        this.props.saveTableGroup({
          id: this.tableGroupID,
          name: this.tableGroupName,
          outlet: this.tableGroupOutlet,
          active: this.tableGroupStatus,
        }, [], true);
      }else if (this.tableGroupName === '') {
        this.props.setErrorTGName(true);
      }
    }else {
      if (this.props.UIState.table_group.edited === 0) {
        this.props.handleEditTableGroup(1)
      }else {
        if (this.tableGroupName === '' || this.tableGroupOutlet === '') {
          this.props.setErrorTGName(true)
        }else {
          if (
            this.prevTableGroupName !== this.tableGroupName ||
            this.prevTableGroupOutlet !== this.tableGroupOutlet ||
            this.prevTableGroupStatus !== this.tableGroupStatus
          ) {
            this.prevTableGroupName = this.tableGroupName;
            this.prevTableGroupOutlet = this.tableGroupOutlet;
            this.prevTableGroupStatus = this.tableGroupStatus;
            this.props.saveTableGroup({
              id: this.tableGroupID,
              name: this.tableGroupName,
              outlet: this.tableGroupOutlet,
              active: this.tableGroupStatus,
            });
          }
        }
      }
    }
  }

  componentDidUpdate() {
    this.tgLen = this.props.tableGroup.length;
  }
  
  render () {
    let optionOutlet = this.props.outlet.map((element, index) => {
      return <option key={`opt${index+1}`} value={ element.id }>{ element.name }</option>;
    });
    
    optionOutlet.unshift(<option key={`opt0`} value=''>- SELECT -</option>)

    const activeTGID = this.props.UIState.table.table_group.id;
    if (
      activeTGID !== undefined ||
      activeTGID !== -1
    ) {
      const that = this;
      this.props.tableGroup.forEach((el, i) => {
        if (el.id == activeTGID) {
          that.tgIndex = i;
        }
      });
    }
    this.tableGroupID = this.props.UIState.table.table_group.id;
    this.tableGroupName = this.props.UIState.table.table_group.name;
    this.tableGroupOutlet = this.props.UIState.table.table_group.outlet;
    if (!this.init && this.tableGroupName !== '') {
      this.tableGroupStatus = this.props.UIState.table.table_group.active;
      this.prevTableGroupOutlet = this.tableGroupOutlet;
      this.prevTableGroupName = this.tableGroupName;
      this.prevTableGroupStatus = this.tableGroupStatus;
      this.prevTableGroupSuffix = this.tableGroupSuffix;
      this.init = true;
    }
    
    const isCreatedTG = this.props.UIState.table.create_table_group,
          isEditedTG = this.props.UIState.table_group.edited,
          isNameError = this.props.UIState.table.is_tg_name_error;
    return (
      <div className={ `card ${this.props.disabled === 1 ? 'disabled-div' : ''}` }>
        <div className="card-body">
          <form>
            <div className="row">
              <div className="col-md-4">
                <div className="form-group">
                  <label htmlFor="name">Table Group Name</label>
                  <input ref="name" type="text" id="name" autoFocus
                    readOnly={ isCreatedTG === 1 || isEditedTG === 1 ? false : true }      
                    className={
                      `form-control ${isCreatedTG === 1 || isEditedTG === 1 ? 
                      '' : 'disabled-div'} ${isNameError ? 'parsley-error' : ''}` 
                    }
                    onChange={ event => this.handleInputChange(event, 'name') }
                    onBlur={ this.saveData }
                    value={ this.props.UIState.table.table_group.name } />
                  { isNameError ? 
                    <ul className="parsley-errors-list filled" 
                      onClick={ () => { this.props.setErrorTGName(false); this.refs['name'].focus();  } }>
                      <li className="parsley-required">This value is required</li>
                    </ul> : ''
                  }
                </div>    
              </div>
              <div className="col-md-4">
                <div className="form-group">
                  <label htmlFor="outlet">Outlet</label>
                  <select ref="outlet" id="outlet"
                    readOnly={ isCreatedTG === 1 || isEditedTG === 1 ? false : true } 
                    className={
                      `form-control ${isCreatedTG === 1 || isEditedTG === 1 ? 
                      '' : 'disabled-div'}` 
                    }
                    onChange={ this.handleOutletChange }
                    value={ this.props.UIState.table.outlet.id } placeholder="Select an outlet...">
                    { optionOutlet }
                  </select>
                </div>
              </div>
              
              <div className="col-md-4">
                <div className="form-group">
                  <label htmlFor="status">Status</label>
                  <div id="status" role="group" aria-label="Status" 
                    className={`btn-group display-inline ${isCreatedTG === 1 || isEditedTG === 1 ? 
                      '' : 'disabled-div'}`}>
                    <button ref="active" onClick={ status => this.changeStatus(true) } type="button" 
                      className={`btn btn-radius ${this.props.UIState.table.table_group.active ? 'btn-success' : ''}`} 
                      style={{width: '50%'}}>Active</button>
                    <button ref="disable" onClick={ status => this.changeStatus(false) } type="button" 
                      className={`btn btn-radius ${!this.props.UIState.table.table_group.active ? 'btn-success' : ''}`} 
                      style={{width: '50%'}}>Disable</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default TableGroup;