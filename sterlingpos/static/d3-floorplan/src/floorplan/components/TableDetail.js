import React from 'react';
import Table from './Table';
import Layout from './Layout';

class TableDetail extends React.Component {
  constructor(props) {
    super(props);
  }

  render(){
    return(
      <div className={this.props.UIState.table.create_table_group === 1 ? 'nav-pill-wrapper disabled-div' : 'nav-pill-wrapper'}>
        <ul className="nav nav-pills" role="tablist" id="nav-tab">
          <li className="nav-item">
            <a className="nav-link active" id="table-tab" data-toggle="tab" href="#table" role="tab" 
              aria-controls="table" aria-selected="true">Table</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" id="layout-tab" data-toggle="tab" href="#layout" role="tab" 
              aria-controls="layout" aria-selected="false">Layout</a>
          </li>
        </ul>
        <div className="tab-content">
          <div className="tab-pane fade show active" id="table" role="tabpanel" aria-labelledby="table-tab">
            <div className="table-container">
              <Table
                ref="table"
                UIState= { this.props.UIState }
                disabled={ this.props.disabled }
                columns={ this.props.columns}
                data={ this.props.data}
                handleFilterTable={ this.props.handleFilterTable }
                handleInlineSaveData={ this.props.handleInlineSaveData }
                handleDeleteData={ this.props.handleDeleteData }
                handleNewRowInput={ this.props.handleNewRowInput }
                handleRemoveRowInput= { this.props.handleRemoveRowInput }
                handleChangeRowInput= { this.props.handleChangeRowInput }
                handleCheckBox= { this.props.handleCheckBox }
                handleCheckboxAll = { this.props.handleCheckboxAll }
                handleShowDeleteAllTable={ this.props.handleShowDeleteAllTable }
                deleteConf="deleteConfirmation"
              />
            </div>
          </div>
          <div className="tab-pane fade" id="layout" role="tabpanel" aria-labelledby="layout-tab">
            <div className="card-body">
              <Layout
                tgID={ this.props.tgID }
                outlet={ this.props.outlet }
                tableGroup={ this.props.tableGroup }
                tables={ this.props.data }
                UIState={ this.props.UIState }
                layout={ this.props.layout }
                getLayoutDetail={ this.props.getLayoutDetail }
                getTableData={ this.props.getTableData }
                uploadImage={ this.props.uploadImage }
                saveLayout={ this.props.saveLayout }
                changeUpdateInit={ this.props.changeUpdateInit }
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TableDetail;