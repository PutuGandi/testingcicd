import React from 'react';

class DialogAddTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startFrom: 1,
      amountTable: 1,
      suffix: ''
    }
    this.handleAddTable = this.handleAddTable.bind(this);
    this.handleChangeSuffix = this.handleChangeSuffix.bind(this);
  }

  handleAddTable() {
    const that = this;
    this.props.pushTableData({
      startFrom: parseInt(that.state.startFrom),
      amountTable: parseInt(that.state.amountTable),
      suffix: that.state.suffix
    });
  }

  handleChangeSuffix(event) {
    this.setState({suffix: event.target.value })
  }

  render() {
    return(
      <div id={ this.props.dialogID } className="modal" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{ this.props.title }</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label htmlFor="tableSuffix">Suffix</label>
                  <input type="input" onChange={ event => this.handleChangeSuffix(event) } className="form-control" 
                    id="tableSuffix" aria-describedby="tableSuffix" placeholder="Enter Suffix Name" />
                  <small className="form-text text-muted">Enter suffix name such as LG, G etc</small>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="tableWidth">Start From</label>
                      <input type="input" pattern="[0-9]*" 
                        onChange={event => this.setState({startFrom: event.target.value.replace(/\D/,'')})} 
                        value={this.state.startFrom} className="form-control" 
                        placeholder="Enter start number of table" />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="tableDimension">Amount of Tables</label>
                      <input type="input" pattern="[0-9]*" 
                        onChange={event => this.setState({amountTable: event.target.value.replace(/\D/,'')})} 
                        value={this.state.amountTable} className="form-control" 
                        placeholder="Enter amount of tables" />
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" onClick={ () => this.handleAddTable() } className="btn btn-primary" data-dismiss="modal">Add Table</button>
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DialogAddTable;