import React from 'react';

class NoTable extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    $('#add-table').on('click', function (e) {
      $('#nav-tab a[href="#table"]').tab('show');
    })
  }

  render(){
    return (
      <div className="no-table-data-container text-center"
        style={this.props.display == 1 ? {} : {display:'none'}}>
        <img src="/static/images/no-table.svg" className="no-table-image" />
        <h3>No Table Available</h3>
        <button className="btn btn-primary" id="add-table">Add table</button>
      </div>
    )
  }
}

export default NoTable;