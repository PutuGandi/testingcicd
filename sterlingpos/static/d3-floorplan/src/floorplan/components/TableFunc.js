import React from 'react';

export const inlineInputField = (index, data, that, prefix) => {
  const inlineInput = that.props.columns.map((item, idx) => {
    const inputAutoFocus = 
        <div>
          <input 
            type="text"
            required
            autoFocus
            disabled={ true }
            style={{ width: '60px', float: 'left' }}
            className="form-control"
            onChange={ (event) => that.handleChangeInputField(index, item.prop, event.target.value) }
            defaultValue={ prefix }
          />
          <input 
            type="text"
            required
            autoFocus
            disabled={ true }
            style={{ width: '60px', float: 'left' }}
            className="form-control"
            defaultValue={ data.value.name }
          />
          <input 
            type="text"
            required
            autoFocus
            style={{ width: 'calc(100% - 120px)', float: 'left' }}
            className="form-control"
            onChange={ (event) => that.handleChangeInputField(index, 'suffix', event.target.value) }
            defaultValue={ data.value.suffix }
          />
        </div>;
    const inputNonAutoFocus = 
        <div>
          <input 
            type="text"
            required
            autoFocus
            disabled={ true }
            style={{ width: '60px', float: 'left' }}
            className="form-control"
            onChange={ (event) => that.handleChangeInputField(index, item.prop, event.target.value) }
            defaultValue={ prefix }
          />
          <input 
            type="text"
            required
            autoFocus
            disabled={ true }
            style={{ width: '60px', float: 'left' }}
            className="form-control"
            defaultValue={ data.value.name }
          />
          <input 
            type="text"
            required
            style={{ width: 'calc(100% - 120px)', float: 'left' }}
            className="form-control"
            onChange={ (event) => that.handleChangeInputField(index, 'suffix', event.target.value) }
            defaultValue={ data.value.suffix }
          />
        </div>;
    if (item.type === 'text') {
      return (
        <td key={`td_input_${index}_${idx}`}>
          { that.autoFocusID === data.value.id ? inputAutoFocus : inputNonAutoFocus }
          { data.attributes[item.prop].error ? 
            <ul className="parsley-errors-list filled"
              onClick={ idx => that.handleClickInputField(data.value.id, data.attributes[item.prop].error) }>
              <li className="parsley-required">{data.attributes[item.prop].err_msg }</li>
            </ul> : ''
          }
        </td>
      )
    }else if (item.type === 'select') {
      const options = item.data.map((itm, i) => {
        return <option key={`opt${i}`} value={ itm.value }>{ itm.label }</option>            
      });
      that.selectValue[`input_${item.prop}_${index}`] = item[item.prop];

      return (
        <td key={`td_input_${idx}`}>
          <select 
            defaultValue={ data.value[item.prop] }
            onChange={ (event) => that.handleChangeInputField(index, item.prop, event.target.value) }
            className="form-control">
            { options }
          </select>
        </td>
      )
    }
  });

  return inlineInput;
}

export const makeHeader = that => {
  return that.props.columns.map(function(item, idx){
    let input = '';
    if (item.type === 'text') {
      input = <input className="form-control" 
                placeholder={ `Input the ${item.title}` } type="text" 
                onChange={ event => that.props.handleFilterTable(item.prop, event.target.value) } />
    }else if (item.type === 'select') {
      let option = item.data.map(function(itm, i){
        return <option key={`opt${i}`} value={ itm.value }>{ itm.label }</option>;           
      });
      const defaultSelect = <option key={`opt${-1}`} value="">- SELECT -</option>;
      option = [
        defaultSelect,
        ...option
      ];
      input = <select ref={`tr${item.id}_th${item.prop}_input`} 
                className="form-control" 
                onChange={ event => that.props.handleFilterTable(item.prop, event.target.value) }>
                { option }
              </select>;
    }
    return (
      <th className="text-left" scope="col" key={`th${idx}`}>
        { item.title }
        { input }
      </th>
    )
  });
}

