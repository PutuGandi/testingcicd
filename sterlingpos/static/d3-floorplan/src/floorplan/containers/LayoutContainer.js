import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, Redirect } from 'react-router-dom';
import { webConfig, FLOORPLAN_PATH } from '../../../settings';
import {
  getTableData,
  uploadImage,
  getLayoutDetail,
  saveLayout
} from '../actions/FloorplanActions';
import {
  SHAPE_SQUARE,
  SHAPE_CIRCLE,
  SHAPE_RECTANGLE
 } from '../constants/ActionTypes';

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.initScript = this.initScript.bind(this);
    this.handleSaveLayout = this.handleSaveLayout.bind( this);
    this.processJsonObject = this.processJsonObject.bind(this);
    this.leftConstant = 921;
    this.topConstant = 147;
    this.jsonObj = [];
    this.hasClicked = {};
    this.leftMaxBorder = 828;
    this.leftMinBorder = 0;
    this.topMaxBorder = 458;
    this.topMinBorder = 0;
  }

  componentWillMount() {
    this.props.getLayoutDetail(this.props.match.params.tgID);
  }

  handleSaveLayout() {
    const imageFile = document.querySelector('#file-upload');
    const that = this;
    const tableResetObj = this.props.tables.filter(el => {
      let isResetObj = true;
      that.jsonObj.forEach(element => {
        if (el.id === element.id) {
          isResetObj = isResetObj & false;
        }
      });
      return isResetObj;
    });

    const tableObj = this.jsonObj.map(el => {
      return {
        ...el,
        table_area: parseInt(that.props.match.params.tgID)
      }
    });
    this.props.saveLayout(this.props.match.params.tgID, imageFile, tableObj, tableResetObj); 
  }

  processJsonObject(tgObj, position, size) {
    let isModified = false,
        ret = {},
        dimension = {};    
    const dropItems = this.jsonObj.map((el, i) => {
      if (el.id === parseInt(tgObj.id)) {
        isModified = true;
        if (position !== null) {
          ret = {
            ...ret,
            location_x: parseInt(position.left),
            location_y: parseInt(position.top)  
          }
        }
        if (size !== null) {
          ret = {
            ...ret,
            height: size.height,
            width: size.width
          }
        }
        return {
          ...el,
          ...ret
        }
      }
      return el;
    });  
    if (size === null) {
      if (tgObj.shape === SHAPE_SQUARE || tgObj.shape === SHAPE_CIRCLE) {
        dimension = {
          height: 40,
          width: 40
        }
      }else if (tgObj.shape === SHAPE_RECTANGLE) {
        dimension = {
          height: 40,
          width: 55
        }
      }
    }
    if (!isModified) {
      let jsonObjAppended = {
        id: parseInt(tgObj.id),
        name: tgObj.name,
        shape: tgObj.shape,
        pax: parseInt(tgObj.pax),        
        location_x: parseInt(position.left),
        location_y: parseInt(position.top),
      }
      if (size === null) {
        jsonObjAppended = {
          ...jsonObjAppended,
          ...dimension
        }
      }else {
        jsonObjAppended = {
          ...jsonObjAppended,
          ...size
        }
      }
      this.jsonObj.push(jsonObjAppended);
    }else {
      this.jsonObj = dropItems.slice();
    }
    
  }

  componentDidMount() {
    const that = this;
    setTimeout(function(){
      that.initScript();
    }, 1000);
  }

  initScript() {
    const that = this;
    
    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function initDroppable() {
      $( "#canvas-editor" ).droppable({
        accept: ".table-component-wrapper",
        drop: function( event, ui ) {
          that.processJsonObject({
            id : ui.draggable.attr('data-id'),
            name : ui.draggable.attr('data-name'),
            shape : ui.draggable.attr('data-shape'),
            pax : ui.draggable.attr('data-pax')
          }, {
            left: ui.position.left,
            top: ui.position.top
          }, null);
          invokeComponent();
        }
      });
    }

    function invokeResizable(id, name, shape, pax, context, aspectRatio) {
      $(`#table${id}`).resizable({
        handles: 'ne, se, sw, nw',
        aspectRatio: aspectRatio,
        resize: function(event, ui) {
          context.processJsonObject({
            id : id,
            name : name,
            type : shape,
            pax : pax
          }, {
            left: ui.position.left,
            top: ui.position.top,
          }, {
            width: parseInt(ui.size.width),
            height: parseInt(ui.size.height)
          });
        }
      });
    }

    function invokeDraggable(id) {
      if (id===null) {
        $(".table-component-wrapper").draggable({ 
          cursor: "move",
          containment: "#canvas-editor",
          drag: function() {
            let dataID = $(this).attr('data-id');
            if (that.hasClicked[dataID] === undefined) {
              $(this).css("position", "absolute");
              //$(this).css("margin", "0");
              that.hasClicked[dataID] = true;
            }
          }
        });
      }else {
        $(`#table${id}`).draggable({ 
          cursor: "move",
          containment: "#canvas-editor",
          drag: function() {
            let dataID = $(this).attr('data-id');
            if (that.hasClicked[dataID] === undefined) {
              $(this).css("position", "absolute");
              //$(this).css("margin", "0");
              that.hasClicked[dataID] = true;
            }
          }
        });
      }
    }

    function invokeComponent() {
      $(".table-component-wrapper").on('click', function() {
        const id = $(this).attr('data-id'),
              name = $(this).attr('data-name'),
              shape = $(this).attr('data-shape'),
              pax = parseInt($(this).attr('data-pax'));
        $('div.ui-resizable-handle').remove();
        $('div.close-bar').removeClass("active");
        $(this).children('.close-bar').addClass('active');
        let dimension = {};
        if (that.hasClicked[id] === undefined) {
          $(this).attr('id', `table${id}`);
          $(this).clone().appendTo('#canvas-editor');
          if (shape === SHAPE_SQUARE || shape === SHAPE_CIRCLE) {
            dimension = {
              height: 40,
              width: 40
            }
          }else if (shape === SHAPE_RECTANGLE) {
            dimension = {
              height: 40,
              width: 55
            }
          }
          const left = getRandomInt(that.leftMinBorder, that.leftMaxBorder),
                top = getRandomInt(that.topMinBorder, that.topMaxBorder);
          $(`#table${id}`).css("position", "absolute");
          $(`#table${id}`).css("left", `${left}px`);
          $(`#table${id}`).css("top", `${top}px`);

          that.processJsonObject({
            id : id,
            name : name,
            shape : shape,
            pax : pax
          }, {
            left: left,
            top: top
          }, dimension);
          const context = this;
          setTimeout(function(){
            $(context).remove();
          }, 100)
          
          that.hasClicked[id] = true;
        }

        if (shape === SHAPE_RECTANGLE) {
          invokeResizable(id, name, shape, pax, that, false);
        }else {
          invokeResizable(id, name, shape, pax, that, true);
        }
        
        $(`#table${id}`).removeClass('ui-square');
        $(`#table${id}`).removeClass('ui-rounded');
        $(`#table${id}`).removeClass('ui-rectangle');
        invokeDraggable(id);
      });

      $(".close-bar").on("click", function(){
        let dataID = $(this).parent().attr('data-id'),
            shape = $(this).parent().attr('data-shape');
        const newObj = that.jsonObj.filter(el => el.id !== parseInt(dataID));
        that.jsonObj = newObj.slice();
        if (that.hasClicked[dataID] !== undefined) {
          $(this).removeClass("active");
          $(this).parent().attr("style", "");
          $(this).parent().removeAttr("id");
          if (shape === SHAPE_SQUARE) {
            $(this).parent().addClass("ui-square");
          }else if (shape === SHAPE_RECTANGLE) {
            $(this).parent().addClass("ui-rectangle");
          }else if (shape === SHAPE_CIRCLE) {
            $(this).parent().addClass("ui-rounded");
          }          
          $('div.ui-resizable-handle').remove();
          $(this).parent().clone().appendTo('#table-toolbox-body');
          
          delete that.hasClicked[dataID];
          $(this).parent().remove();
          invokeComponent();
        }
       
      });
    }

    function handleUnfocusTable() {
      $(document).click(function(event) { 
        if (
          !$(event.target).closest('.table-component-wrapper').length
        ) {
          if (
            $('.table-component-wrapper').is(":visible")
          ) {
            $('div.ui-resizable-handle').remove();
            $('.close-bar').removeClass('active');
          }
        }        
      });
    }

    function setBackground() {
      const imageFile = document.querySelector('#file-upload');
      console.log(imageFile);
      if ( 
        that.props.table_group.layout_image !== null
      ) {
        const layoutImage = that.props.table_group.layout_image;
        $("#canvas-editor").css("background-image", `url(${layoutImage})`);
        $("#canvas-editor").css("background-repeat", "no-repeat");
        $("#canvas-editor").css("background-size", "auto 100%");
      }
    }

    $("#file-upload").on("change", function(){
      console.log('test test');
      var files = !!this.files ? this.files : [];
      if (!files.length || !window.FileReader) return;
      if (/^image/.test( files[0].type)){ 
        var ReaderObj = new FileReader(); 
        ReaderObj.readAsDataURL(files[0]);
        ReaderObj.onloadend = function() { 
          that.props.uploadImage(true);
          $("#canvas-editor").css("background-image", "url("+this.result+")");
          $("#canvas-editor").css("background-repeat", "no-repeat");
          $("#canvas-editor").css("background-size", "auto 100%");
        }
      }else{
        alert("Upload an image");
      }
    });

    //running functionality
    invokeComponent();
    //invokeDraggable();
    initDroppable();
    handleUnfocusTable();
    setBackground();
  }

  render() {
    const that = this;

    if (this.props.UIState.updated === 1) {
      return <Redirect to="/" />
    }
    
    let unlocatedTables = [],
        locatedTables = [];
    
    that.props.tables.forEach((el, i) => {
      let shapeType = '',
          styleProps = {};
      
      if (el.shape === SHAPE_SQUARE) {
        shapeType = 'ui-square';
      }else if (el.shape === SHAPE_CIRCLE) {
        shapeType = 'ui-rounded border-radius-50';
      }else {
        shapeType = 'ui-rectangle';
      }

      if (el.location_x > -1 && el.location_y > -1) {
        that.processJsonObject({
          id: el.id,
          name: el.name,
          shape : el.shape,
          pax : el.pax
        }, {
          left: el.location_x,
          top: el.location_y
        }, {
          height: el.height,
          width: el.width
        });
        styleProps = {
          position: 'absolute',
          left: `${el.location_x}px`,
          top: `${el.location_y}px`,
          height: `${el.height}px`,
          width: `${el.width}px`,
          background: '#0294C9',
          margin: '0'
        }
        that.hasClicked[el.id] = true;
        const data = <div id={`table${el.id}`} key={i} className={`ui-widget-content table-component-wrapper ${shapeType}`}
          data-toggle="tooltip" data-placement="bottom" data-html="true" 
          title={`Name : ${el.name} \nPax : ${el.pax}`}
          data-id={el.id} data-name={el.name} data-shape={el.shape} data-pax={el.pax} style={ styleProps }>
          <div className="close-bar">x</div>
          <div className="table-name">{ el.name }</div>
        </div>;
        locatedTables = [
          ...locatedTables,
          data
        ]
      }else {
        const data = <div id={`table${el.id}`} key={i} className={`ui-widget-content table-component-wrapper ${shapeType}`}
          data-toggle="tooltip" data-placement="bottom" data-html="true" 
          title={`Name : ${el.name} \nPax : ${el.pax}`}
          data-id={el.id} data-name={el.name} data-shape={el.shape} data-pax={el.pax} style={ styleProps }>
          <div className="close-bar">x</div>
          <div className="table-name">{ el.name }</div>
        </div>;
        unlocatedTables = [
          ...unlocatedTables,
          data
        ]
      }
    });
    
    return (
      <div className="table-container">
        <div className="form-group table-header" style={{ display: 'inline-block' }}>
          <div className="title">Layout Group</div>
        </div>
        <div className="info-bar row">
          <div className="form-group outlet row">
            <label htmlFor="inputPassword" className="col-sm-3 col-form-label">Outlet</label>
            <div className="col-sm-9">
              <input type="text" className="form-control" value={ this.props.outlet.name } readOnly="true" />
            </div>
          </div>
          <div className="form-group tg row">
            <label htmlFor="inputPassword" className="col-sm-3 col-form-label">Table Group</label>
            <div className="col-sm-9">
              <input type="text" className="form-control" value={ this.props.table_group.name } readOnly="true" />
            </div>
          </div>          
        </div>
        <div className="row">
          <div className="col-md-9 col-sm-12 canvas-editor-container">
            <div className="canvas-editor-wrapper card">
              <div id="canvas-editor" className="card-body">
                { locatedTables }
              </div>
            </div>
          </div>
          <div className="col-md-3 col-sm-12 tool-editor-container">
            <div className="tool-editor card">
              <div className="card-body">
                <div className="form-group">
                  <label>Layout</label>
                  <label htmlFor="file-upload" className="btn btn-secondary form-control">
                    Upload Layout
                  </label>
                  <input id="file-upload" type="file" name="layout_image"/>
                </div>
                <div className={`table-toolbox ${!this.props.UIState.uploaded ? 'disabled-div' : ''}`}>
                  <label>Tables</label>
                  <div className="card">
                    <div className="card-body table-toolbox-body" id="table-toolbox-body">
                      { unlocatedTables }
                    </div>
                  </div>
                </div>                
                <div className={`form-group table-action`}>
                  <Link ref="cancel" className="btn btn-outline-secondary" 
                    style={{width: 'calc(50% - 10px)', marginRight: '10px'}} to={`/home/${this.props.match.params.tgID}`}>
                    Cancel
                  </Link>
                  <button ref="save" type="button" className={`btn btn-primary ${!this.props.UIState.uploaded ? 'disabled-div' : ''}`} 
                    style={{width: 'calc(50% - 10px)', marginLeft: '10px'}} onClick={ this.handleSaveLayout }>
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    outlet: state.layout.outlet,
    table_group: state.layout.table_group,
    tables: state.layout.tables,
    UIState: state.layout.UIState
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getTableData,
    uploadImage,
    getLayoutDetail,
    saveLayout
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);