import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  getTableData,
  filterTable ,
  addTable,
  deleteTable,
  editTable,
  getOutletData,
  getInitData,
  changeTableGroup,
  saveTableGroup,
  createNewTableGroup,
  addInlineInput,
  removeInlineInput,
  changeInlineInput,
  changeTableGroupValue,
  setTableGroupValue,
  toggleCheckBox,
  toggleCheckBoxAll,
  deleteTableGroup,
  changeAlertStatus,
  nextTableGroup,
  editTableGroup,
  uploadImage,
  setErrorTGName,
  setErrorTGOutlet,
  getLayoutDetail,
  saveLayout,
  changeTGOutlet,
  changeUpdateInit
} from '../actions/FloorplanActions';

import TableGroup from '../components/TableGroup';
import TableDetail from '../components/TableDetail';
import Alert from '../components/Alert';
import Confirm from '../components/Confirm';

class Floorplan extends React.Component {
  constructor(props) {
    super(props);

    this.tgID = -1;
    this.outletID = -1;
    this.disabledGroup = 0;
    this.disabledDetail = 0;
    this.delTablesStatus = true;
    this.delTableStatus = {};
    this.columns = {};
    this.delButtonShown = false;
    this.init = false;
    this.handleFilterTable = this.handleFilterTable.bind(this);
    this.handleInlineSaveData = this.handleInlineSaveData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.changeOutlet = this.changeOutlet.bind(this);
    this.handleSaveTableGroup = this.handleSaveTableGroup.bind(this);
    this.handleCreateTableGroup = this.handleCreateTableGroup.bind(this);
    this.handleNewRowInput = this.handleNewRowInput.bind(this);
    this.handleRemoveRowInput = this.handleRemoveRowInput.bind(this);
    this.handleChangeRowInput = this.handleChangeRowInput.bind(this);
    this.handleChangeTableGroupValue = this.handleChangeTableGroupValue.bind(this);
    this.handleCheckBox = this.handleCheckBox.bind(this);
    this.handleCheckboxAll = this.handleCheckboxAll.bind(this);
    this.handleDeleteTableGroup = this.handleDeleteTableGroup.bind(this);
    this.handleShowDeleteAllTable = this.handleShowDeleteAllTable.bind(this);
    this.handleBulkDeleteTable = this.handleBulkDeleteTable.bind(this);
    this.handleCloseAlert = this.handleCloseAlert.bind(this);
    this.handleNextTableGroup = this.handleNextTableGroup.bind(this);
    this.handleEditTableGroup = this.handleEditTableGroup.bind(this);
    this.handleErrorTGName = this.handleErrorTGName.bind(this);
    this.handleErrorTGOutlet = this.handleErrorTGOutlet.bind(this);
    this.handleLayoutDetail = this.handleLayoutDetail.bind(this);
    this.handleGetTableData = this.handleGetTableData.bind(this);
    this.handleUploadImage = this.handleUploadImage.bind(this);
    this.handleSaveLayout = this.handleSaveLayout.bind(this);
    this.handleChangeTGOutlet = this.handleChangeTGOutlet.bind(this);
    this.handleChangeUpdateInit = this.handleChangeUpdateInit.bind(this);
  }

  componentWillMount() {
    if (this.props.match.params.tgID !== undefined) {
      this.props.getInitData(this.props.match.params.tgID);
    }else {
      this.props.getInitData();
    }
  }

  changeOutlet(event) {
    this.tgID = event.target.value;
    this.props.changeTableGroup(event.target.value);
    this.refs['tableGroup'].refs['outlet'].value = event.target.value;
    this.refs["tableDetail"].refs["table"].table = [];
  }

  handleChangeTGOutlet(outlet) {
    this.props.changeTGOutlet(outlet);
  }

  handleErrorTGOutlet(status) {
    this.props.setErrorTGOutlet(status);
  }

  handleFilterTable(field, val) {
    this.props.filterTable('field', val, field, this.tgID);
  }

  handleChangeTableGroupValue(field, value) {
    this.props.changeTableGroupValue(field, value);
  }

  handleDeleteTableGroup(ID) {
    this.props.deleteTableGroup(ID);
  }

  handleErrorTGName(status) {
    this.props.setErrorTGName(status)
  }

  handleLayoutDetail(tgID) {
    this.props.getLayoutDetail(tgID);
  }

  handleGetTableData(tgID) {
    this.props.getTableData(tgID);
  }

  handleUploadImage(status, initStatus) {
    this.props.uploadImage(status, initStatus);
  }

  handleChangeUpdateInit(value) {
    this.props.changeUpdateInit(value);
  }

  handleSaveLayout(tgID, imageFile, tableObj, tableResetObj) {
    this.props.saveLayout(tgID, imageFile, tableObj, tableResetObj);
  }

  handleBulkDeleteTable() {
    const toIDs = objCheckbox => {
      let ret = [];
      const n = Object.keys(objCheckbox).length,
            arrKeyObjCheckbox = Object.keys(objCheckbox);
      for (let i=0; i<n; i++) {
        const key = arrKeyObjCheckbox[i];
        if (objCheckbox[key] === true) {
          const ID = parseInt(key.split('k')[1]);
          ret.push({id: ID});
        }
      }

      return ret;
    }
    const IDs = toIDs(this.props.UIState.table.checkbox_list);
    this.props.deleteTable(JSON.stringify(IDs), this.props.UIState.table.checkbox_list, this.tgID);
  }

  handleInlineSaveData(data, ID, type) {
    if (type === 'ADD') {
      //ID would be table group ID if type ADD
      this.props.addTable(JSON.stringify(data), ID);
      this.refs["tableDetail"].refs["table"].table = [];
    }else {
      this.props.editTable(data, ID)
    }
  }

  handleNewRowInput(data) {
    this.props.addInlineInput(data, this.tgID);
  }

  handleRemoveRowInput(id) {
    this.props.removeInlineInput(id);
  }

  handleChangeRowInput(id, field, value) {
    this.props.changeInlineInput(id, field, value);
  }

  handleCreateTableGroup() {
    if (this.props.UIState.table_group.edited === 1) {
      window.location = location.href;
      window.location.reload();
    }else {
      if (this.props.UIState.table.create_table_group === 0 ) {
        this.disabledGroup = 0;
        this.disabledDetail = 1;
        this.props.createNewTableGroup(this.outletID);
        this.refs["tableDetail"].refs["table"].table = [];
      }else {
        window.location = location.href;
        window.location.reload();
      }
    }
  }

  handleNextTableGroup(tgObj) {
    this.tgID = tgObj.id;
    this.props.nextTableGroup(tgObj);
    this.props.getTableData(tgObj.id);
    this.refs["tableDetail"].refs["table"].table = [];
    this.refs["tableDetail"].refs["table"].init = false;
  }

  handleEditTableGroup(editStatus) {
    this.props.editTableGroup(editStatus);
  }

  handleShowDeleteAllTable() {
    if (this.delTablesStatus) {
      this.refs["btn-delete-all"].className = "float-left btn-delete";
      this.delTablesStatus = false;
    }else {
      this.refs["btn-delete-all"].className = "float-left btn-delete btn-delete-hidden";
      this.delTablesStatus = true;
    }
  }

  handleDeleteData(ID) {
    this.props.deleteTable(ID, this.props.UIState.table.checkbox_list);
  }

  handleCheckBox(ID) {
    this.props.toggleCheckBox(ID);
  }

  handleSaveTableGroup(tgItem) {
    let isCreated = false,
        tgData = {};
    const tables = this.refs['tableDetail'].refs['table'].table.map((el, i) => {
      return {
        name: el.value.name,
        pax: el.value.pax,
        shape: el.value.shape
      }
    });
    if (this.props.UIState.table.create_table_group === 1) {
      tgData = {
        name: tgItem.name,
        outlet: tgItem.outlet,
        active: tgItem.active
      }
      isCreated = true;
    }else {
      tgData = tgItem
    }

    this.props.saveTableGroup(tgData, tables, isCreated);
  }

  handleCheckboxAll(objData) {
    this.props.toggleCheckBoxAll(objData);
  }

  handleCloseAlert() {
    this.props.changeAlertStatus('LAYOUT', -1);
    this.props.changeAlertStatus('TABLE', -1);
    this.refs["tableDetail"].refs["table"].table = [];
  }

  render () {
    if (this.props.tableGroup.length > 0) {
      if (!this.init) {
        this.tgID = this.props.tableGroup[0].id;
        this.init = true;
      }
      this.disabledGroup = 0;
      this.disabledDetail = 0;
    }else {
      this.tgID = -1;
      if (this.props.UIState.table.create_table_group === 1) {
        this.disabledGroup = 0;
        this.disabledDetail = 0;
      }else {
        this.disabledGroup = 1;
        this.disabledDetail = 1;
      }
    }
    if (this.props.outlet.length > 0) {
      this.outletID = this.props.outlet[0].id;
    }
    const optionOutlet = this.props.outlet.map((element, index) => {
      return <option key={`opt${index}`} value={ element.id }>{ element.name }</option>;
    });

    const checkboxData = this.props.UIState.table.checkbox_list,
          checkboxLen = Object.keys(checkboxData).length,
          objCheckbox = Object.keys(checkboxData);

    this.delButtonShown = false;
    for (let i=0; i<checkboxLen; i++) {
      if (checkboxData[objCheckbox[i]]) {
        this.delButtonShown = true;
      }
    }

    const updated = this.props.layout.UIState.updated,
          saved = this.props.UIState.table.saved,
          error = this.props.UIState.error;
    let category = null,
        message = '',
        alert = '';

    if (error !== null) {
      category = 'danger';
      message = `[${error.code}] ${error.action} - ${error.message}`;
      alert = <Alert handleClose={ this.handleCloseAlert } category={ category } message={ message } />;
    }else {
      if (updated === 1 || saved === 1) {
        category = 'success';
        message = 'Data is successfully saved!';
        alert = <Alert handleClose={ this.handleCloseAlert } category={ category } message={ message } />;
      }else if (updated === 0 || saved === 0) {
        category = 'danger';
        message = `[${error.code}] ${error.action} - ${error.message}`;
        alert = <Alert handleClose={ this.handleCloseAlert } category={ category } message={ message } />;
      }else {
        alert = '';
      }
    }

    return (
      <div className="table-container">
        <div className="form-group table-header">
          <div className="sub-title">&nbsp;</div>
        </div>
        { alert }
        <div className="row">
          <div className="col-sm-12">
            <TableGroup
              ref="tableGroup"
              disabled={ this.disabledGroup }
              outlet={ this.props.outlet }
              UIState={ this.props.UIState }
              layout={ this.props.layout }
              tableGroup={ this.props.tableGroup }
              handleChangeTableGroupValue={ this.handleChangeTableGroupValue }
              handleDeleteTableGroup={ this.handleDeleteTableGroup }
              saveTableGroup={ this.handleSaveTableGroup }
              handleCloseAlert={ this.handleCloseAlert }
              handleNextTableGroup={ this.handleNextTableGroup }
              handleEditTableGroup={ this.handleEditTableGroup }
              setErrorTGName={ this.handleErrorTGName }
              setErrorTGOutlet={ this.handleErrorTGOutlet }
              changeTGOutlet={ this.handleChangeTGOutlet }
            />
          </div>
          <div className="col-sm-12">
            <div className="form-group row bulk-delete-container">
              <div className="col-sm-12">
                <div ref="btn-delete-all" className={ `float-left btn-delete ${!this.delButtonShown ? 'btn-delete-hidden' : ''}` }>
                  <a href="javascipt:void(0)" data-toggle="modal"
                    data-target="#bulkDelete"
                    className={ `btn btn-outline-secondary` }>
                    <i className="fa fa-trash"></i>&nbsp;
                    DELETE TABLE
                  </a>
                </div>
              </div>
            </div>
            <TableDetail
              ref="tableDetail"
              disabled={ this.disabledDetail }
              columns={ this.props.table.columns }
              outlet={ this.props.outlet }
              tableGroup={ this.props.tableGroup }
              tgID={ this.tgID }
              data={ this.props.table.data }
              UIState= { this.props.UIState }
              layout={ this.props.layout }
              handleNewRowInput= { this.handleNewRowInput }
              handleRemoveRowInput = { this.handleRemoveRowInput }
              handleChangeRowInput= { this.handleChangeRowInput }
              handleFilterTable={ this.handleFilterTable }
              handleInlineSaveData={ this.handleInlineSaveData }
              handleDeleteData={ this.handleDeleteData }
              handleCheckBox={ this.handleCheckBox }
              handleCheckboxAll= { this.handleCheckboxAll }
              handleShowDeleteAllTable={ this.handleShowDeleteAllTable }
              getLayoutDetail={ this.handleLayoutDetail }
              getTableData={ this.handleGetTableData }
              uploadImage={ this.handleUploadImage }
              changeUpdateInit={ this.handleChangeUpdateInit }
              saveLayout={ this.handleSaveLayout }
            />
          </div>
        </div>
        <Confirm
          title="Delete Confirmation!"
          button={{close: "Close", action: "Delete"}}
          body="Are you sure to delete this item?"
          handleActionButton={ () => this.handleBulkDeleteTable() }
          confirmID="bulkDelete"
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    table: state.table,
    outlet: state.outlet,
    tableGroup: state.tableGroup,
    UIState: state.UIState,
    layout: state.layout
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getTableData,
    filterTable,
    addTable,
    deleteTable,
    editTable,
    getOutletData,
    getInitData,
    changeTableGroup ,
    saveTableGroup,
    createNewTableGroup,
    addInlineInput,
    removeInlineInput,
    changeInlineInput,
    changeTableGroupValue,
    setTableGroupValue,
    toggleCheckBox,
    toggleCheckBoxAll,
    deleteTableGroup,
    changeAlertStatus,
    nextTableGroup,
    editTableGroup,
    uploadImage,
    setErrorTGName,
    setErrorTGOutlet,
    getLayoutDetail,
    saveLayout,
    changeTGOutlet,
    changeUpdateInit
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Floorplan);
