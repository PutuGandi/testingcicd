import {
  applyMiddleware,
  combineReducers,
  createStore
} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import { tableReducer } from './floorplan/reducers/tableReducer';
import { outletReducer } from './floorplan/reducers/outletReducer';
import { tableGroupReducer } from './floorplan/reducers/tableGroupReducer';
import { UIStateReducer } from './floorplan/reducers/UIStateReducer';
import { layoutReducer } from './floorplan/reducers/layoutReducer';

export default function store(mode){
  const reducer = combineReducers(
    {
      table: tableReducer,
      outlet: outletReducer,
      tableGroup: tableGroupReducer,
      UIState: UIStateReducer,
      layout: layoutReducer,
    }
  )

  if (mode === 'dev') {
    const middleware = applyMiddleware(thunk, logger);
    return createStore(reducer, middleware);
  }else {
    const middleware = applyMiddleware(thunk);
    return createStore(reducer, middleware);
  }
}