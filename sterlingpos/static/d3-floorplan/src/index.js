import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
  Route,
  HashRouter,
} from 'react-router-dom';

import Floorplan from './floorplan/containers/FloorplanContainer';
import store from './store';

const comp = document.getElementById('main'),
      tgID = comp.getAttribute('data-ta-id');
let mode = comp.getAttribute('data-mode');
const _url = window.location.href;
let arrUrl = [];

if (tgID === null) {
  arrUrl = _url.split('#/');
  if (arrUrl[1] === undefined) {
    window.location.href = `${_url}#/new`;
  }
}else {
  arrUrl = _url.split('#/');
  if (arrUrl[1] === undefined) {
    window.location.href = `${_url}#/edit/${tgID}`;
  }
}

if (mode === null) {
  mode = 'dev';
}

ReactDOM.render(
  <Provider store={ store(mode) }>
    <HashRouter>
      <div>
        <Route exact path="/new" component={ Floorplan } />
        <Route exact path="/edit/:tgID" component={ Floorplan } />
      </div>
    </HashRouter>
  </Provider>, 
comp);