import os

from django.utils.termcolors import make_style

from fabric import task
from invocations.console import confirm

APPS_DIR = '/var/www/apps'
ENVS_DIR = '/var/www/envs'
PROJECT_SLUG = 'sterlingpos'
PROJECT_OWNER = 'tapestrix'
PROJECT_DIR = os.path.join(APPS_DIR, PROJECT_SLUG)
ENV_DIR = os.path.join(ENVS_DIR, PROJECT_SLUG)
ACTIVATE_ENV = f'source {ENV_DIR}/bin/activate'
DJANGO_DIR = os.path.join(PROJECT_DIR, PROJECT_SLUG)
LOG_DIR = os.path.join(PROJECT_DIR, 'log')
BIN_DIR = os.path.join(PROJECT_DIR, 'bin')
REQUIREMENTS_PATH = os.path.join(DJANGO_DIR, 'requirements/production.txt')
PRODUCTION_BRANCH = 'master'

UWSGI_SERVICE_NAME = PROJECT_SLUG
UWSGI_RELOAD_FILE = f'/tmp/{PROJECT_SLUG}-reload'
CELERY_W_SERVICE_NAME = f'{PROJECT_SLUG}-celery-worker'
CELERY_B_SERVICE_NAME = f'{PROJECT_SLUG}-celery-beat'
RUN_AS_OWNER = f'sudo -u {PROJECT_OWNER}'


success = make_style(fg='green')
info = make_style(fg='blue')
debug = make_style(fg='cyan')
warning = make_style(fg='yellow')
error = make_style(fg='red')


hosts = []

"""
    How to use this fab script :
    - If your user isn't root, please provide password in `./fabric.yaml` (logged user must have sudo perm).
    - Update your local `~/.ssh/config` to enable server alias (optional).
    - Run fab by specifying the host info and task, example: "fab -H {server_alias} {method-name}"
"""

# - Temporary workaround in order to make "sudo" works nicely with "cd" command
#   [https://github.com/pyinvoke/invoke/issues/459#issuecomment-426128631]


@task
def install_req(c):
    """
    Install all python requirements.
    """
    print(info(f'\nInstalling all python requirements.'))
    c.sudo(
        f'bash -c "cd {DJANGO_DIR} && {ACTIVATE_ENV} && pip install -r {REQUIREMENTS_PATH}"',
        user=PROJECT_OWNER,
    )


@task
def migrate(c):
    """
    Migrate django database.
    """
    print(info(f'\nApplying all migrations.'))
    c.sudo(
        f'bash -c "cd {DJANGO_DIR} && {ACTIVATE_ENV} && python manage.py migrate"',
        user=PROJECT_OWNER,
    )


@task
def compilescss(c):
    """
    Compile all sass/scss files using django sass processor.
    """
    print(info(f'\nCompiling all sass/scss files.'))
    c.sudo(
        f'bash -c "cd {DJANGO_DIR} && {ACTIVATE_ENV} && python manage.py compilescss"',
        user=PROJECT_OWNER,
    )


@task
def collectstatic(c):
    """
    Collect all django staticfiles.
    """
    print(info(f'\nCollecting all static files.'))
    c.sudo(
        f'bash -c "cd {DJANGO_DIR} && {ACTIVATE_ENV} && python manage.py collectstatic --noinput"',
        user=PROJECT_OWNER,
    )


@task
def compilemessages(c):
    """
    Compile all .po files.
    """
    print(info(f'\nCompiling all .po files.'))
    c.sudo(
        f'bash -c "cd {DJANGO_DIR} && {ACTIVATE_ENV} && python manage.py compilemessages"',
        user=PROJECT_OWNER,
    )


@task
def django_cmd(c, cmd):
    """
    Run django management command.
    """
    print(info(f'\nRunning django command "{cmd}".'))
    c.sudo(
        f'bash -c "cd {DJANGO_DIR} && {ACTIVATE_ENV} && python manage.py {cmd}"',
        user=PROJECT_OWNER,
    )


@task
def start_all(c):
    """
    Start all daemon scripts (django, celery worker, celery beat).
    """
    print(info(f'\nStarting all services (django, celery worker, celery beat).'))
    c.sudo(f'systemctl start {UWSGI_SERVICE_NAME}')
    c.sudo(f'systemctl start {CELERY_W_SERVICE_NAME}')
    c.sudo(f'systemctl start {CELERY_B_SERVICE_NAME}')


@task
def stop_all(c):
    """
    Stop all daemon scripts (django, celery worker, celery beat).
    """
    print(info(f'\nStopping all services (django, celery worker, celery beat).'))
    c.sudo(f'systemctl stop {UWSGI_SERVICE_NAME}')
    c.sudo(f'systemctl stop {CELERY_W_SERVICE_NAME}')
    c.sudo(f'systemctl stop {CELERY_B_SERVICE_NAME}')


@task
def restart_app(c, soft=True):
    """
    Restart django application daemon.
    """
    if soft:
        print(info(f'\nRestarting django app server in SOFT mode.'))
        c.sudo(f'touch {UWSGI_RELOAD_FILE}')
    else:
        print(info(f'\nRestarting django app server in HARD mode.'))
        c.sudo(f'systemctl restart {UWSGI_SERVICE_NAME}')


@task
def restart_celery_worker(c):
    """
    Restart celery worker.
    """
    print(info(f'\nRestarting celery worker systemd.'))
    c.sudo(f'systemctl restart {CELERY_W_SERVICE_NAME}')


@task
def restart_celery_beat(c):
    """
    Restart celery beat.
    """
    print(info(f'\nRestarting celery beat systemd.'))
    c.sudo(f'systemctl restart {CELERY_B_SERVICE_NAME}')


@task
def restart_celery(c):
    """
    Restart all celery daemons (celery worker, celery beat).
    """
    restart_celery_beat(c)
    restart_celery_worker(c)


@task
def restart_all(c, soft=True):
    """
    Restart all daemon scripts (django, celery worker, celery beat).
    """
    restart_app(c, soft=soft)
    restart_celery(c)


@task
def deploy(c):
    """
    Restart all daemon scripts (django, celery worker, celery beat).
    """
    confirmed = confirm(warning('Are you sure want to deploy new updates?'))
    if confirmed:
        print(info(f'\nPulling from project\'s "{PRODUCTION_BRANCH}" branch.'))
        c.sudo(
            f'bash -c "cd {DJANGO_DIR} && git pull origin {PRODUCTION_BRANCH}"',
            user=PROJECT_OWNER,
        )
        install_req(c)
        migrate(c)
        collectstatic(c)
        compilemessages(c)
        restart_all(c)
