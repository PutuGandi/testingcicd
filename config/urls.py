from django.urls import reverse_lazy
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import RedirectView
from django.views import defaults as default_views

from sterlingpos.users import views as users_views
from sterlingpos.dashboard import views as dashboard_views
from sterlingpos.core import views as core_views

from rest_framework_jwt.views import obtain_jwt_token
from sterlingpos.core.midtrans.views import MidtransNotificationHandlingView
from sterlingpos.integration.shopeepay.views import ShopeepayNotificationHandlingView


urlpatterns = [
    url(r"^$", RedirectView.as_view(url=reverse_lazy('dashboard')), name="home"),

    url(r'^accounts/signup/$', users_views.WizardUserSignUpView.as_view(), name='account_signup'),
    url(r"^accounts/confirm-email/(?P<key>[-:\w]+)/$", users_views.EmailConfirmation.as_view(),
        name="account_confirm_email"),

    url(settings.ADMIN_URL, admin.site.urls),

    url(
        regex=r'^dashboard/$',
        view=dashboard_views.DashboardIndexView.as_view(),
        name='dashboard'
    ),
    url(
        regex=r'^dashboard/branch/(?P<branch_pk>[0-9]+)$',
        view=dashboard_views.DashboardIndexView.as_view(),
        name='dashboard'
    ),
    url(
        regex=r'^chart/$',
        view=dashboard_views.DashboardChartView.as_view(),
        name='dashboard-chart'
    ),
    url(
        regex=r'^chart/branch/(?P<branch_pk>[0-9]+)$',
        view=dashboard_views.DashboardChartView.as_view(),
        name='dashboard-chart'
    ),
    url(
        regex=r'^summary-chart/$',
        view=dashboard_views.DashboardSalesSummaryGroupView.as_view(),
        name='summary-chart'
    ),
    url(
        regex=r'^summary-chart/branch/(?P<branch_pk>[0-9]+)$',
        view=dashboard_views.DashboardSalesSummaryGroupView.as_view(),
        name='summary-chart'
    ),
    url(
        regex=r'^category-chart/$',
        view=dashboard_views.DashboardCategoryChartView.as_view(),
        name='dashboard-category-chart'
    ),
    url(
        regex=r'^category-chart/branch/(?P<branch_pk>[0-9]+)$',
        view=dashboard_views.DashboardCategoryChartView.as_view(),
        name='dashboard-category-chart'
    ),


    # User management
    url(
        r"^settings/users/",
        include("sterlingpos.users.urls", namespace="users"),
    ),
    url(
        r"^settings/accounts/",
        include("sterlingpos.accounts.urls", namespace="accounts"),
    ),
    url(
        r"^settings/subscription/",
        include("sterlingpos.subscription.urls", namespace="subscription"),
    ),
    url(
        r"^cash-balance/",
        include("sterlingpos.cash_balance.urls", namespace="cash_balance"),
    ),
    url(
        r"^sales/",
        include("sterlingpos.sales.urls", namespace="sales"),
    ),
    url(
        r"^stocks/",
        include("sterlingpos.stocks.urls", namespace="stocks"),
    ),
    url(
        r"^library/promo/",
        include("sterlingpos.promo.urls", namespace="promo"),
    ),
    url(
        r"^library/",
        include("sterlingpos.catalogue.urls", namespace="catalogue"),
    ),
    url(
        r"^device/",
        include("sterlingpos.device.urls", namespace="device"),
    ),
    url(
        r"^private-apps/",
        include("sterlingpos.private_apps.urls", namespace="private-apps"),
    ),
    url(
        r"^table-management/",
        include(
            "sterlingpos.table_management.urls", namespace="table_management"),
    ),
    url(
        r"^table-order/",
        include(
            "sterlingpos.table_order.urls", namespace="table_order"),
    ),
    url(
        r"^role/",
        include(
            "sterlingpos.role.urls", namespace="role"),
    ),
    url(
        r"^waiters/",
        include("sterlingpos.waiters.urls", namespace="waiters"),
    ),
    url(
        r"^kitchen-display/",
        include(
            "sterlingpos.kitchen_display.urls", namespace="kitchen_display"),
    ),
    url(
        r"^settings/",
        include("sterlingpos.outlet.urls", namespace="outlet"),
    ),
    url(
        r"^settings/receipt/",
        include("sterlingpos.receipt.urls", namespace="receipt"),
    ),
    url(
        r"^settings/surcharges/",
        include("sterlingpos.surcharges.urls", namespace="surcharges"),
    ),
    url(
        r"^customers/",
        include("sterlingpos.customer.urls", namespace="customers"),
    ),
    url(
        r"^digital-payment/",
        include("sterlingpos.digital_payment.urls", namespace="digital_payment"),
    ),
    url(
        r"^productions/",
        include("sterlingpos.productions.urls", namespace="productions")
    ),

    url(
        r"^notification/",
        include("sterlingpos.notification.urls", namespace="notification")
    ),


    url(
        regex=r'^formset/$',
        view=core_views.UpdateFormset.as_view(),
        name='formset_update'
    ),

    url(r"^accounts/", include("allauth.urls")),

    url(r'^invitations/', include('sterlingpos.users.invitation_urls', namespace='invitation')),
    url(r'^accounts/invitations/send-invite/complete/$', users_views.SendInviteComplete.as_view(),
        name='send-invite-complete'),
    url(r'^accounts/invitations/send-invite/$', users_views.SendInvite.as_view(),
        name='send-invite'),
    url(r'^accounts/invitations/register/$', users_views.InviteeRegistration.as_view(),
        name='invitee-registration'),
    url(r'^accounts/invitations/register/success/$', users_views.InviteeRegistrationComplete.as_view(),
        name='invitee-registration-success'),

    url(r'^accounts/invitations/accept-invite/(?P<key>\w+)/?$', users_views.AcceptInvite.as_view(),
        name='accept-invite'),

    url(r'^accounts/invitations/delete/$', users_views.UserInvitationDeleteView.as_view(),
        name='invitation-delete'),

    url(r'^accounts/invitations/', include('invitations.urls', namespace='invitations')),

    url(r"^api-token-auth/", obtain_jwt_token),

    url(r'^midtrans/notification/handling/$', MidtransNotificationHandlingView.as_view(), name="veritrans_notification"),
    url(r'^callback/shopeepay/$', ShopeepayNotificationHandlingView.as_view(), name="shoopepay_notification"),

    url(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/v2/', include('sterlingpos.api.urls_2', namespace='v2')),
    url(r'^api/v2.1/', include('sterlingpos.api.urls_2_1', namespace='v2.1')),


    # Your stuff: custom urls includes go here
] + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)

if "silk" in settings.INSTALLED_APPS:
    urlpatterns = [url(r'^silk/', include('silk.urls', namespace='silk'))] + urlpatterns


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(
            r"^400/$",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        url(
            r"^403/$",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        url(
            r"^404/$",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        url(r"^500/$", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [url(r"^__debug__/", include(debug_toolbar.urls))] + urlpatterns
