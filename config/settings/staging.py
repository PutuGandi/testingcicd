from .production import *  # noqa
from .base import env

STAGING = True
SILK_ON = False
# ------------------------------------------------------------------------------
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
INSTALLED_APPS += ['debug_toolbar', 'django_pdb',]  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', 'django_pdb.middleware.PdbMiddleware', ]  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ['127.0.0.1', '10.0.2.2']

if SILK_ON:
    INSTALLED_APPS += ["silk", ]
    MIDDLEWARE = ['silk.middleware.SilkyMiddleware', ] + MIDDLEWARE

    SILKY_PYTHON_PROFILER = True
    SILKY_DYNAMIC_PROFILING = [{
        'module': 'sterlingpos.api.views.sales_views',
        'function': 'SalesOrderListView.get'
    }, {
        'module': 'sterlingpos.api.views.sales_views',
        'function': 'TransactionOrderCreateView.create'
    }]

# django-extensions
# ------------------------------------------------------------------------------
# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration
INSTALLED_APPS += ['django_extensions']  # noqa F405

REST_FRAMEWORK.update({
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'sterlingpos.api.authentication.MultiTenantJSONWebTokenAuthentication',
        'sterlingpos.api.authentication.MultiTenantPrivateAppTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication'
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
})

INSTALLED_APPS.remove('ddtrace.contrib.django')

CELERYBEAT_SCHEDULE = {
    'daily_stock_transaction_aggregation': {
        'task': 'sterlingpos.stocks.tasks.daily_stock_transaction_aggregation',
        'schedule': crontab(hour=0, minute=0),
    },
    'hourly_stock_transaction_aggregation': {
        'task': 'sterlingpos.stocks.tasks.hourly_stock_transaction_aggregation',
        'schedule': crontab(hour='*/2', minute=0),
    },
}
PAYMENT["SHOPEEPAY"]["HOST"] = "https://api.uat.wallet.airpay.co.id"
