"""
Base settings to build other settings files upon.
"""

import environ
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta

from celery.schedules import crontab

ROOT_DIR = environ.Path(__file__) - 3  # (sterlingpos/config/settings/base.py - 3 = sterlingpos/)
APPS_DIR = ROOT_DIR.path('sterlingpos')

env = environ.Env()

READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(ROOT_DIR.path('.env')))

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)
STAGING = env.bool('DJANGO_STAGING', False)
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = 'Asia/Jakarta'
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'id'
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

LANGUAGES = (
    ('id', _('Indonesian')),
)
LOCALE_PATHS = (
    str(ROOT_DIR.path('locale')),
)

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db('DATABASE_URL'),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'config.urls'
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',  # Handy template tags
    'django.contrib.admin',
    'django.forms',
]
THIRD_PARTY_APPS = [
    'treebeard',
    'crispy_forms',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'rest_framework',
    'corsheaders',
    'drf_yasg',
    'dal',
    'dal_select2',

    'import_export',
    'invitations',
    'imagekit',
    'safedelete',

    'django_filters',

    'cacheops',
    'rules.apps.AutodiscoverRulesConfig',
    'qrcode',
]
LOCAL_APPS = [
    'sterlingpos.users.apps.UsersConfig',
    # Your stuff: custom apps go here

    'sterlingpos.accounts.apps.AccountsConfig',
    'sterlingpos.province',
    'sterlingpos.catalogue.apps.CatalogueConfig',
    'sterlingpos.sales.apps.SalesConfig',
    'sterlingpos.outlet.apps.OutletConfig',
    'sterlingpos.device.apps.DeviceConfig',
    'sterlingpos.table_management.apps.TableManagementConfig',
    'sterlingpos.table_order.apps.TableOrderConfig',
    'sterlingpos.cash_balance.apps.CashBalanceConfig',
    'sterlingpos.api.apps.APIConfig',
    'sterlingpos.push_notification.apps.PushNotificationConfig',
    'sterlingpos.waiters.apps.WaitressConfig',
    'sterlingpos.subscription.apps.SubscriptionConfig',
    'sterlingpos.stocks.apps.StocksConfig',
    'sterlingpos.surcharges.apps.SurchargeConfig',
    'sterlingpos.kitchen_display.apps.KitchenDisplayConfig',

    'sterlingpos.role.apps.RoleConfig',

    'sterlingpos.core',

    'sterlingpos.promo',
    'sterlingpos.receipt',

    'sterlingpos.customer.apps.CustomerConfig',
    'sterlingpos.private_apps.apps.PrivateAppsConfig',
    'sterlingpos.productions.apps.ProductionsConfig',

    'sterlingpos.digital_payment',
    'sterlingpos.integration.shopeepay.apps.ShopeePayConfig',
    'sterlingpos.integration.cashlez.apps.CashlezConfig',

    'sterlingpos.notification.apps.NotificationConfig',

]
# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIGRATIONS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
MIGRATION_MODULES = {
    'sites': 'sterlingpos.contrib.sites.migrations'
}

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = 'users.User'
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = 'dashboard'
# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = 'account_login'

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',

    'corsheaders.middleware.CorsMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'sterlingpos.users.middlewares.IsAuthenticatedMiddleware',
    'sterlingpos.core.multitenancy.SetCurrentTenantFromUser',
    'sterlingpos.users.middlewares.SubscriptionUserMiddleware',
    'sterlingpos.users.middlewares.UserDashboardRedirectMiddleware',
]

# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [
    str(APPS_DIR.path('static')),
]
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'sterlingpos.core.context_processors.staging',
            ],
            'libraries':{
                'visibility_contains': 'sterlingpos.sales.template_tags.visibility_contains',
            },
        },
    },
]
# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap4'
FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

# FIXTURES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL regex.
ADMIN_URL = r'^admin/'
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [
    ("""Manta""", 'adhitya.nugroho@tech.indosterling.com'),
    ("""Arif""", 'arif.bintoro@tech.indosterling.com'),
    ("""Agus""", 'agus.wahyudi@tech.indosterling.com'),
]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# Celery
# ------------------------------------------------------------------------------
INSTALLED_APPS += ['sterlingpos.taskapp.celery.CeleryConfig']
BROKER_URL = env('CELERY_BROKER_URL', default='redis://localhost:6379/0')
CELERY_RESULT_BACKEND = env('CELERY_RESULT_BACKEND', default=BROKER_URL)
CELERY_ENABLE_UTC = True
CELERY_TIMEZONE = TIME_ZONE
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-accept_content
CELERY_ACCEPT_CONTENT = ['json']
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-task_serializer
CELERY_TASK_SERIALIZER = 'json'
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-result_serializer
CELERY_RESULT_SERIALIZER = 'json'

CELERYBEAT_SCHEDULE = {
    'automatic_outlet_transaction_export': {
        'task': 'sterlingpos.sales.tasks.automatic_outlet_transaction_export',
        'schedule': crontab(hour=0, minute=0),
    },
    'automatic_daily_transaction_report': {
        'task': 'sterlingpos.sales.tasks.daily_transaction_report',
        'schedule': crontab(hour=0, minute=0),
    },
    'automatic_daily_registration_report': {
        'task': 'sterlingpos.users.tasks.daily_user_registration_report',
        'schedule': crontab(hour=0, minute=0),
    },
    'daily_stock_balance_mv_refresh': {
        'task': 'sterlingpos.stocks.tasks.refresh_daily_stock_balance_materialized_view_task',
        'schedule': crontab(hour=0, minute=0),
    },
    'daily_stock_transaction_aggregation': {
        'task': 'sterlingpos.stocks.tasks.daily_stock_transaction_aggregation',
        'schedule': crontab(hour=0, minute=0),
    },
    'hourly_stock_transaction_aggregation': {
        'task': 'sterlingpos.stocks.tasks.hourly_stock_transaction_aggregation',
        'schedule': crontab(hour='*/2', minute=0),
    },
    'notify_expired_stock_email': {
        "task": 'sterlingpos.notification.tasks.notify_expired_email',
        'schedule': crontab(hour=0, minute=0),
    },
    'cleaning_up_invalid_fcm': {
        'task': 'sterlingpos.push_notification.tasks.cleaning_up_invalid_fcm',
        'schedule': crontab(hour=0, minute=0),
    },
}


# django-allauth
# ------------------------------------------------------------------------------
ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_AUTHENTICATION_METHOD = 'email'
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_EMAIL_REQUIRED = True
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_ADAPTER = 'sterlingpos.users.adapters.AccountAdapter'
# https://django-allauth.readthedocs.io/en/latest/configuration.html
SOCIALACCOUNT_ADAPTER = 'sterlingpos.users.adapters.SocialAccountAdapter'
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True
ACCOUNT_CONFIRM_EMAIL_ON_GET = True

# django-compressor
# ------------------------------------------------------------------------------
# https://django-compressor.readthedocs.io/en/latest/quickstart/#installation
INSTALLED_APPS += ['compressor']
STATICFILES_FINDERS += ['compressor.finders.CompressorFinder']
# Your stuff...
# ------------------------------------------------------------------------------
SILENCED_SYSTEM_CHECKS = ["auth.W004"]

AUTO_UPDATE_COST = env.bool('DJANGO_AUTO_UPDATE_COST', default=True)
EXCLUDE_ZERO_SALES = env.bool('DJANGO_EXCLUDE_ZERO_SALES', default=False)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'sterlingpos.api.authentication.MultiTenantJSONWebTokenAuthentication',
        'sterlingpos.api.authentication.MultiTenantPrivateAppTokenAuthentication',

        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
        'rest_framework.filters.OrderingFilter',
    ),
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30,
}

JWT_AUTH = {
    'JWT_PAYLOAD_HANDLER': 'sterlingpos.api.authentication.sterling_jwt_payload_handler',
    'JWT_EXPIRATION_DELTA': timedelta(days=30),
}

CORS_ORIGIN_WHITELIST = env.list('DJANGO_CORS_ORIGIN_WHITELIST', default=[])
FCM_API_KEY = env('FCM_API_KEY', default="")
INVITATIONS_INVITATION_MODEL = "users.UserInvitation"

CONTACT_RECIPIENT = ['support@kawn.co.id', ]

MKT_PAGE_CACHE_TIMEOUT = 60 * 5

MIDTRANS_CLIENT_KEY = env('MIDTRANS_CLIENT_KEY', default="")
MIDTRANS_SERVER_KEY = env('MIDTRANS_SERVER_KEY', default="")
MIDTRANS_DEBUG = env.bool('MIDTRANS_DEBUG', False)

SWAGGER_SETTINGS = {
    'DEFAULT_GENERATOR_CLASS': 'sterlingpos.api.schema.SterlingposOpenAPISchemaGenerator',
}

# django-sass-processor
# ------------------------------------------------------------------------------
INSTALLED_APPS += ['sass_processor']
STATICFILES_FINDERS += ['sass_processor.finders.CssFinder']
SASS_PROCESSOR_AUTO_INCLUDE = False
SASS_PRECISION = 8

NON_AUTH_URL_NAME = [
    LOGIN_URL,
    'account_signup',
    'account_email_verification_sent',
    'account_confirm_email',
    'account_reset_password',
    'account_reset_password_done',
    'account_reset_password_from_key',
    'account_reset_password_from_key_done',

    'accept-invite',
    'invitee-registration',
    'invitee-registration-success',

    'outlet:province_autocomplete',
    'outlet:cities_autocomplete',

    'veritrans_notification',
    'shoopepay_notification',

    'rest_framework:*',
    'v2:*',
    'v2.1:*',
]

CACHEOPS_DEFAULTS = {
    'timeout': 60*60
}
CACHEOPS = {
    'users.user': {'ops': 'get'},

    'accounts.account': {'ops': 'get'},
    'accounts.accountowner': {'ops': 'get'},

    'sales.paymentmethod': {'ops': {'get', 'fetch', 'count', 'exists'}},
    'sales.paymentmethodusage': {'ops': {'get', 'fetch', 'count', 'exists'}},
    'sales.enabledpaymentmethod': {'ops': {'get', 'fetch', 'count', 'exists'}},

    'device.deviceuser': {'ops': {'get', 'fetch', 'count'}},

    'table_management.tablemanagementsettings': {'ops': {'get', 'fetch', 'count'}},
    'waiters.waitersettings': {'ops': {'get', 'fetch', 'count'}},
    'kitchen_display.kitchendisplaysetting': {'ops': {'get', 'fetch', 'count'}},

    'subscription.subscriptionplan': {'ops': {'get', 'fetch', 'count', 'exists'}},
    'subscription.subscription': {'ops': {'get', 'fetch', 'count', 'exists'}},

    'outlet.outlet': {'ops': {'get', 'fetch', 'count', 'exists'}},

    # Cache all queries to Permission
    # 'all' is an alias for {'get', 'fetch', 'count', 'aggregate', 'exists'}
    'auth.permission': {'ops': 'all'},
}

HIGH_DECIMAL_MAX_DIGITS = 19
LOW_DECIMAL_MAX_DIGITS = 12

DECIMAL_PLACES = 2

PAYMENT = {
    "SHOPEEPAY" : {
        "HOST": "https://api.wallet.airpay.co.id",
        "KEY": env('SHOPEEPAY_KEY'),
        "SECRET": bytes(env('SHOPEEPAY_SECRET'), "utf-8"),
    },
    "CASHLEZ": {
        "KEY": env("CASHLEZ_PUBLIC_KEY", default=""),
        "SECRET": env.str("CASHLEZ_PRIVATE_KEY", default="", multiline=True),
    }
}

CUSTOM_REPORT_RECIPIENTS = {
    "user_registration_report": [
        "adhitya.nugroho@tech.indosterling.com",
        "resky.hakita@lokamedia.com",
        "firna.andriani@tech.indosterling.com",
    ],
    "account_transaction_report": [
        "adhitya.nugroho@tech.indosterling.com",
    ]
}

INVITATIONS_ACCEPT_INVITE_AFTER_SIGNUP = True
INVITATIONS_ADAPTER = ACCOUNT_ADAPTER
