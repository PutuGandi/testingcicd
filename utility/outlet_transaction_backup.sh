#!/usr/bin/env bash

set -o errexit
set -o pipefail

if [ -z "$1" ]
  then
    echo "please provide outlet_id"
	exit 1
fi

if [ -z "$2" ]
  then
    echo "please provide date in format YYYY-mm-dd"
	exit 1
fi

OUTLET_ID=${1}
START_DATE=${2}
END_DATE=$START_DATE
FIELD_NAMES="id, created, modified, status, mode, code, global_discount_percentage, global_discount_fixed_price_currency, global_discount_fixed_price"
FIELD_NAMES_2="global_discount_price_currency, global_discount_price, round_digit, sub_total_currency, sub_total, total_cost_currency, total_cost"
FIELD_NAMES_3="tendered_amount_currency, tendered_amount, transaction_date, tax, change_currency, change, served_by, guest_number, order_name, account_id"
FIELD_NAMES_4="invoice_id, order_option_id, outlet_id, payment_method_id, transaction_type_id, table_id, deleted, gratuity, guest_name, cashier_name, customer_id"
if [[ ! -z "$3" ]]
  then
    END_DATE=${3}
fi

if [ -z "$BACKUP_PG_PASSWORD" ]
  then
    echo "please provide backup password"
	exit 1
fi

if [ -z "$BACKUP_PG_HOST" ]
  then
    echo "please provide backup postgres host"
	exit 1
fi

COPY_SCRIPT="copy (select $FIELD_NAMES, $FIELD_NAMES_2, $FIELD_NAMES_3, $FIELD_NAMES_4 from sales_salesorder where created between date '$START_DATE' and date '$END_DATE' and outlet_id=$OUTLET_ID order by created desc) to stdout with delimiter '|';"

echo "Copy pasta outlet data $OUTLET_ID on $START_DATE - $END_DATE..."

PGPASSWORD=$DB_PWD psql -h private-kawn-postgresql-sgp1-89404-do-user-4120195-0.db.ondigitalocean.com -p 25061 -U sterlingpos -d sterlingpos-production-pool -c "$COPY_SCRIPT" | PGPASSWORD=$BACKUP_PG_PASSWORD psql -h $BACKUP_PG_HOST -p $BACKUP_PG_PORT -d pos -U pos -c "copy sales_salesorder from stdin with delimiter '|';"

echo "The transaction data for outlet '$OUTLET_ID' on $START_DATE - $END_DATE has been successfully pasta'd."
