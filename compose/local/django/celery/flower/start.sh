#!/bin/bash

set -o errexit
set -o nounset

celery flower \
    --app=sterlingpos.taskapp.celery \
    --broker="${CELERY_BROKER_URL}" \
    --basic_auth="${CELERY_FLOWER_USER}:${CELERY_FLOWER_PASSWORD}"
