#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace


celery -A sterlingpos.taskapp worker -l INFO
