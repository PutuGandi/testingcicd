#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A sterlingpos.taskapp beat -l INFO
